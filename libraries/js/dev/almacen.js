var x=$(document);
 x.ready(inicio);
 (function($){
/*------- MANEJO DE ALMACENES -------*/
   	/*--- Buscador ---*/
   	$.fn.reset_input=function(){
 		var id=$(this).attr("id");
 		if(id!="s_cod"){ $("#s_cod").val(""); }
		if(id!="s_nom"){ $("#s_nom").val(""); }
		if(id!="s_tip"){ $("#s_tip").val(""); }
		//caso historial de materiales
		if(id!="s_cod"){ $("#s_cod").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }
		if(id!="s_nom"){ $("#s_nom").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }
		if(id!="s_alm"){ $("#s_alm").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }
		if(id!="s_usu"){ $("#s_usu").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }	
		if(id!="s_sol"){ $("#s_sol").val(""); }else{ $("#hra").val("");$("#fech_a").val("");$("#fech_b").val(""); }	
   	}
   	$.fn.search_almacen=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_almacen($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_almacen($(this));
 			}
 		});
 		function search_almacen(e){
			 e.reset_input();
			 e.search_elements();
 		}
	}
	$.fn.blur_all=function(){
		OnBlur("search_cod");
		OnBlur("search_nom");
		OnBlur("search_tip");
		OnBlur("s_cod");
		OnBlur("s_nom");
		OnBlur("s_alm");
		OnBlur("s_usu");
		OnBlur("s_sol");
		OnBlur("fech_a");
		OnBlur("fech_b");
	}
	$.fn.view_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_almacen($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_almacen($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_almacen($(this));
	    	event.preventDefault();
	    });
 		function view_almacen(e){
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('tip',$("#s_tip").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('almacen/view_almacen',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
	/*--- Reportes ---*/
  	$.fn.reporte_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				reporte_almacen($(this));
 			}
 		});
 		function reporte_almacen(e){
 			if(e.data("a")!=undefined){
		  		modal("ALMACEN: Detalle","sm","1");
		 		btn_modal('',"",'',"",'1');
		 		var atrib=new FormData();
		 		atrib.append('a',e.data("a"));
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('almacen/reporte_almacen',atrib,controls);
 			}
 		}
  	}
	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	$.fn.config_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				config_almacen($(this));
 			}
 		});
 		function config_almacen(e){
			if(e.data("a")!=undefined){
		 		modal("ALMACEN: Modificar","md","1");
		 		var atr="this|"+JSON.stringify({a:e.data("a")});
		 		btn_modal('update_almacen',atr,'',"",'1');
		 		var atrib=new FormData();
		 		atrib.append('a',e.data("a"));
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		$(this).get_1n('almacen/config_almacen',atrib,controls);
	   		}
 		}

	}
	$.fn.update_almacen=function(){
		if($(this).data("a")!=undefined){
		 	var fot=$("#archivo").prop('files');
		 	var cod=$("#cod").val();
		 	var nom=$("#nom").val();
			var des=$("#des").val();
			if(strSpace(cod,2,10)){
				if(strSpace(nom,2,100)){
						var control=true;
						if(des!=""){ if(!textarea(des,0,400)){ control=false; alerta('Ingrese un contenido válido','top','des');}}
						if(control){
							var atrib= new FormData();
							atrib.append("a",$(this).data("a"));
							atrib.append("cod",cod);
							atrib.append("nom",nom);
							atrib.append("des",des);
							atrib.append("archivo",fot[0]);
							var atrib3=new FormData();
							atrib3.append('cod',$("#s_cod").val());
							atrib3.append('nom',$("#s_nom").val());
							atrib3.append('tip',$("#s_tip").val());
 							var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
 							var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
 							$(this).set('almacen/update_almacen',atrib,controls1,'almacen/view_almacen',atrib3,controls3);
						}
				}else{
					alerta('Ingrese un nombre de almacen valido','top','nom');
				}
			}else{
				alerta('Ingrese un código valido','top','cod');
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirm_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_almacen($(this));
 			}
 		});
 		function confirm_almacen(e){
 			if(e.data("a")!=undefined){
		   		modal("ALMACEN: Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({a:e.data("a")});
		   		btn_modal('drop_almacen',atr,'',"",'5');
		   		var atrib=new FormData();atrib.append('a',e.data("a"));
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('almacen/confirmar',atrib,controls);
 			}
 		}
   	}
   	$.fn.drop_almacen=function(){
   		if($(this).data("a")){
	   		var u=$("#e_user").val();
	   		var p=$("#e_password").val();
	   		if(u!=""){
	   			if(p!=""){
					var atrib=new FormData();
					atrib.append('u',u);
					atrib.append('p',p);
					atrib.append('a',$(this).data("a"));
					var atrib3=new FormData();
					atrib3.append('cod',$("#s_cod").val());
					atrib3.append('nom',$("#s_nom").val());
					atrib3.append('tip',$("#s_tip").val());
 					var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
 					var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
 					$(this).set('almacen/drop_almacen',atrib,controls1,'almacen/view_almacen',atrib3,controls2);
					//set('almacen/drop_almacen',atrib,'NULL',true,'almacen/view_almacen',{},'contenido',true,'5');
	   			}else{
	   				alerta("Ingrese un contenido válido","top","e_password");
	   			}
	   		}else{
	   			alerta("Ingrese un contenido válido","top","e_user");
	   		}
   		}
	}
   	/*--- End Eliminar ---*/
	/*--- Nuevo ---*/
    $.fn.new_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_almacen($(this));
 			}
 		});
 		function new_almacen(e){
		 	modal("ALMACEN: Nuevo","md","1");
		 	btn_modal('save_almacen',"this",'',"","1");
			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			$(this).get_1n('almacen/new_almacen',{},controls);
 		}
    }
	$.fn.save_almacen=function(){
	 	var fot=$("#archivo").prop('files');
	 	var cod=$("#cod").val();
	 	var nom=$("#nom").val();
	 	var tip=$("#tip").val();
		var des=$("#des").val();
		if(strSpace(cod,2,10)){
			if(strSpace(nom,2,100)){
				if(tip=="0" || tip=="1"){
					var control=true;
					if(des!=""){ if(!textarea(des,0,400)){ control=false; alerta('Ingrese un contenido válido','top','des');}}
					if(control){
						var atrib=new FormData();
						atrib.append("cod",cod);
						atrib.append("nom",nom);
						atrib.append("tip",tip);
						atrib.append("des",des);
						atrib.append("archivo",fot[0]);
						var atrib3=new FormData();
						atrib3.append('cod',$("#s_cod").val());
						atrib3.append('nom',$("#s_nom").val());
						atrib3.append('tip',$("#s_tip").val());
						var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
 						var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
 						$(this).set('almacen/save_almacen',atrib,controls1,'almacen/view_almacen',atrib3,controls2);
					}
				}else{
					alerta('Seleccione un tipo de Almacen','top','tip');
				}
			}else{
				alerta('Ingrese un nombre de almacen valido','top','nom');
			}
		}else{
			alerta('Ingrese un código valido','top','cod');
		}
	}
	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_almacen($(this));
 			}
 		});
 		function print_almacen(e){
 			if(e.data("tbl")!=undefined){
 				modal("ALMACEN: Configuracion de impresion","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
					var control=false;
		 			if($(tr).attr("style")==undefined){
						control=true;
					}else{
						if($(tr).attr("style").indexOf("display: none")===-1){
							control=true;
						}
					}
					if(control){
						if($(tr).data("a")!=undefined){
							visibles[visibles.length]=$(tr).data("a");
						}
					}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('cod',$("#s_cod").val());
				atrib.append('nom',$("#s_nom").val());
				atrib.append('tip',$("#s_tip").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('almacen/print_almacen',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE ALMACENES -------*/
/*------- MANEJO DE MOVIMIENTO DE MATERIALES -------*/
   	$.fn.search_movimiento_material=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_movimiento($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_movimiento($(this));
 			}
 		});
 		function search_movimiento(e){
 			e.reset_input();
 		}
	}
   	/*--- Buscador ---*/
	$.fn.view_movimiento_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_movimiento($(this));
 				}
 			}
 		});
	    $(this).submit(function(event){
	    	view_movimiento($(this));
	    	event.preventDefault();
	    });
 		function view_movimiento(e){
			var atrib3=new FormData();
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('alm',$("#s_alm").val());
			atrib3.append('usu',$("#s_usu").val());
			atrib3.append('sol',$("#s_sol").val());
			atrib3.append('fech_a',$("#fech_a").val());
			atrib3.append('fech_b',$("#fech_b").val());
			atrib3.append('tip',$("#s_tip").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('almacen/view_movimiento_material',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_movimiento_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_movimiento($(this));
 			}
 		});
 		function print_movimiento(e){
 			if(e.data("tbl")!=undefined){
 				modal("MOVIMENTO ALMACEN: Configuracion de impresion","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("amv")!=undefined){
		 				visibles[visibles.length]=$(tr).data("amv");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
				atrib.append('tbl',e.data("tbl"));
				atrib.append('nom',$("#s_nom").val());
				atrib.append('alm',$("#s_alm").val());
				atrib.append('usu',$("#s_usu").val());
				atrib.append('sol',$("#s_sol").val());
				atrib.append('fech_a',$("#fech_a").val());
				atrib.append('fech_b',$("#fech_b").val());
				atrib.append('tip',$("#s_tip").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('almacen/print_movimiento_material',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirmar_movimiento_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirmar_movimiento($(this));
 			}
 		});
 		function confirmar_movimiento(){
 			modal("Eliminar","xs","5");
			btn_modal('drop_movimiento_material',"this",'',"",'5');
			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			$(this).get_1n("almacen/confirmar_movimiento_material",{},controls);
 		}
   	}
   	$.fn.drop_movimiento_material=function(){
   		var atrib=new FormData();
   		var u=$("#e_user").val();
   		var p=$("#e_password").val();
		if(u!=""){
	   		if(p!=""){
		   		atrib.append('u',u);
				atrib.append('p',p);
				var atrib3=new FormData();
				atrib3.append('nom',$("#s_nom").val());
				atrib3.append('alm',$("#s_alm").val());
				atrib3.append('usu',$("#s_usu").val());
				atrib3.append('sol',$("#s_sol").val());
				atrib3.append('fech_a',$("#fech_a").val());
				atrib3.append('fech_b',$("#fech_b").val());
				atrib3.append('tip',$("#s_tip").val());
				var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
				$(this).set('almacen/drop_movimiento_material',atrib,controls1,'almacen/view_movimiento_material',atrib3,controls3);
	   		}else{
	   			alerta("Ingrese un contenido válido","top","e_password");
	   		}
	   	}else{
	   		alerta("Ingrese un contenido válido","top","e_user");
	   	}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MOVIMIENTO DE MATERIALES -------*/
})(jQuery);
function inicio(){
 	$("a#ver").click(function(){$(this).get_2n('almacen/search_almacen',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'almacen/view_almacen',{},JSON.stringify({id:"contenido",refresh:true,type:"html"})); activar('ver','Almacenes','almacen?p=1');});
 	$("div#ver").click(function(){$(this).get_2n('almacen/search_almacen',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'almacen/view_almacen',{},JSON.stringify({id:"contenido",refresh:true,type:"html"})); activar('ver','Almacenes','almacen?p=1');});
 	$("a#hist").click(function(){$(this).get_2n('almacen/search_movimiento_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'almacen/view_movimiento_material',{},JSON.stringify({id:"contenido",refresh:true,type:"html"})); activar('hist','Historial de movimiento de materiales','almacen?p=2');});
 	$("div#hist").click(function(){$(this).get_2n('almacen/search_movimiento_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'almacen/view_movimiento_material',{},JSON.stringify({id:"contenido",refresh:true,type:"html"})); activar('hist','Historial de movimiento de materiales','almacen?p=2');});
}
function update_almacen(e){e.update_almacen();}
function drop_almacen(e){e.drop_almacen();}
function save_almacen(e){e.save_almacen();}
function drop_movimiento_material(e){e.drop_movimiento_material();}