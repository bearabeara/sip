 var x=$(document);
 x.ready(inicio);
  (function($){
    $.fn.reset_input=function(){
    	var id=$(this).attr("id");
		if(id!="s_nit"){ $("#s_nit").val(""); }
		if(id!="s_razon"){ $("#s_razon").val(""); }
		if(id!="s_encargado"){ $("#s_encargado").val(""); }
		$(this).blur_all();
    }
    $.fn.blur_all=function(id){
      	OnBlur("s_nit");
      	OnBlur("s_razon");
      	OnBlur("s_encargado");
    }
/*------- MANEJO DE CLIENTE -------*/
   	/*--- Buscador ---*/
   	$.fn.search_cliente=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_cliente($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_cliente($(this));
 			}
 		});
 		function search_cliente(e){
 			e.reset_input();
 		}
	}
	$.fn.view_clientes=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_clientes($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_clientes($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_clientes($(this));
	    	event.preventDefault();
	    });
 		function view_clientes(e){
			var atrib3=new FormData();
	   		atrib3.append('nit',$("#s_nit").val());
	   		atrib3.append('nom',$("#s_razon").val());
	   		atrib3.append('resp',$("#s_encargado").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			e.get_1n('cliente_proveedor/view_cliente',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
   	/*--- Reportes ---*/
   	$.fn.detail_sucursal=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				detail_sucursal($(this));
			}
		});
		function detail_sucursal(e){
	   		modal('SUCURSAL: Detalle','md','1');
			btn_modal('',"",'',"",'1','md');
			var atrib=new FormData();
			atrib.append('sc',e.data("sc"));
		 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			e.get_1n('cliente_proveedor/detail_sucursal',atrib,controls);
		}
   	}
   	$.fn.reportes_cliente=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				reportes_cliente($(this));
			}
		});
		function reportes_cliente(e){
			if(e.data("cl")!=undefined){
		   		modal('CLIENTE: Detalle','md','1');
				btn_modal('',"",'',"",'1','md');
				var atrib=new FormData();
				atrib.append('idcl',e.data("cl"))
			 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				e.get_1n('cliente_proveedor/detalle_cliente',atrib,controls);
			}
		}
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	$.fn.config_cliente=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				config_cliente($(this));
			}
		});
		function config_cliente(e){
			if(e.data("cl")!=undefined){
		   		modal('','lg','11');
		   		var atr="this|"+JSON.stringify({cl:e.data("cl")});
				btn_modal('update_cliente',atr,'',"",'11');
				var atrib=new FormData();
				atrib.append('idcl',e.data("cl"));
			 	var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('cliente_proveedor/form_update_cliente',atrib,controls);
			}
		}	
   	}
	$.fn.update_cliente=function(){
			if($(this).data("cl")!=undefined){
		   		var fot=$("#new_file1").prop('files');
				var nit=$("#new_nit1").val();
				var raz=$("#new_raz1").val();
				var res=$("#new_res1").val();
				var tel=$("#new_tel1").val();
				var web=$("#new_web1").val();
				var obs=$("#new_obs1").val();
				if(entero(nit,6,25)){
					console.log("Entro");
					if(strSpace(raz,2,100)){
						var guardar=true;
						if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res1');}}
						if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel1');}}
						if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','new_web1');}}
						if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','new_obs1');}
						if(guardar){
							
							var atrib=new FormData();
							atrib.append('idcl',$(this).data("cl"));
							atrib.append('nit',nit);
							atrib.append('raz',raz);
							atrib.append('res',res);
							atrib.append('tel',tel);
							atrib.append('web',web);
							atrib.append('obs',obs);
							atrib.append('archivo',fot[0]);
							var atrib3=new FormData();
							atrib3.append('nit',$("#s_nit").val());
							atrib3.append('nom',$("#s_razon").val());
							atrib3.append('resp',$("#s_encargado").val());
							var controls1=JSON.stringify({type:"set",preload:true,closed:"11"});
	 						var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 						$(this).set('cliente_proveedor/update_cliente',atrib,controls1,'cliente_proveedor/view_cliente',atrib3,controls2);
						}
					}else{
						alerta('Ingrese nombre o razon social válido','top','new_raz1');
					}
				}else{
					alerta('Ingrese un NIT o CI válido, hasta 25 digitos numéricos.','top','new_nit1');
				}
			}
	}
	$.fn.view_sucursal=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				view_sucursal($(this));
			}
		});
		function view_sucursal(e){
			if(e.data("cl")!=undefined){
				btn_modal('','','',"",'11');
				var atrib=new FormData();
				atrib.append('cl',e.data("cl"));
			 	var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('cliente_proveedor/view_sucursal',atrib,controls);
			}
		}
	}
	$.fn.new_sucursal=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				new_sucursal($(this));
			}
		});
		function new_sucursal(e){
		   	modal('SUCURSAL: Nueva','md','2');
			btn_modal('save_sucursal',"this",'',"",'2');
			var atrib=new FormData();
			atrib.append('cl',$("#client").data("cl"));
			var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
			e.get_1n('cliente_proveedor/new_sucursal',atrib,controls);
		}
	}
	$.fn.save_sucursal=function(){
	   	var fot=$("#new_file").prop('files');
		var nit=$("#new_nit").val();
		var raz=$("#new_raz").val();
		var res=$("#new_res").val();
		var tel=$("#new_tel").val();
		var ema=$("#new_ema").val();
		var dir=$("#new_dir").val();
		if(strSpace(raz,2,100)){
			var guardar=true;
			if(nit!=""){ if(!entero(nit,0,25)){ guardar=false; alerta('Ingrese un NIT o CI válido, hasta 25 digitos numéricos.','top','new_nit');}}
			if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
			if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
			if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','new_ema');}}
			if(dir!=""){ if(!textarea(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','new_dir');}}
			if(guardar){
				var atrib=new FormData();
				atrib.append('cl',$("#client").data("cl"));
				atrib.append('nit',nit);
				atrib.append('raz',raz);
				atrib.append('res',res);
				atrib.append('tel',tel);
				atrib.append('ema',ema);
				atrib.append('dir',dir);
				atrib.append('archivo',fot[0]);
				var atrib3=new FormData();
				atrib3.append('nit',$("#s_nit").val());
				atrib3.append('nom',$("#s_razon").val());
				atrib3.append('resp',$("#s_encargado").val());
				var controls=JSON.stringify({type:"set",preload:true,closed:"2"});
	 			var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
 				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 			$(this).set_2n("cliente_proveedor/save_sucursal",atrib,controls,"cliente_proveedor/view_sucursal",atrib,controls2,"cliente_proveedor/view_cliente",atrib3,controls3);
			}
		}else{
			alerta('Ingrese nombre o razon social válido','top','new_raz');
		}
	}
	$.fn.change_sucursal=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				change_sucursal($(this));
			}
		});
		function change_sucursal(e){
			if(e.data("sc")!=undefined){
			   	modal('SUCURSAL: Modificar','md','2');
			   	var atr="this|"+JSON.stringify({sc:e.data("sc")});
				btn_modal('update_sucursal',atr,'',"",'2');
				var atrib=new FormData();
				atrib.append('sc',e.data("sc"));
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				e.get_1n('cliente_proveedor/change_sucursal',atrib,controls);
			}
		}
	}
	$.fn.update_sucursal=function(){
	   		var fot=$("#new_file").prop('files');
			var nit=$("#new_nit").val();
			var raz=$("#new_raz").val();
			var res=$("#new_res").val();
			var tel=$("#new_tel").val();
			var ema=$("#new_ema").val();
			var dir=$("#new_dir").val();
			if(strSpace(raz,2,100)){
				var guardar=true;
				if(nit!=""){ if(!entero(nit,0,25)){ guardar=false; alerta('Ingrese un NIT o CI válido, hasta 25 digitos numéricos.','top','new_nit');}}
				if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
				if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
				if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','new_ema');}}
				if(dir!=""){ if(!direccion(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','new_dir');}}
				if(guardar){
					var atrib=new FormData();
					atrib.append('sc',$(this).data("sc"));
					atrib.append('cl',$("#client").data("cl"));
					atrib.append('nit',nit);
					atrib.append('raz',raz);
					atrib.append('res',res);
					atrib.append('tel',tel);
					atrib.append('ema',ema);
					atrib.append('dir',dir);
					atrib.append('archivo',fot[0]);
					var atrib3=new FormData();
					atrib3.append('nit',$("#s_nit").val());
					atrib3.append('nom',$("#s_razon").val());
					atrib3.append('resp',$("#s_encargado").val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"2"});
		 			var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
	 				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
		 			$(this).set_2n("cliente_proveedor/update_sucursal",atrib,controls,"cliente_proveedor/view_sucursal",atrib,controls2,"cliente_proveedor/view_cliente",atrib3,controls3);
				}
			}else{
				alerta('Ingrese nombre o razon social válido','top','new_raz');
			}
			return false;
		}
	   	$.fn.confirmar_sucursal=function(){
			$(this).click(function(){
				if($(this).prop("tagName")!="FORM"){
					confirmar_sucursal($(this));
				}
			});
			function confirmar_sucursal(e){
				if(e.data("sc")!=undefined){
					modal("SUCURSAL: Eliminar","xs","5");
					var atr="this|"+JSON.stringify({sc:e.data("sc")});
			   		btn_modal('drop_sucursal',atr,'',"",'5');
			   		var atrib=new FormData();
			   		atrib.append('sc',e.data("sc"));
			   		var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			   		e.get_1n('cliente_proveedor/confirmar_sucursal',atrib,controls);
				}
			}
	   	}
	   	$.fn.drop_sucursal=function(){
	   		if($(this).data("sc")!=undefined){
				var atrib=new FormData();
				atrib.append('sc',$(this).data("sc"));
				atrib.append('cl',$("#client").data("cl"));
				var atrib3=new FormData();
				atrib3.append('nit',$("#s_nit").val());
				atrib3.append('nom',$("#s_razon").val());
				atrib3.append('resp',$("#s_encargado").val());
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 	var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
		 		var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			 	$(this).set_2n("cliente_proveedor/drop_sucursal",atrib,controls,"cliente_proveedor/view_sucursal",atrib,controls2,"cliente_proveedor/view_cliente",atrib3,controls3);
	   		}
		}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirmar_cliente=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirmar_cliente($(this));
			}
		});
		function confirmar_cliente(e){
			if(e.data("cl")!=undefined){
				modal("CLIENTE: Eliminar","xs","5");
				var atr="this|"+JSON.stringify({cl:e.data("cl")});
		   		btn_modal('drop_cliente',atr,'',"",'5');
		   		var atrib=new FormData();
		   		atrib.append('id',e.data("cl"));
			   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			   	e.get_1n('cliente_proveedor/confirmar_cliente',atrib,controls);
			}
		}
   	}
   	$.fn.drop_cliente=function(){
   		if ($(this).data("cl")!=undefined){
			var atrib=new FormData();
			atrib.append('u',$("#e_user").val());
			atrib.append('p',$("#e_password").val());
			atrib.append('idcl',$(this).data("cl"));
			var atrib3=new FormData();
			atrib3.append('nit',$("#s_nit").val());
			atrib3.append('nom',$("#s_razon").val());
			atrib3.append('resp',$("#s_encargado").val());
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
	 		var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).set('cliente_proveedor/drop_cliente',atrib,controls1,'cliente_proveedor/view_cliente',atrib3,controls2);
   		}
	}
   	/*--- End Eliminar ---*/
  	/*--- Nuevo ---*/
    $.fn.new_cliente=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_cliente($(this));
 			}
 		});
 		function new_cliente(e){
	   		modal('CLIENTE: Nuevo','lg','1');
			btn_modal('save_cliente',"this",'',"",'1');
			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			$(this).get_1n('cliente_proveedor/new_cliente',{},controls);
 		}
    };
   	$.fn.save_cliente=function(){
   		var fot=$("#new_file").prop('files');
		var nit=$("#new_nit").val();
		var raz=$("#new_raz").val();
		var res=$("#new_res").val();
		var tel=$("#new_tel").val();
		var web=$("#new_web").val();
		var obs=$("#new_obs").val();
		if(entero(nit,6,25)){
			if(strSpace(raz,2,100)){
				var guardar=true;
				if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
				if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
				if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','new_web');}}
				if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','new_obs');}
				if(guardar){
					var atrib=new FormData();
					atrib.append('nit',nit);
					atrib.append('raz',raz);
					atrib.append('res',res);
					atrib.append('tel',tel);
					atrib.append('web',web);
					atrib.append('obs',obs);
					atrib.append('archivo',fot[0]);
					var atrib3=new FormData();
					atrib3.append('nit',$("#s_nit").val());
					atrib3.append('nom',$("#s_razon").val());
					atrib3.append('resp',$("#s_encargado").val());
					var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
			 		var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			 		$(this).set('cliente_proveedor/save_cliente',atrib,controls1,'cliente_proveedor/view_cliente',atrib3,controls2);
				}
			}else{
				alerta('Ingrese nombre o razon social válido','top','new_raz');
			}
		}else{
			alerta('Ingrese un NIT o CI válido, hasta 25 digitos numéricos.','top','new_nit');
		}
	}
    /*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_clientes=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_clientes($(this));
 			}
 		});
 		function print_clientes(e){
 			if(e.data("tbl")!=undefined){
 				modal("CLIENTES: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("cl")!=undefined){
		 				visibles[visibles.length]=$(tr).data("cl");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('nit',$("#s_nit").val());
				atrib.append('nom',$("#s_razon").val());
				atrib.append('res',$("#s_encargado").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('cliente_proveedor/print_clientes',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE CLIENTE -------*/
/*------- MANEJO DE PROVEEDOR -------*/
   	/*--- Buscador ---*/
   	$.fn.search_proveedor=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_proveedor($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_proveedor($(this));
 			}
 		});
 		function search_proveedor(e){
 			e.reset_input();
 		}
	}
	$.fn.view_proveedores=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_proveedores($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_proveedores($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_proveedores($(this));
	    	event.preventDefault();
	    });
 		function view_proveedores(e){
			var atrib3=new FormData();
	   		atrib3.append('nit',$("#s_nit").val());
	   		atrib3.append('nom',$("#s_razon").val());
	   		atrib3.append('resp',$("#s_encargado").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			e.get_1n('cliente_proveedor/view_proveedor',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
   	/*--- Reportes ---*/
   	$.fn.reportes_proveedor=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				reportes_proveedor($(this));
			}
		});
		function reportes_proveedor(e){
			if(e.data("pr")!=undefined){
		   		modal('CLIENTE: Detalle','md','1');
				btn_modal('',"",'',"",'1','md');
				var atrib=new FormData();
				atrib.append('idpro',e.data("pr"))
			 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				e.get_1n('cliente_proveedor/detalle_proveedor',atrib,controls);
			}
		}
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	$.fn.config_proveedor=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				config_proveedor($(this));
			}
		});
		function config_proveedor(e){
			if(e.data("pr")!=undefined){
		   		modal('CONFIGURACION: Proveedor','md','1');
		   		var atr="this|"+JSON.stringify({pr:e.data("pr")});
				btn_modal('update_proveedor',atr,'',"",'1');
				var atrib=new FormData();
				atrib.append('idpro',e.data("pr"));
			 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				e.get_1n('cliente_proveedor/form_update_proveedor',atrib,controls);
			}
		}	
   	}
	$.fn.update_proveedor=function(){
			if($(this).data("pr")!=undefined){
		   		var fot=$("#new_file").prop('files');
				var nit=$("#new_nit").val();
				var raz=$("#new_raz").val();
				var res=$("#new_res").val();
				var tel=$("#new_tel").val();
				var ema=$("#new_ema").val();
				var dir=$("#new_dir").val();
				var web=$("#new_web").val();
				var obs=$("#new_obs").val();
				if(entero(nit,6,25)){
					if(strSpace(raz,2,100)){
								var guardar=true;
								if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
								if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
								if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','new_ema');}}
								if(dir!=""){ if(!textarea(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','new_dir');}}
								if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','new_web');}}
								if(!textarea(obs,0,500)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','new_obs');}
								if(guardar){
									var atrib=new FormData();
									atrib.append('idpr',$(this).data("pr"));
									atrib.append('nit',nit);
									atrib.append('raz',raz);
									atrib.append('res',res);
									atrib.append('tel',tel);
									atrib.append('ema',ema);
									atrib.append('dir',dir);
									atrib.append('web',web);
									atrib.append('obs',obs);
									atrib.append('archivo',fot[0]);
									var atrib3=new FormData();
									atrib3.append('nit',$("#s_nit").val());
									atrib3.append('nom',$("#s_razon").val());
									atrib3.append('resp',$("#s_encargado").val());
									var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
			 						var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			 						$(this).set('cliente_proveedor/update_proveedor',atrib,controls1,'cliente_proveedor/view_proveedor',atrib3,controls2);
								}
					}else{
						alerta('Ingrese nombre o razon social válido','top','new_raz');
					}
				}else{
					alerta('Ingrese un NIT o CI válido, hasta 25 digitos numéricos.','top','new_nit');
				}
			}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirm_proveedor=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirm_proveedor($(this));
			}
		});
		function confirm_proveedor(e){
			if(e.data("pr")!=undefined){
				modal("PROVEEDOR: Eliminar","xs","5");
				var atr="this|"+JSON.stringify({pr:e.data("pr")});
		   		btn_modal('drop_proveedor',atr,'',"",'5');
		   		var atrib=new FormData();
		   		atrib.append('idpr',e.data("pr"));
			   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			   	e.get_1n('cliente_proveedor/confirmar_proveedor',atrib,controls);
			}
		}
   	}
   	$.fn.drop_proveedor=function(){
   		if ($(this).data("pr")!=undefined){
			var atrib=new FormData();
			atrib.append('u',$("#e_user").val());
			atrib.append('p',$("#e_password").val());
			atrib.append('idpr',$(this).data("pr"));
			var atrib3=new FormData();
			atrib3.append('nit',$("#s_nit").val());
			atrib3.append('nom',$("#s_razon").val());
			atrib3.append('resp',$("#s_encargado").val());
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
	 		var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).set('cliente_proveedor/drop_proveedor',atrib,controls1,'cliente_proveedor/view_proveedor',atrib3,controls2);
   		}
	}
   	/*--- End Eliminar ---*/
   	/*--- Nuevo ---*/
 	$.fn.new_proveedor=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_proveedor($(this));
 			}
 		});
 		function new_proveedor(e){
   			modal('PROVEEDOR: Nuevo','md','1');
			btn_modal('save_proveedor',"this",'',"",'1','md');
			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			$(this).get_1n('cliente_proveedor/new_proveedor',{},controls);
 		}
    };
   	$.fn.save_proveedor=function(){
   		var fot=$("#new_file").prop('files');
		var nit=$("#new_nit").val();
		var raz=$("#new_raz").val();
		var res=$("#new_res").val();
		var tel=$("#new_tel").val();
		var ema=$("#new_ema").val();
		var dir=$("#new_dir").val();
		var web=$("#new_web").val();
		var obs=$("#new_obs").val();
		if(entero(nit,6,25)){
			if(strSpace(raz,2,100)){
						var guardar=true;
						if(res!=""){ if(!strSpace(res,2,150)){ guardar=false; alerta('Ingrese un nombre de responsable válido','top','new_res');}}
						if(tel!=""){ if(!entero(tel,7,15)){ guardar=false; alerta('Ingrese un numero de telefono válido','top','new_tel');}}
						if(ema!=""){ if(!email(ema)){ guardar=false; alerta('Ingrese un correo electronico válido','top','new_ema');}}
						if(dir!=""){ if(!textarea(dir,5,200)){ guardar=false; alerta('Ingrese una dirección de domicilio válida','top','new_dir');}}
						if(web!=""){ if(!webs(web)){ guardar=false; alerta('Ingrese una dirección web válida','top','new_web');}}
						if(!textarea(obs,0,900)){ guardar=false; alerta('Ingrese un contenido de observaciones válida','top','new_obs');}
						if(guardar){
							var ele=new FormData();
							ele.append('nit',nit);
							ele.append('raz',raz);
							ele.append('res',res);
							ele.append('tel',tel);
							ele.append('ema',ema);
							ele.append('dir',dir);
							ele.append('web',web);
							ele.append('obs',obs);
							ele.append('archivo',fot[0]);
							var atrib3=new FormData();atrib3.append('nit',$("#s_nit").val());atrib3.append('nom',$("#s_razon").val());atrib3.append('resp',$("#s_encargado").val());
							set('cliente_proveedor/save_proveedor',ele,'NULL',true,'cliente_proveedor/view_proveedor',atrib3,'contenido',false,'1');
						}
			}else{
				alerta('Ingrese nombre o razon social válido','top','new_raz');
			}
		}else{
			alerta('Ingrese un NIT o CI válido, hasta 25 digitos numéricos.','top','new_nit');
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_proveedores=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_proveedores($(this));
 			}
 		});
 		function print_proveedores(e){
 			if(e.data("tbl")!=undefined){
 				modal("PROVEEDORES: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("pr")!=undefined){
		 				visibles[visibles.length]=$(tr).data("pr");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('nit',$("#s_nit").val());
				atrib.append('nom',$("#s_razon").val());
				atrib.append('res',$("#s_encargado").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('cliente_proveedor/print_proveedores',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE PROVEEDOR -------*/
})(jQuery);
function update_cliente(e){e.update_cliente();}
function save_sucursal(e){e.save_sucursal();}
function update_sucursal(e){e.update_sucursal();}
function drop_sucursal(e){e.drop_sucursal();}
function drop_cliente(e){e.drop_cliente();}
function save_cliente(e){e.save_cliente();}
function update_proveedor(e){e.update_proveedor();}
function drop_proveedor(e){e.drop_proveedor();}
function save_proveedor(e){e.save_proveedor();}
function inicio(){
	$('a#cliente').click(function(){$(this).get_2n('cliente_proveedor/search_cliente',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'cliente_proveedor/view_cliente',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("cliente","Clientes","cliente_proveedor?p=1");});
	$('div#cliente').click(function(){$(this).get_2n('cliente_proveedor/search_cliente',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'cliente_proveedor/view_cliente',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("cliente","Clientes","cliente_proveedor?p=1");});
	$('a#proveedor').click(function(){$(this).get_2n('cliente_proveedor/search_proveedor',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'cliente_proveedor/view_proveedor',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("proveedor","Proveedores","cliente_proveedor?p=2");});
	$('div#proveedor').click(function(){$(this).get_2n('cliente_proveedor/search_proveedor',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'cliente_proveedor/view_proveedor',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("proveedor","Proveedores","cliente_proveedor?p=2");});
	$('a#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'cliente_proveedor/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de cliente proveedor","cliente_proveedor?p=3");});
	$('div#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'cliente_proveedor/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de cliente proveedor","cliente_proveedor?p=3");});
}