 var x=$(document);
 x.ready(inicio);
 (function($){
 	$.fn.reset_input=function(){
 		var id=$(this).attr("id");
 		if(id!="s_cod"){$("#s_cod").val("");}
		if(id!="s_nom"){$("#s_nom").val("");}
		if(id!="s_can"){$("#s_can").val("");}
 	}
	$.fn.blur_all=function(id){
		OnBlur("s_cod");
		OnBlur("s_nom");
		OnBlur("s_can");
	}
/*------- MANEJO DE MATERIALES INDIRECTOS -------*/
 	/*--- Buscar ---*/
   	$.fn.search_material=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_material($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_material($(this));
 			}
 		});
 		function search_material(e){
 			e.reset_input();
 		}
	}
	$.fn.view_materiales=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_materiales($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_materiales($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_materiales($(this));
	    	event.preventDefault();
	    });
 		function view_materiales(e){
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('can',$("#s_can").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('material_indirecto/view_material',atrib3,controls);
 		}
	}
 	/*--- End buscar ---*/
 	/*--- Reportes ---*/
 	$.fn.detalle_material=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				detalle_material($(this));
			}
		});
		function detalle_material(e){
			if(e.data("me")!=undefined){
		 		modal("Reporte de material","md","1");
		 		btn_modal('',"",'',"",'1');
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		var atrib=new FormData();
		 		atrib.append("idme",e.data("me"));
		 		$(this).get_1n('material_indirecto/detalle_material',atrib,controls);
			}
		}
 	}
 	/*--- End reportes ---*/
 	/*--- Configuracion ---*/
 	$.fn.config_material=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				config_material($(this));
			}
		});
		function config_material(e){
			if(e.data("me")!=undefined){
		 		modal("Configurar material","md","1");
		 		var atr="this|"+JSON.stringify({me:e.data("me")});
		 		btn_modal('update_material',atr,'',"",'1');
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		var atrib=new FormData();
		 		atrib.append("idme",e.data("me"));
		 		$(this).get_1n('material_indirecto/config_material',atrib,controls);
			}
		}
 	}
 	$.fn.update_material=function(){
 		if($(this).data("me")!=undefined){
			var fot=$("#fot").prop('files');
	 		var nom=$("#nom").val();
	 		var cod=$("#cod").val();
	 		var med=$("#med").val();
	 		var des=$("#des").val();
	 		if(strSpace(nom,2,100)){
	 			if(strSpace(cod,2,15)){
	 				if(entero(med,0,10)){
	 					if(fot.length<=1){
	 						var control=true;
	 						if(des!=""){if(!textarea(des,0,300)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
	 						if(control){
	 							var atrib= new FormData();
	 							atrib.append("me",$(this).data("me"));
	 							atrib.append("nom",nom);
	 							atrib.append("cod",cod);
	 							atrib.append("med",med);
	 							atrib.append("des",des);
	 							atrib.append("archivo",fot[0]);
								var atrib3=new FormData();
								atrib3.append('cod',$("#s_cod").val());
								atrib3.append('nom',$("#s_nom").val());
								atrib3.append('can',$("#s_can").val());
	 							var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
	 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 							$(this).set('material_indirecto/update_material',atrib,controls1,'material_indirecto/view_material',atrib3,controls2);
	 						}
	 					}else{
	 						alerta("Solo puede subir hasta 1 archivo","top","fot");
	 					}
	 				}else{
	 					alerta("Seleccione una Medida de material...","top","med");
	 				}
	 			}else{
	 				alerta("Ingrese un Código material válido...","top","cod");
	 			}
	 		}else{
	 			alerta("Ingrese un Nombre de material válido...","top","nom");
	 		}
 		}
 	}
 	/*--- End configuracion ---*/
 	/*--- Eliminar ---*/
 	$.fn.confirm_material=function(idme){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirm_material($(this));
			}
		});
		function confirm_material(e){
			if(e.data("me")!=undefined){
		 		modal("Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({me:e.data("me")});
				btn_modal('drop_material',atr,'',"",'5');
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				var atrib=new FormData();
				atrib.append('idme',e.data("me"));
				$(this).get_1n("material_indirecto/confirmar_material",atrib,controls);
			}
		}
 	}
 	$.fn.drop_material=function(){
 		if($(this).data("me")!=undefined){
 			var atrib=new FormData();
 			atrib.append('me',$(this).data("me"));
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('can',$("#s_can").val());
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set('material_indirecto/drop_material',atrib,controls1,'material_indirecto/view_material',atrib3,controls2);
 		}
 	}
 	/*--- End eliminar ---*/
 	/*--- Nuevo ---*/
 	$.fn.new_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_material($(this));
 			}
 		});
 		function new_material(e){
 			modal("Nuevo material indirecto","md","1");
 			btn_modal('save_material',"this",'',"",'1');
 			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
 			e.get_1n('material_indirecto/new_material',{},controls);
 		}
 	}
 	$.fn.save_material=function(){
 		var fot=$("#fot").prop('files');
 		var nom=$("#nom").val();
 		var cod=$("#cod").val();
 		var med=$("#med").val();
 		var des=$("#des").val();
 		if(strSpace(nom,2,100)){
 			if(strSpace(cod,2,15)){
 				if(entero(med,0,10)){
 					if(fot.length<=1){
 						var control=true;
 						if(des!=""){if(!textarea(des,0,300)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
 						if(control){
 							var atrib= new FormData();
 							atrib.append("nom",nom);
 							atrib.append("cod",cod);
 							atrib.append("med",med);
 							atrib.append("des",des);
 							atrib.append("archivo",fot[0]);
							var atrib3=new FormData();
							atrib3.append('cod',$("#s_cod").val());
							atrib3.append('nom',$("#s_nom").val());
							atrib3.append('can',$("#s_can").val());
 							var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
 							$(this).set('material_indirecto/save_material',atrib,controls1,'material_indirecto/view_material',atrib3,controls2);
 						}
 					}else{
 						alerta("Solo puede subir hasta 1 archivo","top","fot");
 					}
 				}else{
 					alerta("Seleccione una Medida de material...","top","med");
 				}
 			}else{
 				alerta("Ingrese un Código material válido...","top","cod");
 			}
 		}else{
 			alerta("Ingrese un Nombre de material válido...","top","nom");
 		}
 	}
 	/*--- end Nuevo ---*/
 	/*--- Imprimir ---*/
   	$.fn.print_materiales=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_materiales($(this));
 			}
 		});
 		function print_materiales(e){
 			if(e.data("tbl")!=undefined){
 				modal("MATERIALES: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("me")!=undefined){
		 				visibles[visibles.length]=$(tr).data("me");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('cod',$("#s_cod").val());
				atrib.append('nom',$("#s_nom").val());
				atrib.append('can',$("#s_can").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('material_indirecto/print_materiales',atrib,controls);
 			}
 		}
   	}
 	/*--- End imprimir ---*/
/*------- END MANEJO DE MATERIALES INDIRECTOS -------*/
/*------- MANEJO DE INGRESOS -------*/
 	/*--- Buscar ---*/
   	$.fn.search_ingreso=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_ingreso($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_ingreso($(this));
 			}
 		});
 		function search_ingreso(e){
 			e.reset_input();
 		}
	}
	$.fn.view_ingresos=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_ingresos($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_ingresos($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_ingresos($(this));
	    	event.preventDefault();
	    });
 		function view_ingresos(e){
			var atrib3=new FormData();
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('can',$("#s_can").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).get_1n('material_indirecto/view_ingreso',atrib3,controls);
 		}
	}
 	/*--- End buscar ---*/
/*------- END MANEJO DE INGRESOS -------*/
/*------- MANEJO DE SALIDAS -------*/
 	/*--- Buscar ---*/
   	$.fn.search_salida=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_salida($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_salida($(this));
 			}
 		});
 		function search_salida(e){
 			e.reset_input();
 		}
	}
	$.fn.view_salidas=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_salidas($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_salidas($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_salidas($(this));
	    	event.preventDefault();
	    });
 		function view_salidas(e){
			var atrib3=new FormData();
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('can',$("#s_can").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).get_1n('material_indirecto/view_salida',atrib3,controls);
 		}
	}
 	/*--- End buscar ---*/
/*------- END MANEJO DE SALIDAS -------*/
/*------- MANEJO DE OTROS MATERIALES -------*/
 	/*--- Buscar ---*/
   	$.fn.search_otro_material=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_otro_material($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_otro_material($(this));
 			}
 		});
 		function search_otro_material(e){
 			e.reset_input();
 		}
	}
	$.fn.view_otro_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_otro_material($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_otro_material($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_otro_material($(this));
	    	event.preventDefault();
	    });
 		function view_otro_material(e){
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).get_1n('material_indirecto/view_otro_material',atrib3,controls);
 		}
	}
 	/*--- End buscar ---*/
 	/*--- Reportes ---*/
 	$.fn.detalle_otro_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				detalle_otro_material($(this));
 			}
 		});
 		function detalle_otro_material(e){
 			if(e.data("mv")!=undefined){
		 		modal("Reporte de material","md","1");
		 		btn_modal('',"",'',"",'1');
		 		var atrib=new FormData();
		 		atrib.append("idmv",e.data("mv"));
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		$(this).get_1n('material_indirecto/detalle_otro_material',atrib,controls);
 			}
 		}
 	}
 	/*--- End reportes ---*/
 	/*--- Configuracion ---*/
 	$.fn.config_otro_material=function(idmv){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				config_otro_material($(this));
 			}
 		});
 		function config_otro_material(e){
 			if(e.data("mv")!=undefined){
		 		modal("Configurar material vario","md","1");
		 		var atr="this|"+JSON.stringify({mv:e.data("mv")});
		 		btn_modal('update_otro_material',atr,'',"",'1');
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		var atrib=new FormData();
		 		atrib.append("idmv",e.data("mv"));
		 		$(this).get_1n('material_indirecto/config_otro_material',atrib,controls);
 			}
 		}
 	}
 	$.fn.update_otro_material=function(){
 		if($(this).data("mv")!=undefined){
			var fot=$("#fot").prop('files');
	 		var nom=$("#nom").val();
	 		var cod=$("#cod").val();
	 		var med=$("#med").val();
	 		var des=$("#des").val();
	 		if(strSpace(nom,2,100)){
	 			if(strSpace(cod,2,15)){
	 				if(entero(med,0,10)){
	 					if(fot.length<=10){
	 						var control=true;
	 						if(des!=""){if(!textarea(des,0,300)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
	 						if(control){
	 							var atrib= new FormData();
	 							atrib.append("mv",$(this).data("mv"));
	 							atrib.append("nom",nom);
	 							atrib.append("cod",cod);
	 							atrib.append("med",med);
	 							atrib.append("des",des);
	 							atrib.append("archivo",fot[0]);
								var atrib3=new FormData();
								atrib3.append('cod',$("#s_cod").val());
								atrib3.append('nom',$("#s_nom").val());
	 							var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
	 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 							$(this).set('material_indirecto/update_otro_material',atrib,controls1,'material_indirecto/view_otro_material',atrib3,controls2);
	 						}
	 					}else{
	 						alerta("Solo puede subir hasta 10 archivos simultaneos","top","fot");
	 					}
	 				}else{
	 					alerta("Seleccione una Medida de material...","top","med");
	 				}
	 			}else{
	 				alerta("Ingrese un Código material válido...","top","cod");
	 			}
	 		}else{
	 			alerta("Ingrese un Nombre de material válido...","top","nom");
	 		}
 		}
 	}
 	/*--- End configuracion ---*/
 	/*--- Eliminar ---*/
 	$.fn.confirm_otro_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_otro_material($(this));
 			}
 		});
 		function confirm_otro_material(e){
 			if(e.data("mv")!=undefined){
		 		modal("Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({mv:e.data("mv")});
				btn_modal('drop_otro_material',atr,'',"",'5');
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				var atrib=new FormData();
				atrib.append('idmv',e.data("mv"));
				$(this).get_1n("material_indirecto/confirmar_otro_material",atrib,controls);
 			}
 		}
 	}
 	$.fn.drop_otro_material=function(){
 		if($(this).data("mv")!=undefined){
 			var atrib=new FormData();
 			atrib.append('mv',$(this).data("mv"));
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set('material_indirecto/drop_otro_material',atrib,controls1,'material_indirecto/view_otro_material',atrib3,controls2);
 		}
 	}
 	/*--- End eliminar ---*/
 	/*--- Nuevo ---*/
 	$.fn.new_otro_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_otro_material($(this));
 			}
 		});
 		function new_otro_material(e){
 			modal("Nuevo otro material","md","1");
 			btn_modal('save_otro_material',"this",'',"",'1');
 			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
 			$(this).get_1n('material_indirecto/new_otro_material',{},controls);
 		}
 	}
 	$.fn.save_otro_material=function(){
 		var fot=$("#fot").prop('files');
 		var nom=$("#nom").val();
 		var cod=$("#cod").val();
 		var med=$("#med").val();
 		var des=$("#des").val();
 		if(strSpace(nom,2,100)){
 			if(strNoSpace(cod,2,15)){
 				if(entero(med,0,10)){
 					if(fot.length<=1){
 						var control=true;
 						if(des!=""){if(!textarea(des,0,300)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
 						if(control){
 							var atrib= new FormData();
 							atrib.append("nom",nom);
 							atrib.append("cod",cod);
 							atrib.append("med",med);
 							atrib.append("des",des);
 							atrib.append("archivo",fot[0]);
							var atrib3=new FormData();
							atrib3.append('cod',$("#s_cod").val());
							atrib3.append('nom',$("#s_nom").val());
 							var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
 							$(this).set('material_indirecto/save_otro_material',atrib,controls1,'material_indirecto/view_otro_material',atrib3,controls2);
 						}
 					}else{
 						alerta("Solo puede subir hasta 1 archivo","top","fot");
 					}
 				}else{
 					alerta("Seleccione una Medida de material...","top","med");
 				}
 			}else{
 				alerta("Ingrese un Código material válido...","top","cod");
 			}
 		}else{
 			alerta("Ingrese un Nombre de material válido...","top","nom");
 		}
 	}
 	/*--- end Nuevo ---*/

 	/*--- Imprimir ---*/
   	$.fn.print_otros_materiales=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_otros_materiales($(this));
 			}
 		});
 		function print_otros_materiales(e){
 			if(e.data("tbl")!=undefined){
 				console.log("Entro");
 				modal("OTROS MATERIALES: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("mv")!=undefined){
		 				visibles[visibles.length]=$(tr).data("mv");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('cod',$("#s_cod").val());
				atrib.append('nom',$("#s_nom").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('material_indirecto/print_otros_materiales',atrib,controls);
 			}
 		}
   	}
 	/*--- End imprimir ---*/
/*------- END MANEJO DE OTROS MATERIALES -------*/
$.fn.save_movimiento=function(){
	    $(this).click(function(){
	    	if($(this).prop("tagName")!="FORM"){
	    		save_movimiento($(this));
	    	}
	    });
	    $(this).submit(function(event){
	    	save_movimiento($(this));
	    	event.preventDefault();
	    });
 		function save_movimiento(e){
 			if(e.data("me")!=undefined && e.data("type")!=undefined && e.data("item")!=undefined){
 				var me=e.data("me");
	 			var sto=$("#c"+me).val();
				var fech=$("#fech"+me).val();
				var can=$("#can"+me).val();
				var obs=$("#obs"+me).val();
				if(fech!=""){
					if(decimal(can,9,7) && can>0){
						if(textarea(obs,0,300)){
							var control=true;
							if(e.data("type")=="s"){
								if((sto*1)<(can*1)){ msj("Stock insuficiente, verifique la cantidad de salida...!"); control=false;}
								var emp=$("#emp"+me).val();
								if(!entero(emp,0,10)){ alerta("Seleccione un empleado","top","emp"+me); control=false;}
							}
							if(control){
								var atrib=new FormData();
								atrib.append('me',me);
								atrib.append('sto',sto);
								atrib.append('fech',fech);
								atrib.append('can',can);
								atrib.append('obs',obs);
								if(e.data("type")=="s"){ atrib.append('emp',emp);}
								atrib.append('type',e.data('type'));
								atrib.append('item',e.data('item'));
								var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
								var controls2=JSON.stringify({id:"tr-"+me,refresh:false,type:"html"});
								$(this).set('material_indirecto/save_movimiento',atrib,controls1,'material_indirecto/tr_material',atrib,controls2);
							}
						}else{
							alerta("Ingrese un contenido válido entre 9999999999.9999999","top","obs"+me);
						}
					}else{
						alerta("Ingrese un cantidad válida mayor a cero...","top","can"+me);
					}
				}else{
					alerta("Ingrese una fecha valida","top","fech"+me);
				}
 			}
 		}
	}
})(jQuery);
function save_material(e){e.save_material();}
function update_material(e){ e.update_material();}
function drop_material(e){e.drop_material();}
function update_otro_material(e){ e.update_otro_material();}
function drop_otro_material(e){e.drop_otro_material();}
function save_otro_material(e){e.save_otro_material();}
function inicio(){
	$('a#material_indirecto').click(function(){$(this).get_2n('material_indirecto/search_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_material',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("material_indirecto","Materiales indirectos","material_indirecto?p=1");});
	$('div#material_indirecto').click(function(){$(this).get_2n('material_indirecto/search_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_material',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("material_indirecto","Materiales indirectos","material_indirecto?p=1");});
	$('a#ingreso').click(function(){$(this).get_2n('material_indirecto/search_ingreso',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_ingreso',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("ingreso","Ingreso de materiales indirectos","material_indirecto?p=2");});
	$('div#ingreso').click(function(){$(this).get_2n('material_indirecto/search_ingreso',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_ingreso',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("ingreso","Ingreso de materiales indirectos","material_indirecto?p=2");});
	$('a#salida').click(function(){$(this).get_2n('material_indirecto/search_salida',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_salida',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("salida","Salida de materiales indirectos","material_indirecto?p=3");});
	$('div#salida').click(function(){$(this).get_2n('material_indirecto/search_salida',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_salida',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("salida","Salida de materiales indirectos","material_indirecto?p=3");});
	$('a#otro_material').click(function(){$(this).get_2n('material_indirecto/search_otro_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_otro_material',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("otro_material","Otros materiales","material_indirecto?p=4");});
	$('div#otro_material').click(function(){$(this).get_2n('material_indirecto/search_otro_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_otro_material',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("otro_material","Otros materiales","material_indirecto?p=4");});
	$('a#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de materiales indirectos","material_indirecto?p=5");});
	$('div#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'material_indirecto/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de materiales indirectos","material_indirecto?p=5");});
}
/*------- MANEJO DE CONFIGURACIONES -------*/
   	/*--- Manejo de unidad ---*/
   	function save_unidad(){
		var nom=$("#nom_u").val();
		var abr=$("#abr_u").val();
		var equ=$("#equ_u").val();
		var des=$("#des_equ").val();
		if(strSpace(nom,2,40)){
			if(strSpace(abr,1,8)){
				if(textarea(des,0,100)){
					var atrib=new FormData();
					atrib.append("nom",nom);
					atrib.append("abr",abr);
					atrib.append("equ",equ);
					atrib.append("des",des);
					set("material_indirecto/save_unidad",atrib,'NULL',true,'material_indirecto/view_config/',{},'contenido',true);					
				}else{
					alerta("Ingrese un contenido válido","top","des_equ");
				}
			}else{
				alerta("Ingrese un abreviatura de unidad válido","top","abr_u");
			}
		}else{
			alerta("Ingrese un nombre de unidad válido","top","nom_u");
		}
		return false;
	}
	function update_unidad(idu){
		var nom=$("#nom_u"+idu).val();
		var abr=$("#abr_u"+idu).val();
		var equ=$("#equ_u"+idu).val();
		var des=$("#des_equ"+idu).val();
		if(strSpace(nom,2,40)){
			if(strSpace(abr,1,8)){
				if(textarea(des,0,100)){
					var atrib=new FormData();
					atrib.append("idu",idu);
					atrib.append("nom",nom);
					atrib.append("abr",abr);
					atrib.append("equ",equ);
					atrib.append("des",des);
					set("material_indirecto/update_unidad",atrib,'NULL',true,'material_indirecto/view_config/',{},'contenido',true);
				}else{
					alerta("Ingrese un contenido válido","top","des_equ");
				}
			}else{
				alerta("Ingrese un abreviatura de unidad válido","top","abr_u"+idu);
			}
		}else{
			alerta("Ingrese un nombre de unidad válido","top","nom_u"+idu);
		}
		return false;
	}
	function alerta_unidad(idu) {
		modal("Eliminar","xs","5");
   		btn_modal('drop_unidad',"'"+idu+"'",'',"",'5');
   		var atrib=new FormData();atrib.append('idu',idu);
   		get('material_indirecto/confirmar_unidad',atrib,'content_5',true);
	}
	function drop_unidad(idu){
		var atrib=new FormData();
		atrib.append("idu",idu);
		set("material_indirecto/drop_unidad",atrib,'NULL',true,'material_indirecto/view_config/',{},'contenido',true,'5');
	}
   	/*--- End Manejo de unidad ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/