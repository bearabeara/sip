var x=$(document);
x.ready(inicio);
(function($){
    $.fn.search_ci=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 			search($(this));
	 		}
	 	});
	 	$(this).keyup(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			search($(this));
	 		}
	 	});
	 	$(this).on("mousewheel",function(e){
	 		if($(this).prop("tagName")!="FORM"){
	 			e.preventDefault();
	 		}
	 	});
 		function search(e){
 			console.log("Entro");
 			if(e.attr("id")!=undefined && e.data("type")!=undefined && e.val()!=undefined){
 				if(e.data("type")=="directivo"){
 					var inputs=JSON.stringify({"0":"#n_fot","1":"#n_ciu","2":"#n_nom1","3":"#n_nom2","4":"#n_pat","5":"#n_mat","6":"#n_tel","7":"#n_ema","8":"#n_obs"});		
 				}
 				if(e.data("type")=="empleado"){
 					var inputs=JSON.stringify({"0":"#n_fot","1":"#n_ciu","2":"#n_nom1","3":"#n_nom2","4":"#n_pat","5":"#n_mat","6":"#n_car","7":"#n_tel","8":"#n_ema","9":"#n_obs"});		
 				}
 				if(e.val()!="" && (e.data("type")=="directivo" || e.data("type")=="empleado")){
			   		var atrib=new FormData();
			   		atrib.append('ci',e.val());
			   		atrib.append("type",e.data("type"));

				 	var controls=JSON.stringify({id:inputs,refresh:false,type:"form_value"});
					$(this).get_1n('capital_humano/search_ci',atrib,controls);
 				}else{
 					$(this).valores_formulario(inputs,"");
 				}
 			}
 		}
   	}
   	$.fn.valores_formulario=function(destino,valores){
   		if($(this).isJSON(valores) && valores!=""){
   			if($(this).isJSON(destino)){
   				var elementos=JSON.parse(destino);
   				var valores=JSON.parse(valores);

				$.each(elementos, function(i,elemento){
					var valor="";
					if(isset(valores[i])){
						valor=valores[i];
					}
					if($(elemento).prop("tagName")=="INPUT" || $(elemento).prop("tagName")=="TEXTAREA"){
						if($(elemento).attr("type")!="file"){
							if(valor!=""){
								$(elemento).val(valor);
							}
						}
					}else{
						console.log($(elemento).prop("tagName"));
						if($(elemento).prop("tagName")=="SELECT"){
							if(valor!=""){
								$(elemento+" option").each(function(j,option){
									if($(option).attr("value")==valor){
										$(option).attr("selected","selected");	
									}else{
										$(option).removeAttr("selected");
									}
								});
							}
						}	
					}
					if(valor=="" || valor==null){$(elemento).removeAttr("disabled");}else{$(elemento).attr("disabled","disabled");}
				});
   			}
   		}else{
   			if($(this).isJSON(destino)){
   				var elementos=JSON.parse(destino);
				$.each(elementos, function(i, elemento){
					if($(elemento).prop("tagName")=="INPUT" || $(elemento).prop("tagName")=="TEXTAREA"){
						if($(elemento).attr("disabled")=="disabled"){
							$(elemento).val("");	
						}
						$(elemento).removeAttr("disabled");
					}else{
						if($(elemento).prop("tagName")=="SELECT"){
							if($(elemento).attr("disabled")=="disabled"){
								$(elemento+" option").each(function(j,option){
									$(option).removeAttr("selected");
								});
							}
							$(elemento).removeAttr("disabled");
						}	
					}
				});
   			}
   		}
   	}
/*------- MANEJO DE CAPITAL HUMANO -------*/
    $.fn.reset_input=function(){
    	var id=$(this).attr("id");
		if(id!="s_cod"){ $("#s_cod").val(""); }
		if(id!="s_nom"){ $("#s_nom").val(""); }
		if(id!="s_ci"){ $("#s_ci").val(""); }
		if(id!="s_con"){ $("#s_con").val(""); }
		if(id!="s_tip"){ $("#s_tip").val(""); }
		$(this).blur_all();
    }
    $.fn.blur_all=function(id){
      	OnBlur("s_cod");
      	OnBlur("s_nom");
      	OnBlur("s_ci");
      	OnBlur("s_con");
      	OnBlur("s_tip");
      	OnBlur("s_est");
    }

   	/*--- Buscador ---*/
   	$.fn.search_capital_humano=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_capital_humano($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_capital_humano($(this));
 			}
 		});
 		function search_capital_humano(e){
 			e.reset_input();
 		}
	}
	$.fn.view_empleados=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_empleados($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_empleados($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_empleados($(this));
	    	event.preventDefault();
	    });
 		function view_empleados(e){
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('ci',$("#s_ci").val());
			atrib3.append('con',$("#s_con").val());
			atrib3.append('tip',$("#s_tip").val());
			atrib3.append('est',$("#s_est").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			e.get_1n('capital_humano/view_empleado',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
   	/*--- Estado ----*/
   	$.fn.confirm_estado=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirm_estado($(this));
			}
		});
		function confirm_estado(e){
	   		if(e.data("e")!=undefined && e.data("estado")!=undefined){
	   			var text="desactivar";
	   			if(e.data("estado")!="1"){text="activar";}
				modal("Confirmar","xs","5");
			   	var atr="this|"+JSON.stringify({e:e.data("e"),estado:e.data("estado")});
			 	btn_modal('cambiar_estado',atr,'',"",'5');
			 	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			 	$("#content_5").html('<div class="list-group"><div class="list-group-item" style="max-width:100%"><h5>¿Desea '+text+' al empleado?</h5></div></div>');
	   		}
		}
	}
   	$.fn.cambiar_estado=function(){
   		if($(this).data("e")!=undefined){
   			var controls=JSON.stringify({type:"set",preload:true});
   			var atrib=new FormData();atrib.append('ide',$(this).data("e"));
   			var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html",closed:"5"});
 			var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
 			$(this).set("capital_humano/cambiar_estado",atrib,controls,'capital_humano/view_empleado',atrib3,controls3);
   		}
	}
   	/*--- End Estado ----*/
   	/*--- Reportes ---*/
   	$.fn.detalle_empleado=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				detalle_empleado($(this));
 			}
 		});
 		function detalle_empleado(e){
 			if(e.data("e")!=undefined){
		   		modal("","md","11");
		   		var atr="this|"+JSON.stringify({area:"div#area",tools:"div#tools-print"});
		 		btn_modal('',"",'printer',atr,'11');
		 		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
		 		var atrib=new FormData();atrib.append('e',e.data("e"));
		 		$(this).get_1n('capital_humano/detalle_empleado',atrib,controls);
 			}
 		}
	}
    $.fn.produccion_empleado=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				produccion_empleado($(this));
 			}
 		});
 		function produccion_empleado(e){
	      	if(e.data("e")!=undefined){
		 		modal("","md","11");
		   		var atr="this|"+JSON.stringify({area:"div#area",tools:"div#tools-print"});
		 		btn_modal('',"",'printer',atr,'11');
		 		var atrib=new FormData();atrib.append('e',e.data("e"));
		 		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
		 		e.get_1n('capital_humano/produccion_empleado',atrib,controls);
	      	}
 		}
    }
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*-- Modificar --*/
   	$.fn.config_empleado=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				config_empleado($(this));
 			}
 		});
 		function config_empleado(e){
 			if(e.data("e")!=undefined){
			 	modal("","xmd","11");
			 	var atr="this|"+JSON.stringify({e:e.data("e")});
			 	btn_modal('update_empleado',atr,'',"",'11');
			 	var atrib=new FormData();
			 	atrib.append('e',e.data("e"));
			 	var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			 	$(this).get_1n('capital_humano/config_empleado',atrib,controls);
 			}
 		}
	}
		/*--- Procesos en el empleado ---*/
		$.fn.new_proceso_empleado=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_proceso_empleado($(this));
	 			}
	 		});
	 		function new_proceso_empleado(e){
	 			if(e.data("container")!=undefined && e.data("type-save")!=undefined){
					var container=$("#"+e.data("container"));
					if(container!=undefined){
						modal("Adicionar proceso","md","3");
						btn_modal('',"",'',"",'3');
						var asignados=[];
						$("#"+e.data("container")+" span.label").each(function(idx,label){
							if($(label).data("pr")!=undefined && $(label).attr("id")!=undefined && $(label).data("type-save")!=undefined){
								if($("span#"+$(label).attr("id")+" span.closed").data("tipo")!=undefined){
								asignados[asignados.length]={pr:$(label).data("pr"),tipo:$("span#"+$(label).attr("id")+" span.closed").data("tipo")};
								}
							}
						});
						var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						var controls2=JSON.stringify({id:"contenido_3",refresh:true,type:"html"});
						var atrib=new FormData();
						atrib.append("container",e.data("container"));
						atrib.append("type-save",e.data("type-save"));
						atrib.append("asignados",JSON.stringify(asignados));
						e.get_2n('produccion/search_proceso',atrib,controls,'produccion/view_proceso',atrib,controls2);
					}
				}
	 		}
		}
		$.fn.view_proceso=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				view_proceso($(this));
	 			}
	 		});
		    $(this).submit(function(event){
		    	view_proceso($(this));
		    	event.preventDefault();
		    });
	 		function view_proceso(e){
			  	if($("#s3_nom").data("type-save")!=undefined){
					var asignados=[];
					$("#"+$("#s3_nom").data("container")+" span.label").each(function(idx,label){
						if($(label).data("pr")!=undefined && $(label).attr("id")!=undefined && $(label).data("type-save")!=undefined){
							if($("span#"+$(label).attr("id")+" span.closed").data("tipo")!=undefined){
								asignados[asignados.length]={pr:$(label).data("pr"),tipo:$("span#"+$(label).attr("id")+" span.closed").data("tipo")};
							}
						}
					});
			  		var controls=JSON.stringify({id:"contenido_3",refresh:true,type:"html"});
				 	var atrib=new FormData();atrib.append("nom",$("#s3_nom").val());atrib.append("type-save",$("#s3_nom").data("type-save"));atrib.append("asignados",JSON.stringify(asignados));
				 	e.get_1n('produccion/view_proceso',atrib,controls);
			  	}
	 		}
		}
	    $.fn.new_proceso=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_proceso($(this));
	 			}
	 		});
	 		function new_proceso(e){
		      	if($("#s3_nom").data("type-save")!=undefined){
		      		modal("Nuevo proceso","sm","4");
				 	btn_modal('save_proceso',"this",'',"",'4');
				 	var atrib=new FormData();atrib.append("type-save",$("#s3_nom").data("type-save"));
				 	var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
				 	e.get_1n('produccion/new_proceso',atrib,controls);
		      	}
	 		}
		}
	    $.fn.save_proceso=function(){
	      	if($("#n3_pr").val()!=undefined && $("#n3_pr").data("type-save")!=undefined){
	      		var nom=$("#n3_pr").val();
	      		if(strSpace(nom,3,100)){
	      			var atrib=new FormData();atrib.append("nom",nom);
	 				var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
					var controls2=JSON.stringify({id:"contenido_3",type:"html"});
					var asignados=[];
					$("#"+$("#s3_nom").data("container")+" span.label").each(function(idx,label){
						if($(label).data("pr")!=undefined && $(label).attr("id")!=undefined && $(label).data("type-save")!=undefined){
							if($("span#"+$(label).attr("id")+" span.closed").data("tipo")!=undefined){
								asignados[asignados.length]={pr:$(label).data("pr"),tipo:$("span#"+$(label).attr("id")+" span.closed").data("tipo")};
							}
						}
					});
					var atrib3=new FormData();atrib3.append("nom",$("#s3_nom").val());
					atrib3.append("type-save",$("#n3_pr").data("type-save"));
					atrib3.append("asignados",JSON.stringify(asignados));
	 				$(this).set("produccion/save_proceso",atrib,controls,"produccion/view_proceso",atrib3,controls2);
	      		}else{
	      			alerta("Ingrese un nombre de proceso válido","top","n3_pr");
	      		}
	      	}
		}
		    $.fn.update_proceso=function(){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			update_proceso($(this));
		      		}
		      	});
		      	$(this).submit(function(event){
		      		update_proceso($(this));
		      		event.preventDefault();
		      	});
		      	function update_proceso(e){
		      		if(e.data("pr")!=undefined && $("input#proc"+e.data("pr")).val()!=undefined && $("#datos-empleado").data("e")!=undefined){
		      			var nom=$("input#proc"+e.data("pr")).val();
		      			if(strSpace(nom,3,100)){
			      			var controls=JSON.stringify({type:"set",preload:true});
			      			var atrib=new FormData();atrib.append("pr",e.data("pr"));atrib.append("nom",nom);
			      			var asignados=[];
							$("#"+$("#s3_nom").data("container")+" span.label").each(function(idx,label){
								if($(label).data("pr")!=undefined && $(label).attr("id")!=undefined && $(label).data("type-save")!=undefined){
									if($("span#"+$(label).attr("id")+" span.closed").data("tipo")!=undefined){
										asignados[asignados.length]={pr:$(label).data("pr"),tipo:$("span#"+$(label).attr("id")+" span.closed").data("tipo")};
									}
								}
							});
		      				var controls2=JSON.stringify({id:"contenido_3",type:"html"});
				 			var atrib2=new FormData();atrib2.append("nom",$("#s3_nom").val());atrib2.append("type-save",$("#s3_nom").data("type-save"));atrib2.append("asignados",JSON.stringify(asignados));
			 				$(this).set("produccion/update_proceso",atrib,controls,'produccion/view_proceso',atrib2,controls2);
		      			}else{
		      				alerta("Ingrese una valor válido","top","#proc"+e.data("pr"));
		      			}
		      		}
		      	}
			}
		    $.fn.confirmar_proceso=function(){
		      	$(this).click(function(){
		      		if($(this).data("pr")!=undefined && $("#s3_nom").data("type-save")!=undefined){
		      			var atrib=new FormData();atrib.append("pr",$(this).data("pr"));
				     	modal("","xs","5");
					   	var atr="this|"+JSON.stringify({pr:$(this).data("pr")});
					   	btn_modal('drop_proceso',atr,'',"",'5');
					   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					   	$(this).get_1n("produccion/confirmar_proceso",atrib,controls);
					}
		      	});
			}
		    $.fn.drop_proceso=function(){
		      	if($(this).data("pr")!=undefined && $("#s3_nom").data("type-save")!=undefined){
					var controls=JSON.stringify({type:"set",preload:true, closed: "5"});
					var atrib=new FormData();atrib.append("pr",$(this).data("pr"));
			      	var asignados=[];
					$("#"+$("#s3_nom").data("container")+" span.label").each(function(idx,label){
						if($(label).data("pr")!=undefined && $(label).attr("id")!=undefined && $(label).data("type-save")!=undefined){
							if($("span#"+$(label).attr("id")+" span.closed").data("tipo")!=undefined){
								asignados[asignados.length]={pr:$(label).data("pr"),tipo:$("span#"+$(label).attr("id")+" span.closed").data("tipo")};
							}
						}
					});
		      		var controls2=JSON.stringify({id:"contenido_3",type:"html"});
				 	var atrib2=new FormData();atrib2.append("nom",$("#s3_nom").val());atrib2.append("type-save",$("#s3_nom").data("type-save"));atrib2.append("asignados",JSON.stringify(asignados));
			 		$(this).set("produccion/drop_proceso",atrib,controls,'produccion/view_proceso',atrib2,controls2);
				}
			}
	 		$.fn.append_proceso_empleado=function(){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			append_proceso_empleado($(this));
		      		}
		      	});
		      	function append_proceso_empleado(e){
		 			if(e.data("pr")!=undefined && $("#s3_nom").data("container")!=undefined && e.data("disableds")!=undefined){
		 				if($("#proc"+e.data("pr"))!=undefined && $("#grado"+e.data("pr"))!=undefined){
		 					var grado=$("#grado"+e.data("pr")).val();
		 					if(grado=="0" || grado=="1"){
			 					var atrib = new FormData();atrib.append('pr',e.data("pr"));atrib.append('grado',grado);atrib.append('container',$("#s3_nom").data("container"));
			 					var controls=JSON.stringify({id:$("#s3_nom").data("container"),refresh:false,type:"append",closed:false,disableds:e.data("disableds")});
			 					if($("#"+$("#s3_nom").data("container")+" span.label").length<=0){
			 						$("#"+$("#s3_nom").data("container")).html("");
			 					}
			 					e.get_1n('capital_humano/append_proceso_empleado',atrib,controls);
		 					}else{
		 						alerta("Seleccionar una opción válida","top","grado"+e.data("pr"));
		 					}
		 				}else{
		 					msj("Error de variables...");
		 				}
		 			}else{
		 				msj("Error de variables...");
		 			}
		      	}
	 		}
	 		/*$.fn.config_proceso_empleado=function(){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			config_proceso_empleado($(this));
		      		}
		      	});
		      	function config_proceso_empleado(e){
				    if(e.data("container")!=undefined){
				    	var container=$("#"+e.data("container"));
				    	if(container!=undefined){
				    		modal("Modificar procesos","sm","3");
						 	btn_modal('',"",'',"",'3');
						 	var asignados=[];
						 	$("#"+e.data("container")+" span.label").each(function(idx,label){
						 		if($(label).data("pr")!=undefined && $(label).attr("id")!=undefined && $(label).data("type-save")!=undefined){
						 			var span=$("span#"+$(label).attr("id")+" span.closed");
						 			if(span.length>0){
						 				if("tipo: "+span.data("tipo")!=undefined){
						 					if($(label).data("type-save")=="new"){
								 				asignados[asignados.length]={pr:$(label).data("pr"),tipo:span.data("tipo"),type_save:$(label).data("type-save")};
								 			}
											if($(label).data("type-save")=="update"){
												if($(label).data("pre")!=undefined && $(label).data("json-db")!=undefined){
													asignados[asignados.length]={pr:$(label).data("pr"),tipo:span.data("tipo"),type_save:$(label).data("type-save"),pre:$(label).data("pre"),json_db:$(label).data("json-db")};
												}
								 			}
						 				}
						 			}
						 		}
						 	});
						 	var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						 	var atrib=new FormData();atrib.append("container",e.data("container"));atrib.append("asignados",JSON.stringify(asignados));
						 	e.get_1n('capital_humano/config_proceso_empleado',atrib,controls);
				    	}
				    }
		      	}
		    }*/
			$.fn.remove_elemento=function(){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			remove_elemento($(this));
		      		}
		      	});
		      	function remove_elemento(e){
		 			if(e.data("padre")!=undefined && e.data("type-padre")!=undefined){
		 				var padre=null;
		 				padre=e.buscar_padre();
		 				if(padre!=null){
		 					if($(padre).attr("id")!=undefined && $(padre).data("pre")!=undefined && $(padre).data("container")!=undefined && $(padre).data("pre")!=undefined){
		 						modal("","xs","5");
			 					var atrib = new FormData();
			 					atrib.append('pre',$(padre).data("pre"));
			 					var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
								var atr="this|"+JSON.stringify({elemento:"span#"+$(padre).attr("id"), padre:"id-padre","type-save":"update",container:$(padre).data("container"),pre:$(padre).data("pre"),hide:"5"});
								btn_modal('drop_this_elemento',atr,"","",'5');
			 					e.get_1n('capital_humano/confirmar_proceso_empleado',atrib,controls);
		 					}
		 				}
		 			}
		      	}
	 		}
			/*--- End procesos en el empleado ---*/
		$.fn.update_empleado=function(){
			if($(this).data("e")){
				var fot=$('#n_fot').prop('files');
				var ci=$('#n_ci').val();
				var ciudad=$('#n_ciu').val();
				var nom=$('#n_nom1').val();
				var nom2=$('#n_nom2').val();
				var pat=$('#n_pat').val();
				var mat=$('#n_mat').val();
				var cod=$('#n_cod').val();
				var te=$('#n_tem').val();
				var car=$('#n_car').val();
				var telf=$('#n_tel').val();
				var ema=$('#n_ema').val();
				var fn=$('#n_fn').val();
				var fi=$('#n_fi').val();
				var tc=$('#n_tc').val();
				var gra=$('#n_gra').val();
				var sal=$('#n_sal').val();
				var dir=$('#n_dir').val();
				var obs=$('#n_obs').val();
				var procesos_eliminados=$("#n_proc #procesos_db").html();
				if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
					if(entero(ciudad,0,10)){
						if(strSpace(nom,2,20)){
							if(strSpace(pat,2,20)){
								if(entero(te,0,1) && te>=0 && te<=1){
									if(entero(tc,0,10)){
											var control=true;
											if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
											if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
											if(cod!=""){ if(!entero(cod,0,10) || cod<0 || cod>9999999999){ control=false; alerta('Ingrese un nombre válido',"top",'n_cod');}}
											if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
											if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
											if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
											if(fn!=""){ if(!fecha(fn)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
											if(fi!=""){ if(!fecha(fi)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fi');}}
											if(gra!=""){ if(!strSpace(gra,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_gra');}}
											if(sal!="" && sal>0){ if(!decimal(sal,4,1) || sal<0 || sal>9999.9){ control=false; alerta('Ingrese salario válido del empleado',"top",'n_sal');}}
											if(dir!=""){ if(!direccion(dir,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_dir');}}
											if(obs!=""){ if(!textarea(obs,0,800)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
											if(control){
												var procesos=[];
												$("#n_proc span.label").each(function(idx,span){
													if($(span).data("type-save")!=undefined && $(span).data("pr")!=undefined && $(span).attr("id")!=undefined){
														if($("#"+$(span).attr("id")+" span.closed").data("tipo")!=undefined){
															if($(span).data("type-save")=="new"){
																procesos[procesos.length]={pr:$(span).data("pr"),tipo:$("#"+$(span).attr("id")+" span.closed").data("tipo"),type_save:$(span).data("type-save")};	
															}
															if($(span).data("type-save")=="update"){
																procesos[procesos.length]={pre:$(span).data("pre"),tipo:$("#"+$(span).attr("id")+" span.closed").data("tipo"),type_save:$(span).data("type-save")};	
															}
														}
													}
												});
												var atribs= new FormData();
												atribs.append("ide",$(this).data("e"));
												atribs.append("ci",ci);
												atribs.append("ciu",ciudad);
												atribs.append("nom1",nom);
												atribs.append("nom2",nom2);
												atribs.append("pat",pat);
												atribs.append("mat",mat);
												atribs.append("cod",cod);
												atribs.append("te",te);
												atribs.append("car",car);
												atribs.append("tc",tc);
												atribs.append("gra",gra);
												atribs.append("sal",sal);
												atribs.append("tel",telf);
												atribs.append("ema",ema);
												atribs.append("nac",fn);
												atribs.append("ing",fi);
												atribs.append("dir",dir);
												atribs.append("obs",obs);
												atribs.append("procesos_eliminados",procesos_eliminados);
												atribs.append("procesos",JSON.stringify(procesos));
												atribs.append("archivo",fot[0]);
												var atrib3=new FormData();
												atrib3.append('cod',$("#s_cod").val());
												atrib3.append('nom',$("#s_nom").val());
												atrib3.append('ci',$("#s_ci").val());
												atrib3.append('con',$("#s_con").val());
												atrib3.append('tip',$("#s_tip").val());
												atrib3.append('est',$("#s_est").val());
												var controls1=JSON.stringify({type:"set",preload:true,closed:"11"});
	 											var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 											$(this).set('capital_humano/update_empleado',atribs,controls1,'capital_humano/view_empleado',atrib3,controls2);
											}
									}else{
										alerta('Seleccione un tipo de contrato',"top",'n_tc');
									}
								}else{
									alerta('Seleccione un tipo de empleado','top','n_tem');
								}
							}else{
								alerta('Ingrese Apellido Paterno',"top",'n_pat');
							}
						}else{
							alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
						}
					}else{
						alerta('Seleccione una ciudad','top','n_ciu');
					}
				}else{
					alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
				}
			}
		}
	/*-- Produccion --*/
		$.fn.producto_empleado=function(){
		   	$(this).click(function(){
		   		if($(this).prop("tagName")!="FORM"){
		   			producto_empleado($(this));
		   		}
		   	});
		   	function producto_empleado(e){
		      	if(e.data("e")!=undefined){
			 		modal("","xmd","11");
			 		btn_modal('update_productos_empleado',"this",'',"",'11');
			 		var atrib=new FormData();atrib.append('e',e.data("e"));
			 		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			 		e.get_1n('capital_humano/producto_empleado',atrib,controls);
		      	}
		   	}
		}
			/*--- manejo de productos ---*/
 			$.fn.view_producto=function(){
 				modal("EMPLEADO: Adicionar producto","md","2");
 				btn_modal('',"",'',"",'2');
 				if($("#datos-empleado").data("e")!=undefined){
	 				var atrib=new FormData();atrib.append("e",$("#datos-empleado").data("e"));
					var pa=[];//productos asignados en el empleado
					$("div.list-group-item div#list_product div.accordion-content").each(function(index,div){
						if($(div).data("p")!=undefined){ pa[pa.length]=$(div).data("p");}
					});
					atrib.append("pa",JSON.stringify(pa));
					get_2n('capital_humano/search_producto',atrib,'content_2',true,'capital_humano/view_producto',atrib,'contenido_2',true);
 				}
			}
 			$.fn.search_producto=function(){
 				if($("#datos-empleado").data("e")!=undefined){
	 				var atrib=new FormData();atrib.append('nom',$("#s2_nom").val());atrib.append("e",$("#datos-empleado").data("e"));
					var pa=[];
					$("div.list-group-item div#list_product div.accordion-content").each(function(index,div){
						if($(div).data("p")!=undefined){ pa[pa.length]=$(div).data("p");}
					});
					atrib.append("pa",JSON.stringify(pa));
					get('capital_humano/view_producto',atrib,'contenido_2',true);
				}
			}
 			$.fn.all_producto=function(){
 				$("#s2_nom").val("");
 				$(this).search_producto();
 			}
 			$.fn.add_producto=function(){
 				if($(this).data("p") && $("#datos-empleado").data("e")!=undefined){
 					var atrib=new FormData();atrib.append("p",$(this).data("p"));atrib.append("e",$("#datos-empleado").data("e"));
					var controls1=JSON.stringify({id:"list_product",refresh:false,type:"append",callback:JSON.stringify({c:"renumber-productos-n1"}),events:JSON.stringify({e1:"drop_this_elemento"})});
 					$(this).get_1n('capital_humano/add_producto',atrib,controls1);
 				}
			}
			/*--- End manejo de productos ---*/
			$.fn.refresh_producto_empleado=function(){
	 			if($(this).data("p")!=undefined && $(this).data("tbl")!=undefined && $("#datos-empleado").data("e")!=undefined){
	 				var atrib=new FormData();
	 				atrib.append("p",$(this).data("p"));
	 				atrib.append("tbl",$(this).data("tbl"));
	 				atrib.append("e",$("#datos-empleado").data("e"));
	 				var controls1=JSON.stringify({id:$(this).data("tbl")+"",refresh:true,type:"html"});
	 				$(this).get_1n('capital_humano/refresh_producto_empleado',atrib,controls1);
	 			}
	 		}
	 		$.fn.drop_producto=function(){
	 			if($(this).data("padre")!=undefined){
	 				var padre=buscar_padre($(this),$(this).data("padre"));
	 				if(padre!=null){
	 					if($("div#"+$(padre).attr("id"))!=undefined){
	 						var span=$("div#"+$(padre).attr("id")+" div.g-table-responsive span.productos_empleado_db");
	 						if(span!=undefined){
	 							$(this).pila_producto_empleado_db("div#"+$(padre).attr("id")+" div.g-table-responsive span.productos_empleado_db","all-0");
	 							span=$("div#"+$(padre).attr("id")+" div.g-table-responsive span.productos_empleado_db");
	 							padre.html(span);
	 						}
	 					}
	 				}
	 			}
	 		}
		    $.fn.update_producto_empleado=function(){
		      	if($(this).data("proe")!=undefined && $(this).data("rangos")!=undefined){
		      		if($("div#rangos_proe"+$(this).data("rangos"))!=undefined){
			      		var calidad=0;
			      		var radio=$("div#rangos_proe"+$(this).data("rangos")+" input[type=radio]");
			      		if(radio.length>0){
				      		var checked=$("div#rangos_proe"+$(this).data("rangos")+" input[type='radio']:checked");
				      		if(checked.length>0){ calidad=checked.data("val");}
				      	}
		      			var atrib=new FormData();atrib.append('proe',$(this).data("proe"));atrib.append('calidad',calidad);
		      			var controls=JSON.stringify({type:"set",preload:true});
						$(this).set("capital_humano/update_producto_empleado",atrib,controls,"",{},null);
		      		}
		      	}
		    }
		    $.fn.drop_this_elemento=function(){
		    	if($(this).data("padre")!=undefined){
			      	if($(this).data("type-padre")!=undefined){
			      		if($(this).data("type-padre")=="tag" && $(this).data("tag-padre")!=undefined){
			      			var eliminar=$(this).tag_padre($(this).data("tag-padre"));	
			      		}else{
			      			var eliminar=buscar_padre($(this),$(this).data("padre"));
			      		}
			      	}else{
			      		if($(this).data("padre")=="id-padre" && $(this).data("elemento")!=undefined){
			      			var eliminar=$($(this).data("elemento"));
			      			console.log(eliminar);
			      		}else{
			      			var eliminar=buscar_padre($(this),$(this).data("padre"));
			      		}
			      	}
			      	if(eliminar!=null && eliminar!=undefined){
			      		var control=false;
			      		if($(this).data("padre")=="row-proceso"){
			      			if($(eliminar).data("save")!=undefined){
			      				if($(eliminar).data("save")=="update"){
			      					if($(eliminar).data("proe")!=undefined && $(eliminar).data("json-db")!=undefined){
			      						$(this).pila_producto_empleado_db("span#"+$(eliminar).data("json-db"),$(eliminar).data("proe"));
			      					}
			      				}
			      			}
				      		var table=$(eliminar).tag_padre("table");
				      		if(table!=null){
				      			var trs=$("table#"+table.attr("id")+" tr.row-proceso");
				      			if(trs.length<=1){
				      				var producto=buscar_padre(table,"row-producto");
				      				if(producto!=null){
				      					var padre=producto.parent();
				      					producto.remove();
				      					padre.renumber_producto_empleado({padre:"table",type:"tag",item:"div.item.item-2"});
				      				}
				      			}else{
				      				var e=$(eliminar).parent();
				      				$(eliminar).remove();
				      			}
				      		}
			      		}
			      		if($(this).data("padre")=="label-process" || $(this).data("padre")=="id-padre"){
			      			if($(eliminar).data("type-save")!=undefined){
			      				if($(eliminar).data("type-save")=="update"){
			      					if($(eliminar).data("pre")!=undefined && $(eliminar).data("container")!=undefined){
			      						$(this).pila_empleado_proceso_db($(eliminar).data("container"),$(eliminar).data("pre"));
			      					}
			      				}
			      				$(eliminar).remove();
			      				if($(this).data("hide")!=undefined){
			      					hide_modal($(this).data("hide"));
			      				}
			      			}
			      		}
			      	}
			    }
			}
		    $.fn.pila_producto_empleado_db=function(dir,idproe){
		      	if($(dir).html()!=undefined){
		      		var users=$(dir).html();
			      	var users=JSON.parse(users);
			      	var result=[];
			      	for(var i = 0;i < users.length; i++){
						if(users[i]!=undefined){
							var status=users[i].status;
							if(idproe!="all-0"){
								if(users[i].idproe==idproe){ status="0";}
							}else{
								if(idproe=="all-0"){ status="0";}
							}
							result[result.length]={idproe:users[i].idproe, status:status};
						}
					}
					$(dir).html(JSON.stringify(result));
		      	}
		    }
		    $.fn.pila_empleado_proceso_db=function(dir,pre){
		      	if($(dir).html()!=undefined){
		      		var result=[];
		      		var procesos=$(dir).html();
			      	procesos=JSON.parse(procesos);
			      	var result=[];
			      	for(var i = 0;i < procesos.length; i++){
						if(procesos[i]!=undefined){
							var status=procesos[i].status;
							if(procesos[i].pre==pre){ status="0";}
							result[result.length]={pre:procesos[i].pre, status:status};
						}
					}
					$(dir).html(JSON.stringify(result));
		      	}
		    }
		    $.fn.renumber_producto_empleado=function(a){
		      	if(a.type=="tag"){ var ele=$(this).tag_padre(a.padre);}
		      	if(a.type=="atr"){ var ele=buscar_padre($(this),a.padre);}
		      	if(ele!=null){
		      		if(ele.attr("id")!=undefined){
		      			var i=1;
		      			$("#"+ele.attr("id")+" "+a.item).each(function(idx,item){
		      				$(item).html(i);i++;
		      			});
		      		}
		      	}
			}
			/*-- Adicionar procesos producto--*/
			$.fn.procesos_empleado=function(){
 				if($(this).data("tbl")!=undefined && $(this).data("type")!=undefined && $(this).data("pgc") && $(this).data("p")!=undefined && $("#datos-empleado").data("e")!=undefined){
					modal("EMPLEADO: Seleccionar proceso","md","2");
					btn_modal('',"",'',"",'2');
	 				var v=[];
	 				var tabla=$("table#"+$(this).data("tbl")).html();
	 				if(tabla!=undefined){
	 					$("table#"+$(this).data("tbl")+" tr.row-proceso").each(function(idx,ele){ v[v.length]=$(ele).data("pre");});
		 				var atrib = new FormData();
		 				atrib.append('pgc',$(this).data("pgc"));
		 				atrib.append('p',$(this).data("p"));
		 				atrib.append('pres',JSON.stringify(v));
		 				atrib.append("tbl",$(this).data("tbl"));
		 				atrib.append("type",$(this).data("type"));
		 				atrib.append("e",$("#datos-empleado").data("e"));
		 				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html",closed:false});
		 				$(this).get_1n('capital_humano/procesos_empleado',atrib,controls);
	 				}
 				}
 			}
 			$.fn.add_proceso_producto=function(){
 				if($(this).data("pre")!=undefined && $("#datos-proceso-empleado").data("tbl")!=undefined && $("#datos-proceso-empleado").data("type")!=undefined && $("#datos-empleado").data("e")!=undefined && $("#datos-proceso-empleado").data("pgc")!=undefined){
					var atrib = new FormData();
					atrib.append('pre',$(this).data("pre"));
					atrib.append('pgc',$("div#datos-proceso-empleado").data("pgc"));
					var v=[];
					$("table#"+$("#datos-proceso-empleado").data("tbl")+" tr.row-proceso").each(function(idx,ele){ v[v.length]=$(ele).data("pre");});
 					v[v.length]=$(this).data("pre");
 					atrib.append('pres',JSON.stringify(v));
 					atrib.append('type',$("#datos-proceso-empleado").data("type"));
 					var controls=JSON.stringify({id:$("#datos-proceso-empleado").data("tbl"),refresh:false,type:"append",child:"tbody",closed:false});
 					$(this).get_1n('capital_humano/btn_add_proceso_producto',atrib,controls);
 				}
 			}
	 		$.fn.confirmar_proceso_empleado=function(){
	 			if($(this).data("pre")!=undefined && $("#datos-proceso-empleado").data("tbl")!=undefined && $("#datos-proceso-empleado").data("type")!=undefined && $("#datos-empleado").data("e")!=undefined && $("#datos-proceso-empleado").data("pgc")!=undefined){
		   			modal("","xs","5");
		   			var atr="this|"+JSON.stringify({pre:$(this).data("pre")});
		   			btn_modal('drop_proceso_empleado',atr,'',"",'5');
		   			var atrib=new FormData();atrib.append("pre",$(this).data("pre"));
		   			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
		   			$(this).get_1n("capital_humano/confirmar_proceso_empleado",atrib,controls);
		      	}
	 		}
		    $.fn.drop_proceso_empleado=function(){
		      	if($(this).data("pre")!=undefined && $("#datos-proceso-empleado").data("tbl")!=undefined && $("#datos-proceso-empleado").data("type")!=undefined && $("#datos-empleado").data("e")!=undefined && $("#datos-proceso-empleado").data("pgc")!=undefined){
		 			var v=[];
			 		$("table#"+$("#datos-proceso-empleado").data("tbl")+" tr.row-proceso").each(function(idx,ele){ v[v.length]=$(ele).data("pre");});
				 	var atrib=new FormData();
				 	atrib.append('pre',$(this).data("pre"));
				 	atrib.append('pgc',$("#datos-proceso-empleado").data("pgc"));
				 	atrib.append('pres',JSON.stringify(v));
				 	atrib.append("tbl",$("#datos-proceso-empleado").data("tbl"));
				 	atrib.append("type",$("#datos-proceso-empleado").data("type"));
				 	atrib.append("e",$("#datos-empleado").data("e"));
				 	var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				 	var controls1=JSON.stringify({id:"content_2",refresh:false,type:"html"});
		 			$(this).set("capital_humano/drop_proceso_empleado",atrib,controls,"capital_humano/procesos_empleado",atrib,controls1);
		      	}
	      	}
		    $.fn.view_producto_proceso=function(){
		      	if($(this).data("type-save")!=undefined && $("div#datos-proceso-empleado").data("pgc")!=undefined){
				 	modal("Adicionar:producto procesos","sm","3");
				 	btn_modal('',"",'',"",'3');
				 	var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
				 	var atrib=new FormData();atrib.append("e",$("#datos-empleado").data("e"));atrib.append("pgc",$("div#datos-proceso-empleado").data("pgc"));atrib.append("type-save",$(this).data("type-save"));
				 	$(this).get_1n('capital_humano/view_producto_proceso',atrib,controls);
		      	}
		    }
		    $.fn.add_proceso_empleado=function(){
		      	if($(this).data("pr")!=undefined && $(this).data("type")!=undefined && $("#datos-proceso-empleado").data("tbl")!=undefined && $("#datos-proceso-empleado").data("type")!=undefined && $("#datos-empleado").data("e")!=undefined && $("#datos-proceso-empleado").data("pgc")!=undefined){
		      		var car=$("select#car"+$(this).data("pr")).val()+"";
		      		if(car=="0" || car=="1"){
		      			var atrib=new FormData();atrib.append("e",$("#datos-empleado").data("e"));atrib.append("pr",$(this).data("pr"));atrib.append("car",car);
		      			var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
		 				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
		 				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
		 				if($(this).data("type")=="produccion"){
		 					var v=[];
			 				$("table#"+$("#datos-proceso-empleado").data("tbl")+" tr.row-proceso").each(function(idx,ele){ v[v.length]=$(ele).data("pre");});
				 			atrib.append('pgc',$("#datos-proceso-empleado").data("pgc"));
				 			atrib.append('pres',JSON.stringify(v));
				 			atrib.append("tbl",$("#datos-proceso-empleado").data("tbl"));
				 			atrib.append("type",$("#datos-proceso-empleado").data("type"));
				 			atrib.append("e",$("#datos-empleado").data("e"));
				 			var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
				 			$(this).set("capital_humano/add_proceso_empleado",atrib,controls,"capital_humano/procesos_empleado",atrib,controls2);
			 			}
		      		}else{
		      			alerta("Seleccione una opción válida","top","car"+$(this).data("pr"));
		      		}
		      	}
		    }
	    	/*-- End adicionar procesos producto--*/
	    	/*-- Adicionar color producto --*/
	 		$.fn.colores_producto=function(){
	 			if($(this).data("tbl")!=undefined && $(this).data("type")!=undefined && $(this).data("p")!=undefined && $("div#datos-empleado").data("e")!=undefined){
					modal("PRODUCTO: Adicionar color","sm","2");btn_modal('',"",'',"",'2');
		 			var v=[];
		 			var tabla=$("table#"+$(this).data("tbl"));
		 			if(tabla!=undefined){
		 				$("table#"+$(this).data("tbl")+" tr.row-producto").each(function(idx,ele){ v[v.length]=$(ele).data("pgc");});
			 			var atrib = new FormData();atrib.append('p',$(this).data("p"));
			 			atrib.append('pgcs',JSON.stringify(v));atrib.append("tbl",$(this).data("tbl"));atrib.append("type",$(this).data("type"));atrib.append("e",$("div#datos-empleado").data("e"));
			 			var controls=JSON.stringify({id:"content_2",refresh:true,type:"html",closed:false});
			 			$(this).get_1n('capital_humano/colores_producto',atrib,controls);
		 			}
	 			}
	 		}
	 		$.fn.add_producto_color=function(){
	 			var v=[];
	 			$("table#"+$("div#datos_producto_add_color").data("tbl")+" tr.row-producto").each(function(idx,ele){ v[v.length]=$(ele).data("pgc");});
	 			v[v.length]=$(this).data("pgc");
	 			var acordion=buscar_padre($("table#"+$("div#datos_producto_add_color").data("tbl")),"accordion-panel");
	 			var controls1=JSON.stringify({id:$(acordion).attr("id"),refresh:false,type:"append",child:"tbody.producto",closed:false,callback:JSON.stringify({c:"renumber-productos-n2"})});
	 			var atrib = new FormData();atrib.append('pgc',$(this).data("pgc"));atrib.append('p',$("div#datos_producto_add_color").data("p"));atrib.append('tbl',$("div#datos_producto_add_color").data("tbl"));
	 			if($("div#datos_producto_add_color").data("type")!=undefined){ atrib.append("type",$("div#datos_producto_add_color").data("type"));}
	 			if($("#datos-empleado").data("e")!=undefined){
	 				atrib.append("e",$("#datos-empleado").data("e"));
	 				atrib.append('pgcs',JSON.stringify(v));
	 				$(this).get_1n('capital_humano/add_producto_color',atrib,controls1);
	 			}
	 		}
	    	/*-- End adicionar color producto --*/
	    $.fn.update_producto_colores_empleado=function(){
 			if($(this).data("p")!=undefined !=undefined && $(this).data("tbl")!=undefined && $(this).data("tbl-refresh")!=undefined && $("#datos-empleado").data("e")!=undefined){
 				if($("div#"+$(this).data("tbl-refresh")+" span.productos_empleado_db").html()!=undefined && $("table#"+$(this).data("tbl")+" tr td div.rangos").length>0){
	 				var procesos=[];
	 				$("table#"+$(this).data("tbl")+" tr td div.rangos").each(function(id,rango){
	 					if($(rango).attr("id")!=undefined && $(rango).data("type-save")!=undefined){
	 						var calidad=0;
				      		var radio=$("div#"+$(rango).attr("id")+" input.rango[type=radio]");
				      		if(radio.length>0){
					      		var checked=$("div#"+$(rango).attr("id")+" input.rango[type='radio']:checked");
					      		if(checked.length>0){ calidad=checked.data("val");}
					      	}
					      	if($(rango).data("type-save")=="update"){
					      		if($(rango).data("proe")!=undefined){
					      			procesos[procesos.length]={type:$(rango).data("type-save"), proe:$(rango).data("proe"), calidad:calidad}
					      		}
					      	}
					      	if($(rango).data("type-save")=="new"){
					      		if($(rango).data("pre")!=undefined && $(rango).data("pgc")!=undefined){
					      			procesos[procesos.length]={type:$(rango).data("type-save"), pgc:$(rango).data("pgc"), pre:$(rango).data("pre"), calidad:calidad}
					      		}
					      	}
	 					}
	 				});
 					var atrib=new FormData();
	 				atrib.append("p",$(this).data("p"));
	 				atrib.append("tbl",$(this).data("tbl-refresh"));
	 				atrib.append("e",$("#datos-empleado").data("e"));
	 				atrib.append("eliminados",$("div#"+$(this).data("tbl-refresh")+" span.productos_empleado_db").html());
	 				atrib.append("procesos",JSON.stringify(procesos));
 					var controls=JSON.stringify({type:"set",preload:true});
 					var controls1=JSON.stringify({id:$(this).data("tbl-refresh")+"",refresh:false,type:"html"});
 					$(this).set("capital_humano/update_producto_colores_empleado",atrib,controls,"capital_humano/refresh_producto_empleado",atrib,controls1);
 				}
 			}
	    }
	    $.fn.update_productos_empleado=function(){
 			if($("#datos-empleado").data("e")!=undefined){
 				if($("div#list_product table tr.row-proceso td div.rangos").length>0){
 					var control=true;
 					var productos=[];
 					$("div#list_product div.accordion-content table tr.row-proceso td div.rangos").each(function(idx,rango){
						if($(rango).attr("id")!=undefined && $(rango).data("type-save")!=undefined){
		      				var calidad=0;
		      				var radio=$("div#"+$(rango).attr("id")+" input[type=radio]");
		      				if(radio.length>0){
			      				var checked=$("div#"+$(rango).attr("id")+" input[type='radio']:checked");
			      				if(checked.length>0){ calidad=checked.data("val");}
			      			}
		      				if($(rango).data("type-save")=="new"){
		      					if($(rango).data("pre")!=undefined && $(rango).data("pgc")!=undefined){
		      						productos[productos.length]={type:$(rango).data("type-save"), pre:$(rango).data("pre"),pgc:$(rango).data("pgc"),calidad:calidad};
		      					}else{
		      						control=false;
		      					}
		      				}
		      				if($(rango).data("type-save")=="update"){
		      					if($(rango).data("proe")!=undefined){
		      						productos[productos.length]={type:$(rango).data("type-save"), proe:$(rango).data("proe"),calidad:calidad};
		      					}else{
		      						control=false;
		      					}
		      				}
		      			}else{
		      				control=false;
		      			}
 					});
 					if(control){
 						//console.log("Entro");
 						var atrib=new FormData();atrib.append('productos',JSON.stringify(productos));
 						var eliminados=[];
 						if($("div#list_product span.productos_empleado_db").length>0){
 							$("div#list_product span.productos_empleado_db").each(function(idx,span){
 								var ele=JSON.parse($(span).html());
 								for (var i = 0; i < ele.length; i++) {
 									if(ele[i].idproe!=undefined && ele[i].status!=undefined){
 										eliminados[eliminados.length]={idproe:ele[i].idproe,status:ele[i].status};
 									}
 								}
 							});
 						}
 						atrib.append("eliminados",JSON.stringify(eliminados));
 						atrib.append("e",$("#datos-empleado").data("e"));
	 					var controls=JSON.stringify({type:"set",preload:true});
	 					var controls1=JSON.stringify({id:"content_11",refresh:true,type:"html"});
	 					$(this).set("capital_humano/update_productos_empleado",atrib,controls,"capital_humano/producto_empleado",atrib,controls1);
 					}else{
 						msj("Errores de variable...!!")
 					}
 				}
	      	}
	  	}
		/*-- End produccion --*/
		/*-- End modificar --*/
	/*--- Eliminar ---*/
   	$.fn.confirm_empleado=function(id){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_empleado($(this));
 			}
 		});
 		function confirm_empleado(e){
 			if(e.data("e")!=undefined){
		   		modal("EMPLEADO: Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({e:e.data("e")});
		   		btn_modal('drop_empleado',atr,'',"",'5');
		   		var atrib=new FormData();atrib.append('e',e.data("e"));
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				e.get_1n('capital_humano/confirmar_empleado',atrib,controls);
 			}
 		}
   	}
   	$.fn.drop_empleado=function(){
   		if($(this).data("e")!=undefined){
			var atrib=new FormData();
			atrib.append('u',$("#e_user").val());
			atrib.append('p',$("#e_password").val());
			atrib.append('e',$(this).data("e"));
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
		 	var controls=JSON.stringify({type:"set",preload:true, closed:"5"});
		 	var controls1=JSON.stringify({id:"contenido",refresh:true,type:"html"});
		 	$(this).set("capital_humano/drop_empleado",atrib,controls,"capital_humano/view_empleado",atrib3,controls1);
   		}
	}
   	/*--- End Eliminar ---*/
   	/*--- Nuevo ---*/
    $.fn.new_empleado=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_empleado($(this));
 			}
 		});
 		function new_empleado(e){
	 		modal("","xmd","11");
	 		btn_modal('save_empleado',"this",'',"",'11');
	 		var atrib=new FormData();
	 		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
	 		$(this).get_1n('capital_humano/new_empleado',atrib,controls);
 		}
 	}
    $.fn.save_empleado=function(){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var cod=$('#n_cod').val();
		var te=$('#n_tem').val();
		var gra=$('#n_gra').val();
		var car=$('#n_car').val();
		var telf=$('#n_tel').val();
		var ema=$('#n_ema').val();
		var fn=$('#n_fn').val();
		var fi=$('#n_fi').val();
		var tc=$('#n_tc').val();
		var sal=$('#n_sal').val();
		var dir=$('#n_dir').val();
		var obs=$('#n_obs').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						if(entero(te,0,1) && te>=0 && te<=1){
							if(entero(tc,0,10)){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(cod!=""){ if(!entero(cod,0,10) || cod<0 || cod>9999999999){ control=false; alerta('Ingrese un nombre válido',"top",'n_cod');}}
									if(gra!=""){ if(!strSpace(gra,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_gra');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
									if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
									if(fn!=""){ if(!fecha(fn)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
									if(fi!=""){ if(!fecha(fi)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fi');}}
									if(sal!="" && sal>0){ if(!decimal(sal,4,1) || sal<0 || sal>9999.9){ control=false; alerta('Ingrese salario válido del empleado',"top",'n_sal');}}
									if(dir!=""){ if(!direccion(dir,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_dir');}}
									if(obs!=""){ if(!textarea(obs,0,800)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
									if(control){
										var atribs= new FormData();
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("cod",cod);
										atribs.append("te",te);
										atribs.append("gra",gra);
										atribs.append("car",car);
										atribs.append("tc",tc);
										atribs.append("sal",sal);
										atribs.append("tel",telf);
										atribs.append("ema",ema);
										atribs.append("nac",fn);
										atribs.append("ing",fi);
										atribs.append("dir",dir);
										atribs.append("obs",obs);
										atribs.append("archivo",fot[0]);
 										var controls=JSON.stringify({type:"set",preload:true,closed:"11"});
										var atrib3=new FormData();
										atrib3.append('cod',$("#s_cod").val());
										atrib3.append('nom',$("#s_nom").val());
										atrib3.append('ci',$("#s_ci").val());
										atrib3.append('con',$("#s_con").val());
										atrib3.append('tip',$("#s_tip").val());
										atrib3.append('est',$("#s_est").val());
										var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
										$(this).set("capital_humano/save_empleado",atribs,controls,'capital_humano/view_empleado',atrib3,controls3);
									}
							}else{
								alerta('Seleccione un tipo de contrato',"top",'n_tc');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tem');
						}
					}else{
						alerta('Ingrese Apellido Paterno',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
	}
   	/*--- End nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_empleados=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_empleados($(this));
 			}
 		});
 		function print_empleados(e){
 			if(e.data("tbl")!=undefined){
 				modal("COMPRAS: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("e")!=undefined){
		 				visibles[visibles.length]=$(tr).data("e");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('cod',$("#s_cod").val());
				atrib.append('nom',$("#s_nom").val());
				atrib.append('ci',$("#s_ci").val());
				atrib.append('con',$("#s_con").val());
				atrib.append('tip',$("#s_tip").val());
				atrib.append('est',$("#s_est").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('capital_humano/print_empleados',atrib,controls);
 			}
 		}
   	}
/*------- END MANEJO DE CAPITAL HUMANO -------*/
/*------- MANEJO DE DIRECTIVOS -------*/
	/*--- Buscador ---*/
	$.fn.view_directivos=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_directivos($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_directivos($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_directivos($(this));
	    	event.preventDefault();
	    });
 		function view_directivos(e){
			var atrib3=new FormData();
			atrib3.append('ci',$("#s_ci").val());
			atrib3.append('nom',$("#s_nom").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('capital_humano/view_directivo',atrib3,controls);
 		}
	}
	/*--- End Buscador ---*/
	/*--- Reportes ---*/
	$.fn.reportes_directivo=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			reportes_directivo($(this));
	 		}
	 	});
	 	function reportes_directivo(e){
	 		if(e.data("di")!=undefined){
		 		modal("Detalle directivo","md","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		atrib.append("di",e.data("di"));
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		$(this).get_1n('capital_humano/detalle_directivo',atrib,controls);
	 		}
	 	}
	}
	/*--- End reportes ---*/
	/*--- Configuración ---*/
	$.fn.config_directivo=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			config_directivo($(this));
	 		}
	 	});
	 	function config_directivo(e){
	 		if(e.data("di")!=undefined){
		 		modal("Configuración directivo","xmd","1");
	 			var atr="this|"+JSON.stringify({di:e.data("di")});
		 		btn_modal('update_directivo',atr,'','','1');
		 		var atrib=new FormData();
		 		atrib.append("di",e.data("di"));
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		$(this).get_1n('capital_humano/config_directivo',atrib,controls);
	 		}
	 	}
	}
	$.fn.update_directivo=function(){
		if($(this).data("di")!=undefined && $("span#datos_cuenta").html()!=undefined){
			var fot=$('#n_fot').prop('files');
			var ci=$('#n_ci').val();
			var ciudad=$('#n_ciu').val();
			var nom=$('#n_nom1').val();
			var nom2=$('#n_nom2').val();
			var pat=$('#n_pat').val();
			var mat=$('#n_mat').val();
			var car=$('#n_car').val();
			var telf=$('#n_tel').val();
			var ema=$('#n_ema').val();
			var fec=$('#n_fec').val();
			var obs=$('#n_obs').val();
			if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
				if(entero(ciudad,0,10)){
					if(strSpace(nom,2,20)){
						if(strSpace(pat,2,20)){
							var control=true;
							if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
							if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
							if(car!=""){ if(!strSpace(car,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
							if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
							if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
							if(fec!=""){ if(!fecha(fec)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
							if(obs!=""){ if(!textarea(obs,0,800)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
							if(control){
								var cuentas=[];
								$("#n_proc span.label").each(function(idx,span){
									if($(span).data("type-save")!=undefined && $(span).data("ba")!=undefined && $(span).data("cuenta")!=undefined && $(span).data("tipo")!=undefined && $(span).attr("id")!=undefined){
										if($(span).data("type-save")=="new"){
											cuentas[cuentas.length]={ba:$(span).data("ba"),cuenta:$(span).data("cuenta"),tipo:$(span).data("tipo"),type_save:$(span).data("type-save")};	
										}
										if($(span).data("type-save")=="update"){
											cuentas[cuentas.length]={bp:$(span).data("bp"),ba:$(span).data("ba"),cuenta:$(span).data("cuenta"),tipo:$(span).data("tipo"),type_save:$(span).data("type-save")};	
										}
									}
								});
								var atrib= new FormData();
								atrib.append("di",$(this).data("di"));
								atrib.append("ci",ci);
								atrib.append("ciu",ciudad);
								atrib.append("nom1",nom);
								atrib.append("nom2",nom2);
								atrib.append("pat",pat);
								atrib.append("mat",mat);
								atrib.append("car",car);
								atrib.append("tel",telf);
								atrib.append("ema",ema);
								atrib.append("nac",fec);
								atrib.append("obs",obs);
								atrib.append("archivo",fot[0]);
								atrib.append("cuentas",JSON.stringify(cuentas));
								atrib.append("eliminados",$("span#datos_cuenta").html());
								var atrib3=new FormData();
								atrib3.append('nom',$("#s_nom").val());
								atrib3.append('ci',$("#s_ci").val());
								var controls=JSON.stringify({type:"set",preload:true,closed:"1"});
							 	var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
								$(this).set("capital_humano/update_directivo",atrib,controls,'capital_humano/view_directivo',atrib3,controls3);
							}
						}else{
							alerta('Ingrese Apellido Paterno',"top",'n_pat');
						}
					}else{
						alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
					}
				}else{
					alerta('Seleccione una ciudad','top','n_ciu');
				}
			}else{
				alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
			}
		}
	}
	/*--- End Configuración ---*/
	/*--- Eliminar ---*/
	$.fn.confirm_directivo=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			confirm_directivo($(this));
	 		}
	 	});
	 	function confirm_directivo(e){
	 		if(e.data("di")!=undefined){
		   		modal("DIRECTIVO: Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({di:e.data("di")});
		   		btn_modal('drop_directivo',atr,'',"","5");
				var atrib=new FormData();
		   		atrib.append('di',e.data("di"));
				var controls1=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('capital_humano/confirm_directivo',atrib,controls1);
	 		}
	 	}
	}
	$.fn.drop_directivo=function(){
	   	if($(this).data("di")!=undefined){
		   	var atrib=new FormData();
			atrib.append('di',$(this).data("di"));
		   	var atrib3=new FormData();
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('ci',$("#s_ci").val());
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set("capital_humano/drop_directivo",atrib,controls1,"capital_humano/view_directivo",atrib3,controls3);
	   	}
	}
	/*--- End Eliminar ---*/
	/*--- Imprimir ---*/
	$.fn.print_directivos=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_directivos($(this));
 			}
 		});
 		function print_directivos(e){
 			if(e.data("tbl")!=undefined){
 				modal("DIRECTIVOS: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("di")!=undefined){
		 				visibles[visibles.length]=$(tr).data("di");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('nom',$("#s_nom").val());
				atrib.append('ci',$("#s_ci").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('capital_humano/print_directivos',atrib,controls);
 			}
 		}
	}
	/*--- End imprimir ---*/
	/*--- Nuevo ---*/
	    $.fn.new_directivo=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_directivo($(this));
	 			}
	 		});
	 		function new_directivo(e){
		 		modal("Nuevo directivo","xmd","1");
		 		btn_modal('save_directivo',"this",'',"",'1');
		 		var atrib=new FormData();
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 		$(this).get_1n('capital_humano/new_directivo',atrib,controls);
	 		}
	 	}
	 	$.fn.new_cuenta_banco=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_cuenta_banco($(this));
	 			}
	 		});
	 		function new_cuenta_banco(e){
	 			if(e.data("container")!=undefined){
	 				modal("Nuevo: Cuenta bancaria","ms","2");
	 				var atr="this|"+JSON.stringify({container:e.data("container")});
			 		btn_modal('append_cuenta_banco',atr,'',"",'2');
			 		var atrib=new FormData();
			 		atrib.append("container",e.data("container"));
			 		var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
			 		$(this).get_1n('capital_humano/new_cuenta_banco',atrib,controls);
	 			}
	 		}
	 	}
	 	/*--Banco--*/
	 	$.fn.view_bancos=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				view_bancos($(this));
	 			}
	 		});
	 		function view_bancos(e){
	 			if(e.data("container")!=undefined){
			 		modal("Lista: Entidad bancaria","sm","3");
			 		btn_modal('',"",'',"",'3');
			 		var atrib=new FormData();
			 		atrib.append("container",e.data("container"));
			 		var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
			 		$(this).get_1n('capital_humano/view_bancos',atrib,controls);
	 			}
	 		}
	 	}
	 	$.fn.new_banco=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_banco($(this));
	 			}
	 		});
	 		function new_banco(e){
	 			if(e.data("container")!=undefined && e.data("type")!=undefined){
			 		modal("Nueva: Banco","xs","4");
			 		var atr="this|"+JSON.stringify({container:e.data("container"),type:e.data("type")});
			 		btn_modal('save_banco',atr,'',"",'4');
			 		var atrib=new FormData();
			 		atrib.append("container",e.data("container"));
			 		atrib.append("type",e.data("type"));
			 		var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
			 		$(this).get_1n('capital_humano/new_banco',atrib,controls);
		 		}
	 		}
	 	}
	 	$.fn.save_banco=function(){
	 		if($(this).data("container")!=undefined && $(this).data("type")!=undefined){
		 		var fot=$('input#b4_fot').prop('files');
		 		var nom=$('input#b4_nom').val();
		 		var url=$('input#b4_url').val();
		 		if(strSpace(nom,4,200)){
		 			var atrib=new FormData();
		 			atrib.append("archivo",fot[0]);
		 			atrib.append("nom",nom);
		 			atrib.append("url",url);
		 			atrib.append("container",$(this).data("container"));
		 			atrib.append("selected",$("#"+$(this).data("container")).val());
	 				var controls1=JSON.stringify({type:"set",preload:true,closed:"4"});
	 				var controls3=JSON.stringify({id:$(this).data("container"),refresh:false,type:"html"});
					if($(this).data("type")=="new"){
		 				$(this).set('capital_humano/save_banco',atrib,controls1,'capital_humano/options_bancos',atrib,controls3);
	 				}
	 				if($(this).data("type")=="new_modal"){
	 					var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
	 					$(this).set_2n('capital_humano/save_banco',atrib,controls1,'capital_humano/view_bancos',atrib,controls2,'capital_humano/options_bancos',atrib,controls3);
	 				}
		 		}else{
		 			alerta("Ingrese un nombre de entidad válido","top","b4_nom");
		 		}
	 		}
	 	}
	 	$.fn.config_banco=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				config_banco($(this));
	 			}
	 		});
	 		function config_banco(e){
	 			if(e.data("ba")!=undefined && e.data("container")!=undefined){
			 		modal("Modificar: Banco","xs","4");
			 		var atr="this|"+JSON.stringify({container:e.data("container"),ba:e.data("ba")});
			 		btn_modal('update_banco',atr,'',"",'4');
			 		var atrib=new FormData();
			 		atrib.append("ba",e.data("ba"));
			 		atrib.append("container",e.data("container"));
			 		var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
			 		$(this).get_1n('capital_humano/config_banco',atrib,controls);
		 		}
	 		}
	 	}
	 	$.fn.update_banco=function(){
	 		if($(this).data("ba")!=undefined && $(this).data("container")!=undefined){
		 		var fot=$('input#b4_fot').prop('files');
		 		var nom=$('input#b4_nom').val();
		 		var url=$('input#b4_url').val();
		 		if(strSpace(nom,4,200)){
		 			var atrib=new FormData();
		 			atrib.append("archivo",fot[0]);
		 			atrib.append("nom",nom);
		 			atrib.append("url",url);
		 			atrib.append("container",$(this).data("container"));
		 			atrib.append("selected",$("#"+$(this).data("container")).val());
		 			atrib.append("ba",$(this).data("ba"));
	 				var controls1=JSON.stringify({type:"set",preload:true,closed:"4"});
	 				var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
	 				var controls3=JSON.stringify({id:$(this).data("container"),refresh:false,type:"html"});
	 				$(this).set_2n('capital_humano/update_banco',atrib,controls1,'capital_humano/view_bancos',atrib,controls2,'capital_humano/options_bancos',atrib,controls3);
		 		}else{
		 			alerta("Ingrese un nombre de entidad válido","top","b4_nom");
		 		}
	 		}
	 	}
	    $.fn.confirm_banco=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				confirm_banco($(this));
	 			}
	 		});
	 		function confirm_banco(e){
		    	if(e.data("ba")!=undefined && e.data("container")!=undefined){
			    	modal("Eliminar","xs","5");
			    	var atr="this|"+JSON.stringify({container:e.data("container"),ba:e.data("ba")});
				   	btn_modal('drop_banco',atr,'',"",'5');
				   	var atrib=new FormData();
			 		atrib.append("ba",e.data("ba"));
			 		atrib.append("container",e.data("container"));
				   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				   	$(this).get_1n('capital_humano/confirm_banco',atrib,controls);
		    	}
	 		}
	    }
	    $.fn.drop_banco=function(){
	    	if($(this).data("ba")!=undefined && $(this).data("container")!=undefined){
		 		var atrib=new FormData();
		 		atrib.append("ba",$(this).data("ba"));
		 		atrib.append("container",$(this).data("container"));
		 		atrib.append("selected",$("#"+$(this).data("container")).val());
	 			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
	 			var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
	 			var controls3=JSON.stringify({id:$(this).data("container"),refresh:false,type:"html"});
	 			$(this).set_2n('capital_humano/drop_banco',atrib,controls1,'capital_humano/view_bancos',atrib,controls2,'capital_humano/options_bancos',atrib,controls3);
	    	}
	    }
	 	/*--End Banco--*/
	 	$.fn.append_cuenta_banco=function(){
	 		if($(this).data("container")!=undefined){
		 		var ba=$('select#b3_ban').val();
		 		var cta=$('input#b3_cta').val();
		 		var tip=$('select#b3_tip').val();
		 		if(entero(ba,0,10)){
		 			if(entero(cta,4,100)){
		 				if(tip=="1" || tip=="2" || tip=="3"){
		 					var atrib=new FormData();
		 					atrib.append("ba",ba);
		 					atrib.append("cta",cta);
		 					atrib.append("tip",tip);
							var controls=JSON.stringify({id:$(this).data("container"),refresh:false,type:"append",closed:"2"});
				   			$(this).get_1n('capital_humano/append_cuenta_banco',atrib,controls);
		 				}else{
		 					alerta("Seleccione una valor válido","top","b3_tip");
		 				}
		 			}else{
		 				alerta("Ingrese un número de cuenta válida","top","b3_cta");
		 			}
		 		}else{
		 			alerta("Seleccione una entidad bancaria válida","top","b3_ban");
		 		}
	 		}
	 	}
    $.fn.save_directivo=function(){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var car=$('#n_car').val();
		var telf=$('#n_tel').val();
		var ema=$('#n_ema').val();
		var fec=$('#n_fec').val();
		var obs=$('#n_obs').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						var control=true;
						if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
						if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
						if(car!=""){ if(!strSpace(car,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
						if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
						if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
						if(fec!=""){ if(!fecha(fec)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
						if(obs!=""){ if(!textarea(obs,0,800)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
						if(control){
							var cuentas=[];
							$("#n_proc span.label").each(function(idx,span){
								if($(span).data("type-save")!=undefined && $(span).data("ba")!=undefined && $(span).data("cuenta")!=undefined && $(span).data("tipo")!=undefined && $(span).attr("id")!=undefined){
									if($(span).data("type-save")=="new"){
										cuentas[cuentas.length]={ba:$(span).data("ba"),cuenta:$(span).data("cuenta"),tipo:$(span).data("tipo"),type_save:$(span).data("type-save")};	
									}
									if($(span).data("type-save")=="update"){
										cuentas[cuentas.length]={bp:$(span).data("bp"),ba:$(span).data("ba"),cuenta:$(span).data("cuenta"),tipo:$(span).data("tipo"),type_save:$(span).data("type-save")};	
									}
								}
							});
							var atrib= new FormData();
							atrib.append("ci",ci);
							atrib.append("ciu",ciudad);
							atrib.append("nom1",nom);
							atrib.append("nom2",nom2);
							atrib.append("pat",pat);
							atrib.append("mat",mat);
							atrib.append("car",car);
							atrib.append("tel",telf);
							atrib.append("ema",ema);
							atrib.append("nac",fec);
							atrib.append("obs",obs);
							atrib.append("archivo",fot[0]);
							atrib.append("cuentas",JSON.stringify(cuentas));
							var atrib3=new FormData();
							atrib3.append('cod',$("#s_cod").val());
							atrib3.append('nom',$("#s_nom").val());
							atrib3.append('ci',$("#s_ci").val());
							var controls=JSON.stringify({type:"set",preload:true,closed:"1"});
						 	var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							$(this).set("capital_humano/save_directivo",atrib,controls,'capital_humano/view_directivo',atrib3,controls3);
						}
					}else{
						alerta('Ingrese Apellido Paterno',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
	}
 	/*--- End Nuevo ---*/
/*------- MANEJO DE DIRECTIVOS -------*/
/*------- END MANEJO DE PLANILLA -------*/
    /*$.fn.extend({
      config_empleado: function(){
      	if($(this).data("e")!=undefined){
	 		modal("","xmd","11");
	 		btn_modal('',"",'',"",'11');
	 		var atrib=new FormData();atrib.append('e',$(this).data("e"));
	 		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
	 		$(this).get_1n('capital_humano/config_empleado',atrib,controls);
      	}
        return false;
      }
    });
    $.fn.extend({
      update_proceso_empleado: function(){
      	if($(this).data("pre")!=undefined){
      		var cat=$("select#n1_cat"+$(this).data("pre")).val()+"";
      		if(entero(cat,0,1) && (cat=="0" || cat=="1")){
      			var atrib=new FormData();atrib.append("e",$("#datos-empleado").data("e"));atrib.append("pre",$(this).data("pre"));atrib.append("cat",cat);

 				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
      			var controls=JSON.stringify({type:"set",preload:true});
	 			var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
 				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 			$(this).set_2n("capital_humano/update_proceso_empleado",atrib,controls,"capital_humano/config_empleado",atrib,controls2,"capital_humano/view_empleado",atrib3,controls3);
      		}else{
      			alerta("Seleccione una opción válida","top","n1_cat"+$(this).data("pre"));
      		}
      	}
        return false;
      }
    });
    $.fn.extend({
      confirmar_producto_empleado: function(){
      	if($(this).data("proe")!=undefined && $(this).data("padre")!=undefined){
   			modal("","xs","5");
   			var atr="this|"+JSON.stringify({proe:$(this).data("proe")});
   			btn_modal('drop_proceso_empleado',atr,'',"",'5');
   			var atrib=new FormData();atrib.append("e",$("#datos-empleado").data("e"));atrib.append("pre",$(this).data("pre"));
   			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
   			$(this).get_1n("capital_humano/confirmar_producto_empleado",atrib,controls);
      	}
        return false;
      }
    });*/
    /*--- end configuracion ---*/
})(jQuery);
function cambiar_estado(e){e.cambiar_estado();}
function save_proceso(e){e.save_proceso();}
function drop_proceso(e){e.drop_proceso();}
function update_empleado(e){e.update_empleado();}
function drop_empleado(e){e.drop_empleado();}
function update_directivo(e){e.update_directivo();}
function drop_directivo(e){e.drop_directivo();}
function save_banco(e){e.save_banco();}
function update_banco(e){e.update_banco();}
function drop_banco(e){e.drop_banco();}
function append_cuenta_banco(e){e.append_cuenta_banco();}
function save_directivo(e){e.save_directivo();}
function save_empleado(e){e.save_empleado();}
function save_proceso_empleado(e){ e.save_proceso_empleado();}
function drop_proceso_empleado(e) { e.drop_proceso_empleado();}
function save_procesos_productos_empleado(e){ e.save_procesos_productos_empleado();}
function update_productos_empleado(e){ e.update_productos_empleado();}
function drop_this_elemento(e){ e.drop_this_elemento();}
function inicio(){
	$('a#empleado').click(function(){$(this).get_2n('capital_humano/search_empleado',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'capital_humano/view_empleado',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("empleado","Empleados","capital_humano?p=1");});
	$('div#empleado').click(function(){$(this).get_2n('capital_humano/search_empleado',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'capital_humano/view_empleado',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("empleado","Empleados","capital_humano?p=1");});
	$('a#directivo').click(function(){$(this).get_2n('capital_humano/search_directivo',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'capital_humano/view_directivo',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("directivo","Directivos","capital_humano?p=3");});
	$('div#directivo').click(function(){$(this).get_2n('capital_humano/search_directivo',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'capital_humano/view_directivo',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("directivo","Directivos","capital_humano?p=3");});
	$('a#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'capital_humano/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de capital humano","capital_humano?p=5");});
	$('div#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'capital_humano/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de capital humano","capital_humano?p=5");});
}



/*------- MANEJO DE CONFIGURACION -------*/
   	/*--- Paises ---*/
   	function save_pais(){
   		var p=$("#p").val();
   		if(strSpace(p,2,100)){
   			var atrib=new FormData();
   			atrib.append('p',p);
   			set('capital_humano/save_pais',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p');
   		}
   		return false;
   	}
   	function update_pais(idpa) {
   		var p=$("#p"+idpa).val();
   		if(strSpace(p,2,100)){
   			var atrib=new FormData();
   			atrib.append('idpa',idpa);
   			atrib.append('p',p);
   			set('capital_humano/update_pais',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p'+idpa);
   		}
   		return false;
   	}
   	function alerta_pais(idpa){
   		modal("PAIS: Eliminar","xs","5");
   		btn_modal('delete_pais',"'"+idpa+"'",'',"",'5');
   		var atrib=new FormData();atrib.append('idpa',idpa);
   		get('capital_humano/confirmar_pais',atrib,'content_5',true);
   	}
   	function delete_pais(idpa) {
   		var atrib=new FormData();
   		atrib.append('idpa',idpa);
   		set('capital_humano/delete_pais',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Paises ---*/
   	/*--- Ciudades ---*/
   	function save_ciudad() {
   		var c=$("#c").val();
   		var cp=$("#cp").val();
   		if(strSpace(c,2,100)){
   			if(cp!=""){
	   			var atrib=new FormData();
	   			atrib.append('cp',cp);
	   			atrib.append('c',c);
	   			set('capital_humano/save_ciudad',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   			}else{
   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp');
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c');
   		}
   		return false;
   	}
   	function update_ciudad(idci) {
   		var c=$("#c"+idci).val();
   		var cp=$("#cp"+idci).val();
   		if(strSpace(c,2,100)){
   			if(cp!=""){
   				var atrib=new FormData();
	   			atrib.append('idci',idci);
	   			atrib.append('cp',cp);
	   			atrib.append('c',c);
	   			set('capital_humano/update_ciudad',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
	   		}else{
   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp'+idci);
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c'+idci);
   		}
   		return false;
   	}
   	function alerta_ciudad(idci){
   		modal("PAIS: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_ciudad',"'"+idci+"'");
   		var atrib=new FormData();atrib.append('idci',idci);
   		atrib.append('titulo','eliminar definitivamente la ciudad: '+$("#c"+idci).val());
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_ciudad(idci) {
   		var atrib=new FormData();
   		atrib.append('idci',idci);
   		set('capital_humano/delete_ciudad',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Ciudades ---*/
   	/*--- Feriados ---*/
   	function save_feriado(){
   		var fec=$("#f_fec").val();
   		var tip=$("#f_tip").val();
   		var des=$("#f_des").val();
   		if(fecha(fec)){
   			if(entero(tip,0,10)){
	   			if(textarea(des,0,150)){
		   			var atrib=new FormData();
		   			atrib.append('fec',fec);
		   			atrib.append('tip',tip);
		   			atrib.append('des',des);
		   			set('capital_humano/save_feriado',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
	   			}else{
	   				alerta('Ingrese una contenido válido','top','f_des');
	   			}
   			}else{
   				alerta('Seleccione una opcion','top','f_tip');
   			}
   		}else{
   			alerta('Seleccione una fecha válida','top','f_fec');
   		}
   		return false;
   	}
   	function update_feriado(idf) {
   		var fec=$("#f_fec"+idf).val();
   		var tip=$("#f_tip"+idf).val();
   		var des=$("#f_des"+idf).val();
   		if(entero(idf,0,10)){
	   		if(fecha(fec)){
	   			if(entero(tip,0,10)){
		   			if(textarea(des,5,150)){
			   			var atrib=new FormData();
			   			atrib.append('idf',idf);
			   			atrib.append('fec',fec);
			   			atrib.append('tip',tip);
			   			atrib.append('des',des);
			   			set('capital_humano/update_feriado',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
		   			}else{
		   				alerta('Ingrese una contenido válido','top','f_des'+idf);
		   			}
				}else{
	   				alerta('Seleccione una opcion','top','f_tip'+idf);
	   			}
	   		}else{
	   			alerta('Seleccione una fecha válida','top','f_fec'+idf);
	   		}
   		}else{
   			msj("fail");
   		}
   		return false;
   	}
   	function alerta_feriado(idf,fecha,descripcion){
   		modal("FERIADO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_feriado',"'"+idf+"'");
   		var atrib=new FormData();atrib.append('idf',idf);
   		atrib.append('titulo','eliminar definitivamente el feriado : <b>'+descripcion+'</b> en fecha '+fecha);
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_feriado(idf){
   		var atrib=new FormData();
   		atrib.append('idf',idf);
   		set('capital_humano/delete_feriado',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End Feriados ---*/
	/*--- tipo de contrato ---*/
   	function save_tipo(){
   		var tip=$("#t_tip").val();
   		var hor=$("#t_hor").val();
   		var des=$("#t_des").val();
   		if(entero(tip,0,1) && tip>=0 && tip<=1){
   			if(hor>=0 && hor<=99){
   				if(textarea(des,0,150)){
					var atrib=new FormData();
		   			atrib.append('tip',tip);
		   			atrib.append('hor',hor);
		   			atrib.append('des',des);
   					set('capital_humano/save_tipo',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   				}else{
   					alerta('Ingrese un contenido válido','top','t_des');	
   				}
   			}else{
   				alerta('Ingrese un valor válido','top','t_hor');
   			}
   		}else{
   			alerta('Seleccione una tipo válido','top','t_tip');
   		}
   		return false;
   	}
   	function select_principal(idtc){
   		if(entero(idtc,0,10)){
   			var atrib=new FormData();
		   	atrib.append('idtc',idtc);
   			set('capital_humano/select_principal',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
   		}else{
   			msj("fail");
   		}
   	}
   	function update_tipo(idtc){
   		var tip=$("#t_tip"+idtc).val();
   		var hor=$("#t_hor"+idtc).val();
   		var des=$("#t_des"+idtc).val();
   		if(entero(idtc,0,10)){
	   		if(entero(tip,0,1) && tip>=0 && tip<=1){
	   			if(hor>=0 && hor<=99){
	   				if(textarea(des,0,150)){
						var atrib=new FormData();
						atrib.append('idtc',idtc);
			   			atrib.append('tip',tip);
			   			atrib.append('hor',hor);
			   			atrib.append('des',des);
	   					set('capital_humano/update_tipo',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false);
	   				}else{
	   					alerta('Ingrese un contenido válido','top','t_des'+idtc);	
	   				}
	   			}else{
	   				alerta('Ingrese un valor válido','top','t_hor'+idtc);
	   			}
	   		}else{
	   			alerta('Seleccione una tipo válido','top','t_tip'+idtc);
	   		}
   		}else{
   			msj("fail");
   		}
   		return false;
   	}
   	function alerta_tipo(idtc,tipo,horas){
   		modal("TIPO DE CONTRATO: Eliminar","xs","5");
   		addClick_v2('modal_ok_5','delete_tipo',"'"+idtc+"'");
   		var atrib=new FormData();atrib.append('idtc',idtc);
   		atrib.append('titulo','eliminar definitivamente el tipo de contrato de : <b>'+tipo+'</b> de '+horas+'h.');
   		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function delete_tipo(idtc){
   		var atrib=new FormData();
   		atrib.append('idtc',idtc);
   		set('capital_humano/delete_tipo',atrib,'NULL',true,'capital_humano/view_config',{},'contenido',false,'5');
   		return false;
   	}
   	/*--- End tipo de contrato ---*/
/*------- END MANEJO DE CONFIGURACION -------*/






/*------- MANEJO DE CAPITAL HUMANO -------*/
   	/*--- Buscador ---*/
   	/*function reset_input(id){
		if(id!="s_cod"){ $("#s_cod").val(""); }
		if(id!="s_nom"){ $("#s_nom").val(""); }
		if(id!="s_ci"){ $("#s_ci").val(""); }
		if(id!="s_con"){ $("#s_con").val(""); }
		if(id!="s_tip"){ $("#s_tip").val(""); }
	}*/
	/*function reset_input_3(id){
		if(id!="cod_3"){ $("#cod_3").val("");}
		if(id!="nom_3"){ $("#nom_3").val("");}
	}
	/*function blur_all(){
		OnBlur("s_cod");
		OnBlur("s_nom");
		OnBlur("s_ci");
		OnBlur("s_con");
		OnBlur("s_tip");
		OnBlur("s_est");
	}*/
   	/*function view_empleados(){
		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
		var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
		$(this).get_1n("capital_humano/view_empleado",atrib3,controls);
		$(this).blur_all();
		return false;
	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
	/*function all_empleado(){
		$(this).reset_input("");
		view_empleados();
	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
   	/*function new_empleado(){
 		modal("CAPITAL HUMANO: Nuevo","xmd","1");
 		btn_modal('save_empleado',"",'',"",'1');
 		var atrib=new FormData();
 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
 		$(this).get_1n('capital_humano/new_empleado',atrib,controls);
	}*/
	/*function search_ci(){
   		var ci=$("#n_ci").val();
   		if(ci!=""){
	   		var inputs="n_fot|n_ciu|n_nom1|n_nom2|n_pat|n_mat|n_gra|n_car|n_tel";
	   		var atrib=new FormData();
	   		atrib.append('ci',ci);
	   		get_input('capital_humano/search_ci',atrib,"NULL",inputs,false);
   		}
   	}
	function save_empleado(){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var cod=$('#n_cod').val();
		var te=$('#n_tem').val();
		var gra=$('#n_gra').val();
		var car=$('#n_car').val();
		var telf=$('#n_tel').val();
		var ema=$('#n_ema').val();
		var fn=$('#n_fn').val();
		var fi=$('#n_fi').val();
		var tc=$('#n_tc').val();
		var sal=$('#n_sal').val();
		var dir=$('#n_dir').val();
		var obs=$('#n_obs').val();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						if(entero(te,0,1) && te>=0 && te<=1){
							if(entero(tc,0,10)){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(cod!=""){ if(!entero(cod,0,10) || cod<0 || cod>9999999999){ control=false; alerta('Ingrese un nombre válido',"top",'n_cod');}}
									if(gra!=""){ if(!strSpace(gra,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_gra');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
									if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
									if(fn!=""){ if(!fecha(fn)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
									if(fi!=""){ if(!fecha(fi)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fi');}}
									if(sal!="" && sal>0){ if(!decimal(sal,4,1) || sal<0 || sal>9999.9){ control=false; alerta('Ingrese salario válido del empleado',"top",'n_sal');}}
									if(dir!=""){ if(!direccion(dir,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_dir');}}
									if(obs!=""){ if(!textarea(obs,0,800)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
									if(control){
										var atribs= new FormData();
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("cod",cod);
										atribs.append("te",te);
										atribs.append("gra",gra);
										atribs.append("car",car);
										atribs.append("tc",tc);
										atribs.append("sal",sal);
										atribs.append("tel",telf);
										atribs.append("ema",ema);
										atribs.append("nac",fn);
										atribs.append("ing",fi);
										atribs.append("dir",dir);
										atribs.append("obs",obs);
										atribs.append("archivo",fot[0]);
										var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
										set('capital_humano/save_empleado',atribs,'NULL',true,'capital_humano/view_empleado',atrib3,'contenido',false,'1');
									}
							}else{
								alerta('Seleccione un tipo de contrato',"top",'n_tc');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tem');
						}
					}else{
						alerta('Ingrese Apellido Paterno',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
		return false;
	}*/
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*function imprimir_empleados(json){
   		modal("EMPLEADOS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('capital_humano/imprimir_empleados',atrib,'content_1',true,'capital_humano/arma_empleados',atrib,'area',true);
   	}
   	function arma_empleados(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var v12=""; if($("#12:checked").val()){v12='ok'}
		var v13=""; if($("#13:checked").val()){v13='ok';}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('v12',v12);
		atrib.append('v13',v13);
		atrib.append('nro',nro);
		get('capital_humano/arma_empleados',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Estado ----*/
   	/*--- End Estado ----*/
   	/*function cambiar_estado(ide){
 		var atrib=new FormData();atrib.append('ide',ide);
 		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
 		set('capital_humano/cambiar_estado',atrib,'NULL',true,'capital_humano/view_empleado',atrib3,'contenido',false);
	}*/
   	/*--- Reportes ---*/
   	/*function detalle_empleado(ide){
   		modal("","md","11");
 		btn_modal('',"",'imprimir',"'area'",'11');
 		var atrib=new FormData();atrib.append('ide',ide);
 		get('capital_humano/detalle_empleado',atrib,'content_11',true);
	}*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*
   	function change_empleado(ide){
 		modal("","xmd","11");
 		btn_modal('update_empleado',"'"+ide+"'",'',"",'11','md');
 		var atrib=new FormData();
 		atrib.append('ide',ide);
 		get('capital_humano/change_empleado',atrib,'content_11',true);
	}
	function update_empleado(ide){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var cod=$('#n_cod').val();
		var te=$('#n_tem').val();
		var car=$('#n_car').val();
		var telf=$('#n_tel').val();
		var ema=$('#n_ema').val();
		var fn=$('#n_fn').val();
		var fi=$('#n_fi').val();
		var tc=$('#n_tc').val();
		var gra=$('#n_gra').val();
		var sal=$('#n_sal').val();
		var dir=$('#n_dir').val();
		var obs=$('#n_obs').val();
		var procesos_eliminados=$("#n_proc #procesos_db").html();
		if(entero(ci,6,8) && ci>=100000 && ci<=99999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						if(entero(te,0,1) && te>=0 && te<=1){
							if(entero(tc,0,10)){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_mat');}}
									if(cod!=""){ if(!entero(cod,0,10) || cod<0 || cod>9999999999){ control=false; alerta('Ingrese un nombre válido',"top",'n_cod');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(telf!=""){ if(!entero(telf,6,15) || telf<100000 || telf>999999999999999){ control=false; alerta('Ingrese un contenido válido',"top",'n_tel');}}
									if(ema!=""){ if(!email(ema)){ control=false; alerta('Ingrese un contenido válido',"top",'n_ema');}}
									if(fn!=""){ if(!fecha(fn)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fn');}}
									if(fi!=""){ if(!fecha(fi)){ control=false; alerta('Ingrese una fecha válida',"top",'n_fi');}}
									if(gra!=""){ if(!strSpace(gra,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_gra');}}
									if(sal!="" && sal>0){ if(!decimal(sal,4,1) || sal<0 || sal>9999.9){ control=false; alerta('Ingrese salario válido del empleado',"top",'n_sal');}}
									if(dir!=""){ if(!direccion(dir,0,200)){ control=false; alerta('Ingrese un contenido válido',"top",'n_dir');}}
									if(obs!=""){ if(!textarea(obs,0,800)){ control=false; alerta('Ingrese un contenido válido',"top",'n_obs');}}
									if(control){
										var procesos=[];
										$("#n_proc span.label").each(function(idx,span){
											if($(span).data("type-save")!=undefined && $(span).data("pr")!=undefined && $(span).attr("id")!=undefined){
												if($("#"+$(span).attr("id")+" span.closed").data("tipo")!=undefined){
													if($(span).data("type-save")=="new"){
														procesos[procesos.length]={pr:$(span).data("pr"),tipo:$("#"+$(span).attr("id")+" span.closed").data("tipo"),type_save:$(span).data("type-save")};	
													}
													if($(span).data("type-save")=="update"){
														procesos[procesos.length]={pre:$(span).data("pre"),tipo:$("#"+$(span).attr("id")+" span.closed").data("tipo"),type_save:$(span).data("type-save")};	
													}
												}
											}
										});
										var atribs= new FormData();
										atribs.append("ide",ide);
										atribs.append("ci",ci);
										atribs.append("ciu",ciudad);
										atribs.append("nom1",nom);
										atribs.append("nom2",nom2);
										atribs.append("pat",pat);
										atribs.append("mat",mat);
										atribs.append("cod",cod);
										atribs.append("te",te);
										atribs.append("car",car);
										atribs.append("tc",tc);
										atribs.append("gra",gra);
										atribs.append("sal",sal);
										atribs.append("tel",telf);
										atribs.append("ema",ema);
										atribs.append("nac",fn);
										atribs.append("ing",fi);
										atribs.append("dir",dir);
										atribs.append("obs",obs);
										atribs.append("procesos_eliminados",procesos_eliminados);
										atribs.append("procesos",JSON.stringify(procesos));
										atribs.append("archivo",fot[0]);
										var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('ci',$("#s_ci").val());atrib3.append('con',$("#s_con").val());atrib3.append('tip',$("#s_tip").val());atrib3.append('est',$("#s_est").val());
										set_2n('capital_humano/update_empleado',atribs,'NULL',true,'capital_humano/change_empleado',atribs,'content_11',false,'capital_humano/view_empleado',atrib3,'contenido',false,'1');
									}
							}else{
								alerta('Seleccione un tipo de contrato',"top",'n_tc');
							}
						}else{
							alerta('Seleccione un tipo de empleado','top','n_tem');
						}
					}else{
						alerta('Ingrese Apellido Paterno',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
		return false;
	}
*/
/*--- End configuracion ---*/

/*------- END MANEJO DE CAPITAL HUMANO -------*/
/*------- MANEJO DE PLANILLA -------*/
   	/*--- Buscador ---*/
   /*	function view_planilla(){
		var ci=$("#s_ci").val();
		var nom=$("#s_nom").val();
		var f1=$("#f1").val();
		var f2=$("#f2").val();
		var atrib=new FormData();
		atrib.append('ci',ci);
		atrib.append('nom',nom);
		atrib.append('f1',f1);
		atrib.append('f2',f2);
		get("capital_humano/view_planilla",atrib,'contenido',true);
		return false;	
	}
   	/*--- End Buscador ---*/
   	/*--- Nuevo ---*/
 /*  	function new_biometrico(){
   		modal("PLANILLA: Importar Archivo Biometrico","xs","1");
   		btn_modal('save_biometrico',"",'',"",'1','xs');
 		get("capital_humano/new_biometrico",{},'content_1');
   	}
   	function save_biometrico(){
   		var archivo=$("#archivo").prop('files');
   		var tipo=$("#tipo").val();
   		if(archivo.length==1){
   			if(entero(tipo,1,1) && tipo>=0 && tipo<=1){
		   		var atrib=new FormData();
		   		atrib.append('archivo',archivo[0]);
		   		atrib.append('tipo',tipo);
				var atrib2=new FormData();atrib2.append('f1',$("#f1").val());atrib2.append('f2',$("#f2").val());
				set('capital_humano/save_biometrico',atrib,'NULL',true,'capital_humano/view_planilla',atrib2,'contenido',false,"1");
   			}else{
   				alerta("Seleccione un tipo de busqueda válido","top","tipo");
   			}
	   	}else{
	   		alerta("Seleccione un archivo de formato .xls 2003","top","archivo");
	   	}
   	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   /*	function imprimir_planilla(json,f1,f2){
   		modal("PLANILLA: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);atrib.append('f1',f1);atrib.append('f2',f2);
 		get_2n('capital_humano/imprimir_planilla',atrib,'content_1',true,'capital_humano/arma_planilla',atrib,'area',true);
   	}
   	function arma_planilla(json,f1,f2){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('nro',nro);
		atrib.append('f1',f1);
		atrib.append('f2',f2);
		get('capital_humano/arma_planilla',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
/*   	function reporte_planilla(ide,f1,f2){
   		modal("PLANILLA: Detalle","md","1");
   		btn_modal('',"",'imprimir',"'area'",'1','md');
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
 		get("capital_humano/reporte_planilla",atrib,'content_1',true);
   	}
   	function nro_planilla(ide,f1,f2){
   		modal("PLANILLA: Detalle","md","1");
   		btn_modal('',"",'imprimir',"'area'",'1','md');
   		var nro=$("#nro").val();
   		if(entero(nro,0,2) && nro>=0){
	   		var atrib=new FormData();
	   		atrib.append('ide',ide); atrib.append('f1',f1); atrib.append('f2',f2); atrib.append("nro",nro);
	 		get("capital_humano/reporte_planilla",atrib,'content_1');
   		}else{
   			alerta("Seleccione un archivo de formato .xls 2003","top","archivo");
   		}
   	}
   	function pae(ide,f1,f2){
   		modal("PLANILLA: PAE","lg","1");
   		btn_modal('',"",'imprimir',"'area'",'1','lg');
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
 		get("capital_humano/pae",atrib,'content_1',true);
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
/*   	function detalle_anticipos(ide,f1,f2){
   		if(entero(ide,0,10) && fecha(f1) && fecha(f2)){
   			modal("PLANILLA: Detalle de anticipos","sm","1");
	   		btn_modal('',"",'',"",'1','sm');
	   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
	 		get("capital_humano/detalle_anticipos",atrib,'content_1',true);
   		}else{
   			msj("fail");
   		}
   	}
		function save_planilla_anticipo(ide,f1,f2){
			var fec=$("#fec_e").val();
			var mon=$("#mon_e").val();
			var des=$("#des_e").val();
			if(fecha(f1) && fecha(f2)){
				if(fecha(fec)){
					if(decimal(mon,5,1) && mon>0 && mon<=99999.9){
						if(textarea(des,0,300)){
							var atrib=new FormData();atrib.append('ide',ide);atrib.append('fec',fec);atrib.append('mon',mon);atrib.append('des',des);
							var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
							var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
						 	set_2n('capital_humano/save_anticipo',atrib,'NULL',true,'capital_humano/detalle_anticipos',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
						}else{
							alerta('Ingrese un contenido válido','top','des_e');						
						}
					}else{
						alerta('Ingrese un valor válido','top','mon_e');
					}
				}else{
					alerta('Ingrese una fecha válida','top','fec_e');
				}
			}else{
				msj("Error en lectura de datos.");
			}
			return false;
		}
		function update_planilla_anticipo(ide,idan,f1,f2){
			var fec=$("#fec_e"+idan).val();
			var mon=$("#mon_e"+idan).val();
			var des=$("#des_e"+idan).val();
			if(fecha(f1) && fecha(f2)){
				if(fecha(fec)){
					if(decimal(mon,5,1) && mon>0 && mon<=99999.9){
						if(textarea(des,0,300)){
							var atrib=new FormData();atrib.append('idan',idan);atrib.append('fec',fec);atrib.append('mon',mon);atrib.append('des',des);
							var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
							var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
						 	set_2n('capital_humano/update_anticipo',atrib,'NULL',true,'capital_humano/detalle_anticipos',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
						}else{
							alerta('Ingrese un contenido válido','top','des_e'+idan);						
						}
					}else{
						alerta('Ingrese un valor válido','top','mon_e'+idan);
					}
				}else{
					alerta('Ingrese una fecha válida','top','fec_e'+idan);
				}
			}else{
				msj("Error en lectura de datos.");
			}
			return false;
		}
		function alerta_planilla_anticipo(ide,idan,f1,f2){
			modal("EMPLEADOS: Eliminar proceso","xs","5");
		   	addClick_v2('modal_ok_5','drop_planilla_anticipo',"'"+ide+"','"+idan+"','"+f1+"','"+f2+"'");
		   	var atrib=new FormData();atrib.append('idan',idan);
		   	atrib.append('titulo','eliminar definitivamente el anticipo');
		   	get('insumo/alerta',atrib,'content_5',true);
		}
		function drop_planilla_anticipo(ide,idan,f1,f2){
			var atrib=new FormData();atrib.append('idan',idan);
			var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
			var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
			set_2n('capital_humano/drop_anticipo',atrib,'NULL',true,'capital_humano/detalle_anticipos',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false,'5');
		}
   	function config_planilla(ide,f1,f2){
   		modal("PLANILLA: Configuración","md","1");
   		btn_modal('',"",'',"",'1','md');
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
 		get("capital_humano/config_planilla",atrib,"content_1");
   	}

   	function add_hora(ide,fecha,f1,f2){
   		modal("PLANILLA: Adicionar hora","xs","2");
   		btn_modal('save_hora',"'"+ide+"','"+fecha+"','"+f1+"','"+f2+"'",'',"",'2','xs');
   		var atrib=new FormData();atrib.append('fecha',fecha);
		get("capital_humano/add_hora",atrib,"content_2");
		//ajax_get('capital_humano/add_hora',atribs,'content_2');
   	}
   	function save_hora(ide,fecha,f1,f2){
   		var hora=$("#hora_new").val();
   		if(hora!=""){
   			var atrib=new FormData();atrib.append('fecha',fecha);atrib.append('hora',hora);atrib.append('ide',ide);atrib.append('f1',f1);atrib.append('f2',f2);
			var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
			set_2n('capital_humano/save_hora',atrib,'NULL',true,'capital_humano/config_planilla',atrib,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false,"2");
   		}else{
   			alerta("Ingrese una hora valida","top","hora_new");
   		}
   	}
   	function alerta_hora(ide,idhb,f1,f2){
   		modal("PLANILLA: Eliminar hora","xs","5");
		addClick_v2('modal_ok_5','drop_hora',"'"+ide+"','"+idhb+"','"+f1+"','"+f2+"'");
		var atrib=new FormData();
		atrib.append('titulo','eliminar definitivamente la hora registrada');
		atrib.append('descripcion','<strong class="text-danger">Si eliminar la hora puede influir en el tiempo de trabajo y monto de sueldo a pagar</strong>');
		get('insumo/alerta',atrib,'content_5',true);
   	}
   	function drop_hora(ide,idhb,f1,f2){
		var atrib=new FormData();atrib.append('ide',ide);atrib.append('idhb',idhb);atrib.append('f1',f1);atrib.append('f2',f2);
		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
		set_2n('capital_humano/drop_hora',atrib,'NULL',true,'capital_humano/config_planilla',atrib,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false,"5");
	}
	/*--- Control de feriados ---*/
/*	function update_planilla_c_feriado(ide,f1,f2){
		var feriado=$("#c_fec").val();
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('feriado',feriado);
   		var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
   		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
		set_2n('capital_humano/update_c_feriado',atrib,'NULL',true,'capital_humano/config_planilla',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
   	}
	/*--- End Control de feriados ---*/
	/*--- Control de feriados ---*/
/*	function update_planilla_c_descuento(ide,f1,f2){
		var des=$("#c_dec").val();
   		var atrib=new FormData();atrib.append('ide',ide);atrib.append('des',des);
   		var atrib2=new FormData();atrib2.append('ide',ide);atrib2.append('f1',f1);atrib2.append('f2',f2);
   		var atrib3=new FormData();atrib3.append('ci',$("#s_ci").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('f1',$("#f1").val());atrib3.append('f2',$("#f2").val());
		set_2n('capital_humano/update_c_descuento',atrib,'NULL',true,'capital_humano/config_planilla',atrib2,'content_1',false,'capital_humano/view_planilla',atrib3,'contenido',false);
   	}
	/*--- End Control de feriados ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLANILLA -------*/
