(function($){
/*------- MANEJO DE MATERIALES -------*/
   	$.fn.reset_input=function(){
   		if($(this).attr("id")!=undefined){
   			var id=$(this).attr("id");
   		}else{
   			var id="";
   		}
		if(id!="s_cod"){$("#s_cod").val("");}
		if(id!="s_nom"){$("#s_nom").val("");}
		if(id!="s_gru"){$("#s_gru").val("");}
		if(id!="s_can"){$("#s_can").val("");}
		if(id!="s_uni"){$("#s_uni").val("");}
		if(id!="s_col"){$("#s_col").val("");}
	}
	$.fn.blur_all=function(id){
		if(id!="s_cod"){ OnBlur("s_cod"); }
		if(id!="s_nom"){ OnBlur("s_nom"); }
		if(id!="s_gru"){ OnBlur("s_gru"); }
		if(id!="s_cod"){ OnBlur("s_can"); }
		if(id!="s_cod"){ OnBlur("s_uni"); }
		if(id!="s_cod"){ OnBlur("s_col"); }
	}
   	/*--- Buscador ---*/
   	$.fn.search_material=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_material($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_material($(this));
 			}
		 });
		 $(this).on("mousewheel",function(e){
			if($(this).prop("tagName")!="FORM"){
				e.preventDefault();
			}
		});
 		function search_material(e){
			 e.reset_input();
			 e.search_elements();
		 }
	}
   	$.fn.view_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_material($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_material($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_material($(this));
	    	event.preventDefault();
	    });
 		function view_material(e){
			var atrib3=new FormData();
			atrib3.append('cod',$("#s_cod").val());
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('gru',$("#s_gru").val());
			atrib3.append('can',$("#s_can").val());
			atrib3.append('col',$("#s_col").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('../../material/view_material/'+aid,atrib3,controls);
			$(this).blur_all("");
 		}
   	}
   	/*--- End Buscador ---*/
   	/*--- Reportes ---*/
   	$.fn.reportes_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				reportes_material($(this));
 			}
 		});
 		function reportes_material(e){
 			if(e.data("am")!=undefined){
	   			modal('MATERIAL: Detalle','md','11');
				btn_modal('',"",'',"",'11');
				var atrib=new FormData();
				atrib.append('am',e.data("am"));
			 	var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				$(this).get_1n('../../material/detalle_material/'+aid,atrib,controls);
	   		}
 		}
	}
	$.fn.material_productos=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				material_productos($(this));
 			}
 		});
 		function material_productos(e){
 			if(e.data("am")!=undefined){
 				modal('','md','11');
				btn_modal('',"",'',"",'11');
				var atrib=new FormData();
				atrib.append('am',e.data("am"));
			 	var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('../../material/material_productos/'+aid,atrib,controls);
 			}
 		}
    }
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	$.fn.config_material=function(idam,idm){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				config_material($(this));
 			}
 		});
 		function config_material(e){
 			if(e.data("am")!=undefined && e.data("m")!=undefined){
 				modal('MATERIAL: Modificar','md','1');
 				var atr="this|"+JSON.stringify({m:e.data("m")});
			 	btn_modal('update_material',atr,'',"",'1');
			 	var atrib=new FormData();
			 	atrib.append('am',e.data("am"));
			 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('../../material/config_material/'+aid,atrib,controls);
 			}
 		}
	}
	$.fn.update_material=function(){
		if($(this).data("m")!=undefined){
			var fot=$("#fot").prop('files');
			var nom=$("#nom").val();
			var cod=$("#cod").val();
			var gru=$("#gru").val();
			var med=$("#med").val();
			var col=$("#col").val();
			var des=$("#des").val();
			var pro=$("#pro").val();
			var cos_pro=$("#cos_pro").val();
			if(strSpace(nom,2,100)){
				if(strSpace(cod,2,15)){
					if(entero(gru,0,10)){
							if(entero(col,0,10)){
								if(entero(med,0,10)){
									if(fot.length<=1){
										var control=true;
										if(des!=""){if(!textarea(des,0,300)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
										var atrib= new FormData();
										atrib.append("idm",$(this).data("m"));
										atrib.append("nom",nom);
										atrib.append("cod",cod);
										atrib.append("gru",gru);
										atrib.append("med",med);
										atrib.append("col",col);
										atrib.append("des",des);
										atrib.append("archivo",fot[0]);
										var atrib3=new FormData();
										atrib3.append('cod',$("#s_cod").val());
										atrib3.append('nom',$("#s_nom").val());
										atrib3.append('gru',$("#s_gru").val());
										atrib3.append('can',$("#s_can").val());
										atrib3.append('col',$("#s_col").val());
										var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
			 							var controls3=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			 							$(this).set('../../material/update_material/'+aid,atrib,controls1,"../../material/view_material/"+aid,atrib3,controls3);
									}else{
										alerta("Solo puede subir hasta 1 archivo simultaneos","top","fot");
									}
								}else{
									alerta("Seleccione una Medida de material...","top","med");
								}
							}else{
								alerta("Selecciones un color de material...","top","col");
							}
					}else{
						alerta("Selecciones un Grupo de material...","top","gru");
					}
				}else{
					alerta("Ingrese un Código material válido...","top","cod");
				}
			}else{
				alerta("Ingrese un Nombre de material válido...","top","nom");
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirm_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_material($(this));
 			}
 		});
 		function confirm_material(e){
 			if(e.data("am")!=undefined){
				modal("Eliminar","xs","5");
				var atr="this|"+JSON.stringify({am:e.data("am")});
		   		btn_modal('drop_material',atr,'',"",'5');
		   		var atrib=new FormData();
		   		atrib.append('am',e.data("am"));
			 	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('../../material/confirmar_material/'+aid,atrib,controls);
 			}
 		}
   	}
   	$.fn.drop_material=function(){
   		if($(this).data("am")!=undefined){
	   		var u=$("#e_user").val();
	   		var p=$("#e_password").val();
	   		if(u!=""){
	   			if(p!=""){
					var atrib=new FormData();
					atrib.append('u',u);
					atrib.append('p',p);
					atrib.append('am',$(this).data("am"));
					var atrib3=new FormData();
					atrib3.append('cod',$("#s_cod").val());
					atrib3.append('nom',$("#s_nom").val());
					atrib3.append('gru',$("#s_gru").val());
					atrib3.append('can',$("#s_can").val());
					atrib3.append('col',$("#s_col").val());
					var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls3=JSON.stringify({id:"contenido",refresh:true,type:"html"});
					$(this).set('../../material/drop_material/'+aid,atrib,controls1,"../../material/view_material/"+aid,atrib3,controls3);
	   			}else{
	   				alerta("Ingrese un contenido válido","top","e_password");
	   			}
	   		}else{
	   			alerta("Ingrese un contenido válido","top","e_user");
	   		}
   		}
	}
   	/*--- End Eliminar ---*/
   	/*--- Nuevo ---*/
    $.fn.new_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_material($(this));
 			}
 		});
 		function new_material(e){
		 	modal("","md","11");
		 	btn_modal('save_material',"this",'',"","11");
			var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			$(this).get_1n("../../material/new_material/"+aid,{},controls);
 		}
    };
	$.fn.save_material=function(){
		var fot=$("#fot").prop('files');
		var nom=$("#nom").val();
		var cod=$("#cod").val();
		var gru=$("#gru").val();
		var col=$("#col").val();
		var med=$("#med").val();
		var des=$("#des").val();
		if(strSpace(nom,2,100)){
			if(strSpace(cod,2,15)){
				if(entero(gru,0,10)){
						if(entero(col,0,10)){
							if(entero(med,0,10)){
								if(fot.length<=1){
									var control=true;
									if(des!=""){if(!textarea(des,0,300)){ control=false;alerta("Ingrese un contenido válido","top","des");}}
									if(control){
										var atrib=new FormData();
										atrib.append("nom",nom);
										atrib.append("cod",cod);
										atrib.append("gru",gru);
										atrib.append("med",med);
										atrib.append("col",col);
										atrib.append("des",des);
										atrib.append("archivo",fot[0]);
										var atrib3=new FormData();
										atrib3.append('cod',$("#s_cod").val());
										atrib3.append('nom',$("#s_nom").val());
										atrib3.append('gru',$("#s_gru").val());
										atrib3.append('can',$("#s_can").val());
										atrib3.append('col',$("#s_col").val());
										var controls1=JSON.stringify({type:"set",preload:true,closed:"11"});
			 							var controls3=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			 							$(this).set('../../material/save_material/'+aid,atrib,controls1,"../../material/view_material/"+aid,atrib3,controls3);
									}
								}else{
									alerta("Solo puede subir hasta 1 archivo simultaneos","top","fot");
								}
							}else{
								alerta("Seleccione una Medida de material...","top","med");
							}
						}else{
							alerta("Selecciones un color de material...","top","col");
						}
				}else{
					alerta("Selecciones un Grupo de material...","top","gru");
				}
			}else{
				alerta("Ingrese un Código material válido...","top","cod");
			}
		}else{
			alerta("Ingrese un Nombre de material válido...","top","nom");
		}
	}
	$.fn.add_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				add_material($(this));
 			}
 		});
 		function add_material(e){
 			modal('',"md",'11');
		 	btn_modal('',"",'',"",'11');
	 		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
	 		e.get_2n('../../material/add_material/'+aid,{},controls,'../../material/view_add_material/'+aid,{},controls2);
 		}
	}
	$.fn.search_add_material=function(){
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_add_material($(this));
 			}
 		});
 		function search_add_material(e){
 			var alm=$("#alm").val();
			var atrib=new FormData();atrib.append('alm',alm);
			var controls=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
			$(this).get_1n("../../material/view_add_material/"+aid,atrib,controls);
 		}
	}
	$.fn.add_material_almacen=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				add_material_almacen($(this));
 			}
 		});
 		function add_material_almacen(e){
 			if(e.data("m")!=undefined && e.data("a")!=undefined){
				var atrib=new FormData();
				atrib.append('idm',e.data("m"));
				atrib.append('ida',e.data("a"));
				atrib.append('alm',$("#alm").val());
				var atrib3=new FormData();
				atrib3.append('cod',$("#s_cod").val());
				atrib3.append('nom',$("#s_nom").val());
				atrib3.append('gru',$("#s_gru").val());
				atrib3.append('can',$("#s_can").val());
				atrib3.append('col',$("#s_col").val());
				var controls1=JSON.stringify({type:"set",preload:true});
			 	var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			 	e.set('../../material/add_material_almacen/'+aid,atrib,controls1,"../../material/view_material/"+aid,atrib3,controls3);
 			}
 		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_material=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_material($(this));
 			}
 		});
 		function print_material(e){
 			if(e.data("tbl")!=undefined){
 				modal("MATERIAL: Configuracion de impresion","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
					var control=false;
					if($(tr).attr("style")==undefined){
					   control=true;
				  	}else{
					   if($(tr).attr("style").indexOf("display: none")===-1){
						   control=true;
					   }
				   	}
				  	if(control){
					   	if($(tr).data("m")!=undefined){
		 					visibles[visibles.length]=$(tr).data("m");
					 	}
					}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('cod',$("#s_cod").val());
				atrib.append('nom',$("#s_nom").val());
				atrib.append('gru',$("#s_gru").val());
				atrib.append('can',$("#s_can").val());
				atrib.append('col',$("#s_col").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('../../material/print_material/'+aid,atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
/*------- END MANEJO DE MATERIALES -------*/
/*------- MANEJO DE INGRESO SALIDA DE MATERIAL -------*/
   	/*--- Buscador ---*/
   	$.fn.search_ingreso=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_ingreso($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_ingreso($(this));
 			}
		 });
		 $(this).on("mousewheel",function(e){
			if($(this).prop("tagName")!="FORM"){
				e.preventDefault();
			}
		});
 		function search_ingreso(e){
			e.reset_input();
			e.search_elements();
 		}
	}
   	$.fn.view_ingreso=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_ingreso($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_ingreso($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_ingreso($(this));
	    	event.preventDefault();
	    });
 		function view_ingreso(e){
	   		var atrib3=new FormData();
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('can',$("#s_can").val());
			atrib3.append('uni',$("#s_uni").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n("../../material/view_ingreso/"+aid,atrib3,controls);
			$(this).blur_all("");
 		}
   	}
   	/*--- End Buscador ---*/
/*------- END MANEJO DE INGRESO SALIDA DE MATERIAL -------*/
/*------- SALIDA DE MATERIAL -------*/
   	/*--- Buscador ---*/
   	$.fn.search_salida=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
				search_salida($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_salida($(this));
 			}
		 });
		$(this).on("mousewheel",function(e){
			if($(this).prop("tagName")!="FORM"){
				e.preventDefault();
			}
		});
 		function search_salida(e){
			 e.reset_input();
			 e.search_elements();
 		}
	}
   	$.fn.view_salida=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_salida($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_salida($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_salida($(this));
	    	event.preventDefault();
	    });
 		function view_salida(e){
	   		var atrib3=new FormData();
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('can',$("#s_can").val());
			atrib3.append('uni',$("#s_uni").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n("../../material/view_salida/"+aid,atrib3,controls);
			$(this).blur_all("");
 		}
   	}
   	/*--- End Buscador ---*/
/*------- END SALIDA DE MATERIAL -------*/
	$.fn.save_movimiento=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					save_movimiento($(this));
 				}
 			}
 		});
	    $(this).submit(function(event){
	    	save_movimiento($(this));
	    	event.preventDefault();
	    });
	    function save_movimiento(e){
	    	var idam=e.data("am");
	    	var type=e.data("type");
	    	var item=e.data("item");
	    	if(idam!=undefined && type!=undefined && item!=undefined){
				var sto=$("#c"+idam).val();
				var fech=$("#fech"+idam).val();
				var can=$("#can"+idam).val();
				var obs=$("#obs"+idam).val();
				if(fech!=""){
					if(decimal(can,9,7) && can>0){
						if(textarea(obs,0,300)){
							var control=true;
							if(type=="salida"){
								var emp=$("#emp"+idam).val();
								if(!entero(emp,0,10)){ alerta("Seleccione un empleado","top","emp"+idam); control=false;}
								if((sto*1)<(can*1)){ msj("Stock insuficiente, verifique la cantidad de salida...!"); control=false;}
							}
							if(control){
								var atrib=new FormData();
								atrib.append('idam',idam);
								atrib.append('sto',sto);
								atrib.append('fech',fech);
								atrib.append('can',can);
								atrib.append('obs',obs);
								if(type=="salida"){ atrib.append('emp',emp);}
								var atrib3=new FormData();
								atrib3.append('idam',idam);
								atrib3.append('nom',$("#s_nom").val());
								atrib3.append('can',$("#s_can").val());
								atrib3.append('uni',$("#s_uni").val());
								atrib3.append('item',item);
								atrib3.append('type',type);
								var controls1=JSON.stringify({type:"set",preload:true});
								var controls2=JSON.stringify({id:"tr-"+idam,refresh:false,type:"html"});
								$(this).set("../../material/save_movimiento/"+aid,atrib,controls1,"../../material/tr_material/"+aid,atrib3,controls2);
							}
						}else{
							alerta("Ingrese un contenido válido entre 9999999999.9999999","top","obs"+idam);
						}
					}else{
						alerta("Ingrese un cantidad válida mayor a cero...","top","can"+idam);
					}
				}else{
					alerta("Ingrese una fecha valida","top","fech"+idam);
				}
	    	}
	    }
	}
/*------- MANEJO DE CONFIGURACIONES -------*/
   	/*--- Manejo de unidad ---*/
	$.fn.detalle_unidad=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				detalle_unidad($(this));
 			}
 		});
 		function detalle_unidad(e){
 			if(e.data("u")!=undefined){
 				modal("Detalle de unidad","xs","1");
	 			btn_modal('',"",'',"",'1');
				var atrib=new FormData();
				atrib.append("u",e.data("u"));
	 			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
	 			e.get_1n("../../material/detalle_unidad/"+aid,atrib,controls);
 			}
 		}
	}
	$.fn.update_unidad=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				update_unidad($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	update_unidad($(this));
	    	event.preventDefault();
	    });
 		function update_unidad(e){
 			var idu=e.data("u");
 			if(idu!=undefined){
				var nom=$("#nom_u"+idu).val();
				var abr=$("#abr_u"+idu).val();
				var equ=$("#equ_u"+idu).val();
				var des=$("#des_equ"+idu).val();
				if(strSpace(nom,2,40)){
					if(strSpace(abr,1,8)){
						if(textarea(des,0,100)){
							var atrib=new FormData();
							atrib.append("idu",idu);
							atrib.append("nom",nom);
							atrib.append("abr",abr);
							atrib.append("equ",equ);
							atrib.append("des",des);
							var controls1=JSON.stringify({type:"set",preload:true});
							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							$(this).set("../../material/update_unidad/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
						}else{
							alerta("Ingrese un contenido válido","top","des_equ"+idu);
						}
					}else{
						alerta("Ingrese un abreviatura de unidad válido","top","abr_u"+idu);
					}
				}else{
					alerta("Ingrese un nombre de unidad válido","top","nom_u"+idu);
				}
 			}
 		}
	}
	$.fn.confirm_unidad=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_unidad($(this));
 			}
 		});
 		function confirm_unidad(e){
 			if(e.data("u")!=undefined){
 				modal("Eliminar","xs","5");
 				var atr="this|"+JSON.stringify({u:e.data("u")});
	   			btn_modal('drop_unidad',atr,'',"",'5');
				var atrib=new FormData();
				atrib.append("u",e.data("u"));
	 			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
	 			e.get_1n("../../material/confirm_unidad/"+aid,atrib,controls);
 			}
 		}
	}
	$.fn.drop_unidad=function(){
 		if($(this).data("u")!=undefined){
 			var atrib=new FormData();
			atrib.append("u",$(this).data("u"));
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set("../../material/drop_unidad/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
			//set("../../material/drop_unidad",atrib,'NULL',true,'../../material/view_config/',{},'contenido',true,'5');
 		}
	}
   	$.fn.save_unidad=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				save_unidad($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	save_unidad($(this));
	    	event.preventDefault();
	    });
 		function save_unidad(e){
			var nom=$("#nom_u").val();
			var abr=$("#abr_u").val();
			var equ=$("#equ_u").val();
			var des=$("#des_equ").val();
			if(strSpace(nom,2,40)){
				if(strSpace(abr,1,8)){
					if(textarea(des,0,100)){
						var atrib=new FormData();
						atrib.append("nom",nom);
						atrib.append("abr",abr);
						atrib.append("equ",equ);
						atrib.append("des",des);
						var controls1=JSON.stringify({type:"set",preload:true});
						var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						e.set("../../material/save_unidad/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
					}else{
						alerta("Ingrese un contenido válido","top","des_equ");
					}
				}else{
					alerta("Ingrese un abreviatura de unidad válido","top","abr_u");
				}
			}else{
				alerta("Ingrese un nombre de unidad válido","top","nom_u");
			}
 		}
	}
   	/*--- End Manejo de unidad ---*/
   	/*--- End Manejo de color ---*/
	$.fn.detalle_color=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				detalle_color($(this));
 			}
 		});
 		function detalle_color(e){
 			if(e.data("c")!=undefined){
 				modal("Detalle de color","xs","1");
	 			btn_modal('',"",'',"",'1');
				var atrib=new FormData();
				atrib.append("c",e.data("c"));
	 			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
	 			e.get_1n("../../material/detalle_color/"+aid,atrib,controls);
 			}
 		}
	}
	$.fn.update_color=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				update_color($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	update_color($(this));
	    	event.preventDefault();
	    });
 		function update_color(e){
 			if(e.data("c")!=undefined){
				var nom=$("#nom_c"+e.data("c")).val();
				var cod=$("#cod_c"+e.data("c")).val();
				var abr=$("#abr_c"+e.data("c")).val();
				if(strSpace(nom,2,50)){
					if(cod!=""){
						if(strNoSpace(abr,0,10)){
							var atrib=new FormData();
							atrib.append("c",e.data("c"));
							atrib.append("nom",nom);
							atrib.append("cod",cod);
							atrib.append("abr",abr);
							var controls1=JSON.stringify({type:"set",preload:true});
							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							e.set("../../material/update_color/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
							//set("../../material/update_color",atrib,'NULL',true,'../../material/view_config/',{},'contenido',true);
						}else{
							alerta("Ingrese un valor válido","top","abr_c"+e.data("c"));
						}
					}else{
						alerta("Ingrese un color válido","top","cod_c"+e.data("c"));
					}
				}else{
					alerta("Ingrese un nombre de color válido","top","nom_c"+e.data("c"));
				}
 			}
 		}
	}
	$.fn.confirm_color=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_color($(this));
 			}
 		});
 		function confirm_color(e){
 			if(e.data("c")!=undefined){
 				modal("Eliminar","xs","5");
 				var atr="this|"+JSON.stringify({c:e.data("c")});
	   			btn_modal('drop_color',atr,'',"",'5');
				var atrib=new FormData();
				atrib.append("c",e.data("c"));
	 			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
	 			e.get_1n("../../material/confirm_color/"+aid,atrib,controls);
 			}
 		}
	}
	$.fn.drop_color=function(){
 		if($(this).data("c")!=undefined){
 			var atrib=new FormData();
			atrib.append("c",$(this).data("c"));
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set("../../material/drop_color/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
			//set("../../material/drop_unidad",atrib,'NULL',true,'../../material/view_config/',{},'contenido',true,'5');
 		}
	}
   	$.fn.save_color=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				save_color($(this));
 			}
 		});
 		function save_color(e){
			var nom=$("#nom_c").val();
			var cod=$("#cod_c").val();
			var abr=$("#abr_c").val();
			if(strSpace(nom,2,50)){
				if(cod!=""){
					if(strNoSpace(abr,0,10)){
						var atrib=new FormData();
						atrib.append("nom",nom);
						atrib.append("cod",cod);
						atrib.append("abr",abr);
						var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
						var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						$(this).set("../../material/save_color/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
						//set("../../material/save_color",atrib,'NULL',true,'../../material/view_config/',{},'contenido',true);
					}else{
						alerta("Ingrese un valor válido","top","abr_c");
					}
				}else{
					alerta("Ingrese un color válido","top","cod_c");
				}
			}else{
				alerta("Ingrese un nombre de color válido","top","nom_c");
			}
 		}
	}
   	/*--- End Manejo de color ---*/  
	/*--- Manejo de grupo ---*/
	$.fn.detalle_grupo=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				detalle_grupo($(this));
 			}
 		});
 		function detalle_grupo(e){
 			if(e.data("g")!=undefined){
 				modal("Detalle de grupo","xs","1");
	 			btn_modal('',"",'',"",'1');
				var atrib=new FormData();
				atrib.append("g",e.data("g"));
	 			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
	 			e.get_1n("../../material/detalle_grupo/"+aid,atrib,controls);
 			}
 		}
	}
	$.fn.update_grupo=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				update_grupo($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	update_grupo($(this));
	    	event.preventDefault();
	    });
 		function update_grupo(e){
 			if(e.data("g")!=undefined){
				var nom=$("#nom_g"+e.data("g")).val();
				var des=$("#des_g"+e.data("g")).val();
				var abr=$("#abr_g"+e.data("g")).val();
				if(strSpace(nom,2,50)){
					if(textarea(des,0,100)){
						if(strNoSpace(abr,0,10)){
							var atrib=new FormData();
							atrib.append("g",e.data("g"));
							atrib.append("nom",nom);
							atrib.append("abr",abr);
							atrib.append("des",des);
							var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							$(this).set("../../material/update_grupo/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
						}else{
							alerta("Ingrese un valor válido","top","abr_g"+e.data("g"));
						}
					}else{
						alerta("Ingrese una descripción de grupo valida","top","des_g"+e.data("g"));
					}
				}else{
					alerta("Ingrese un nombre de grupo válido","top","nom_g"+e.data("g"));
				}
 			}
 		}
	}
	$.fn.confirm_grupo=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_grupo($(this));
 			}
 		});
 		function confirm_grupo(e){
 			if(e.data("g")!=undefined){
 				modal("Eliminar","xs","5");
 				var atr="this|"+JSON.stringify({g:e.data("g")});
	   			btn_modal('drop_grupo',atr,'',"",'5');
				var atrib=new FormData();
				atrib.append("g",e.data("g"));
	 			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
	 			e.get_1n("../../material/confirm_grupo/"+aid,atrib,controls);
 			}
 		}
	}
	$.fn.drop_grupo=function(){
 		if($(this).data("g")!=undefined){
 			var atrib=new FormData();
			atrib.append("g",$(this).data("g"));
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set("../../material/drop_grupo/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
 		}
	}
   	$.fn.save_grupo=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				save_grupo($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	save_grupo($(this));
	    	event.preventDefault();
	    });
 		function save_grupo(e){
			var nom=$("#nom_g").val();
			var des=$("#des_g").val();
			var abr=$("#abr_g").val();
			if(strSpace(nom,2,50)){
				if(textarea(des,0,100)){
					if(strNoSpace(abr,0,10)){
						var atrib=new FormData();
						atrib.append("nom",nom);
						atrib.append("abr",abr);
						atrib.append("des",des);
						var controls1=JSON.stringify({type:"set",preload:true});
						var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						$(this).set("../../material/save_grupo/"+aid,atrib,controls1,"../../material/view_config/"+aid,atrib,controls2);
					}else{
						alerta("Ingrese un valor válido","top","abr_g");	
					}
				}else{
					alerta("Ingrese una descripción de grupo valida","top","des_g");
				}
			}else{
				alerta("Ingrese un nombre de grupo válido","top","nom_g");
			}
 		}
	}
	/*--- End Manejo de grupo ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/
})(jQuery);
function update_material(e){e.update_material();}
function drop_material(e){e.drop_material();}
function save_material(e){e.save_material();}
function drop_unidad(e){e.drop_unidad();}
function drop_color(e){e.drop_color();}
function drop_grupo(e){e.drop_grupo();}
var aid='';
function inicio(v){
 	aid=v;
	$("a#material").click(function(){$(this).get_2n('../../material/search_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_material/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('material','Materiales',aid+'?p=1');});
	$("div#material").click(function(){$(this).get_2n('../../material/search_material',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_material/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('material','Materiales',aid+'?p=1');});
	$("a#ingreso").click(function(){$(this).get_2n('../../material/search_ingreso',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_ingreso/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('ingreso','Ingreso de materiales',aid+'?p=2');});
	$("div#ingreso").click(function(){$(this).get_2n('../../material/search_ingreso',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_ingreso/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('ingreso','Ingreso de materiales',aid+'?p=2');});
	$("a#salida").click(function(){$(this).get_2n('../../material/search_salida',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_salida/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('salida','Salida de materiales',aid+'?p=3');});
	$("div#salida").click(function(){$(this).get_2n('../../material/search_salida',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_salida/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('salida','Salida de materiales',aid+'?p=3');});
	$("a#config").click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_config/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('config','Configuración de materiales',aid+'?p=5');});
	$("div#config").click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),"../../material/view_config/"+aid,{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('config','Configuración de materiales',aid+'?p=5');});
}