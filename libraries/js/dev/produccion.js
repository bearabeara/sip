var x=$(document);
x.ready(inicio);
(function($){
/*------- MANEJO DE PRODUCTOS -------*/
	$.fn.reset_input=function(){
	 	var id=$(this).attr("id");
		if(id!="s_cod"){$("#s_cod").val("");}
		if(id!="s_cod2"){$("#s_cod2").val("");}
		if(id!="s_nom"){$("#s_nom").val("");}
		if(id!="s_fec"){$("#s_fec").val("");}
		//caso produccion
		if(id!="sp_num"){$("#sp_num").val("");}
		if(id!="sp_nom"){$("#sp_nom").val("");}
		if(id!="sp_cli"){$("#sp_cli").val("");}
		if(id!="sp_est"){$("#sp_est").val("");}
	}
	$.fn.reset_input_2=function(){
		if($(this).attr("id")!="s2_gru"){$("#s2_gru").val("");}
		if($(this).attr("id")!="s2_abr"){$("#s2_abr").val("");}
	}
	$.fn.search_produccion=function(){
	 	$(this).keyup(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			search_produccion($(this));
	 		}
	 	});
	 	$(this).change(function(){
	 		if($(this).prop("tagName")!="FORM"){
				search_produccion($(this));
			}
		});
		$(this).on("mousewheel",function(e){
			if($(this).prop("tagName")!="FORM"){
				e.preventDefault();
			}
		});
	 	function search_produccion(e){
	 		if(e.data("module")==undefined){
				e.reset_input();
				e.search_elements();
	 		}else{
	 			if(e.data("module")=="2"){
	 				e.reset_input_2();
	 			}
	 		}
	 	}
	}
   	/*--- Buscador ---*/
	$.fn.view_producto=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_producto($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_producto($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_producto($(this));
	    	event.preventDefault();
	    });
 		function view_producto(e){
	   		var atrib3=new FormData();
	   		atrib3.append('cod',$("#s_cod").val());
	   		atrib3.append('cod2',$("#s_cod2").val());
	   		atrib3.append('nom',$("#s_nom").val());
	   		atrib3.append('fec',$("#s_fec").val());
	   		atrib3.append('est',$("#s_est").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n("produccion/view_producto",atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
    $.fn.change_estado=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				change_estado($(this));
 			}
 		});
 		function change_estado(e){
	      	if(e.data("p")!=undefined){
	      		var atrib=new FormData();atrib.append("p",e.data("p"));
		   		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
	 			var controls1=JSON.stringify({type:"set",preload:false});
	 			var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 			e.set('produccion/change_estado',atrib,controls1,'produccion/view_producto',atrib3,controls2);
	      	}
 		}
    }
   	/*---- Reportes ----*/
	$.fn.view_reportes=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_reportes($(this));
 			}
 		});
 		function view_reportes(e){
 			if(e.data("p")!=undefined){
			 	modal("PRODUCTO: Detalle","lg","1");
			 	var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
			 	btn_modal('',"",'print_aux',atr,'1');
			 	var atrib=new FormData();
			 	atrib.append('p',e.data("p"));
			 	if(e.data("pgc")!=undefined){
			 		atrib.append("pgc",e.data("pgc"));
			 	}
				var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			 	$(this).get_1n('produccion/ficha_tecnica',atrib,controls);
 			}
 		}
	}
	/*---- End Reportes ----*/
   	/*--- Configuracion ---*/
   		/*-Modificar producto-*/
		$.fn.config_producto=function(){
			$(this).click(function(){
				if($(this).prop("tagName")!="FORM"){
					config_producto($(this));
				}
			});
			function config_producto(e){
				if(e.data("p")!=undefined){
					modal("PRODUCTO: Configuración","lg","11");
				 	var atr="this|"+JSON.stringify({p:e.data("p")});
					btn_modal('update_producto',atr,'','','11');
					var ele=new FormData();ele.append('idp',e.data("p"));
					var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
					$(this).get_1n('produccion/config_producto',ele,controls);
				}
			}
		}
		$.fn.update_producto=function(){
			if($(this).data("p")!=undefined){
			 	var cod=$('#cod_1').val();
			 	var nom=$('#nom').val();
			 	var fec=$('#fec').val();
			 	var obs=$('#obs').val();
			 	var cod2=$('#cod_2').val();
		 		if(strNoSpace(cod,2,20)){
					if(strSpace(nom,2,90)){
						var control=true;
						if(fec!=""){if(!fecha(fec)){ control=false; alerta('Ingree una fecha válida','top','fec');}}
						if(obs!=""){ if(!textarea(obs,0,500)){ control=false;alerta('Ingrese una valor válido','top','obs');}}
						if(cod2!=""){ if(!strSpace(cod2,0,20)){ control=false;alerta('Ingrese una valor válido','top','cod_2');}}
						$("#form_atr textarea.form-control").each(function(i,ele) {
							if($(ele).val()!=""){if(!textarea($(ele).val(),0,150)){ control=false;alerta('Ingrese una valor válido','top',$(ele).attr('id'));}}
						});
						if(control){
							var vec_n=[];
							var vec_u=[];
							$("#form_atr textarea.form-control").each(function(i,ele){
								if($(ele).data('typele')=="child"){
									vec_n[vec_n.length]={id:$(ele).data("a"), valor:$(ele).val()}
								}
								if($(ele).data('typele')=="db"){
									vec_u[vec_u.length]={id:$(ele).data("prod-atr"), valor:$(ele).val()}
								}
							});
							var atrib=new FormData();atrib.append('idp',$(this).data("p"));atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('fec',fec);atrib.append('obs',obs);atrib.append('cod2',cod2);
							atrib.append('atrib_n',JSON.stringify(vec_n));
							atrib.append('atrib_u',JSON.stringify(vec_u));
							atrib.append('atrib_d',$("#control-delete").val());
							var atrib3=new FormData();
							atrib3.append('cod',$("#s_cod").val());
							atrib3.append('cod2',$("#s_cod2").val());
							atrib3.append('nom',$("#s_nom").val());
							atrib3.append('fec',$("#s_fec").val());
							atrib3.append('est',$("#s_est").val());
							var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
		 					var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
	 						var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
		 					$(this).set_2n("produccion/update_producto",atrib,controls,"produccion/config_producto",atrib,controls2,"produccion/view_producto",atrib3,controls3);
					 		//set_2n('produccion/update_producto',atrib,'NULL',true,'produccion/config_producto',atrib,'content_11',true,'produccion/view_producto',atrib3,'contenido',false);
						}
					}else{
						alerta('Ingrese un nombre de producto válido','top','nom');
					}
				}else{
					alerta('Ingrese un código de producto válido','top','cod_1');
				}
			}
		}
		/*----End modificar producto----*/
		/*----Modificar categorias----*/
			/*Controles de producto grupo*/
			$.fn.categoria=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						categoria($(this));
					}
				});
				function categoria(e){
				 	modal("PRODUCTO: Configuración","lg","11");
					btn_modal('',"",'','','11');
					var ele=new FormData();ele.append('idp',e.data('p'));
					var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
					$(this).get_1n('produccion/categoria',ele,controls);
				}
			}
			$.fn.refresh_producto_grupo=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						refresh_producto_grupo($(this));
					}
				});
				function refresh_producto_grupo(e){
					var atrib=new FormData();atrib.append('pg',e.data("pg"));
			 		var controls=JSON.stringify({id:'titulo_grupo'+e.data("pg"),refresh:false,type:"html"});
					var controls2=JSON.stringify({id:'pg'+e.data("pg"),refresh:true,type:"html"});
			 		e.get_2n('produccion/titulo_producto_grupo',atrib,controls,'produccion/producto_grupo',atrib,controls2);
					//get_2n("produccion/titulo_producto_grupo",atrib,'titulo_grupo'+e.data("pg"),false,'produccion/producto_grupo',atrib,'pg'+e.data("pg"),true)
				}
			}
			$.fn.confirm_producto_grupo=function(e){
				$(this).click(function(){
			     	if($(this).prop("tagName")!="FORM"){
			     		confirm_producto_grupo($(this));
			     	}
				});
				function confirm_producto_grupo(e){
					if($("#datos-producto").data("p")!=undefined && e.data("pg")!=undefined){
						modal("Eliminar","xs","5");
						var atr="this|"+JSON.stringify({p:$("#datos-producto").data("p"),pg:e.data("pg")});
						btn_modal('drop_producto_grupo',atr,'',"",'5');
						var atrib=new FormData();atrib.append('pg',e.data("pg"));
						var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
						$(this).get_1n('produccion/confirm_producto_grupo',atrib,controls);
					}
				}
			}
			$.fn.drop_producto_grupo=function(){
				if($(this).data("p")!=undefined && $(this).data("pg")!=undefined){
					var atrib=new FormData();atrib.append('idp',$(this).data("p"));atrib.append('pg',$(this).data("pg"));
		   			var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
		 			var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			 		$(this).set_2n("produccion/drop_producto_grupo",atrib,controls,"produccion/categoria",atrib,controls2,"produccion/view_producto",atrib3,controls3);
					//set_2n("produccion/drop_producto_grupo",atrib,'NULL',true,'produccion/categoria',atrib,'content_11',false,'produccion/view_producto',atrib3,'contenido',true,"5");
				}
			}
			$.fn.save_producto_grupo_atr=function(){
				$(this).click(function(){
			     	if($(this).prop("tagName")!="FORM"){
			     		save_producto_grupo_atr($(this));
			     	}
				});
				function save_producto_grupo_atr(e){
					var control=true;
					$("#pga"+e.data("pg")+" textarea.form-control").each(function(i,ele){
						if($(ele).val()!=""){if(!textarea($(ele).val(),0,150)){ console.log($(ele).val());control=false; alerta('Ingrese una valor válido','top',$(ele).attr('id'));}}
					});
					if(control){
						var vec_n=[];
						var vec_u=[];
						$("#pga"+e.data("pg")+" textarea.form-control").each(function(i,ele){
							if($(ele).data('typele')=="child"){
								vec_n[vec_n.length]={id:$(ele).data("a"), valor:$(ele).val()}
							}
							if($(ele).data('typele')=="db"){
								vec_u[vec_u.length]={id:$(ele).data("pga"), valor:$(ele).val()}
							}
						});
						var atrib=new FormData();atrib.append('pg',e.data("pg"));atrib.append('atrib_n',JSON.stringify(vec_n));atrib.append('atrib_u',JSON.stringify(vec_u));atrib.append('atrib_d',$("#delete-grupo-atributo"+e.data("pg")).val());
			 			var controls1=JSON.stringify({type:"set",preload:true});
			 			var controls2=JSON.stringify({id:'pg'+e.data("pg"),refresh:false,type:"html"});
			 			$(this).set('produccion/save_producto_grupo_atr',atrib,controls1,'produccion/producto_grupo',atrib,controls2);
						//set("produccion/save_producto_grupo_atr",atrib,'NULL',true,'produccion/producto_grupo',atrib,'pg'+e.data("pg"),false);
					}
				}
			}
			/*End controles de producto grupo*/
			/*Controles colores*/
			$.fn.refresh_grupo_color=function(){
				$(this).click(function(){
			     	if($(this).prop("tagName")!="FORM"){
			     		refresh_grupo_color($(this));
			     	}
				});
				function refresh_grupo_color(e){
					var atrib=new FormData();atrib.append('pgc',e.data("pgc"));
			 		var controls=JSON.stringify({id:'titulo_color'+e.data("pgc"),refresh:false,type:"html"});
					var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:true,type:"html"});
			 		e.get_2n('produccion/titulo_grupo_color',atrib,controls,'produccion/grupo_color',atrib,controls2);
					//get_2n("produccion/titulo_grupo_color",atrib,'titulo_color'+e.data("pgc"),false,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),true);
				}
			}
			$.fn.confirm_grupo_color=function(){
				$(this).click(function(){
			     	if($(this).prop("tagName")!="FORM"){
			     		confirm_grupo_color($(this));
			     	}
				});
				function confirm_grupo_color(e){
					modal("Eliminar color","xs","5");
					clear_datas("btn_52");
					var atr="this|"+JSON.stringify({pg:e.data("pg"),pgc:e.data('pgc')});
				   	btn_modal('drop_grupo_color',atr,'',"",'5');
				   	var atrib=new FormData();atrib.append('pgc',e.data("pgc"));
					var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					e.get_1n('produccion/confirm_grupo_color',atrib,controls);
				   	//get('produccion/confirm_grupo_color',atrib,'content_5',true);
				}
			}
			$.fn.drop_grupo_color=function(){
				if($(this).data("pg")!=undefined && $(this).data("pgc")){
					var atrib=new FormData();atrib.append('pg',$(this).data("pg"));atrib.append('pgc',$(this).data("pgc"));
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:'pg'+$(this).data("pg"),refresh:false,type:"html"});
		 			var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			 		$(this).set_2n("produccion/drop_grupo_color",atrib,controls,"produccion/producto_grupo",atrib,controls2,"produccion/view_producto",atrib3,controls3);
					//set_2n("produccion/drop_grupo_color",atrib,'NULL',true,'produccion/producto_grupo',atrib,'pg'+e.data("pg"),false,'produccion/view_producto',atrib3,'contenido',true,'5');
				}
			}
			$.fn.save_producto_grupo_color_atr=function(){
				$(this).click(function(){
			     	if($(this).prop("tagName")!="FORM"){
			     		save_producto_grupo_color_atr($(this));
			     	}
				});
				function save_producto_grupo_color_atr(e){
					var control=true;
					$("#pgc-atr"+e.data("pgc")+" textarea.form-control").each(function(i,ele){
						if($(ele).val()!=""){ if(!textarea($(ele).val(),0,150)){ console.log($(ele).val());control=false; alerta('Ingrese una valor válido','top',$(ele).attr('id'));}}
					});
					if(control){
						var vec_n=[];
						var vec_u=[];
						$("#pgc-atr"+e.data("pgc")+" textarea.form-control").each(function(i,ele){
							if($(ele).data('typele')=="child"){
								vec_n[vec_n.length]={id:$(ele).data("a"), valor:$(ele).val()}
							}
							if($(ele).data('typele')=="db"){
								vec_u[vec_u.length]={id:$(ele).data("pgca"), valor:$(ele).val()}
							}
						});
						var atrib=new FormData();atrib.append('pgc',e.data("pgc"));
						atrib.append('atrib_n',JSON.stringify(vec_n));atrib.append('atrib_u',JSON.stringify(vec_u));atrib.append('atrib_d',$("#delete-color"+e.data("pgc")).val());
						var controls1=JSON.stringify({type:"set",preload:true});
			 			var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
			 			$(this).set('produccion/save_producto_grupo_color_atr',atrib,controls1,'produccion/grupo_color',atrib,controls2);
						//set("produccion/save_producto_grupo_color_atr",atrib,'NULL',true,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false);
					}
				}
			}
			/*End controles colores*/
		/*Manejo de colores en el de grupos de producto*/
			/*Manejo de imagenes en el color del producto*/
			/*	function change_producto_color_imagen(e){// en uso
					var atrib=new FormData();atrib.append('pi',e.data("pi"));atrib.append("pgc",$("#datos-pgc").data("pgc"));
					var atrib2=new FormData();atrib2.append('pgc',$("#datos-pgc").data("pgc"));
					set_button('produccion/change_producto_color_imagen',atrib,"produccion/producto_grupo_color_imagen",atrib2,"pgc-img"+$("#datos-pgc").data("pgc"),false,e);
				}
				function drop_color_imagen(e){// en uso
					var atrib=new FormData();atrib.append('pgc',e.data("pgc"));atrib.append('i',e.data("i"));
					set("produccion/drop_color_imagen",atrib,'NULL',true,'produccion/producto_grupo_color_imagen',atrib,"pgc-img"+e.data("pgc"),false);
				}
			/*End Manejo de imagenes en el color del producto*/
				 
				/*Manejo de atributos en los colores del grupo*/
					
				/*End manejo de atributos en los colores del grupo*/
			 /*End Manejo de colores en el de grupos de producto*/
		/*----End modificar categorias----*/
	/*--- End Configuracion ---*/




















   	/*--- Nuevo ---*/

	$.fn.new_producto=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				new_producto($(this));
			}
		});
		function new_producto(e){
			modal("PRODUCTO: Nuevo","lg","1");
			btn_modal('save_producto',"this",'',"",'1');
			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			$(this).get_1n('produccion/new_producto',{},controls);
		}
	}
	 $.fn.save_producto=function(){
	 	var cod=$('#cod_1').val();
	 	var nom=$('#nom').val();
	 	var fec=$('#fec').val();
	 	var obs=$('#obs').val();	
		var cod2=$('#cod_2').val();
	 	if(strNoSpace(cod,2,20)){
			if(strSpace(nom,2,90)){
				var control=true;
				if(fec!=""){if(!fecha(fec)){ control=false; alerta('Ingrese una fecha válida','top','fec');}}
				if(obs!=""){ if(!textarea(obs,0,500)){ control=false;alerta('Ingrese una valor válido','top','obs');}}
				if(cod2!=""){ if(!strSpace(cod2,0,20)){ alerta('Ingrese un código de producto','top','cod_2'); control=false;}}
				$("#form_atr textarea.form-control").each(function(i,ele){
					if($(ele).val()!=""){if(!textarea($(ele).val(),0,150)){ control=false;alerta('Ingrese una valor válido no vacio','top',$(ele).attr('id'));}}
				});
				if(control){
					var vec=[];
					$("#form_atr textarea.form-control").each(function(i,ele){vec[vec.length]={id:$(ele).data("a"), valor:$(ele).val()}});
					var controls=JSON.stringify({type:"set",preload:true,closed:"1"});
					var atrib=new FormData();atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('fec',fec);atrib.append('obs',obs);atrib.append('atrib',JSON.stringify(vec));atrib.append("cod2",cod2);
					var controls1=JSON.stringify({id:"contenido",refresh:true,type:"html"});
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					$(this).set("produccion/save_producto",atrib,controls,'produccion/view_producto',atrib3,controls1);
				}
			}else{
				alerta('Ingrese un nombre de producto válido','top','nom');
			}
		}else{
			alerta('Ingrese un código de producto','top','cod_1');
		}
	 }
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_producto=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_producto($(this));
 			}
 		});
 		function print_producto(e){
 			if(e.data("tbl")!=undefined){
 				modal("PRODUCTOS: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("p")!=undefined){
		 				visibles[visibles.length]=$(tr).data("p");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		   		atrib.append('cod',$("#s_cod").val());
		   		atrib.append('cod2',$("#s_cod2").val());
		   		atrib.append('nom',$("#s_nom").val());
		   		atrib.append('fec',$("#s_fec").val());
		   		atrib.append('est',$("#s_est").val());
		   		atrib.append('tbl',e.data("tbl"));
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('produccion/print_producto',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
/*---- END MANEJO DE PRODUCTOS ----*/
/*---- FUNCIONES GENERICAS ----*/
	/*Manejo de atributos en el producto*/
	$.fn.atributos=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				atributos($(this));
			}
		});
		function atributos(e){
			modal("ATRIBUTO: Adicionar","sm","2");
			btn_modal('',"",'',"",'2');
			var atrib=new FormData();
			atrib.append('type',e.data("type"));
			if(e.data("type")=="grupo"){atrib.append('pg',e.data("pg"));}
			if(e.data("type")=="color"){atrib.append('pgc',e.data("pgc"));}
			var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
			var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
			e.get_2n('produccion/search_atributo',atrib,controls,'produccion/view_atributo',{},controls2);
		}
	}
	$.fn.search_atributo=function(){
		var atrib3=new FormData();atrib3.append('nom',$("#s2_atr").val());
		var controls=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
		$(this).get_1n('produccion/view_atributo',atrib3,controls);
	}
	$.fn.all_atributo=function(){
		$("#s2_atr").val("");
		$(this).search_atributo();
	}
	$.fn.new_atributo=function(){
		modal("ATRIBUTO: Nuevo","sm","3");
		btn_modal('save_atributo',"this",'',"",'3');
		var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
		$(this).get_1n('produccion/new_atributo',{},controls);
	}
	$.fn.save_atributo=function(){
		var nom=$("#n_atrib").val();
		if(strSpace(nom,2,100)){
			var atrib=new FormData(); atrib.append('nom',nom);
			var atrib3=new FormData();atrib3.append('nom',$("#s2_atr").val());
 			var controls1=JSON.stringify({type:"set",preload:false, closed:"3"});
 			var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
 			$(this).set('produccion/save_atributo',atrib,controls1,'produccion/view_atributo',atrib3,controls2);
		}else{
			alerta("Ingrese un contenido válido.","top","n_atrib");
		}
	}
	$.fn.update_atributo=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				update_atributo($(this));
			}
		});
		$(this).submit(function(event){
			update_atributo($(this));
			event.preventDefault();
		});
		function update_atributo(e){
			if(e.data("a")!=undefined){
				var nom=$("#atr"+e.data("a")).val();
				if(strSpace(nom,2,100)){
					var atrib=new FormData();atrib.append('id',e.data("a"));atrib.append('nom',nom);
					var atrib3=new FormData();atrib3.append('nom',$("#s2_atr").val());
		 			var controls1=JSON.stringify({type:"set",preload:true, closed:"3"});
		 			var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
		 			e.set('produccion/update_atributo',atrib,controls1,'produccion/view_atributo',atrib3,controls2);
				}else{
					alerta("Ingrese un contenido válido.","top","atr"+e.data("a"));
				}
			}
		}
	}
	$.fn.confirm_atributo=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirm_atributo($(this));
			}
		});
		function confirm_atributo(e){
			modal("Eliminar","xs","5");
			var atr="this|"+JSON.stringify({a:e.data("a")});
		   	btn_modal('drop_atributo',atr,'',"",'5');
		   	var atrib=new FormData();atrib.append('id',e.data("a"));
		   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
		   	$(this).get_1n('produccion/confirm_atributo',atrib,controls);
		}
	}
	$.fn.drop_atributo=function(){
		if($(this).data("a")!=undefined){
			var atrib=new FormData();atrib.append("id",$(this).data("a"));
			var atrib3=new FormData();atrib3.append('nom',$("#s2_atr").val());
			var controls1=JSON.stringify({type:"set",preload:true, closed:"5"});
			var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
			$(this).set('produccion/drop_atributo',atrib,controls1,'produccion/view_atributo',atrib3,controls2);
		}
	}
	/*End manejo de atributos en el producto*/
	/*Manejo de atributos*/
	$.fn.adicionar_atributo=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				adicionar_atributo($(this));
			}
		});
		function adicionar_atributo(e){
	 		var atrib=new FormData();atrib.append('id',e.data("a"));atrib.append('type',$("#s2_atr").data("type"));
	 		if($("#s2_atr").data("type")=="grupo"){
	 			atrib.append('pg',$("#s2_atr").data("pg"));
	 		}
	 		if($("#s2_atr").data("type")=="color"){
	 			atrib.append('pgc',$("#s2_atr").data("pgc"));
	 		}
			$.ajax({
			    url:"produccion/adicionar_atributo",
			    async:true,
			    type: "POST",
			    contentType:false,
			    data:atrib,
			    processData:false,//Debe estar en false para que JQuery no procese los datos a enviar
			    timeout:9000,
			    beforeSend: function(){},
			    success: function(result){ 
			    	if(result!="logout" && result!="error" && result!="fail"){
			    		if($("#s2_atr").data("type")=="producto"){
			    			$("#form_atr").append(result);
				    		$("#form_atr div.col-sm-4").each(function(i,el){
				    			if((i+1)%2==0){
				    				$(el).next().attr('class','clearfix');
				    			}else{
				    				$(el).next().removeAttr('class');
				    			}
				    		});
			    		}
			    		if($("#s2_atr").data("type")=="grupo"){
			    			$("#pga"+$("#s2_atr").data("pg")).append(result);
				    		$("#pga"+$("#s2_atr").data("pg")+" div.col-sm-4").each(function(i,el){
				    			if((i+1)%2==0){
				    				$(el).next().attr('class','clearfix');
				    			}else{
				    				$(el).next().removeAttr('class');
				    			}
				    		});	
			    		}
			    		if($("#s2_atr").data("type")=="color"){
			    			$("#pgc-atr"+$("#s2_atr").data("pgc")).append(result);
				    		$("#pgc-atr"+$("#s2_atr").data("pgc")+" div.col-sm-4").each(function(i,el){
				    			if((i+1)%2==0){
				    				$(el).next().attr('class','clearfix');
				    			}else{
				    				$(el).next().removeAttr('class');
				    			}
				    		});	
			    		}
			    		$('[data-toggle="popover"]').popover({html:true});
			    	}else{
			    		msj(result);
			    	}
			    },
			    error:function(){ problemas("")}
			});
		}
	 }
	$.fn.eliminar_atributo=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				eliminar_atributo($(this));
			}
		});
		function eliminar_atributo(e){
	 		var textarea=e.prev().prev();
	 		if(textarea.data('typele')=="db"){
	 			var v="";
	 			if(textarea.data('type')==undefined){// es producto atributo
		 			var id=textarea.data('prod-atr');
		 			var content=$("input#control-delete").val();
		 			if(content!=""){
		 				var rec=content.split("|");
		 				for (var i = 0; i < rec.length; i++){ v+=rec[i]+"|"; };
		 				v+=id;
		 			}else{
		 				v=id;
		 			}
		 			$("input#control-delete").val(v);
	 			}else{
	 				if(textarea.data('type')=="grupo"){
			 			var id=textarea.data('pga');
			 			var idpg=textarea.data('pg');
			 			var content=$("input#delete-grupo-atributo"+idpg).val();
			 			if(content!=""){
			 				var rec=content.split("|");
			 				for (var i = 0; i < rec.length; i++){ v+=rec[i]+"|"; };
			 				v+=id;
			 			}else{
			 				v=id;
			 			}
			 			$("input#delete-grupo-atributo"+idpg).val(v);
	 				}else{
		 				if(textarea.data('type')=="color"){
				 			var id=textarea.data('pgca');
				 			var idpgc=textarea.data('pgc');
				 			var content=$("input#delete-color"+idpgc).val();
				 			if(content!=""){
				 				var rec=content.split("|");
				 				for (var i = 0; i < rec.length; i++){ v+=rec[i]+"|"; };
				 				v+=id;
				 			}else{
				 				v=id;
				 			}
				 			$("input#delete-color"+idpgc).val(v);
		 				}
	 				}
	 			}
	 		}
	 		var div=e.parent().parent();
	 		var label=div.prev();
	 		var span=div.next();
	 		div.remove();label.remove();span.remove();
	 		var id="";
	 		if(textarea.data('type')=="producto"){
	 			id="#form_atr";
			}else{
				if(textarea.data('type')=="grupo"){
					id="#pga"+textarea.data("pg");
				}else{
					if(textarea.data('type')=="color"){
						id="#pgc-atr"+textarea.data("pgc");
					}else{
						if(textarea.data('type')==undefined){
							id="#form_atr";
						}
					}
				}
			}
			if(id!=""){
				$(id+" div.col-sm-4").each(function(i,el){
					if((i+1)%2==0){
					   	$(el).next().attr('class','clearfix');
					}else{
					   	$(el).next().removeAttr('class');
					}
				});
			}
		}
	}
	 /*End manejo de atributos*/
		/*--- Historiales de cambios ---*/
		$.fn.producto_seg=function(){
		    $(this).click(function(){
		    	if($(this).prop("tagName")!="FORM"){
		    		producto_seg($(this));
		    	}
		    });
		    function producto_seg(e){
		    	if(e.data("type")!=undefined){
					var titulo="Cambios generales en el producto";
					switch($(this).data("type")){
						case "producto": titulo="Cambios en el producto"; break;
						case "producto_fotografia": titulo="Cambios en fotografías"; break;
						case "pm-modal": titulo="Cambios en materiales"; break;
						case "pf-modal": titulo="Cambios en fotografías"; break;
						case "ppi-modal": titulo="Cambios en piezas"; break;
					}
					modal(titulo,"md","4");
					btn_modal('',"",'',"",'4');
					var atrib=new FormData();atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
					var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
					$(this).get_1n('produccion/producto_seg',atrib,controls);
				}
		    }
		}
		$.fn.producto_grupo_color_seg=function(){
		    $(this).click(function(){
		    	if($(this).prop("tagName")!="FORM"){
		    		producto_grupo_color_seg($(this));
		    	}
		    });
		    function producto_grupo_color_seg(e){
		    	if((e.data("pg")!=undefined || e.data("pgc")!=undefined) && e.data("type")!=undefined){
			    	var titulo="Cambios generales en el producto";
					var atrib=new FormData();
					switch(e.data("type")){
						case "producto_grupo": titulo="Cambios en la categoria"; atrib.append('pg',e.data("pg")); break;
						case "pgm-modal": titulo="Cambios en los materiales de la categoria"; atrib.append('pg',e.data("pg")); break;
						case "producto_grupo_color": titulo="Cambios en el color"; atrib.append('pg',e.data("pg")); atrib.append('pgc',e.data("pgc"));break;
						case "pgcm-modal": titulo="Cambios en los materiales del color"; atrib.append('pgc',e.data("pgc"));break;
						case "pgcf-modal": titulo="Cambios en las fotografías del color"; atrib.append('pgc',e.data("pgc"));break;
						case "pgcpi-modal": titulo="Cambios en las piezas del color"; atrib.append('pgc',e.data("pgc"));break;
						case "producto_grupo_complet": titulo="Cambios en el grupo y color"; atrib.append('pg',e.data("pg")); atrib.append('pgc',e.data("pgc"));break;
					}
					modal(titulo,"md","4");
					btn_modal('',"",'',"",'4');
					atrib.append('p',$("#datos-producto").data("p")); atrib.append('type',e.data("type"));
					var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
					e.get_1n('produccion/producto_grupo_color_seg',atrib,controls);
		    	}
		    }
		}
		/*--- End historiales de cambios ---*/


	/*--- Materiales en el producto ---*/
		$.fn.producto_materiales=function(){
		    $(this).click(function(){
		    	if($(this).prop("tagName")!="FORM"){
		    		producto_materiales($(this));
		    	}
		    });
		    function producto_materiales(e){
				var atrib=new FormData();
				var tit="";
				if(e.data("type")=="pm" || e.data("type")=="pm-modal"){ tit="PRODUCTO: Materiales generales"; }
				if(e.data("type")=="pgm" || e.data("type")=="pgm-modal"){ tit="CATEGORIA: Materiales generales"; atrib.append('pg',e.data("pg")); }
				if(e.data("type")=="pgcm" || e.data("type")=="pgcm-modal"){ tit="CATEGORIA COLOR: Materiales"; atrib.append('pgc',e.data("pgc")); }
				modal(tit,"xmd","2");
				btn_modal('',"",'','','2');
				atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
				$(this).get_2n('produccion/search_material_producto',atrib,controls,'produccion/view_material_producto',atrib,controls2);
		    }
		}
		$.fn.search_producto_materiales=function(){
		 	$(this).click(function(){
		 		if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
		 			search_producto_materiales($(this));
		 		}
		 	});
			$(this).submit(function(event){
				search_producto_materiales($(this));
				event.preventDefault();
			});
			function search_producto_materiales(e){
		 		if(e.data("type")!=undefined){
		 			if(e.data("type")=="all"){
		 				$("#s2_mat").val("");
		 			}
					var mat=$("#s2_mat").val();
					var atrib3=new FormData();atrib3.append('mat',mat);atrib3.append('p',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_mat").data("type"));
					if($("#s2_mat").data("type")=="pgm-modal"){ atrib3.append('pg',$("#s2_mat").data("pg"));}
					if($("#s2_mat").data("type")=="pgcm-modal"){ atrib3.append('pgc',$("#s2_mat").data("pgc"));}
					var controls=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
					e.get_1n('produccion/view_material_producto',atrib3,controls);
		 		}
			}
		}
		/*--Materiales--*/
			$.fn.material=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		material($(this));
			    	}
			    });
			    function material(e){
					var atrib=new FormData();
					var atrib2=new FormData();
					var tit="";
					var type="";
					var ancho="md"
					if(e.data("type")!=undefined){
						if(e.data("type")=="pm"){ tit="PRODUCTO: Nuevo material"; }
						if(e.data("type")=="pm-modal"){ tit="PRODUCTO: Nuevo material";}
						if(e.data("type")=="pm-modal-change"){ tit="PRODUCTO: Cambiar material";atrib.append('pm',e.data("pm"));atrib2.append('pm',e.data("pm"));}
						if(e.data("type")=="pgm"){ tit="CATEGORIA: Nuevo material"; atrib.append('pg',e.data("pg"));atrib2.append('pg',e.data("pg"));}
						if(e.data("type")=="pgm-modal"){ tit="CATEGORIA: Nuevo material";atrib.append('pg',$("#s2_mat").data("pg"));atrib2.append('pg',$("#s2_mat").data("pg"));}
						if(e.data("type")=="pgm-modal-change"){ tit="CATEGORIA: Cambiar material";atrib.append('pgm',e.data("pgm"));atrib2.append('pgm',e.data("pgm"));}
						if(e.data("type")=="pgcm"){ tit="CATEGORIA COLOR: Nuevo material"; atrib.append('pgc',e.data("pgc"));atrib2.append('pgc',e.data("pgc"));}
						if(e.data("type")=="pgcm-modal"){ tit="CATEGORIA COLOR: Nuevo material";atrib.append('pgc',$("#s2_mat").data("pgc"));atrib2.append('pgc',$("#s2_mat").data("pgc"));}
						if(e.data("type")=="pgcm-modal-change"){ tit="CATEGORIA COLOR: Cambiar material";atrib.append('pgcm',e.data("pgcm"));atrib2.append('pgcm',e.data("pgcm"));}
						if(e.data("type")=="pim"){ tit="PIEZA: Nuevo material"; ancho="sm"; if(e.data("container")!=undefined){atrib.append("container",e.data("container"));atrib2.append("container",e.data("container"));}}
						if(e.data("type")=="pim-modal"){ tit="PIEZA: Nuevo material"; ancho="sm"; if(e.data("container")!=undefined){atrib.append("container",e.data("container"));atrib2.append("container",e.data("container"));}}
						type=e.data("type");
					}
					modal(tit,ancho,"4");
					btn_modal('',"",'','','4');
					atrib.append('type',type);
					if(e.data("type")!="pim" && e.data("type")!="pim-modal"){
						atrib2.append('p',$("#datos-producto").data("p"));
					}
					atrib2.append('type',type);
					var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
					var controls2=JSON.stringify({id:"contenido_4",refresh:true,type:"html"});
					e.get_2n('produccion/search_material',atrib,controls,'produccion/view_material',atrib2,controls2);
			    }
			}
			$.fn.search_materiales=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
		 				material($(this));
		 			}
		 		});
		 		$(this).change(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				material($(this));
		 			}
		 		});
			    $(this).submit(function(event){
			    	material($(this));
			    	event.preventDefault();
			    });
			    function material(e){
		 			if(e.data("type")!=undefined){
		 				if(e.data("type")=="all"){
		 					$("input#s3_mat").val("");
		 				}
		 			}
			    	var alm=$("#alm_3").val();
					var mat=$("#s3_mat").val();
					var atrib=new FormData();atrib.append('alm',alm);atrib.append('mat',mat);atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',$("#s3_mat").data("type"));
					if($("#s3_mat").data("type")=="pm-modal-change"){atrib.append('pm',$("#s3_mat").data("pm"));}
					if($("#s3_mat").data("type")=="pgm" || $("#s3_mat").data("type")=="pgm-modal"){atrib.append('pg',$("#s3_mat").data("pg"));}
					if($("#s3_mat").data("type")=="pgm-modal-change"){atrib.append('pgm',$("#s3_mat").data("pgm"));}
					if($("#s3_mat").data("type")=="pgcm" || $("#s3_mat").data("type")=="pgcm-modal"){atrib.append('pgc',$("#s3_mat").data("pgc"));}
					if($("#s3_mat").data("type")=="pgcm-modal-change"){atrib.append('pgcm',$("#s3_mat").data("pgcm"));}
					var controls=JSON.stringify({id:"contenido_4",refresh:true,type:"html"});
					e.get_1n('produccion/view_material',atrib,controls);
			    }
			}
			$.fn.seleccionar_material=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		seleccionar_material($(this));
			    	}
			    });
		      	$(this).submit(function(event){
		      		seleccionar_material($(this));
		      		event.preventDefault();
		      	});
			    function seleccionar_material(e){
			    	if($("#s3_mat").data("type")!=undefined && e.data("m")!=undefined){
			    		var atrib=new FormData();
			    		atrib.append('m',e.data("m"));
			    		atrib.append('type',$("#s3_mat").data("type"));
			    		if($("#s3_mat").data("type")!="pim"){
							var can=($("#can_m"+e.data("m")).val()*1)+"";
							var obs=$("#obs_m"+e.data("m")).val();
							if(decimal(can,6,4) && can>0 && can<=999999.9999){
								if(textarea(obs,0,200)){
									atrib.append('can',can);
									atrib.append('obs',obs);
									atrib.append('p',$("#datos-producto").data("p"));
									var atrib2=new FormData();
									if($("#s3_mat").data("type")=="pm"){
										atrib2.append('idp',$("#datos-producto").data("p"));
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
										var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
										$(this).set("produccion/seleccionar_material",atrib,controls,'produccion/config_producto',atrib2,controls3);
										//set('produccion/seleccionar_material',atrib,'NULL',true,'produccion/config_producto',atrib2,'content_11',true,"3");
									}
									if($("#s3_mat").data("type")=="pm-modal"){
										atrib2.append('idp',$("#datos-producto").data("p"));
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
				 						var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
			 							var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
				 						$(this).set_2n("produccion/seleccionar_material",atrib,controls,"produccion/view_material_producto",atrib,controls2,"produccion/config_producto",atrib2,controls3);
										//set_2n('produccion/seleccionar_material',atrib,'NULL',true,'produccion/view_material_producto',atrib,'contenido_2',false,'produccion/config_producto',atrib2,'content_11',false,"3");
									}
									if($("#s3_mat").data("type")=="pgm"){
										atrib.append('pg',$("#s3_mat").data("pg"));
										atrib2.append('idp',$("#datos-producto").data("p"));
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
										var controls3=JSON.stringify({id:'pg'+$("#s3_mat").data("pg"),refresh:false,type:"html"});
										$(this).set("produccion/seleccionar_material",atrib,controls,'produccion/producto_grupo',atrib,controls3);
										//set('produccion/seleccionar_material',atrib,'NULL',true,'produccion/producto_grupo',atrib,'pg'+$("#s3_mat").data("pg"),false,"3");
									}
									if($("#s3_mat").data("type")=="pgm-modal"){
										atrib.append('pg',$("#s3_mat").data("pg"));
										atrib2.append('idp',$("#datos-producto").data("p"));
										var mat=$("#s2_mat").val();
										var atrib3=new FormData();atrib3.append('mat',mat);atrib3.append('p',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_mat").data("type"));atrib3.append('pg',$("#s2_mat").data("pg"));
			      						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
				 						var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
			 							var controls3=JSON.stringify({id:'pg'+$("#s2_mat").data("pg"),refresh:false,type:"html"});
				 						$(this).set_2n("produccion/seleccionar_material",atrib,controls,"produccion/view_material_producto",atrib3,controls2,"produccion/producto_grupo",atrib,controls3);
										//set_2n('produccion/seleccionar_material',atrib,'NULL',true,'produccion/view_material_producto',atrib3,'contenido_2',false,'produccion/producto_grupo',atrib,'pg'+$("#s2_mat").data("pg"),false,"3");
									}
									if($("#s3_mat").data("type")=="pgcm"){
										atrib.append('pgc',$("#s3_mat").data("pgc"));
										atrib2.append('idp',$("#datos-producto").data("p"));
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
										var controls3=JSON.stringify({id:'pgc'+$("#s3_mat").data("pgc"),refresh:false,type:"html"});
										$(this).set("produccion/seleccionar_material",atrib,controls,'produccion/grupo_color',atrib,controls3);
										//set('produccion/seleccionar_material',atrib,'NULL',true,'produccion/grupo_color',atrib,'pgc'+$("#s3_mat").data("pgc"),false,"3");//SOLO REFRESCAR COLOR
									}
									if($("#s3_mat").data("type")=="pgcm-modal"){
										atrib.append('pgc',$("#s3_mat").data("pgc"));
										atrib2.append('idp',$("#datos-producto").data("p"));
										var mat=$("#s2_mat").val();
										var atrib3=new FormData();atrib3.append('mat',mat);atrib3.append('p',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_mat").data("type"));atrib3.append('pg',$("#s2_mat").data("pg"));atrib3.append('pgc',$("#s3_mat").data("pgc"));
			      						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
				 						var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
			 							var controls3=JSON.stringify({id:'pgc'+$("#s3_mat").data("pgc"),refresh:false,type:"html"});
				 						$(this).set_2n("produccion/seleccionar_material",atrib,controls,"produccion/view_material_producto",atrib3,controls2,"produccion/grupo_color",atrib,controls3);
										//set_2n('produccion/',atrib,'NULL',true,'produccion/',atrib3,'',false,'produccion/grupo_color',atrib,'pgc'+$("#s3_mat").data("pgc"),false,"3");
									}
								}else{
									alerta("Ingrese un contenido válido","top","obs_m"+e.data("m"));
								}
							}else{
								alerta("Ingrese una cantidad válida mayor a cero y menor o igual a 999999.9999","top","can_m"+e.data("m"));
							}
			    		}else{
							if($("#s3_mat").data("type")=="pim"){
								if($("#s3_mat").data("container")!=undefined){
									var controls=JSON.stringify({id:$("#s3_mat").data("container"),refresh:false,type:"html",closed:"4"});
									$(this).get_1n("produccion/add_pieza_marterial",atrib,controls);
								}
							}
			    		}
			    	}

			    }
			}
			$.fn.update_producto_material=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		update_producto_material($(this));
			    	}
			    });
		      	$(this).submit(function(event){
		      		update_producto_material($(this));
		      		event.preventDefault();
		      	});
		      	function update_producto_material(e){
					var id="";
					if($("#s2_mat").data("type")=="pm-modal"){ id=e.data("pm"); }
					if($("#s2_mat").data("type")=="pgm-modal"){ id=e.data("pgm"); }
					if($("#s2_mat").data("type")=="pgcm-modal"){ id=e.data("pgcm"); }
					var can=($("#can_mat"+id).val()*1)+"";
					var ver=$("#ver_mat"+id).prop("checked");
					var obs=$("#obs_mat"+id).val();
					console.log(ver);
					if(decimal(can,6,4) && can>0 && can<=999999.9999){
						if(textarea(obs,0,200)){
							if(ver){ ver="1";}else{ ver="0";}
							var atrib=new FormData();
							atrib.append('can',can);
							atrib.append('ver',ver);
							atrib.append('obs',obs);
							atrib.append('type',$("#s2_mat").data("type"));
							//var atrib3=new FormData();atrib3.append('mat',$("#s2_mat").val());atrib3.append('p',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_mat").data("type"));
							if($("#s2_mat").data("type")=="pm-modal"){
								atrib.append('idpm',id);
								var controls=JSON.stringify({type:"set",preload:true});
								atrib3=new FormData();
								atrib3.append("idp",$("#datos-producto").data("p"));
								var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
								$(this).set("produccion/update_material",atrib,controls,'produccion/config_producto',atrib3,controls3);


								//set("produccion/update_material",atrib,'NULL',true,'',{},'',false);
								//set("produccion/update_material",atrib,'NULL',true,'produccion/view_material_producto',atrib3,'contenido_2',false);
							}
							//console.log($("#s2_mat").data("type"));
							if($("#s2_mat").data("type")=="pgm-modal"){
								atrib.append('pgm',id);
								var controls=JSON.stringify({type:"set",preload:true});
								$(this).set("produccion/update_material",atrib,controls,'',{},null);
								//set("produccion/update_material",atrib,'NULL',true,'',{},'',false);
								//atrib3.append('pg',$("#s2_mat").data("pg"));
								//set("produccion/update_material",atrib,'NULL',true,'produccion/view_material_producto',atrib3,'contenido_2',false);
							}
							if($("#s2_mat").data("type")=="pgcm-modal"){
								atrib.append('pgcm',id);
								var controls=JSON.stringify({type:"set",preload:true});
								$(this).set("produccion/update_material",atrib,controls,'',{},null);
								//set("produccion/update_material",atrib,'NULL',true,'',{},'',false);
							}
						}else{
							alerta("Ingrese un contenido válido","top","obs_mat"+id);
						}
					}else{
						alerta("Ingrese una cantidad válida","top","can_mat"+id);
					}
		      	}
			}
			$.fn.change_material=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		change_material($(this));
			    	}
			    });
		      	$(this).submit(function(event){
		      		change_material($(this));
		      		event.preventDefault();
		      	});
				function change_material(e){
					if(entero(e.data("m")+"",0,10)){
						var can=($("#can_m"+e.data("m")).val()*1)+"";
						console.log(can);
						var obs=$("#obs_m"+e.data("m")).val();
						if(decimal(can,6,4) && can>0 && can<=999999.9999){
							if(textarea(obs,0,200)){
								var atrib=new FormData();
								var atrib2=new FormData();
								if($("#s3_mat").data("type")=="pm-modal-change"){
									atrib.append('m',e.data("m"));atrib.append('pm',$("#s3_mat").data("pm"));atrib.append('can',can);atrib.append('obs',obs);atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',$("#s2_mat").data("type"));atrib.append('type2',$("#s3_mat").data("type"));
									atrib2.append('idp',$("#datos-producto").data("p"));
		      						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
			 						var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
		 							var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
		 							$(this).set_2n("produccion/change_material",atrib,controls,"produccion/view_material_producto",atrib,controls2,"produccion/config_producto",atrib2,controls3);
									//set_2n('produccion/change_material',atrib,'NULL',true,'produccion/view_material_producto',atrib,'contenido_2',false,'produccion/config_producto',atrib2,'content_11',false,"3");
								}
								if($("#s3_mat").data("type")=="pgm-modal-change"){
									atrib.append('m',e.data("m"));atrib.append('pg',$("#s2_mat").data("pg"));atrib.append('pgm',$("#s3_mat").data("pgm"));atrib.append('can',can);atrib.append('obs',obs);atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',$("#s2_mat").data("type"));atrib.append('type2',$("#s3_mat").data("type"));
									atrib2.append('idp',$("#datos-producto").data("p"));atrib2.append('pg',$("#s2_mat").data("pg"));
		      						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
			 						var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
		 							var controls3=JSON.stringify({id:'pg'+$("#s2_mat").data("pg"),refresh:false,type:"html"});
		 							$(this).set_2n("produccion/change_material",atrib,controls,"produccion/view_material_producto",atrib,controls2,"produccion/producto_grupo",atrib2,controls3);
									//set_2n('produccion/change_material',atrib,'NULL',true,'produccion/view_material_producto',atrib,'contenido_2',false,'produccion/producto_grupo',atrib2,'pg'+$("#s2_mat").data("pg"),false,"3");
								}
								if($("#s3_mat").data("type")=="pgcm-modal-change"){
									atrib.append('m',e.data("m"));atrib.append('pgc',$("#s2_mat").data("pgc"));atrib.append('pgcm',$("#s3_mat").data("pgcm"));atrib.append('can',can);atrib.append('obs',obs);atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',$("#s2_mat").data("type"));atrib.append('type2',$("#s3_mat").data("type"));
									atrib2.append('idp',$("#datos-producto").data("p"));atrib2.append('pgc',$("#s2_mat").data("pgc"));
		      						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
			 						var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
		 							var controls3=JSON.stringify({id:'pgc'+$("#s2_mat").data("pgc"),refresh:false,type:"html"});
		 							$(this).set_2n("produccion/change_material",atrib,controls,"produccion/view_material_producto",atrib,controls2,"produccion/grupo_color",atrib2,controls3);
									//set_2n('produccion/change_material',atrib,'NULL',true,'produccion/view_material_producto',atrib,'contenido_2',false,'produccion/grupo_color',atrib2,'pgc'+$("#s2_mat").data("pgc"),false,"3");
								}							
							}else{
								alerta("Ingrese un contenido válido","top","obs_m"+e.data("m"));
							}
						}else{
							alerta("Ingrese una cantidad válida","top","can_m"+e.data("m"));
						}
					}else{
						msj("Error de variables en el sistema");
					}
				}
		    }
			$.fn.confirmar_material=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		confirmar_material($(this));
			    	}
			    });
			    function confirmar_material(e){
					var atrib=new FormData();
					modal("Eliminar","xs","5");
					clear_datas("btn_52");
					if(e.data("type")=="pm" || e.data("type")=="pm-modal"){
						var atr="this|"+JSON.stringify({pm:e.data("pm"),type:e.data("type")});
						atrib.append('id',e.data("pm"));
					}
					if(e.data("type")=="pgm" || e.data("type")=="pgm-modal"){
						var atr="this|"+JSON.stringify({pgm:e.data("pgm"),type:e.data("type"),pg:e.data("pg")});
						atrib.append('id',e.data("pgm"));
					}
					if(e.data("type")=="pgcm" || e.data("type")=="pgcm-modal"){
						var atr="this|"+JSON.stringify({pgcm:e.data("pgcm"),type:e.data("type"),pgc:e.data("pgc")});
						atrib.append('id',e.data("pgcm"));
					}
			   		btn_modal('drop_material',atr,'',"",'5');
			   		atrib.append('type',e.data("type"));
					var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					$(this).get_1n("produccion/confirm_material",atrib,controls);
			    }
			}
			$.fn.drop_material=function(){
				var e=$(this);
				var atrib=new FormData();
				var atrib2=new FormData();
				var atrib3=new FormData();
				atrib.append('type',e.data("type"));
				if(e.data("type")=="pm"){
					atrib.append("pm",e.data("pm"));
					atrib2.append('idp',$("#datos-producto").data("p"));
		      		var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
		 			$(this).set("produccion/drop_material",atrib,controls,"produccion/config_producto",atrib2,controls2);
					//set("produccion/drop_material",atrib,'NULL',true,"produccion/config_producto",atrib2,'content_11',true,'5');
				}
				if(e.data("type")=="pm-modal"){
					atrib.append("pm",e.data("pm"));
					atrib2.append('p',$("#datos-producto").data("p"));atrib2.append('type',$("#s2_mat").data("type"));
					atrib3.append('idp',$("#datos-producto").data("p"));
		      		var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
		 			var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
		 			$(this).set_2n("produccion/drop_material",atrib,controls,"produccion/view_material_producto",atrib2,controls2,"produccion/config_producto",atrib3,controls3);
					//set_2n("produccion/drop_material",atrib,'NULL',true,'produccion/view_material_producto',atrib2,'contenido_2',false,"produccion/config_producto",atrib3,'content_11',false,'5');
				}
				if(e.data("type")=="pgm"){
					atrib.append("pgm",e.data("pgm"));
					atrib.append("pg",e.data("pg"));
					atrib2.append('idp',$("#datos-producto").data("p"));
		      		var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:'pg'+e.data("pg"),refresh:false,type:"html"});
		 			$(this).set("produccion/drop_material",atrib,controls,"produccion/producto_grupo",atrib,controls2);
					//set("produccion/drop_material",atrib,'NULL',true,"produccion/producto_grupo",atrib,'pg'+e.data("pg"),false,'5');
				}
				if(e.data("type")=="pgm-modal"){
					atrib.append("pgm",e.data("pgm"));atrib.append('pg',$("#s2_mat").data("pg"));
					atrib2.append('idp',$("#datos-producto").data("p"));
					var mat=$("#s2_mat").val();
					var atrib3=new FormData();atrib3.append('mat',mat);atrib3.append('p',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_mat").data("type"));atrib3.append('pg',$("#s2_mat").data("pg"));
		      		var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
		 			var controls3=JSON.stringify({id:'pg'+$("#s2_mat").data("pg"),refresh:false,type:"html"});
		 			$(this).set_2n("produccion/drop_material",atrib,controls,"produccion/view_material_producto",atrib3,controls2,"produccion/producto_grupo",atrib,controls3);
					//set_2n("produccion/drop_material",atrib,'NULL',true,'produccion/view_material_producto',atrib3,'contenido_2',false,'produccion/producto_grupo',atrib,'pg'+$("#s2_mat").data("pg"),false,'5');
				}
				if(e.data("type")=="pgcm"){
					atrib.append("pgcm",e.data("pgcm"));atrib.append('pgc',e.data("pgc"));
					atrib2.append('idp',$("#datos-producto").data("p"));
		      		var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
		 			$(this).set("produccion/drop_material",atrib,controls,"produccion/grupo_color",atrib,controls2);
					//set("produccion/drop_material",atrib,'NULL',true,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,'5');
				}
				if(e.data("type")=="pgcm-modal"){
					atrib.append("pgcm",e.data("pgcm"));atrib.append('pgc',$("#s2_mat").data("pgc"));
					atrib2.append('idp',$("#datos-producto").data("p"));
					var mat=$("#s2_mat").val();
					var atrib3=new FormData();atrib3.append('mat',mat);atrib3.append('p',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_mat").data("type"));atrib3.append('pg',$("#s2_mat").data("pg"));
					atrib3.append('pgc',$("#s2_mat").data("pgc"));
		      		var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
			 		var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
		 			var controls3=JSON.stringify({id:'pgc'+$("#s2_mat").data("pgc"),refresh:false,type:"html"});
		 			$(this).set_2n("produccion/drop_material",atrib,controls,"produccion/view_material_producto",atrib3,controls2,"produccion/grupo_color",atrib,controls3);
					//set_2n("produccion/drop_material",atrib,'NULL',true,'produccion/view_material_producto',atrib3,'contenido_2',false,'produccion/grupo_color',atrib,'pgc'+$("#s2_mat").data("pgc"),false,'5');
				}
			}
		/*--Materiales--*/
		/*Manejo de fotografias en el producto*/
			$.fn.producto_fotografias=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		producto_fotografias($(this));
			    	}
			    });
			    function producto_fotografias(e){
					var atrib=new FormData();
					var titulo="";
					if(e.data("type")=="pf-modal"){ titulo="PRODUCTO: Contenedor general de fotografías"; }
					if(e.data("type")=="pgcf-modal"){ titulo="CATEGORIA COLOR: Contenedor de fotografías"; atrib.append('pgc',e.data("pgc"));}
					modal(titulo,"md","2");
					btn_modal('',"",'','','2');
					atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
					var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					$(this).get_1n("produccion/producto_fotografias",atrib,controls);
			    }
			}
			$.fn.new_fotografias=function (){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		new_fotografias($(this));
			    	}
			    });
			    function new_fotografias(e){
					var atrib=new FormData();
					var titulo="";
					var atr;
					atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
					if(e.data("type")=="pf" || e.data("type")=="pf-modal"){ titulo="PRODUCTO: Nueva fotografía"; atr="this|"+JSON.stringify({type:e.data("type")});}
					if(e.data("type")=="pgcf" || e.data("type")=="pgcf-modal"){ titulo="CATEGORIA COLOR: Nueva fotografía"; atr="this|"+JSON.stringify({type:e.data("type"),pgc:e.data("pgc")});}
					modal(titulo,"sm","3");				
					btn_modal('save_fotografias',atr,'','','3');
					var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
					$(this).get_1n('produccion/new_fotografias',atrib,controls);
			    }
			}
			$.fn.save_fotografias=function(){
				var e=$(this);
				var imgs=$("#imgs").prop("files");
				var des=($("#des_img").val()).trim();
				if(imgs.length>0){
					if(imgs.length<=10){
							if(textarea(des,0,150)){
								var atrib=new FormData();
								var atrib2=new FormData();
								for(var i=0; i<imgs.length; i++){ atrib.append("archivo"+i,imgs[i]);}
								atrib.append('des',des);
								if(e.data("type")=="pf"){
									if($("#cc_img").val()!=undefined){ atrib.append('cc',$("#cc_img").val()); }
									atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
									atrib2.append('idp',$("#datos-producto").data("p"));
						      		var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
							 		var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						 			$(this).set("produccion/save_fotografias",atrib,controls,"produccion/config_producto",atrib2,controls2);
									//set('produccion/save_fotografias',atrib,'NULL',true,'produccion/config_producto',atrib2,'content_11',true,"3");
								}
								if(e.data("type")=="pf-modal"){
									if($("#cc_img").val()!=undefined){ atrib.append('cc',$("#cc_img").val()); }
									atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
									atrib2.append('idp',$("#datos-producto").data("p"));
						      		var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
							 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						 			var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						 			$(this).set_2n("produccion/save_fotografias",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/config_producto",atrib2,controls3);
									//set_2n('produccion/save_fotografias',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/config_producto',atrib2,'content_11',false,"3");
								}
								if(e.data("type")=="pgcf"){
									console.log("Entro");
									atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));atrib.append('pgc',e.data("pgc"));
									atrib2.append('idp',$("#datos-producto").data("p"));
						      		var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
							 		var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
						 			$(this).set("produccion/save_fotografias",atrib,controls,"produccion/grupo_color",atrib,controls2);
									//set('produccion/save_fotografias',atrib,'NULL',true,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,"3");
								}
								if(e.data("type")=="pgcf-modal"){
									atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));atrib.append('pgc',e.data("pgc"));
									atrib2.append('idp',$("#datos-producto").data("p"));
						      		var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
							 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						 			var controls3=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
						 			$(this).set_2n("produccion/save_fotografias",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/grupo_color",atrib,controls3);
									//set_2n('produccion/save_fotografias',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,"3");
								}
							}else{
								alerta('Ingrese una descripcion válida','top','des_img');
							}
					}else{
						alerta('Solo puede subir hasta 10 imágenes simultaneas','top','imgs');
					}
				}else{
					alerta('Seleccione al menos una fotografias','top','imgs');
				}
			}
			$.fn.rotate_fotografia=function(){
				$(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		rotate_fotografia($(this));
			    	}
			    });
			    function rotate_fotografia(e){
			    	if(e.data("type")!=undefined && e.data("rotate")!=undefined && $("#datos-producto").data("p")!=undefined){
			    		var atrib=new FormData();
			    		var atrib2=new FormData();
			    		atrib.append("type",e.data("type"));
			    		atrib.append("rotate",e.data("rotate"));
			    		if(e.data("type")=="pf-modal"){
			    			if(e.data("pi")!=undefined){
			    				atrib.append("pi",e.data("pi"));
								atrib.append('p',$("#datos-producto").data("p"));
								atrib2.append('idp',$("#datos-producto").data("p"));
							    var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
								var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
							 	var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						   		var atrib3=new FormData();
						   		atrib3.append('cod',$("#s_cod").val());
						   		atrib3.append('cod2',$("#s_cod2").val());
						   		atrib3.append('nom',$("#s_nom").val());
						   		atrib3.append('fec',$("#s_fec").val());
						   		atrib3.append('est',$("#s_est").val());
								var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							 	$(this).set_3n("produccion/rotate_fotografia",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/config_producto",atrib2,controls3,"produccion/view_producto",atrib3,controls4);
			    			}
			    		}
			    		if(e.data("type")=="pgcf-modal"){
			    			if(e.data("pgc")!=undefined && e.data("pig")!=undefined){
			    				atrib.append("pig",e.data("pig"));
			    				atrib.append('p',$("#datos-producto").data("p"));
								atrib.append('pgc',e.data("pgc"));
								atrib2.append('idp',$("#datos-producto").data("p"));
							    var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
								var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
							 	var controls3=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
						   		var atrib3=new FormData();
						   		atrib3.append('cod',$("#s_cod").val());
						   		atrib3.append('cod2',$("#s_cod2").val());
						   		atrib3.append('nom',$("#s_nom").val());
						   		atrib3.append('fec',$("#s_fec").val());
						   		atrib3.append('est',$("#s_est").val());
								var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							 	$(this).set_3n("produccion/rotate_fotografia",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/grupo_color",atrib,controls3,"produccion/view_producto",atrib3,controls4);
			    			}
			    		}
			    	}
			    }
			}
			$.fn.config_fotografia=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		config_fotografia($(this));
			    	}
			    });
			    function config_fotografia(e){
			    	modal("FOTOGRAFIA: Modificar","sm","3");
					var atrib=new FormData();
					if(e.data("type")=="pf-modal"){
						var atr="this|"+JSON.stringify({pi:e.data("pi"),type:e.data("type")});atrib.append('pi',e.data("pi"));
					}
					if(e.data("type")=="pgcf-modal"){
						var atr="this|"+JSON.stringify({pig:e.data("pig"),pgc:e.data("pgc"),type:e.data("type")});atrib.append('pig',e.data("pig"));
					}
					atrib.append('type',e.data("type"));				
					btn_modal('update_fotografia',atr,'','','3');
					var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
					$(this).get_1n('produccion/config_fotografia',atrib,controls);
			    }
			}
			$.fn.update_fotografia=function(e){
				var e=$(this);
				var imgs=$("#imgs").prop("files");
				var des=($("#des_img").val()).trim();
				if(textarea(des,0,150)){
					var atrib=new FormData();
					var atrib2=new FormData();
					atrib.append("archivo",imgs[0]);atrib.append('des',des);
					if(e.data("type")=="pf-modal"){
						atrib.append('pi',e.data("pi"));atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
						atrib2.append('idp',$("#datos-producto").data("p"));
						var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
						var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						$(this).set_3n("produccion/update_fotografia",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/config_producto",atrib2,controls3,"produccion/view_producto",atrib3,controls4);
						//set_3n('produccion/update_fotografia',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/config_producto',atrib2,'content_11',false,'produccion/view_producto',atrib3,'contenido',false,"3");
					}
					if(e.data("type")=="pgcf-modal"){
						atrib.append('pi',e.data("pi"));atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));atrib.append('pig',e.data("pig"));atrib.append('pgc',e.data("pgc"));
						atrib2.append('idp',$("#datos-producto").data("p"));
						var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
						var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
						var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						var controls3=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
						var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						$(this).set_3n("produccion/update_fotografia",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/grupo_color",atrib,controls3,"produccion/view_producto",atrib3,controls4);
						//set_3n('produccion/update_fotografia',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,'produccion/view_producto',atrib3,'contenido',false,"3");
					}
				}else{
					alerta('Ingrese una descripcion válida','top','des_img');
				}
			}
			$.fn.confirmar_fotografia=function(){
			    $(this).click(function(){
			    	if($(this).prop("tagName")!="FORM"){
			    		confirmar_fotografia($(this));
			    	}
			    });
			    function confirmar_fotografia(e){
					modal("Eliminar","xs","5");
					var atrib=new FormData();
					var atr="";
					console.log("Entro");
					if(e.data("type")=="pf" || e.data("type")=="pf-modal"){
						atr="this|"+JSON.stringify({pi:e.data("pi"),type:e.data("type")});
						atrib.append('pi',e.data("pi"));atrib.append('type',e.data("type"));
					}
					if(e.data("type")=="pgcf"){
						atr="this|"+JSON.stringify({pgcf:e.data("pgcf"),type:e.data("type"),pgc:e.data("pgc")});
						atrib.append('pgcf',e.data("pgcf"));atrib.append('type',e.data("type"));
					}
					if(e.data("type")=="pgcf-modal"){
						var atr="this|"+JSON.stringify({pig:e.data("pig"),pgc:e.data("pgc"),type:e.data("type")});atrib.append('pig',e.data("pig"));
						atrib.append('pgcf',e.data("pig"));atrib.append('type',e.data("type"));
					}
			   		btn_modal('drop_fotografia',atr,'',"",'5');
			   		var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			   		$(this).get_1n('produccion/confirmar_fotografia',atrib,controls);
			    }
			}
			$.fn.drop_fotografia=function(){
				var e=$(this);
				var atrib=new FormData();
				var atrib2=new FormData();
	   			if(e.data("type")=="pf"){
	   				atrib.append("pi",e.data("pi"));atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
					atrib2.append('idp',$("#datos-producto").data("p"));
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
					$(this).set("produccion/drop_fotografia",atrib,controls,"produccion/config_producto",atrib2,controls2);
					//set('produccion/drop_fotografia',atrib,'NULL',true,'produccion/config_producto',atrib2,'content_11',true,"5");
	   			}
	   			if(e.data("type")=="pf-modal"){
	   				atrib.append('pi',e.data("pi"));atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
					atrib2.append('idp',$("#datos-producto").data("p"));
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
					var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
					$(this).set_3n("produccion/drop_fotografia",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/config_producto",atrib2,controls3,"produccion/view_producto",atrib3,controls4);
					//set_3n('produccion/drop_fotografia',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/config_producto',atrib2,'content_11',false,'produccion/view_producto',atrib3,'contenido',false,"5");
	   			}
	   			if(e.data("type")=="pgcf"){
	   				atrib.append("pgcf",e.data("pgcf"));atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));atrib.append('pgc',e.data("pgc"));
					atrib2.append('idp',$("#datos-producto").data("p"));
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
					$(this).set("produccion/drop_fotografia",atrib,controls,"produccion/grupo_color",atrib,controls2);
					//set('produccion/drop_fotografia',atrib,'NULL',true,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,"5");
	   			}
	   			if(e.data("type")=="pgcf-modal"){
	   				atrib.append("pgcf",e.data("pig"));atrib.append("pgc",e.data("pgc"));atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
					atrib2.append('idp',$("#datos-producto").data("p"));
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
					var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
					$(this).set_3n("produccion/drop_fotografia",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/grupo_color",atrib,controls3,"produccion/view_producto",atrib3,controls4);
					//set_3n('produccion/drop_fotografia',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,'produccion/view_producto',atrib3,'contenido',false,"5");
	   			}
			}
			$.fn.portada=function(){
				$(this).click(function(){
				   	if($(this).prop("tagName")!="FORM"){
				   		portada($(this));
				   	}
				});
				function portada(e){
					if(e.data("type-save")!=undefined){
						if(e.data("type-save")=="producto"){
							if(e.data("type")=="pf-modal"){
								var atrib=new FormData();atrib.append("p",e.data("p"));atrib.append("status",e.data("status"));atrib.append("type",e.data("type"));
				   				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
				   				var controls=JSON.stringify({type:"set",preload:false});
							 	var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						 		var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						 		$(this).set_2n("produccion/portada",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/view_producto",atrib3,controls3);
								//set_2n('produccion/portada',atrib,'NULL',false,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/view_producto',atrib3,'contenido',false);
							}
							if(e.data("type")=="pgcf-modal"){
								var atrib=new FormData();atrib.append("p",e.data("p"));atrib.append("status",e.data("status"));atrib.append("type",e.data("type"));atrib.append('pgc',e.data("pgc"));
					   			var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
								var controls=JSON.stringify({type:"set",preload:false});
							 	var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						 		var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						 		$(this).set_2n("produccion/portada",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/view_producto",atrib3,controls3);
								//set_2n('produccion/portada',atrib,'NULL',false,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/view_producto',atrib3,'contenido',false);
							}
						}
						if(e.data("type-save")=="color"){
							var atrib=new FormData();atrib.append("p",e.data("p"));atrib.append("status",e.data("status"));atrib.append("type",e.data("type"));atrib.append('pgc',e.data("pgc"));
					   		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());
							var controls=JSON.stringify({type:"set",preload:false});
							var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						 	$(this).set("produccion/portada_color",atrib,controls,"produccion/producto_fotografias",atrib,controls2);
							//set('produccion/portada_color',atrib,'NULL',false,'produccion/producto_fotografias',atrib,'content_2',false);
						}
					}
				}
			}
			$.fn.copiar_imagen_color=function(){
				$(this).click(function(){
				   	if($(this).prop("tagName")!="FORM"){
				   		copiar_imagen_color($(this));
				   	}
				});
				function copiar_imagen_color(e){
					var atrib=new FormData();
					atrib.append('pi',e.data("pi"));
					atrib.append('pgc',e.data("pgc"));
					atrib.append('p',$("#datos-producto").data("p"));
					atrib.append('type',e.data("type"));
					var controls=JSON.stringify({type:"set",preload:true});
					var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
					$(this).set_2n("produccion/copiar_imagen_color",atrib,controls,"produccion/producto_fotografias",atrib,controls2,"produccion/grupo_color",atrib,controls3);
					//set_2n('produccion/copiar_imagen_color',atrib,'NULL',true,'produccion/producto_fotografias',atrib,'content_2',false,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false);
				}
			}
		/*--- Manejo de piezas ---*/
		$.fn.producto_piezas=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		producto_piezas($(this));
			   	}
			});
			function producto_piezas(e){
				var atrib=new FormData();
				var titulo="";
				atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
				if(e.data("type")=="ppi-modal"){ titulo="PRODUCTO: Contenedor general de piezas"; }
				if(e.data("type")=="pgcpi-modal"){ titulo="COLOR: Contenedor de piezas"; atrib.append('pgc',e.data("pgc"));}
				modal(titulo,"xmd","2");
				btn_modal('',"",'','','2');
			   	var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
			   	$(this).get_1n('produccion/producto_piezas',atrib,controls);
			}
		}
		$.fn.new_pieza=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		new_pieza($(this));
			   	}
			});
			function new_pieza(e){
				var atrib=new FormData();
				var tit="";
				var type="";
				var atr="";
				type=e.data("type");
				if(isset(e)){
					if(e.data("type")=="ppi" || e.data("type")=="ppi-modal"){ 
						tit="PRODUCTO: Nueva pieza";
						atr="this|"+JSON.stringify({type:e.data("type")});
					}
					if(e.data("type")=="pgcpi" || e.data("type")=="pgcpi-modal"){ 
						tit="COLOR: Nueva pieza";
						atr="this|"+JSON.stringify({type:e.data("type"),pgc:e.data("pgc")});
						atrib.append('pgc',e.data("pgc"));
					}
				}
				modal(tit,"md","3");
				btn_modal('save_pieza',atr,'','','3');
				atrib.append('type',type);
				atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',type);
			   	var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
			   	$(this).get_1n('produccion/new_pieza',atrib,controls);
			}
		}
		$.fn.save_pieza=function(e){
			var e=$(this);
			if(e.data("type")!=undefined){
				var img=$("#img_pi").prop("files");
				var nom=($("#nom_pi").val()).trim();
				var alt=($("#alt_pi").val()*1)+"";
				var anc=($("#anc_pi").val()*1)+"";
				var can=($("#can_pi").val()*1)+"";
				var mat=$("div#n_pi_mat div.card-group").data("m")+"";
				var rs=$("#rs_pi").val();
				var des=($("#des_pi").val()).trim();
				if(strSpace(nom,2,100)){
					if(decimal(alt,4,3) && alt>=0 && alt<=9999.999){
						if(decimal(anc,4,3) && anc>=0 && anc<=9999.999){
							if(entero(can,0,2) && can>=0 && can<=99){
								if(entero(mat,0,11)){
									if(textarea(des,0,200)){
										var atrib=new FormData();
										atrib.append('p',$("#datos-producto").data("p"));
										atrib.append('type',e.data("type"));
										atrib.append('archivo',img[0]);
										atrib.append('nom',nom);
										atrib.append('alt',alt);
										atrib.append('anc',anc);
										atrib.append('can',can);
										atrib.append('mat',mat);
										atrib.append('rs',rs);
										atrib.append('des',des);
										var atrib2=new FormData();
										atrib2.append('idp',$("#datos-producto").data("p"));
										if(e.data("type")=="ppi"){
											var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
											var controls2=JSON.stringify({id:'content_11',refresh:false,type:"html"});
											$(this).set("produccion/save_pieza",atrib,controls,"produccion/config_producto",atrib2,controls2);
											//set("produccion/save_pieza",atrib,'NULL',true,"produccion/config_producto",atrib2,'content_11',true,"3");
										}
										if(e.data("type")=="ppi-modal"){
											var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
											var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
											var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
											$(this).set_2n("produccion/save_pieza",atrib,controls,"produccion/producto_piezas",atrib,controls2,"produccion/config_producto",atrib2,controls3);
											//set_2n("produccion/save_pieza",atrib,'NULL',true,'produccion/producto_piezas',atrib,'content_2',false,"produccion/config_producto",atrib2,'content_11',false,"3");
										}
										if(e.data("type")=="pgcpi"){
											atrib.append('pgc',e.data("pgc"));
											var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
											var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
											$(this).set("produccion/save_pieza",atrib,controls,"produccion/grupo_color",atrib,controls2);
											//set("produccion/save_pieza",atrib,'NULL',true,'produccion/grupo_color',atrib,'pgc'+e.data("pgc"),false,"3");
										}
										if(e.data("type")=="pgcpi-modal"){
											atrib.append('pgc',$("#datos-pgc").data("pgc"));
											var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
											var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
											var controls3=JSON.stringify({id:'pgc'+$("#datos-pgc").data("pgc"),refresh:false,type:"html"});
											$(this).set_2n("produccion/save_pieza",atrib,controls,"produccion/producto_piezas",atrib,controls2,"produccion/grupo_color",atrib,controls3);
											//set_2n("produccion/save_pieza",atrib,'NULL',true,'produccion/producto_piezas',atrib,'content_2',false,"produccion/grupo_color",atrib,'pgc'+$("#datos-pgc").data("pgc"),false,"3");
										}
									}else{
										alerta("Ingrese una contenido válido","top","des_pi");
									}
								}else{
									alerta("Adicione un material","top","n_pi_mat");
								}
							}else{
								alerta("Ingrese una valor válido","top","can_pi");
							}
						}else{
							alerta("Ingrese una valor válido","top","anc_pi");
						}
					}else{
						alerta("Ingrese una valor válido","top","alt_pi");
					}
				}else{
					alerta("Ingrese una valor válido","top","nom_pi");
				}
			}
		}
		$.fn.configurar_pieza=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		configurar_pieza($(this));
			   	}
			});
			function configurar_pieza(e){
				var atrib=new FormData();
				var tit="";
				var type="";
				var atr="";
				if(isset(e)){
					if(e.data("type")=="ppi-modal"){ 
						tit="PRODUCTO: Modificar pieza";
						atr="this|"+JSON.stringify({type:e.data("type"),ppi:e.data("ppi")});
						atrib.append('ppi',e.data("ppi"));
					}
					if(e.data("type")=="pgcpi-modal"){ 
						tit="COLOR: Modificar pieza";
						atr="this|"+JSON.stringify({type:e.data("type"),pgcpi:e.data("pgcpi")});
						atrib.append('pgcpi',e.data("pgcpi"));
					}
					type=e.data("type");
				}
				modal(tit,"md","3");
				btn_modal('update_pieza',atr,'','','3');
				atrib.append('p',$("#datos-producto").data("p"));atrib.append('type',type);
				var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
				$(this).get_1n('produccion/configurar_pieza',atrib,controls);
			}
		}
		$.fn.update_pieza=function(){
			var e=$(this);
			//var img=e.data("ppi");
			var img=$("#img_pi").prop("files");
			var nom=($("#nom_pi").val()).trim();
			var alt=($("#alt_pi").val()*1)+"";
			var anc=($("#anc_pi").val()*1)+"";
			var can=($("#can_pi").val()*1)+"";
			var mat=$("div#n_pi_mat div.card-group").data("m")+"";
			var rs=$("#rs_pi").val();
			var des=($("#des_pi").val()).trim();
			if(strSpace(nom,2,100)){
				if(decimal(alt,4,3) && alt>=0 && alt<=9999.999){
					if(decimal(anc,4,3) && anc>=0 && anc<=9999.999){
						if(entero(can,0,99)){
							if(entero(mat,0,11)){
								if(textarea(des,0,200)){
									var atrib=new FormData();
									atrib.append('p',$("#datos-producto").data("p"));
									atrib.append('type',e.data("type"));
									atrib.append('archivo',img[0]);
									atrib.append('nom',nom);
									atrib.append('alt',alt);
									atrib.append('anc',anc);
									atrib.append('can',can);
									atrib.append('mat',mat);
									atrib.append('rs',rs);
									atrib.append('des',des);
									var atrib2=new FormData();
									atrib2.append('idp',$("#datos-producto").data("p"));
									if(e.data("type")=="ppi"){
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
										var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
										$(this).set("produccion/update_pieza",atrib,controls,"produccion/config_producto",atrib2,controls2);
										//set("produccion/update_pieza",atrib,'NULL',true,"produccion/config_producto",atrib2,'content_11',true,"3");
									}
									if(e.data("type")=="ppi-modal"){
										atrib.append('ppi',e.data("ppi"));
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
										var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
										var controls3=JSON.stringify({id:'content_11',refresh:false,type:"html"});
										$(this).set_2n("produccion/update_pieza",atrib,controls,"produccion/producto_piezas",atrib,controls2,"produccion/config_producto",atrib2,controls3);
										//set_2n("produccion/update_pieza",atrib,'NULL',true,'produccion/producto_piezas',atrib,'content_2',false,"produccion/config_producto",atrib2,'content_11',false,"3");
									}
									if(e.data("type")=="pgcpi-modal"){
										atrib.append('pgcpi',e.data("pgcpi"));
										atrib.append('pgc',$("#datos-pgc").data("pgc"));
										var controls=JSON.stringify({type:"set",preload:true,closed:"3"});
										var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
										var controls3=JSON.stringify({id:'pgc'+$("#datos-pgc").data("pgc"),refresh:false,type:"html"});
										$(this).set_2n("produccion/update_pieza",atrib,controls,"produccion/producto_piezas",atrib,controls2,"produccion/grupo_color",atrib,controls3);
										//set_2n("produccion/update_pieza",atrib,'NULL',true,'produccion/producto_piezas',atrib,'content_2',false,"produccion/grupo_color",atrib,'pgc'+$("#datos-pgc").data("pgc"),false,"3");
									}
								}else{ 
									alerta("Ingrese una contenido válido","top","des_pi"); control=false;
								}
							}else{
								alerta("Adicione un material","top","n_pi_mat");
							}
						}else{
							alerta("Ingrese una valor válido","top","can_pi");
						}
					}else{
						alerta("Ingrese una valor válido","top","anc_pi");
					}
				}else{
					alerta("Ingrese una valor válido","top","alt_pi");
				}
			}else{
				alerta("Ingrese una valor válido","top","nom_pi");
			}
		}
		$.fn.confirmar_pieza=function(e){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		confirmar_pieza($(this));
			   	}
			});
			function confirmar_pieza(e){
				modal("Eliminar","xs","5");
				var atr="";
				var atrib=new FormData();
				if(e.data("type")=="ppi" || e.data("type")=="ppi-modal"){
					atr="this|"+JSON.stringify({destino:e.data("destino"),type:e.data("type"),ppi:e.data("ppi")});
					atrib.append('ppi',e.data("ppi"));
				}
				if(e.data("type")=="pgcpi" || e.data("type")=="pgcpi-modal"){
					atr="this|"+JSON.stringify({type:e.data("type"),pgcpi:e.data("pgcpi"),pgc:e.data("pgc")});
					atrib.append('pgcpi',e.data("pgcpi"));
				}
				atrib.append('type',e.data("type"));
			   	btn_modal('drop_pieza',atr,'',"",'5');
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('produccion/confirmar_pieza',atrib,controls);
			   	//get('produccion/confirmar_pieza',atrib,'content_5',true);
			}
		}
		$.fn.drop_pieza=function(){
			var e=$(this);
			var atrib=new FormData();
			var atrib2=new FormData();
			atrib.append('type',e.data("type"));atrib.append('p',$("#datos-producto").data("p"));
			if(e.data("type")=="ppi"){
				atrib.append('ppi',e.data("ppi"));
				atrib2.append('idp',$("#datos-producto").data("p"));
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
				$(this).set("produccion/drop_pieza",atrib,controls,"produccion/config_producto",atrib2,controls2);
				//set("produccion/drop_pieza",atrib,'NULL',false,"produccion/config_producto",atrib2,'content_11',true,"5");
			}
			if(e.data("type")=="ppi-modal"){
				atrib.append('ppi',e.data("ppi"));atrib.append('type',e.data("type"));
				atrib2.append('idp',$("#datos-producto").data("p"));
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
				var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
				$(this).set_2n("produccion/drop_pieza",atrib,controls,"produccion/producto_piezas",atrib,controls2,"produccion/config_producto",atrib2,controls3);
				//set_2n("produccion/drop_pieza",atrib,'NULL',false,'produccion/producto_piezas',atrib,'content_2',false,"produccion/config_producto",atrib2,'content_11',true,"5");
			}
			if(e.data("type")=="pgcpi"){
				atrib.append('pgcpi',e.data("pgcpi"));atrib.append('pgc',e.data("pgc"));
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:'pgc'+e.data("pgc"),refresh:false,type:"html"});
				$(this).set("produccion/drop_pieza",atrib,controls,"produccion/grupo_color",atrib,controls2);
				//set("produccion/drop_pieza",atrib,'NULL',false,"produccion/grupo_color",atrib,'pgc'+e.data("pgc"),false,"5");
			}
			if(e.data("type")=="pgcpi-modal"){
				atrib.append('pgcpi',e.data("pgcpi"));atrib.append('pgc',$("#datos-pgc").data("pgc"));
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
				var controls3=JSON.stringify({id:'pgc'+$("#datos-pgc").data("pgc"),refresh:false,type:"html"});
				$(this).set_2n("produccion/drop_pieza",atrib,controls,"produccion/producto_piezas",atrib,controls2,"produccion/grupo_color",atrib,controls3);
				//set_2n("produccion/drop_pieza",atrib,'NULL',false,'produccion/producto_piezas',atrib,'content_2',false,"produccion/grupo_color",atrib,'pgc'+$("#datos-pgc").data("pgc"),false,"5");
			}
		}
		/*--- End manejo de piezas ---*/
		/*--- Manejo de rejujados y sellos ---*/
		$.fn.new_repujado_sello=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		new_repujado_sello($(this));
			   	}
			});
			function new_repujado_sello(e){
				modal("Nuevo repujado o sello","sm","7");
				var atr="";
				if(e.data("type")=="rs" || e.data("type")=="rs-modal"){
					atr="this|"+JSON.stringify({destino:e.data("destino"),type:e.data("type")});
				}
				btn_modal('save_repujado_sello',atr,'','','7');
			   	var controls=JSON.stringify({id:"content_7",refresh:true,type:"html"});
			   	$(this).get_1n('produccion/new_repujado_sello',{},controls);
			}
		}
		$.fn.save_repujado_sello=function(){
			var img=$("#img_rs").prop('files');
			var nom=$("#nom_rs").val();
			var tip=$("#tip_rs").val();
			if(strSpace(nom,1,150)){
				if(tip=="0" || tip=="1"){
					var atrib=new FormData();atrib.append('archivo',img[0]);atrib.append('nom',nom);atrib.append('tip',tip);
					var atrib2=new FormData();
					if($(this).data('type')=="rs"){
						var controls=JSON.stringify({type:"set",preload:true,closed:"7"});
						var controls2=JSON.stringify({id:$(this).data("destino"),refresh:false,type:"html"});
						$(this).set("produccion/save_repujado_sello",atrib,controls,"produccion/opciones_repujados_sellos",{},controls2);
						//set("produccion/save_repujado_sello",atrib,'NULL',true,"produccion/opciones_repujados_sellos",{},$(this).data("destino"),false,"7");
					}
					if($(this).data('type')=="rs-modal"){
						atrib2.append("destino",$(this).data("destino"));atrib2.append("type",$(this).data("type"));
						var controls=JSON.stringify({type:"set",preload:true,closed:"7"});
						var controls2=JSON.stringify({id:"content_4",refresh:false,type:"html"});
						var controls3=JSON.stringify({id:$(this).data("destino"),refresh:false,type:"html"});
						$(this).set_2n("produccion/save_repujado_sello",atrib,controls,"produccion/view_repujados_sellos",atrib2,controls2,"produccion/opciones_repujados_sellos",{},controls3);
						//set_2n("produccion/save_repujado_sello",atrib,'NULL',true,'produccion/view_repujados_sellos',atrib2,'content_4',false,"produccion/opciones_repujados_sellos",{},$(this).data("destino"),false,"7");
					}
				}else{
					alerta("Seleccione un opción válida","top","tip_rs");
				}
			}else{
				alerta("Ingrese un contenido válido.","top","nom_rs");
			}
		}
		$.fn.view_repujados_sellos=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		view_repujados_sellos($(this));
			   	}
			});
			function view_repujados_sellos(e){
				modal("Repujados y sellos","sm","4");
				var atrib=new FormData();
				atrib.append("destino",e.data("destino"));
				atrib.append("type",e.data("type"));
				btn_modal('',"",'','','4');
				var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
				$(this).get_1n('produccion/view_repujados_sellos',atrib,controls);
			}
		}
		$.fn.config_repujado_sello=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		config_repujado_sello($(this));
			   	}
			});
			function config_repujado_sello(e){
				modal("Modificar repujado o sello","sm","7");
				var atr="";
				if(e.data("type")=="rs" || e.data("type")=="rs-modal"){
					atr="this|"+JSON.stringify({destino:e.data("destino"),type:e.data("type"),rs:e.data("rs")});
				}
				btn_modal('update_repujado_sello',atr,'','','7');
				var atrib=new FormData();
				atrib.append('rs',e.data("rs"));
				atrib.append('destino',e.data("destino"));
				atrib.append('type',e.data("type"));
				var controls=JSON.stringify({id:"content_7",refresh:true,type:"html"});
				$(this).get_1n('produccion/config_repujado_sello',atrib,controls);
			}
		}
		$.fn.update_repujado_sello=function(){
			var e=$(this);
			var img=$("#img_rs").prop('files');
			var nom=$("#nom_rs").val();
			var tip=$("#tip_rs").val();
			if(strSpace(nom,1,150)){
				if(tip=="0" || tip=="1"){
					var atrib=new FormData();atrib.append('rs',e.data("rs"));atrib.append('archivo',img[0]);atrib.append('nom',nom);atrib.append('tip',tip);
					var atrib2=new FormData();
					if(e.data('type')=="rs"){
						var controls=JSON.stringify({type:"set",preload:true,closed:"7"});
						var controls2=JSON.stringify({id:e.data("destino"),refresh:false,type:"html"});
						$(this).set("produccion/save_repujado_sello",atrib,controls,"produccion/opciones_repujados_sellos",{},controls2);
						//set("produccion/save_repujado_sello",atrib,'NULL',true,"produccion/opciones_repujados_sellos",{},e.data("destino"),false,"7");
					}
					if(e.data('type')=="rs-modal"){
						atrib2.append("destino",e.data("destino"));atrib2.append("type",e.data("type"));
						var controls=JSON.stringify({type:"set",preload:true,closed:"7"});
						var controls2=JSON.stringify({id:"content_4",refresh:false,type:"html"});
						var controls3=JSON.stringify({id:e.data("destino"),refresh:false,type:"html"});
						$(this).set_2n("produccion/update_repujado_sello",atrib,controls,"produccion/view_repujados_sellos",atrib2,controls2,"produccion/opciones_repujados_sellos",{},controls3);
						//set_2n("produccion/update_repujado_sello",atrib,'NULL',true,'produccion/view_repujados_sellos',atrib2,'content_4',false,"produccion/opciones_repujados_sellos",{},e.data("destino"),false,"7");
					}
				}else{
					alerta("Seleccione un opción válida","top","tip_rs");
				}
			}else{
				alerta("Ingrese un contenido válido.","top","nom_rs");
			}
		}
		$.fn.confirmar_repujado_sello=function(){
			$(this).click(function(){
			   	if($(this).prop("tagName")!="FORM"){
			   		confirmar_repujado_sello($(this));
			   	}
			});
			function confirmar_repujado_sello(e){
				modal("Eliminar","xs","5");
				var atrib=new FormData();
				var atrib2=new FormData();
				if(e.data('type')=="rs-modal"){
					atrib.append('rs',e.data("rs"));
					atr="this|"+JSON.stringify({destino:e.data("destino"),type:e.data("type"),rs:e.data("rs")});
					btn_modal('drop_repujado_sello',atr,'',"",'5');
					var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					$(this).get_1n('produccion/confirmar_repujado_sello',atrib,controls);
					//get('produccion/confirmar_repujado_sello',atrib,'content_5',true);
				}
			}
		}
		$.fn.drop_repujado_sello=function(){
			var e=$(this);
			var atrib=new FormData();
			var atrib2=new FormData();
			if(e.data('type')=="rs-modal"){
				atrib.append('rs',e.data("rs"));
				atrib2.append("destino",e.data("destino"));atrib2.append("type",e.data("type"));
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:"content_4",refresh:false,type:"html"});
				var controls3=JSON.stringify({id:e.data("destino"),refresh:false,type:"html"});
				$(this).set_2n("produccion/drop_repujado_sello",atrib,controls,"produccion/view_repujados_sellos",atrib2,controls2,"produccion/opciones_repujados_sellos",{},controls3);
				//set_2n("produccion/drop_repujado_sello",atrib,'NULL',true,'produccion/view_repujados_sellos',atrib2,'content_4',false,"produccion/opciones_repujados_sellos",{},e.data("destino"),false,"5");
			}
		}
		/*--- End manejo de rejujados y sellos ---*/
    /*------ Config producto ------*/
    $.fn.pila_producto_proceso=function(){
		$(this).click(function(){
		   	if($(this).prop("tagName")!="FORM"){
		   		pila_producto_proceso($(this));
		   	}
		});
		function pila_producto_proceso(e){
	      	if(e.data("type-save")!=undefined){
				var atrib=new FormData();
				var titulo="PRODUCTO: procesos asignados";
				modal(titulo,"md","2");
				btn_modal('',"",'','','2');
				atrib.append('p',$("#datos-producto").data("p"));atrib.append('type-save',e.data("type-save"));
				if(e.data("type-save")=="ppr-modal"){ titulo="Contenedor general de procesos"; }
				if(e.data("type-save")=="pgcpr-modal"){ titulo="Contenedor general de procesos en el color"; atrib.append("pgc",e.data("pgc"));atrib.append("pg",e.data("pg"));}
				if(e.data("type-save")=="pgpr-modal"){ titulo="Contenedor general de procesos en el grupo"; atrib.append("pg",e.data("pg"));}
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				e.get_1n('produccion/pila_producto_proceso',atrib,controls);
	      	}
		}
    }
	$.fn.new_producto_proceso=function(){
		$(this).click(function(){
		   	if($(this).prop("tagName")!="FORM"){
		   		new_producto_proceso($(this));
		   	}
		});
		function new_producto_proceso(e){
			if(e.data("type-save")!=undefined && $("#datos-producto").data("p")!=undefined){
				var atrib=new FormData();
				modal("Nuevo proceso","sm","3");
				btn_modal('',"",'','','3');
			 	var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
			 	var controls2=JSON.stringify({id:"contenido_3",refresh:true,type:"html"});
			 	var atrib=new FormData();atrib.append("p",$("#datos-producto").data("p"));atrib.append("type-save",e.data("type-save"));
			 	if(e.data("type-save")=="pgcpr-modal" || e.data("type-save")=="pgcpr"){ atrib.append("pgc",e.data("pgc"));atrib.append("pg",e.data("pg"));}
			 	if(e.data("type-save")=="pgpr-modal" || e.data("type-save")=="pgpr"){ atrib.append("pg",e.data("pg"));}
			 	e.get_2n('produccion/search_proceso',atrib,controls,'produccion/view_proceso',atrib,controls2);
			}
		}
	}
	/*--Manejo de procesos--*/
    $.fn.view_proceso=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			view_proceso($(this));
	 		}
	 	});
		$(this).submit(function(event){
			view_proceso($(this));
			event.preventDefault();
		});
	 	function view_proceso(e){
	    	if($("#datos-producto").data("p")!=undefined && $("#s3_nom").val()!=undefined && $("#s3_nom").data("type-save")!=undefined){
			 	var controls=JSON.stringify({id:"contenido_3",refresh:true,type:"html"});
			 	var atrib3=new FormData();
			 	atrib3.append("p",$("#datos-producto").data("p"));
			 	atrib3.append("nom",$("#s3_nom").val());
			 	atrib3.append("type-save",$("#s3_nom").data("type-save"));
			 	e.get_1n('produccion/view_proceso',atrib3,controls);
	    	}
	 	}
	}
    $.fn.new_proceso=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_proceso($(this));
	 			}
	 		});
	 		function new_proceso(e){
			 	modal("Nuevo proceso","sm","4");
			 	var atr="this";
			 	btn_modal('save_proceso',atr,'',"",'4');
			 	var atrib=new FormData();if($("input#s3_nom").data("type-save")!=undefined){atrib.append("type-save",$("input#s3_nom").data("type-save"));}
			 	var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
			 	e.get_1n('produccion/new_proceso',atrib,controls);
	 		}
	}
    $.fn.save_proceso=function(){
      	if($("#n3_pr").val()!=undefined){
      		var nom=$("#n3_pr").val();
      		if(strSpace(nom,3,100)){
      			var atrib=new FormData();atrib.append("nom",nom);
 				var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
 				if($("input#s3_nom").data("type-save")!=undefined){
					var controls2=JSON.stringify({id:"contenido_3",type:"html"});
 					var atrib3=new FormData();atrib3.append("p",$("#datos-producto").data("p"));atrib3.append("nom",$("#s3_nom").val());atrib3.append("type-save",$("input#s3_nom").data("type-save"));
 					$(this).set("produccion/save_proceso",atrib,controls,'produccion/view_proceso',atrib3,controls2);
 				}
      		}else{
      			alerta("Ingrese un nombre de proceso válido","top","n3_pr");
      		}
      	}
	}
    $.fn.update_proceso=function(){
      	$(this).click(function(){
      		if($(this).prop("tagName")!="FORM"){
      			update_proceso($(this));
      		}
      	});
      	$(this).submit(function(event){
      		update_proceso($(this));
      		event.preventDefault();
      	});
      	function update_proceso(e){
      		if(e.data("pr")!=undefined && $("input#proc"+e.data("pr")).val()!=undefined){
      			var nom=$("input#proc"+e.data("pr")).val();
      			if(strSpace(nom,3,100)){
	      			var atrib=new FormData();atrib.append("pr",e.data("pr"));atrib.append("nom",nom);
	      			var controls=JSON.stringify({type:"set",preload:true});
	      			var controls2=JSON.stringify({id:"contenido_3",type:"html"});
	 				var atrib3=new FormData();
	 				atrib3.append("p",$("#datos-producto").data("p"));
	 				atrib3.append("nom",$("#s3_nom").val());
	 				atrib3.append("type-save",$("#s3_nom").data("type-save"));
	 				$(this).set("produccion/update_proceso",atrib,controls,'produccion/view_proceso',atrib3,controls2);
      			}else{
      				alerta("Ingrese una valor válido","top","#proc"+e.data("pr"));
      			}
      		}
      	}
	}
    $.fn.confirmar_proceso=function(){
      	$(this).click(function(){
      		if($(this).prop("tagName")!="FORM"){
      			confirmar_proceso($(this));
      		}
      	});
      	function confirmar_proceso(e){
	      	if(e.data("pr")!=undefined){
	      		var atrib=new FormData();atrib.append("pr",e.data("pr"));
	      		modal("","xs","5");
			   	var atr="this|"+JSON.stringify({pr:e.data("pr")});
			   	btn_modal('drop_proceso',atr,'',"",'5');
			   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			   	e.get_1n("produccion/confirmar_proceso",atrib,controls);
			}
      	}
	}
    $.fn.drop_proceso=function(){
      	if($(this).data("pr")!=undefined){
      		var atrib=new FormData();atrib.append("pr",$(this).data("pr"));
			var controls=JSON.stringify({type:"set",preload:true, closed: "5"});
	      	var controls2=JSON.stringify({id:"contenido_3",type:"html"});
	 		var atrib3=new FormData();atrib3.append("p",$("#datos-producto").data("p"));atrib3.append("nom",$("#s3_nom").val());atrib3.append("type-save",$("#s3_nom").data("type-save"));
	 		$(this).set("produccion/drop_proceso",atrib,controls,'produccion/view_proceso',atrib3,controls2);
		}
	}
	/*--End manejo de procesos--*/
    $.fn.add_proceso_producto=function(){
      	$(this).click(function(){
      		if($(this).prop("tagName")!="FORM"){
      			add_proceso_producto($(this));
      		}
      	});
      	function add_proceso_producto(e){
	      	if(e.data("pr")!=undefined && $("#datos-producto").data("p")!=undefined){
	      		var atrib=new FormData();atrib.append("p",$("#datos-producto").data("p"));atrib.append("pr",e.data("pr"));
	      		var controls=JSON.stringify({type:"set",preload:true});
	      		if($("input#s3_nom").data("type-save")!=undefined){
	      			atrib.append("type-save",$("input#s3_nom").data("type-save"));
	      			if($("input#s3_nom").data("type-save")=="ppr"){
				 		atrib.append("idp",$("#datos-producto").data("p"));
				 		var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						e.set("produccion/add_proceso_producto",atrib,controls,"produccion/config_producto",atrib,controls3);
	      			}
	      			if($("input#s3_nom").data("type-save")=="ppr-modal"){
				 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
				 		atrib.append("idp",$("#datos-producto").data("p"));
				 		var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						e.set_2n("produccion/add_proceso_producto",atrib,controls,"produccion/pila_producto_proceso",atrib,controls2,"produccion/config_producto",atrib,controls3);
	      			}
	      			if($("input#s3_nom").data("type-save")=="pgcpr-modal"){
	      				if($("input#s3_nom").data("pgc")!=undefined && $("input#s3_nom").data("pg")!=undefined){
	      					atrib.append("pgc",$("input#s3_nom").data("pgc"));atrib.append("pg",$("input#s3_nom").data("pg"));
					 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					 		atrib.append("idp",$("#datos-producto").data("p"));
					 		var controls3=JSON.stringify({id:'pgc'+$("input#s3_nom").data("pgc"),refresh:false,type:"html"});
							e.set_2n("produccion/add_proceso_producto",atrib,controls,"produccion/pila_producto_proceso",atrib,controls2,"produccion/grupo_color",atrib,controls3);
	      				}
	      			}
	      			if($("input#s3_nom").data("type-save")=="pgcpr"){
	      				if($("input#s3_nom").data("pgc")!=undefined){
					 		atrib.append("idp",$("#datos-producto").data("p"));atrib.append("pgc",$("input#s3_nom").data("pgc"));
					 		var controls3=JSON.stringify({id:'pgc'+$("input#s3_nom").data("pgc"),refresh:false,type:"html"});
							e.set("produccion/add_proceso_producto",atrib,controls,"produccion/grupo_color",atrib,controls3);
	      				}
	      			}
	      			if($("input#s3_nom").data("type-save")=="pgpr-modal"){
	      				if($("input#s3_nom").data("pg")!=undefined){
	      					atrib.append("pg",$("input#s3_nom").data("pg"));
					 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					 		atrib.append("idp",$("#datos-producto").data("p"));
					 		var controls3=JSON.stringify({id:'pg'+$("input#s3_nom").data("pg"),refresh:false,type:"html"});
							e.set_2n("produccion/add_proceso_producto",atrib,controls,"produccion/pila_producto_proceso",atrib,controls2,"produccion/producto_grupo",atrib,controls3);
	      				}
	      			}
	      			if($("input#s3_nom").data("type-save")=="pgpr"){
	      				if($("input#s3_nom").data("pg")!=undefined){
					 		atrib.append("idp",$("#datos-producto").data("p"));atrib.append("pg",$("input#s3_nom").data("pg"));
					 		var controls3=JSON.stringify({id:'pg'+$("input#s3_nom").data("pg"),refresh:false,type:"html"});
							e.set("produccion/add_proceso_producto",atrib,controls,"produccion/producto_grupo",atrib,controls3);
	      				}
	      			}
	      		}
	      	}
      	}
    }
    $.fn.confirmar_producto_proceso=function(){
      	$(this).click(function(){
      			if($(this).prop("tagName")!="FORM"){
      			confirmar_producto_proceso($(this));
      		}
      	});
      	function confirmar_producto_proceso(e){
	      	if(e.data("ppr")!=undefined && e.data("type-save")!=undefined){
	      		var atrib=new FormData();atrib.append("ppr",e.data("ppr"));
				modal("","xs","5");
				if(e.data("type-save")=="ppr-modal" || e.data("type-save")=="ppr"){
					var atr="this|"+JSON.stringify({ppr:e.data("ppr"),"type-save":e.data("type-save")});
				}
				if(e.data("type-save")=="pgcpr-modal" || e.data("type-save")=="pgcpr"){
					var atr="this|"+JSON.stringify({ppr:e.data("ppr"),"type-save":e.data("type-save"),"pgc":e.data("pgc"),"pg":e.data("pg")});
				}
				if(e.data("type-save")=="pgpr-modal" || e.data("type-save")=="pgpr"){
					if(e.data("pg")!=undefined){
						var atr="this|"+JSON.stringify({ppr:e.data("ppr"),"type-save":e.data("type-save"),"pg":e.data("pg")});
					}
				}
				btn_modal('drop_producto_proceso',atr,'',"",'5');
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				e.get_1n("produccion/confirmar_producto_proceso",atrib,controls);
	      	}
      	}
    }
    $.fn.drop_producto_proceso=function(){
      	if($(this).data("ppr")!=undefined && $(this).data("type-save")!=undefined){
      		var atrib=new FormData();atrib.append("p",$("#datos-producto").data("p"));atrib.append("ppr",$(this).data("ppr"));atrib.append("type-save",$(this).data("type-save"));
			var controls=JSON.stringify({type:"set",preload:true, closed:"5"});
			if($(this).data("type-save")=="ppr-modal"){
		 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
		 		atrib.append("idp",$("#datos-producto").data("p"));
		 		var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
				$(this).set_2n("produccion/drop_producto_proceso",atrib,controls,"produccion/pila_producto_proceso",atrib,controls2,"produccion/config_producto",atrib,controls3);
			}
			if($(this).data("type-save")=="ppr"){
		 		atrib.append("idp",$("#datos-producto").data("p"));
		 		var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
				$(this).set("produccion/drop_producto_proceso",atrib,controls,"produccion/config_producto",atrib,controls3);
			}
			if($(this).data("type-save")=="pgcpr-modal"){
				if($(this).data("pgc")!=undefined){
			 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
			 		atrib.append("idp",$("#datos-producto").data("p"));atrib.append("pgc",$(this).data("pgc"));atrib.append("pg",$(this).data("pg"));
			 		var controls3=JSON.stringify({id:'pgc'+$(this).data("pgc"),refresh:false,type:"html"});
					$(this).set_2n("produccion/drop_producto_proceso",atrib,controls,"produccion/pila_producto_proceso",atrib,controls2,"produccion/grupo_color",atrib,controls3);
				}
			}
			if($(this).data("type-save")=="pgcpr"){
				if($(this).data("pgc")!=undefined){
			 		atrib.append("idp",$("#datos-producto").data("p"));atrib.append("pgc",$(this).data("pgc"));
			 		var controls3=JSON.stringify({id:'pgc'+$(this).data("pgc"),refresh:false,type:"html"});
					$(this).set("produccion/drop_producto_proceso",atrib,controls,"produccion/grupo_color",atrib,controls3);
				}
			}
			if($(this).data("type-save")=="pgpr-modal"){
				if($(this).data("pg")!=undefined){
			 		var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
			 		atrib.append("idp",$("#datos-producto").data("p"));atrib.append("pg",$(this).data("pg"));
			 		var controls3=JSON.stringify({id:'pg'+$(this).data("pg"),refresh:false,type:"html"});
					$(this).set_2n("produccion/drop_producto_proceso",atrib,controls,"produccion/pila_producto_proceso",atrib,controls2,"produccion/producto_grupo",atrib,controls3);
				}
			}
			if($(this).data("type-save")=="pgpr"){
				if($(this).data("pg")!=undefined){
			 		atrib.append("idp",$("#datos-producto").data("p"));atrib.append("pg",$(this).data("pg"));
			 		var controls3=JSON.stringify({id:'pg'+$(this).data("pg"),refresh:false,type:"html"});
					$(this).set("produccion/drop_producto_proceso",atrib,controls,"produccion/producto_grupo",atrib,controls3);
				}
			}
		}
	}
    /*------ End config producto ------*/
	/*--- Manejo de grupos ---*/
			$.fn.grupos=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						grupos($(this));
					}
				});
				function grupos(e){
				 	var atrib=new FormData();
				 	if(e.data("type")=="new"){ modal("CATEGORIA: Adicionar","md","2");}
				 	if(e.data("type")=="update"){ modal("CATEGORIA: Cambiar","md","2"); atrib.append('idpgr',e.data("pg"));}
				 	btn_modal('',"",'',"",'2');
				 	atrib.append('id',$("#datos-producto").data("p"));atrib.append('type',e.data("type"));
				 	var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
			 		e.get_2n('produccion/search_grupo',atrib,controls,'produccion/view_grupo',atrib,controls2);
				}
			}
			$.fn.view_grupo=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				if($(this).data("type")!=undefined){
		 					if($(this).data("type")=="all"){
		 						$(this).reset_input_2();
		 					}
		 					view_grupo($(this));
		 				}
		 			}
		 		});
		 		$(this).change(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				view_grupo($(this));
		 			}
		 		});
			    $(this).submit(function(event){
			    	view_grupo($(this));
			    	event.preventDefault();
			    });
		 		function view_grupo(e){
		 			var atrib3=new FormData();
					atrib3.append('id',$("#datos-producto").data("p"));
					atrib3.append('type',$("#s2_gru").data("type"));
					atrib3.append('cat',$("#s2_gru").val());
					atrib3.append('abr',$("#s2_abr").val());
					if($("#s2_gru").data("type")=="update"){atrib3.append('idpgr',$("#s2_gru").data("pg"));}
					var controls=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
				 	e.get_1n('produccion/view_grupo',atrib3,controls);
		 		}
			}
			 $.fn.new_grupo=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						new_grupo($(this));
					}
				});
				function new_grupo(e){
				 	modal("CATEGORIA: Nuevo","sm","3");
				 	btn_modal('save_grupo',"this",'',"",'3');
					var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
				 	e.get_1n('produccion/new_grupo',{},controls);
				}
			 }
			 $.fn.save_grupo=function(){
			 	var cat=$("#n3_cat").val();
			 	var abr=$("#n3_abr").val();
			 	if(strSpace(cat,2,50)){
			 		if(strSpace(abr,0,10)){
			 			var atrib=new FormData();atrib.append('cat',cat);atrib.append('abr',abr);
				 		var atrib3=new FormData();atrib3.append('id',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_gru").data("type"));atrib3.append('cat',$("#s2_gru").val());atrib3.append('abr',$("#s2_abr").val());
				 		if($("#s2_gru").data("type")=="update"){ atrib3.append('idpgr',$("#s2_gru").data("pg"));}
			 			var controls1=JSON.stringify({type:"set",preload:true,closed:"3"});
			 			var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
			 			$(this).set('produccion/save_grupo',atrib,controls1,'produccion/view_grupo',atrib3,controls2);
			 		}else{
			 			alerta("Ingrese un contenido válido.","top","n3_abr");
			 		}
			 	}else{
			 		 alerta("Ingrese un contenido válido.","top","n3_cat");
			 	}
			 }
			 $.fn.update_grupo=function(){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			update_grupo($(this));
		      		}
		      	});
		      	$(this).submit(function(event){
		      		update_grupo($(this));
		      		event.preventDefault();
		      	});
		      	function update_grupo(e){
				 	var cat=$("#cat"+e.data("g")).val();
				 	var abr=$("#abr"+e.data("g")).val();
				 	if(strSpace(cat,2,50)){
				 		if(strSpace(abr,0,10)){
					 		var atrib=new FormData();atrib.append('id',e.data("g"));atrib.append('cat',cat);atrib.append('abr',abr);
					 		var atrib3=new FormData();atrib3.append('id',$("#datos-producto").data("p"));atrib3.append('type',$("#s2_gru").data("type"));atrib3.append('cat',$("#s2_gru").val());atrib3.append('abr',$("#s2_abr").val());
					 		if($("#s2_gru").data("type")=="update"){ atrib3.append('idpgr',$("#s2_gru").data("pg"));}
				 			var controls1=JSON.stringify({type:"set",preload:true});
				 			var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
				 			$(this).set('produccion/update_grupo',atrib,controls1,'produccion/view_grupo',atrib3,controls2);
					 		//set('produccion/update_grupo',atrib,'NULL',true,'produccion/view_grupo',atrib3,'contenido_2',false);
				 		}else{
				 			alerta("Ingrese un contenido válido.","top","atr"+e.data("g"));
				 		}
				 	}else{
				 		alerta("Ingrese un contenido válido.","top","cat"+e.data("g"));
				 	}
		      	}
			 }
			$.fn.confirmar_grupo=function(e){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			confirmar_grupo($(this));
		      		}
		      	});
		      	function confirmar_grupo(e){
					modal("Eliminar","xs","5");
					var atr="this|"+JSON.stringify({g:e.data("g")});
			   		btn_modal('drop_grupo',atr,'',"",'5');
			   		var atrib=new FormData();
			   		atrib.append('id',e.data("g"));
					var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				 	$(this).get_1n('produccion/confirm_grupo',atrib,controls);
			   		//get('produccion/confirm_grupo',atrib,'content_5',true);
		      	}
			}
			$.fn.drop_grupo=function(){
				if($(this).data("g")!=undefined){
					var atrib=new FormData();atrib.append("id",$(this).data("g"));
					var atrib3=new FormData();atrib3.append('id',$("#s2_gru").data("p"));atrib3.append('type',$("#s2_gru").data("type"));atrib3.append('cat',$("#s2_gru").val());atrib3.append('abr',$("#s2_abr").val());if($("#s2_gru").data("type")=="update"){ atrib3.append('idpgr',$("#s2_gru").data("pg"));}
					var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
					$(this).set('produccion/drop_grupo',atrib,controls1,'produccion/view_grupo',atrib3,controls2);
					//set("produccion/drop_grupo",atrib,'NULL',true,'produccion/view_grupo',atrib3,'contenido_2',false,'5');
				}
			}
			$.fn.seleccionar_grupo=function(){
		      	$(this).click(function(){
		      		if($(this).prop("tagName")!="FORM"){
		      			seleccionar_grupo($(this));
		      		}
		      	});
		      	function seleccionar_grupo(e){
					if(e.data('type')=="new"){
						var atrib=new FormData();atrib.append('idp',$("#datos-producto").data("p"));atrib.append('g',e.data("g"));
						var controls1=JSON.stringify({type:"set",preload:false});
						var controls2=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						$(this).set('produccion/adicionar_grupo',atrib,controls1,'produccion/categoria',atrib,controls2);
						//set("produccion/adicionar_grupo",atrib,'NULL',false,'produccion/categoria',atrib,'content_11',false);
					}
					if(e.data('type')=="update"){
						var atrib=new FormData();atrib.append('pg',e.data("pg"));atrib.append('idp',$("#datos-producto").data("p"));atrib.append('g',e.data("g"));
						var controls=JSON.stringify({type:"set",preload:false,closed:"2"});
		 				var controls2=JSON.stringify({id:'titulo_grupo'+e.data("pg"),refresh:false,type:"html"});
	 					var controls3=JSON.stringify({id:'pg'+e.data("pg"),refresh:false,type:"html"});
		 				$(this).set_2n("produccion/update_producto_grupo",atrib,controls,"produccion/titulo_producto_grupo",atrib,controls2,"produccion/producto_grupo",atrib,controls3);
						//set_2n("produccion/update_producto_grupo",atrib,'NULL',false,"produccion/titulo_producto_grupo",atrib,'titulo_grupo'+e.data("pg"),false,'produccion/producto_grupo',atrib,'pg'+e.data("pg"),false,"2");
					}
		      	}
			}
	/*--- End Manejo de grupos ---*/
	/*--- Manejo de colores ---*/
		$.fn.colores=function(e){
		    $(this).click(function(){
		    	if($(this).prop("tagName")!="FORM"){
		    		colores($(this));
		    	}
		    });
		    function colores(e){
				var atrib=new FormData();
				if(e.data("type")=="new"){ modal("COLOR: adicionar","md","2");atrib.append('idpgr',e.data("pg"));}
				if(e.data("type")=="update"){ modal("COLOR: cambiar","md","2");atrib.append('pgc',e.data("pgc"));}
				btn_modal('',"",'',"",'2');
				atrib.append('type',e.data("type"));
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
			 	e.get_2n('produccion/search_color',atrib,controls,'produccion/view_color',atrib,controls2);
				//get_2n('produccion/search_color',atrib,'content_2',false,'produccion/view_color',atrib,'contenido_2',true);
		    }
		}
			$.fn.view_color=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				if($(this).data("type")!=undefined){
		 					if($(this).data("type")=="all"){
		 						$(this).reset_input_2();
		 					}
		 					view_color($(this));
		 				}
		 			}
		 		});
		 		$(this).change(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				view_color($(this));
		 			}
		 		});
			    $(this).submit(function(event){
			    	view_color($(this));
			    	event.preventDefault();
			    });
		 		function view_color(e){
					var atrib3=new FormData();
					atrib3.append('idpgr',$("#s2_gru").data("pg"));
					atrib3.append('type',$("#s2_gru").data("type"));
					atrib3.append('nom',$("#s2_gru").val());
					atrib3.append('abr',$("#s2_abr").val());
					if($("#s2_gru").data("type")=="update"){atrib3.append('pgc',$("#s2_gru").data("pgc"));}
					var controls=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
					$(this).get_1n('produccion/view_color',atrib3,controls);
		 		}
			}
		$.fn.new_color=function(){
		    $(this).click(function(){
		    	if($(this).prop("tagName")!="FORM"){
		    		new_color($(this));
		    	}
		    });
		    function new_color(e){
				modal("COLOR: Nuevo","sm","3");
				btn_modal('save_color',"this",'',"",'3');
				var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
				$(this).get_1n('produccion/new_color',{},controls);
		    }
		}
		$.fn.save_color=function(){
		 	var nom=$("#n3_nom").val();
		 	var abr=$("#n3_abr").val();
		 	var col=$("#n3_col").val();
		 	if(strSpace(nom,2,50)){
		 		if(strSpace(abr,0,10)){
		 			if(col!=""){
		 				var atrib=new FormData();atrib.append('nom',nom);atrib.append('abr',abr);atrib.append('col',col);
				 		var atrib3=new FormData();atrib3.append('idpgr',$("#s2_gru").data("pg"));atrib3.append('type',$("#s2_gru").data("type"));atrib3.append('nom',$("#s2_gru").val());atrib3.append('abr',$("#s2_abr").val());
				 		if($("#s2_gru").data("type")=="update"){ atrib3.append('pgc',$("#s2_gru").data("pgc"));}
				 		var controls1=JSON.stringify({type:"set",preload:true, closed:"3"});
				 		var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
				 		$(this).set('produccion/save_color',atrib,controls1,'produccion/view_color',atrib3,controls2);
				 		//set('produccion/save_color',atrib,'NULL',true,'produccion/view_color',atrib3,'contenido_2',false,'3');
		 			}else{
		 				alerta("Ingrese un contenido válido.","top","n3_col");
		 			}
		 		}else{
		 			alerta("Ingrese un contenido válido.","top","n3_abr");
		 		}
		 	}else{
		 		 alerta("Ingrese un contenido válido.","top","n3_nom");
		 	}
		}
		$.fn.update_color=function(){
			$(this).click(function(){
		    	if($(this).prop("tagName")!="FORM"){
		    		update_color($(this));
		    	}
			});
			$(this).submit(function(event){
		    	update_color($(this));
		    	event.preventDefault();
			});
			function update_color(e){
				var nom=$("#nom"+e.data("co")).val();
				var abr=$("#abr"+e.data("co")).val();
				var col=$("#col"+e.data("co")).val();
				if(strSpace(nom,2,50)){
					if(strSpace(abr,0,10)){
						if(col!=""){
							var atrib=new FormData();atrib.append('id',e.data("co"));atrib.append('nom',nom);atrib.append('abr',abr);atrib.append('col',col);
				 			var atrib3=new FormData();atrib3.append('idpgr',$("#s2_gru").data("pg"));atrib3.append('type',$("#s2_gru").data("type"));atrib3.append('nom',$("#s2_gru").val());atrib3.append('abr',$("#s2_abr").val());
				 			if($("#s2_gru").data("type")=="update"){ atrib3.append('pgc',$("#s2_gru").data("pgc"));}
					 		var controls1=JSON.stringify({type:"set",preload:true});
					 		var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
					 		$(this).set('produccion/update_color',atrib,controls1,'produccion/view_color',atrib3,controls2);
				 			//set('produccion/update_color',atrib,'NULL',true,'produccion/view_color',atrib3,'contenido_2',false);
						}else{
							alerta("Ingrese un contenido válido.","top","col"+e.data("co"));
						}
					}else{
						alerta("Ingrese un contenido válido.","top","atr"+e.data("co"));
					}
				}else{
					alerta("Ingrese un contenido válido.","top","cat"+e.data("co"));
				}
			}
		}



	$.fn.confirmar_color=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		confirmar_color($(this));
		  	}
		});
		function confirmar_color(e){
			if(e.data("co")!=undefined){
				modal("Eliminar","xs","5");
				var atr="this|"+JSON.stringify({co:e.data("co")});
				btn_modal('drop_color',atr,'',"",'5');
				var atrib=new FormData();atrib.append('id',e.data("co"));
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('produccion/confirm_color',atrib,controls);
				//get('produccion/confirm_color',atrib,'content_5',true);
			}
		}
	}
	$.fn.drop_color=function(){
		if($(this).data("co")!=undefined){
			var atrib=new FormData();atrib.append("id",$(this).data("co"));
			var atrib3=new FormData();atrib3.append('idpgr',$("#s2_gru").data("pg"));atrib3.append('type',$("#s2_gru").data("type"));atrib3.append('nom',$("#s2_gru").val());atrib3.append('abr',$("#s2_abr").val());
			if($("#s2_gru").data("type")=="update"){ atrib3.append('pgc',$("#s2_gru").data("pgc"));}
			var controls1=JSON.stringify({type:"set",preload:true, closed:"5"});
			var controls2=JSON.stringify({id:"contenido_2",refresh:false,type:"html"});
			$(this).set('produccion/drop_color',atrib,controls1,'produccion/view_color',atrib3,controls2);
			//set("produccion/drop_color",atrib,'NULL',true,'produccion/view_color',atrib3,'contenido_2',false,'5');
		}
	}
	$.fn.seleccionar_color=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		seleccionar_color($(this));
		  	}
		});
		function seleccionar_color(e){
			if($("#s2_gru").data('type')=="new"){
				var atrib=new FormData();atrib.append('idp',$("#datos-producto").data("p"));atrib.append('pg',$("#s2_gru").data("pg"));atrib.append('pgc',$("#s2_gru").data("pgc"));atrib.append('co',e.data("co"));
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
				var controls=JSON.stringify({type:"set",preload:false});
			 	var controls2=JSON.stringify({id:'pg'+$("#s2_gru").data("pg"),refresh:false,type:"html"});
		 		var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			 	$(this).set_2n("produccion/adicionar_color",atrib,controls,"produccion/producto_grupo",atrib,controls2,"produccion/view_producto",atrib3,controls3);
				//set_2n("produccion/adicionar_color",atrib,'NULL',true,'produccion/producto_grupo',atrib,'pg'+$("#s2_gru").data("pg"),false,"produccion/view_producto",atrib3,"contenido",false);
			}
			if($("#s2_gru").data('type')=="update"){
				var atrib=new FormData();atrib.append('co',e.data("co"));atrib.append('pgc',$("#s2_gru").data("pgc"));
				var atrib2=new FormData();atrib2.append('pgc',$("#s2_gru").data("pgc"));
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
				var controls=JSON.stringify({type:"set",preload:false, closed:"2"});
			 	var controls2=JSON.stringify({id:'titulo_color'+$("#s2_gru").data("pgc"),refresh:false,type:"html"});
		 		var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			 	$(this).set_2n("produccion/update_producto_color",atrib,controls,"produccion/titulo_grupo_color",atrib,controls2,"produccion/view_producto",atrib3,controls3);
				//set_2n("produccion/update_producto_color",atrib,'NULL',true,"produccion/titulo_grupo_color",atrib,'titulo_color'+$("#s2_gru").data("pgc"),false,"produccion/view_producto",atrib3,"contenido",false,"2");
			}
		}
	}
	/*--- End manejo de colores ---*/

			
		/*End manejo de fotografias en el producto*/



	/*-- End materiales en el producto --*/
/*---- END FUNCIONES GENERICAS ----*/
/*---- MANEJO DE PRODUCCION ----*/
	 /*--- Buscador ---*/
	 $.fn.view_produccion=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_produccion($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_produccion($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_produccion($(this));
	    	event.preventDefault();
	    });
	    function view_produccion(e){
	    	var atrib=new FormData();
		 	atrib.append("num",$("input#sp_num").val());
		 	atrib.append("nom",$("input#sp_nom").val());
		 	atrib.append("cli",$("select#sp_cli").val());
		 	atrib.append("est",$("select#sp_est").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('produccion/view_produccion',atrib,controls);
	    }
	 }
	 /*--- End Buscador ---*/
	 /*--- Nuevo ---*/
	 /*--- End Nuevo ---*/
	 /*--- Configuracion ---*/
	 /*--- End Configuracion ---*/
	 /*--- Eliminar ---*/
	 /*--- End Eliminar ---*/
	$.fn.reporte_produccion=function(pe){
   		modal("","lg","11");
   		btn_modal('',"",'',"","11");
   		var atrib=new FormData();atrib.append('pe',pe);
		var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
		$(this).get_1n('produccion/reporte_produccion',atrib,controls);
	}
	$.fn.config_produccion=function(pe){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				config_produccion($(this));
 			}
 		});
 		function config_produccion(e){
 			if(e.data("pe")!=undefined){
				modal("","lg","11");
		   		btn_modal('',"",'',"","11");
				   var atrib=new FormData();
				   atrib.append('pe',e.data("pe"));
				   if(e.data("pp")!=undefined){
					   atrib.append("pp",e.data("pp"));
				   }
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				$(this).get_1n('produccion/configurar_produccion',atrib,controls);
 			}
 		}
	}
	$.fn.parte_pedido_produccion=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		parte_pedido_produccion($(this));
		  	}
		});
		function parte_pedido_produccion(e){
			if(e.data("pp")!=undefined){
		   		modal("","lg","11");
		   		btn_modal('',"",'',"","11");
		   		var atrib=new FormData();atrib.append('pp',e.data("pp"));
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				$(this).get_1n('produccion/parte_pedido_produccion',atrib,controls);
			}
		}
	}
	$.fn.refresh_producto_pedido=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		refresh_producto_pedido($(this));
		  	}
		});
		function refresh_producto_pedido(e){
			if(e.data("p")!=undefined && e.data("pp")!=undefined && e.data("tbl")!=undefined){
				var atrib=new FormData();
				atrib.append("p",e.data("p"));
				atrib.append("pp",e.data("pp"));
				atrib.append("tbl",e.data("tbl"));
				var controls=JSON.stringify({id:"accordion-progress-"+e.data("tbl"),refresh:false,type:"html"});
				var controls2=JSON.stringify({id:"accordion-content-"+e.data("tbl"),refresh:true,type:"html"});
				$(this).get_2n('produccion/progress_producto',atrib,controls,'produccion/colores_producto_pedido',atrib,controls2);
			}
		}
	}
	$.fn.refresh_producto_color_pedido=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		refresh_producto_color_pedido($(this));
		  	}
		});
		function refresh_producto_color_pedido(e){
			if(e.data("p")!=undefined && e.data("pgc")!=undefined && e.data("pp")!=undefined && e.data("dp")!=undefined && e.data("tbl-color")!=undefined  && e.data("tbl-producto")!=undefined){
				console.log("Entro");
				var atrib=new FormData();
				atrib.append("p",e.data("p"));
				atrib.append("pgc",e.data("pgc"));
				atrib.append("pp",e.data("pp"));
				atrib.append("dp",e.data("dp"));
				atrib.append("tbl_color_progress",e.data("tbl-color"));
				atrib.append("tbl_producto_progress",e.data("tbl-producto"));
				//var controls=JSON.stringify({id:"color-progress-"+e.data("tbl"),refresh:false,type:"html"});
				var controls2=JSON.stringify({id:"content-colors-sucursal-"+e.data("tbl-color"),refresh:true,type:"html"});
				//$(this).get_2n('produccion/progress_producto_color',atrib,controls,'produccion/sucursales_color_pedido',atrib,controls2);
				$(this).get_1n('produccion/sucursales_color_pedido',atrib,controls2);
			}
		}
	}
	$.fn.asignar_empleado_sucursal=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		asignar_empleado_sucursal($(this));
		  	}
		});
		function asignar_empleado_sucursal(e){
			if(e.data("sdp")!=undefined && e.data("p")!=undefined && e.data("tbl-color")!=undefined && e.data("tbl-producto")!=undefined){
				modal("Asignar a empleado","xmd","2");
	   			btn_modal('',"",'',"","2");
	   			var atrib=new FormData();
	   			atrib.append('p',e.data("p"));
	   			atrib.append('sdp',e.data("sdp"));
	   			atrib.append('tbl-color',e.data("tbl-color"));
	   			atrib.append('tbl-producto',e.data("tbl-producto"));
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				e.get_1n('produccion/asignar_empleado_sucursal',atrib,controls);		
			}
		}
	}
	$.fn.adicionar_empleado_sucursal_proceso=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		adicionar_empleado_sucursal_proceso($(this));
		  	}
		});
		function adicionar_empleado_sucursal_proceso(e){
			if($("div#datos-procesos-empleados").data("p")!=undefined && $("div#datos-procesos-empleados").data("pgc")!=undefined && e.data("pr")!=undefined && $("div#datos-procesos-empleados").data("sdp")!=undefined){
				modal("Asignar a empleado","md","3");
	   			btn_modal('',"",'',"","3");
	   			var atrib=new FormData();
	   			atrib.append('p',$("#datos-procesos-empleados").data("p"));
	   			atrib.append('pgc',$("#datos-procesos-empleados").data("pgc"));
	   			atrib.append('sdp',$("#datos-procesos-empleados").data("sdp"));
	   			atrib.append('pr',e.data("pr"));
				var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
				e.get_1n('produccion/adicionar_empleado_sucursal_proceso',atrib,controls);
			}
		}
	}
	$.fn.adicionar_producto_empleado=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		adicionar_producto_empleado($(this));
		  	}
		});
		function adicionar_producto_empleado(e){
			if($("div#datos-procesos-empleados").data("pgc")!=undefined && $("div#datos-producto-empleado").data("pr")!=undefined){
				modal("Asignar a empleado","sm","4");
	   			btn_modal('',"",'',"","4");
	   			var atrib=new FormData();
	   			atrib.append('pgc',$("div#datos-procesos-empleados").data("pgc"));
	   			atrib.append('pr',$("div#datos-producto-empleado").data("pr"));
				var controls=JSON.stringify({id:"content_4",refresh:true,type:"html"});
				e.get_1n('produccion/adicionar_producto_empleado',atrib,controls);
			}
		}
	}
	$.fn.add_producto_pedido_empleado=function(){
		$(this).click(function(){
		   	if($(this).prop("tagName")!="FORM"){
		   		add_producto_pedido_empleado($(this));
		   	}
		});
		$(this).submit(function(event){
		   	add_producto_pedido_empleado($(this));
		   	event.preventDefault();
		});
		function add_producto_pedido_empleado(e){
			if(e.data("proe")!=undefined){
				var input=$("input#can-3-"+e.data("proe"));
				if(input.data("pre")!=undefined && input.data("max")!=undefined && $("div#datos-procesos-empleados").data("p")!=undefined && $("div#datos-procesos-empleados").data("sdp")!=undefined && $("div#datos-procesos-empleados").data("tbl-color")!=undefined && $("div#datos-procesos-empleados").data("tbl-producto")!=undefined && $("div#datos-procesos-empleados").data("pgc")!=undefined && $("div#datos-procesos-empleados").data("pp")!=undefined && $("div#datos-procesos-empleados").data("dp")!=undefined){
					var max_value=input.data("max")*1;
					if(input.val()*1>0 && input.val()*1<=input.data("max")){
						var atrib=new FormData();
						atrib.append("cantidad",input.val());
						atrib.append("max",input.data("max"));
						atrib.append("pre",input.data("pre"));
						atrib.append("p",$("div#datos-procesos-empleados").data("p"));
						atrib.append("sdp",$("div#datos-procesos-empleados").data("sdp"));
			   			atrib.append('tbl-color',$("div#datos-procesos-empleados").data("tbl-color"));
			   			atrib.append('tbl-producto',$("div#datos-procesos-empleados").data("tbl-producto"));
			   			var atrib3=new FormData();
			   			atrib3.append("p",$("div#datos-procesos-empleados").data("p"));
						atrib3.append("pgc",$("div#datos-procesos-empleados").data("pgc"));
						atrib3.append("pp",$("div#datos-procesos-empleados").data("pp"));
						atrib3.append("dp",$("div#datos-procesos-empleados").data("dp"));
						atrib3.append("tbl_color_progress",$("div#datos-procesos-empleados").data("tbl-color"));
						atrib3.append("tbl_producto_progress",$("div#datos-procesos-empleados").data("tbl-producto"));
						var controls1=JSON.stringify({type:"set",preload:true, closed:"3"});
						var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						var controls3=JSON.stringify({id:"content-colors-sucursal-"+$("div#datos-procesos-empleados").data("tbl-color"),refresh:false,type:"html"});
						e.set_2n('produccion/add_producto_pedido_empleado',atrib,controls1,'produccion/asignar_empleado_sucursal',atrib,controls2,'produccion/sucursales_color_pedido',atrib3,controls3);
					}else{
						alerta("Ingrese un valor válido mayor a cero y menor o igual a "+max_value,"top","can-3-"+e.data("proe"));
					}
				}
			}
		}
	}
	$.fn.change_producto_empleado=function(){
		$(this).click(function(){
		   	if($(this).prop("tagName")!="FORM"){
		   		change_producto_empleado($(this));
		   	}
		});
		function change_producto_empleado(e){
			if(e.data("ppe")!=undefined && e.data("sdp")!=undefined && e.data("pr")!=undefined && e.data("p")!=undefined){
				modal("Cantidad de producción","sm","3");
				var atr="this|"+JSON.stringify({ppe:e.data("ppe")});
	   			btn_modal('update_producto_pedido_empleado',atr,'',"","3");
				var atrib=new FormData();
				atrib.append("ppe",e.data("ppe"));
				atrib.append("sdp",e.data("sdp"));
				atrib.append("pr",e.data("pr"));
				atrib.append("p",e.data("p"));
				var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
				e.get_1n('produccion/change_producto_empleado',atrib,controls);
			}
		}
	}
	$.fn.update_producto_pedido_empleado=function(){
		if($("div#datos-producto-empleado").data("ppe")!=undefined && $("div#datos-procesos-empleados").data("sdp")!=undefined && $("div#datos-procesos-empleados").data("p")!=undefined){
			var asignado=$("input#can-prod-3");
			if(asignado.data("max")!=undefined && $("textarea#obs-prod-3").val()!=undefined){
				if(asignado.val()>0 && asignado.val()<=asignado.data("max")){
					var obs=$("textarea#obs-prod-3").val();
					if(textarea(obs,0,500)){
						var terminado=$("input#ter-prod-3");
						if(terminado.val()*1>=0 && terminado.val()*1<=asignado.val()*1){
							if($("input#fini-prod-3").val()!=undefined && $("input#ffin-prod-3").val()!=undefined){
								var atrib=new FormData();
								atrib.append("ppe",$("div#datos-producto-empleado").data("ppe"));
								atrib.append("asignado",asignado.val());
								atrib.append("terminado",terminado.val());
								atrib.append("fini",$("input#fini-prod-3").val());
								atrib.append("ffin",$("input#ffin-prod-3").val());
								atrib.append("obs",obs);
								atrib.append("max",asignado.data("max"));
		   						atrib.append('sdp',$("div#datos-procesos-empleados").data("sdp"));
		   						atrib.append('p',$("div#datos-procesos-empleados").data("p"));
								atrib.append("pgc",$("div#datos-procesos-empleados").data("pgc"));
								atrib.append("pp",$("div#datos-procesos-empleados").data("pp"));
								atrib.append("dp",$("div#datos-procesos-empleados").data("dp"));
								atrib.append("tbl-color",$("div#datos-procesos-empleados").data("tbl-color"));
								atrib.append("tbl-producto",$("div#datos-procesos-empleados").data("tbl-producto"));
								atrib.append("tbl_color_progress",$("div#datos-procesos-empleados").data("tbl-color"));
								atrib.append("tbl_producto_progress",$("div#datos-procesos-empleados").data("tbl-producto"));
							 	var atrib3=new FormData();
							 	atrib3.append("num",$("input#sp_num").val());
							 	atrib3.append("nom",$("input#sp_nom").val());
							 	atrib3.append("cli",$("select#sp_cli").val());
							 	atrib3.append("est",$("select#sp_est").val());
								var controls1=JSON.stringify({type:"set",preload:true, closed:"3"});
								var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
								var controls3=JSON.stringify({id:"content-colors-sucursal-"+$("div#datos-procesos-empleados").data("tbl-color"),refresh:false,type:"html"});
								var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
								$(this).set_3n('produccion/update_producto_pedido_empleado',atrib,controls1,'produccion/asignar_empleado_sucursal',atrib,controls2,'produccion/sucursales_color_pedido',atrib,controls3,'produccion/view_produccion',atrib3,controls4);
							}
						}else{
							alerta("Ingrese una valor válido entre 0 y "+asignado.val(),"top","ter-prod-3");
						}
					}else{
						alerta("Ingrese una contenido válido","top","obs-prod-3")
					}
				}else{
					alerta("Ingrese una valor válido entre 1 y "+asignado.data("max"),"top","can-prod-3");
				}
			}
		}
	}
	$.fn.pendiente=function(){
		var cantidad=$("input#can-prod-3").val()*1;
		var entregado=$("input#ter-prod-3").val()*1;
		$("input#pen-prod-3").val(cantidad-entregado);
	}
	$.fn.confirmar_producto_pedido_empleado=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		confirmar_producto_pedido_empleado($(this));
		  	}
		});
		function confirmar_producto_pedido_empleado(e){
			if(e.data("ppe")!=undefined && $("div#datos-procesos-empleados").data("p")!=undefined && $("div#datos-procesos-empleados").data("sdp")!=undefined){
		      	var atrib=new FormData();atrib.append("ppe",e.data("ppe"));
				modal("","xs","5");
			   	var atr="this|"+JSON.stringify({ppe:e.data("ppe")});
			   	btn_modal('drop_producto_pedido_empleado',atr,'',"",'5');
			   	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			   	$(this).get_1n("produccion/confirmar_producto_pedido_empleado",atrib,controls);
			}
		}
	}
	$.fn.drop_producto_pedido_empleado=function(){
		if($(this).data("ppe")!=undefined && $("div#datos-procesos-empleados").data("p")!=undefined && $("div#datos-procesos-empleados").data("sdp")!=undefined){
			var atrib=new FormData();
			atrib.append("ppe",$(this).data("ppe"));
		   	atrib.append('sdp',$("div#datos-procesos-empleados").data("sdp"));
		   	atrib.append('p',$("div#datos-procesos-empleados").data("p"));
			atrib.append("pgc",$("div#datos-procesos-empleados").data("pgc"));
			atrib.append("pp",$("div#datos-procesos-empleados").data("pp"));
			atrib.append("dp",$("div#datos-procesos-empleados").data("dp"));
			atrib.append("tbl-color",$("div#datos-procesos-empleados").data("tbl-color"));
			atrib.append("tbl-producto",$("div#datos-procesos-empleados").data("tbl-producto"));
			atrib.append("tbl_color_progress",$("div#datos-procesos-empleados").data("tbl-color"));
			atrib.append("tbl_producto_progress",$("div#datos-procesos-empleados").data("tbl-producto"));
		 	var atrib3=new FormData();
		 	atrib3.append("num",$("input#sp_num").val());
		 	atrib3.append("nom",$("input#sp_nom").val());
		 	atrib3.append("cli",$("select#sp_cli").val());
		 	atrib3.append("est",$("select#sp_est").val());
			var controls1=JSON.stringify({type:"set",preload:true, closed:"5"});
			var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
			var controls3=JSON.stringify({id:"content-colors-sucursal-"+$("div#datos-procesos-empleados").data("tbl-color"),refresh:false,type:"html"});
			var controls4=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			$(this).set_3n('produccion/drop_producto_pedido_empleado',atrib,controls1,'produccion/asignar_empleado_sucursal',atrib,controls2,'produccion/sucursales_color_pedido',atrib,controls3,'produccion/view_produccion',atrib3,controls4);
		}
	}
	$.fn.detalle_produccion=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		detalle_produccion($(this));
		  	}
		});
		function detalle_produccion(e){
			if(e.data("sdp")!=undefined && e.data("p")!=undefined && e.data("type")!=undefined){
				if(e.data("type")=="cantidad"){modal("Cantidad total en el pedido","sm","2");}
				if(e.data("type")=="pendiente"){modal("Cantidad pendiente en el pedido","sm","2");}
				if(e.data("type")=="produccion"){modal("Cantidad en producción en el pedido","md","2");}
				if(e.data("type")=="terminado"){modal("Cantidad terminada en el pedido","md","2");}
	   			btn_modal('',"",'',"","2");
				var atrib=new FormData();
				atrib.append("p",e.data("p"));
				atrib.append("sdp",e.data("sdp"));
				atrib.append("type",e.data("type"));
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				e.get_1n('produccion/detalle_produccion',atrib,controls);
			}
		}
	}
	$.fn.save_porcentaje=function(){
		$(this).click(function(){
		   	if($(this).prop("tagName")!="FORM"){
		   		save_porcentaje($(this));
		   	}
		});
		$(this).submit(function(event){
		   	save_porcentaje($(this));
		   	event.preventDefault();
		});
		function save_porcentaje(e){
			var input=$("input#up_porcentaje");
			if(input!=undefined && e.data("dp")!=undefined){
				var porcentaje=input.val()*1;
				if(porcentaje>0 && porcentaje<=100){
	      			var controls=JSON.stringify({type:"set",preload:true});
	      			var atrib=new FormData();
	      			atrib.append("porcentaje",porcentaje);
	      			atrib.append("dp",e.data("dp"));
					$(this).set("produccion/save_porcentaje",atrib,controls,"",{},null);
				}else{
					msj("valor inválido");
				}
			}else{
				msj("Error");
			}
		}
	}
/*---- END MANEJO DE PRODUCCION ----*/
/*----- CONFIGURACIONES -----*/
    /*----- Manejo de produccion------*/
    $.fn.extend({
      produccion: function(){
      	if($(this).data("p")!=undefined){
		 	modal("PRODUCTO: Produccion","lg","11");
			btn_modal('',"",'','','11');
			var ele=new FormData();ele.append('p',$(this).data("p"));
			get('produccion/produccion',ele,'content_11',true);
      	}
        return false;
      }
    });
    $.fn.extend({
      refresh_producto_proceso: function(){
      	if($(this).data("tbl")!=undefined){
      		var tbl=$(this).data("tbl");
      		if($("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("pr")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
				var controls=JSON.stringify({id:tbl,refresh:true,type:"html"});
				var atrib=new FormData();atrib.append("tbl",tbl);atrib.append("pgc",$("div#"+tbl).data("pgc"));atrib.append("pr",$("div#"+tbl).data("pr"));atrib.append("ppr",$("div#"+tbl).data("ppr"));
				$(this).get_1n("produccion/tbl_producto_proceso_empleados",atrib,controls);
      		}
      	}
        return false;
      }
    });
    $.fn.extend({
      update_producto_empleado: function(){
      	if($(this).data("proe")!=undefined && $(this).data("rangos")!=undefined && $(this).data("tbl")!=undefined){
      		var tbl=$(this).data("tbl");
      		if($("div#rangos_proe"+$(this).data("rangos"))!=undefined && $("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("pr")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
	      		var calidad=0;
	      		var radio=$("div#rangos_proe"+$(this).data("rangos")+" input[type=radio]");
	      		if(radio.length>0){
		      		var checked=$("div#rangos_proe"+$(this).data("rangos")+" input[type='radio']:checked");
		      		if(checked.length>0){ calidad=checked.data("val");}
		      	}
      			var atrib=new FormData();atrib.append('proe',$(this).data("proe"));atrib.append('calidad',calidad);
      			var controls=JSON.stringify({type:"set",preload:true});
      			var atrib2=new FormData();atrib2.append("tbl",tbl);atrib2.append("pgc",$("div#"+tbl).data("pgc"));atrib2.append("pr",$("div#"+tbl).data("pr"));atrib2.append("ppr",$("div#"+tbl).data("ppr"));
				var controls3=JSON.stringify({id:tbl,refresh:false,type:"html"});
				$(this).set("produccion/update_producto_empleado",atrib,controls,"produccion/tbl_producto_proceso_empleados",atrib2,controls3);
      		}
      	}
        return false;
      }
    });
    $.fn.extend({
      confirmar_producto_empleado: function(){
      	if($(this).data("proe")!=undefined && $(this).data("tbl")!=undefined){
      		var tbl=$(this).data("tbl");
      		if($("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
	      		var atrib=new FormData();atrib.append("proe",$(this).data("proe"));atrib.append("pgc",$("div#"+tbl).data("pgc"));
				modal("","xs","5");
				var atr="this|"+JSON.stringify({proe:$(this).data("proe"),tbl:tbl});
				btn_modal('drop_producto_empleado',atr,'',"",'5');
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n("produccion/confirmar_producto_empleado",atrib,controls);
      		}
      	}
        return false;
      }
    });
    $.fn.extend({
      drop_producto_empleado: function(){
      	if($(this).data("proe")!=undefined && $(this).data("tbl")!=undefined){
      		var tbl=$(this).data("tbl");
      		if($("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("pr")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
			    var atrib=new FormData();atrib.append("proe",$(this).data("proe"));
			    var controls=JSON.stringify({type:"set",preload:true, closed:"5"});
	      		var atrib2=new FormData();atrib2.append("tbl",tbl);atrib2.append("pgc",$("div#"+tbl).data("pgc"));atrib2.append("pr",$("div#"+tbl).data("pr"));atrib2.append("ppr",$("div#"+tbl).data("ppr"));
				var controls3=JSON.stringify({id:tbl,refresh:false,type:"html"});
				$(this).set("produccion/drop_producto_empleado",atrib,controls,"produccion/tbl_producto_proceso_empleados",atrib2,controls3);
      		}
      	}
        return false;
      }
    });

    $.fn.extend({
      view_empleado_proceso: function(){
      	if($(this).data("tbl")!=undefined){
			var tbl=$(this).data("tbl");
      		if($("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("pr")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
			 	modal("PRODUCTO PROCESO: Adicionar empleado","xmd","2");
				btn_modal('',"",'','','2');
				var atrib=new FormData();atrib.append('pgc',$("div#"+tbl).data("pgc"));atrib.append('pr',$("div#"+tbl).data("pr"));atrib.append('ppr',$("div#"+tbl).data("ppr"));atrib.append("tbl",tbl);
				get('produccion/view_empleado_proceso',atrib,'content_2',true);
      		}
      	}
        return false;
      }
    });
    $.fn.add_empleado_proceso=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		add_empleado_proceso($(this));
		  	}
		});
		function add_empleado_proceso(e){
			if(e.data("pre")!=undefined && e.data("type")!=undefined){
				var atrib=new FormData();
				atrib.append('pre',e.data("pre"));
				var calidad=0;
				var radio=$("div#rangos_empleado"+e.data("pre")+" input[type=radio]");
				if(radio.length>0){
					var checked=$("div#rangos_empleado"+e.data("pre")+" input[type='radio']:checked");
					if(checked.length>0){ calidad=checked.data("val");}
				}
				atrib.append('calidad',calidad);
				if(e.data("type")=="producto"){
			      	if($("#datos-empleado-proceso").data("pgc")!=undefined && $("#datos-empleado-proceso").data("ppr")!=undefined && $("#datos-empleado-proceso").data("tbl")!=undefined){
			      		var tbl=$("#datos-empleado-proceso").data("tbl");
			      		if($("div#rangos_empleado"+e.data("pre"))!=undefined && $("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("pr")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
			      			atrib.append('pgc',$("#datos-empleado-proceso").data("pgc"));
			      			var controls=JSON.stringify({type:"set",preload:true});
							var controls3=JSON.stringify({id:tbl,refresh:false,type:"html"});
							var atrib3=new FormData();
							atrib3.append("tbl",tbl);
							atrib3.append("pgc",$("div#"+tbl).data("pgc"));
							atrib3.append("pr",$("div#"+tbl).data("pr"));
							atrib3.append("ppr",$("div#"+tbl).data("ppr"));
							e.set("produccion/add_empleado_proceso",atrib,controls,"produccion/tbl_producto_proceso_empleados",atrib3,controls3);
			      		}
			      	}
				}
				if(e.data("type")=="produccion"){
					if($("div#datos-procesos-empleados").data("p")!=undefined && $("#datos-producto-empleado").data("pr")!=undefined && $("div#datos-procesos-empleados").data("pgc")!=undefined && $("div#datos-procesos-empleados").data("sdp")!=undefined){
			   			atrib.append('p',$("div#datos-procesos-empleados").data("p"));
			   			atrib.append('pgc',$("div#datos-procesos-empleados").data("pgc"));
			   			atrib.append('pr',$("#datos-producto-empleado").data("pr"));
			   			atrib.append('sdp',$("div#datos-procesos-empleados").data("sdp"));
			   			var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
						var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
						e.set("produccion/add_empleado_proceso",atrib,controls,"produccion/adicionar_empleado_sucursal_proceso",atrib,controls2);
					}
				}
			}
		}
    }
    $.fn.extend({
      confirmar_empleado_producto: function(){
      	if($(this).data("pre")!=undefined && $("#datos-empleado-proceso").data("pgc")!=undefined && $("#datos-empleado-proceso").data("ppr")!=undefined && $("#datos-empleado-proceso").data("pr")!=undefined && $("#datos-empleado-proceso").data("tbl")!=undefined){
	      	var atrib=new FormData();atrib.append("pre",$(this).data("pre"));atrib.append("pr",$("#datos-empleado-proceso").data("pr"));
			modal("","xs","5");
			var atr="this|"+JSON.stringify({pre:$(this).data("pre"),"pgc":$("#datos-empleado-proceso").data("pgc"),"ppr":$("#datos-empleado-proceso").data("ppr"),"pr":$("#datos-empleado-proceso").data("pr")});
			btn_modal('drop_empleado_producto',atr,'',"",'5');
			var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			$(this).get_1n("produccion/confirmar_empleado_producto",atrib,controls);
      	}
        return false;
      }
    });
    $.fn.extend({
      drop_empleado_producto: function(){
      	if($(this).data("pre")!=undefined && $(this).data("pgc")!=undefined && $(this).data("ppr")!=undefined && $(this).data("pr")!=undefined && $("#datos-empleado-proceso").data("tbl")!=undefined){
	      	var atrib=new FormData();atrib.append("pre",$(this).data("pre"));atrib.append("pgc",$(this).data("pgc"));atrib.append("ppr",$(this).data("ppr"));atrib.append("pr",$(this).data("pr"));atrib.append("tbl",$("#datos-empleado-proceso").data("tbl"));
	      	var controls=JSON.stringify({type:"set",preload:true, closed:"5"});
			var controls3=JSON.stringify({id:'content_2',refresh:false,type:"html"});
			$(this).set("produccion/drop_empleado_producto",atrib,controls,"produccion/view_empleado_proceso",atrib,controls3);
      	}
        return false;
      }
    });

    $.fn.extend({
      search_empleado: function(){
      	if($("#datos-empleado-proceso").data("pgc")!=undefined && $("#datos-empleado-proceso").data("pr")!=undefined && $("#datos-empleado-proceso").data("ppr")!=undefined){
		 	modal("PROCESO: Adicionar empleado","md","3");
			btn_modal('',"",'','','3');
			var atrib=new FormData();atrib.append("pr",$("#datos-empleado-proceso").data("pr"));
			var controls=JSON.stringify({id:"content_3",refresh:true,type:"html"});
			var controls2=JSON.stringify({id:"view_3",refresh:true,type:"html"});
			$(this).get_2n("produccion/search_empleado",atrib,controls,"produccion/view_empleado",atrib,controls2);
      	}
        return false;
      }
    });
    $.fn.extend({
      view_empleado: function(){
      	if($("#s3_nom").val()!=undefined && $("#datos-empleado-proceso").data("pr")!=undefined && $("#datos-empleado-proceso").data("ppr")!=undefined){
			var atrib=new FormData();atrib.append("pr",$("#datos-empleado-proceso").data("pr"));atrib.append("nom",$("#s3_nom").val());
			var controls2=JSON.stringify({id:"view_3",refresh:true,type:"html"});
			$(this).get_1n("produccion/view_empleado",atrib,controls2);
      	}
        return false;
      }
    });
    $.fn.extend({
      all_empleado: function(){
      	if($("#s3_nom").val()!=undefined && $("#datos-empleado-proceso").data("pr")!=undefined && $("#datos-empleado-proceso").data("ppr")!=undefined){
			$("#s3_nom").val("");
			$(this).view_empleado();
      	}
        return false;
      }
    });
    $.fn.extend({
      adicionar_empleado: function(){
      	if($(this).data("e")!=undefined && $("#s3_nom").val()!=undefined && $("#datos-empleado-proceso").data("pr")!=undefined && $("#datos-empleado-proceso").data("pgc")!=undefined && $("#datos-empleado-proceso").data("tbl")!=undefined){
      		var tip=$("select#tip"+$(this).data("e")).val();
      		if(tip!=undefined){
      			if(tip+""=="0" || tip+""=="1"){
	      			var atrib=new FormData();atrib.append("e",$(this).data("e"));atrib.append("pr",$("#datos-empleado-proceso").data("pr"));atrib.append("tip",tip); atrib.append("pgc",$("#datos-empleado-proceso").data("pgc"));atrib.append("ppr",$("#datos-empleado-proceso").data("ppr"));atrib.append("tbl",$("#datos-empleado-proceso").data("tbl"));
			      	var controls=JSON.stringify({type:"set",preload:true, closed:"5"});
					var controls3=JSON.stringify({id:'content_2',refresh:false,type:"html"});
					$(this).set("produccion/adicionar_empleado",atrib,controls,"produccion/view_empleado_proceso",atrib,controls3);
      			}else{
      				alerta("Seleccione una valor válido","top","tip"+$(this).data("e"));
      			}
      		}
      	}
        return false;
      }
    });
    $.fn.extend({
      save_empleados_producto: function(){
      	if($(this).data("tbl")!=undefined){
      		var tbl=$(this).data("tbl");
      		if($("div#"+tbl+" table td div.rangos").html()!=undefined && $("div#"+tbl).data("pgc")!=undefined && $("div#"+tbl).data("pr")!=undefined && $("div#"+tbl).data("ppr")!=undefined){
      			var proes=[];
      			$("div#"+tbl+" table td div.rangos").each(function(id,rango){
      				if($(rango).attr("id")!=undefined && $(rango).data("proe")!=undefined){
      					var calidad=0;
			      		var radio=$("div#"+$(rango).attr("id")+" input.rango[type=radio]");
			      		if(radio.length>0){
				      		var checked=$("div#"+$(rango).attr("id")+" input.rango[type='radio']:checked");
				      		if(checked.length>0){ calidad=checked.data("val");}
				      		proes[proes.length]={proe:$(rango).data("proe"),calidad:calidad};
				      	}
      				}
      			});
      			var atrib=new FormData();atrib.append("proes",JSON.stringify(proes));
	      		var controls=JSON.stringify({type:"set",preload:true});
				var controls3=JSON.stringify({id:tbl,refresh:false,type:"html"});
				var atrib3=new FormData();atrib3.append("tbl",tbl);atrib3.append("pgc",$("div#"+tbl).data("pgc"));atrib3.append("pr",$("div#"+tbl).data("pr"));atrib3.append("ppr",$("div#"+tbl).data("ppr"));
				$(this).set("produccion/save_empleados_producto",atrib,controls,"produccion/tbl_producto_proceso_empleados",atrib3,controls3);
      		}
      	}
        return false;
      }
    });
    /*----- End manejo de produccion------*/
/*----- END CONFIGURACIONES -----*/
	$.fn.confirm_producto=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		confirm_producto($(this));
		  	}
		});
		function confirm_producto(e){
			if(e.data("p")!=undefined){
				modal("PRODUCTO: Eliminar","xs","5");
				var atr="this|"+JSON.stringify({p:e.data("p")});
		   		btn_modal('drop_producto',atr,'',"",'5');
		   		var atrib=new FormData();atrib.append('id',e.data("p"));
				var controls2=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n("produccion/confirmar_producto",atrib,controls2);
		   		//get('produccion/confirmar_producto',atrib,'content_5',true);
			}
		}
	}
   	$.fn.drop_producto=function(){
			if($(this).data("p")!=undefined){
				console.log($(this).data("p"));
				var atrib=new FormData();atrib.append('id',$(this).data("p"));atrib.append('u',$("#e_user").val());atrib.append('p',$("#e_password").val());
				var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html",});
				$(this).set("produccion/drop_producto",atrib,controls,"produccion/view_producto",atrib3,controls3);
			}
	}
	/*----END ELIMINAR PRODUCTO----*/drop_producto
/*END MANEJO DE PRODUCCION*/
/*---- CONFIGURACION ----*/
	/*--- HOJA DE COSTOS ----*/
    $.fn.view_producto_config=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				if($(this).data("type")!=undefined){
	 				if($(this).data("type")=="all"){
	 					$("input#nom").val("");
	 				}
	 				view_producto_config($(this));
	 			}
			}
		});
		$(this).submit(function(event){
		   	view_producto_config($(this));
		   	event.preventDefault();
		});
		function view_producto_config(e){
	      	if($("input#nom")!=undefined){
	      		var atrib=new FormData();atrib.append("nom",$("input#nom").val());
	 			var controls1=JSON.stringify({id:"list-producto-config",refresh:true,type:"html"});
	 			e.get_1n('produccion/view_producto_config',atrib,controls1);
	      	}
		}
	}
    $.fn.extend({
      all_producto_config: function(){
      	$("input#nom").val("");
      	$(this).view_producto_config();
        return false;
      }
    });
    $.fn.extend({
      save_costo: function(){
      	if($(this).data("pgc")!=undefined){
      		var pgc=$(this).data("pgc");
      		var costo=$("input#cos"+pgc).val();
      		if(decimal(costo,6,1) && (costo*1)>=0 && (costo*1)<999999.9){
      			var atrib=new FormData();atrib.append("pgc",pgc);atrib.append("costo",costo);
		      	var controls=JSON.stringify({type:"set",preload:true,callback:JSON.stringify({c:"costo_promedio"})});
		      	$(this).set("produccion/save_costo",atrib,controls,'',{},null);
      		}else{
      			alerta("Ingrese un valor válido","top","cos"+pgc);
      		}
      	}
        return false;
      }
    });
    $.fn.extend({
      costo_promedio: function(){
      	var idp=$(this).data("p");
      	if(idp!=undefined){
      		var promedio=0;
      		var costos=$("div#accordion-panel"+idp+" table tr input.input-150");
      		if(costos.length>0){
	      		costos.each(function(index,input){ promedio+=($(input).val()*1); });
	      		promedio/=(costos.length);
      		}
      		promedio=number_format(promedio,1,'.','');
      		$("div#accordion-panel"+idp+" label span.promedio").html(number_format(promedio,2,'.',''));
      	}
        return false;
      }
    });
	/*--- END HOJA DE COSTOS ----*/
/*---- END CONFIGURACION ----*/
})(jQuery);
function inicio(){
	$('a#producto').click(function(){$(this).get_2n('produccion/search_producto',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_producto',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('producto','Productos','produccion?p=1');});
	$('div#producto').click(function(){$(this).get_2n('produccion/search_producto',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_producto',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('producto','Productos','produccion?p=1');});
	$('a#produccion').click(function(){$(this).get_2n('produccion/search_produccion',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_produccion',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('produccion','Producción','produccion?p=2');});
	$('div#produccion').click(function(){$(this).get_2n('produccion/search_produccion',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_produccion',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('produccion','Producción','produccion?p=2');});
	$('a#seguimiento').click(function(){$(this).get_2n('produccion/search_seguimiento',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_seguimiento',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('seguimiento','Seguimiento de producción','produccion?p=3');});
	$('div#seguimiento').click(function(){$(this).get_2n('produccion/search_seguimiento',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_seguimiento',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('seguimiento','Seguimiento de producción','produccion?p=3');});
	$('a#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('config','Configuración de producción','produccion?p=5');});
	$('div#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'produccion/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('config','Configuración de producción','produccion?p=5');});
}
function drop_producto(e){e.drop_producto();}






function view_producto(){ $(this).view_producto();}
function all_producto(){ $(this).all_producto();}
function save_producto(e){e.save_producto();}
function save_atributo(e){e.save_atributo();}
function drop_atributo(e){e.drop_atributo();}
function reportes(id,e){e.reportes(id);}
function configuracion_producto(idp){ $(this).configuracion_producto(idp);}
function drop_material(e){e.drop_material();}
function save_fotografias(e){e.save_fotografias();}
function update_fotografia(e){e.update_fotografia();}
function drop_fotografia(e){e.drop_fotografia();}
function save_repujado_sello(e){e.save_repujado_sello();}
function update_repujado_sello(e){e.update_repujado_sello();}
function drop_repujado_sello(e){e.drop_repujado_sello();}
function save_pieza(e){e.save_pieza();}
function update_pieza(e){e.update_pieza();}
function drop_pieza(e){e.drop_pieza();}
function save_proceso(e){e.save_proceso();}
function drop_proceso(e){e.drop_proceso();}
function drop_producto_proceso(e){e.drop_producto_proceso();}
function update_producto(e){e.update_producto();}
function save_grupo(e){e.save_grupo();}
function drop_grupo(e){e.drop_grupo();}
function drop_producto_grupo(e){ e.drop_producto_grupo();}
function save_color(e){e.save_color();}
function drop_color(e){e.drop_color();}
function drop_grupo_color(e){ e.drop_grupo_color();}
//function view_produccion(){$(this).view_produccion();}
//function all_produccion(){$(this).all_produccion();}
//function configurar_produccion(pe){$(this).configurar_produccion(pe);}
//function reporte_produccion(pe){$(this).reporte_produccion(pe);}
function update_producto_pedido_empleado(e){ e.update_producto_pedido_empleado();}
function pendiente(e){ e.pendiente();}
function drop_producto_pedido_empleado(e){ e.drop_producto_pedido_empleado();}







function drop_empleado_producto(e){ e.drop_empleado_producto();}
function drop_producto_empleado(e){ e.drop_producto_empleado();}











/*------- MANEJO DE PRODUCTOS -------*/
   	/*--- Buscador ---*/
   	/*function view_producto(){
   		var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
   		get('produccion/view_producto',atrib3,'contenido',true);
   		return false;
   	}
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*function all_producto(){
   		reset_input("");
   		view_producto();
   	}
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
	/* function new_producto(){
	   	modal("PRODUCTO: Nuevo","lg","1");
		btn_modal('save_producto',"this",'',"",'1');
		var ele=new FormData();
		get('produccion/new_producto',{},'content_1',true);
	 }
	 function save_producto(e){
	 	var cod=$('#cod_1').val();
	 	var nom=$('#nom').val();
	 	var fec=$('#fec').val();
	 	var obs=$('#obs').val();	
		var cod2=$('#cod_2').val();
	 	if(strNoSpace(cod,2,20)){
			if(strSpace(nom,2,90)){
				var control=true;
				if(fec!=""){if(!fecha(fec)){ control=false; alerta('Ingrese una fecha válida','top','fec');}}
				if(obs!=""){ if(!textarea(obs,0,500)){ control=false;alerta('Ingrese una valor válido','top','obs');}}
				if(cod2!=""){ if(!strSpace(cod2,0,20)){ alerta('Ingrese un código de producto','top','cod_2'); control=false;}}
				$("#form_atr textarea.form-control").each(function(i,ele){
					if($(ele).val()!=""){if(!textarea($(ele).val(),0,150)){ control=false;alerta('Ingrese una valor válido no vacio','top',$(ele).attr('id'));}}
				});
				if(control){
					var vec=[];
					$("#form_atr textarea.form-control").each(function(i,ele){vec[vec.length]={id:$(ele).data("a"), valor:$(ele).val()}});
					var controls=JSON.stringify({type:"set",preload:true,closed:"1"});
					var atrib=new FormData();atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('fec',fec);atrib.append('obs',obs);atrib.append('atrib',JSON.stringify(vec));atrib.append("cod2",cod2);
					var controls1=JSON.stringify({id:"contenido",refresh:true,type:"html"});
					var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					e.set("produccion/save_producto",atrib,controls,'produccion/view_producto',atrib3,controls1);
				}
			}else{
				alerta('Ingrese un nombre de producto válido','top','nom');
			}
		}else{
			alerta('Ingrese un código de producto','top','cod_1');
		}
		return false;
	 }
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*function imprimir_productos(json){
   		modal("PRODUCTOS: Configuracion de impresion","xlg","1");
 		btn_modal('',"",'imprimir',"'area'",'1','xlg');
 		var atrib=new FormData();atrib.append('json',json);
 		get_2n('produccion/config_imprimir_productos',atrib,'content_1',true,'produccion/imprimir_productos',atrib,'area',true);
   	}
   	function arma_productos(json){
		var v1=""; if($("#1:checked").val()){v1='ok'}
		var v2=""; if($("#2:checked").val()){v2='ok'}
		var v3=""; if($("#3:checked").val()){v3='ok'}
		var v4=""; if($("#4:checked").val()){v4='ok'}
		var v5=""; if($("#5:checked").val()){v5='ok'}
		var v6=""; if($("#6:checked").val()){v6='ok'}
		var v7=""; if($("#7:checked").val()){v7='ok'}
		var v8=""; if($("#8:checked").val()){v8='ok'}
		var v9=""; if($("#9:checked").val()){v9='ok'}
		var v10=""; if($("#10:checked").val()){v10='ok'}
		var v11=""; if($("#11:checked").val()){v11='ok'}
		var nro=$("#nro").val();
		var atrib=new FormData();
		atrib.append('json',json);
		atrib.append('v1',v1);
		atrib.append('v2',v2);
		atrib.append('v3',v3);
		atrib.append('v4',v4);
		atrib.append('v5',v5);
		atrib.append('v6',v6);
		atrib.append('v7',v7);
		atrib.append('v8',v8);
		atrib.append('v9',v9);
		atrib.append('v10',v10);
		atrib.append('v11',v11);
		atrib.append('nro',nro);
		get('produccion/imprimir_productos',atrib,'area',true);
	}
   	/*--- End Imprimir ---*/
   	/*function img_producto(e){
	    modal("",null,"6");
	    var atrib=new FormData();atrib.append('p',e.data("p"));
	    get('produccion/img_producto',atrib,'content_6',false);
	}
   	function img_producto_grupo_color(e){
	    modal("",null,"6");
	    var atrib=new FormData();atrib.append('pgc',e.data("pgc"));
	    get('produccion/img_producto_grupo_color',atrib,'content_6',false);
	}
   	function img_material(e){
	    modal("",null,"6");
	    var atrib=new FormData();atrib.append('idmi',e.data("mi"));
	    get('material/img_material',atrib,'content_6',false);
	}
   	/*--- Reportes ---*/
   		/*----FICHA TECNICA DEL PRODUCTO----*/
		/*function reportes(id,e){
		 	modal("PRODUCTO: Detalle","lg","1");
		 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		 	var atrib=new FormData();atrib.append('p',id);
			if(isset(e)){ if(e.data("type")!="refresh"){ $("#content_1").html(""); } }else{ $("#content_1").html("");}
		 	get('produccion/ficha_tecnica',atrib,'content_1',true);
		}
		function ficha_tecnica(e){
		 	modal("PRODUCTO: Detalle","lg","1");
		 	btn_modal('',"",'imprimir',"'area'",'1','lg');
		 	var atrib=new FormData();atrib.append('p',e.data("p"));atrib.append('v1',"");
		 	if(isset(e)){ if(e.data("type")!="refresh"){ $("#content_1").html(""); } }else{ $("#content_1").html("");}
		 	get('produccion/ficha_tecnica',atrib,'content_1',true);
		}
		function ficha_tecnica_color(id,v1){
		 	modal("PRODUCTO: Detalle","lg","2");
		 	btn_modal('',"",'imprimir',"'area2'",'2');
		 	var atrib=new FormData();atrib.append('pgc',id);
		 	if(isset(v1)){atrib.append('v1',v1);}
		 	get('produccion/ficha_tecnica_color',atrib,'content_2',false);
		}
		/*----END FICHA TECNICA DEL PRODUCTO----*/
   	/*--- configuracion ---*/
   		/*----MODIFICAR DATOS DE PRODUCTO----*/
		/*function config_producto(idp,e){
		 	modal("PRODUCTO: Configuración","lg","11");
			btn_modal('update_producto',"'"+idp+"'",'','','11');
			var ele=new FormData();ele.append('idp',idp);
			get('produccion/config_producto',ele,'content_11',true);
		}
		function update_producto(idp){
		 	var cod=$('#cod_1').val();
		 	var nom=$('#nom').val();
		 	var fec=$('#fec').val();
		 	var obs=$('#obs').val();
		 	var cod2=$('#cod_2').val();
		 		if(strNoSpace(cod,2,20)){
					if(strSpace(nom,2,90)){
						var control=true;
						if(fec!=""){if(!fecha(fec)){ control=false; alerta('Ingree una fecha válida','top','fec');}}
						if(obs!=""){ if(!textarea(obs,0,500)){ control=false;alerta('Ingrese una valor válido','top','obs');}}
						if(cod2!=""){ if(!strSpace(cod2,0,20)){ control=false;alerta('Ingrese una valor válido','top','cod_2');}}
						$("#form_atr textarea.form-control").each(function(i,ele) {
							if($(ele).val()!=""){if(!textarea($(ele).val(),0,150)){ control=false;alerta('Ingrese una valor válido','top',$(ele).attr('id'));}}
						});
						if(control){
							var vec_n=[];
							var vec_u=[];
							$("#form_atr textarea.form-control").each(function(i,ele){
								if($(ele).data('typele')=="child"){
									vec_n[vec_n.length]={id:$(ele).data("a"), valor:$(ele).val()}
								}
								if($(ele).data('typele')=="db"){
									vec_u[vec_u.length]={id:$(ele).data("prod-atr"), valor:$(ele).val()}
								}
							});
							var atrib=new FormData();atrib.append('idp',idp);atrib.append('cod',cod);atrib.append('nom',nom);atrib.append('fec',fec);atrib.append('obs',obs);atrib.append('cod2',cod2);
							atrib.append('atrib_n',JSON.stringify(vec_n));
							atrib.append('atrib_u',JSON.stringify(vec_u));
							atrib.append('atrib_d',$("#control-delete").val());
							var atrib3=new FormData();atrib3.append('cod',$("#s_cod").val());atrib3.append('cod2',$("#s_cod2").val());atrib3.append('nom',$("#s_nom").val());atrib3.append('fec',$("#s_fec").val());atrib3.append('est',$("#s_est").val());
					 		set_2n('produccion/update_producto',atrib,'NULL',true,'produccion/config_producto',atrib,'content_11',true,'produccion/view_producto',atrib3,'contenido',false);
						}
					}else{
						alerta('Ingrese un nombre de producto válido','top','nom');
					}
				}else{
					alerta('Ingrese un código de producto válido','top','cod_1');
				}
			return false;
		}
		/*----END MODIFICAR DATOS DE PRODUCTO----*/
		/*----MODIFICAR CATEGORIAS DEL PRODUCTO----*/

		/*----END MODIFICAR CATEGORIAS DEL PRODUCTO----*/
		/*----MODIFICAR MATERIALES DEL PRODUCTO----*/
		function material(e){
		 	modal("PRODUCTO: Configuración de materiales","lg","1");
			btn_modal('',"",'','','1');
			var ele=new FormData();ele.append('p',e.data('a'));
			get('produccion/material',ele,'content_1',true);
		}
		function refresh_color_material(e){
			var atrib=new FormData();atrib.append('pgc',e.data("pgc"));
			get('produccion/color_producto',atrib,'pgc'+e.data("pgc"),true);
		}
		function update_color_material(e){
			var m=$("#color-mat"+e.data("pgcm")).val();
			var c=($("#color-cant"+e.data("pgcm")).val()*1)+"";
			if(entero(m,0,10)){
				if(decimal(c,7,4) && c>0 && c<=9999999.9999){
					var atrib=new FormData();atrib.append('m',m);atrib.append('c',c);atrib.append('pgcm',e.data("pgcm"));atrib.append('pgc',e.data("pgc"));
					//console.log(e.data("refresh"));
					if(e.data("refresh")){
						set("produccion/update_color_material",atrib,'NULL',true,'produccion/color_producto',atrib,'pgc'+e.data("pgc"),false);
					}else{
						set("produccion/update_color_material",atrib,'NULL',true,'',{},"",false);
					}
				}else{
					alerta("Ingrese una cantidad válida mayor a cero y menor a 9999999","top","color-cant"+e.data("pgcm"));
				}
			}else{
				alerta("Seleccione una material","top","color-mat"+e.data("pgcm"));
			}
			return false;
		}
		function confirm_color_material(e){
			modal("Eliminar","xs","5");
			clear_datas("btn_52");
			var atr="this|"+JSON.stringify({pgcm:e.data("pgcm"),pgc:e.data("pgc")});
			btn_modal('drop_color_material',atr,'',"",'5');
			var atrib=new FormData();atrib.append('pgcm',e.data("pgcm"));atrib.append('pgc',e.data("pgc"));
			get('produccion/confirm_color_material',atrib,'content_5',true);
		}
		function drop_color_material(e){
			var atrib=new FormData();atrib.append('pgcm',e.data("pgcm"));atrib.append('pgc',e.data("pgc"));
			set("produccion/drop_color_material",atrib,'NULL',true,'produccion/color_producto',atrib,'pgc'+e.data("pgc"),false,'5');
		}
		/*----END MODIFICAR MATERIALES DEL PRODUCTO----*/
			/*----ELIMINAR PRODUCTO----*/

	/*----END ELIMINAR PRODUCTO----*/
/*------ CONFIGURACION ---------*/
(function($){

})(jQuery);
/*------ END CONFIGURACION ---------*/
/*----- FUNCIONES GENERICAS--------*/







		
	/*----- END FUNCIONES GENERICAS--------*/

