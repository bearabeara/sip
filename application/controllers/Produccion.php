<?php if(!defined('BASEPATH'))exit('No direct script access allowed');
class Produccion extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if($privilegio[0]->pr=="1"){
				if(!isset($_GET['p'])){
					if($privilegio[0]->pr1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->pr2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->pr5r==1){ 
								$listado['pestania']=5;
							}else{
								$listado['pestania']=0;
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				date_default_timezone_set("America/La_Paz");
				if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
				$this->load->view('v_produccion',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE PRODUCTOS -------*/
	public function search_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
			if($privilegio){
				$listado['privilegio']=$privilegio;
				$this->load->view('produccion/producto/search',$listado);
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
			if(!empty($privilegio)){
				$estado="";
				if(isset($_POST['est'])){ if($_POST['est']==0 || $_POST['est']==1){ $estado=trim($_POST['est']);}}else{ $estado="1";}
				$atrib="";$val="";$type_search="";
				if(isset($_POST['cod']) && isset($_POST['cod2']) && isset($_POST['nom']) && isset($_POST['fec'])){
					if($_POST['cod']!=""){
						$atrib='codigo';$val=$_POST['cod'];$type_search="like";
					}else{
						if($_POST['cod2']!=""){
							$atrib='codigo_aux';$val=$_POST['cod2'];$type_search="like";
						}else{
							if($_POST['nom']!=""){
								$atrib='nombre';$val=trim($_POST['nom']);$type_search="like";
							}else{
								if($_POST['fec']!=""){
									$atrib='fecha_creacion';$val=$_POST['fec'];$type_search="equals";
								}
							}
						}
					}
				}
				$listado['productos']=$this->M_producto->get_search("","","");
				$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("","");
				$listado['privilegio']=$privilegio[0];
				$listado['atrib']=$atrib;
				$listado['val']=$val;
				$listado['type_search']=$type_search;
				$listado['estado']=$estado;
				$this->load->view('produccion/producto/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function change_estado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p'])){
				$idp=trim($_POST['p']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					if($producto[0]->estado==1){$estado=0; $msj="cambió el estado del producto a <strong>inactivo</strong>";}else{$estado=1;$msj="cambió el estado del producto a <strong>activo</strong>";}
					if($this->M_producto->modificar_row($idp,"estado",$estado)){
						$prod=$producto[0]->codigo." ".$producto[0]->nombre;
						if($this->M_producto_seg->insertar($idp,$prod,"u",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
						$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
						foreach($resultados as $key => $result){
							if($result->idno=="none"){
								if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
							}else{
								if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
							}
						}
						$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
						echo json_encode($result);
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
	public function new_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view('produccion/producto/3-nuevo/view');
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_producto(){//guardamos el producto
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['cod']) && isset($_POST['cod2']) && isset($_POST['nom']) && isset($_POST['fec']) && isset($_POST['obs']) && isset($_POST['atrib'])){
				$cod=trim($_POST['cod']);
				$nom=trim($_POST['nom']);
				$fec=trim($_POST['fec']);
				$obs=trim($_POST['obs']);
				$cod2=trim($_POST['cod2']);
				$atribs=json_decode($_POST['atrib']);
				if($this->val->strSpace($nom,2,90) && $this->val->strNoSpace($cod,2,20) && $this->val->strSpace($cod2,0,20)){
					$control=true;
					if($fec!=""){if(!$this->val->fecha($fec)){$control=false;}}
					if($obs!=""){if(!$this->val->textarea($obs."",0,500)){$control=false;}}
					if($control){
						foreach($atribs as $key => $atrib){
							if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
						}
					}
					if($control){
						foreach($atribs as $key => $atrib){
							$aux=$this->M_atributo->get($atrib->id);
							if(empty($aux)){ $control=false; break;}
							if(!$this->val->textarea($atrib->valor,0,150)){ $control=false;}
						}
					}
					if($control){
						$idp=$this->M_producto->max('idp')+1;
						if($this->M_producto->insertar($idp,$cod,$nom,$fec,$obs,$cod2)){
							foreach($atribs as $key => $atrib){
								if($this->M_producto_atributo->insertar($idp,$atrib->id,$atrib->valor)){}
							}
							$prod=$cod." ".$nom;
							if($this->M_producto_seg->insertar($idp,$prod,"c","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){
								//notificacion
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
								foreach ($resultados as $key => $result) {
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	public function img_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p'])){
				$idp=trim($_POST['p']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					$imagenes=$this->M_producto_imagen->get_row("idp",$idp);
					$url=base_url()."libraries/img/productos/";
					$img="default.png";
					$archivos = array();
					if(!empty($imagenes)){
						for ($i=0; $i < count($imagenes) ; $i++) { 
							$img=$imagenes[$i]->archivo;
							$titulo=$producto[0]->nombre;
							$descripcion=$imagenes[$i]->descripcion;
							$archivos[] = array('titulo' => $titulo,'url' =>  $url.$img,'descripcion' => $descripcion);
						}
					}else{
						$archivos[] = array('titulo' => "",'url' =>  $url.$img,"descripcion"=>"");
					}
					$listado['imagenes']=json_decode(json_encode($archivos));
					$this->load->view('estructura/visor',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function img_producto_grupo_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pgc'])){
				$idpgrc=trim($_POST['pgc']);
					$imagenes=$this->M_producto_imagen_color->get_imagen('pig.idpgrc',$idpgrc);
					$url=base_url()."libraries/img/productos/";
					$img="default.png";
					$archivos = array();
					if(!empty($imagenes)){
						for ($i=0; $i < count($imagenes) ; $i++) { 
							$img=$imagenes[$i]->archivo;
							$titulo=$imagenes[$i]->titulo;
							$descripcion=$imagenes[$i]->descripcion;
							$archivos[] = array('titulo' => $titulo,'url' =>  $url.$img,'descripcion' => $descripcion);
						}
					}else{
						$archivos[] = array('titulo' => "",'url' =>  $url.$img,"descripcion"=>"");
					}
					$listado['imagenes']=json_decode(json_encode($archivos));
					$this->load->view('estructura/visor',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Imprimir ---*/
   	public function print_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['cod']) && isset($_POST['cod2']) && isset($_POST['nom']) && isset($_POST['fec']) && isset($_POST['est'])){
				$listado['visibles']=json_decode($_POST['visibles']);
				$listado['productos']=$this->M_producto->get_search("","","");
				$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("","");
				$listado['tbl']=trim($_POST['tbl']);
				$listado['cod']=trim($_POST['cod']);
				$listado['cod2']=trim($_POST['cod2']);
				$listado['nom']=trim($_POST['nom']);
				$listado['fec']=trim($_POST['fec']);
				$listado['est']=trim($_POST['est']);
				$this->load->view('produccion/producto/4-imprimir/print_producto',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
		public function excel_productos(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_GET['cod']) && isset($_GET['cod2']) && isset($_GET['nom']) && isset($_GET['fec']) && isset($_GET['est'])){
						$estado="";
						if(isset($_GET['est'])){ if($_GET['est']==0 || $_GET['est']==1){ $estado=trim($_GET['est']);}}else{ $estado="1";}
						$atrib="";$val="";$type_search="";
						if(isset($_GET['cod']) && isset($_GET['cod2']) && isset($_GET['nom']) && isset($_GET['fec'])){
							if($_GET['cod']!=""){
								$atrib='codigo';$val=$_GET['cod'];
							}else{
								if($_GET['cod2']!=""){
									$atrib='codigo_aux';$val=$_GET['cod2'];
								}else{
									if($_GET['nom']!=""){
										$atrib='nombre';$val=trim($_GET['nom']);
									}else{
										if($_GET['fec']!=""){
											$atrib='fecha_creacion';$val=$_GET['fec'];
										}
									}
								}
							}
						}
						$listado['productos']=$this->M_producto->get_search($atrib,$val,$estado);
						$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("","");
						$this->load->view('produccion/producto/4-imprimir/excel_productos',$listado);
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function word_productos(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_GET['cod']) && isset($_GET['cod2']) && isset($_GET['nom']) && isset($_GET['fec']) && isset($_GET['est'])){
						$estado="";
						if(isset($_GET['est'])){ if($_GET['est']==0 || $_GET['est']==1){ $estado=trim($_GET['est']);}}else{ $estado="1";}
						$atrib="";$val="";$type_search="";
						if(isset($_GET['cod']) && isset($_GET['cod2']) && isset($_GET['nom']) && isset($_GET['fec'])){
							if($_GET['cod']!=""){
								$atrib='codigo';$val=$_GET['cod'];
							}else{
								if($_GET['cod2']!=""){
									$atrib='codigo_aux';$val=$_GET['cod2'];
								}else{
									if($_GET['nom']!=""){
										$atrib='nombre';$val=trim($_GET['nom']);
									}else{
										if($_GET['fec']!=""){
											$atrib='fecha_creacion';$val=$_GET['fec'];
										}
									}
								}
							}
						}
						$listado['productos']=$this->M_producto->get_search($atrib,$val,$estado);
						$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("","");
						$this->load->view('produccion/producto/4-imprimir/word_productos',$listado);
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   		/*-FICHA TECNICA DEL PRODUCTO-*/
		public function ficha_tecnica(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['p'])){
					$idp=trim($_POST['p']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$listado['producto']=$producto[0];
						$listado['producto_atributos']=$this->M_producto_atributo->get_atributo('pa.idp',$idp);
						$listado['producto_materiales']=$this->M_producto_material->get_material('pm.idp',$idp);
						$listado['producto_piezas']=$this->M_producto_pieza->get_row('idp',$idp);
						$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp);
						$listado['unidades']=$this->M_unidad->get_all();
						$listado['all_materiales']=$this->M_material_item->get_material("","");
						$listado['all_repujados_sellos']=$this->M_repujado_sello->get_all();
						if(isset($_POST['pgc'])){$listado['pgc']=trim($_POST['pgc']);}
						$this->load->view('produccion/producto/5-reporte/ficha_tecnica',$listado);
					}else{

						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function excel_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_GET['p']) && isset($_GET['pgc'])){
					$idp=trim($_GET['p']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$listado['producto']=$producto[0];
						$listado['producto_atributos']=$this->M_producto_atributo->get_atributo('pa.idp',$idp);
						$listado['producto_materiales']=$this->M_producto_material->get_material('pm.idp',$idp);
						$listado['producto_piezas']=$this->M_producto_pieza->get_row('idp',$idp);
						$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp);
						$listado['unidades']=$this->M_unidad->get_all();
						$listado['all_materiales']=$this->M_material_item->get_material("","");
						$listado['all_repujados_sellos']=$this->M_repujado_sello->get_all();
						$listado['idpgrc']=trim($_GET['pgc']);
						$this->load->view('produccion/producto/5-reporte/excel_producto',$listado);
					}else{

						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function word_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_GET['p']) && isset($_GET['pgc'])){
					$idp=trim($_GET['p']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$listado['producto']=$producto[0];
						$listado['producto_atributos']=$this->M_producto_atributo->get_atributo('pa.idp',$idp);
						$listado['producto_materiales']=$this->M_producto_material->get_material('pm.idp',$idp);
						$listado['producto_piezas']=$this->M_producto_pieza->get_row('idp',$idp);
						$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp);
						$listado['unidades']=$this->M_unidad->get_all();
						$listado['all_materiales']=$this->M_material_item->get_material("","");
						$listado['all_repujados_sellos']=$this->M_repujado_sello->get_all();
						$listado['idpgrc']=trim($_GET['pgc']);
						$this->load->view('produccion/producto/5-reporte/word_producto',$listado);
					}else{

						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		/*-END FICHA TECNICA DEL PRODUCTO-*/
   	/*--- configuracion ---*/
	public function config_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idp'])){
				$idp=trim($_POST['idp']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					$listado['producto']=$producto[0];
					$listado['imagenes']=$this->M_producto_imagen->get_row('idp',$idp);
					$listado['materiales']=$this->M_producto_material->get_search_producto($idp,"","");
					$listado['piezas']=$this->M_producto_pieza->get_row('idp',$idp);
					$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","ASC");
					$listado['producto_grupo_procesos']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_procesos']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['unidades']=$this->M_unidad->get_all();
					$this->load->view('produccion/producto/6-config/modificar',$listado);

				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function update_producto(){//guardar modificar producto
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idp']) && isset($_POST['cod']) && isset($_POST['cod2']) && isset($_POST['nom']) && isset($_POST['fec']) && isset($_POST['obs']) && isset($_POST['atrib_n']) && isset($_POST['atrib_u']) && isset($_POST['atrib_d'])){
				$idp=trim($_POST['idp']);
				$cod=trim($_POST['cod']);
				$nom=trim($_POST['nom']);
				$fec=trim($_POST['fec']);
				$obs=trim($_POST['obs']);
				$cod2=trim($_POST['cod2']);
				$atribs_new=json_decode($_POST['atrib_n']);
				$atribs_update=json_decode($_POST['atrib_u']);
				$atribs_delete=explode("|",$_POST['atrib_d']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto) && $this->val->strSpace($cod,2,20) && $this->val->strSpace($nom,2,90) && $this->val->strSpace($cod2,0,20)){
					$control=true;
					if($fec!=""){if(!$this->val->fecha($fec)){$control=false;}}
					if($obs!=""){if(!$this->val->textarea($obs,0,500)){$control=false;}}
					if($control){
						foreach($atribs_new as $key => $atrib){
							if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
						}
					}
					if($control){
						foreach($atribs_update as $key => $atrib){
							if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
						}
					}
					if($control){
						foreach($atribs_new as $key => $atrib){
							$aux=$this->M_atributo->get($atrib->id);
							if(empty($aux)){ $control=false; break;}
							if(!$this->val->textarea($atrib->valor,0,150)){ $control=false;}
						}
					}
					if($control){
						if($this->M_producto->modificar($idp,$cod,$nom,$fec,$obs,$cod2)){
							$msja="";
							$ca_del=0;$ca_add=0;
							for($i=0; $i < count($atribs_delete) ; $i++){ if($this->M_producto_atributo->eliminar($atribs_delete[$i])){ $ca_del++;} }
							if($_POST['atrib_d']!=""){ if($ca_del>0){ $msja="Eliminó ".$ca_del." atributo";if($ca_del>0){$msja.="s";}}}
							foreach($atribs_update as $key => $atrib){
								$producto_atributo=$this->M_producto_atributo->get($atrib->id);
								if(!empty($producto_atributo)){
									if($atrib->valor!=$producto_atributo[0]->valor){
										if($this->M_producto_atributo->modificar($atrib->id,$atrib->valor)){
											$atributo=$this->M_atributo->get($producto_atributo[0]->idatr);
											if($msja!=""){ $msja.=", ";}$msja.="modificó el atributo <strong>".$atributo[0]->atributo."</strong>";
										}
									}
								}
							}
							foreach ($atribs_new as $key => $atrib){ if($this->M_producto_atributo->insertar($idp,$atrib->id,$atrib->valor)){ $ca_add++;}}
							if($ca_add>0){ if($msja!=""){ $msja.=", ";}$msja.="adicionó ".$ca_add." atributo(s)";}
							$msj="";
							if($cod!=$producto[0]->codigo){$msj="Cambió el codigo de <strong>".$producto[0]->codigo."</strong> a <strong>".$cod."</strong>";}
							if($nom!=$producto[0]->nombre){ if($msj!=""){ $msj.=", ";} $msj.="Cambió el nombre de <strong>".$producto[0]->nombre."</strong> a <strong>".$nom."</strong>";}
							if($fec!=$producto[0]->fecha_creacion){ if($msj!=""){ $msj.=", ";} $msj.="Cambió la fecha de creación";}
							if($obs!=$producto[0]->observaciones){ if($msj!=""){ $msj.=", ";} $msj.="modificó las observaciones";}
							if($msj!=""){ $msj.=", ";} $msj.=$msja;
							if($msj!=""){
								$prod=$producto[0]->codigo." ".$producto[0]->nombre;
								if($this->M_producto_seg->insertar($idp,$prod,"u",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
								foreach($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "ok";
							}
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*---CONTROL DE ATRIBUTOS EN EL PRODUCTO---*/
		public function categoria(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if (isset($_POST['idp'])){
					$idp=trim($_POST['idp']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto) && $this->val->entero($idp,0,10)){
						$grupos=$this->M_producto_grupo->get_grupo('pg.idp',$idp);
						$listado['grupos']=$grupos;
						$listado['colores']=$this->M_color->get_all();
						$listado['unidades']=$this->M_unidad->get_all();
						$listado['producto']=$producto[0];
						$listado['producto_piezas']=$this->M_producto_pieza->get_row('idp',$idp);
						$listado['imagenes_producto']=$this->M_producto_imagen->get_row('idp',$idp);
						$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","ASC");
						$listado['producto_grupo_procesos']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_procesos']=$this->M_producto_grupo_color_proceso->get_all();
						$this->load->view('produccion/producto/6-config/categoria',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		/*Manejo de atributos en el producto*/
			public function search_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['id']) && isset($_POST['type'])){
						if(isset($_POST['idpgr'])){ $listado['idpgr']=$_POST['idpgr']; }
						$listado['idp']=$_POST['id'];
						$listado['type']=$_POST['type'];
						$this->load->view('produccion/producto/6-config/categorias/categoria/search',$listado);
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function view_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['id']) && isset($_POST['type'])){
						if(isset($_POST['idpgr'])){ $listado['idpgr']=$_POST['idpgr']; }
						$listado['idp']=$_POST['id'];
						$listado['type']=$_POST['type'];
						$atrib="";$val="";
						if(isset($_POST['cat']) && isset($_POST['abr'])){
							if($_POST['cat']!=""){
								$atrib="nombre";$val=$_POST["cat"];
							}else{
								if($_POST['abr']!=""){
									$atrib="abr";$val=$_POST["abr"];
								}
							}
						}
						$listado['categorias']=$this->M_grupo->get_search($atrib,$val);
						$this->load->view('produccion/producto/6-config/categorias/categoria/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function new_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$this->load->view('produccion/producto/6-config/categorias/categoria/new');
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function save_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['cat']) && isset($_POST['abr'])){
						if($this->val->strSpace($_POST['cat'],2,50) && $this->val->strSpace($_POST['abr'],0,10)){
							if($this->M_grupo->insertar($_POST['cat'],$_POST['abr'])){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function update_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['id']) && isset($_POST['cat']) && isset($_POST['abr'])){
						$control=$this->M_grupo->get($_POST['id']);
						if($this->val->strSpace($_POST['cat'],2,50) && $this->val->strSpace($_POST['abr'],0,10) && !empty($control)){
							if($this->M_grupo->modificar($_POST['id'],$_POST['cat'],$_POST['abr'])){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		   	public function confirm_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['id'])){
								$idgr=$_POST['id'];
								$grupo=$this->M_grupo->get($idgr);
								if(!empty($grupo)){
									$listado['open_control']="true";
									$listado['control']="";
									$control=$this->M_producto_grupo->get_row('idgr',$idgr);
									if(!empty($control)){
										$listado['titulo']="";
										$listado['desc']="Imposible eliminar, la categoria se encuentra siendo usada por ".count($control)." productos.";	
										$listado['btn_ok']="none";
									}else{
										$listado['titulo']="eliminar la categoria <strong>".$grupo[0]->nombre."</strong>";
										$listado['desc']="Se eliminara definitivamente la categoria.";
									}
									$this->load->view('estructura/form_eliminar',$listado);
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function drop_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['id'])){
						$id=$_POST['id'];
						$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
						if(!empty($privilegio)){
							if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
								$grupo=$this->M_grupo->get($id);
								$control=$this->M_producto_grupo->get_row('idgr',$id);
								if(!empty($grupo) && empty($control)){
									if($this->M_grupo->eliminar($id)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}	
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}			
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function adicionar_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['idp']) && isset($_POST['g'])){
						$idp=$_POST['idp'];
						$idgr=$_POST['g'];
						$producto=$this->M_producto->get($idp);
						$grupo=$this->M_grupo->get($idgr);
						if(!empty($producto) && !empty($grupo)){
							$idpgr=$this->M_producto_grupo->max("idpgr")+1;
							if($this->M_producto_grupo->insertar($idpgr,$idp,$idgr)){
								//notificacion
								$cod=$producto[0]->codigo;
								$prod=$producto[0]->nombre;
								if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
								$msj="Adicionó la categoría en el producto.";
								if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$cod." ".$prod,$idpgr,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"cg",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
								foreach($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function update_producto_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pg']) && isset($_POST['g'])){
						$idpgr=trim($_POST['pg']);
						$idgr=trim($_POST['g']);
						$producto_grupo=$this->M_producto_grupo->get($idpgr);
						$grupo=$this->M_grupo->get($idgr);
						if(!empty($producto_grupo) && !empty($grupo)){
							if($producto_grupo[0]->idgr!=$idgr){ $grupo_anterior=$this->M_grupo->get($producto_grupo[0]->idgr); }
							if($this->M_producto_grupo->modificar($idpgr,$idgr)){
								//notificacion
								if($producto_grupo[0]->idgr!=$idgr){
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$msj="Cambió la categoría de <strong>".$grupo_anterior[0]->nombre."</strong> a <strong>".$grupo[0]->nombre."</strong>";
									$cod=$producto[0]->codigo;
									$prod=$producto[0]->nombre;
									if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
									if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$cod." ".$prod,$idpgr,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"ug",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
									$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
									foreach($resultados as $key => $result){
										if($result->idno=="none"){
											if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
										}else{
											if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
										}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
								}else{
									echo "ok";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		   	public function confirm_producto_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['pg'])){
								$idpgr=$_POST['pg'];
								$grupo=$this->M_producto_grupo->get_grupo('idpgr',$idpgr);
								if(!empty($grupo)){
									$listado['open_control']="true";
									$listado['control']="";
									$listado['titulo']="eliminar la categoria <strong>".$grupo[0]->nombre."</strong> del producto";
									$listado['desc']="Se eliminara definitivamente la categoria del producto.";
									$this->load->view('estructura/form_eliminar',$listado);
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function drop_producto_grupo(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pg'])){
						$idpgr=$_POST['pg'];
						$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
						if(!empty($privilegio)){
							if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
								$producto_grupo=$this->M_producto_grupo->get_row('idpgr',$idpgr);
								if(!empty($producto_grupo)){
									$producto_grupo_color=$this->M_producto_grupo_color->get_row('idpgr',$producto_grupo[0]->idpgr);
									for($i=0; $i < count($producto_grupo_color) ; $i++){ $pgc=$producto_grupo_color[$i];
										$fotografias=$this->M_producto_imagen_color->get_row('idpgrc',$pgc->idpgrc);
										$url="./libraries/img/productos/";
										for($f=0; $f < count($fotografias) ; $f++){
											if($this->lib->eliminar_imagen($fotografias[$f]->archivo,$url)){ $control="ok"; }
										}
										$piezas=$this->M_producto_grupo_color_pieza->get_row("idpgrc",$pgc->idpgrc);
								    	if(!empty($piezas)){
								    		$url="./libraries/img/piezas/";
											for($p=0; $p < count($piezas) ; $p++){ 
												if($this->lib->eliminar_imagen($piezas[$p]->fotografia,$url)){ $control="ok"; }
											}
								    	}
									}
									if($this->M_producto_grupo->eliminar($idpgr)){
										//notificacion
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre; }
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$idpgr,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"dg","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										$this->M_producto_grupo_color_seg->null_2n('idp',$producto[0]->idp,'idpgr',$idpgr);
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}	
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}			
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		/*End manejo de atributos en el producto*/
		/*Manejo de atriguto el el grupo de producto*/
		public function save_producto_grupo_atr(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
				if(!empty($privilegio)){
					if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
						if(isset($_POST['pg']) && isset($_POST['atrib_n']) && isset($_POST['atrib_u']) && isset($_POST['atrib_d'])){
							$idpg=trim($_POST['pg']);
							$atribs_new=json_decode($_POST['atrib_n']);
							$atribs_update=json_decode($_POST['atrib_u']);
							$atribs_delete=explode("|",$_POST['atrib_d']);
							$producto_grupo=$this->M_producto_grupo->get($idpg);
							if(!empty($producto_grupo)){
								$control=true;
								if($control){
									foreach($atribs_new as $key => $atrib){
										if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
									}
								}
								if($control){
									foreach($atribs_update as $key => $atrib){
										if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
									}
								}
								if($control){
									foreach($atribs_new as $key => $atrib){
										$aux=$this->M_atributo->get($atrib->id);
										if(empty($aux)){ $control=false; break;}
									}
								}
								if($control){
									$atr_new=0;
									$atr_delete=0;
									$atr_update=0;
									$msj="";
									for ($i=0; $i < count($atribs_delete); $i++){
										if($this->M_producto_grupo_atributo->eliminar($atribs_delete[$i])){ if($_POST['atrib_d']!=""){ $atr_delete++;}}
									}
									if($atr_delete>0 && $_POST['atrib_d']!=""){$msj.="Eliminó ".$atr_delete." atributo";if($atr_delete>1){ $msj.="s";}}
									foreach ($atribs_update as $key => $atrib){
										$producto_grupo_atributo=$this->M_producto_grupo_atributo->get($atrib->id);
										if(!empty($producto_grupo_atributo)){
											if($atrib->valor!=$producto_grupo_atributo[0]->valor){
												if($this->M_producto_grupo_atributo->modificar($atrib->id,$atrib->valor)){
													$atributo=$this->M_atributo->get($producto_grupo_atributo[0]->idatr);
													if($msj!=""){ $msj.=", ";}$msj.="modificó el atributo <strong>".$atributo[0]->atributo."</strong>";
												}
											}
										}
									}
									foreach ($atribs_new as $key => $atrib){
										if($this->M_producto_grupo_atributo->insertar($idpg,$atrib->id,$atrib->valor)){  $atr_new++;}
									}
									if($atr_new>0){ if($msj!=""){ $msj.=", ";}$msj.="adicionó ".$atr_new." atributo";if($atr_new>1){ $msj.="s";}}
									if($msj!=""){
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre; }
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$idpg,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"uga",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "ok";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
			public function grupo_atributo(){//CLASE GENERICA
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pg'])){
						$idpgr=$_POST['pg'];
						$producto_grupo=$this->M_producto_grupo->get($idpgr);
						if(!empty($producto_grupo)){
							$listado['grupo']=$producto_grupo[0];
							$this->load->view('produccion/producto/6-config/categorias/ajax/grupo_atributo',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}

		/*End manejo de atriguto el el grupo de producto*/
		/*Manejo de colores en los grupos*/
			public function search_color(){//CLASE GENERICA
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['type'])){
						if($_POST['type']=="new"){ $listado['idpgr']=$_POST['idpgr'];}
						if($_POST['type']=="update"){ $listado['idpgrc']=$_POST['pgc'];}
						$listado['type']=$_POST['type'];
						$this->load->view('produccion/producto/6-config/categorias/color/search',$listado);
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function view_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['type'])){
						if($_POST['type']=="new"){ $listado['idpgr']=$_POST['idpgr'];}
						if($_POST['type']=="update"){ $listado['idpgrc']=$_POST['pgc'];}
						$listado['type']=$_POST['type'];
						$atrib="";$val="";
						if(isset($_POST['nom']) && isset($_POST['abr'])){
							if($_POST['nom']!=""){
								$atrib="nombre";$val=$_POST["nom"];
							}else{
								if($_POST['abr']!=""){
									$atrib="abr";$val=$_POST["abr"];
								}
							}
						}
						$listado['colores']=$this->M_color->get_search($atrib,$val);
						$this->load->view('produccion/producto/6-config/categorias/color/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function new_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$this->load->view('produccion/producto/6-config/categorias/color/new');
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function save_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['col'])){
						$nom=trim($_POST['nom']);
						$abr=trim($_POST['abr']);
						$col=trim($_POST['col']);
						if($this->val->strSpace($nom,2,50) && $this->val->strSpace($abr,0,10) && $col!=""){
							if($this->M_color->insertar($nom,$col,$abr)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function update_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['id']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['col'])){
						$id=trim($_POST['id']);
						$nom=trim($_POST['nom']);
						$abr=trim($_POST['abr']);
						$col=trim($_POST['col']);
						$control=$this->M_color->get($id);
						if($this->val->strSpace($nom,2,50) && $this->val->strSpace($abr,0,10) && !empty($control) && $col!=""){
							if($this->M_color->modificar($id,$nom,$col,$abr)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		   	public function confirm_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['id'])){
								$idco=$_POST['id'];
								$color=$this->M_color->get($idco);
								if(!empty($color)){
									$listado['open_control']="true";
									$listado['control']="";
									$control=$this->M_producto_grupo_color->get_row('idco',$idco);
									if(!empty($control)){
										$listado['titulo']="";
										$listado['desc']="Imposible eliminar, el color se encuentra siendo usada por ".count($control)." productos.";
										$listado['btn_ok']="none";
									}else{
										$control=$this->M_material->get_row('idco',$idco);
										if(!empty($control)){
											$listado['titulo']="";
											$listado['desc']="Imposible eliminar, el color se encuentra siendo usada por ".count($control)." materiales.";
											$listado['btn_ok']="none";
										}else{
											$listado['titulo']="eliminar el color <strong>".$color[0]->nombre."</strong>";
											$listado['desc']="Se eliminara definitivamente el color.";
										}
									}
									$this->load->view('estructura/form_eliminar',$listado);
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function drop_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['id'])){
						$idco=$_POST['id'];
						$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
						if(!empty($privilegio)){
							if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
								$color=$this->M_color->get($idco);
								$control=$this->M_producto_grupo_color->get_row('idco',$idco);
								$control2=$this->M_material->get_row('idco',$idco);
								if(!empty($color) && empty($control) && empty($control2)){
									if($this->M_color->eliminar($idco)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}	
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}			
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function adicionar_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pg']) && isset($_POST['co'])){
						$idpgr=trim($_POST['pg']);
						$idco=trim($_POST['co']);
						$producto_grupo=$this->M_producto_grupo->get($idpgr);
						$color=$this->M_color->get($idco);
						if(!empty($producto_grupo) && !empty($color)){
							$idpgrc=$this->M_producto_grupo_color->max("idpgrc")+1;
							if($this->M_producto_grupo_color->insertar($idpgrc,$idpgr,$idco)){
								$producto=$this->M_producto->get($producto_grupo[0]->idp);
								$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
								$cod=$producto[0]->codigo;
								$prod=$producto[0]->nombre;
								if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre; }
								$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
								$prod=$cod." ".$prod;
								$msj="Adicionó el color a la categoría";
								if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$idpgrc,$color[0]->idco,$color[0]->nombre,"cc",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
								foreach($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function update_producto_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pgc']) && isset($_POST['co'])){
						$idpgrc=$_POST['pgc'];
						$idco=$_POST['co'];
						$color=$this->M_color->get($idco);
						$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
						if(!empty($color) && !empty($producto_grupo_color)){
							$msj="";
							if($idco!=$producto_grupo_color[0]->idco){
								$color_anterior=$this->M_color->get($producto_grupo_color[0]->idco);
								$msj="Cambió el color de <strong>".$color_anterior[0]->nombre."</strong> a <strong>".$color[0]->nombre."</strong>";
							}
							if($this->M_producto_grupo_color->modificar_row($idpgrc,"idco",$idco)){
								if($msj!=""){
									$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
									$cod=$producto[0]->codigo;
									$prod=$producto[0]->nombre;
									if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
									$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
									$prod=$cod." ".$prod;
									if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"uc",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
									$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
									foreach($resultados as $key => $result){
										if($result->idno=="none"){
											if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
										}else{
											if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
										}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
								}else{
									echo "ok";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		/*End manejo de colores en los grupos*/
		/*Manejo de color del producto*/
		public function titulo_grupo_color(){// En uso
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pgc'])){
					$idpgrc=$_POST['pgc'];
					$color=$this->M_producto_grupo_color->get_color('pgc.idpgrc',$idpgrc);
					if(!empty($color)){
						$producto_grupo=$this->M_producto_grupo->get($color[0]->idpgr);
						$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
						$producto=$this->M_producto->get($producto_grupo[0]->idp);
						$gru="";
						$abr_gru="";
						if($grupo[0]->abr!="" && $grupo[0]->abr!=NULL){ $abr_gru="-".$grupo[0]->abr; $gru=" - ".$grupo[0]->nombre;}
						echo $producto[0]->codigo.$abr_gru."-".$color[0]->abr.": ".$color[0]->nombre;
					}else{
						echo "ok";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function grupo_color(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pgc'])){
					$idpgrc=$_POST['pgc'];
					$color=$this->M_producto_grupo_color->get_color('pgc.idpgrc',$idpgrc);
					if(!empty($color)){
						$listado['producto_grupo_color']=$color[0];
						$listado['producto_grupo_procesos']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_procesos']=$this->M_producto_grupo_color_proceso->get_all();
						$listado['unidades']=$this->M_unidad->get_all();
						$this->load->view('produccion/producto/6-config/categorias/refresh/grupo_color',$listado);
					}else{
						echo "ok $idpgrc";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function confirm_grupo_color(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pgc'])){
					$idpgrc=$_POST['pgc'];
					$color=$this->M_producto_grupo_color->get_color('pgc.idpgrc',$idpgrc);
					if(!empty($color)){
						$listado['open_control']="true";
						$listado['control']="";
						$listado['titulo']="eliminar el color <b>".$color[0]->nombre."</b> de la categoria del producto.";
						$listado['desc']="";
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function drop_grupo_color(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pgc'])){
					$idpgrc=$_POST['pgc'];
					$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
					if(!empty($producto_grupo_color)){
						$fotografias=$this->M_producto_imagen_color->get_row("idpgrc",$producto_grupo_color[0]->idpgrc);
				    	if(!empty($fotografias)){
				    		$url="./libraries/img/productos/";
							for($i=0; $i < count($fotografias) ; $i++){ 
								if($this->lib->eliminar_imagen($fotografias[$i]->archivo,$url)){ $control="ok"; }
							}
				    	}
				    	$piezas=$this->M_producto_grupo_color_pieza->get_row("idpgrc",$producto_grupo_color[0]->idpgrc);
				    	if(!empty($piezas)){
				    		$url="./libraries/img/piezas/";
							for($i=0; $i < count($piezas) ; $i++){ 
								if($this->lib->eliminar_imagen($piezas[$i]->fotografia,$url)){ $control="ok"; }
							}
				    	}
						if($this->M_producto_grupo_color->eliminar($idpgrc)){
							//para notificaciones
							$color=$this->M_color->get($producto_grupo_color[0]->idco);
							$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
							$producto=$this->M_producto->get($producto_grupo[0]->idp);
							$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
							$cod=$producto[0]->codigo;
							$prod=$producto[0]->nombre;
							if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
							$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
							$prod=$cod." ".$prod;
								if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"dc","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
								foreach($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								$this->M_producto_grupo_color_seg->null_3n('idp',$producto[0]->idp,'idpgr',$producto_grupo[0]->idpgr,'idpgrc',$idpgrc);
								echo json_encode($result);
						}else{
							echo "error";
						}
					}else{
						echo "ok";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		/*End manejo de color del producto*/
		/*Manejo de Imagenes en los colores de los productos*/
			public function change_producto_color_imagen(){// en uso
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pi']) && isset($_POST['pgc'])){
						$idpi=$_POST['pi'];
						$idpgrc=$_POST['pgc'];
						$control1=$this->M_producto_imagen->get($idpi);
						$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
						if(!empty($control1) && !empty($producto_grupo_color)){
							$es_imagen=$this->M_producto_imagen_color->get_row_2n("idpi",$idpi,"idpgrc",$idpgrc);
							if(!empty($es_imagen)){
								if($this->M_producto_imagen_color->eliminar_row("idpi",$idpi,"idpgrc",$idpgrc)){
									//para notificaciones
									$color=$this->M_color->get($producto_grupo_color[0]->idco);
									$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"dcf","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => '0|ok','tipo'=>'2');
										echo json_encode($result);
								}else{
									echo "|error";
								}
							}else{
								$portada=0;
								$control=$this->M_producto_imagen_color->get_row("idpgrc",$idpgrc);
								if(empty($control)){
									$portada=1;
								}
								if($this->M_producto_imagen_color->insertar($idpgrc,$idpi,$portada)){
									//para notificaciones
									$color=$this->M_color->get($producto_grupo_color[0]->idco);
									$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ccf","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => '1|ok','tipo'=>'2');
										echo json_encode($result);
								}else{
									echo "|error";
								}	
							}
						}else{
							echo "|fail";
						}
					}else{
						echo "|fail";
					}			
				}else{
					echo "|logout";
				}
			}
			public function drop_color_imagen(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['i'])){
						$idpig=$_POST['i'];
						$imagen=$this->M_producto_imagen_color->get($idpig);
						if(!empty($imagen)){
							if($this->M_producto_imagen_color->eliminar($idpig)){
									//para notificaciones
									$producto_grupo_color=$this->M_producto_grupo_color->get($imagen[0]->idpgrc);
									$color=$this->M_color->get($producto_grupo_color[0]->idco);
									$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"dcf","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
							}else{
								echo "error";
							}
						}else{
							echo "ok";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function producto_grupo_color_imagen(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pgc'])){
						$idpgrc=$_POST['pgc'];
						$color=$this->M_producto_grupo_color->get_color('pgc.idpgrc',$idpgrc);
						if(!empty($color)){
							$listado['color']=$color[0];
							$this->load->view('produccion/producto/6-config/categorias/ajax/grupo_color_imagen',$listado);
						}else{
							echo "ok";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}

		/*End Manejo de Imagenes en los colores de los productos*/
		/*Manejo de atributos en los colores del grupo*/
			public function save_producto_grupo_color_atr(){ // en uso
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['pgc']) && isset($_POST['atrib_n']) && isset($_POST['atrib_u']) && isset($_POST['atrib_d'])){
								$idpgc=trim($_POST['pgc']);
								$atribs_new=json_decode($_POST['atrib_n']);
								$atribs_update=json_decode($_POST['atrib_u']);
								$atribs_delete=explode("|",$_POST['atrib_d']);
								$producto_grupo_color=$this->M_producto_grupo_color->get($idpgc);
								if(!empty($producto_grupo_color)){
									$control=true;
									if($control){
										foreach($atribs_new as $key => $atrib){
											if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
										}
									}
									if($control){
										foreach($atribs_update as $key => $atrib){
											if(!$this->val->textarea($atrib->valor,0,150)){ $control=false; break;}
										}
									}
									if($control){
										foreach($atribs_new as $key => $atrib){
											$aux=$this->M_atributo->get($atrib->id);
											if(empty($aux)){ $control=false; break;}
										}
									}
									if($control){
										$msj="";
										$c_c=0;$c_u=0;$c_d=0;
											foreach($atribs_new as $key => $atrib){
												if($this->M_producto_grupo_color_atributo->insertar($idpgc,$atrib->id,$atrib->valor)){ $c_c++;}
											}
											for ($i=0; $i < count($atribs_delete); $i++){
												if($this->M_producto_grupo_color_atributo->eliminar($atribs_delete[$i])){if($_POST['atrib_d']!=""){ $c_d++; }}
											}
											foreach ($atribs_update as $key => $atrib){
												if($this->M_producto_grupo_color_atributo->modificar($atrib->id,$atrib->valor)){ $c_u++; }
											}
									//para notificaciones
											if($c_d>0){ if($msj!=""){ $msj.=", ";}$msj.="Eliminó ".$c_d." atributo";if($c_d>1){$msj.="s";}}
											if($c_c>0){ if($msj!=""){ $msj.=", ";}$msj.="adicionó ".$c_c." atributo";if($c_c>1){$msj.="s";}};
											if($c_u>0){ if($msj!=""){ $msj.=", ";}$msj.="modificó ".$c_u." atributo";if($c_u>1){$msj.="s";}}
											if($msj!=""){
												$color=$this->M_color->get($producto_grupo_color[0]->idco);
												$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
												$producto=$this->M_producto->get($producto_grupo[0]->idp);
												$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
												$cod=$producto[0]->codigo;
												$prod=$producto[0]->nombre;
												if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
												$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
												$prod=$cod." ".$prod;
												if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"uca",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
												foreach($resultados as $key => $result){
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
													}
												}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}else{
												echo "ok";
											}
									}else{
										echo "fail";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function producto_grupo_color_atributo(){// en uso
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pgc'])){
						$idpgrc=$_POST['pgc'];
						$color=$this->M_producto_grupo_color->get_color('pgc.idpgrc',$idpgrc);
						if(!empty($color)){
							$listado['color']=$color[0];
							$this->load->view('produccion/producto/6-config/categorias/ajax/grupo_color_atributo',$listado);
						}else{
							echo "ok";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		/*End manejo de atributos en los colores del grupo*/
		public function titulo_producto_grupo(){//refrescamos solo pa rate de color de una grupo de prodcutos
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pg'])){
					$idpgr=$_POST['pg'];
					$grupo=$this->M_producto_grupo->get_grupo('pg.idpgr',$idpgr);
					if(!empty($grupo)){
						$producto=$this->M_producto->get($grupo[0]->idp);
						echo $this->lib->all_mayuscula("<ins>".$producto[0]->codigo."-".$grupo[0]->abr.": ".$producto[0]->nombre." - ".$grupo[0]->nombre."</ins>");
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function producto_grupo(){//refrescamos solo parte de colores de una grupo de prodcutos
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pg'])){
					$idpgr=$_POST['pg'];
					$grupo=$this->M_producto_grupo->get_grupo('pg.idpgr',$idpgr);
					if(!empty($grupo)){
						$listado['grupo']=$grupo[0];
						$listado['colores']=$this->M_color->get_all();
						$listado['unidades']=$this->M_unidad->get_all();
						$listado['producto_grupo_procesos']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_procesos']=$this->M_producto_grupo_color_proceso->get_all();
						$this->load->view('produccion/producto/6-config/categorias/refresh/grupo',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
	/*---END CONTROL DE ATRIBUTOS EN EL PRODUCTO---*/
	/*---MANEJO DE MATERIALES EN EL PRODUCTO---*/
	/*public function material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if (isset($_POST['p'])){
				$idp=trim($_POST['p']);
				$productos=$this->M_producto_grupo_color->get_producto('p.idp',$idp);
				$listado['productos']=$productos;
				$listado['colores']=$this->M_color->get_all();
				$listado['grupos']=$this->M_grupo->get_all();
				$listado['materiales']=$this->M_material_item->get_search("","");
				$listado['idp']=$idp;
				$this->load->view('produccion/producto/6-config/materiales',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function change_producto_color_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if (isset($_POST['pgc']) && isset($_POST['pm'])){
				$pgc=trim($_POST['pgc']);
				$pm=trim($_POST['pm']);
				$producto_material=$this->M_producto_material->get($pm);
				if($this->val->entero($pgc,0,10) && !empty($producto_material)){
					$control=$this->M_producto_grupo_color_material->get_row_2n('idpm',$pm,'idpgrc',$pgc);
					if(empty($control)){
						if($this->M_producto_grupo_color_material->insertar($pm,$pgc,$producto_material[0]->cantidad)){
							echo "1|ok";
						}else{
							echo "|error";
						}
					}else{
						if($this->M_producto_grupo_color_material->eliminar_2n('idpm',$pm,'idpgrc',$pgc)){
							echo "0|ok";
						}else{
							echo "|error";
						}
					}
				}else{
					echo "|fail";
				}
			}else{
				echo "|fail";
			}
		}else{
			echo "|logout";
		}
	}
	public function color_producto(){//en uso en materieles del producto
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if (isset($_POST['pgc'])){
				$idpgrc=trim($_POST['pgc']);
				$producto=$this->M_producto_grupo_color->get_producto('pgc.idpgrc',$idpgrc);
				if(!empty($producto)){
					$listado['producto']=$producto[0];
					$listado['colores']=$this->M_color->get_all();
					$listado['grupos']=$this->M_grupo->get_all();
					$listado['materiales']=$this->M_material_item->get_search("","");
					$this->load->view('produccion/producto/6-config/materiales/color_producto',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_color_material(){//en uso en materieles del producto
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if (isset($_POST['pgcm']) && isset($_POST['m']) && isset($_POST['c'])){
				$pgcm=trim($_POST['pgcm']);
				$m=trim($_POST['m']);
				$c=trim($_POST['c']);
				if($this->val->entero($pgcm,0,10) && $this->val->entero($m,0,10) && $this->val->decimal($c,7,4) && $c>0 && $c<=9999999.9999){
					$producto_grupo_color_material=$this->M_producto_grupo_color_material->get($pgcm);
					$msj="";
					if($producto_grupo_color_material[0]->idm!=$m || $producto_grupo_color_material[0]->cantidad!=$c){
						$msj="true";
					}
					if($this->M_producto_grupo_color_material->modificar($pgcm,$m,$c)){
						//para notificaciones
						if($msj!=""){$msj="";
							$material=$this->M_material_item->get_material("m.idm",$m);
							if($producto_grupo_color_material[0]->idm!=$m){
								$material_anterior=$this->M_material_item->get_material("m.idm",$producto_grupo_color_material[0]->idm);
								$msj="Cambió el material de <strong>".$material_anterior[0]->nombre."</strong> a <strong>".$material[0]->nombre."</strong>";
							}
							if($producto_grupo_color_material[0]->cantidad!=$c){
								if($msj!=""){$msj.=", ";}
								$msj.="Cambió la cantidad del material <strong>".$material[0]->nombre."</strong> de <strong>".$producto_grupo_color_material[0]->cantidad."</strong> a <strong>".$c."</strong>";
							}
									$producto_grupo_color=$this->M_producto_grupo_color->get($producto_grupo_color_material[0]->idpgrc);
									$color=$this->M_color->get($producto_grupo_color[0]->idco);
									$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
									if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ugcm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
									$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
									foreach($resultados as $key => $result){
										if($result->idno=="none"){
											if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
										}else{
											if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
										}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
						}else{
							echo "ok";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
		   	public function confirm_color_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['pgcm'])){
								$pgcm=$_POST['pgcm'];
								$producto_material=$this->M_producto_grupo_color_material->get_material("pgcm.idpgcm",$pgcm);
								if(!empty($producto_material)){
									$url=base_url().'libraries/img/materiales/miniatura/';
		    						$img="default.png";
		    						$fotografia=$this->M_material_imagen->get_row_2n("idmi",$producto_material[0]->idmi,"portada","1");
								    if(!empty($fotografia)){ $img=$fotografia[0]->nombre; }
								    $listado['img']=$url.$img;
									$listado['open_control']="true";
									$listado['control']="";
									$listado['titulo']="eliminar el(la) <strong>".$producto_material[0]->nombre."</strong> del color del producto";
									$listado['desc']="Se eliminara definitivamente el material en color del producto.";
									$this->load->view('estructura/form_eliminar',$listado);
								}else{
									echo "fail $pgcm";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function drop_color_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pgcm'])){
						$pgcm=$_POST['pgcm'];
						$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
						if(!empty($privilegio)){
							if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
								$producto_grupo_color_material=$this->M_producto_grupo_color_material->get($pgcm);
								if($this->M_producto_grupo_color_material->eliminar($pgcm)){
									//para notificacion
									$material=$this->M_material_item->get_material("m.idm",$producto_grupo_color_material[0]->idm);
									$producto_grupo_color=$this->M_producto_grupo_color->get($producto_grupo_color_material[0]->idpgrc);
									$color=$this->M_color->get($producto_grupo_color[0]->idco);
									$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
									$producto=$this->M_producto->get($producto_grupo[0]->idp);
									$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
									$msj="Eliminó el material <strong>".$material[0]->nombre."</strong>";
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
									if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ugcm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
									$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
									foreach($resultados as $key => $result){
										if($result->idno=="none"){
											if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
										}else{
											if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
										}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
								}else{
									echo "error";
								}
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}			
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}*/
	/*---END MANEJO DE MATERIALES EN EL PRODUCTO---*/
	/*---MANEJO DE PRDUCCION EN EL PRODUCTO---*/
		public function produccion(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['p'])){
					$idp=trim($_POST['p']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto) && $this->val->entero($idp,0,10)){
						$listado['producto']=$producto[0];
						$listado['colores']=$this->M_producto->get_colores("p.idp",$idp,"");
						$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
						$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
						$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
						$this->load->view('produccion/producto/6-config/produccion',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function tbl_producto_proceso_empleados(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['tbl']) && isset($_POST['pgc']) && isset($_POST['pr']) && isset($_POST['ppr'])){
					$idpgrc=trim($_POST['pgc']);
					$idpr=trim($_POST['pr']);
					$idppr=trim($_POST['ppr']);
					$color=$this->M_producto_grupo_color->get($idpgrc);
					$proceso=$this->M_proceso->get($idpr);
					$producto_proceso=$this->M_producto_proceso->get($idppr);
					if(!empty($color) && !empty($proceso) && !empty($producto_proceso)){
						$listado['producto_procesos_empleados']=$this->M_producto_empleado->get_empleado("pe.idpgrc",$idpgrc,"pe.calidad","desc");
						$listado['proceso']=$proceso[0];
						$listado['color']=$color[0];
						$listado['producto_proceso']=$producto_proceso[0];
						$listado['tbl']=trim($_POST["tbl"]);
						$this->load->view('produccion/producto/6-config/produccion/producto/tbl_proceso_empleados',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function update_producto_empleado(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['proe']) && isset($_POST['calidad'])){
					$idproe=trim($_POST['proe']);
					$calidad=trim($_POST['calidad']);
					$producto_empleado=$this->M_producto_empleado->get($idproe);
					if(!empty($producto_empleado)){
						if($this->M_producto_empleado->modificar($idproe,$calidad)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function confirmar_producto_empleado(){
	   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   			if(isset($_POST['proe']) && isset($_POST['pgc'])){
					$idproe=trim($_POST['proe']);
					$idpgrc=trim($_POST['pgc']);
					$producto_empleado=$this->M_producto_empleado->get($idproe);
					$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
					if(!empty($producto_empleado) && !empty($producto_grupo_color)){
						$url=base_url().'libraries/img/';
						$proceso_empleado=$this->M_proceso_empleado->get($producto_empleado[0]->idpre);
						$proceso=$this->M_proceso->get($proceso_empleado[0]->idpr);
						$empleado=$this->M_empleado->get($proceso_empleado[0]->ide);
						$persona=$this->M_persona->get($empleado[0]->ci);
						$nombre=$persona[0]->nombre." ".$persona[0]->nombre2." ".$persona[0]->paterno;
						$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
						$producto=$this->M_producto->get($producto_grupo[0]->idp);
						$listado['titulo']="eliminar el empleado <strong>".$nombre."</strong> del proceso <strong>".$proceso[0]->nombre."</strong> en el producto <strong>".$producto[0]->nombre."</strong>";
						$listado['desc']="Se eliminara definitivamente el empleado";
						$img="sistema/default.jpg";
						if($persona[0]->fotografia!="" && $persona[0]->fotografia!=NULL){ $img="personas/miniatura/".$persona[0]->fotografia; }
						$listado['open_control']="false";
						$listado['control']="";
						$listado['img']=$url.$img;
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
	   		}else{
	   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	   		}
		}
		public function drop_producto_empleado(){
	   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   			if(isset($_POST['proe'])){
					$idproe=trim($_POST['proe']);
					$producto_empleado=$this->M_producto_empleado->get($idproe);
					if(!empty($producto_empleado)){
						if($this->M_producto_empleado->eliminar($idproe)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
	   		}else{
	   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	   		}
		}
		public function view_empleado_proceso(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pgc']) && isset($_POST['pr']) && isset($_POST['ppr']) && isset($_POST['tbl'])){
					$idpgrc=trim($_POST['pgc']);
					$idpr=trim($_POST['pr']);
					$idppr=trim($_POST['ppr']);
					$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
					$proceso=$this->M_proceso->get($idpr);
					$producto_proceso=$this->M_producto_proceso->get($idppr);
					if(!empty($producto_proceso) && !empty($proceso) && !empty($producto_grupo_color)){
						$listado['empleados']=$this->M_proceso_empleado->get_empleado("ep.idpr",$idpr,"nombre_completo","asc");
						$listado['producto_empleados']=$this->M_producto_empleado->get_proceso_empleado("pe.idpgrc",$idpgrc,'pre.idpr',$idpr);
						$listado['producto_grupo_color']=$producto_grupo_color[0];
						$listado['proceso']=$proceso[0];
						$listado['producto_proceso']=$producto_proceso[0];
						$listado['tbl']=trim($_POST['tbl']);
						$this->load->view('produccion/producto/6-config/produccion/empleados/proceso_empleado',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function add_empleado_proceso(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pgc']) && isset($_POST['pre']) && isset($_POST['calidad'])){
					$idpgrc=trim($_POST['pgc']);
					$idpre=trim($_POST['pre']);
					$calidad=trim($_POST['calidad']);
					$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
					$proceso_empleado=$this->M_proceso_empleado->get($idpre);

					if($this->val->entero($calidad,0,2) && !empty($proceso_empleado) && !empty($producto_grupo_color)){
						if($this->M_producto_empleado->insertar($idpgrc,$idpre,$calidad,$proceso_empleado[0]->tipo)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "faillll";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function confirmar_empleado_producto(){
	   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   			if(isset($_POST['pre']) && isset($_POST['pr'])){
					$idpre=$_POST['pre'];
					$idpr=$_POST['pr'];
					$proceso_empleado=$this->M_proceso_empleado->get($idpre);
					$proceso=$this->M_proceso->get($idpr);
					$url=base_url().'libraries/img/';
					if(!empty($proceso_empleado) && !empty($proceso)){
						$control=$this->M_producto_empleado->get_row("idpre",$idpre);
						if(!empty($control)){
							$listado['titulo']="";
							$listado['desc']="Imposible eliminar el empleado del proceso ".$proceso[0]->nombre.", pues esta siendo usado por ".count($control)." producto(s).";
							$listado['btn_ok']="none";
						}else{
							$listado['titulo']="eliminar el empleado del proceso <strong>".$proceso[0]->nombre."</strong> del sistema";
							$listado['desc']="Se eliminara definitivamente el empleado del proceso ".$proceso[0]->nombre;
						}
						$empleado=$this->M_empleado->get($proceso_empleado[0]->ide);
						$persona=$this->M_persona->get($empleado[0]->ci);
						$img="sistema/default.jpg";
						if($persona[0]->fotografia!="" && $persona[0]->fotografia!=NULL){ $img="personas/miniatura/".$persona[0]->fotografia; }
						$listado['open_control']="false";
						$listado['control']="";
						$listado['img']=$url.$img;
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
	   		}else{
	   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	   		}
		}
		public function drop_empleado_producto(){
	   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   			if(isset($_POST['pre'])){
					$idpre=$_POST['pre'];
					$proceso_empleado=$this->M_proceso_empleado->get($idpre);
					if(!empty($proceso_empleado)){
						$control=$this->M_producto_empleado->get_row("idpre",$idpre);
						if(empty($control)){
							if($this->M_proceso_empleado->eliminar($idpre)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
	   		}else{
	   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	   		}
		}
		public function search_empleado(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pr'])){
					$idpr=trim($_POST['pr']);
					$proceso=$this->M_proceso->get($idpr);
					if(!empty($proceso)){
						$listado['proceso']=$proceso[0];
						$listado['pr']=$idpr;
						$this->load->view('produccion/producto/6-config/produccion/empleados/new_empleado/search',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function view_empleado(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pr'])){
					$idpr=trim($_POST['pr']);
					$proceso=$this->M_proceso->get($idpr);
					if(!empty($proceso)){
						$col="";$val="";
						if(isset($_POST['nom'])){ if($_POST['nom']!=""){ $col="nombre_completo";$val=$_POST['nom'];}}
						$listado['proceso']=$proceso[0];
						$listado['empleados']=$this->M_empleado->get_empleado($col,$val,false,"1");
						$listado['proceso_empleados']=$this->M_proceso_empleado->get_row("idpr",$idpr);
						//$listado['productos_empleado']=$this->M_producto_empleado->get_row("idpgrc",$idpgrc);
						$this->load->view('produccion/producto/6-config/produccion/empleados/new_empleado/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function adicionar_empleado(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['e']) && isset($_POST['pr']) && isset($_POST['tip'])){
					$ide=trim($_POST['e']);
					$idpr=trim($_POST['pr']);
					$tip=trim($_POST['tip'])."";
					$empleado=$this->M_empleado->get($ide);
					$proceso=$this->M_proceso->get($idpr);
					if(!empty($empleado) && !empty($proceso) && ($tip=="0" || $tip=="1")){
						if($this->M_proceso_empleado->insertar($idpr,$ide,$tip)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function save_empleados_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['proes'])){
					if($_POST['proes']!="[]"){
						$producto_empleados=json_decode($_POST['proes']);
						foreach ($producto_empleados as $key => $pe) {
							if($this->M_producto_empleado->modificar($pe->proe,$pe->calidad)){}
						}
						echo "ok";
					}else{
						echo "okkk";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}

	/*---END MANEJO DE PRODUCCION EN EL PRODUCTO---*/
   	/*--- Eliminar ---*/
   	public function confirmar_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['id'])){
				$idp=$_POST['id'];
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					$url='./libraries/img/';
					$img="sistema/miniatura/default.jpg";
					if($producto[0]->portada!="" && $producto[0]->portada!=null){
						$v=explode("|", $producto[0]->portada);
						if(count($v==2)){
							if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
							if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
						}
					}
					if(!empty($imagen)){ $img=$imagen[0]->archivo; }
					$listado['titulo']="el producto <strong>".$producto[0]->nombre."</strong>";
					$listado['desc']="Se eliminara definitivamente el producto del sistema";
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
			if(!empty($privilegio)){
				if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1d==1){
					if(isset($_POST['id']) && isset($_POST['u']) && isset($_POST['p'])){
						$id=$_POST['id'];
						$u=$_POST['u'];
						$p=$_POST['p'];
						if($u==$this->session->userdata("login")){
							$usuario=$this->M_usuario->validate($u,$p);
							if(!empty($usuario)){
								$producto=$this->M_producto->get($id);
								if(!empty($producto)){
									/*$imagenes=$this->M_producto_imagen->get_row('idp',$producto[0]->idp);
									for($i=0; $i < count($imagenes); $i++){
										if($this->lib->eliminar_imagen($imagenes[$i]->archivo,'./libraries/img/productos/')){ }
									}
									$piezas=$this->M_producto_pieza->get_row('idp',$producto[0]->idp);
									for($i=0; $i < count($piezas); $i++){
										if($this->lib->eliminar_imagen($piezas[$i]->fotografia,'./libraries/img/piezas/')){ }
									}
									$producto_grupos=$this->M_producto_grupo->get_row('idp',$producto[0]->idp);
									for ($i=0; $i < count($producto_grupos) ; $i++) { $pg=$producto_grupos[$i];
										$producto_grupos_colores=$this->M_producto_grupo_color->get_row('idpgr',$pg->idpgr);
										for($j=0; $j < count($producto_grupos_colores); $j++) { $pgc=$producto_grupos_colores[$j];
											$imagenes=$this->M_producto_imagen_color->get_row('idpgrc',$pgc->idpgrc);
											for($k=0; $k < count($imagenes); $k++){
												if($this->lib->eliminar_imagen($imagenes[$k]->archivo,'./libraries/img/productos/')){ }
											}
											$piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$pgc->idpgrc);
											for($k=0; $k < count($piezas); $k++){
												if($this->lib->eliminar_imagen($piezas[$k]->fotografia,'./libraries/img/piezas/')){ }
											}
										}
									}*/


									if($this->M_producto->eliminar($id)){
										//notificacion
										$prod=$producto[0]->codigo." ".$producto[0]->nombre;
										if($this->M_producto_seg->insertar($producto[0]->idp,$prod,"d","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										$this->M_producto_seg->null('idp',$producto[0]->idp);
										$this->M_producto_grupo_color_seg->null('idp',$producto[0]->idp);
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	
/*----- MANEJO DE PRODUCCION -----*/
	public function search_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"produccion");
			if($privilegio){
				$listado['privilegio']=$privilegio;
				$listado['clientes']=$this->M_cliente->get_all();
				$this->load->view('produccion/produccion/search',$listado);
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"produccion");
			if($privilegio){
				$atrib=""; $val="";$type_search="";
				if(isset($_POST['num']) && isset($_POST['nom']) && isset($_POST['cli']) && isset($_POST['est'])){
					if($_POST['num']!=""){
						$atrib='numero';$val=$_POST['num'];$type_search="like";
					}else{
						if($_POST['cli']!=""){
							$atrib='c.idcl';$val=$_POST['cli'];$type_search="equals";
						}else{
							if($_POST['nom']!=""){
								$atrib='p.nombre';$val=$_POST['nom'];$type_search="like";
							}else{
								if($_POST['est']!=""){
									$atrib='p.estado';$val=$_POST['est'];$type_search="equals";
								}
							}
						}
					}
				}
				$listado['pedidos']=$this->M_pedido->get_search($atrib,$val,"");
				$listado['privilegio']=$privilegio[0];
				$listado['all_detalles']=$this->M_parte_pedido->get_detalle(NULL,NULL);
				$listado['atrib']=$atrib;
				$listado['val']=$val;
				$listado['type_search']=$type_search;
				$this->load->view('produccion/produccion/view',$listado);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- Nuevo ---*/
	/*--- End nuevo ---*/
	/*--- Configuracion ---*/
	public function reporte_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				if(!empty($pedido)){
   					echo "En desarrollo";
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function configurar_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				if(!empty($pedido)){
   					$partes=$this->M_parte_pedido->get_row('idpe',$idpe);
   					if(!empty($partes)){
						$cliente=$this->M_cliente->get($pedido[0]->idcl);
						if(isset($_POST['pp'])){
							$parte_pedido=$this->lib->search_elemento($partes,'idpp',trim($_POST['pp']));
						}else{
							$parte_pedido=$partes[0];
						}
		   				$listado['parte_pedido']=$parte_pedido;
		   				$listado['cliente']=$cliente[0];
		   				$listado['pedido']=$pedido[0];
		   				$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
						$listado['partes']=$partes;
						$productos=$this->M_producto->get_search_all(NULL,NULL,NULL);
						//$productos=$this->M_producto->get_search(NULL,NULL,NULL);
						$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
						$productos_imagenes=$this->M_producto_imagen->get_all();
						$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
						$detalles_pedidos=$this->M_detalle_pedido->get_search("idpp",$parte_pedido->idpp);
						$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
						$order_by="posicion";//codigo o posicion
						$listado['productos']=$this->lib->productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);
		   				$listado['productos_pedido_empleado']=$this->M_detalle_pedido->producto_pedido_empleado("dp.idpp",$parte_pedido->idpp);
		   				//procesos del producto
						$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
						$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
		   				$this->load->view('produccion/produccion/6-config/partes_pedido',$listado);
   					}
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function parte_pedido_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pp'])){
   				$idpp=trim($_POST['pp']);
   				$parte=$this->M_parte_pedido->get($idpp);
   				if(!empty($parte)){
   					$pedido=$this->M_pedido->get($parte[0]->idpe);
   					if(!empty($pedido)){
   						$partes=$this->M_parte_pedido->get_row("idpe",$parte[0]->idpe);
		   				$cliente=$this->M_cliente->get($pedido[0]->idcl);
		   				$listado['parte_pedido']=$parte[0];
		   				$listado['cliente']=$cliente[0];
		   				$listado['pedido']=$pedido[0];
		   				$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
		   				$listado['partes']=$partes;
		   				$listado['detalle_pedido']=$this->M_detalle_pedido->get_search('idpp',$parte[0]->idpp);
		   				$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
		   				$listado['productos_pedido_empleado']=$this->M_detalle_pedido->producto_pedido_empleado("dp.idpp",$parte[0]->idpp);
		   				//procesos del producto
						$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
						$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
		   				$this->load->view('produccion/produccion/6-config/partes_pedido',$listado);
   					}
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function progress_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['pp'])){
				$idp=trim($_POST['p']);
				$idpp=trim($_POST['pp']);
				$producto=$this->M_producto->get($idp);
				$parte_pedido=$this->M_parte_pedido->get($idpp);
				if(!empty($producto) && !empty($parte_pedido)){
					$listado['colores']=$this->M_producto_grupo_color->get_producto("p.idp",$idp);
					$listado['detalles']=$this->M_detalle_pedido->get_search("idpp",$idpp);
					$this->load->view('produccion/produccion/6-config/producto/progress_producto',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function colores_producto_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['pp']) && isset($_POST['tbl'])){
				$idp=trim($_POST['p']);
				$idpp=trim($_POST['pp']);
				$producto=$this->M_producto->get($idp);
				$parte_pedido=$this->M_parte_pedido->get_complet("pp.idpp",$idpp);
				if(!empty($producto) && !empty($parte_pedido)){
					$categorias=$this->M_producto->get_colores('p.idp',$idp,"1");
					$pedido_productos=$this->M_detalle_pedido->get_search('idpp',$idpp);
					$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
					$grupos=[];
					for($i=0;$i < count($categorias);$i++){ $cat=$categorias[$i];
						for($j=0; $j < count($pedido_productos); $j++){ $pp=$pedido_productos[$j];
							if($cat->idpgrc==$pp->idpgrc){
								$sucursales_detalle_pedido=$this->lib->select_from($sucursales_detalles_pedidos,'iddp',$pp->iddp);
								if(count($sucursales_detalle_pedido)>0){
									for($k=0;$k<count($sucursales_detalle_pedido);$k++){$sucursal_detalle_pedido=$sucursales_detalle_pedido[$k];
										$grupos[] = array('idpgr'=>$cat->idpgr, 'idgr'=>$cat->idgr, 'nombre_g'=>$cat->nombre_g, 
										'abr_g'=>$cat->abr_g, 'idpgrc'=>$cat->idpgrc, 'portada'=>$cat->portada, 'costo'=>$cat->costo, 
										'idco'=>$cat->idco, 'nombre_c'=>$cat->nombre_c, 'codigo_c'=>$cat->codigo_c, 'abr_c'=>$cat->abr_c,
										'iddp'=>$pp->iddp, 'idpp'=>$pp->idpp, 'cu'=>$pp->cu, 'cv'=>$pp->cv, 'observacion'=>$pp->observacion,
										'idsdp'=>$sucursal_detalle_pedido->idsdp, 'idsc'=>$sucursal_detalle_pedido->idsc, 'cantidad'=>$sucursal_detalle_pedido->cantidad, 'porcentaje'=>$pp->porcentaje);
									}
								}
							}
						}
					}
					$v_pgc=$this->lib->array_multi_unique($grupos,'iddp');
					$colores=[];
					for($j=0; $j < count($v_pgc); $j++){ $pgc=$v_pgc[$j];
						$sucursales=[];
						for($k=0;$k < count($grupos); $k++){ $suc=$grupos[$k];
							if($suc['iddp']==$pgc['iddp']){
								$cantidad=$this->lib->desencriptar_num($suc['cantidad'])."";
								$sucursales[]=array('idsdp'=>$suc['idsdp'],'idsc'=>$suc['idsc'],'cantidad'=>$cantidad);
							}
						}
						$cu=$this->lib->desencriptar_num($pgc['cu'])."";
						$cv=$this->lib->desencriptar_num($pgc['cv'])."";
						$colores[] = array('idpgr'=>$pgc['idpgr'],'idgr'=>$pgc['idgr'],'nombre_g' => $pgc['nombre_g'],'abr_g' => $pgc['abr_g'],'idpgrc' => $pgc['idpgrc'],'costo' => $pgc['costo'],'portada' => $pgc['portada'],'idco' => $pgc['idco'],'nombre_c' => $pgc['nombre_c'],'codigo_c' => $pgc['codigo_c'],'abr_c' => $pgc['abr_c'],'iddp' => $pgc['iddp'],'observacion' => $pgc['observacion'],'porcentaje' => $pgc['porcentaje'],'cu' => $cu,'cv' => $cv, 'sucursales' => $sucursales);
					}
					$listado['grupos']=json_decode(json_encode($colores));
					$listado['sucursales']=$this->M_sucursal_cliente->get_row("idcl",$parte_pedido[0]->idcl);
					$listado['parte_pedido']=$parte_pedido[0];
					$listado['producto']=$producto[0];
					$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
					$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
					$listado['productos_pedido_empleado']=$this->M_detalle_pedido->producto_pedido_empleado("dp.idpp",$idpp);
					$listado['idtbl']=trim($_POST['tbl']);
					$this->load->view('produccion/produccion/6-config/producto/accordion_producto',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function progress_producto_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['dp'])){
				$iddp=trim($_POST['dp']);
				$detalle_pedido=$this->M_detalle_pedido->get($iddp);
				if(!empty($detalle_pedido)){
					$listado['porcentaje']=$detalle_pedido[0]->porcentaje*1;
					$this->load->view('produccion/produccion/6-config/producto/progress_producto_color',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function sucursales_color_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['dp']) && isset($_POST['pgc']) && isset($_POST['pp']) && isset($_POST['tbl_color_progress']) && isset($_POST['tbl_producto_progress'])){
				$idp=trim($_POST['p']);
				$iddp=trim($_POST['dp']);
				$idpgrc=trim($_POST['pgc']);
				$idpp=trim($_POST['pp']);
				$producto=$this->M_producto->get($idp);
				$detalle_pedido=$this->M_detalle_pedido->get($iddp);
				$producto_grupo_color=$this->M_producto_grupo_color->get_producto("pgc.idpgrc",$idpgrc);
				$parte_pedido=$this->M_parte_pedido->get($idpp);
				if(!empty($producto) && !empty($detalle_pedido) && !empty($producto_grupo_color)){
					$producto_proceso=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
					$producto_grupo_proceso=$this->M_producto_grupo_proceso->get_all();
					$producto_grupo_color_proceso=$this->M_producto_grupo_color_proceso->get_all();
					$procesos=$this->lib->producto_grupo_color_procesos($producto_grupo_color[0],$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso);
					$listado['procesos']=json_decode(json_encode($procesos));
					$listado['grupo']=$producto_grupo_color[0];
					$listado['color_sucursales']=$this->M_sucursal_detalle_pedido->get_search("iddp",$iddp);
					$listado['sucursales']=$this->M_sucursal_cliente->get_all();
					$listado['producto']=$producto[0];
					$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
					$listado['productos_pedido_empleado']=$this->M_detalle_pedido->producto_pedido_empleado("dp.idpp",$idpp);
					$listado['tbl_color_progress']=trim($_POST['tbl_color_progress']);
					$listado['tbl_producto_progress']=trim($_POST['tbl_producto_progress']);
					$listado['detalle_pedido']=$detalle_pedido[0];
					$listado['parte_pedido']=$parte_pedido[0];
					$this->load->view('produccion/produccion/6-config/producto/accordion_producto_color',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function asignar_empleado_sucursal(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['sdp']) && isset($_POST['p']) && isset($_POST['tbl-color']) && isset($_POST['tbl-producto'])){
				$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("idsdp",trim($_POST['sdp']));
				$producto=$this->M_producto->get(trim($_POST['p']));
				if(!empty($sucursal_detalle_pedido) && !empty($producto)){
					$detalle_pedido=$this->M_detalle_pedido->get_search('iddp',$sucursal_detalle_pedido[0]->iddp);
					$producto_grupo_color=$this->M_producto->get_colores("pgc.idpgrc",$detalle_pedido[0]->idpgrc,NULL);
					if(!empty($producto_grupo_color)){
						$sucursal=$this->M_sucursal_cliente->get_complet("sc.idsc",$sucursal_detalle_pedido[0]->idsc);
						$listado['sucursal']=$sucursal[0];
						//procesos del producto
						$listado['detalle_pedido']=$detalle_pedido[0];
						$listado['sucursal_detalle_pedido']=$sucursal_detalle_pedido[0];
						$listado['producto']=$producto[0];
						$listado['producto_grupo_color']=$producto_grupo_color[0];
						$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("pp.idp",$producto[0]->idp,"p.nombre","asc");
						$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
						$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
						$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
						$listado['productos_pedidos_empleados']=$this->M_producto_pedido_empleado->get_produccion("idsdp",$sucursal_detalle_pedido[0]->idsdp);
						$listado['tbl_color']=trim($_POST['tbl-color']);
						$listado['tbl_producto']=trim($_POST['tbl-producto']);
						$this->load->view('produccion/produccion/6-config/empleado_sucursal/view',$listado);
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}		
	}
	public function adicionar_empleado_sucursal_proceso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['pgc']) && isset($_POST['pr']) && isset($_POST['sdp'])){
				$idp=trim($_POST['p']);
				$idpgrc=trim($_POST['pgc']);
				$idpr=trim($_POST['pr']);
				$idsdp=trim($_POST['sdp']);
				$proceso=$this->M_proceso->get($idpr);
				$producto_grupo_color=$this->M_producto->get_colores("pgc.idpgrc",$idpgrc,NULL);
				$producto=$this->M_producto->get($idp);
				$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("idsdp",$idsdp);
				if(!empty($proceso) && !empty($producto_grupo_color) && !empty($producto) && !empty($sucursal_detalle_pedido)){
					$listado['proceso']=$proceso[0];
					$listado['producto']=$producto[0];
					$listado['producto_grupo_color']=$producto_grupo_color[0];
					$listado['idpr']=$idpr;
					$listado['idsdp']=$idsdp;
					$listado['empleados']=$this->M_producto_empleado->get_2n_empleado("pe.idpgrc",$idpgrc,"pre.idpr",$idpr,"pe.calidad","DESC");
					$listado['productos_pedido_empleado']=$this->M_producto_pedido_empleado->get_search('idsdp',$idsdp);
					$listado['sucursal_detalle_pedido']=$sucursal_detalle_pedido[0];
					$listado['productos_pedidos_empleados']=$this->M_producto_pedido_empleado->get_produccion("","");
					$this->load->view('produccion/produccion/6-config/empleado_sucursal/empleados/view',$listado);
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function adicionar_producto_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pgc']) && isset($_POST['pr'])){
				$idpgrc=trim($_POST['pgc']);
				$idpr=trim($_POST['pr']);
				$proceso=$this->M_proceso->get($idpr);
				$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
				if(!empty($proceso) && !empty($producto_grupo_color)){
					$listado['empleados']=$this->M_proceso_empleado->get_empleado("ep.idpr",$idpr,"p.nombre","ASC");
					$listado['proceso']=$proceso[0];
					$listado['idpgrc']=$idpgrc;
					$listado['producto_empleados']=$this->M_producto_empleado->get_row("idpgrc",$idpgrc);
					$this->load->view('produccion/produccion/6-config/empleado_sucursal/empleados/new_empleado',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_producto_pedido_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['cantidad']) && isset($_POST['max']) && isset($_POST['pre']) && isset($_POST['p']) && isset($_POST['sdp'])){
				$max=trim($_POST['max'])*1;
				$cantidad=trim($_POST['cantidad'])*1;
				$idpre=trim($_POST['pre']);
				$idsdp=trim($_POST['sdp']);
				if($cantidad>0 && $cantidad<=$max){
					$proceso_empleado=$this->M_proceso_empleado->get($idpre);
					$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get($idsdp);
					if(!empty($proceso_empleado) && !empty($sucursal_detalle_pedido)){
						$proceso=$this->M_proceso->get($proceso_empleado[0]->idpr);
						if($this->M_producto_pedido_empleado->insertar($idsdp,$idpre,$cantidad,0,NULL,NULL,NULL,$proceso_empleado[0]->ide,$proceso[0]->idpr,$proceso[0]->nombre,$proceso_empleado[0]->tipo)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function change_producto_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ppe']) && isset($_POST['sdp']) && isset($_POST['pr']) && isset($_POST['p'])){
				$idppe=trim($_POST['ppe']);
				$idsdp=trim($_POST['sdp']);
				$idpr=trim($_POST['pr']);
				$idp=trim($_POST['p']);
				$producto_pedido_empleado=$this->M_producto_pedido_empleado->get_search("idppe",$idppe);
				$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("idsdp",$idsdp);
				$proceso=$this->M_proceso->get($idpr);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto_pedido_empleado) && !empty($sucursal_detalle_pedido) && !empty($proceso)){
					$productos_pedido_empleados=$this->M_producto_pedido_empleado->get_search("idsdp",$idsdp);
					$asignado=0;
					for($i=0; $i<count($productos_pedido_empleados); $i++){
						if($productos_pedido_empleados[$i]->idpr==$idpr){
							$cantidad=$this->lib->desencriptar_num($productos_pedido_empleados[$i]->cantidad)*1;
							$asignado+=$cantidad;
						}
					}                                                  
					$empleado=$this->M_empleado->get_empleado("e.ide",$producto_pedido_empleado[0]->ide,true,NULL);
					$listado['asignado']=$asignado;
					$listado['producto_pedido_empleado']=$producto_pedido_empleado[0];
					$listado['sucursal_detalle_pedido']=$sucursal_detalle_pedido[0];
					$listado['proceso']=$proceso[0];
					$listado['empleado']=$empleado[0];
					$listado['producto']=$producto[0];
					$this->load->view('produccion/produccion/6-config/empleado_sucursal/cantidad_produccion',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_producto_pedido_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ppe']) && isset($_POST['asignado']) && isset($_POST['terminado']) && isset($_POST['fini']) && isset($_POST['ffin']) && isset($_POST['obs']) && isset($_POST['max']) && isset($_POST['sdp']) && isset($_POST['p'])){
				$max=trim($_POST['max'])*1;
				$idppe=trim($_POST['ppe']);
				$asignado=trim($_POST['asignado'])*1;
				$terminado=trim($_POST['terminado'])*1;
				$fini=str_replace("T", " ", trim($_POST['fini']));
				$ffin=str_replace("T", " ", trim($_POST['ffin']));
				$idsdp=trim($_POST['sdp']);
				$idp=trim($_POST['p']);
				$obs=trim($_POST['obs']);
				$producto_pedido_empleado=$this->M_producto_pedido_empleado->get_search("idppe",$idppe);
				if($asignado>0 && $asignado<=$max && $terminado>=0 && $terminado<=$asignado && $this->val->textarea($obs,0,500) && !empty($producto_pedido_empleado)){
					$vfini=explode(" ", $fini);$vffin=explode(" ", $ffin);
					if(count($vfini)==2){
						if(!$this->val->fecha($vfini[0])){ $fini=NULL;}
					}else{
						$fini=NULL;
					}
					if(count($vffin)==2){
						if(!$this->val->fecha($vffin[0])){ $ffin=NULL;}
					}else{
						$ffin=NULL;
					}
					if($this->M_producto_pedido_empleado->modificar_1($idppe,$asignado,$terminado,$fini,$ffin,$obs)){
						if($ffin!=NULL || $this->lib->desencriptar_num($producto_pedido_empleado[0]->terminado)*1!=$terminado*1){//actualizando porcentaje de produccion

							$sdp=$this->M_sucursal_detalle_pedido->get($idsdp);
							$producto=$this->M_producto->get($idp);
							if(!empty($sdp) && !empty($producto)){
								$dp=$this->M_detalle_pedido->get($sdp[0]->iddp);
								$color=$this->M_producto_grupo_color->get($dp[0]->idpgrc);
								$producto_procesos=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","ASC");
								$producto_grupo_procesos=$this->M_producto_grupo_proceso->get_all();
								$producto_grupo_color_procesos=$this->M_producto_grupo_color_proceso->get_all();
								$sucursales_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("iddp",$dp[0]->iddp);
								$producto_pedido_empleado=$this->M_detalle_pedido->producto_pedido_empleado("dp.iddp",$dp[0]->iddp);
								$procesos=$this->lib->producto_grupo_color_procesos($color[0],$producto_procesos,$producto_grupo_procesos,$producto_grupo_color_procesos);
								$procesos=json_decode(json_encode($procesos));
								$c_total=0;
								$c_entregada=0;
								foreach($procesos as $key => $proceso){
									for($i=0;$i<count($sucursales_detalle_pedido);$i++){ $sucursal=$sucursales_detalle_pedido[$i];
										$c_total+=($this->lib->desencriptar_num($sucursal->cantidad));
										/*$ppe=$this->lib->search_elemento_2n($producto_pedido_empleado,"idsdp",$sucursal->idsdp,"idpr",$proceso->idpr);
										if($ppe!=NULL){
											$terminado=$this->lib->desencriptar_num($ppe->terminado)*1;
											if($terminado>0){
												$c_entregada+=$terminado;
											}
										}*/
										for($j=0; $j < count($producto_pedido_empleado) ; $j++){
											if($producto_pedido_empleado[$j]->idsdp==$sucursal->idsdp && $producto_pedido_empleado[$j]->idpr==$proceso->idpr){
												$terminado=$this->lib->desencriptar_num($producto_pedido_empleado[$j]->terminado)*1;
												if($terminado>0){
													$c_entregada+=$terminado;
												}
											}
										}
									}
								}
								$porcentaje=0;
								if($c_total>0){ $porcentaje=$c_entregada/$c_total;}
								if($this->M_detalle_pedido->modificar_row($dp[0]->iddp,"porcentaje",$porcentaje));
							}
						}
						echo "ok";
					}else{
						echo "error";
					}
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_producto_pedido_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ppe'])){
				$idppe=trim($_POST['ppe']);
				$producto_pedido_empleado=$this->M_producto_pedido_empleado->get($idppe);
				if(!empty($producto_pedido_empleado)){
					$empleado=$this->M_empleado->get_empleado("e.ide",$producto_pedido_empleado[0]->ide,false,NULL);
					if(!empty($empleado)){
						$url=base_url().'libraries/img/';
						$img="sistema/miniatura/default.jpg";
						if($empleado[0]->fotografia!="" && $empleado[0]->fotografia!=NULL){ $img="personas/miniatura/".$empleado[0]->fotografia;}
						$listado["producto_pedido_empleado"]=$producto_pedido_empleado[0];
						$listado['empleado']=$empleado[0];
						$listado['open_control']=false;
						$listado['control']="";
						$listado['img']=$url.$img;
						$listado['titulo']="eliminar el empleado  <strong>".$empleado[0]->nombre_completo."</strong> del proceso de producción";
						$listado['desc']="Se eliminara definitivamente el empleado del proceso de producción.";
						$this->load->view('estructura/form_eliminar',$listado);
					}else{ echo "fail";}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_producto_pedido_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ppe']) && isset($_POST['p'])){
				$idppe=trim($_POST["ppe"]);
				$producto_pedido_empleado=$this->M_producto_pedido_empleado->get_search("idppe",$idppe);
				if(!empty($producto_pedido_empleado)){
					if($producto_pedido_empleado[0]->fecha_fin=="" || $producto_pedido_empleado[0]->fecha_fin==NULL){
						if($this->M_producto_pedido_empleado->eliminar($idppe)){
							if($this->lib->desencriptar_num($producto_pedido_empleado[0]->terminado)*1>0){
								$idp=trim($_POST['p']);
								$sdp=$this->M_sucursal_detalle_pedido->get($producto_pedido_empleado[0]->idsdp);
								$producto=$this->M_producto->get($idp);
								if(!empty($sdp) && !empty($producto)){
									$dp=$this->M_detalle_pedido->get($sdp[0]->iddp);
									$color=$this->M_producto_grupo_color->get($dp[0]->idpgrc);
									$producto_procesos=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","ASC");
									$producto_grupo_procesos=$this->M_producto_grupo_proceso->get_all();
									$producto_grupo_color_procesos=$this->M_producto_grupo_color_proceso->get_all();
									$sucursales_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("iddp",$dp[0]->iddp);
									$producto_pedido_empleado=$this->M_detalle_pedido->producto_pedido_empleado("dp.iddp",$dp[0]->iddp);
									$procesos=$this->lib->producto_grupo_color_procesos($color[0],$producto_procesos,$producto_grupo_procesos,$producto_grupo_color_procesos);
									$procesos=json_decode(json_encode($procesos));
									$c_total=0;
									$c_entregada=0;
									foreach($procesos as $key => $proceso){
										for($i=0;$i<count($sucursales_detalle_pedido);$i++){ $sucursal=$sucursales_detalle_pedido[$i];
											$c_total+=($this->lib->desencriptar_num($sucursal->cantidad));
											for($j=0; $j < count($producto_pedido_empleado) ; $j++){
												if($producto_pedido_empleado[$j]->idsdp==$sucursal->idsdp && $producto_pedido_empleado[$j]->idpr==$proceso->idpr){
													$terminado=$this->lib->desencriptar_num($producto_pedido_empleado[$j]->terminado)*1;
													if($terminado>0){
														$c_entregada+=$terminado;
													}
												}
											}
										}
									}
									$porcentaje=0;
									if($c_total>0){ $porcentaje=$c_entregada/$c_total;}
									if($this->M_detalle_pedido->modificar_row($dp[0]->iddp,"porcentaje",$porcentaje));
								}
							}
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function detalle_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['sdp']) && isset($_POST['p']) && isset($_POST['type'])){
				$idp=trim($_POST['p']);
				$idsdp=trim($_POST['sdp']);
				$type=trim($_POST['type']);
				$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido ->get_search("idsdp",$idsdp);
				$producto=$this->M_producto->get($idp);
				if(!empty($sucursal_detalle_pedido) && !empty($producto)){
					$detalle_pedido=$this->M_detalle_pedido->get_search("iddp",$sucursal_detalle_pedido[0]->iddp);
					$producto_grupo_color=$this->M_producto->get_colores("pgc.idpgrc",$detalle_pedido[0]->idpgrc,NULL);
					if(!empty($producto_grupo_color)){
						$sucursal=$this->M_sucursal_cliente->get_complet("sc.idsc",$sucursal_detalle_pedido[0]->idsc);
						$listado['sucursal']=$sucursal[0];
						$listado['type']=$type;
						//procesos del producto
						$producto_proceso=$this->M_producto_proceso->producto_proceso("pp.idp",$producto[0]->idp,"p.nombre","asc");
						$producto_grupo_proceso=$this->M_producto_grupo_proceso->get_all();
						$producto_grupo_color_proceso=$this->M_producto_grupo_color_proceso->get_all();						
						$procesos=$this->lib->producto_grupo_color_procesos($producto_grupo_color[0],$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso);
						$listado['procesos']=json_decode(json_encode($procesos));
						$listado['sucursal_detalle_pedido']=$sucursal_detalle_pedido[0];
						$listado['producto']=$producto[0];
						$listado['producto_grupo_color']=$producto_grupo_color[0];
						//$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("pp.idp",$producto[0]->idp,"p.nombre","asc");
						//$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
						//$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
						$listado['productos_procesos_empleados']=$this->M_producto_empleado->get_empleado("","","pe.calidad","desc");
						$listado['productos_pedidos_empleados']=$this->M_producto_pedido_empleado->get_produccion("idsdp",$sucursal_detalle_pedido[0]->idsdp);
						$this->load->view('produccion/produccion/6-config/empleado_sucursal/detalle_produccion',$listado);
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- End configuracion ---*/
	public function save_porcentaje(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['dp']) && isset($_POST['porcentaje'])){
				$iddp=trim($_POST['dp']);
				$porcentaje=trim($_POST['porcentaje'])*1;
				$detalle_pedido=$this->M_detalle_pedido->get($iddp);
				if(!empty($detalle_pedido) && $porcentaje>0 && $porcentaje<=100){
					$porcentaje/=100;
					if($this->M_detalle_pedido->modificar_row($iddp,"porcentaje",$porcentaje)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*----- END MANEJO DE PRODUCCION -----*/
/*------- MANEJO DE SEGUIMIENTO -------*/
	public function search_seguimiento(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"produccion");
			if($privilegio){
				$listado['privilegio']=$privilegio;
				$this->load->view('produccion/seguimiento/search',$listado);
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_seguimiento(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"produccion");
			if(!empty($privilegio)){
				$estado="";
				if(isset($_POST['est'])){if($_POST['est']==0 || $_POST['est']==1){ $estado=trim($_POST['est']);}}else{ $estado="1";}
				$atrib="";$val="";$type_search="";
				if(isset($_POST['cod']) && isset($_POST['cod2']) && isset($_POST['nom']) && isset($_POST['fec'])){
					if($_POST['cod']!=""){
						$atrib='codigo';$val=$_POST['cod'];$type_search="like";
					}else{
						if($_POST['cod2']!=""){
							$atrib='codigo_aux';$val=$_POST['cod2'];$type_search="like";
						}else{
							if($_POST['nom']!=""){
								$atrib='nombre';$val=trim($_POST['nom']);$type_search="like";
							}else{
								if($_POST['fec']!=""){
									$atrib='fecha_creacion';$val=$_POST['fec'];$type_search="equals";
								}
							}
						}
					}
				}
				$listado['productos']=$this->M_producto->get_search("","","");
				$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("","");
				$listado['privilegio']=$privilegio[0];
				$listado['atrib']=$atrib;
				$listado['val']=$val;
				$listado['type_search']=$type_search;
				$listado['estado']=$estado;
				$this->load->view('produccion/seguimiento/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- Nuevo ---*/
	/*--- End Nuevo ---*/
	/*--- Imprimir ---*/
	/*--- End Imprimir ---*/
	/*--- Reportes ---*/
	/*--- End Reportes ---*/
	/*--- Configuración ---*/
	/*--- End configuración ---*/
	/*--- Eliminar ---*/
	/*--- End Eliminar ---*/
/*------- END MANEJO DE SEGUIMIENTO -------*/







/*------ CONFIGURACION ---------*/
	public function view_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","1");
			$this->load->view('produccion/config/view',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_producto_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";$val="";
			if(isset($_POST['nom'])){
				$atrib="p.nombre";$val=trim($_POST['nom']);
			}
			$listado['productos_grupos_colores']=$this->M_producto->get_colores($atrib,$val,"1");
			$this->load->view('produccion/config/producto/view',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- HOJA DE COSTOS ----*/
	public function save_costo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pgc']) && isset($_POST['costo'])){
				$idpgrc=$_POST['pgc'];
				$costo=$_POST['costo'];
				$pgc=$this->M_producto_grupo_color->get_row("idpgrc",$idpgrc);
				if(!empty($pgc) && $this->val->decimal($costo,6,1) && $costo>=0 && $costo<=999999.9){
					if($this->M_producto_grupo_color->modificar_row($idpgrc,'costo',$costo)){
						if($pgc[0]->costo!=$costo){
							$msj="Modificó el costo de <strong>".$pgc[0]->costo."Bs.</strong> a <strong>".$costo."Bs.</strong>";
							$color=$this->M_color->get($pgc[0]->idco);
							$producto_grupo=$this->M_producto_grupo->get($pgc[0]->idpgr);
							$producto=$this->M_producto->get($producto_grupo[0]->idp);
							$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
							$cod=$producto[0]->codigo;
							$prod=$producto[0]->nombre;
							if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$pgc[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"uca",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_privilegio->get_usuario_producion("","config"),$this->M_notificacion->get_all(),"producto_categoria_costo");
								foreach($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "ok";
							}
						}else{
							echo "ok";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- END HOJA DE COSTOS ----*/
/*------ END CONFIGURACION ---------*/
/*----- FUNCIONES GENERICAS -----*/
	/*Manejo de atributos en el producto*/
		public function search_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['type'])){
					$listado = array();
					if($_POST["type"]=="grupo"){
						if(isset($_POST['pg'])){
							$listado['idpg']=$_POST['pg'];
						}						
					}
					if($_POST["type"]=="color"){
						if(isset($_POST['pgc'])){
							$listado['idpgc']=$_POST['pgc'];
						}						
					}
					$listado['type']=$_POST['type'];
					$this->load->view('produccion/producto/3-nuevo/atributos/search',$listado);
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function view_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['nom'])){
					if($_POST['nom']!=""){
						$listado['atributos']=$this->M_atributo->get_search("atributo",$_POST["nom"]);
					}else{
						$listado['atributos']=$this->M_atributo->get_search("","");
					}
				}else{
					$listado['atributos']=$this->M_atributo->get_search("","");
				}
				$this->load->view('produccion/producto/3-nuevo/atributos/view',$listado);
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function new_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$this->load->view('produccion/producto/3-nuevo/atributos/new');
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function save_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['nom'])){
					if($this->val->strSpace($_POST['nom'],2,100)){
						if($this->M_atributo->insertar(trim($_POST['nom']))){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function update_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['id']) && isset($_POST['nom'])){
					$control=$this->M_atributo->get($_POST['id']);
					if($this->val->strSpace($_POST['nom'],2,100) && !empty($control)){
						if($this->M_atributo->modificar($_POST['id'],trim($_POST['nom']))){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
	   	public function confirm_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
				if(!empty($privilegio)){
					if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
						if(isset($_POST['id'])){
							$idatr=$_POST['id'];
							$atributo=$this->M_atributo->get($idatr);
							if(!empty($atributo)){
								$listado['open_control']="true";
								$listado['control']="";
								$control=$this->M_producto_atributo->get_row('idatr',$idatr);
								if(!empty($control)){
									$listado['titulo']="";
									$listado['desc']="Imposible eliminar el atributo se encuentra siendo usado por ".count($control)." productos.";	
									$listado['btn_ok']="none";
								}else{
									$listado['titulo']="eliminar el atributo <strong>".$atributo[0]->atributo."</strong>";
									$listado['desc']="Se eliminara definitivamente el atributo.";
								}
								$this->load->view('estructura/form_eliminar',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function drop_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['id'])){
					$id=$_POST['id'];
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							$atributo=$this->M_atributo->get($id);
							$control=$this->M_producto_atributo->get_row('idatr',$id);
							if(!empty($atributo) && empty($control)){
								if($this->M_atributo->eliminar($id)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}	
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "fail";
				}			
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
	/*End manejo de atributos en el producto*/
	/*Manejo de atributos*/
		public function adicionar_atributo(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['id'])){
					$id=$_POST['id'];
					$atributo=$this->M_atributo->get($id);
					if(!empty($atributo)){
						$pg="";$type="";
						if(isset($_POST['type'])){
							if($_POST['type']!="producto" && isset($_POST['pg'])){
								$pg="data-pg='".$_POST['pg']."'";
							}
							if($_POST['type']=="color"){
								$pg.=" data-pgc='".$_POST['pgc']."'";
							}
							$type="data-type='".$_POST['type']."'";
						}
						$help1='title="'.$atributo[0]->atributo.'" data-content="Ingrese un contenido alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
						$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
						$tipo="'child'";
						$id_a="atr".rand(10,9999);
						$resp='<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">'.$atributo[0]->atributo.'</label>
						<div class="col-sm-4 col-xs-12">
							<div class="input-group">
								<textarea id="atr'.rand(1,9999).$atributo[0]->idatr.'" maxlength="150" class="form-control form-control-sm" data-a='.$atributo[0]->idatr.' data-typele="child" '.$type.' '.$pg.' placeholder="'.$atributo[0]->atributo.'"></textarea>
								<span class="input-group-addon form-control-sm btn-default" '.$popover.$help1.'><i class="fa fa-info-circle"></i></span>
								<a href="javascript:" class="input-group-addon form-control-sm btn-default eliminar_atributo" id="'.$id_a.'"><i class="fa fa-trash-o"></i></a>
							</div><script>$("a#'.$id_a.'").eliminar_atributo();</script>
						</div><span></span>';
						echo $resp;
					}else{
						echo "";
					}	
				}else{
					echo "";
				}			
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}	
	/*End Manejo de atributos*/
	/*Manejo de materiales en el producto*/
			public function search_material_producto(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['p']) && isset($_POST['type'])){
						$idp=$_POST['p'];
						$type=$_POST['type'];
						$producto=$this->M_producto->get($idp);
						if(!empty($producto)){
							$listado['producto']=$producto[0];
							$listado['type']=$type;
							if($type=="pgm-modal"){ if(isset($_POST['pg'])){ $listado['pg']=$_POST['pg'];}}
							if($type=="pgcm-modal"){ if(isset($_POST['pgc'])){ $listado['pgc']=$_POST['pgc'];}}
							if($type=="pgcm-modal-change"){ if(isset($_POST['pgcm'])){ $listado['pgcm']=$_POST['pgcm'];}} 
							$this->load->view('produccion/producto/6-config/producto_materiales/search',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function view_material_producto(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['p']) && isset($_POST['type'])){
						$idp=$_POST['p'];
						$type=$_POST['type'];
						$atrib="";$val="";
						if($this->val->entero($idp,0,10)){
							if(isset($_POST['mat'])){
								if($_POST['mat']!=""){
									$atrib="mi.nombre";$val=$_POST['mat'];
								}
							}
							$listado['materiales']=$this->M_producto_material->get_search_producto($idp,$atrib,$val);
							if($type=="pgm-modal"){
								$listado['grupo_materiales']=$this->M_producto_grupo_material->get_search_grupo($_POST['pg'],$atrib,$val);
							}
							if($type=="pgcm-modal"){
								$listado['color_materiales']=$this->M_producto_grupo_color_material->get_search_grupo_color($_POST['pgc'],$atrib,$val);
								$aux=$this->M_producto_grupo_color->get($_POST['pgc']);
								$listado['grupo_materiales']=$this->M_producto_grupo_material->get_search_grupo($aux[0]->idpgr,$atrib,$val);
							}
							$listado['type']=$type;
							$this->load->view('produccion/producto/6-config/producto_materiales/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function update_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['can']) && isset($_POST['ver']) && isset($_POST['obs']) && isset($_POST['type'])){
						$can=trim($_POST['can']);
						$ver=trim($_POST['ver']);
						$obs=trim($_POST['obs']);
						$type=trim($_POST['type']);
						if($this->val->decimal($can,7,4) && $can>0 && $can<=999999.9999 && $this->val->textarea($obs,0,200) && ($ver=="0" || $ver=="1")){
							if($type=="pm-modal"){
								$idpm=trim($_POST['idpm']);
								$producto_material=$this->M_producto_material->get($idpm);
								if($this->M_producto_material->modificar($idpm,$can,$ver,$obs)){
									$msj="";
									if($producto_material[0]->cantidad!=$can){
										if($msj!=""){ $msj.=", ";}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_material[0]->idm);}
										$msj.="Modificó la cantidad del material <strong>".$material[0]->nombre."</strong> de <strong>".$producto_material[0]->cantidad."</strong> a <strong>".$can."</strong>";
									}
									if($producto_material[0]->verificado!=$ver){
										if($msj!=""){ $msj.=", ";}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_material[0]->idm);}
										$msj.="Cambió el estado del material <strong>".$material[0]->nombre."</strong>";
									}
									if($producto_material[0]->observacion!=$obs){
										if($msj!=""){ $msj.=", ";}
										$obs1="[vacio]";$obs2="[vacio]";
										if($producto_material[0]->observacion!="" || $producto_material[0]->observacion!=null){ $obs1=$producto_material[0]->observacion;}
										if($obs!="" || $obs!=null){ $obs2=$obs;}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_material[0]->idm);}
										if($msj==""){
											$msj="Modificó la observación del material <strong>".$material[0]->nombre."</strong> de ";
										}else{
											$msj.="y la observación de ";
										}
										$msj.="<strong>".$obs1."</strong> a <strong>".$obs2."</strong>";
									}
									if($msj!=""){
										$producto=$this->M_producto->get($producto_material[0]->idp);
										$prod=$producto[0]->codigo." ".$producto[0]->nombre;
										if($this->M_producto_seg->insertar($producto[0]->idp,$prod,"um",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
										foreach ($resultados as $key => $result) {
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "ok";
									}
								}else{
									echo "error";
								}
							}
							if($type=="pgm-modal"){
								$idpgm=trim($_POST['pgm']);
								$producto_grupo_material=$this->M_producto_grupo_material->get($idpgm);
								if($this->M_producto_grupo_material->modificar($idpgm,$can,$ver,$obs)){
									//notificacion
									$msj="";
									if($producto_grupo_material[0]->cantidad!=$can){
										if($msj!=""){ $msj.=", ";}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_grupo_material[0]->idm);}
										$msj.="modificó la cantidad del material <strong>".$material[0]->nombre."</strong> de <strong>".$producto_grupo_material[0]->cantidad."</strong> a <strong>".$can."</strong>";
									}
									if($producto_grupo_material[0]->verificado!=$ver){
										if($msj!=""){ $msj.=", ";}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_grupo_material[0]->idm);}
										$msj.="Cambió el estado del material <strong>".$material[0]->nombre."</strong>";
									}
									if($producto_grupo_material[0]->observacion!=$obs){
										if($msj!=""){ $msj.=", ";}
										$obs1="[vacio]";$obs2="[vacio]";
										if($producto_grupo_material[0]->observacion!="" || $producto_grupo_material[0]->observacion!=null){ $obs1=$producto_grupo_material[0]->observacion;}
										if($obs!="" || $obs!=null){ $obs2=$obs;}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_grupo_material[0]->idm);}
										if($msj==""){
											$msj="Modificó la observación del material <strong>".$material[0]->nombre."</strong> de ";
										}else{
											$msj.="y la observación de ";
										}
										$msj.="<strong>".$obs1."</strong> a <strong>".$obs2."</strong>";
									}
									if($msj!=""){
										$producto_grupo=$this->M_producto_grupo->get($producto_grupo_material[0]->idpgr);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"ugm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach ($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "ok";
									}
								}else{
									echo "error";
								}
							}
							if($type=="pgcm-modal"){
								$idpgcm=trim($_POST['pgcm']);
								$producto_grupo_color_material=$this->M_producto_grupo_color_material->get($idpgcm);
								if($this->M_producto_grupo_color_material->modificar($idpgcm,$can,$ver,$obs)){
									//notificacion
									$msj="";
									if($producto_grupo_color_material[0]->cantidad!=$can){
										if($msj!=""){ $msj.=", ";}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_grupo_color_material[0]->idm);}
										$msj.="modificó la cantidad del material <strong>".$material[0]->nombre."</strong> de <strong>".$producto_grupo_color_material[0]->cantidad."</strong> a <strong>".$can."</strong>";
									}
									if($producto_grupo_color_material[0]->verificado!=$ver){
										if($msj!=""){ $msj.=", ";}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_grupo_color_material[0]->idm);}
										$msj.="Cambió el estado del material <strong>".$material[0]->nombre."</strong>";
									}
									if($producto_grupo_color_material[0]->observacion!=$obs){
										if($msj!=""){ $msj.=", ";}
										$obs1="[vacio]";$obs2="[vacio]";
										if($producto_grupo_color_material[0]->observacion!="" || $producto_grupo_color_material[0]->observacion!=null){ $obs1=$producto_grupo_color_material[0]->observacion;}
										if($obs!="" || $obs!=null){ $obs2=$obs;}
										if(empty($material)){ $material=$this->M_material_item->get_material("m.idm",$producto_grupo_color_material[0]->idm);}
										if($msj==""){
											$msj="Modificó la observación del material <strong>".$material[0]->nombre."</strong> de ";
										}else{
											$msj.="y la observación de ";
										}
										$msj.="<strong>".$obs1."</strong> a <strong>".$obs2."</strong>";
									}
									if($msj!=""){
										$producto_grupo_color=$this->M_producto_grupo_color->get($producto_grupo_color_material[0]->idpgrc);
										$color=$this->M_color->get($producto_grupo_color[0]->idco);
										$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ugcm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach ($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "ok";
									}
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		   	public function confirm_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['id']) && isset($_POST['type'])){
								if($_POST['type']=="pm" || $_POST['type']=="pm-modal"){
									$idpm=$_POST['id'];
									$producto_material=$this->M_producto_material->get_complet($idpm);
									if(!empty($producto_material)){
										$url=base_url().'libraries/img/';
			    						$img="sistema/miniatura/default.jpg";
									    if($producto_material[0]->fotografia!="" && $producto_material[0]->fotografia!=NULL){ $img="materiales/miniatura/".$producto_material[0]->fotografia; }
									    $listado['img']=$url.$img;
										$listado['open_control']="true";
										$listado['control']="";
										$listado['titulo']="eliminar el material <strong>".$producto_material[0]->nombre."</strong>";
										$listado['desc']="Se eliminara definitivamente el material del producto.";
										$this->load->view('estructura/form_eliminar',$listado);
									}else{
										echo "fail $idpm;";
									}									
								}
								if($_POST['type']=="pgm" || $_POST['type']=="pgm-modal"){
									$idpgm=$_POST['id'];
									$producto_grupo_material=$this->M_producto_grupo_material->get_material('pgm.idpgm',$idpgm);
									if(!empty($producto_grupo_material)){
										$url=base_url().'libraries/img/';
			    						$img="sistema/miniatura/default.jpg";
									    if($producto_grupo_material[0]->fotografia!="" && $producto_grupo_material[0]->fotografia!=NULL){ $img="materiales/miniatura/".$producto_grupo_material[0]->fotografia; }
									    $listado['img']=$url.$img;
										$listado['open_control']="true";
										$listado['control']="";
										$listado['titulo']="eliminar el material <strong>".$producto_grupo_material[0]->nombre."</strong>";
										$listado['desc']="Se eliminara definitivamente el material del producto.";
										$this->load->view('estructura/form_eliminar',$listado);
									}else{
										echo "fail;";
									}									
								}
								if($_POST['type']=="pgcm" || $_POST['type']=="pgcm-modal"){
									$idpgcm=$_POST['id'];
									$producto_grupo_color_material=$this->M_producto_grupo_color_material->get_material('pgcm.idpgcm',$idpgcm);
									if(!empty($producto_grupo_color_material)){
										$url=base_url().'libraries/img/';
			    						$img="sistema/miniatura/default.jpg";
									    if($producto_grupo_color_material[0]->fotografia!="" && $producto_grupo_color_material[0]->fotografia!=NULL){ $img="materiales/miniatura/".$producto_grupo_color_material[0]->fotografia; }
									    $listado['img']=$url.$img;
										$listado['open_control']="true";
										$listado['control']="";
										$listado['titulo']="eliminar el material <strong>".$producto_grupo_color_material[0]->nombre."</strong>";
										$listado['desc']="Se eliminara definitivamente el material del producto.";
										$this->load->view('estructura/form_eliminar',$listado);
									}else{
										echo "fail;";
									}									
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function drop_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['type'])){
						$type=$_POST['type'];
						$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
						if(!empty($privilegio)){
							if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
								if($type=="pm" || $type=="pm-modal"){
									$id=$_POST['pm'];
									$producto_material=$this->M_producto_material->get($id);
									if($this->M_producto_material->eliminar($id)){
										if(!empty($producto_material)){
											$material=$this->M_material_item->get_material('m.idm',$producto_material[0]->idm);
											$producto=$this->M_producto->get($producto_material[0]->idp);
											$msj="Eliminó el material <strong>".$material[0]->nombre."</strong>";
											$prod=$producto[0]->codigo." ".$producto[0]->nombre;
											if($this->M_producto_seg->insertar($producto_material[0]->idp,$prod,"dm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
											$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
											foreach ($resultados as $key => $result) {
												if($result->idno=="none"){
													if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){ }
												}else{
													if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){ }
												}
											}
											$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
											echo json_encode($result);
										}else{
											echo "ok";
										}
									}else{
										echo "error";
									}
								}
								if($type=="pgm" || $type=="pgm-modal"){
									$id=$_POST['pgm'];
									$producto_grupo_material=$this->M_producto_grupo_material->get($id);
									if(!empty($producto_grupo_material)){
										if($this->M_producto_grupo_material->eliminar($id)){
											if(!empty($producto_grupo_material)){
												$material=$this->M_material_item->get_material('m.idm',$producto_grupo_material[0]->idm);
												$producto_grupo=$this->M_producto_grupo->get($producto_grupo_material[0]->idpgr);
												$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
												$producto=$this->M_producto->get($producto_grupo[0]->idp);
												$cod=$producto[0]->codigo;
												$prod=$producto[0]->nombre;
												if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
												$prod=$cod." ".$prod;
												$msj="Eliminó el material <strong>".$material[0]->nombre."</strong> de la categoría";
												if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"dgm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
												foreach ($resultados as $key => $result) {
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
													}
												}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}else{
												echo "ok";
											}
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}
								if($type=="pgcm" || $type=="pgcm-modal"){
									$id=$_POST['pgcm'];
									$producto_grupo_color_material=$this->M_producto_grupo_color_material->get($id);
									if($this->M_producto_grupo_color_material->eliminar($id)){
										if(!empty($producto_grupo_color_material)){
											$material=$this->M_material_item->get_material('m.idm',$producto_grupo_color_material[0]->idm);
											$producto_grupo_color=$this->M_producto_grupo_color->get($producto_grupo_color_material[0]->idpgrc);
											$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
											$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
											$color=$this->M_color->get($producto_grupo_color[0]->idco);
											$producto=$this->M_producto->get($producto_grupo[0]->idp);
											$msj="Eliminó el material <strong>".$material[0]->nombre."</strong>";
											$cod=$producto[0]->codigo;
											$prod=$producto[0]->nombre;
											if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
											$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
											$prod=$cod." ".$prod;
											if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"dgcm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
											$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
											foreach ($resultados as $key => $result) {
												if($result->idno=="none"){
													if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
												}else{
													if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
												}
											}
											$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
											echo json_encode($result);
										}else{
											echo "ok";
										}
									}else{
										echo "error";
									}
								}
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}			
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		/*Manejo de materiales*/
			public function search_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['type'])){
						$control=true;
						$listado['almacenes']=$this->M_almacen->get_row('tipo','0');
						$listado['type']=$_POST['type'];
						if($_POST['type']=="pm-modal-change"){ $listado['idpm']=$_POST['pm'];}
						if($_POST['type']=="pgm" || $_POST['type']=="pgm-modal"){ $listado['pg']=$_POST['pg'];}
						if($_POST['type']=="pgm-modal-change"){ $listado['pgm']=$_POST['pgm'];}
						if($_POST['type']=="pgcm" || $_POST['type']=="pgcm-modal"){ $listado['pgc']=$_POST['pgc'];}
						if($_POST['type']=="pgcm-modal-change"){ $listado['pgcm']=$_POST['pgcm'];}
						if($_POST['type']=="pim" || $_POST['type']=="pim-modal"){
							if(isset($_POST['container'])){
								$listado['container']=$_POST['container'];
							}else{
								$control=false;
							}
						}
						if($control){
							$this->load->view('produccion/producto/6-config/producto_materiales/materiales/search',$listado);
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function view_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['type'])){
						$type=$_POST['type'];
						$atrib="";$val="";$almacen="";
						$control=true;
						if(isset($_POST['mat'])){
							if($_POST['mat']!=""){
								$atrib="mi.nombre";$val=$_POST['mat'];
							}
						}
						if(isset($_POST['alm'])){
							if($this->val->entero($_POST['alm'],1,10)){
								$almacen=$_POST['alm'];
							}
						}
						if($almacen!=""){
							$listado['materiales']=$this->M_almacen_material->get_search($almacen,$atrib,$val);
						}else{
							$listado['materiales']=$this->M_material_item->get_search($atrib,$val);
						}
						if($_POST['type']!="pim" && $_POST['type']!="pim-modal"){
							if(isset($_POST['p'])){
								$listado['producto_materiales']=$this->M_producto_material->get_row('idp',$_POST['p']);
								//hallando los materiales en grupo y color
								$producto_grupos=$this->M_producto_grupo->get_all();
								$producto_grupos_materiales=$this->M_producto_grupo_material->get_all();
								$grupos=$this->M_grupo->get_all();
								$productos_grupos_colores=$this->M_producto_grupo_color->get_all();
								$productos_grupos_colores_materiales=$this->M_producto_grupo_color_material->get_all();
								$colores=$this->M_color->get_all();
								$listado['j_pgm']=$this->lib->producto_grupo_material($_POST['p'],$producto_grupos,$producto_grupos_materiales,$grupos);
								$listado['j_pgcm']=$this->lib->producto_grupo_color_material($_POST['p'],$producto_grupos,$grupos,$productos_grupos_colores,$productos_grupos_colores_materiales,$colores);
								//End hallando los materiales en grupo y color
							}else{
								$control=false;
							}
						}
						if($control){
							$listado['type']=$_POST['type'];
							if($_POST['type']=="pm-modal-change"){ $listado['idpm']=$_POST['pm'];}
							if($_POST['type']=="pgm" || $_POST['type']=="pgm-modal"){ $listado['pg']=$_POST['pg']; }
							if($_POST['type']=="pgm-modal-change"){ $listado['pgm']=$_POST['pgm'];}
							if($_POST['type']=="pgcm" || $_POST['type']=="pgcm-modal"){ $listado['pgc']=$_POST['pgc'];}
							if($_POST['type']=="pgcm-modal-change"){ $listado['pgcm']=$_POST['pgcm'];}
							$this->load->view('produccion/producto/6-config/producto_materiales/materiales/view',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function seleccionar_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['m']) && isset($_POST['can']) && isset($_POST['obs']) && isset($_POST['p']) && isset($_POST['type'])){
						$idm=$_POST['m'];
						$idp=$_POST['p'];
						$can=$_POST['can'];
						$obs=$_POST['obs'];
						$type=$_POST['type'];
						if($this->val->entero($idm,0,10) && $this->val->entero($idp,0,10) && $this->val->decimal($can,6,4) && $can>0 && $can<=999999.9999 && $this->val->textarea($obs,0,200)){
							if($type=="pm" || $type=="pm-modal"){
								if($this->M_producto_material->insertar($idp,$idm,$can,$obs)){
									//para notificaciones
									$material=$this->M_material_item->get_material("m.idm",$idm);
									$producto=$this->M_producto->get($idp);
									$msj="adicionó el material <strong>".$material[0]->nombre."</strong>";
									$prod=$producto[0]->codigo." ".$producto[0]->nombre;
									if($this->M_producto_seg->insertar($producto[0]->idp,$prod,"cm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
									$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
									foreach ($resultados as $key => $result){
										if($result->idno=="none"){
											if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){ }
										}else{
											if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){ }
										}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
								}else{
									echo "error";
								}
							}
							if($type=="pgm" || $type=="pgm-modal"){
								if(isset($_POST['pg'])){
									$idpg=$_POST['pg'];
									if($this->M_producto_grupo_material->insertar($idpg,$idm,$can,$obs)){
										//notificaciones
										$material=$this->M_material_item->get_material("m.idm",$idm);
										$producto=$this->M_producto->get($idp);
										$producto_grupo=$this->M_producto_grupo->get($idpg);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$prod=$cod." ".$prod;
										$msj="adicionó el material <strong>".$material[0]->nombre."</strong> en la categoría";
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$idpg,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"cgm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach ($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}
							if($type=="pgcm" || $type=="pgcm-modal"){
								if(isset($_POST['pgc'])){
									$idpgc=$_POST['pgc'];
									if($this->M_producto_grupo_color_material->insertar($idpgc,$idm,$can,$obs)){
										//para notificaciones
										$material=$this->M_material_item->get_material("m.idm",$idm);
										$producto=$this->M_producto->get($idp);
										$producto_grupo_color=$this->M_producto_grupo_color->get($idpgc);
										$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$color=$this->M_color->get($producto_grupo_color[0]->idco);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
										$msj="adicionó el material <strong>".$material[0]->nombre."</strong>";
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$idpgc,$color[0]->idco,$color[0]->nombre,"cgcm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach ($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function change_material(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['m']) && isset($_POST['can']) && isset($_POST['obs']) && isset($_POST['type2'])){
						$idm=$_POST['m'];
						$can=$_POST['can'];
						$obs=$_POST['obs'];
						$type=$_POST['type2'];
						
						if($this->val->entero($idm,0,10) && $this->val->decimal($can,6,4) && $can>0 && $can<=999999.9999 && $this->val->textarea($obs,0,200)){
							if($type=="pm-modal-change"){
								if(isset($_POST['pm'])){
									$idpm=$_POST['pm'];
									$producto_material=$this->M_producto_material->get($idpm);
									if($this->M_producto_material->modificar2($idpm,$idm,$can,$obs)){
										//para notificaciones
										$material_anterior=$this->M_material_item->get_material('m.idm',$producto_material[0]->idm);
										$material_actual=$this->M_material_item->get_material('m.idm',$idm);
										$producto=$this->M_producto->get($producto_material[0]->idp);
										$msj="Cambió el material <strong>".$material_anterior[0]->nombre."</strong> por <strong>".$material_actual[0]->nombre."</strong>";
										$prod=$producto[0]->codigo." ".$producto[0]->nombre;
										if($this->M_producto_seg->insertar($producto[0]->idp,$prod,"um",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
										foreach ($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}
							if($type=="pgm-modal-change"){
								if(isset($_POST['pgm'])){
									$idpgm=$_POST['pgm'];
									$producto_grupo_material=$this->M_producto_grupo_material->get($idpgm);
									if($this->M_producto_grupo_material->modificar2($idpgm,$idm,$can,$obs)){
										//para notificaciones
										$producto_grupo=$this->M_producto_grupo->get($producto_grupo_material[0]->idpgr);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$material_anterior=$this->M_material_item->get_material('m.idm',$producto_grupo_material[0]->idm);
										$material_actual=$this->M_material_item->get_material('m.idm',$idm);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$prod=$cod." ".$prod;
										$msj="Cambió el material <strong>".$material_anterior[0]->nombre."</strong> por <strong>".$material_actual[0]->nombre."</strong>";
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,NULL,NULL,NULL,"ugm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}
							if($type=="pgcm-modal-change"){
								if(isset($_POST['pgcm'])){
									$idpgcm=$_POST['pgcm'];
									$producto_grupo_color_material=$this->M_producto_grupo_color_material->get($idpgcm);
									if($this->M_producto_grupo_color_material->modificar2($idpgcm,$idm,$can,$obs)){
										//para notificaciones
										$material_anterior=$this->M_material_item->get_material('m.idm',$producto_grupo_color_material[0]->idm);
										$material_actual=$this->M_material_item->get_material('m.idm',$idm);
										$producto_grupo_color=$this->M_producto_grupo_color->get($producto_grupo_color_material[0]->idpgrc);
										$color=$this->M_color->get($producto_grupo_color[0]->idco);
										$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$msj="Cambió el material <strong>".$material_anterior[0]->nombre."</strong> por <strong>".$material_actual[0]->nombre."</strong>";
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
										$prod=$cod." ".$prod;
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ugcm",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){ }
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){ }
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
		public function add_pieza_marterial(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['m']) && isset($_POST['type'])){
					$idm=trim($_POST['m']);
					$type=trim($_POST['type']);
					$material=$this->M_material_item->get_material("m.idm",$idm);
					if(!empty($material)){
						$url=base_url().'libraries/img/';
						$img="sistema/miniatura/default.jpg";
				    	if($material[0]->fotografia!="" && $material[0]->fotografia!=NULL){
							$img="materiales/miniatura/".$material[0]->fotografia;
						}
						echo '<div class="card-group" style="text-align: center !important;" data-m="'.$material[0]->idm.'">
								<div class="card img-thumbnail-35">
									<img class="card-img-top" src="'.$url.$img.'" alt="image" data-title="'.$material[0]->nombre.'" data-desc="" style="vertical-align: top;cursor: zoom-in;">
								</div>
								<div class="card card-padding-0" style="vertical-align: middle !important;">
									<div class="a-card-block">
										<p class="a-card-text" style="line-height: .8 !important;"><small style="color: #069bcc !important;">'.$material[0]->nombre.'</small></p>
									</div>
								</div>
								<div class="card card-padding-0 card-30 card-middle">
									<div class="card-block" style="padding: 9px 0px 5px 12px !important;">
										<div class="g-control-accordion" style="width:30px;"><button class="drop_material_pieza" data-type="pieza_material" data-padre="card-group" data-type-padre="atr"><i class="icon-trashcan2"></i></button></div>
									</div>
								</div>
								<script>$("img.card-img-top").visor();$("button.drop_material_pieza").drop_elemento();</script>
							</div>';
						
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
	/*Manejo de materiales en el producto*/
	/*Manejo de fotografias en el producto*/
		public function producto_fotografias(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['p']) && isset($_POST['type'])){
					$idp=trim($_POST['p']);
					$type=trim($_POST['type']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$listado['producto']=$producto[0];
						$listado['fotografias']=$this->M_producto_imagen->get_row('idp',$idp);
						$listado['type']=$type;
						if($type=="pgcf-modal"){ 
							$listado['pgc']=$_POST['pgc']; 
							$listado['fotografias_color']=$this->M_producto_imagen_color->get_row('idpgrc',$_POST['pgc']);
						}
						$this->load->view('produccion/producto/6-config/fotografias/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function portada_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pi']) && isset($_POST['p'])){
					$idpi=trim($_POST['pi']);
					$idp=trim($_POST['p']);
					if($this->M_producto_imagen->reset_portada('idp',$idp)){
						if($this->M_producto_imagen->modificar_row($idpi,'portada','1')){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function new_fotografias(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['type']) && isset($_POST['p'])){
					$listado="";
					if($_POST['type']=="pf" || $_POST['type']=="pf-modal"){
						$listado['categorias_colores']=$this->M_producto_grupo->get_grupo_colores('pg.idp',$_POST['p']);
					}
					$this->load->view('produccion/producto/6-config/fotografias/new',$listado);
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function save_fotografias(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
				if(!empty($privilegio)){
					if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
						if(isset($_POST['p']) && isset($_FILES) && isset($_POST['des']) && isset($_POST['type'])){
							$idp=trim($_POST['p']);
							$des=trim($_POST['des']);
							$type=trim($_POST['type']);
							$producto=$this->M_producto->get($idp);
							if(!empty($producto)){
								if(count($_FILES)>0 && count($_FILES)<=10 && $this->val->textarea($des,0,150)){
									$valida=$this->lib->valida_imagenes($_FILES);
									if($valida=="ok"){
										if($type=="pf" || $type=="pf-modal"){
											$save_producto=true;
											if(isset($_POST['cc'])){//imagen en el color del producto
												if($_POST['cc']!=""){
													$idpgrc=$_POST['cc'];
													$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
													if(!empty($producto_grupo_color)){
														for($i=0; $i < count($_FILES); $i++){
															$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/productos/',$i.'',$this->resize,$idp."-".rand(0,9999));
															if($img!="error_type" && $img!="error" && $img!="error_size_img"){
																if($this->M_producto_imagen_color->insertar($idpgrc,$des,$img)){
																	//echo "ok";
																}else{
																	if($this->lib->eliminar_imagen($img,'./libraries/img/productos/')){ }
																}
															}
														}
														//notificaciones
														$color=$this->M_color->get($producto_grupo_color[0]->idco);
														$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
														$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
														$plural="";
														if(count($_FILES)>1){$plural="s";}
														$msj="adicionó ".count($_FILES)." fotografía".$plural;
														$cod=$producto[0]->codigo;
														$prod=$producto[0]->nombre;
														if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
														$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
														$prod=$cod." ".$prod;
														if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"cgcf",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
														$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
														foreach ($resultados as $key => $result) {
															if($result->idno=="none"){
																if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
															}else{
																if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
															}
														}
														$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
														echo json_encode($result);
													}else{
														echo "fail";
													}
												}else{
																									for($i=0; $i < count($_FILES); $i++){
													$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/productos/',$i.'',$this->resize,$idp."-".rand(0,9999));
													if($img!="error_type" && $img!="error" && $img!="error_size_img"){
														if($this->M_producto_imagen->insertar($idp,$des,$img)){
															//echo "ok";
														}else{
															if($this->lib->eliminar_imagen($img,'./libraries/img/productos/')){ }
														}
													}
												}
												$plural="";
												if(count($_FILES)>1){$plural="s";}
												$msj="adicionó ".count($_FILES)." fotografía".$plural." en el producto";
												$prod=$producto[0]->codigo." ".$producto[0]->nombre;
												if($this->M_producto_seg->insertar($idp,$prod,"cf",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
												foreach ($resultados as $key => $result) {
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
													}
												}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
												}

											}else{//imagen en el producto
												for($i=0; $i < count($_FILES); $i++){
													$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/productos/',$i.'',$this->resize,$idp."-".rand(0,9999));
													if($img!="error_type" && $img!="error" && $img!="error_size_img"){
														if($this->M_producto_imagen->insertar($idp,$des,$img)){
															//echo "ok";
														}else{
															if($this->lib->eliminar_imagen($img,'./libraries/img/productos/')){ }
														}
													}
												}
												$plural="";
												if(count($_FILES)>1){$plural="s";}
												$msj="adicionó ".count($_FILES)." fotografía".$plural." en el producto";
												$prod=$producto[0]->codigo." ".$producto[0]->nombre;
												if($this->M_producto_seg->insertar($idp,$prod,"cf",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
												foreach ($resultados as $key => $result) {
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
													}
												}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}
										}
										if($type=="pgcf" || $type=="pgcf-modal"){
											if(isset($_POST['pgc'])){
												$idpgrc=$_POST['pgc'];
												$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
												if(!empty($producto_grupo_color)){
													for($i=0; $i < count($_FILES); $i++){
														$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/productos/',$i.'',$this->resize,$idp."-".rand(0,9999));
														if($img!="error_type" && $img!="error" && $img!="error_size_img"){
															if($this->M_producto_imagen_color->insertar($idpgrc,$des,$img)){
																//echo "ok";
															}else{
																if($this->lib->eliminar_imagen($img,'./libraries/img/productos/')){ }
															}
														}
													}
													//notificaciones
													$color=$this->M_color->get($producto_grupo_color[0]->idco);
													$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
													$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
													$cod=$producto[0]->codigo;
													$prod=$producto[0]->nombre;
													if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
													$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
													$prod=$cod." ".$prod;
													$plural="";
													if(count($_FILES)>1){$plural="s";}
													$msj="adicionó ".count($_FILES)." fotografía".$plural;
													if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"cgcf",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
													foreach ($resultados as $key => $result) {
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
														}
													}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
												}else{
													echo "fail";
												}
											}else{
												echo "fail";
											}
										}
									}else{
										echo $valida;
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function rotate_fotografia(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['type']) && isset($_POST['rotate'])){
					if($_POST['type']=="pf-modal"){
						if(isset($_POST['pi'])){
							$rotate=trim($_POST['rotate'])*1;
							$idpi=trim($_POST['pi']);
							$producto_imagen=$this->M_producto_imagen->get($idpi);
							if(!empty($producto_imagen)){
								if($producto_imagen[0]->archivo!="" && $producto_imagen[0]->archivo!=NULL){
									$url="./libraries/img/productos/";
									$img=$producto_imagen[0]->archivo;
									$img=$this->lib->rotar_imagen($url,$img,$rotate);
									if($img!=NULL){
										if($this->M_producto_imagen->modificar_row($idpi,'archivo',$img)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									echo "ok";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}
					if($_POST['type']=="pgcf-modal"){
						if(isset($_POST['pig'])){
							$rotate=trim($_POST['rotate'])*1;
							$idpig=trim($_POST['pig']);
							$producto_imagen_color=$this->M_producto_imagen_color->get($idpig);
							if(!empty($producto_imagen_color)){
								if($producto_imagen_color[0]->archivo!="" && $producto_imagen_color[0]->archivo!=NULL){
									$url="./libraries/img/productos/";
									$img=$producto_imagen_color[0]->archivo;
									$img=$this->lib->rotar_imagen($url,$img,$rotate);
									if($img!=NULL){
										if($this->M_producto_imagen_color->modificar_row($idpig,'archivo',$img)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									echo "ok";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function config_fotografia(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['type'])){
					$type=$_POST['type'];
					if($type=="pf-modal"){
						if(isset($_POST['pi'])){
							$idpi=$_POST['pi'];
							$fotografia=$this->M_producto_imagen->get($idpi);
							if(!empty($fotografia)){
								$listado['fotografia']=$fotografia[0];
								$this->load->view('produccion/producto/6-config/fotografias/update',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}
					if($type=="pgcf-modal"){
						if(isset($_POST['pig'])){
							$idpig=$_POST['pig'];
							$fotografia=$this->M_producto_imagen_color->get($idpig);
							if(!empty($fotografia)){
								$listado['fotografia']=$fotografia[0];
								$this->load->view('produccion/producto/6-config/fotografias/update',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}
				}else{
					echo "fail";
				}

			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function update_fotografia(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
				if(!empty($privilegio)){
					if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
						if(isset($_POST['p']) && isset($_POST['des']) && isset($_POST['type'])){
							$type=trim($_POST['type']);
							$idp=trim($_POST['p']);
							$des=trim($_POST['des']);
							if($type=="pf-modal"){
								if(isset($_POST['pi'])){
									$idpi=trim($_POST['pi']);
									$producto=$this->M_producto->get($idp);
									$producto_imagen=$this->M_producto_imagen->get($idpi);
									if(!empty($producto) && !empty($producto_imagen)){
										if($this->val->textarea($des,0,150)){
											$img=$producto_imagen[0]->archivo;
											$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/productos/','',$this->resize,$img,$idp."-".rand(0,9999));
											if($img!='error' && $img!="error_type" && $img!="error_size_img"){
												if($this->M_producto_imagen->modificar($idpi,$des,$img)){
													//notificacion
													$msj="Modificó una fotografía en el producto.";
													$prod=$producto[0]->codigo." ".$producto[0]->nombre;
													if($this->M_producto_seg->insertar($idp,$prod,"uf",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
													foreach($resultados as $key => $result){
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
														}
													}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
												}
											}else{
												echo $img;
											}
										}else{
											echo "fail";
										}
									}else{
										echo "fail";
									}
								}else{
									echo "fail";
								}
							}
							if($type=="pgcf-modal"){
								if(isset($_POST['pig'])){
									$idpig=trim($_POST['pig']);
									$producto_imagen_color=$this->M_producto_imagen_color->get($idpig);
									if(!empty($producto_imagen_color)){
										if($this->val->textarea($des,0,150)){
											$img=$producto_imagen_color[0]->archivo;
											$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/productos/','',$this->resize,$img,$idp."-".rand(0,9999));
											if($img!='error' && $img!="error_type" && $img!="error_size_img"){
												if($this->M_producto_imagen_color->modificar($idpig,$des,$img)){
													//notificacion
													$producto_grupo_color=$this->M_producto_grupo_color->get($producto_imagen_color[0]->idpgrc);
													$color=$this->M_color->get($producto_grupo_color[0]->idco);
													$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
													$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
													$producto=$this->M_producto->get($producto_grupo[0]->idp);
													$cod=$producto[0]->codigo;
													$prod=$producto[0]->nombre;
													if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
													$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
													$prod=$cod." ".$prod;
													$msj="Modificó una fotografia en el color del producto.";
													if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ugcf",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
													foreach($resultados as $key => $result){
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
														}
													}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
												}else{
													echo "error";
												}
											}else{
												echo $img;
											}
										}else{
											echo "fail";
										}
									}else{
										echo "fail";
									}
								}else{
									echo "fail";
								}
							}
						}else{
							echo "fail";
						}
					}
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		   	public function confirmar_fotografia(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
					if(!empty($privilegio)){
						if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['type'])){
								$type=$_POST["type"];
								if($type=="pf" || $type=="pf-modal"){
									if(isset($_POST['pi'])){
										$idpi=$_POST['pi'];
										$producto_imagen=$this->M_producto_imagen->get($idpi);
										if(!empty($producto_imagen)){
											$listado['open_control']="true";
											$listado['control']="";
											$listado['img']=base_url().'libraries/img/productos/miniatura/'.$producto_imagen[0]->archivo;
											$listado['titulo']="eliminar la imagen";
											$desc="Se eliminara definitivamente la imagen del producto y de sus categorias.";
											$listado['desc']=$desc;
											$this->load->view('estructura/form_eliminar',$listado);
										}else{
											echo "fail";
										}
									}else{
										echo "fail";
									}
								}
								if($type=="pgcf" || $type=="pgcf-modal"){
									if(isset($_POST['pgcf'])){
										$idpig=$_POST['pgcf'];
										$producto_imagen=$this->M_producto_imagen_color->get($idpig);
										if(!empty($producto_imagen)){
											$listado['open_control']="true";
											$listado['control']="";
											$listado['img']=base_url().'libraries/img/productos/miniatura/'.$producto_imagen[0]->archivo;
											$listado['titulo']="eliminar la imagen";
											$desc="Se eliminara definitivamente la imagen del color de categoria en el producto.";
											$listado['desc']=$desc;
											$this->load->view('estructura/form_eliminar',$listado);
										}else{
											echo "fail";
										}
									}else{
										echo "fail";
									}
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function drop_fotografia(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['type'])){
						$type=$_POST['type'];
						$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
						if(!empty($privilegio)){
							if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
								if($type=="pf" || $type=="pf-modal"){
									if(isset($_POST['pi'])){
										$idpi=$_POST['pi'];
										$producto_imagen=$this->M_producto_imagen->get($idpi);
										if(!empty($producto_imagen)){
											$producto=$this->M_producto->get($producto_imagen[0]->idp);
											if($this->lib->eliminar_imagen($producto_imagen[0]->archivo,'./libraries/img/productos/')){
												if($this->M_producto_imagen->eliminar($idpi)){
													//notificacion
													$msj="Eliminó una fotografia en el producto";
													$prod=$producto[0]->codigo." ".$producto[0]->nombre;
													if($this->M_producto_seg->insertar($producto[0]->idp,$prod,"df",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
													foreach($resultados as $key => $result){
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
														}
													}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo "ok";
										}
									}else{
										echo "fail";
									}
								}
								if($type=="pgcf" || $type=="pgcf-modal"){
									if(isset($_POST['pgcf'])){
										$idpig=$_POST['pgcf'];
										$producto_imagen_color=$this->M_producto_imagen_color->get($idpig);
										if(!empty($producto_imagen_color)){
											if($this->lib->eliminar_imagen($producto_imagen_color[0]->archivo,'./libraries/img/productos/')){
												if($this->M_producto_imagen_color->eliminar($idpig)){
													//notificacion
													$producto_grupo_color=$this->M_producto_grupo_color->get($producto_imagen_color[0]->idpgrc);
													$color=$this->M_color->get($producto_grupo_color[0]->idco);
													$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
													$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
													$producto=$this->M_producto->get($producto_grupo[0]->idp);
													$cod=$producto[0]->codigo;
													$prod=$producto[0]->nombre;
													if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
													$cod.='-'.$color[0]->abr;$prod.="-".$color[0]->nombre;
													$prod=$cod." ".$prod;
													if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"dgcf","",$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
													foreach($resultados as $key => $result){
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
														}
													}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo "ok";
										}
									}else{
										echo "fail";
									}
								}	
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}			
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
			public function copiar_imagen_color(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pi']) && isset($_POST['pgc'])){
						$idpi=$_POST['pi'];
						$idpgrc=$_POST['pgc'];
						$producto_imagen=$this->M_producto_imagen->get($idpi);
						$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
						if(!empty($producto_imagen) && !empty($producto_grupo_color)){
							$imagen=$producto_imagen[0]->archivo;
							$url="./libraries/img/productos/";
							$img=$this->lib->copiar_imagen($url,$url,$imagen,$idpgrc);
							if($img!=""){
								if($this->M_producto_imagen_color->insertar($idpgrc,$producto_imagen[0]->descripcion,$img)){
									echo "ok";
								}else{
									if($this->eliminar_imagen($img,$url)){}
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
				}
			}
	/*End manejo de fotografias en el producto*/
	/*--- Historiales de cambios ---*/
	public function producto_seg(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type'])){
				$type=$_POST['type'];
				$listado=null;
				if($type!="producto_complet"){
					if($type=="pm-modal" || $type=="producto" || $type=="pf-modal" || $type=="ppi-modal"){
						$control=false;
						switch($type){
							case 'pm-modal': $type="producto_material"; $control=true; break;
							case 'pf-modal': $type="producto_fotografia"; $control=true; break;
							case 'producto': $control=true; break;
							case 'ppi-modal': $type="producto_pieza"; $control=true; break;
							case 'pgm-modal': $type="producto_grupo_material"; $control=true; break;
						}
						if($control){
							$listado['historial']=$this->M_producto_seg->get_producto($_POST['p'],$type);
						}
					}
				}else{
					$listado['historial_producto']=$this->M_producto_seg->get_producto($_POST['p'],$type);
					$listado['historial_categoria']=$this->M_producto_grupo_color_seg->get_producto($_POST['p'],$_POST["type"],NULL,NULL);
				}
				$this->load->view('produccion/producto/historial/producto',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function producto_grupo_color_seg(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type'])){
				$idpgr=NULL;
				$idpgrc=NULL;
				if($_POST['type']=="producto_grupo" || $_POST['type']=="pgm-modal"){ if(isset($_POST['pg'])){ $idpgr=$_POST['pg']; }}
				if($_POST['type']=="producto_grupo_color" || $_POST['type']=="producto_grupo_complet"){if(isset($_POST['pg'])){ $idpgr=$_POST['pg']; } if(isset($_POST['pgc'])){ $idpgrc=$_POST['pgc']; }}
				if($_POST['type']=="pgcm-modal" || $_POST['type']=="pgcf-modal" || $_POST['type']=="pgcpi-modal"){ if(isset($_POST['pgc'])){ $idpgrc=$_POST['pgc']; }}
				$listado['historial']=$this->M_producto_grupo_color_seg->get_producto($_POST['p'],$_POST["type"],$idpgr,$idpgrc);
				$this->load->view('produccion/producto/historial/producto_grupo_color',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- End historiales de cambios ---*/
	/*--- Manejo de piezas---*/
	public function producto_piezas(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type'])){
				$producto=$this->M_producto->get($_POST['p']);
				if(!empty($producto)){
					$listado['type']=$_POST['type'];
					$listado['producto_piezas']=$this->M_producto_pieza->get_row('idp',$_POST['p']);
					if($_POST['type']=="pgcpi-modal"){
						if(isset($_POST['pgc'])){
							$listado['color_piezas']=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$_POST['pgc']);
							$listado['pgc']=$_POST['pgc'];
						}else{
							echo "fail";
						}
					}
					$listado['producto']=$producto[0];
					$this->load->view('produccion/producto/6-config/piezas/producto_piezas/view',$listado);
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function new_pieza(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type'])){
				$listado['type']=$_POST['type'];
				$listado['repujados_sellos']=$this->M_repujado_sello->get_all();
				$this->load->view('produccion/producto/6-config/piezas/new',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_pieza(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type']) && isset($_POST['nom']) && isset($_POST['alt']) && isset($_POST['anc']) && isset($_POST['can']) && isset($_POST['mat']) && isset($_POST['rs']) && isset($_POST['des'])){
				$idp=trim($_POST['p']);
				$type=trim($_POST['type']);
				$nom=trim($_POST['nom']);
				$alt=trim($_POST['alt']);
				$anc=trim($_POST['anc']);
				$can=trim($_POST['can']);
				$idm=trim($_POST['mat']);
				$rs=trim($_POST['rs']);
				$des=trim($_POST['des']);
				$material=$this->M_material->get($idm);
				if($this->val->strSpace($nom,2,100) && $this->val->decimal($alt,4,3) && $alt>=0 && $alt<=9999.999 && $this->val->decimal($anc,4,3) && $anc>=0 && $anc<=9999.999 && $this->val->strSpace($nom,2,100) && $this->val->entero($can,0,99) && !empty($material) && $this->val->textarea($des,0,200)){
					if($type=="ppi" || $type=="ppi-modal"){
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/piezas/','',$this->resize,NULL);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_producto_pieza->insertar($idp,$rs,$idm,$nom,$alt,$anc,$img,$can,$des)){
								//notificacion
								$producto=$this->M_producto->get($idp);
								$msj="Creó la pieza <strong>".$nom."</strong> en el producto.";
								$prod=$producto[0]->codigo." ".$producto[0]->nombre;
								if($this->M_producto_seg->insertar($idp."",$prod,"cp",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
								foreach($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								$this->lib->eliminar_imagen($img,'./libraries/img/repujados_sellos/');
								echo "error";
							}
						}else{
							echo $img;
						}
					}
					if($type=="pgcpi" || $type=="pgcpi-modal"){
						if(isset($_POST['pgc'])){
							$idpgrc=$_POST['pgc'];
							$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
							if(!empty($producto_grupo_color)){
								$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/piezas/','',$this->resize,$idpgrc."-".rand(0,9999));
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_producto_grupo_color_pieza->insertar($idpgrc,$rs,$idm,$nom,$alt,$anc,$img,$can,$des)){								
										//para notificaciones
										$color=$this->M_color->get($producto_grupo_color[0]->idco);
										$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
										$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
										$producto=$this->M_producto->get($producto_grupo[0]->idp);
										$cod=$producto[0]->codigo;
										$prod=$producto[0]->nombre;
										if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
										$cod.="-".$color[0]->abr;
										$prod.="-".$color[0]->nombre;
										$msj="Creó la pieza <strong>".$nom."</strong> en el color";
										if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$cod." ".$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"cgcp",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										$this->lib->eliminar_imagen($img,'./libraries/img/repujados_sellos/');
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function configurar_pieza(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type'])){
				$type=$_POST['type'];
				$listado['type']=$type;
				if($type=="ppi-modal"){
					if(isset($_POST['ppi'])){
						$listado['repujados_sellos']=$this->M_repujado_sello->get_all();
						$producto_pieza=$this->M_producto_pieza->get($_POST['ppi']);
						$listado['producto_pieza']=$producto_pieza[0];
						$this->load->view('produccion/producto/6-config/piezas/update',$listado);
					}else{
						echo "fail";
					}
				}
				if($type=="pgcpi-modal"){
					if(isset($_POST['pgcpi'])){
						$listado['repujados_sellos']=$this->M_repujado_sello->get_all();
						$producto_pieza=$this->M_producto_grupo_color_pieza->get($_POST['pgcpi']);
						$listado['producto_pieza']=$producto_pieza[0];
						$this->load->view('produccion/producto/6-config/piezas/update',$listado);
					}else{
						echo "fail";
					}
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_pieza(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type']) && isset($_POST['nom']) && isset($_POST['alt']) && isset($_POST['anc']) && isset($_POST['can']) && isset($_POST['rs']) && isset($_POST['mat']) && isset($_POST['des'])){
				$idp=$_POST['p'];
				$type=$_POST['type'];
				$nom=trim($_POST['nom']);
				$alt=trim($_POST['alt']);
				$anc=trim($_POST['anc']);
				$can=trim($_POST['can']);
				$idm=trim($_POST['mat']);
				$rs=trim($_POST['rs']);
				$des=trim($_POST['des']);
				$material=$this->M_material->get($idm);
				if($this->val->strSpace($nom,2,100) && $this->val->decimal($alt,4,3) && $alt>=0 && $alt<=9999.999 && $this->val->decimal($anc,4,3) && $anc>=0 && $anc<=9999.999 && $this->val->strSpace($nom,2,100) && $this->val->entero($can,0,99) && $this->val->textarea($des,0,200) && !empty($material)){
					if($type=="ppi" || $type=="ppi-modal"){
						if(isset($_POST['ppi'])){
							$idppi=$_POST['ppi'];
							$producto_pieza=$this->M_producto_pieza->get($idppi);
							$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/piezas/','',$this->resize,$producto_pieza[0]->fotografia,$idppi);
							if($img!='error' && $img!="error_type" && $img!="error_size_img"){
								if($this->M_producto_pieza->modificar($idppi,$rs,$idm,$nom,$alt,$anc,$img,$can,$des)){
									//notificacion
									$producto=$this->M_producto->get($idp);
									$msj="";
									if(count($_FILES)>0){ $msj="cambió la fotografía";}
									if($producto_pieza[0]->nombre!=$nom){ if($msj!=""){$msj.=", ";} $msj.="cambió el nombre de <strong>".$producto_pieza[0]->nombre."</strong> a <strong>".$nom."</strong>";}
									if($producto_pieza[0]->largo!=$alt){ if($msj!=""){$msj.=", ";} $msj.="modificó el largo de <strong>".$producto_pieza[0]->largo."[cm.]</strong> a <strong>".$alt."[cm.]</strong>";}
									if($producto_pieza[0]->ancho!=$anc){ if($msj!=""){$msj.=", ";} $msj.="modificó el ancho de <strong>".$producto_pieza[0]->ancho."[cm.]</strong> a <strong>".$anc."[cm.]</strong>";}
									if($producto_pieza[0]->cantidad!=$can){ if($msj!=""){$msj.=", ";} $msj.="modificó la cantidad de <strong>".$producto_pieza[0]->cantidad."[unid.]</strong> a <strong>".$can."[unid.]</strong>";}
									if($producto_pieza[0]->descripcion!=$des){ if($msj!=""){$msj.=", ";} $msj.="modificó la descripcion de <strong>".$producto_pieza[0]->descripcion."</strong> a <strong>".$des."</strong>";}
									if($producto_pieza[0]->idm!=$idm){ if($msj!=""){$msj.=", ";} $msj.="cambió el material"; }
									if($producto_pieza[0]->idrs!=$rs){ if($msj!=""){$msj.=", ";} $msj.="cambió el repujado o sello"; }
									if($msj!=""){
										$prod=$producto[0]->codigo." ".$producto[0]->nombre;
										if($this->M_producto_seg->insertar($idp."",$prod,"up",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
										foreach($resultados as $key => $result){
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
											}
										}
										$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
										echo json_encode($result);
									}else{
										echo "ok";
									}
								}else{
									$this->lib->eliminar_imagen($img,'./libraries/img/repujados_sellos/');
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "fail";
						}
					}
					if($type=="pgcpi-modal"){
						if(isset($_POST['pgcpi'])){
							$idpgcpi=$_POST['pgcpi'];
							$color_pieza=$this->M_producto_grupo_color_pieza->get($idpgcpi);
							if(!empty($color_pieza)){
								$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/piezas/','',$this->resize,$color_pieza[0]->fotografia,$idpgcpi."-".rand(0,9999));
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_producto_grupo_color_pieza->modificar($idpgcpi,$rs,$idm,$nom,$alt,$anc,$img,$can,$des)){
										//notificacion
										$producto=$this->M_producto->get($idp);
										$msj="";
										if(count($_FILES)>0){ $msj="cambió la fotografía";}
										if($color_pieza[0]->nombre!=$nom){ if($msj!=""){$msj.=", ";} $msj.="cambió el nombre de <strong>".$color_pieza[0]->nombre."</strong> a <strong>".$nom."</strong>";}
										if($color_pieza[0]->largo!=$alt){ if($msj!=""){$msj.=", ";} $msj.="modificó el largo de <strong>".$color_pieza[0]->largo."[cm.]</strong> a <strong>".$alt."[cm.]</strong>";}
										if($color_pieza[0]->ancho!=$anc){ if($msj!=""){$msj.=", ";} $msj.="modificó el ancho de <strong>".$color_pieza[0]->ancho."[cm.]</strong> a <strong>".$anc."[cm.]</strong>";}
										if($color_pieza[0]->cantidad!=$can){ if($msj!=""){$msj.=", ";} $msj.="modificó la cantidad de <strong>".$color_pieza[0]->cantidad."[unid.]</strong> a <strong>".$can."[unid.]</strong>";}
										if($color_pieza[0]->descripcion!=$des){ if($msj!=""){$msj.=", ";} $msj.="modificó la descripcion de <strong>".$color_pieza[0]->descripcion."</strong> a <strong>".$des."</strong>";}
										if($color_pieza[0]->idm!=$idm){ if($msj!=""){$msj.=", ";} $msj.="cambió el material"; }
										if($color_pieza[0]->idrs!=$rs){ if($msj!=""){$msj.=", ";} $msj.="cambió el repujado o sello"; }
										if($msj!=""){ $msj.=", de la pieza <strong>".$color_pieza[0]->nombre."</strong>";}
										if($msj!=""){
											$producto_grupo_color=$this->M_producto_grupo_color->get($color_pieza[0]->idpgrc);
											$color=$this->M_color->get($producto_grupo_color[0]->idco);
											$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
											$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
											$producto=$this->M_producto->get($producto_grupo[0]->idp);
											$cod=$producto[0]->codigo;
											$prod=$producto[0]->nombre;
											if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
											$cod.="-".$color[0]->abr;
											$prod.="-".$color[0]->nombre;
											if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$cod." ".$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"ugcp",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
											$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
											foreach($resultados as $key => $result){
												if($result->idno=="none"){
													if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
												}else{
													if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
												}
											}
											$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
											echo json_encode($result);
										}else{
											echo "ok";
										}
									}else{
										$this->lib->eliminar_imagen($img,'./libraries/img/repujados_sellos/');
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "fail";
							}
						}
					}					
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function confirmar_pieza(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST["type"])){
				if($_POST['type']=="ppi" || $_POST['type']=="ppi-modal"){
					if(isset($_POST['ppi'])){
						$idppi=$_POST['ppi'];
						$url='./libraries/img/';
						$pieza=$this->M_producto_pieza->get($idppi);
						if(!empty($pieza)){
							$listado['titulo']="eliminar la pieza <b>".$pieza[0]->nombre."</b>";
							$listado['desc']="Se eliminara definitivamente la pieza del producto, de las categorias y de los colores";
							$listado['control']="";
							$listado['open_control']="false";
							$img='sistema/miniatura/default.jpg';
							if($pieza[0]->fotografia!=NULL && $pieza[0]->fotografia!=""){ $img="piezas/miniatura/".$pieza[0]->fotografia; }
							$listado['img']=$url.$img;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}
				if($_POST['type']=="pgcpi" || $_POST['type']=="pgcpi-modal"){
					if(isset($_POST['pgcpi'])){
						$idpgcpi=$_POST['pgcpi'];
						$url='./libraries/img/';
						$pieza=$this->M_producto_grupo_color_pieza->get($idpgcpi);
						if(!empty($pieza)){
							$listado['titulo']="eliminar la pieza <b>".$pieza[0]->nombre."</b>";
							$listado['desc']="Se eliminara definitivamente la pieza del producto, de las categorias y de los colores";
							$listado['control']="";
							$listado['open_control']="false";
							$img='sistema/miniatura/default.jpg';
							if($pieza[0]->fotografia!=NULL && $pieza[0]->fotografia!=""){ $img="piezas/miniatura/".$pieza[0]->fotografia; }
							$listado['img']=$url.$img;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}				
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_pieza(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
				if(!empty($privilegio)){
					if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
						if(isset($_POST['type']) && isset($_POST['p'])){
							$idp=$_POST['p'];
							if($_POST['type']=="ppi" || $_POST['type']=="ppi-modal"){
								if(isset($_POST['ppi'])){
									$idppi=$_POST['ppi'];
									$pieza=$this->M_producto_pieza->get($idppi);
									if(!empty($pieza)){
										if($this->lib->eliminar_imagen($pieza[0]->fotografia,'./libraries/img/piezas/')){
											if($this->M_producto_pieza->eliminar($idppi)){
												//notificacion
												$producto=$this->M_producto->get($idp);
												$msj="Eliminó la pieza <strong>".$pieza[0]->nombre."</strong> en el producto.";
												$prod=$producto[0]->codigo." ".$producto[0]->nombre;
												if($this->M_producto_seg->insertar($idp."",$prod,"dp",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto");
												foreach($resultados as $key => $result){
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_producto($result->emisor,$result->reseptor,$result->cantidad)){}
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"producto",$result->cantidad)){}
													}
												}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}else{
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									echo "fail";
								}
							}
							if($_POST['type']=="pgcpi" || $_POST['type']=="pgcpi-modal"){
								if(isset($_POST['pgcpi'])){
									$idpgcpi=$_POST['pgcpi'];
									$pieza=$this->M_producto_grupo_color_pieza->get($idpgcpi);
									if(!empty($pieza)){
										if($this->lib->eliminar_imagen($pieza[0]->fotografia,'./libraries/img/piezas/')){
											if($this->M_producto_grupo_color_pieza->eliminar($idpgcpi)){
												//notificacion
												$producto_grupo_color=$this->M_producto_grupo_color->get($pieza[0]->idpgrc);
												$color=$this->M_color->get($producto_grupo_color[0]->idco);
												$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
												$grupo=$this->M_grupo->get($producto_grupo[0]->idgr);
												$producto=$this->M_producto->get($producto_grupo[0]->idp);
												$cod=$producto[0]->codigo;
												$prod=$producto[0]->nombre;
												if($grupo[0]->abr!=""){ $cod.='-'.$grupo[0]->abr;$prod.="-".$grupo[0]->nombre;}
												$cod.="-".$color[0]->abr;
												$prod.="-".$color[0]->nombre;
												$msj="Eliminó la pieza <strong>".$pieza[0]->nombre	."</strong>.";
												if($this->M_producto_grupo_color_seg->insertar($producto[0]->idp,$cod." ".$prod,$producto_grupo[0]->idpgr,$grupo[0]->idgr,$grupo[0]->nombre,$producto_grupo_color[0]->idpgrc,$color[0]->idco,$color[0]->nombre,"dgcp",$msj,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'))){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"producto_categoria");
												foreach($resultados as $key => $result){
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_producto_categoria($result->emisor,$result->reseptor,$result->cantidad)){}
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"producto_categoria",$result->cantidad)){}
													}
												}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}else{
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{
										echo "ok";
									}
								}else{
									echo "fail";
								}
							}
						}else{
							echo "fail";
						}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- End manejo de piezas ---*/
	/*--- Manejo de repujado y sello---*/
	public function new_repujado_sello(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view('produccion/producto/6-config/repujados_sellos/new');
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_repujado_sello(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['nom']) && isset($_POST['tip'])){
				$nom=trim($_POST['nom']);
				$tip=$_POST['tip'];
				if($this->val->strSpace($nom,1,150) && ($tip=="0" || $tip=="1")){
					$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/repujados_sellos/','',$this->resize,NULL);
					if($img!='error' && $img!="error_type" && $img!="error_size_img"){
						if($this->M_repujado_sello->insertar($img,$nom,$tip)){
							echo "ok";
						}else{
							$this->lib->eliminar_imagen($img,'./libraries/img/repujados_sellos/');
							echo "error";
						}
					}else{
						echo $img;
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function opciones_repujados_sellos(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$repujados_sellos=$this->M_repujado_sello->get_all();
			$resp="<option value='0'>Ninguno</option>";
			for($i=0; $i < count($repujados_sellos); $i++){ 
				$idrs=$repujados_sellos[$i]->idrs;
				$nombre=$repujados_sellos[$i]->nombre;
				$tipo="";
				if($repujados_sellos[$i]->tipo==0){
					$tipo="[Repujado]";
				}else{
					$tipo="[Sello]";
				}
				$resp.="<option value='".$idrs."'>".$tipo." ".$nombre."</option>";
			}
			echo $resp;
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_repujados_sellos(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type']) && isset($_POST['destino'])){
				$listado['type']=$_POST['type'];
				$listado['destino']=$_POST['destino'];
				$listado['repujados_sellos']=$this->M_repujado_sello->get_all();
				$this->load->view('produccion/producto/6-config/repujados_sellos/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function config_repujado_sello(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['rs'])){
				$repujados_sellos=$this->M_repujado_sello->get($_POST['rs']);
				if(!empty($repujados_sellos)){
					if(isset($_POST['destino'])){ $listado['destino']=trim($_POST['destino']);}
					if(isset($_POST['type'])){ $listado['type']=trim($_POST['type']);}				
					$listado['repujado_sello']=$repujados_sellos[0];
					$this->load->view('produccion/producto/6-config/repujados_sellos/update',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_repujado_sello(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['rs']) && isset($_POST['nom']) && isset($_POST['tip'])){
				$idrs=$_POST['rs'];
				$nom=trim($_POST['nom']);
				$tip=$_POST['tip'];
				$repujados_sellos=$this->M_repujado_sello->get($idrs);
				if(!empty($repujados_sellos) && $this->val->strSpace($nom,1,150) && ($tip=="0" || $tip=="1")){
					$img=$repujados_sellos[0]->imagen;
					$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/repujados_sellos/','',$this->resize,$img,$idrs);
					if($img!='error' && $img!="error_type" && $img!="error_size_img"){
						if($this->M_repujado_sello->modificar($idrs,$img,$nom,$tip)){
							echo "ok";
						}else{
							$this->lib->eliminar_imagen($img,'./libraries/img/repujados_sellos/');
							echo "error";
						}
					}else{
						echo $img;
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function confirmar_repujado_sello(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['rs'])){
				$idrs=$_POST['rs'];
				$url='./libraries/img/';
				$repujado_sello=$this->M_repujado_sello->get($idrs);
				if(!empty($repujado_sello)){
					$tipo="";
					if($repujado_sello[0]->tipo==0){
						$tipo="Repujado";
					}else{
						$tipo="Sello";
					}
					$listado['titulo']="eliminar el <b>".$tipo." ".$repujado_sello[0]->nombre."</b>";
					$control1=$this->M_producto_pieza->get_row('idrs',$idrs);
					//$control2=$this->M_producto_grupo_pieza->get_row('idrs',$idrs);
					$control3=$this->M_producto_grupo_color_pieza->get_row('idrs',$idrs);
					if(!empty($control1) || !empty($control2) || !empty($control3)){
						$listado['desc']="El ".$tipo." esta <strong>siendo usado en algunos productos</strong>, si elimina este $tipo se eliminaran tambien de las piezas donde se este siando usando.";					
					}else{
						$listado['desc']="Se eliminara definitivamente el ".$tipo;
					}
					$listado['control']="";
					$listado['open_control']="false";
					$img='sistema/miniatura/default.jpg';
					if($repujado_sello[0]->imagen!=NULL && $repujado_sello[0]->imagen!=""){ $img="repujados_sellos/miniatura/".$repujado_sello[0]->imagen; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_repujado_sello(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
				if(!empty($privilegio)){
					if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
					if(isset($_POST['rs'])){
						$idrs=$_POST['rs'];
						$repujado_sello=$this->M_repujado_sello->get($idrs);
						if(!empty($repujado_sello)){
							if($this->lib->eliminar_imagen($repujado_sello[0]->imagen,'./libraries/img/repujados_sellos/')){
								if($this->M_repujado_sello->eliminar($idrs)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "ok";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- End manejo de repujado y sello ---*/
	public function portada(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
			if(!empty($privilegio)){
				if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['p']) || isset($_POST['status'])){
								$idp=$_POST['p'];
								if($this->M_producto->modificar_row($idp,'portada',$_POST['status'])){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function portada_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_producion($this->session->userdata("id"),"producto");
			if(!empty($privilegio)){
				if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1 && $privilegio[0]->pr1u==1){
							if(isset($_POST['p']) && isset($_POST['status']) && isset($_POST['pgc'])){
								$idp=$_POST['p'];
								if($this->M_producto_grupo_color->modificar_row($_POST['pgc'],'portada',$_POST['status'])){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- Manejo de procesos producto---*/
	public function pila_producto_proceso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type-save'])){
				$idp=trim($_POST['p']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					$listado['procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","ASC");
					$listado['producto_grupo_procesos']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_procesos']=$this->M_producto_grupo_color_proceso->get_all();
					if(isset($_POST['pgc'])){ $listado['pgc']=$_POST['pgc'];}
					if(isset($_POST['pg'])){ $listado['pg']=$_POST['pg'];}
					$listado['type_save']=$_POST['type-save'];
					$this->load->view('produccion/producto/6-config/procesos/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function search_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type-save'])){
				$listado['type_save']=$_POST['type-save'];
				if(isset($_POST['pgc'])){ $listado['pgc']=$_POST['pgc'];}
				if(isset($_POST['pg'])){ $listado['pg']=$_POST['pg'];}
				if(isset($_POST['container'])){ $listado['container']=$_POST['container'];}
				$this->load->view("produccion/producto/6-config/procesos/new/search",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type-save'])){
				$control=true;
				if($_POST['type-save']!="pre"){
					if(isset($_POST['p'])){
						$idp=$_POST['p'];
						$producto=$this->M_producto->get($idp);
						if(isset($producto)){
							$listado['producto_proceso']=$this->M_producto_proceso->get_row("idp",$idp);
						}else{
							$control=false;
						}
					}else{
						$control=false;
					}
				}else{
					if(isset($_POST['asignados'])){
						$listado['asignados']=json_decode($_POST['asignados']);
					}else{
						$control=false;
					}
				}
				if($control){
					$col="";$val="";
					if(isset($_POST['nom'])){ $col="nombre";$val=$_POST['nom'];}
					$listado['procesos']=$this->M_proceso->get_search($col,$val);
					$listado['type_save']=$_POST['type-save'];
					$this->load->view("produccion/producto/6-config/procesos/new/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function new_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado=[];
			if(isset($_POST['type'])){ $listado['type']=$_POST['type'];}
			if(isset($_POST['type-save'])){ $listado['type_save']=$_POST['type-save'];}
			$this->load->view("produccion/producto/6-config/procesos/new/new",$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['nom'])){
				$nom=trim($_POST['nom']);
				if($this->val->strSpace($nom,3,100)){
					if($this->M_proceso->insertar($nom)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pr']) && isset($_POST['nom'])){
				$idpr=trim($_POST['pr']);
				$nom=trim($_POST['nom']);
				
				if($this->val->strSpace($nom,3,100)){
					if($this->M_proceso->modificar($idpr,$nom)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pr'])){
				$idpr=trim($_POST['pr']);
				$proceso=$this->M_proceso->get($idpr);
				if(!empty($proceso)){
					$control=$this->M_proceso_empleado->get_row("idpr",$idpr);
					$control2=$this->M_producto_proceso->get_row("idpr",$idpr);
					if(empty($control) && empty($control2)){
						$listado['open_control']="true";
						$listado['control']="";
						$listado['titulo']="eliminar el proceso <strong>".$proceso[0]->nombre."</strong> del sistema";
						$listado['desc']="Se eliminara definitivamente el proceso del empleado.";
					}else{
						$listado['open_control']="false";
						$listado['control']="";
						$listado['btn_ok']="none";
						$desc="Imposible eliminar el proceso <strong>".$proceso[0]->nombre."</strong>";
						$aux="";
						if(!empty($control)){
							$aux=", se encuentra asignado en ".count($control)." empleado";
							if(count($control)>1){ $aux.="s";}
						}
						if(!empty($control2)){
							if($aux!=""){ 
								$aux.=" y en";
							}else{
								$aux=", se encuentra asignado en";
							}
							$aux.=" ".count($control2)." producto";
							if(count($control2)>0){ $aux.="s";}
						}
						$listado['desc']=$desc.$aux;
					}
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_proceso(){// en uso en produccion,capital humano
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pr'])){
				$idpr=$_POST['pr'];
				$proceso=$this->M_proceso->get($idpr);
				if(!empty($proceso)){
					$control=$this->M_proceso_empleado->get_row("idpr",$idpr);
					$control2=$this->M_producto_proceso->get_row("idpr",$idpr);
					if(empty($control) && empty($control2)){
						if($this->M_proceso->eliminar($idpr)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_proceso_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type-save']) && isset($_POST['pr']) ){
				$type_save=$_POST['type-save'];
				$idpr=trim($_POST['pr']);
				$proceso=$this->M_proceso->get($idpr);
				if($type_save=="ppr" || $type_save=="ppr-modal"){
					if(isset($_POST['p']) && isset($proceso)){
						$idp=$_POST['p'];
						$producto=$this->M_producto->get($idp);
						if(!empty($producto)){
							$idppr=$this->M_producto_proceso->max("idppr")+1;
							if($this->M_producto_proceso->insertar($idppr,$idpr,$idp)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}
				}
				if($type_save=="pgpr" || $type_save=="pgpr-modal"){
					if(isset($_POST['p']) && isset($_POST['pg']) && isset($proceso)){
						$idp=$_POST['p'];
						$idpg=$_POST['pg'];
						$producto=$this->M_producto->get($idp);
						$producto_grupo=$this->M_producto_grupo->get($idpg);
						if(!empty($producto) && !empty($producto_grupo)){
							$idppr=$this->M_producto_proceso->max("idppr")+1;
							if($this->M_producto_proceso->insertar($idppr,$idpr,$idp)){
								if($this->M_producto_grupo_proceso->insertar($idpg,$idppr)){
									echo "ok";
								}else{
									$this->M_producto_proceso->eliminar($idppr);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}
				}
				if($type_save=="pgcpr" || $type_save=="pgcpr-modal"){
					if(isset($_POST['p']) && isset($_POST['pgc']) && isset($proceso)){
						$idp=$_POST['p'];
						$idpgc=$_POST['pgc'];
						$producto=$this->M_producto->get($idp);
						$producto_grupo_color=$this->M_producto_grupo_color->get($idp);
						if(isset($producto) && isset($producto_grupo_color)){
							$idppr=$this->M_producto_proceso->max("idppr")+1;
							if($this->M_producto_proceso->insertar($idppr,$idpr,$idp)){
								if($this->M_producto_grupo_color_proceso->insertar($idpgc,$idppr)){
									echo "ok";
								}else{
									$this->M_producto_proceso->eliminar($idppr);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_producto_proceso(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['ppr'])){
				$idppr=$_POST['ppr'];
				$control=$this->M_producto_proceso->get($idppr);
				if(!empty($control)){
					$proceso=$this->M_proceso->get($control[0]->idpr);
					$listado['titulo']="eliminar el proceso <strong>".$proceso[0]->nombre."</strong> del producto.";
					$listado['desc']="Se eliminara definitivamente el proceso de producto";
					$listado['open_control']="false";
					$listado['control']="";
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
	}
	public function drop_producto_proceso(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['ppr'])){
				$idppr=$_POST['ppr'];
				$control=$this->M_producto_proceso->get($idppr);
				if(!empty($control)){
					if($this->M_producto_proceso->eliminar($idppr)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
	}
	/*--- End Manejo de procesos producto---*/
/*----- END FUNCIONES GENERICAS -----*/

}
/* End of file produccion.php */
/* Location: ./application/controllers/produccion.php */