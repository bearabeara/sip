<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Controls extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){ //validamos usuario
	}
	/*control de alertas*/
	public function view_nav_alerts(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['alerts']=$this->M_notificacion->get_row('reseptor',$this->session->userdata("id"));
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			$listado['privilegio']=$privilegio[0];
			$this->load->view('estructura/controls/alerts/alert_nav',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_nav_produccion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['alerts']=$this->M_pedido->get_row('estado','1');
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			$listado['privilegio']=$privilegio[0];
			$this->load->view('estructura/controls/alerts/alert_nav_produccion',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function nav_refresh_version(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['alerts']=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","0","ac.idac","asc");
			$this->load->view('estructura/controls/alerts/refresh_version',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function iniciar_actualizacion(){
		if(isset($_POST['ac'])){
			$idac=trim($_POST['ac']);
			$actualizacion=$this->M_actualizacion->get($idac);
			if(!empty($actualizacion)){
				date_default_timezone_set("America/La_Paz");
				$fecha_ahora = strtotime(date('Y-m-d H:i:s'));
				$fecha_inicio = strtotime($actualizacion[0]->fecha_inicio);
				if($fecha_inicio<=$fecha_ahora){
					if($actualizacion[0]->estado!=1){
						if($this->M_actualizacion->modificar_row($idac,"estado","1")){
							echo "actualizar";
						}else{
							echo "error";
						}
					}else{
						echo "actualizar";
					}
				}else{
					echo date('Y-m-d H:i:s')."|".$actualizacion[0]->fecha_inicio;
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}
	public function all_updates(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['actualizaciones']=$this->M_actualizacion->get_search("","","idac","desc");
			$this->load->view('estructura/controls/update/all',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function user_alters(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['b'])){
				$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata('id'));
				if(!empty($privilegio)){
					$b=$_POST['b'];
					$control=$this->M_notificacion->get_row_2n('reseptor',$this->session->userdata("id"),'emisor',$b);
					if(!empty($control)){//si estan las notificacaiones activas
						$listado['alert']=$control[0];
					}
					$listado['notificaciones_almacen']=$this->M_almacen_seg->get_row('idu',$b);
					$listado['notificaciones_material']=$this->M_almacen_material_seg->get_row('idu',$b);
					$listado['notificaciones_producto']=$this->M_producto_seg->get_row('idu',$b);
					$listado['notificaciones_producto_categoria']=$this->M_producto_grupo_color_seg->get_row('idu',$b);
					$listado['notificaciones_pedido']=$this->M_pedido_seg->get_row('idu',$b);
					$listado['privilegio']=$privilegio[0];
					$this->load->view('estructura/controls/alerts/user_alters',$listado);
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function refresh_alters(){
		if(isset($_POST['idno'])){
			if($this->M_notificacion->eliminar($_POST['idno'])){}
			$resp=$this->M_notificacion->get_row('reseptor',$this->session->userdata("id"));
			echo count($resp)."";
		}else{
			echo "fail";
		}
	}
	public function all_user_alters(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['usuarios']=$this->M_usuario->get_persona("");
			$this->load->view('estructura/controls/alerts/search_user_alerts',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function user_form_msj(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['a'])){
				$amigo=$this->M_usuario->get_persona($_POST['a']);
				if(!empty($amigo)){
					$listado['amigo']=$amigo[0];
					if($this->M_mensaje->modificar_mensajes_leidos($amigo[0]->idus,$this->session->userdata('id'))){}
					$this->load->view('estructura/controls/chat/chat_user',$listado);
				}else{
					echo "user_no_exist";
				}
			}else{
				echo "fail";
			}	
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function user_msj(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['a'])){
				$amigo=$this->M_usuario->get_persona($_POST['a']);
				if(!empty($amigo)){
					$listado['amigo']=$amigo[0];
					$listado['mensajes']=$this->M_mensaje->get_mensajes($this->session->userdata("id"),$amigo[0]->idus);							
					$this->load->view('estructura/controls/chat/msj_user',$listado);
				}else{
					echo "user_no_exist";
				}
			}else{
				echo "fail";
			}	
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function send_mensaje(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['a']) && isset($_POST['msj'])){
				$amigo=$this->M_usuario->get_persona($_POST['a']);
				if(!empty($amigo) && $this->val->strSpace($_POST['msj'],1,900)){
					$url=base_url()."libraries/img/";
    				$img_destino="sistema/avatar.jpg";
    				if($amigo[0]->fotografia!="" && $amigo[0]->fotografia!=null){ $img_destino="personas/miniatura/".$amigo[0]->fotografia;}
    				$img_origen="sistema/avatar.jpg";
    				if($this->session->userdata('fotografia')!="" && $this->session->userdata('fotografia')!=null){ $img_origen="personas/miniatura/".$this->session->userdata('fotografia');}
    				date_default_timezone_set("America/La_Paz");
					$fecha = date("Y-m-d H:i:s");
					if($this->M_mensaje->insertar($this->session->userdata('id'),$amigo[0]->idus,$fecha,$_POST['msj'],"1")){
						$arrayjson = array();
						$destino=$amigo[0]->idus."personal".$this->session->userdata('id');
						$origen=$this->session->userdata('id')."personal".$amigo[0]->idus;
						$arrayjson[] = array(
							'badge_msj' => "badge".$amigo[0]->idus."msj",//tipo de actualizacion
							'destino' => $destino,//tipo de actualizacion
							'img_destino' => $url.$img_destino,//tipo de actualizacion
							'origen' => $origen,//tipo de actualizacion
							'img_origen' => $url.$img_origen,//tipo de actualizacion
							'mensaje' => $_POST['msj'],
							'fecha' => $fecha,//fecha de envio
							'tipo' => '1'
						);
						//tipo=1(Mensajes), tipo=2(Notificaciones)
						echo json_encode($arrayjson);
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}	
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function refresh_mensajes_user(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$usuarios=$this->M_usuario->get_complet_all();
			if(!empty($usuarios)){
				$vus = array();
				for($i=0; $i < count($usuarios) ; $i++) { $usuario=$usuarios[$i];
					if($usuario->idus!=$this->session->userdata("id")){
						$alerts=$this->M_mensaje->get_row_2n('reseptor',$this->session->userdata("id"),"emisor",$usuario->idus);
						$msj_ingreso="";
						if($usuario->fecha_ingreso!="" && $usuario->fecha_ingreso!=NULL){
                            $tiempo=$this->lib->mensaje_tiempo_transcurrido($usuario->fecha_ingreso."","","Y-m-d");
							$msj_ingreso="Activo ".$tiempo->tiempo;
						}
						$vus[] = array('idus' => $usuario->idus, 'msj' => count($alerts), 'msj_ingreso' => $msj_ingreso);
					}
				}
				echo json_encode($vus);
			}else{
				echo "fail";
			}	
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function actualiza_alerta_msj(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$alerts=$this->M_mensaje->get_row('reseptor',$this->session->userdata("id"));
			echo count($alerts);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function actualiza_alerta_notificacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$alerts=$this->M_notificacion->get_row('reseptor',$this->session->userdata("id"));
			echo count($alerts);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function actualiza_alerta_update(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$alerts=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","0","ac.idac","asc");
			echo count($alerts);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*controle de ajuste de usuario*/
	public function my_history(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$idus=$this->session->userdata("id");
			$usuario=$this->M_usuario->search_usuarios('u.idus',$idus,true);
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata('id'));
			if(!empty($usuario) && !empty($privilegio)){
				$listado['notificaciones_almacen']=$this->M_almacen_seg->get_row('idu',$idus);
				$listado['notificaciones_material']=$this->M_almacen_material_seg->get_row('idu',$idus);
				$listado['notificaciones_producto']=$this->M_producto_seg->get_row('idu',$idus);
				$listado['notificaciones_producto_categoria']=$this->M_producto_grupo_color_seg->get_row('idu',$idus);
				$listado['notificaciones_pedido']=$this->M_pedido_seg->get_row('idu',$idus);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('estructura/controls/alerts/user_alters',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function my_config_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$idus=$this->session->userdata("id");
				$usuario=$this->M_usuario->search_usuarios('u.idus',$idus,true);
				if(!empty($usuario)){
					$listado['usuario']=$usuario[0];
					$listado['ciudades']=$this->M_ciudad->get_all();
					$this->load->view("estructura/controls/users/update",$listado);
				}else{
					echo "fail";
				}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function my_update_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['tel']) && isset($_POST['usu']) && isset($_POST['car']) && isset($_FILES)){
						$ci=trim($_POST['ci']);
						$ciu=trim($_POST['ciu']);
						$nom1=trim($_POST['nom1']);
						$nom2=trim($_POST['nom2']);
						$pat=trim($_POST['pat']);
						$mat=trim($_POST['mat']);
						$tel=trim($_POST['tel']);
						$usu=trim($_POST['usu']);
						$car=trim($_POST['car']);
						if($this->val->entero($ci,6,9) && $ci>100000 && $ci<=999999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->entero($tel,0,15) && $this->val->password($usu,4,15)){
							$control=true;
							if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
							if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
							if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
							if($pat!=""){ if(!$this->val->strSpace($pat,2,20)){ $control=false;}}
							if($control){
								$idus=$this->session->userdata("id");
								$usuario=$this->M_usuario->get_row('idus',$idus);
								if(!empty($usuario)){
									$usuario=$usuario[0];
									$ci_org=$usuario->ci;
									$control_usuario=$this->M_usuario->get_row('usuario',$usu);
									if(empty($control_usuario)){
										$control=true;
									}else{
										if($usuario->idus==$control_usuario[0]->idus){
											$control=true;
										}else{
											$control=false;
										}
									}
									if($control){
										$control=$this->M_persona->get($ci);
										$guardar="ok";
										if(!empty($control)){
											if($ci==$ci_org){
												$guardar="ok";
											}else{
												$guardar="ci_exist";
											}
										}
										if($guardar=="ok"){
											$persona=$this->M_persona->get($ci_org);
											$img=$persona[0]->fotografia;
											$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$ci);//cambiar_imagen_miniatura($FILES,$ruta,$pos,$resize,$origen,$id)
											if($img!='error' && $img!="error_type" && $img!="error_size_img"){
												if($this->M_persona->modificar_usuario($ci_org,$ci,$ciu,$nom1,$nom2,$pat,$mat,$car,$tel,$img)){
													if($this->M_usuario->modificar($idus,$ci,$usu,$control_usuario[0]->tipo)){
														$this->session->set_userdata('login',$usu);
														$this->session->set_userdata('nombre',$nom1,true);
														$this->session->set_userdata('nombre2',$nom2,true);
														$this->session->set_userdata('paterno',$pat,true);
														$this->session->set_userdata('cargo',$car,true);
														$this->session->set_userdata('fotografia',$img,true);
														echo "ok";
													}else{
														echo "error";
													}
												}else{
													echo "error";
												}
											}else{
												echo $img;
											}
										}else{
											echo $guardar;
										}
									}else{
										echo "name_user_exist";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function form_cambiar_password(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$idus=$this->session->userdata("id");
				$empleado=$this->M_usuario->get_row_complet("u.idus",$idus);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$this->load->view('estructura/controls/users/change_password',$listado);
				}else{
					echo "fail";
				}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function cambiar_password(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pass']) && isset($_POST['nuevo']) && isset($_POST['nuevo2'])){
				$idus=$this->session->userdata("id");
				$pass=$_POST['pass'];
				$nuevo=$_POST['nuevo'];
				$nuevo2=$_POST['nuevo2'];
				if($this->val->password($nuevo,4,25)){
					if($nuevo==$nuevo2){
						$usuario=$this->M_usuario->get($idus);
						if(!empty($usuario)){
							if($usuario[0]->usuario==$this->session->userdata("login")){
								$control=$this->M_usuario->validate($this->session->userdata("login"),$pass);
								if(!empty($control)){
									if($this->M_usuario->modificar_password($idus,$nuevo)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_img(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['id']) && isset($_POST['type'])){
				$url=base_url()."libraries/img/";
				$type=trim($_POST['type']);
				$archivos = array();
				if($type=="almacen"){
					$ida=trim($_POST['id']);
					$img="sistema/default.jpg";
					$almacen=$this->M_almacen->get_row("ida",$ida);
					if(!empty($almacen)){
							if($almacen[0]->fotografia!="" && $almacen[0]->fotografia!=NULL){ $img="almacenes/".$almacen[0]->fotografia;}
							$archivos = array(array('titulo' => $almacen[0]->nombre,'url' =>  $url.$img,"descripcion"=>""));
					}else{
						$archivos[] = array('titulo' => "",'url' =>  $url.$img,"descripcion"=>"");
					}
				}
				if($type=="material"){
					$idmi=trim($_POST['id']);
					$material=$this->M_material_item->get_row("idmi",$idmi);
					if(!empty($material)){
						$imagenes=$this->M_material_imagen->get_row("idmi",$idmi);
						$img="sistema/default.jpg";
						$archivos = array();
						if(!empty($imagenes)){
							for ($i=0; $i < count($imagenes) ; $i++) { 
								$img="materiales/".$imagenes[$i]->nombre;
								$titulo=$imagenes[$i]->titulo;
								$descripcion=$imagenes[$i]->descripcion;
								if($titulo=="" || $titulo==NULL){ $titulo=$material[0]->nombre; }
								$archivos[] = array('titulo' => $titulo,'url' =>  $url.$img,'descripcion' => $descripcion);
							}
						}else{
							$archivos[] = array('titulo' => $material[0]->nombre,'url' =>  $url.$img,'descripcion' => "");
						}
					}else{
						echo "fail";
					}
				}
				if($type=="empleado"){
					$ide=trim($_POST['id']);
					$img="sistema/default.jpg";
					$empleado=$this->M_empleado->get_row("ide",$ide);
					if(!empty($empleado)){
						$persona=$this->M_persona->get($empleado[0]->ci);
						if($persona[0]->fotografia!="" && $persona[0]->fotografia!=NULL){ $img="personas/".$persona[0]->fotografia;}
						$nombre=$persona[0]->nombre." ".$persona[0]->nombre2." ".$persona[0]->paterno." ".$persona[0]->materno;
						$archivos = array(array('titulo' => $nombre,'url' =>  $url.$img,"descripcion"=>""));
					}else{
						$archivos[] = array('titulo' => "",'url' =>  $url.$img,"descripcion"=>"");
					}
				}
				if($type=="producto"){
					$idp=trim($_POST['id']);
					$producto=$this->M_producto->get($idp);
					if(!empty($producto)){
						$img="sistema/default.jpg";
						$imagenes=$this->M_producto_imagen->get_row("idp",$idp);
						if(!empty($imagenes)){
							$archivos=array();
							for($i=0; $i < count($imagenes) ; $i++){ $imagen=$imagenes[$i];
								$img="productos/".$imagen->archivo;
								$descripcion=$imagen->descripcion;
								$archivos[]=array('titulo' => $producto[0]->codigo." - ".$producto[0]->nombre,'url' =>  $url.$img,"descripcion"=>$descripcion);
							}
						}else{
							$archivos[]=array('titulo' => $producto[0]->codigo." - ".$producto[0]->nombre,'url' =>  $url.$img,"descripcion"=>"");
						}
					}else{
						echo "fail";
					}
				}
				if($type=="producto_imagen"){
					$idpi=trim($_POST['id']);
					$imagen=$this->M_producto_imagen->get_row("idpi",$idpi);
					if(!empty($imagen)){
						$producto=$this->M_producto->get($imagen[0]->idp);
						if(!empty($producto)){
							$img="productos/".$imagen[0]->archivo;
							$descripcion=$imagen[0]->descripcion;
							$archivos = array(array('titulo' => $producto[0]->codigo." - ".$producto[0]->nombre,'url' =>  $url.$img,"descripcion"=>$descripcion));
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}
				if($type=="pieza"){
					$idppi=trim($_POST['id']);
					$pieza=$this->M_producto_pieza->get_row("idppi",$idppi);
					if(!empty($pieza)){
						$img="sistema/default.jpg";
						if($pieza[0]->imagen!="" && $pieza[0]->imagen!=NULL){
							$img="piezas/".$pieza[0]->imagen;
						}
						$nombre=$pieza[0]->nombre;
						$descripcion=$pieza[0]->descripcion;
						$archivos = array(array('titulo' => $nombre,'url' =>  $url.$img,"descripcion"=>$descripcion));
					}else{
						echo "fail";
					}
				}
				$listado['imagenes']=json_decode(json_encode($archivos));
				$this->load->view('estructura/visor',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function error(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view('estructura/404');
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function error_404(){
			$this->load->view('estructura/404');
	}
	public function help($module){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($module)){
				if($module!=""){
					switch($module){
						case 'almacen':$this->load->view('almacen/almacenes/1-help/help');break;
						default: echo "Error... ";break;
					}
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "Acceso denegado";
			}
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */