<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Movimiento extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if($privilegio[0]->mo=="1"){
				if(!isset($_GET['p'])){
					if($privilegio[0]->mo1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->mo2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->mo3r==1){ 
								$listado['pestania']=3;
							}else{
								$listado['pestania']="";
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				date_default_timezone_set("America/La_Paz");
				if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
				$this->load->view('v_movimiento',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
	/*------- MANEJO DE PEDIDOS -------*/
	public function search_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['clientes']=$this->M_cliente->get_all();
			$listado['privilegio']=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
			$this->load->view('movimiento/pedidos/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
			if(!empty($privilegio)){
				$atrib=""; $val="";$type_search="";
				if(isset($_POST['num']) && isset($_POST['nom']) && isset($_POST['cli']) && isset($_POST['est'])){
					if($_POST['num']!=""){
						$atrib='idpe';$val=$_POST['num'];$type_search="like";
					}else{
						if($_POST['cli']!=""){
							$atrib='idcl';$val=$_POST['cli'];$type_search="equals";
						}else{
							if($_POST['nom']!=""){
								$atrib='nombre';$val=$_POST['nom'];$type_search="like";
							}else{
								if($_POST['est']!=""){
									$atrib='estado';$val=$_POST['est'];$type_search="equals";
								}
							}
						}
					}
				}
				$listado['pedidos']=$this->M_pedido->get_search("","");
				$listado['pagos']=$this->M_pago->get_all();
				$listado['partes_detalles']=$this->M_parte_pedido->get_detalle("","");
				$listado['atrib']=$atrib;
				$listado['val']=$val;
				$listado['type_search']=$type_search;
				$listado['privilegio']=$privilegio[0];
				$this->load->view('movimiento/pedidos/view',$listado);	
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- Nuevo ---*/
	public function new_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['productos']=$this->M_producto->get_all();
			$listado['clientes']=$this->M_cliente->get_all();
			$max=$this->M_pedido->max('orden');
			$listado['nro_pedido']=$max+1;
			$this->load->view('movimiento/pedidos/3-nuevo/form',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function refresh_nro_orden(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$max=$this->M_pedido->max('idpe');
			echo $max+1;
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function search_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type']) && isset($_POST['cl'])){
				$idcl=trim($_POST['cl']);
				$cliente=$this->M_cliente->get($idcl);
				if(!empty($cliente)){
					$listado['type']=$_POST['type'];
					if(isset($_POST['tipo'])){
						$tipo=trim($_POST['tipo']);
						if($tipo=="0" || $tipo=="1" || $tipo=="2"){
							$listado['tipo']=trim($_POST['tipo']);
						}
					}
					$listado['cliente']=$cliente[0];
					$this->load->view('movimiento/pedidos/productos/search',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type']) && isset($_POST['pa'])){
				$control=true;
				$atrib="";$val="";
				if(isset($_POST['nom'])){
					if($_POST['nom']!=""){ $atrib='nombre';$val=trim($_POST['nom']); }
				}
					$listado['productos']=$this->M_producto->get_search($atrib,$val,"1");
					$listado['colores']=$this->M_color->get_all();
					$listado['procesos']=$this->M_proceso->get_all();
					$listado['producto_imagenes']=$this->M_producto_imagen->get_all();
					$listado['producto_imagenes_color']=$this->M_producto_imagen_color->get_all();
					$listado['productos_grupos']=$this->M_producto_grupo->get_grupo_colores("","");
					$listado['type']=trim($_POST['type']);
					$listado['pa']=trim($_POST['pa']);
					$this->load->view('movimiento/pedidos/productos/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function add_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['type']) && isset($_POST['cl'])){
				$idp=trim($_POST['p']);
				$type=trim($_POST['type']);
				$idcl=trim($_POST['cl']);
				$producto=$this->M_producto->get($idp);
				$cliente=$this->M_cliente->get($idcl);
				if(!empty($producto) && !empty($cliente)){
					if(isset($_POST['tipo'])){
						$tipo=trim($_POST['tipo']);
						if($tipo=="0" || $tipo=="1" || $tipo=="2"){
							$listado['tipo']=$tipo;
						}
					}
					//echo "Tipo ".$tipo;
					$colores=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp);
					$productos_imagenes=$this->M_producto_imagen->get_all();
					$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
					$listado['grupos']=$this->lib->colores_producto($producto[0],$colores,$productos_imagenes,$productos_imagenes_colores);
					$listado['producto']=$producto[0];
					$listado['sucursales']=$this->M_sucursal_cliente->get_row("idcl",$idcl);
					$listado['cliente']=$cliente[0];
					$listado['type']=$_POST['type'];
					$listado['productos_imagenes']=$productos_imagenes;
					$listado['productos_imagenes_colores']=$productos_imagenes_colores;
					$this->load->view('movimiento/pedidos/productos/accordion_new_producto',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function colores_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['pgcs']) && isset($_POST['tbl'])){
				$idp=trim($_POST['p']);
				$producto=$this->M_producto->get($idp);
				if(!empty($producto)){
					$listado['colores']=$this->M_producto->get_colores('p.idp',$idp,"1");
					$listado['colores_asignados']=json_decode(json_encode(trim($_POST['pgcs'])));
					$listado['producto']=$producto[0];
					$listado['tbl']=trim($_POST['tbl']);
					if(isset($_POST['tipo'])){$listado['tipo']=trim($_POST['tipo']);}//tipo de parte Parte,Muestra o Extra
					if(isset($_POST['type'])){$listado['type']=$_POST['type'];}
					$this->load->view('movimiento/pedidos/productos/categorias_colores_new',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_producto_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){

			if(isset($_POST['p']) && isset($_POST['pgc']) && isset($_POST['tbl']) && isset($_POST['tipo']) && isset($_POST['cl'])){
				$idp=trim($_POST['p']);
				$idpgrc=trim($_POST['pgc']);
				$tbl=trim($_POST['tbl']);
				$tipo=trim($_POST['tipo']);
				$idcl=trim($_POST['cl']);
				$producto=$this->M_producto->get($idp);
				$cliente=$this->M_cliente->get($idcl);
				$color=$this->M_producto_grupo->get_grupo_colores("pgc.idpgrc",$idpgrc);
				if(!empty($producto) && !empty($cliente) && !empty($color) && ($tipo=="0" || $tipo=="1" || $tipo=="2")){
					$productos_imagenes=$this->M_producto_imagen->get_all();
					$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
					$listado['producto']=$producto[0];
					$listado['cliente']=$cliente[0];
					$listado['sucursales']=$this->M_sucursal_cliente->get_row("idcl",$idcl);
					$grupo=$this->lib->colores_producto($producto[0],$color,$productos_imagenes,$productos_imagenes_colores);
					//var_dump($grupo);
					$listado['grupo']=$grupo[0];
					$listado['idtb']=$tbl;
					$listado['tipo']=$tipo;
					$this->load->view('movimiento/pedidos/productos/tr_categorias_colores',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['detalles']) && isset($_POST['nro']) && isset($_POST['nom']) && isset($_POST['fec']) && isset($_POST['obs']) && isset($_POST['des']) && isset($_POST['cl']) && isset($_POST['tipo'])){
				$nro=trim($_POST['nro']);
				$nom=trim($_POST['nom']);
				$fec=trim($_POST['fec']);
				$obs=trim($_POST['obs']);
				$descuento=trim($_POST['des'])*1;
				$tipo=trim($_POST['tipo']);
				$idcl=trim($_POST['cl']);
				$cliente=$this->M_cliente->get($idcl);
				if(!empty($cliente) && $this->val->entero($nro,0,10) && $this->val->strSpace($nom,3,150) && ($tipo=="0" || $tipo=="1" || $tipo=="2") && $this->val->fecha($fec)){
					$all_productos=$this->M_producto->get_colores("","","");
					$detalles_pedido=NULL;
					$all_sucursales=$this->M_sucursal_cliente->get_complet("sc.idcl",$idcl);
					$all_sucursal_detalle_pedido=NULL;
					/*VALIDANDO DATOS*/
					$detalles=$this->lib->validate_colores_producto_pedido($_POST['detalles'],$all_productos,$detalles_pedido,$all_sucursales,$all_sucursal_detalle_pedido,$this->val);
					/*END VALIDANDO DATOS*/
					if($detalles->control){
                        $idpe=$this->M_pedido->max('idpe') + 1;
						if($this->M_pedido->insertar($idpe,$nro,$idcl,$nom,$descuento,$obs)){
							$idpp=$this->M_parte_pedido->max("idpp")+1;
							if($this->M_parte_pedido->insertar($idpp,$idpe,1,$fec,$tipo,"")){
								foreach($detalles->productos as $key => $detalle){
									$iddp=$this->M_detalle_pedido->max("iddp")+1;
									$costo_venta=$detalle->cu*$detalle->cantidad;
									if($detalle->cantidad*1>0){
										if($this->M_detalle_pedido->insertar($iddp,$idpp,$detalle->pgc,$detalle->posicion,$detalle->observacion,$detalle->cu,$costo_venta)){
										$sucursales=json_decode(json_encode($detalle->sucursales));
										foreach($sucursales as $key => $sucursal){
											if($sucursal->cantidad*1>0){
												if($this->M_sucursal_detalle_pedido->insertar($sucursal->sc,$iddp,$sucursal->cantidad)){}
											}else{/*echo "error";*/}
										}
									}else{/*echo "error";*/}
									}else{/*echo "fail";*/}
								}//end foreach
							}else{/*echo "error";*/}
							/*Notificacion*/
							$msj="Creó el pedido <strong>N°".$nro." ".$nom."</strong>";
							if($this->M_pedido_seg->insertar($idpe,NULL,NULL,"cp",$msj)){
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
								foreach ($resultados as $key => $result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "ok";
							}
							/*End Notificacion*/
						}else{/*echo "error";*/}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*--- End Nuevo ---*/
/*--- Imprimir ---*/
   	public function print_pedidos(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['num']) && isset($_POST['nom']) && isset($_POST['cli'])){
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['pedidos']=$this->M_pedido->get_search("","");
					$listado['pagos']=$this->M_pago->get_all();
					$listado['partes_detalles']=$this->M_parte_pedido->get_detalle("","");
					$listado['privilegio']=$privilegio[0];
					$listado['tbl']=trim($_POST['tbl']);
					$listado['num']=trim($_POST['num']);
					$listado['nom']=trim($_POST['nom']);
					$listado['cli']=trim($_POST['cli']);
					$this->load->view('movimiento/pedidos/4-imprimir/print_pedido',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_pedidos(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
			if(!empty($_GET['file']) && !empty($privilegio) && isset($_GET['num']) && isset($_GET['nom']) && isset($_GET['cli'])){
				$atrib="";$val="";
				if($_GET['num']!=""){
					$atrib='p.idpe';$val=$_GET['num'];
				}else{
					if($_GET['cli']!=""){
						$atrib='c.idcl';$val=$_GET['cli'];
					}else{
						if($_GET['nom']!=""){
							$atrib='p.nombre';$val=$_GET['nom'];
						}
					}
				}
				$listado['pedidos']=$this->M_pedido->get_search($atrib,$val);
				$listado['pagos']=$this->M_pago->get_all();
				$listado['partes_detalles']=$this->M_parte_pedido->get_detalle("","");
				$listado['privilegio']=$privilegio[0];
				$listado['file']=trim($_GET['file']);
				$this->load->view('movimiento/pedidos/4-imprimir/export_pedidos',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function reporte_pedido(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get_search("p.idpe",$idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
					/*para descuento*/ 
					$partes=$this->M_parte_pedido->get_row("idpe",$idpe);
					$detalles_partes_pedido=$this->M_parte_pedido->get_detalle("idpe",$idpe);
					$productos=$this->M_producto->get_search_all(NULL,NULL,NULL);
					$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
					$productos_imagenes=$this->M_producto_imagen->get_all();
					$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
					$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
					$order_by="codigo";//codigo o posicion
					$descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
					$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
					$usuarios=$this->M_usuario->get_search(NULL,NULL);
					$v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
					$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
					/*end para descuento*/ 
					$descuentos_producto=json_decode(json_encode($v_productos));
					$total_descuento=0;
					foreach ($descuentos_producto as $key => $descuento) {
						$total_descuento+=($descuento->descuento*1);
					}
					$listado['descuento']=$total_descuento;
   					$listado['privilegio']=$privilegio[0];
   					$listado['pedido']=$pedido[0];
   					$listado['pagos']=$this->M_pago->get_row("idpe",$idpe);
   					$this->load->view('movimiento/pedidos/5-reportes/detalle_pedido',$listado);
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function exportar_reporte_pedido(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_GET['pe']) && isset($_GET['file'])){
   				$idpe=trim($_GET['pe']);
   				$pedido=$this->M_pedido->get_search("p.idpe",$idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					$listado['privilegio']=$privilegio[0];
   					$listado['pedido']=$pedido[0];
   					$listado['file']=$_GET['file'];
   					$this->load->view('movimiento/pedidos/5-reportes/exportar_detalle_pedido',$listado);
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function taller_pedido(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get_search("p.idpe",$idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
   					$partes_pedido=$this->M_parte_pedido->get_row("idpe",$idpe);
   					$listado['pedido']=$pedido[0];
   					$listado['privilegio']=$privilegio[0];
   					$listado['cliente']=$cliente[0];
   					$listado['partes_pedido']=$partes_pedido;
   					$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
//   					$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
   					if(isset($_POST['pp'])){$idpp=trim($_POST['pp']);}else{if(!empty($partes_pedido)){$idpp=$partes_pedido[0]->idpp;}}
   					$parte_pedido=null;
   					if($idpp!=""){
   						$parte_pedido=$this->lib->search_elemento($partes_pedido,'idpp',$idpp);
						$productos=$this->M_producto->get_search_all(NULL,NULL,NULL);
				   		//$productos_grupos=$this->M_producto_grupo->get_grupo(NULL,NULL);
				   		$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
				   		$productos_imagenes=$this->M_producto_imagen->get_all();
				   		$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
				   		$detalles_pedidos=$this->M_detalle_pedido->get_search("idpp",$parte_pedido->idpp);
				   		$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
				   		$order_by="posicion";//codigo o posicion
				   		$listado['productos']=$this->lib->productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);
   						//$listado['detalle_pedido']=$this->M_detalle_pedido->get_search('dp.idpp',$idpp);
   						$listado['parte_pedido']=$parte_pedido;
   						$this->load->view('movimiento/pedidos/5-reportes/taller_pedido',$listado);
   					} else {
   					    echo "fail";
                    }
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function exportar_taller_pedido(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_GET['pe']) && isset($_GET['pp']) && isset($_GET['file'])){
   				$idpe=trim($_GET['pe']);
   				$idpp=trim($_GET['pp']);
   				$pedido=$this->M_pedido->get_search("p.idpe",$idpe);
   				$parte_pedido=$this->M_parte_pedido->get_row('idpp',$idpp);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($parte_pedido) && !empty($privilegio)){
   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
   					$partes_pedido=$this->M_parte_pedido->get_row("idpe",$idpe);
   					$listado['pedido']=$pedido[0];
   					$listado['cliente']=$cliente[0];
   					$listado['privilegio']=$privilegio[0];
   					$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
   					$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
   					$listado['detalle_pedido']=$this->M_detalle_pedido->get_search('dp.idpp',$idpp);
   					$listado['parte_pedido']=$parte_pedido[0];
   					$listado['file']=$_GET['file'];
   					$productos=$this->M_producto->get_search(NULL,NULL,NULL);
					$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
					$productos_imagenes=$this->M_producto_imagen->get_all();
					$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
					$detalles_pedidos=$this->M_detalle_pedido->get_search("idpp",$parte_pedido[0]->idpp);
					$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
					$order_by="posicion";//codigo o posicion
					$listado['productos']=$this->lib->productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);
   					$this->load->view('movimiento/pedidos/5-reportes/exportar_taller_pedido',$listado);
   				}else{
   					echo "fail";
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function informe_economico_pedido_detallado(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
						/*para descuento*/ 
						$partes=$this->M_parte_pedido->get_row("idpe",$idpe);
						$detalles_partes_pedido=$this->M_parte_pedido->get_detalle("pp.idpe",$idpe);
						$productos=$this->M_producto->get_search(NULL,NULL,NULL);
						$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
						$productos_imagenes=$this->M_producto_imagen->get_all();
						$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
						$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
						$order_by="codigo";//codigo o posicion
						$descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
						$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
						$usuarios=$this->M_usuario->get_search(NULL,NULL);
						$v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
						$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
						/*end para descuento*/ 
						$descuentos_producto=json_decode(json_encode($v_productos));
						$total_descuento=0;
						foreach ($descuentos_producto as $key => $descuento) {
							$total_descuento+=($descuento->descuento*1);
						}
						$listado['descuento']=$total_descuento;


	   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
	   					$listado['cliente']=$cliente[0];
	   					$listado['pedido']=$pedido[0];
	   					$listado['sucursales']=$sucursales_cliente;
	   					$listado['partes']=$partes;
	   					$listado['detalle_pedido']=$detalles_partes_pedido;
	   					$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
	   					$this->load->view('movimiento/pedidos/5-reportes/informe_economico_detallado',$listado);
   					}
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function exportar_pedido_informe_detallado(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_GET['pe']) && isset($_GET['file'])){
   				$idpe=trim($_GET['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					
	   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
	   					$listado['cliente']=$cliente[0];
	   					$listado['pedido']=$pedido[0];
	   					$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
	   					$listado['partes']=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
	   					$listado['detalle_pedido']=$this->M_parte_pedido->get_detalle('pp.idpe',$idpe);
	   					$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
	   					$listado['file']=$_GET['file'];
	   					$listado['privilegio']=$privilegio[0];
	   					$this->load->view('movimiento/pedidos/5-reportes/exportar_pedido_informe_detallado',$listado);
   					
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function informe_economico_pedido_general(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
						/*para descuento*/ 
						$partes=$this->M_parte_pedido->get_row("idpe",$idpe);
						$detalles_partes_pedido=$this->M_parte_pedido->get_detalle("pp.idpe",$idpe);
						$productos=$this->M_producto->get_search(NULL,NULL,NULL);
						$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
						$productos_imagenes=$this->M_producto_imagen->get_all();
						$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
						$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
						$order_by="codigo";//codigo o posicion
						$descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
						$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
						$usuarios=$this->M_usuario->get_search(NULL,NULL);
						$v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
						$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
						/*end para descuento*/ 
						$descuentos_producto=json_decode(json_encode($v_productos));
						$total_descuento=0;
						foreach ($descuentos_producto as $key => $descuento) {
							$total_descuento+=($descuento->descuento*1);
						}
						$listado['descuento']=$total_descuento;

						
	   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
	   					$listado['cliente']=$cliente[0];
	   					$listado['pedido']=$pedido[0];
	   					$listado['sucursales']=$sucursales_cliente;
	   					$listado['partes']=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
	   					$listado['detalle_pedido']=$detalles_partes_pedido;
	   					$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
	   					$this->load->view('movimiento/pedidos/5-reportes/informe_economico_general',$listado);
   					}
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function exportar_pedido_informe_general(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_GET['pe']) && isset($_GET['file'])){
   				$idpe=trim($_GET['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
	   				$cliente=$this->M_cliente->get($pedido[0]->idcl);
	   				$listado['cliente']=$cliente[0];
	   				$listado['pedido']=$pedido[0];
	   				$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
	   				$listado['partes']=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
	   				$listado['detalle_pedido']=$this->M_parte_pedido->get_detalle('pp.idpe',$idpe);
	   				$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
	   				$listado['file']=$_GET['file'];
	   				$listado['privilegio']=$privilegio[0];
	   				$this->load->view('movimiento/pedidos/5-reportes/exportar_pedido_informe_general',$listado);
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function informe_depositos() {
        if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
            if(isset($_POST['pe'])) {
                $idpe = trim($_POST['pe']);
                $pedido = $this->M_pedido->get($idpe);
                $privilegio = $this->M_privilegio->get_movimiento($this->session->userdata("id"), "pedido");
                if (!empty($pedido) && !empty($privilegio)) {
                    if ($privilegio[0]->mo == 1 && $privilegio[0]->mo1r == 1) {
                        $listado['pedido'] = $pedido[0];
                        $auxPagos=$this->M_pago->get_search("p.idpe",$pedido[0]->idpe,"fecha","asc");
                        $pagos = [];
                        foreach ($auxPagos as $pago) {
                            $pago->fecha = $this->lib->desencriptar_str($pago->fecha);
                            $pagos[] = $pago;
                        }
                        usort($pagos, array($this->lib, 'ordenarPorFecha'));
                        $listado['pagos'] = $pagos;
                        $listado['personas']=$this->M_persona->get_all();
                        $listado['bancos']=$this->M_banco->get_all();
                        $listado['detalles_pedido']=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido[0]->idpe);
                        $listado['descuentos_pagos']=$this->M_descuento_pago->get_all();
                        $listado['tipos_cambio']=$this->M_tipo_cambio->get_all();
                        /*para descuento*/
                        $partes=$this->M_parte_pedido->get_row("idpe",$idpe);
                        $detalles_partes_pedido=$this->M_parte_pedido->get_detalle("idpe",$idpe);
                        $productos=$this->M_producto->get_search(NULL,NULL,NULL);
                        $productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
                        $productos_imagenes=$this->M_producto_imagen->get_all();
                        $productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
                        $sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
                        $order_by="codigo";//codigo o posicion
                        $descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
                        $sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
                        $usuarios=$this->M_usuario->get_search(NULL,NULL);
                        $v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
                            $productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
                        /*end para descuento*/
                        $descuentos_producto=json_decode(json_encode($v_productos));
                        $total_descuento=0;
                        foreach ($descuentos_producto as $key => $descuento) {
                            $total_descuento+=($descuento->descuento*1);
                        }
                        $listado['descuento']=$total_descuento;
                        $this->load->view('movimiento/pedidos/5-reportes/informe_depositos', $listado);
                    }
                }
            } else {
                echo "fail";
            }
        } else{
            if(!empty($this->actualizaciones)){
                redirect(base_url().'update');
            }else{
                echo "logout";
            }
        }
    }
    public function exportar_informe_depositos() {
        if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
            if(isset($_GET['pe']) && isset($_GET['file'])){
                $idpe=trim($_GET['pe']);
                $pedido = $this->M_pedido->get($idpe);
                $privilegio = $this->M_privilegio->get_movimiento($this->session->userdata("id"), "pedido");
                if (!empty($pedido) && !empty($privilegio)) {
                    if ($privilegio[0]->mo == 1 && $privilegio[0]->mo1r == 1 && $privilegio[0]->mo1p == 1) {
                        $listado['pedido'] = $pedido[0];
                        $auxPagos=$this->M_pago->get_search("p.idpe",$pedido[0]->idpe,"fecha","asc");
                        $pagos = [];
                        foreach ($auxPagos as $pago) {
                            $pago->fecha = $this->lib->desencriptar_str($pago->fecha);
                            $pagos[] = $pago;
                        }
                        usort($pagos, array($this->lib, 'ordenarPorFecha'));
                        $listado['pagos'] = $pagos;
                        $listado['personas']=$this->M_persona->get_all();
                        $listado['bancos']=$this->M_banco->get_all();
                        $listado['detalles_pedido']=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido[0]->idpe);
                        $listado['descuentos_pagos']=$this->M_descuento_pago->get_all();
                        $listado['tipos_cambio']=$this->M_tipo_cambio->get_all();
                        /*para descuento*/
                        $partes=$this->M_parte_pedido->get_row("idpe",$idpe);
                        $detalles_partes_pedido=$this->M_parte_pedido->get_detalle("idpe",$idpe);
                        $productos=$this->M_producto->get_search(NULL,NULL,NULL);
                        $productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
                        $productos_imagenes=$this->M_producto_imagen->get_all();
                        $productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
                        $sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
                        $order_by="codigo";//codigo o posicion
                        $descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
                        $sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
                        $usuarios=$this->M_usuario->get_search(NULL,NULL);
                        $v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
                            $productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
                        /*end para descuento*/
                        $descuentos_producto=json_decode(json_encode($v_productos));
                        $total_descuento=0;
                        foreach ($descuentos_producto as $key => $descuento) {
                            $total_descuento+=($descuento->descuento*1);
                        }
                        $listado['descuento']=$total_descuento;
                        $listado['file'] = $_GET['file'];
                        $this->load->view('movimiento/pedidos/5-reportes/exportar/exportar_informe_depositos', $listado);
                    }
                }
            }else{
                echo "fail";
            }
        }else{
            if(!empty($this->actualizaciones)){
                redirect(base_url().'update');
            }else{
                echo "logout";
            }
        }
    }
   	public function informe_materiales(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe'])){
   				$idpe=trim($_POST['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1){
	   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
	   					$partes=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
	   					$listado['cliente']=$cliente[0];
	   					$listado['pedido']=$pedido[0];
	   					$listado['partes']=$partes;
	   					$listado['productos_colores_materiales']=$this->M_producto_grupo_color_material->get_material(NULL,NULL);
						$listado['productos_grupos_materiales']=$this->M_producto_grupo_material->get_material(NULL,NULL);
						$listado['productos_materiales']=$this->M_producto_material->get_material(NULL,NULL);
						$listado['detalles_pedido']=$this->M_parte_pedido->get_detalle('pp.idpe',$idpe);
						$listado['sucursales_detalles_pedidos']=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
						$listado['productos_grupos_colores']=$this->M_producto_grupo_color->get_all();
						$listado['productos_grupos']=$this->M_producto_grupo->get_all();
						$listado['unidades']=$this->M_unidad->get_all();
	   					//$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
	   					if(isset($_POST['pp'])){
	   						$parte=$this->M_parte_pedido->get_row('idpp',$_POST['pp']);
	   						if(!empty($parte)){
	   							$listado['parte']=$parte[0];
	   						}
	   					}else{
	   						$listado['parte']=$partes[0];
	   					}
	   					
	   					
	   					//$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","");
	   					$listado['all_materiales']=$this->M_material->get_all_cantidad();
	   					$listado['privilegio']=$privilegio[0];
	   					$this->load->view('movimiento/pedidos/5-reportes/informe_materiales',$listado);
   					}
   				}
   			}else{
   				echo "fail";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function exportar_materiales_parte_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_GET['pe']) && isset($_GET['file'])){
   				$idpe=trim($_GET['pe']);
   				$pedido=$this->M_pedido->get($idpe);
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($pedido) && !empty($privilegio)){
   					if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
	   					$cliente=$this->M_cliente->get($pedido[0]->idcl);
	   					$partes=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
	   					$listado['cliente']=$cliente[0];
	   					$listado['pedido']=$pedido[0];
	   					$listado['partes']=$partes;
	   					$listado['productos_colores_materiales']=$this->M_producto_grupo_color_material->get_material(NULL,NULL);
						$listado['productos_grupos_materiales']=$this->M_producto_grupo_material->get_material(NULL,NULL);
						$listado['productos_materiales']=$this->M_producto_material->get_material(NULL,NULL);
						$listado['detalles_pedido']=$this->M_parte_pedido->get_detalle('pp.idpe',$idpe);
						$listado['sucursales_detalles_pedidos']=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
						$listado['productos_grupos_colores']=$this->M_producto_grupo_color->get_all();
						$listado['productos_grupos']=$this->M_producto_grupo->get_all();
						$listado['unidades']=$this->M_unidad->get_all();
	   					$listado['all_materiales']=$this->M_material->get_all_cantidad();
	   					if(isset($_GET['pp'])){
	   						$parte=$this->M_parte_pedido->get_row('idpp',$_GET['pp']);
	   						if(!empty($parte)){
	   							$listado['parte']=$parte[0];
	   						}
	   					}else{
	   						$listado['parte']=$partes[0];
	   					}
	   					$listado['privilegio']=$privilegio[0];
	   					$listado['file']=trim($_GET['file']);
	   					$this->load->view('movimiento/pedidos/5-reportes/exportar_materiales_parte_pedido',$listado);
   					}
   				}
   			}else{
   				echo "fail";
   			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_pedido(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   			if(!empty($privilegio)){
   				if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
		   			if(isset($_POST['pe'])){
		   				$idpe=trim($_POST['pe']);
		   				if($this->val->entero($idpe,0,10)){
		   					$pedido=$this->M_pedido->get($idpe);
		   					if(!empty($pedido)){
		   						$listado['clientes']=$this->M_cliente->get_all();
		   						$listado['pedido']=$pedido[0];
								$listado['partes']=$this->M_parte_pedido->get_row('idpe',$idpe);
								$listado['pedidos'] = $this->M_pedido->get_all();
								/*para descuento*/ 
								$partes=$this->M_parte_pedido->get_row("idpe",$idpe);
								$detalles_partes_pedido=$this->M_parte_pedido->get_detalle("idpe",$idpe);
								$productos=$this->M_producto->get_search(NULL,NULL,NULL);
								$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
								$productos_imagenes=$this->M_producto_imagen->get_all();
								$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
								$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
								$order_by="codigo";//codigo o posicion
								$descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
								$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
								$usuarios=$this->M_usuario->get_search(NULL,NULL);
								$v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
								$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
								/*end para descuento*/ 
								$descuentos_producto=json_decode(json_encode($v_productos));
								$total_descuento=0;
								foreach ($descuentos_producto as $key => $descuento) {
									$total_descuento+=($descuento->descuento*1);
								}
								$listado['descuento']=$total_descuento;
		   						$this->load->view('movimiento/pedidos/6-config/form',$listado);
		   					}else{
		   						echo "fail";
		   					}
		   				}else{
		   					echo "fail";
		   				}
		   			}else{
		   				echo "fail";
		   			}
   				}else{
   					echo "No permitido";
   				}
   			}else{
   				echo "No permitido";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   public function pedido_seg(){
   	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   		if(isset($_POST['pe'])){
   			$idpe=trim($_POST['pe']);
   			$pedido=$this->M_pedido->get($idpe);
   			if(!empty($pedido)){
   				$listado['historial']=$this->M_pedido_seg->get_row("idpe",$idpe);
   				$this->load->view('movimiento/pedidos/historial/parte_pedido',$listado);
   			}else{
   				echo "fail";
   			}
   		}else{
   			echo "fail";
   		}
   	}else{
   		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   	}
   }
   	public function update_pedido(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['pe']) && isset($_POST['nro']) && isset($_POST['nom']) && isset($_POST['obs']) && isset($_POST['des']) && isset($_POST['est']) && isset($_POST['est_pag'])){
   				$idpe=trim($_POST['pe']);
                $nro=trim($_POST['nro']);
   				$nom=trim($_POST['nom']);
   				$obs=trim($_POST['obs']);
   				$est=trim($_POST['est']);
   				$est_pag=trim($_POST['est_pag']);
   				$descuento=trim($_POST['des'])*1;
   				if($this->val->entero($idpe,0,10) && $this->val->strSpace($nom,3,150) && $this->val->entero($est,0,1) && $est>=0 && $est<=3 && $this->val->entero($est_pag,0,1) && $est_pag>=0 && $est_pag<=1){
   					$control=true;
   					if($obs!=""){if(!$this->val->textarea($obs,0,300)){ $control=false;}}
   					if($descuento!=""){if(!$this->val->decimal($descuento,6,1) || $descuento<0 || $descuento>999999.9){ $control=false;}}
   					if($control){
   						$pedido=$this->M_pedido->get($idpe);
   						if(!empty($pedido)){
   						    $cambioOrden = false;
   						    if ($pedido[0]->orden != $nro) {
   						        $cambioOrden = true;
   						        $pedidoCambio = $this->M_pedido->get_row("orden", $nro);
   						        $nuevoOrden = $pedido[0]->orden;
                            }
   							if($this->M_pedido->modificar($idpe,$nro,$nom,$descuento,$obs,$est,$est_pag)){
   								$msj="";
   								$des_org=$this->lib->desencriptar_num($pedido[0]->descuento)*1;
                                if($nro!=$pedido[0]->orden){ if($msj!=""){ $msj.=", ";} $msj.=" cambió el orden del pedido de <strong>".$pedido[0]->orden."</strong> a <strong>".$nro."</strong>";}
   								if($nom!=$pedido[0]->nombre){ if($msj!=""){ $msj.=", ";} $msj.=" cambió el nombre del pedido de <strong>".$pedido[0]->nombre."</strong> a <strong>".$nom."</strong>";}
   								if($descuento*1!=$des_org){ if($msj!=""){ $msj.=", ";} $msj.="modificó el descuento del pedido de <strong>".$des_org." Bs.</strong> a <strong>".$descuento." Bs.</strong>";}
   								if($obs!=$pedido[0]->observacion){ if($msj!=""){ $msj.=", ";} $msj.="modificó las observaciones del pedido";}
   								if($est*1!=$pedido[0]->estado*1){ if($msj!=""){ $msj.=", ";} $msj.="modificó el estado de producción del pedido.";}
   								if($est_pag*1!=$pedido[0]->estado_pago*1){ if($msj!=""){ $msj.=", ";} $msj.="modificó el estado de pagos del pedido.";}
   								if($msj!=""){
   									$msj="Pedido <strong>N°".$nro." ".$nom."</strong>(".$msj.")";
   									if($this->M_pedido_seg->insertar($idpe,NULL,NULL,"up",$msj)){
   										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
   										foreach ($resultados as $key => $result) {
   											if($result->idno=="none"){
   												if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
   											}else{
   												if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
   											}
	   									}
	   									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
	   									echo json_encode($result);
	   								}else{
   									    echo "ok";
	   								}
	   							}
                                if ($cambioOrden) {
                                    if($this->M_pedido->modificar($pedidoCambio[0]->idpe,$nuevoOrden,$pedidoCambio[0]->nombre,$pedidoCambio[0]->descuento,$pedidoCambio[0]->observacion,$pedidoCambio[0]->estado,$pedidoCambio[0]->estado_pago)){
                                        $msj = " cambió el orden del pedido de <strong>".$pedidoCambio[0]->orden."</strong> a <strong>".$nuevoOrden."</strong>";
                                        $msj = "Pedido <strong>N°".$nuevoOrden." ".$nom."</strong>(".$msj.")";
                                        if($this->M_pedido_seg->insertar($pedidoCambio[0]->idpe,NULL,NULL,"up",$msj)){
                                            $resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
                                            foreach ($resultados as $key => $result) {
                                                if($result->idno=="none"){
                                                    if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
                                                }else{
                                                    if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
                                                }
                                            }
                                        }else{

                                        }
                                    } else {

                                    }
                                } else {

                                }
	   						}else{
	   							echo "error";
	   						}							
	   					}else{
	   						echo "fail";
	   					}
	   				}
	   			}else{
	   				echo "fail";
	   			}
	   		}else{
	   			echo "fail";
	   		}
	   	}else{
	   		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}	
	   	}
   }
   public function config_parte_pedido(){
	   	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   		$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
	   		if(!empty($privilegio)){
	   			if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
					if(isset($_POST['pp'])){
			   			$idpp=trim($_POST['pp']);
			   			$parte_pedido=$this->M_parte_pedido->get($idpp);
			   			if(!empty($parte_pedido)){
			   				$pedido=$this->M_pedido->get($parte_pedido[0]->idpe);
			   				if(!empty($pedido)){
								$cliente=$this->M_cliente->get($pedido[0]->idcl);
				   				$listado['cliente']=$cliente[0];
			   					$listado['pedido']=$pedido[0];
			   					$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
			   					$listado['partes']=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
			   					$listado['parte_pedido']=$parte_pedido[0];
								$productos=$this->M_producto->get_search_all(NULL,NULL,NULL);
				   				$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
				   				$productos_imagenes=$this->M_producto_imagen->get_all();
				   				$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
				   				$detalles_pedidos=$this->M_detalle_pedido->get_search("idpp",$parte_pedido[0]->idpp);
				   				$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
				   				$order_by="posicion";//codigo o posicion
				   				$listado['productos']=$this->lib->productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);
//				   				print_r($listado['productos']);
			   					$this->load->view('movimiento/pedidos/6-config/partes/view',$listado);
			   				}else{
			   					echo "fail";
			   				}
			   			}else{
			   				echo "fail";
			   			}
			   		}else{
			   			echo "fail";
			   		}
	   			}else{
	   				echo "No permitido";
	   			}
	   		}else{
	   			echo "No permitido";
	   		}
	   	}else{
	   		if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
			}else{
				echo "logout";
			}
	   	}
	}
   public function parte_seg(){
   	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   		if(isset($_POST['pp']) && isset($_POST['pe'])){
   			$idpp=trim($_POST['pp']);
   			$idpe=trim($_POST['pe']);
   			$parte_pedido=$this->M_parte_pedido->get($idpp);
   			$pedido=$this->M_pedido->get($idpe);
   			if(!empty($parte_pedido) && !empty($pedido)){
   				$listado['historial']=$this->M_pedido_seg->get_row_2n("idpp",$idpp,"idpe",$idpe);
   				$this->load->view('movimiento/pedidos/historial/parte_pedido',$listado);
   			}else{
   				echo "fail";
   			}
   		}else{
   			echo "fail";
   		}
   	}else{
   		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   	}
   }
	public function refresh_producto_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type'])){
				$control=false;
					if(isset($_POST['type-refresh']) && isset($_POST['tbl']) && isset($_POST['elemento'])){
						if($_POST['type-refresh']=="db"){
							if(isset($_POST['p']) && isset($_POST['pp'])){
								$idp=trim($_POST['p']);
								$idpp=trim($_POST['pp']);
//								$producto=$this->M_producto->get_seach("idp",$idp,NULL);
                                $producto=$this->M_producto->get_search_all("idp",$idp,NULL);
								$parte_pedido=$this->M_parte_pedido->get_complet("pp.idpp",$idpp);
								if(!empty($producto) && !empty($parte_pedido)){
									$url=base_url().'libraries/img/';
									//$productos_grupos=$this->M_producto_grupo->get_grupo("pg.idp",$idp);
			   						//$productos_grupos_colores=$this->M_producto_grupo->get_colores("pg.idp",$idp);
			   						$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp);
					   				$productos_imagenes=$this->M_producto_imagen->get_all();
					   				$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
					   				$detalles_pedidos=$this->M_detalle_pedido->get_search("idpp",$idpp);
					   				$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
					   				$order_by="codigo";
					   				$json_producto=$this->lib->productos_pedido($producto,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);
					   				if(isset($json_producto[0]->categorias)){
					   					$cliente=$this->M_cliente->get($parte_pedido[0]->idcl);
					   					$listado['grupos']=json_decode(json_encode($json_producto[0]->categorias));
					   					$listado['producto']=$producto[0];
					   					$listado['parte_pedido']=$parte_pedido[0];
					   					$listado['cliente']=$cliente[0];
					   					$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
										$listado['idtb']=$_POST['tbl'];
										$listado['elemento']=$_POST['elemento'];
										$listado['type']=$_POST['type'];
										$this->load->view('movimiento/pedidos/productos/table_producto',$listado);
					   				}else{
					   					echo "fail";
					   				}
								}else{
									echo "fail";
								}
							}
						}
					}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function refresh_producto_detalle_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['dp']) && isset($_POST['p']) && isset($_POST['item']) && isset($_POST['tbl']) && isset($_POST['container']) && isset($_POST['container-2'])){
				$iddp=trim($_POST['dp']);
				$idp=trim($_POST['p']);
				$item=trim($_POST['item']);
				$tbl=trim($_POST['tbl']);
				$detalle_pedido=$this->M_detalle_pedido->get_search("iddp",$iddp);
				$producto=$this->M_producto->get_search("idp",$idp,NULL);
				if(!empty($detalle_pedido) && !empty($producto) && $this->val->entero($item,0,10)){
					$parte_pedido=$this->M_parte_pedido->get_complet("pp.idpp",$detalle_pedido[0]->idpp);
					if(!empty($parte_pedido)){
						$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp);
						$productos_imagenes=$this->M_producto_imagen->get_all();
						$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
						$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
						$order_by="codigo";
						$json_producto=$this->lib->productos_pedido($producto,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalle_pedido,$sucursales_detalles_pedidos,$order_by);
						if(isset($json_producto[0]->categorias)){
					   		$cliente=$this->M_cliente->get($parte_pedido[0]->idcl);
					   		$listado['grupo']=json_decode(json_encode($json_producto[0]->categorias));
					   		$listado['producto']=$producto[0];
					   		$listado['parte_pedido']=$parte_pedido[0];
					   		//$listado['cliente']=$cliente[0];
					   		$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
					   		$listado['item']=$item;
					   		$listado['tbl']=$tbl;
					   		$listado['container']=$_POST['container'];
					   		$listado['container_2']=$_POST['container-2'];
							//$listado['idtb']=$_POST['tbl'];
							//$listado['elemento']=$_POST['elemento'];
							//$listado['type']=$_POST['type'];
							$this->load->view('movimiento/pedidos/productos/tr_refresh_categorias_colores',$listado);
					   	}else{
					   		echo "fail";
					   	}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   public function update_parte_pedido(){
   	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   		if(isset($_POST['status']) && isset($_POST['detalles']) && isset($_POST['pp']) && isset($_POST['fec']) && isset($_POST['obs']) && isset($_POST['tipo']) && isset($_POST['cl'])){
   			$idpp=trim($_POST['pp']);
			$fec=trim($_POST['fec']);
			$obs=trim($_POST['obs']);
			$tipo=trim($_POST['tipo']);
			$idcl=trim($_POST['cl']);
			$parte_pedido=$this->M_parte_pedido->get($idpp);
			$cliente=$this->M_cliente->get($idcl);
			if(!empty($parte_pedido) && !empty($cliente) && ($tipo=="0" || $tipo=="1" || $tipo=="2") && $this->val->fecha($fec) && $this->val->textarea($obs,0,300)){
				$status=json_decode($_POST['status']);//lista de productos con estado eliminado (0) o no (1)
				$pedido=$this->M_pedido->get($parte_pedido[0]->idpe);
				if($pedido[0]->estado!="2"){
					//$all_productos=$this->M_producto->get_colores("","","");
					$all_productos=$this->M_producto->get_colores_all("","","");
					$detalles_pedido=$this->M_detalle_pedido->get_search("dp.idpp",$idpp);
					$all_sucursales=$this->M_sucursal_cliente->get_complet("sc.idcl",$pedido[0]->idcl);
					$all_sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_row(NULL,NULL);
					//sacando todos las sucursales productos de la parte
					$detalles=$this->lib->validate_colores_producto_pedido($_POST['detalles'],$all_productos,$detalles_pedido,$all_sucursales,$all_sucursal_detalle_pedido,$this->val);
					if($detalles->control){
						$msj_parte="";
						$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
						if($this->M_parte_pedido->modificar($idpp,$parte_pedido[0]->numero,$fec,$tipo,$obs)){
							if($tipo!=$parte_pedido[0]->tipo){$msj_parte.="el tipo de <b>".$v[$parte_pedido[0]->tipo*1]."</b> a <b>".$v[$tipo]." ".$numero."</b>";}
							   if($fec!=$parte_pedido[0]->fecha){ if($msj_parte!=""){ $msj_parte.=", ";} $msj_parte.="la fecha de ".$parte_pedido[0]->fecha." a ".$fec;}
							   if($obs!=$parte_pedido[0]->observacion){if($msj_parte!=""){$msj_parte.=", ";}$msj_parte.="las observaciones";}
							/*Eliminando*/
							$cont_del=0;$msj_del="";
							foreach($status as $key => $eliminado){
								if($eliminado->status==0){
									$det_ped=$this->lib->search_elemento($detalles_pedido,"iddp",$eliminado->dp);
									if($det_ped!=NULL){
										$producto_grupo_color=$this->lib->search_elemento($all_productos,"idpgrc",$det_ped->idpgrc);
										if($producto_grupo_color!=null){
											if($this->M_detalle_pedido->eliminar($eliminado->dp)){
												$cont_del++;
												$nom=$producto_grupo_color->nombre;
												if($producto_grupo_color->abr_g!=""){$nom.="-".$producto_grupo_color->nombre_g;}
												$nom.="-".$producto_grupo_color->nombre_c;
												if($msj_del!=""){$msj_del.=",";}
												$msj_del.=$nom;
											}
										}
									}
								}
							}
							/*End eliminando*/
							$cont_up=0;$msj_up_prod="";$cont_new=0;$msj_new="";$msj_aux="";
							foreach($detalles->productos as $key => $detalle){
								$msj_up="";
								if(isset($detalle->dp)){//MODIFICAR DETALLE
									$detalle_anterior=$this->lib->search_elemento($detalles_pedido,"iddp",$detalle->dp);
									$cu_ant=$this->lib->desencriptar_num($detalle_anterior->cu);
									$cv_ant=$this->lib->desencriptar_num($detalle_anterior->cv);
									$sw=false;
									if($detalle_anterior->posicion!=$detalle->posicion || $detalle->cu*1!=$cu_ant*1 || $detalle_anterior->observacion!=$detalle->observacion || ($cv_ant*1)!=($detalle->cu*$detalle->cantidad)){
										if($this->M_detalle_pedido->modificar($detalle->dp,$detalle->posicion,$detalle->observacion,$detalle->cu,$detalle->cu*$detalle->cantidad)){
											$sw=true;
											if($cu_ant*1!=$detalle->cu*1){
												$msj_up="el costo unitario de venta de ".($cu_ant*1)."Bs. a ".($detalle->cu*1)."Bs.";
											}
											if($detalle_anterior->observacion!=$detalle->observacion){
												if($msj_up!=""){$msj_up.=", ";}$msj_up.="las observaciones";
											}
											if($detalle_anterior->posicion!=$detalle->posicion){
												$msj_aux="la posición de los productos.";
											}
										}else{}
									}
									$j_sucursales=json_decode(json_encode($detalle->sucursales));
									foreach($j_sucursales as $key => $sucursal){
										if(isset($sucursal->sdp)){//MODIFICAR SUCURSAL
											$sucursal_detalle_pedido=$this->lib->search_elemento($all_sucursal_detalle_pedido,"idsdp",$sucursal->sdp);
											$can_ant=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad);
											if($can_ant*1!=$sucursal->cantidad*1){
												$suc=$this->lib->search_elemento($all_sucursales,"idsc",$sucursal->sc);
												$sw=true;
												if($sucursal->cantidad*1>0){
													if($this->M_sucursal_detalle_pedido->modificar($sucursal->sdp,$sucursal->cantidad*1)){
														if($msj_up!=""){$msj_up.=", ";}$msj_up.="de ".($can_ant*1)."u. a ".($sucursal->cantidad*1)."u. en ".$suc->nombre_sucursal;
													}else{}
												}else{
													if($this->M_sucursal_detalle_pedido->eliminar($sucursal->sdp)){
														if($msj_up!=""){$msj_up.=", ";}$msj_up.="de ".($can_ant*1)."u. a 0u. en ".$suc->nombre_sucursal;
													}else{}
												}
											}
										}else{//INSERTAR SUCURSAL
											if($sucursal->cantidad*1>0){
												if($this->M_sucursal_detalle_pedido->insertar($sucursal->sc,$detalle->dp,$sucursal->cantidad*1)){
													$sw=true;
													$suc=$this->lib->search_elemento($all_sucursales,"idsc",$sucursal->sc);
													if($msj_up!=""){$msj_up.=", ";}$msj_up.="de 0u. a ".($sucursal->cantidad*1)."u. en ".$suc->nombre_sucursal;
												}else{}
											}
										}
									}//End foreach
									if($sw && $msj_up!=""){
										$cont_up++;
										$producto_grupo_color=$this->lib->search_elemento($all_productos,"idpgrc",$detalle->pgc);
										$nom=$producto_grupo_color->nombre;
										if($producto_grupo_color->abr_g!=""){
											$nom.="-".$producto_grupo_color->nombre_g;
										}
										$nom.="-".$producto_grupo_color->nombre_c;
										$msj_up_prod.="<br>- ".$nom.": ".$msj_up;
									}
								}else{//INSERTAR DETALLE
									$iddp=$this->M_detalle_pedido->max("iddp")+1;
									if($this->M_detalle_pedido->insertar($iddp,$parte_pedido[0]->idpp,$detalle->pgc,$detalle->posicion,$detalle->observacion,$detalle->cu,$detalle->cu*$detalle->cantidad)){
										$producto_grupo_color=$this->lib->search_elemento($all_productos,"idpgrc",$detalle->pgc);
										$nom=$producto_grupo_color->nombre;
										if($producto_grupo_color->abr_g!=""){
											$nom.="-".$producto_grupo_color->nombre_g;
										}
										$nom.="-".$producto_grupo_color->nombre_c;
										$cont_new++;
										if($msj_new!=""){$msj_new.=", ";}$msj_new.=$nom;
										$j_sucursales=json_decode(json_encode($detalle->sucursales));
										foreach($j_sucursales as $key => $sucursal){
											if($sucursal->cantidad*1>0){
												if($this->M_sucursal_detalle_pedido->insertar($sucursal->sc,$iddp,$sucursal->cantidad*1)){
												}else{}
											}
										}
									}else{}
								}
							}//end foreach
							if($msj_aux!=""){
								if($msj_parte!=""){$msj_parte.=", ";}
								$msj_parte.=$msj_aux;
							}
							/*Notificacion*/
							if($msj_parte!="" || $msj_up_prod!="" || $msj_new!="" || $msj_del!=""){
								$msj="<ins>Pedido <strong>N°".$pedido[0]->idpe." ".$pedido[0]->nombre.": ".$v[$parte_pedido[0]->tipo]." ".$parte_pedido[0]->numero."</strong></ins>";
								if($msj_parte!=""){$msj.="<br><b>Modificó</b>: ".$msj_parte;}
								if($msj_up_prod!=""){$msj.="<br><b>Modificó ".$cont_up." producto</b>";if($cont_up>0){$msj.="s";}$msj.=": ".$msj_up_prod.".";}
								if($msj_new!=""){$msj.="<br><b>Adicionó ".$cont_new." producto</b>";if($cont_new>0){$msj.="s";}$msj.=": ".$msj_new.".";}
								if($msj_del!=""){$msj.="}<br><b>Eliminó ".$cont_del." producto</b>";if($cont_del>0){$msj.="s";}$msj.=": ".$msj_del.".";}
								if($this->M_pedido_seg->insertar($pedido[0]->idpe,$idpp,NULL,"cpp",$msj)){
									$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
									foreach($resultados as $key=>$result){
										if($result->idno=="none"){
											if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
										}else{
											if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
										}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
								}else{
									echo "ok";
								}
							}else{
								echo "ok";
							}
							/*End Notificacion*/
						}else{
							echo "error";
						}
					}else{
						echo "failll";
					}
				}else{//estado de pedido terminado
					echo "ok";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}else{
		if(!empty($this->actualizaciones)){
			redirect(base_url().'update');
		}else{
			echo "logout";
		}
	}
}
	public function update_detalles_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['detalles']) && isset($_POST['pp']) && isset($_POST['status']) && isset($_POST['pos'])){
				$idpp=trim($_POST['pp']);
				$pos=trim($_POST['pos']);
				$parte_pedido=$this->M_parte_pedido->get($idpp);
				$status=json_decode($_POST['status']);//lista de productos con estado eliminado (0) o no (1)
				if(!empty($parte_pedido) && $this->val->entero($pos,0,10)){
					$pedido=$this->M_pedido->get($parte_pedido[0]->idpe);
					$all_productos=$this->M_producto->get_colores_all("","","");
					$detalles_pedido=$this->M_detalle_pedido->get_search("dp.idpp",$idpp);
					$all_sucursales=$this->M_sucursal_cliente->get_complet("sc.idcl",$pedido[0]->idcl);
					$all_sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_row(NULL,NULL);
					//sacando todos las sucursales productos de la parte
					$detalles=$this->lib->validate_colores_producto_pedido($_POST['detalles'],$all_productos,$detalles_pedido,$all_sucursales,$all_sucursal_detalle_pedido,$this->val);
					if($detalles->control){
						$msj="";
						/*Eliminando productos*/
						$cont_del=0;$msj_del="";
						foreach($status as $key => $eliminado){
							if($eliminado->status==0){
								$det_ped=$this->lib->search_elemento($detalles_pedido,"iddp",$eliminado->dp);
								if($det_ped!=NULL){
									$producto_grupo_color=$this->lib->search_elemento($all_productos,"idpgrc",$det_ped->idpgrc);
									if($producto_grupo_color!=null){
										if($this->M_detalle_pedido->eliminar($eliminado->dp)){
											$cont_del++;
											$nom=$producto_grupo_color->nombre;
											if($producto_grupo_color->abr_g!=""){$nom.="-".$producto_grupo_color->nombre_g;}
											$nom.="-".$producto_grupo_color->nombre_c;
											if($msj_del!=""){$msj_del.=",";}
											$msj_del.=$nom;
										}
									}
								}
							}
						}
						/*End eliminando productos*/
						$cont_up=0;$msj_up_prod="";$cont_new=0;$msj_new="";$msj_aux="";
						foreach($detalles->productos as $key => $detalle){
							$msj_up="";
							if(isset($detalle->dp)){//MODIFICAR DETALLE
								$detalle_anterior=$this->lib->search_elemento($detalles_pedido,"iddp",$detalle->dp);
								$cu_ant=$this->lib->desencriptar_num($detalle_anterior->cu)*1;
								$cv_ant=$this->lib->desencriptar_num($detalle_anterior->cv)*1;
								$sw=false;
								if($detalle_anterior->posicion!=$detalle->posicion || $detalle->cu*1!=$cu_ant*1 || $detalle_anterior->observacion!=$detalle->observacion || ($cv_ant*1)!=($detalle->cu*$detalle->cantidad)){
										if($this->M_detalle_pedido->modificar($detalle->dp,$detalle->posicion,$detalle->observacion,$detalle->cu,$detalle->cu*$detalle->cantidad)){
											$sw=true;
											if($cu_ant*1!=$detalle->cu*1){
												$msj_up="el costo unitario de venta de ".($cu_ant*1)."Bs. a ".($detalle->cu*1)."Bs.";
											}
											if($detalle_anterior->observacion!=$detalle->observacion){
												if($msj_up!=""){$msj_up.=", ";}$msj_up.="las observaciones";
											}
											if($detalle_anterior->posicion!=$detalle->posicion){
												$msj_aux="la posición de los productos.";
											}
										}else{}
								}
								$j_sucursales=json_decode(json_encode($detalle->sucursales));
								foreach($j_sucursales as $key => $sucursal){
									if(isset($sucursal->sdp)){//MODIFICAR SUCURSAL
										$sucursal_detalle_pedido=$this->lib->search_elemento($all_sucursal_detalle_pedido,"idsdp",$sucursal->sdp);
										$can_ant=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad);
										if($can_ant*1!=$sucursal->cantidad*1){
											$suc=$this->lib->search_elemento($all_sucursales,"idsc",$sucursal->sc);
											$sw=true;
											if($sucursal->cantidad*1>0){
													if($this->M_sucursal_detalle_pedido->modificar($sucursal->sdp,$sucursal->cantidad*1)){
														if($msj_up!=""){$msj_up.=", ";}$msj_up.="de ".($can_ant*1)."u. a ".($sucursal->cantidad*1)."u. en ".$suc->nombre_sucursal;
													}else{}
											}else{
													if($this->M_sucursal_detalle_pedido->eliminar($sucursal->sdp)){
														if($msj_up!=""){$msj_up.=", ";}$msj_up.="de ".($can_ant*1)."u. a 0u. en ".$suc->nombre_sucursal;
													}else{}
											}
										}
									}else{//INSERTAR SUCURSAL
										if($sucursal->cantidad*1>0){
												if($this->M_sucursal_detalle_pedido->insertar($sucursal->sc,$detalle->dp,$sucursal->cantidad*1)){
													$sw=true;
													$suc=$this->lib->search_elemento($all_sucursales,"idsc",$sucursal->sc);
													if($msj_up!=""){$msj_up.=", ";}$msj_up.="de 0u. a ".($sucursal->cantidad*1)."u. en ".$suc->nombre_sucursal;
												}else{}
										}
									}
								}//End foreach
								if($sw && $msj_up!=""){
									$cont_up++;
									$producto_grupo_color=$this->lib->search_elemento($all_productos,"idpgrc",$detalle->pgc);
									$nom=$producto_grupo_color->nombre;
									if($producto_grupo_color->abr_g!=""){
										$nom.="-".$producto_grupo_color->nombre_g;
									}
									$nom.="-".$producto_grupo_color->nombre_c;
									$msj_up_prod.="<br>- ".$nom.": ".$msj_up;
								}
							}else{//INSERTAR DETALLE
									$iddp=$this->M_detalle_pedido->max("iddp")+1;
									if($this->M_detalle_pedido->insertar($iddp,$parte_pedido[0]->idpp,$detalle->pgc,$detalle->posicion,$detalle->observacion,$detalle->cu,$detalle->cu*$detalle->cantidad)){
										$producto_grupo_color=$this->lib->search_elemento($all_productos,"idpgrc",$detalle->pgc);
										$nom=$producto_grupo_color->nombre;
										if($producto_grupo_color->abr_g!=""){
											$nom.="-".$producto_grupo_color->nombre_g;
										}
										$nom.="-".$producto_grupo_color->nombre_c;
										$cont_new++;
										if($msj_new!=""){$msj_new.=", ";}$msj_new.=$nom;
										$j_sucursales=json_decode(json_encode($detalle->sucursales));
										foreach($j_sucursales as $key => $sucursal){
											if($sucursal->cantidad*1>0){
												if($this->M_sucursal_detalle_pedido->insertar($sucursal->sc,$iddp,$sucursal->cantidad*1)){
												}else{}
											}
										}
									}else{}
							}
						}//end foreach
						$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
						/*Notificacion*/
						if($msj_up_prod!="" || $msj_new!="" || $msj_del!=""){
							$msj="<ins>Pedido <strong>N°".$pedido[0]->idpe." ".$pedido[0]->nombre.": ".$v[$parte_pedido[0]->tipo]." ".$parte_pedido[0]->numero."</strong></ins>";
							if($msj_up_prod!=""){$msj.="<br><b>Modificó ".$cont_up." producto</b>";if($cont_up>0){$msj.="s";}$msj.=": ".$msj_up_prod.".";}
							if($msj_new!=""){$msj.="<br><b>Adicionó ".$cont_new." producto</b>";if($cont_new>0){$msj.="s";}$msj.=": ".$msj_new.".";}
							if($msj_del!=""){$msj.="}<br><b>Eliminó ".$cont_del." producto</b>";if($cont_del>0){$msj.="s";}$msj.=": ".$msj_del.".";}
							if($this->M_pedido_seg->insertar($pedido[0]->idpe,$idpp,NULL,"cpp",$msj)){
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
								foreach($resultados as $key=>$result){
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "ok";
							}
						}else{
							echo "ok";
						}
						/*End Notificacion*/
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function tr_producto_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['dp'])){
				
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
public function new_parte_pedido(){
	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   		$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   		if(!empty($privilegio)){
   			if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
				if(isset($_POST['pe'])){
					$idpe=trim($_POST['pe']);
					$pedido=$this->M_pedido->get($idpe);
					if(!empty($pedido)){
						$cliente=$this->M_cliente->get($pedido[0]->idcl);
						$listado['cliente']=$cliente[0];
						$listado['pedido']=$pedido[0];
						$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
						$listado['partes']=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
						$this->load->view('movimiento/pedidos/6-config/partes/new',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
   			}else{
   				echo "No permitido";
   			}
   		}else{
   			echo "No permitido";
   		}
	}else{
		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	}
}
public function save_parte_pedido(){
	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		if(isset($_POST['detalles']) && isset($_POST['pe']) && isset($_POST['tipo']) && isset($_POST['fec']) && isset($_POST['obs']) && isset($_POST['cl'])){
			$idpe=trim($_POST['pe']);
			$tipo=trim($_POST['tipo']);
			$fec=trim($_POST['fec']);
			$obs=trim($_POST['obs']);
			$idcl=trim($_POST['cl']);
			$pedido=$this->M_pedido->get($idpe);
			$cliente=$this->M_cliente->get($idcl);
			if(!empty($pedido) && !empty($cliente) && ($tipo=="0" || $tipo=="1" || $tipo=="2") && $this->val->fecha($fec) && $this->val->textarea($obs,0,300)){
				$all_productos=$this->M_producto->get_colores("","","");
				$detalles_pedido=NULL;
				$all_sucursales=$this->M_sucursal_cliente->get_complet("sc.idcl",$idcl);
				$all_sucursal_detalle_pedido=NULL;
				$detalles=$this->lib->validate_colores_producto_pedido($_POST['detalles'],$all_productos,$detalles_pedido,$all_sucursales,$all_sucursal_detalle_pedido,$this->val);
				if($detalles->control){
					$numero=$this->M_parte_pedido->max_where("numero","idpe = ".$idpe." AND tipo = ".$tipo)+1;
					$idpp=$this->M_parte_pedido->max("idpp")+1;
					if($this->M_parte_pedido->insertar($idpp,$idpe,$numero,$fec,$tipo,$obs)){
								foreach($detalles->productos as $key => $detalle){
									$iddp=$this->M_detalle_pedido->max("iddp")+1;
									$costo_venta=$detalle->cu*$detalle->cantidad;
									if($detalle->cantidad*1>0){
										if($this->M_detalle_pedido->insertar($iddp,$idpp,$detalle->pgc,$detalle->posicion,$detalle->observacion,$detalle->cu,$costo_venta)){
										$sucursales=json_decode(json_encode($detalle->sucursales));
										foreach($sucursales as $key => $sucursal){
											if($sucursal->cantidad*1>0){
												if($this->M_sucursal_detalle_pedido->insertar($sucursal->sc,$iddp,$sucursal->cantidad)){}
											}else{/*echo "error";*/}
										}
									}else{/*echo "error";*/}
									}else{/*echo "fail";*/}
								}//end foreach
						/*Notificacion*/
						$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
						$msj="Pedido <strong>N°".$pedido[0]->idpe." ".$pedido[0]->nombre.":</strong>: Adicionó <strong>".$v[$tipo]." ".$numero."</strong> en el pedido.";
						if($this->M_pedido_seg->insertar($pedido[0]->idpe,$idpp,NULL,"cpp",$msj)){
							$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
							foreach ($resultados as $key => $result) {
								if($result->idno=="none"){
									if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
								}else{
									if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
								}
							}
							$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
							echo json_encode($result);
						}else{
							echo "ok";
						}
						/*End Notificacion*/
					}else{/*echo "error";*/}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}else{
		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	}
}
public function confirmar_parte_pedido(){
	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		if(isset($_POST['pp'])){
			$idpp=$_POST['pp'];
			$parte_pedido=$this->M_parte_pedido->get($idpp);
			if(!empty($parte_pedido)){
				$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
				$listado['open_control']="false";
				$listado['control']="";
				$listado['titulo']="eliminar el sub pedido <strong>".$v[$parte_pedido[0]->tipo]." ".$parte_pedido[0]->numero."</strong> del pedido";
				$listado['desc']="Se eliminara definitivamente el sub pedido";
				$this->load->view('estructura/form_eliminar',$listado);
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}else{
		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	}
}
public function drop_parte_pedido(){
	if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		if(isset($_POST['pp'])){
			$idpp=$_POST['pp'];
			$parte_pedido=$this->M_parte_pedido->get($idpp);
			if(!empty($parte_pedido)){
				$pedido=$this->M_pedido->get($parte_pedido[0]->idpe);
				if($this->M_parte_pedido->eliminar($idpp)){
					$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
						//para notificaciones
						if($this->M_pedido_seg->eliminar_row("idpp",$idpp)){
							$msj="Pedido <strong>N°".$pedido[0]->idpe." ".$pedido[0]->nombre.": ".$v[$parte_pedido[0]->tipo]." ".$parte_pedido[0]->numero."</strong>Eliminó el sub pedido <strong>".$v[$parte_pedido[0]->tipo]." ".$parte_pedido[0]->numero."</strong> del pedido.";
							if($this->M_pedido_seg->insertar($pedido[0]->idpe,NULL,NULL,"dpp",$msj)){
								$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
								foreach ($resultados as $key => $result) {
									if($result->idno=="none"){
										if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
									}else{
										if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
									}
								}
								$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
								echo json_encode($result);
							}else{
								echo "ok";
							}
						}else{
							echo "ok";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
		/*-- Pagos --*/
		public function pagos(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
   				if(!empty($privilegio)){
   					if($privilegio[0]->mo==1 && $privilegio[0]->mo1r==1 && $privilegio[0]->mo1u==1){
						if(isset($_POST['pe'])){
							$idpe=trim($_POST['pe']);
							$pedido=$this->M_pedido->get($idpe);
							if(!empty($pedido)){
								$listado['pedido']=$pedido[0];
								//$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$cliente[0]->idcl);
								$listado['partes']=$this->M_parte_pedido->get_row('idpe',$pedido[0]->idpe);
								$auxPagos=$this->M_pago->get_search("p.idpe",$pedido[0]->idpe,"fecha","asc");
                                $pagos = [];
								foreach ($auxPagos as $pago) {
								    $pago->fecha = $this->lib->desencriptar_str($pago->fecha);
								    $pagos[] = $pago;
                                }
                                usort($pagos, array($this->lib, 'ordenarPorFecha'));
                                $listado['pagos'] = $pagos;
								$listado['personas']=$this->M_persona->get_all();
								$listado['bancos']=$this->M_banco->get_all();
								$listado['detalles_pedido']=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido[0]->idpe);
								$listado['descuentos_pagos']=$this->M_descuento_pago->get_all();
								$listado['tipos_cambio']=$this->M_tipo_cambio->get_all();
								/*para descuento*/ 
								$partes=$this->M_parte_pedido->get_row("idpe",$idpe);
								$detalles_partes_pedido=$this->M_parte_pedido->get_detalle("idpe",$idpe);
								$productos=$this->M_producto->get_search(NULL,NULL,NULL);
								$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
								$productos_imagenes=$this->M_producto_imagen->get_all();
								$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
								$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
								$order_by="codigo";//codigo o posicion
								$descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
								$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
								$usuarios=$this->M_usuario->get_search(NULL,NULL);
								$v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
								$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
								/*end para descuento*/ 
								$descuentos_producto=json_decode(json_encode($v_productos));
								$total_descuento=0;
								foreach ($descuentos_producto as $key => $descuento) {
									$total_descuento+=($descuento->descuento*1);
								}
								$listado['descuento']=$total_descuento;
								$this->load->view('movimiento/pedidos/6-config/pagos/view',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
   					}else{
   						echo "No permitido";
   					}
   				}else{
   					echo "No permitido";
   				}
			}else{
				if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
			}
		}
		public function new_pago(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pe']) && isset($_POST['max'])){
					$pedido=$this->M_pedido->get(trim($_POST['pe']));
					if(!empty($pedido)){
						$listado['pedido']=$pedido[0];
						$listado['max']=trim($_POST['max']);
						$listado['personas']=$this->M_persona->get_all();
						$listado['empleados']=$this->M_empleado->get_all();
						$listado['usuarios']=$this->M_usuario->get_all();
						$listado['directivos']=$this->M_directivo->get_all();
						$listado['tipos']=$this->M_tipo_cambio->get_all();
						$this->load->view('movimiento/pedidos/6-config/pagos/new',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
			/*-Personas-*/
				public function view_personas(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						$listado['personas']=$this->M_persona->get_all();
						$listado['empleados']=$this->M_empleado->get_all();
						$listado['usuarios']=$this->M_usuario->get_all();
						$listado['directivos']=$this->M_directivo->get_all();
						$this->load->view('movimiento/pedidos/6-config/pagos/personas/view',$listado);
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function options_bancos(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['ci'])){
							$persona=$this->M_persona->get(trim($_POST['ci']));
							$resp="<option value=''>Seleccionar...</option>";
							if(!empty($persona)){
								$bancos=$this->M_banco->get_all();
								$bancos_persona=$this->M_banco_persona->get_search("bp.ci",trim($_POST['ci']));
								echo "count:".count($bancos_persona)." ".$_POST['ci'];
								for($i=0; $i < count($bancos); $i++){ $banco=$bancos[$i];
									$selected=false;
									if(isset($_POST['selected_banco'])){
										if($banco->idba==trim($_POST['selected_banco'])){
											$selected=true;
										}
									}else{
										if(!empty($bancos_persona)){
											if($banco->idba==$bancos_persona[0]->idba){
												$selected=true;
											}
										}
									}
									if($selected){
										$selected="SELECTED='SELECTED'";
									}else{
										$selected="";
									}
									$resp.="<option value='".$banco->idba."' ".$selected.">".$banco->razon."</option>";
								}
								echo $resp;
							}else{
								echo $resp;
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function options_cuentas(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['ci'])){
							$persona=$this->M_persona->get(trim($_POST['ci']));
							$resp="<option value=''>Seleccionar...</option>";
							if(!empty($persona)){
								$idba="";
								if(isset($_POST["ba"])){
									$idba=trim($_POST['ba']);
								}else{//obtenemos en primer banco
									$bancos_persona=$this->M_banco_persona->get_search("bp.ci",trim($_POST['ci']));
									if(!empty($bancos_persona)){
										$idba=$bancos_persona[0]->idba;
									}
								}
								if($idba!=""){
									$cuentas=$this->M_banco_persona->get_search_2n("bp.ci",trim($_POST['ci']),"b.idba",$idba);
									$sw=0;
									for($i=0; $i < count($cuentas); $i++){ $cuenta=$cuentas[$i];
										if($cuenta->estado=="1"){
											$selected="";
											if($sw==0){$selected="SELECTED='SELECTED'";$sw+=1;}
											$resp.="<option value='".$cuenta->idbp."' ".$selected.">".$cuenta->cuenta."</option>";
										}
									}
									
								}
								echo $resp;
							}else{
								echo $resp;
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
			/*-End personas-*/
			/*-Bancos-*/
				public function view_bancos(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						$listado['bancos']=$this->M_banco->get_all();
						$this->load->view('movimiento/pedidos/6-config/pagos/bancos/view',$listado);
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function new_banco(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['type'])){
							$listado['type']=trim($_POST['type']);
							$this->load->view('movimiento/pedidos/6-config/pagos/bancos/new',$listado);
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function save_banco(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_FILES) && isset($_POST['raz']) && isset($_POST['url']) && isset($_POST['des'])){
							$raz=trim($_POST['raz']);
							$url=trim($_POST['url']);
							$des=trim($_POST['des']);
							if($this->val->strSpace($raz,2,200) && $this->val->textarea($des,0,500)){
								$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/bancos/','',$this->resize,rand(100,99999));
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_banco->insertar($img,$raz,$url,$des)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function config_banco(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['ba'])){
							$idba=trim($_POST['ba']);
							$banco=$this->M_banco->get($idba);
							if(!empty($banco)){
								$listado['banco']=$banco[0];
								$this->load->view('movimiento/pedidos/6-config/pagos/bancos/update',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function update_banco(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
						if(!empty($privilegio)){
							if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
								if(isset($_FILES) && isset($_POST['ba']) && isset($_POST['raz']) && isset($_POST['url']) && isset($_POST['des'])){
									$raz=trim($_POST['raz']);
									$url=trim($_POST['url']);
									$des=trim($_POST['des']);
									$idba=trim($_POST['ba']);
									$banco=$this->M_banco->get($idba);
									if(!empty($banco) && $this->val->strSpace($raz,2,200) && $this->val->textarea($des,0,500)){
										$img=$banco[0]->fotografia;
										$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/bancos/','',$this->resize,$img,$idba);
										if($img!='error' && $img!="error_type" && $img!="error_size_img"){
											if($this->M_banco->modificar($idba,$img,$raz,$url,$des)){
												echo "ok";
											}else{
												echo "error";
											}
										}else{
											echo $img;
										}
									}else{
										echo "fail";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "bloqueado";
							}
						}else{
							echo "bloqueado";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function confirm_banco(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['ba'])){
							$idba=trim($_POST['ba']);
							$banco=$this->M_banco->get($idba);
							if(!empty($banco)){
								$url='./libraries/img/';
								$img='sistema/miniatura/default.jpg';
								if($banco[0]->fotografia!=NULL && $banco[0]->fotografia!=""){$img="bancos/miniatura/".$banco[0]->fotografia;}
								$control=$this->M_banco_persona->get_row("idba",$banco[0]->idba);
								if(empty($control)){
									$listado['titulo']="eliminar el banco <strong>".$banco[0]->razon."</strong> del sistema";
									$listado['desc']="Se eliminara definitivamente el banco del sistema.";
									$listado['open_control']="false";
									$listado['control']="";
								}else{
									$listado['desc']="Imposible eliminar el banco <strong>".$proceso[0]->nombre."</strong>, se encuentra asignado a ".count($control)." persona(s)";
									$listado['open_control']="false";
									$listado['control']="";
									$listado['btn_ok']="none";
								}
								$this->load->view('estructura/form_eliminar',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
			   	public function drop_banco(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['ba'])){
							$idba=$_POST['ba'];
							$banco=$this->M_banco->get($idba);
							if(!empty($banco)){
								if($this->lib->eliminar_imagen($banco[0]->fotografia,'./libraries/img/bancos/')){
									if($this->M_banco->eliminar($idba)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}			
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
			/*-End Bancos-*/
			/*- Cuentas -*/
			public function new_cuenta(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['ci']) && isset($_POST['ba'])){
						$ci=trim($_POST['ci']);
						$idba=trim($_POST['ba']);
						$persona=$this->M_persona->get($ci);
						$banco=$this->M_banco->get($idba);
						if(!empty($persona) && !empty($banco)){
							$listado['persona']=$persona[0];
							$listado['banco']=$banco[0];
							$listado['personas']=$this->M_persona->get_all();
							$listado['bancos']=$this->M_banco->get_all();
							$this->load->view('movimiento/pedidos/6-config/pagos/cuentas/new',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function save_cuenta(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['cta']) && isset($_POST['ci']) && isset($_POST['ba'])){
						$cta=trim($_POST['cta']);
						$ci=trim($_POST['ci']);
						$idba=trim($_POST['ba']);
						$persona=$this->M_persona->get($ci);
						$banco=$this->M_banco->get($idba);
						if(!empty($persona) && !empty($banco)){
							if($this->val->strNoSpace($cta,5,100)){
								if($this->M_banco_persona->insertar($idba,$ci,$cta,1,1)){
									echo "ok";

								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			/*- End cuentas -*/
			/*- Tipo de cuenta -*/
				public function view_tipos(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						$listado['tipos']=$this->M_tipo_cambio->get_all();
						$this->load->view('movimiento/pedidos/6-config/pagos/tipo_cambio/view',$listado);
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function new_tipo_cambio(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['type'])){
							$listado['type']=trim($_POST['type']);
							$this->load->view('movimiento/pedidos/6-config/pagos/tipo_cambio/new',$listado);
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function save_tipo_cambio(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['mon'])){
							$nom=trim($_POST['nom']);
							$abr=trim($_POST['abr']);
							$mon=trim($_POST['mon']);
							if($this->val->strSpace($nom,2,100) && strpos($abr," ") === false && $this->val->decimal($mon,6,2) && $mon>0 && $mon<=9999.99){
								if($this->M_tipo_cambio->insertar($nom,$abr,0,$mon)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function options_tipos_moneda(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						$tipos=$this->M_tipo_cambio->get_all();
						$resp="<option value=''>Seleccionar...</option>";
						for($i=0; $i < count($tipos); $i++){$tipo_cambio=$tipos[$i];
							if($tipo_cambio->tipo==0){
								$selected="";
								if(isset($_POST['selected'])){
									if($tipo_cambio->idtc==trim($_POST['selected'])){
										$selected="selected='selected'";
									}
								}
								$resp.="<option value='".$tipo_cambio->idtc."' ".$selected.">".$tipo_cambio->moneda."(".$tipo_cambio->monto.")"."</option>";
							}
						}
						echo $resp;
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function config_tipo_cambio(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['tc'])){
							$idtc=trim($_POST['tc']);
							$tipo=$this->M_tipo_cambio->get($idtc);
							if(!empty($tipo)){
								$listado['tipo']=$tipo[0];
								$this->load->view('movimiento/pedidos/6-config/pagos/tipo_cambio/update',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function update_tipo_cambio(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['tc']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['mon'])){
							$idtc=trim($_POST['tc']);
							$nom=trim($_POST['nom']);
							$abr=trim($_POST['abr']);
							$mon=trim($_POST['mon']);
							$tipo_cambio=$this->M_tipo_cambio->get($idtc);
							if(!empty($tipo_cambio) && $this->val->strSpace($nom,2,100) && strpos($abr," ") === false && $this->val->decimal($mon,6,2) && $mon>0 && $mon<=9999.99){
								if($tipo_cambio[0]->tipo=="0"){// solo modifica los registros de tipo COMPRA
									if($this->M_tipo_cambio->modificar($idtc,$nom,$abr,0,$mon)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "ok";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
				public function confirm_tipo_cambio(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['tc'])){
							$idtc=trim($_POST['tc']);
							$tipo_cambio=$this->M_tipo_cambio->get($idtc);
							if(!empty($tipo_cambio)){
									$listado['titulo']="eliminar el tipo de cambio <strong>".$tipo_cambio[0]->moneda."</strong> del sistema";
									$listado['desc']="Se eliminara definitivamente el tipo de cambio del sistema.";
									$listado['open_control']="false";
									$listado['control']="";
								$this->load->view('estructura/form_eliminar',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
			   	public function drop_tipo_cambio(){
					if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['tc'])){
							$idtc=trim($_POST['tc']);
							$tipo_cambio=$this->M_tipo_cambio->get($idtc);
							if(!empty($tipo_cambio)){
								if($this->M_tipo_cambio->eliminar($idtc)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}			
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
				}
			/*- End tipo de cuenta -*/
			/*- Moneda -*/
			public function abreviatura_tipo_cuenta(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
						if(isset($_POST['tc'])){
							$idtc=trim($_POST['tc']);
							$tipo_cambio=$this->M_tipo_cambio->get($idtc);
							if(!empty($tipo_cambio)){
								echo $tipo_cambio[0]->simbolo;
							}else{
								echo "";
							}
						}else{
							echo "fail";
						}
					}else{
						if(!empty($this->actualizaciones)){
							redirect(base_url().'update');
						}else{
							echo "logout";
						}
					}
			}
			public function moneda_nacional(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['tc']) && isset($_POST['monto'])){
						$idtc=trim($_POST['tc']);
						$monto=trim($_POST['monto']);
						$tipo_cambio=$this->M_tipo_cambio->get($idtc);
						if(!empty($tipo_cambio) && $this->val->decimal($monto,10,2) && $monto>0 && $monto<999999999.99){
							echo number_format(number_format(($monto*$tipo_cambio[0]->monto),1,'.',''),2,'.','');
						}else{
							echo "";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			/*- End moneda -*/
		public function save_pago(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['fec']) && isset($_POST['dep']) && isset($_POST['ci']) && isset($_POST['idba']) && isset($_POST['idbp']) && isset($_POST['idtc']) && isset($_POST['mon']) && isset($_POST['des']) && isset($_POST['pe']) && isset($_POST['max'])){
						$fec=trim($_POST['fec']);
						$dep=trim($_POST['dep']);
						$ci=trim($_POST['ci']);
						$idba=trim($_POST['idba']);
						$idbp=trim($_POST['idbp']);
						$idtc=trim($_POST['idtc']);
						$mon=trim($_POST['mon']);
						$des=trim($_POST['des']);
						$idpe=trim($_POST['pe']);
						$max=trim($_POST['max']);
						$banco_persona=$this->M_banco_persona->get($idbp);
						$persona=$this->M_persona->get($ci);
						$tipo_cambio=$this->M_tipo_cambio->get($idtc);
						$pedido=$this->M_pedido->get($idpe);
						if(!empty($persona) && !empty($banco_persona) && !empty($tipo_cambio) && !empty($pedido) && $this->val->fecha($fec) && $this->val->strSpace($dep,2,500) && $this->val->decimal($mon,10,2) && $mon>0 && $mon<=999999999.99 && $this->val->textarea($des,0,900)){
							$monto_bs=number_format(($mon*$tipo_cambio[0]->monto),1,'.','');
							if($banco_persona[0]->idba==$idba && $banco_persona[0]->ci==$ci && $monto_bs>0 && $monto_bs<=($max*1)){
								if($this->M_pago->insertar($idpe,$idbp,$this->session->userdata('id'),$idtc,$dep,$fec,$mon,$monto_bs,$tipo_cambio[0]->monto,$des)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function config_pago(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if(!empty($privilegio)){
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						if(isset($_POST['pa']) && isset($_POST['max'])){
							$max=trim($_POST['max']);
							$idpa=trim($_POST['pa']);
							$pago=$this->M_pago->get_search("p.idpa",$idpa,"fecha","asc");
							if(!empty($pago)){
								$listado['max']=trim($_POST['max']);
								$listado['personas']=$this->M_persona->get_all();
								$listado['empleados']=$this->M_empleado->get_all();
								$listado['usuarios']=$this->M_usuario->get_all();
								$listado['directivos']=$this->M_directivo->get_all();
								$listado['tipos']=$this->M_tipo_cambio->get_all();
								$listado['pago']=$pago[0];
								$listado['bancos']=$this->M_banco->get_all();
								$listado['cuentas']=$this->M_banco_persona->get_search_2n("b.idba",$pago[0]->idba,"bp.ci",$pago[0]->ci_persona);
								$this->load->view('movimiento/pedidos/6-config/pagos/config',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function update_pago(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['fec']) && isset($_POST['dep']) && isset($_POST['ci']) && isset($_POST['idba']) && isset($_POST['idbp']) && isset($_POST['idtc']) && isset($_POST['mon']) && isset($_POST['des']) && isset($_POST['pa']) && isset($_POST['max'])){
						$fec=trim($_POST['fec']);
						$dep=trim($_POST['dep']);
						$ci=trim($_POST['ci']);
						$idba=trim($_POST['idba']);
						$idbp=trim($_POST['idbp']);
						$idtc=trim($_POST['idtc']);
						$mon=trim($_POST['mon']);
						$des=trim($_POST['des']);
						$idpa=trim($_POST['pa']);
						$max=trim($_POST['max']);
						$banco_persona=$this->M_banco_persona->get($idbp);
						$persona=$this->M_persona->get($ci);
						$tipo_cambio=$this->M_tipo_cambio->get($idtc);
						$pago=$this->M_pago->get($idpa);
						if(!empty($persona) && !empty($banco_persona) && !empty($tipo_cambio) && !empty($pago) && $this->val->fecha($fec) && $this->val->decimal($mon,10,2) && $mon>0 && $mon<=999999999.99 && $this->val->textarea($des,0,900)){
							if($this->session->userdata("id")==$pago[0]->idus){
								$monto_bs=number_format(($mon*$tipo_cambio[0]->monto),1,'.','');
								if($banco_persona[0]->idba==$idba && $banco_persona[0]->ci==$ci && $monto_bs>0 && $monto_bs<=($max*1)){
									if($this->M_pago->modificar($idpa,$idbp,$this->session->userdata('id'),$idtc,$dep,$fec,$mon,$monto_bs,$tipo_cambio[0]->monto,$des)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "permiso_bloqueado";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";	
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function confirm_pago(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pa'])){
					$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						$idpa=$_POST['pa'];
						$pago=$this->M_pago->get($idpa);
						if(!empty($pago)){
							$desc="Se eliminara definitivamente el depósito";
							$listado['titulo']="eliminar el depósito definitivamente";
							$listado['desc']=$desc;
							$listado['open_control']="false";
							$listado['control']="";
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function drop_pago(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['pa'])){
					$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						$idpa=$_POST['pa'];
						$pago=$this->M_pago->get($idpa);
						if(!empty($pago)){
							if($this->M_pago->eliminar($idpa)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
			/*--- Descuentos ---*/
			public function view_descuentos(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['pa']) && isset($_POST['pe'])){
						$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
						if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
							$idpa=$_POST['pa'];
							$idpe=$_POST['pe'];
							$pago=$this->M_pago->get($idpa);
							$pedido=$this->M_pedido->get($idpe);
							if(!empty($pago) && !empty($pedido)){
								$listado['descuentos']=$this->M_descuento_pago->get_search("idpa",$pago[0]->idpa);
								$listado['usuarios']=$this->M_usuario->get_persona(NULL);
								$listado['pago']=$pago[0];
								$listado['tipos_cambio']=$this->M_tipo_cambio->get_all();
								$this->load->view('movimiento/pedidos/6-config/pagos/descuentos/view',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function new_descuento(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						if(isset($_POST['pa'])){
							$idpa=$_POST['pa'];
							$pago=$this->M_pago->get($idpa);
							if(!empty($pago)){
								$listado['pago']=$pago[0];
								$listado['tipos_cambio']=$this->M_tipo_cambio->get_all();
								$this->load->view('movimiento/pedidos/6-config/pagos/descuentos/new',$listado);
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function save_descuento(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						if(isset($_POST['pa']) && isset($_POST['mon']) && isset($_POST['obs'])){
							$idpa=$_POST['pa'];
							$mon=$_POST['mon'];
							$obs=$_POST['obs'];
							$pago=$this->M_pago->get($idpa);
							if(!empty($pago) && $this->val->decimal($mon,8,1) && $mon>0 && $mon<9999999.9 && $this->val->textarea($obs,0,500)){
								if($this->M_descuento_pago->insertar($idpa,$this->session->userdata('id'),$mon,$obs)){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function config_descuento(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						if(isset($_POST['dpa']) && isset($_POST['pa'])){
							$iddpa=$_POST['dpa'];
							$idpa=$_POST['pa'];
							$pago=$this->M_pago->get($idpa);
							$descuento_pago=$this->M_descuento_pago->get($iddpa);
							if(!empty($pago) && !empty($descuento_pago)){
								if($descuento_pago[0]->idus==$this->session->userdata("id")){
									$listado['pago']=$pago[0];
									$listado['descuento_pago']=$descuento_pago[0];
									$listado['tipos_cambio']=$this->M_tipo_cambio->get_all();
									$this->load->view('movimiento/pedidos/6-config/pagos/descuentos/change',$listado);
								}else{
									echo "permiso_bloqueado";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function update_descuento(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
					if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
						if(isset($_POST['dpa']) && isset($_POST['mon']) && isset($_POST['obs'])){
							$iddpa=$_POST['dpa'];
							$mon=$_POST['mon'];
							$obs=$_POST['obs'];
							$descuento_pago=$this->M_descuento_pago->get($iddpa);
							if(!empty($descuento_pago) && $this->val->decimal($mon,8,1) && $mon>0 && $mon<9999999.9 && $this->val->textarea($obs,0,500)){
								if($descuento_pago[0]->idus==$this->session->userdata("id")){
									if($this->M_descuento_pago->modificar($iddpa,$mon,$obs)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "permiso_bloqueado";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function confirm_descuento(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['dpa'])){
						$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
						if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
							$iddpa=$_POST['dpa'];
							$descuento_pago=$this->M_descuento_pago->get($iddpa);
							if(!empty($descuento_pago)){
								if($descuento_pago[0]->idus==$this->session->userdata("id")){
									$desc="Se eliminara definitivamente el descuento";
									$listado['titulo']="eliminar el descuento definitivamente";
									$listado['desc']=$desc;
									$listado['open_control']="false";
									$listado['control']="";
									$this->load->view('estructura/form_eliminar',$listado);
								}else{
									echo "permiso_bloqueado";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			public function drop_descuento(){
				if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
					if(isset($_POST['dpa'])){
						$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
						if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
							$iddpa=$_POST['dpa'];
							$descuento_pago=$this->M_descuento_pago->get($iddpa);
							if(!empty($descuento_pago)){
								if($descuento_pago[0]->idus==$this->session->userdata("id")){
									if($this->M_descuento_pago->eliminar($iddpa)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "permiso_bloqueado";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "permiso_bloqueado";
						}
					}else{
						echo "fail";
					}
				}else{
					if(!empty($this->actualizaciones)){
						redirect(base_url().'update');
					}else{
						echo "logout";
					}
				}
			}
			/*--- End descuentos ---*/
		/*-- End pagos --*/
		/*-- Descuentos de productos en pedido --*/
		public function descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['pe'])){
						$idpe=trim($_POST['pe']);
						$pedido=$this->M_pedido->get($idpe);
						if(!empty($pedido)){
							$partes=$this->M_parte_pedido->get_row("idpe",$idpe);
							$detalles_partes_pedido=$this->M_parte_pedido->get_detalle("idpe",$idpe);
							$productos=$this->M_producto->get_search(NULL,NULL,NULL);
							$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
							$productos_imagenes=$this->M_producto_imagen->get_all();
							$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
							$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
							$order_by="codigo";//codigo o posicion
							$descuentos=$this->M_descuento_producto->get_search("idpe",$idpe);
							$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
							$usuarios=$this->M_usuario->get_search(NULL,NULL);
							$v_productos=$this->lib->descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
							$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios);
							$detalles_descuentos_productos=$this->M_detalle_descuento_producto->get_all();
							$listado['partes']=$partes;
							$listado['pedido']=$pedido[0];
							$listado['descuentos']=json_decode(json_encode($v_productos));
							$listado['detalles_descuentos_productos']=$detalles_descuentos_productos;
							$listado['privilegio']=$privilegio[0];
							$this->load->view('movimiento/pedidos/6-config/descuentos/view',$listado);
							
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function new_descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['pe'])){
						$idpe=trim($_POST['pe']);
						$pedido=$this->M_pedido->get($idpe);
						if(!empty($pedido)){
							$partes_pedido=$this->M_parte_pedido->get_row("idpe",$idpe);
							if(!empty($partes_pedido)){
								if(isset($_POST['pp'])){
									$idpp=trim($_POST['pp']);
								}else{
									$idpp=$partes_pedido[0]->idpp;
								}
								$parte_pedido=$this->lib->search_elemento($partes_pedido,'idpp',$idpp);
								if($parte_pedido!=null){
									$productos=$this->M_producto->get_search(NULL,NULL,NULL);
							   		$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
							   		$productos_imagenes=$this->M_producto_imagen->get_all();
							   		$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
							   		$detalles_pedidos=$this->M_detalle_pedido->get_search("idpp",$parte_pedido->idpp);
							   		$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
							   		$order_by="posicion";//codigo o posicion
							   		$listado['productos']=$this->lib->productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);	
									$listado['sucursales']=$this->M_sucursal_cliente->get_row('idcl',$pedido[0]->idcl);
							   		$listado['pedido']=$pedido[0];
									$listado['partes_pedido']=$partes_pedido;
									$listado['parte_pedido']=$parte_pedido;
									//para calucular la cantidd maxima asignada
									$listado['descuentos_productos']=$this->M_descuento_producto->get_search("idpe",$idpe);
									$this->load->view('movimiento/pedidos/6-config/descuentos/new/view',$listado);
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}							
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function save_descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['pe']) && isset($_POST['sdp']) && isset($_POST['cantidad']) && isset($_POST['porcentaje']) && isset($_POST['obs'])){
						$idpe=trim($_POST['pe']);
						$idsdp=trim($_POST['sdp']);
						$cantidad=trim($_POST['cantidad']);
						$porcentaje=trim($_POST['porcentaje']);
						$observaciones=trim($_POST['obs']);
						$pedido=$this->M_pedido->get($idpe);
						$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_row(NULL,NULL);
						$sucursal_detalle_pedido=$this->lib->search_elemento($sucursales_detalles_pedidos,"idsdp",$idsdp);
						if(!empty($pedido) && $sucursal_detalle_pedido!=null && $this->val->decimal($porcentaje,3,1) && $porcentaje>0 && $porcentaje<=100 && $this->val->textarea($observaciones,0,900)){							
							$descuentos_productos=$this->M_descuento_producto->get_search("idsdp",$idsdp);
							$cantidad_descontada=0;
							for($i=0;$i<count($descuentos_productos);$i++){$descuento=$descuentos_productos[$i];
								$suc_cantidad=$this->lib->desencriptar_num($descuento->cantidad)*1;
								$cantidad_descontada+=$suc_cantidad;
							}
							$cantidad_detalle=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1;
							$cantidad_maxima=$cantidad_detalle-$cantidad_descontada;
							//echo "OBS ".$observaciones;
							if($cantidad*1<=$cantidad_maxima){
								$porcentaje/=100;
								if($this->M_descuento_producto->insertar($idsdp,$idpe,$this->session->userdata("id"),$cantidad,$porcentaje,$observaciones)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function config_descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['des'])){
						$iddes=trim($_POST['des']);
						$descuento_producto=$this->M_descuento_producto->get_search("iddes",$iddes);
						if(!empty($descuento_producto)){
							$pedido=$this->M_pedido->get($descuento_producto[0]->idpe);
							$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
							$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("idsdp",$descuento_producto[0]->idsdp);
							$sucursal=$this->lib->search_elemento($sucursales_cliente,'idsc',$sucursal_detalle_pedido[0]->idsc);
							if($sucursal!=null){
								$parte_pedido=$this->M_parte_pedido->get_detalle('dp.iddp',$sucursal_detalle_pedido[0]->iddp);
								$color=$this->M_producto_grupo->get_grupo_colores("pgc.idpgrc",$parte_pedido[0]->idpgrc);
								if(!empty($color)){
									$producto=$this->M_producto->get($color[0]->idp);
									$productos_imagenes=$this->M_producto_imagen->get_all();
									$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
									$grupo=$this->lib->colores_producto($producto[0],$color,$productos_imagenes,$productos_imagenes_colores);
									$listado['descuento_producto']=$descuento_producto[0];
									$listado['descuentos_producto']=$this->M_descuento_producto->get_search('idsdp',$descuento_producto[0]->idsdp);
									$listado['sucursal_detalle_pedido']=$sucursal_detalle_pedido[0];
									$listado['sucursal']=$sucursal;
									$listado['parte_pedido']=$parte_pedido[0];
									$listado['grupo']=$grupo[0];
									$this->load->view('movimiento/pedidos/6-config/descuentos/update/view',$listado);
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function update_descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['pe']) && isset($_POST['des']) && isset($_POST['cantidad']) && isset($_POST['porcentaje']) && isset($_POST['observacion']) && isset($_POST['max'])){
						$idpe=trim($_POST['pe']);
						$iddes=trim($_POST['des']);
						$cantidad=trim($_POST['cantidad']);
						$porcentaje=trim($_POST['porcentaje']);
						$observaciones=trim($_POST['observacion']);
						$max=trim($_POST['max'])*1;
						$pedido=$this->M_pedido->get($idpe);
						$descuento_producto=$this->M_descuento_producto->get_search("iddes",$iddes);
						if(!empty($pedido) && !empty($descuento_producto) && $this->val->decimal($porcentaje,3,1) && $cantidad>0 && $porcentaje>0 && $porcentaje<=100 && $this->val->textarea($observaciones,0,900)){
							$descuentos_productos=$this->M_descuento_producto->get_search("idsdp",$descuento_producto[0]->idsdp);
							$cantidad_descontada=0;
							for($i=0;$i<count($descuentos_productos);$i++){$descuento=$descuentos_productos[$i];
								$suc_cantidad=$this->lib->desencriptar_num($descuento->cantidad)*1;
								$cantidad_descontada+=$suc_cantidad;
							}
							$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("idsdp",$descuento_producto[0]->idsdp);
							$cantidad_detalle=$this->lib->desencriptar_num($sucursal_detalle_pedido[0]->cantidad)*1;
							$cantidad_maxima=$cantidad_detalle-$cantidad_descontada+$cantidad;
							if($cantidad*1<=$cantidad_maxima){
								$porcentaje/=100;
								if($this->M_descuento_producto->modificar($iddes,$cantidad,$porcentaje,$observaciones)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function config_descuento_producto_detalle(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['des'])){
						$iddes=trim($_POST['des']);
						$descuento_producto=$this->M_descuento_producto->get_search("iddes",$iddes);
						if(!empty($descuento_producto)){
							$pedido=$this->M_pedido->get($descuento_producto[0]->idpe);
							$sucursales_cliente=$this->M_sucursal_cliente->get_complet("idcl",$pedido[0]->idcl);
							$sucursal_detalle_pedido=$this->M_sucursal_detalle_pedido->get_search("idsdp",$descuento_producto[0]->idsdp);
							$sucursal=$this->lib->search_elemento($sucursales_cliente,'idsc',$sucursal_detalle_pedido[0]->idsc);
							if($sucursal!=null){
								$parte_pedido=$this->M_parte_pedido->get_detalle('dp.iddp',$sucursal_detalle_pedido[0]->iddp);
								$color=$this->M_producto_grupo->get_grupo_colores("pgc.idpgrc",$parte_pedido[0]->idpgrc);
								if(!empty($color)){
									$producto=$this->M_producto->get($color[0]->idp);
									$productos_imagenes=$this->M_producto_imagen->get_all();
									$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
									$grupo=$this->lib->colores_producto($producto[0],$color,$productos_imagenes,$productos_imagenes_colores);
									$listado['descuento_producto']=$descuento_producto[0];
									$listado['sucursal']=$sucursal;
									$listado['parte_pedido']=$parte_pedido[0];
									$listado['grupo']=$grupo[0];
									$listado['detalles_descuento_producto']=$this->M_detalle_descuento_producto->get_search("iddes",$iddes);
									$listado['privilegio']=$privilegio[0];
									$this->load->view('movimiento/pedidos/6-config/descuentos/update/detalles',$listado);
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function adicionar_detalle_descuento(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['des'])){
						$iddes=trim($_POST['des']);
						$descuento_producto=$this->M_descuento_producto->get_search("iddes",$iddes);
						if(!empty($descuento_producto)){
							$listado['privilegio']=$privilegio[0];
							$this->load->view('movimiento/pedidos/6-config/descuentos/update/add_detalle_descuento',$listado);
							
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function save_detalle_descuento(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['des']) && isset($_FILES) && isset($_POST['obs'])){
						$iddes=trim($_POST['des']);
						$descripcion=trim($_POST['obs']);
						$descuento_producto=$this->M_descuento_producto->get_search("iddes",$iddes);
						if(!empty($descuento_producto) && count($_FILES)>0 && $this->val->textarea($descripcion,0,500)){
							$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/errores_productos/','',$this->resize,$iddes);
							if($img!='error' && $img!="error_type" && $img!="error_size_img"){
								if($this->M_detalle_descuento_producto->insertar($iddes,$img,$descripcion)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function change_detalle_descuento(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['desp'])){
						$iddesp=trim($_POST['desp']);
						$detalle_descuento_producto=$this->M_detalle_descuento_producto->get($iddesp);
						if(!empty($detalle_descuento_producto)){
							$listado['privilegio']=$privilegio[0];
							$listado['detalle_descuento_producto']=$detalle_descuento_producto[0];
							$this->load->view('movimiento/pedidos/6-config/descuentos/update/update_detalle_descuento',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function update_detalle_descuento(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['desp']) && isset($_FILES) && isset($_POST['obs'])){
						$iddesp=trim($_POST['desp']);
						$descripcion=trim($_POST['obs']);
						$detalle_descuento_producto=$this->M_detalle_descuento_producto->get($iddesp);
						if(!empty($detalle_descuento_producto) && $this->val->textarea($descripcion,0,500)){
							$img=$detalle_descuento_producto[0]->fotografia;
							$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/errores_productos/','',$this->resize,$img,$iddesp);
							if($img!='error' && $img!="error_type" && $img!="error_size_img"){
								if($this->M_detalle_descuento_producto->modificar($iddesp,$img,$descripcion)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function confirm_detalle_descuento(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['desp'])){
						$iddesp=trim($_POST['desp']);
						$detalle_descuento_producto=$this->M_detalle_descuento_producto->get($iddesp);
						if(!empty($detalle_descuento_producto)){
							$listado['titulo']="eliminar el detalle de descuento";
							$listado['desc']="Se eliminara definitivamente el detalle de descuento";
							$listado['control']="";
							$listado['open_control']="false";
							$img='sistema/miniatura/default.jpg';
							if($detalle_descuento_producto[0]->fotografia!=NULL && $detalle_descuento_producto[0]->fotografia!=""){ $img="errores_productos/miniatura/".$detalle_descuento_producto[0]->fotografia;}
							$url='./libraries/img/';
							$listado['img']=$url.$img;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function drop_detalle_descuento(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['desp'])){
						$iddesp=trim($_POST['desp']);
						$detalle_descuento_producto=$this->M_detalle_descuento_producto->get($iddesp);
						if(!empty($detalle_descuento_producto)){
							if($this->lib->eliminar_imagen($detalle_descuento_producto[0]->fotografia,'./libraries/img/errores_productos/')){
								if($this->M_detalle_descuento_producto->eliminar($iddesp)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function confirm_descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['des'])){
						$iddes=trim($_POST['des']);
						$descuento_producto=$this->M_descuento_producto->get($iddes);
						if(!empty($descuento_producto)){
							$listado['titulo']="eliminar el descuento del producto";
							$listado['desc']="Se eliminara definitivamente el descuento y los detalles que contenga.";
							$listado['control']="";
							$listado['open_control']="false";
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function drop_descuento_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"pedido");
				if($privilegio[0]->mo=="1" && $privilegio[0]->mo1u=="1"){
					if(isset($_POST['des'])){
						$iddes=trim($_POST['des']);
						$idpe=trim($_POST['pe']);
						$descuento_producto=$this->M_descuento_producto->get($iddes);
						if(!empty($descuento_producto)){
							$detalles_descuento_producto=$this->M_detalle_descuento_producto->get_search('iddes',$iddes);
							if($this->M_descuento_producto->eliminar($iddes)){
								for($i=0;$i<count($detalles_descuento_producto);$i++){ $detalle_descuento_producto=$detalles_descuento_producto[$i];
									if($this->lib->eliminar_imagen($detalle_descuento_producto->fotografia,'./libraries/img/errores_productos/')){
									}
								}
								echo "ok";
							}else{
								echo "error";
							}						
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		/*-- End descuentos de productos en pedido --*/
	/*--- End configuracion ---*/
	/*--- Eliminar ---*/
	public function confirmar_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpe'])){
				$idpe=$_POST['idpe'];
				$pedido=$this->M_pedido->get($idpe);
				if(!empty($pedido)){
					$desc="Se eliminara definitivamente el pedido";
					/*if($pedido[0]->estado==1){
						$desc=" Imposible Eliminar el producto, pues el pedido se encuentra en producción:";
						$listado['open_control']="false";
						$listado['control']="";
					}*/
					$listado['titulo']="eliminar el pedido <b> ".$pedido[0]->nombre."</b> definitivamente";
					$listado['desc']=$desc;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_pedido(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpe']) && isset($_POST['u']) && isset($_POST['p'])){
				$idpe=$_POST['idpe'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				if($u==$this->session->userdata("login")){
					$usuario=$this->M_usuario->validate($u,$p);
					if(!empty($usuario)){
						$pedido=$this->M_pedido->get($idpe);
						if(!empty($pedido)){
							if($this->M_pedido->eliminar($idpe)){
								if($this->M_pedido_seg->eliminar_row("idpe",$idpe)){
									$msj="Eliminó el pedido <strong>N°".$idpe." ".$pedido[0]->nombre."</strong>";
									if($this->M_pedido_seg->insertar(NULL,NULL,NULL,"dp",$msj)){
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_privilegio("","pedido"),$this->M_notificacion->get_all(),"pedido");
										foreach ($resultados as $key => $result) {
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_pedido($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"pedido",$result->cantidad)){}
											}
									}
									$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
									echo json_encode($result);
								}else{
									echo "ok";
								}
							}else{
								echo "ok";
							}
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			echo "fail";
		}
	}else{
		if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	}
}
/*--- End Eliminar ---*/
/*------- END MANEJO DE PEDIDOS -------*/
/*------- MANEJO DE COMPRAS -------*/
	public function search_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['privilegio']=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"compra");
			$listado['proveedores']=$this->M_proveedor->get_all();
			$this->load->view('movimiento/compras/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"compra");
			if(!empty($privilegio)){
				$atrib=""; $val="";
				if(isset($_POST['pro']) && isset($_POST['fe1']) && isset($_POST['fe2']) && isset($_POST['mes'])){
					$anio_mes=explode("-", $_POST['mes']);
					if($_POST['pro']!="" && $_POST['mes']!=""){
						$atrib="where";
						$val="material like '%".$_POST['pro']."%' AND year(fecha)='".$anio_mes[0]."' AND month(fecha)='".$anio_mes[1]."'";
					}else{
						if($_POST['fe1']!="" && $_POST['fe2']!=""){
							$atrib="where";
							$val="date(fecha)>='".$_POST['fe1']."' AND date(fecha)<='".$_POST['fe2']."'";
						}
					}
					if($atrib=="" && $val==""){
						if($_POST['pro']!=""){
							$atrib='material';$val=$_POST['pro'];
						}else{
							if($_POST['fe1']!=""){
								$atrib='fecha';$val=$_POST['fe1'];
							}else{
								if($_POST['fe2']!=""){
									$atrib='fecha';$val=$_POST['fe2'];
								}else{
									if($_POST['mes']!=""){
										$atrib="where";
										$val="year(fecha)='".$anio_mes[0]."' AND month(fecha)='".$anio_mes[1]."'";
									}
								}
							}
						}
					}
				}else{
					date_default_timezone_set("America/La_Paz");
					$atrib="where";
					$val="year(fecha)='".date('Y')."' AND month(fecha)='".date('m')."'";
				}
				$listado['proveedores']=$this->M_proveedor->get_all();
				$listado['compras']=$this->M_compra->get_search($atrib,$val,"fecha","ASC");
				$listado['privilegio']=$privilegio[0];
				$this->load->view('movimiento/compras/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function detalle_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['c'])){
				$compra=$this->M_compra->get(trim($_POST['c']));
				if(!empty($compra)){
					$listado['compra']=$compra[0];
					$listado['proveedores']=$this->M_proveedor->get_all();
					$listado['materiales']=$this->M_material_item->get_all();
					$listado['usuarios']=$this->M_usuario->get_complet_all();
					$this->load->view('movimiento/compras/5-reportes/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function config_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['c'])){
				$compra=$this->M_compra->get(trim($_POST['c']));
				if(!empty($compra)){
					$listado['compra']=$compra[0];
					$listado['proveedores']=$this->M_proveedor->get_all();
					$listado['materiales']=$this->M_material_item->get_all();
					$listado['usuarios']=$this->M_usuario->get_complet_all();
					$listado['unidades']=$this->M_unidad->get_all();
					$this->load->view('movimiento/compras/6-config/update',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idc']) && isset($_POST['num_doc']) && isset($_POST['fec']) && isset($_POST['mat']) && isset($_POST['can']) && isset($_POST['cos']) && isset($_POST['uni']) && isset($_POST['pro']) && isset($_POST['des'])){
				$idc=trim($_POST['idc']);
				$num_doc=trim($_POST['num_doc']);
				$fec=str_replace('T', ' ', $_POST['fec']);
				$idmi=trim($_POST['mat']);
				$can=trim($_POST['can']);
				$cos=trim($_POST['cos']);
				$idu=trim($_POST['uni']);
				$idpr=trim($_POST['pro']);
				$des=trim($_POST['des']);
				$vfecha=explode(" ", $fec);
				if(!$this->val->fecha($vfecha[0])){
					date_default_timezone_set("America/La_Paz");
					$fec=date('Y-m-d H:i');
				}
				$proveedor=$this->M_proveedor->get($idpr);
				$material=$this->M_material_item->get_unidad("mi.idmi",$idmi);
				$unidad=$this->M_unidad->get($idu);
				if(empty($proveedor)){
					$idpr=NULL;
				}
				if(!empty($material) && !empty($unidad) && $this->val->entero($num_doc,0,15) && $this->val->decimal($can,9,2) && $can>0 && $can<=999999999.99 && $this->val->decimal($cos,9,1) && $cos>=0 && $cos<=99999999.9 && $this->val->textarea($des,0,700)){
					if($this->M_compra->modificar($idc,$idmi,$idpr,$num_doc,$material[0]->nombre,$unidad[0]->nombre,$unidad[0]->abr,$can,$cos,$fec,$des)){
						$proveedor_material=$this->M_proveedor_material->get_row_2n('idmi',$idmi,'idpr',$idpr);
						if(empty($proveedor_material) && $idpr!=NULL){if($this->M_proveedor_material->insertar($idpr,$idmi)){}}
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo $control;
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['c'])){
				$idc=$_POST['c'];
				$compra=$this->M_compra->get($idc);
				if(!empty($compra)){
					$listado['open_control']="false";
					$listado['control']="";
					$listado['titulo']="eliminar el registro de compra";
					$listado['desc']="Se eliminara definitivamente la compra";
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
		}
	}
	public function drop_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['c'])){
				$idc=trim($_POST['c']);
				if($this->M_compra->eliminar($idc)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
		}
	}
	public function new_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type'])){
				$type=trim($_POST['type']);
				$listado['proveedores']=$this->M_proveedor->get_all();
				$listado['unidades']=$this->M_unidad->get_all();
				$listado['type']=$type;
				switch ($type) {
					case 'add':
						$this->load->view('movimiento/compras/3-nuevo/nuevo_add',$listado);
						break;
					case 'create':
						$listado['unidades']=$this->M_unidad->get_all();
						$listado['proveedores']=$this->M_proveedor->get_all();
						$this->load->view('movimiento/compras/3-nuevo/nuevo_create',$listado);
						break;
					default: echo "fail"; break;
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_material_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['mi'])){
				$idmi=trim($_POST['mi']);
				$material=$this->M_material_item->get_unidad("mi.idmi",$idmi);
				if(!empty($material)){
					$url=base_url().'libraries/img/';
					$img="sistema/miniatura/default.jpg";
				    if($material[0]->fotografia!="" && $material[0]->fotografia!=NULL){
						$img="materiales/miniatura/".$material[0]->fotografia;
					}
						echo '<div class="card-group" style="text-align: center !important;display: table;width: 100%;" data-mi="'.$material[0]->idmi.'">
								<div class="card img-thumbnail-35" style="display: table-cell;">
									<img class="card-img-top" src="'.$url.$img.'" alt="image" data-title="'.$material[0]->nombre.'" data-desc="" style="vertical-align: top;">
								</div>
								<div class="card card-padding-0" style="vertical-align: middle !important;display: table-cell;">
									<div class="a-card-block">
										<p class="a-card-text" style="line-height: .8 !important;"><small style="color: #069bcc !important;">'.$material[0]->nombre.'</small></p>
									</div>
								</div>
							</div>';
						
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function proveedor_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['mi'])){
				$idmi=trim($_POST['mi']);
				$material=$this->M_material_item->get($idmi);
				if(!empty($material)){
					$proveedor_material=$this->M_proveedor_material->get_row("idmi",$idmi);
					$proveedores=$this->M_proveedor->get_all();
					$options="<option value=''>Seleccionar...</option>";
					for($i=0;$i<count($proveedores);$i++){ $proveedor=$proveedores[$i];
						$selected=$this->lib->search_elemento($proveedor_material,'idpr',$proveedor->idpr);
						if($selected!=null){$selected="SELECTED";}
						$options.="<option value='".$proveedor->idpr."' ".$selected.">".$proveedor->razon."</option>";
					}
					echo $options;						
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function unidad_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['mi'])){
				$idmi=trim($_POST['mi']);
				$material=$this->M_material_item->get($idmi);
				if(!empty($material)){
					$options=$this->M_unidad->get_all();
					$resp="<option value=''>Seleccionar...</option>";
					for($i=0; $i < count($options); $i++){ $option=$options[$i];
						$selected="";
						if($material[0]->idu==$option->idu){$selected="selected='selected'";}
						$resp.="<option value='".$option->idu."' ".$selected.">".$option->nombre." [".$option->abr.".]</option>";
					}
					echo $resp;						
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*Manejode unidades en el material*/
	public function new_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['container']) && isset($_POST['type'])){
				$listado['container']=trim($_POST['container']);
				$listado['type']=trim($_POST['type']);
				$this->load->view('movimiento/compras/genericas/unidad/form_new',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['uni']) && isset($_POST['abr'])){
				$uni=trim($_POST['uni']);
				$abr=trim($_POST['abr']);
				if($this->val->strSpace($uni,2,40) && $this->val->strSpace($abr,1,8)){
					if($this->M_unidad->insertar($uni,$abr,0,"")){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function option_unidades(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['selected']) && isset($_POST['abr'])){
				$options=$this->M_unidad->get_all();
				$resp="<option value=''>Seleccionar...</option>";
				$selected="";
				for($i=0; $i < count($options); $i++){$option=$options[$i];
					$selected="";
					if(trim($_POST['selected'])==$option->idu){$selected="SELECTED='selected'";}
					$resp.="<option value='".$option->idu."' ".$selected.">".$option->nombre." [".$option->abr.".]</option>";
				}
				echo $resp;
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*End manejode unidades en el material*/
	/*Manejo de proveedor en materiales*/
	public function new_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['container']) && isset($_POST['type'])){
				$listado['container']=trim($_POST['container']);
				$listado['type']=trim($_POST['type']);
				$this->load->view('movimiento/compras/genericas/proveedor/form_new',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['nit']) && isset($_POST['raz'])){
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				if($this->val->entero($nit,0,25) && $this->val->strSpace($raz,1,100)){
					if($this->M_proveedor->insertar($nit,$raz,"","","","","","","")){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function option_proveedores(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$options=$this->M_proveedor->get_all();
			$resp="<option value=''>Seleccionar...</option>";
			for ($i=0; $i < count($options); $i++){ $option=$options[$i];
				$resp.="<option value='".$option->idpr."'>".$option->razon."</option>";
			}
			echo $resp;
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*End manejo de proveedor en materiales*/
	public function save_compra(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type']) && isset($_POST['num_doc']) && isset($_POST['fec']) && isset($_POST['mat']) && isset($_POST['can']) && isset($_POST['cos']) && isset($_POST['pro']) && isset($_POST['des']) && isset($_POST['uni'])){
				$type=trim($_POST['type']);
				$num_doc=trim($_POST['num_doc']);
				$fec=str_replace('T', ' ', $_POST['fec']);
				if($type=="create"){
					$nombre_material=trim($_POST['mat']);
				}else{
					$idmi=trim($_POST['mat']);
				}
				$can=trim($_POST['can']);
				$cos=trim($_POST['cos']);
				$idpr=trim($_POST['pro']);
				$des=trim($_POST['des']);
				$vfecha=explode(" ", $fec);
				$proveedor=$this->M_proveedor->get($idpr);
				if(!$this->val->fecha($vfecha[0])){
					date_default_timezone_set("America/La_Paz");
					$fec=date('Y-m-d H:i');
				}
				if(empty($proveedor)){
					$idpr=NULL;
				}
				$control="ok";
				if($type=="create"){
					$control="fail";
					if(isset($_FILES)){
						$idmi=$this->M_material_item->max('idmi')+1;
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales/','',$this->resize,$idmi);
						if($img!="error_type" && $img!="error" && $img!="error_size_img"){
							$control="ok";
						}else{
							$control=$img;
						}
					}
				}
				$unidad=$this->M_unidad->get(trim($_POST['uni']));
				if($control=="ok" && $this->val->entero($num_doc,0,15) && $this->val->decimal($can,9,2) && $can>0 && $can<=999999999.99 && $this->val->decimal($cos,9,1) && $cos>=0 && $cos<=99999999.9 && $this->val->textarea($des,0,700) && !empty($unidad)){
					$control=true;
					if($type=="create"){
						if(!empty($idmi) && !empty($unidad) && !empty($nombre_material)){
							if($this->val->strSpace($nombre_material,2,100)){ 
									if($this->M_material_item->insertar($idmi,$unidad[0]->idu,NULL,$nombre_material,$img,NULL)){
									if($this->M_material_vario->insertar($idmi)){
										$control=true;
									}else{
										$this->M_material_item->eliminar($idmi);
										$this->lib->eliminar_imagen($img,'./libraries/img/materiales/');
										$control=false;
									}
								}else{
									$this->lib->eliminar_imagen($img,'./libraries/img/materiales/');
									$control=false;
								}
							}else{
								$control=false;
							}
						}else{
							$control=false;
						}
					}
					if($control){
						$material=$this->M_material_item->get($idmi);
						if(!empty($material)){
							if($this->M_compra->insertar($this->session->userdata('id'),$idmi,$idpr,$num_doc,$material[0]->nombre,$unidad[0]->nombre,$unidad[0]->abr,$can,$cos,$fec,$des)){
								if($idpr!="" && $idpr!=null && $idpr!="undefined"){
									$proveedor_material=$this->M_proveedor_material->get_row_2n('idmi',$idmi,'idpr',$idpr);
									if(empty($proveedor_material)){if($this->M_proveedor_material->insertar($idpr,$idmi)){}}
								}
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail $idmi $type";
						}
					}else{
						echo "fail";
					}
				}else{
					echo $control;
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*--- Imprimir ---*/
   	public function print_compras(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"compra");
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['pro']) && isset($_POST['fe1']) && isset($_POST['fe2']) && isset($_POST['mes'])){
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['compras']=$this->M_compra->get_search("","","fecha","ASC");
					$listado['proveedores']=$this->M_proveedor->get_all();
					$listado['privilegio']=$privilegio[0];
					$listado['tbl']=trim($_POST['tbl']);
					$listado['pro']=trim($_POST['pro']);
					$listado['fe1']=trim($_POST['fe1']);
					$listado['fe2']=trim($_POST['fe2']);
					$listado['mes']=trim($_POST['mes']);
					$this->load->view('movimiento/compras/4-imprimir/print_compras',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_compras(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_movimiento($this->session->userdata("id"),"compra");
			if(!empty($_GET['file']) && !empty($privilegio) && isset($_GET['pro']) && isset($_GET['fe1']) && isset($_GET['fe2']) && isset($_GET['mes']) && isset($_GET['file'])){
				$anio_mes=explode("-", $_GET['mes']);
				$atrib="";$val="";
				if($_GET['pro']!="" && $_GET['mes']!=""){
					$atrib="where";
					$val="material like '%".$_GET['pro']."%' AND year(fecha)='".$anio_mes[0]."' AND month(fecha)='".$anio_mes[1]."'";
				}else{
					if($_GET['fe1']!="" && $_GET['fe2']!=""){
						$atrib="where";
						$val="date(fecha)>='".$_GET['fe1']."' AND date(fecha)<='".$_GET['fe2']."'";
					}
				}
				if($atrib=="" && $val==""){
					if($_GET['pro']!=""){
						$atrib='material';$val=$_GET['pro'];
					}else{
						if($_GET['fe1']!=""){
							$atrib='fecha';$val=$_GET['fe1'];
						}else{
							if($_GET['fe2']!=""){
								$atrib='fecha';$val=$_GET['fe2'];
							}else{
								if($_GET['mes']!=""){
									$atrib="where";
									$val="year(fecha)='".$anio_mes[0]."' AND month(fecha)='".$anio_mes[1]."'";
								}
							}
						}
					}
				}
				$listado['proveedores']=$this->M_proveedor->get_all();
				$listado['compras']=$this->M_compra->get_search($atrib,$val,"fecha","ASC");
				$listado['privilegio']=$privilegio[0];
				$listado['file']=trim($_GET['file']);
				$listado['fe1']=trim($_GET['fe1']);
				$listado['fe2']=trim($_GET['fe2']);
				$listado['mes']=trim($_GET['mes']);
				$this->load->view('movimiento/compras/4-imprimir/export_compras',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*--- End imprimir ---*/
/*------- MANEJO DE COMPRAS -------*/
/*------- FUNCIONES GENERICAS -------*/
	public function view_materiales(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['container'])){
				$listado['materiales_item']=$this->M_material_item->get_all();
				$listado['materiales']=$this->M_material->get_all();
				$listado['colores']=$this->M_color->get_all();
				$listado['almacenes']=$this->M_almacen->get_all();
				$listado['almacenes_materiales']=$this->M_almacen_material->get_all();
				$listado['materiales_indirectos']=$this->M_material_extra->get_all();
				$listado['otros_materiales']=$this->M_material_vario->get_all();
				$listado['container']=$_POST['container'];
				if(isset($_POST['pro'])){ $listado['container_proveedor']=trim($_POST['pro']);}
				if(isset($_POST['uni'])){ $listado['container_unidad']=trim($_POST['uni']);}
				$this->load->view('movimiento/compras/genericas/materiales',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END FUNCIONES GENERICAS -------*/
}
/* End of file pedidos_ventas.php */
/* Location: ./application/controllers/pedidos_ventas.php */