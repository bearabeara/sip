<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo_color_seg extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('producto_grupo_color_seg');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo_color_seg',['idpgcs' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo_atributo WHERE idpgcs='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$this->db->order_by('fecha','desc');
		$this->db->order_by('idpgcs','desc');
		$query=$this->db->get_where('producto_grupo_color_seg',[$col => $val]);
		return $query->result();
	}
	function get_producto($idp,$type,$idpgr,$idpgrc){
		$this->db->select("*");
		$this->db->from("producto_grupo_color_seg");
		$this->db->where("idp = '$idp'");
		switch ($type){
			case 'producto_grupo': $this->db->where("accion IN ('cg','dg','ug','uga','cgm','dgm','ugm') AND idpgr = '$idpgr'"); break;
			case 'pgm-modal': $this->db->where("accion IN ('cgm','dgm','ugm') AND idpgr = '$idpgr'"); break;
			case 'producto_grupo_color': $this->db->where("accion IN ('cc','dc','uc','cgcm','dgcm','ugcm','cgcf','dgcf','ugcf','cgcp','dgcp','ugcp','uca') AND idpgr = '$idpgr'"); break;
			case 'pgcm-modal': $this->db->where("accion IN ('cgcm','dgcm','ugcm') AND idpgrc = '$idpgrc'"); break;
			case 'pgcf-modal': $this->db->where("accion IN ('cgcf','dgcf','ugcf') AND idpgrc = '$idpgrc'"); break;
			case 'pgcpi-modal': $this->db->where("accion IN ('cgcp','dgcp','ugcp') AND idpgrc = '$idpgrc'"); break;
			case 'producto_grupo_complet': $this->db->where("accion IN ('cg','dg','ug','uga','cgm','dgm','ugm','cc','dc','uc','cgcm','dgcm','ugcm','cgcf','dgcf','ugcf','cgcp','dgcp','ugcp','uca') AND idpgr = '$idpgr' AND (idpgrc = '$idpgrc' OR idpgrc IS NULL)"); break;
		}
		$this->db->order_by('fecha','desc');
		$this->db->order_by('idpgcs','desc');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idp,$producto,$idpgr,$idgr,$grupo,$idpgrc,$idco,$color,$accion,$msj_adicional,$idu,$usuario){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'idp' => $idp,
			'producto' => $producto,
			'idpgr' => $idpgr,
			'idgr' => $idgr,
			'grupo' => $grupo,
			'idpgrc' => $idpgrc,
			'idco' => $idco,
			'color' => $color,
			'accion' => $accion,
			'msj_adicional' => $msj_adicional,
			'idu' => $idu,
			'usuario' => $usuario,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('producto_grupo_color_seg',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$valor){
		$datos=array(
			'valor' => $valor
		);
		if($this->db->update('producto_grupo_color_seg',$datos,array('idpgcs' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function null($col,$val){
		$datos=array( $col => NULL );
		if($this->db->update('producto_grupo_color_seg',$datos,array($col => $val))){
			return true;
		}else{
			return false;
		}
	}
	function null_2n($col,$val,$col2,$val2){
		$datos=array( $col2 => NULL );
		if($this->db->update('producto_grupo_color_seg',$datos,array($col => $val,$col2 => $val2))){
			return true;
		}else{
			return false;
		}
	}
	function null_3n($col,$val,$col2,$val2,$col3,$val3){
		$datos=array( $col3 => NULL );
		if($this->db->update('producto_grupo_color_seg',$datos,array($col => $val,$col2 => $val2,$col3 => $val3))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo_color_seg',['idpgcs' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max_where($atrib,$col,$val){// en uso
		$this->db->select("*");
		$this->db->from("producto_grupo_color_seg");
		$this->db->where("$col = '$val'");
		$this->db->where(" msj_adicional not like '%odificó el costo de%'");
		$this->db->order_by("$atrib","desc");
		$this->db->limit(5);
		$query=$this->db->get();
		return $query->result();
	}

}

/* End of file M_producto_grupo_color_seg.php */
/* Location: ./application/models/M_producto_grupo_color_seg.php*/