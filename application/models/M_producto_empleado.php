<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_empleado extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_empleado');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_empleado',['idproe' => $id]);
		return $query->result();
	}
    function get_row($col,$val){
      $query=$this->db->get_where('producto_empleado',[$col=>$val]);
      return $query->result();
    }
    function get_row_2n($col,$val,$col2,$val2){
      $query=$this->db->get_where('producto_empleado',[$col=>$val,$col2=>$val2]);
      return $query->result();
    }
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_empleado WHERE idproe='$id'");
		return $query->result();
	}
	function get_search($col,$val,$order_by,$order){
		$this->db->select("*");
		$this->db->from("producto_empleado");
		if($order_by=="" || $order_by==null || $order=="" || $order==null || ($order!="ASC" && $order!="DESC")){
			$this->db->order_by("idproe", "asc");
		}else{
			//echo $order_by." ".$order." ";
			$this->db->order_by($order_by, $order);
		}
		if($col!="" && $val!=""){
			if($col=="idproe" || $col=="idpre" || $col=="calidad"){
				$this->db->where("$col = '$val'");
			}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_complet($col,$val){
		$cols="pe.idproe, pe.idpgrc, pe.calidad,
		pre.idpre, pre.ide, pre.tipo,
		pr.idpr, pr.nombre";
		$this->db->select($cols);
		$this->db->from("producto_empleado pe");
		$this->db->join("proceso_empleado pre","pre.idpre = pe.idpre","inner");
		$this->db->join("proceso pr","pr.idpr = pre.idpr","inner");
		if($col!="" && $val!=""){
			if($col=="pe.idproe" || $col=="pre.ide"){
				$this->db->where("$col = '$val'");
			}
			if($col=="pr.nombre"){
				$this->db->where("$col like '$val%'");
			}
		}
		$this->db->order_by("pr.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_proceso_empleado($col,$val,$col2,$val2){
		$cols="pe.idproe, pe.idpgrc, pe.calidad,
		pre.idpre, pre.idpr ,pre.ide, pre.tipo";
		$this->db->select($cols);
		$this->db->from("producto_empleado pe");
		$this->db->join("proceso_empleado pre","pre.idpre = pe.idpre","inner");
		if($col!="" && $val!=""){
			if($col=="pe.idproe" || $col=="pe.idpgrc" || $col=="pre.idpre" || $col=="pre.idpr" || $col=="pre.ide"){
				$this->db->where("$col = '$val'");
			}
		}
		if($col2!="" && $val2!=""){
			if($col2=="pe.idproe" || $col2=="pe.idpgrc" || $col2=="pre.idpre" || $col2=="pre.idpr" || $col2=="pre.ide"){
				$this->db->where("$col2 = '$val2'");
			}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_empleado($col,$val,$order_by,$order){
		$cols="pe.idproe, pe.idpgrc, pe.calidad,
		pre.idpre, pre.idpr, pre.tipo,
		e.ide,e.idtc,e.codigo,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.c_descuento,e.direccion,e.estado, e.grado_academico,
		p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo";
		$this->db->select($cols);
		$this->db->from("producto_empleado pe");
		if($col!="" && $val!=""){
			if($col=="pe.idproe" || $col=="pe.idpgrc")
				{ $this->db->where("$col = '$val'"); }
			if($col=="nombre" || $col=="abr")
				{ $this->db->where("$col like '$val%'"); }
		}
		$this->db->join("proceso_empleado pre","pre.idpre = pe.idpre","inner");
		$this->db->join("empleado e","e.ide = pre.ide","inner");
		$this->db->join("persona p","p.ci = e.ci","inner");
		if($order_by=="" || $order=="" || $order_by==NULL || $order==NULL){
			$this->db->order_by("p.nombre", "asc");
		}else{
			$this->db->order_by($order_by, $order);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_2n_empleado($col,$val,$col2,$val2,$order_by,$order){
		$cols="pe.idproe, pe.idpgrc, pe.calidad,
		pre.idpre, pre.idpr, pre.tipo,
		e.ide,e.idtc,e.codigo,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.c_descuento,e.direccion,e.estado, e.grado_academico,
		p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo";
		$this->db->select($cols);
		$this->db->from("producto_empleado pe");
		if($col!="" && $val!=""){
			if($col=="pe.idproe" || $col=="pe.idpgrc" || $col=="pre.idpr"){ $this->db->where("$col = '$val'"); }
			if($col=="nombre" || $col=="abr"){ $this->db->where("$col like '$val%'"); }
		}
		if($col2!="" && $val2!=""){
			if($col2=="pe.idproe" || $col2=="pe.idpgrc" || $col2=="pre.idpr"){ $this->db->where("$col2 = '$val2'"); }
			if($col2=="nombre" || $col2=="abr"){ $this->db->where("$col2 like '$val2%'"); }
		}
		$this->db->join("proceso_empleado pre","pre.idpre = pe.idpre","inner");
		$this->db->join("empleado e","e.ide = pre.ide","inner");
		$this->db->join("persona p","p.ci = e.ci","inner");
		$this->db->order_by("pre.tipo", "asc");
		if($order_by=="" || $order=="" || $order_by==NULL || $order==NULL){
			$this->db->order_by("p.nombre", "asc");
		}else{
			$this->db->order_by($order_by, $order);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpgrc,$idpre,$calidad,$tipo){
		$datos=array(
			'idpgrc' => $idpgrc,
			'idpre' => $idpre,
			'calidad' => $calidad,
			'tipo' => $tipo
		);
		if($this->db->insert('producto_empleado',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$calidad){
		$datos=array( 'calidad' => $calidad );
		if($this->db->update('producto_empleado',$datos,array('idproe' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_empleado',['idproe' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_producto_empleado.php */
/* Location: ./application/models/m_producto_empleado.php*/