<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_cliente extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("razon", "asc");
		$query=$this->db->get('cliente');
		return $query->result();
	}
	function get($id){
		$this->db->order_by("razon", "asc");
		$query=$this->db->get_where('cliente',['idcl' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM cliente WHERE idcl='$id'");
		return $query->result();
	}
	function get_row_2($col,$val){//en uso, Clientes, 
      $query=$this->db->get_where('cliente',[$col => $val]);
      return $query->result();
    }
	function get_row($atrib,$val){
		$query=$this->db->get_where('cliente',[$atrib => $val]);
		return $query->result();
	}
	function get_search($col,$val){// en uso:  cliente
		$cols="idcl,nit,razon,gerente,telefono,url,fotografia,observacion";
		$this->db->select($cols);
		$this->db->from("cliente");
		if($col!="" && $val!=""){
			if($col=="idcl"){ $this->db->where("$col = '$val'");}
			if($col=="nit"){ $this->db->where("$col like '$val%'");}
			if($col=="gerente" || $col=="razon"){ $this->db->where("$col like '%$val%'");}
		}
		$this->db->order_by("razon", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($nit,$razon,$gerente,$telefono,$url,$fotografia,$observacion){
		$datos=array(
			'nit' => $nit,
			'razon' => $razon,
			'gerente' => $gerente,
			'telefono' => $telefono,
			'url' => $url,
			'fotografia' => $fotografia,
			'observacion' => $observacion
		);
		if($this->db->insert('cliente',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nit,$razon,$gerente,$telefono,$url,$fotografia,$observacion){
		$datos=array(
			'nit' => $nit,
			'razon' => $razon,
			'gerente' => $gerente,
			'telefono' => $telefono,
			'url' => $url,
			'fotografia' => $fotografia,
			'observacion' => $observacion
		);
		if($this->db->update('cliente',$datos,array('idcl'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('cliente',['idcl' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_cliente.php */
/* Location: ./application/models/m_cliente.php*/