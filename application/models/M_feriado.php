<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_feriado extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('feriado');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('feriado',['idf' => $id]);
		return $query->result();
	}
	function get_fecha($dia,$mes,$anio,$action){
		switch($action){
			case 'dia': $query=$this->db->get_where('feriado',['day(fecha)' => $dia,'month(fecha)' => $mes,'year(fecha)' => $anio]); return $query->result(); break;
			case 'mes': $query=$this->db->get_where('feriado',['month(fecha)' => $mes,'year(fecha)' => $anio]); return $query->result(); break;
			case 'anio': $query=$this->db->get_where('feriado',['year(fecha)' => $anio]); return $query->result(); break;
		}
	}
	function get_fechas($f1,$f2){
		$this->db->select("*");
	    $this->db->from("feriado");
	    $this->db->where("fecha >= '$f1'");
	    $this->db->where("fecha <= '$f2'");
	    $this->db->order_by('fecha');
	    $query=$this->db->get();
	    return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM feriado WHERE idf='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM feriado WHERE $col='$val'");
		return $query->result();
	}
	
	function insertar($fecha,$tipo,$descripcion){
		$datos=array(
			'fecha' => $fecha,
			'tipo' => $tipo,
			'descripcion' => $descripcion
		);
		if($this->db->insert('feriado',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fecha,$tipo,$descripcion){
		$datos=array(
			'fecha' => $fecha,
			'tipo' => $tipo,
			'descripcion' => $descripcion
		);
		if($this->db->update('feriado',$datos,array('idf'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('feriado',['idf' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_feriado.php */
/* Location: ./application/models/m_feriado.php*/