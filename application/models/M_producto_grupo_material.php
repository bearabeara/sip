<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_grupo_material');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo_material',['idpgm' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo_material WHERE idpgm='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_grupo_material',[$col => $val]);
		return $query->result();
	}
	function get_search_grupo($idpgr,$col,$val){// en uso: PRODUCTO
		$cols="pgm.idpgm, pgm.idpgr, pgm.cantidad,pgm.verificado,pgm.observacion,
			m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c,c.abr as abr_c,
			u.idu, u.nombre as nombre_u,u.abr as abr_u";
		$this->db->select($cols);
		$this->db->from("producto_grupo_material pgm");
		if($col!="" && $val!=""){
			if($col=="mi.idmi" || $col=="pgm.idpgm" || $col=="pgm.idpgr"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->where("pgm.idpgr = '$idpgr'");
		$this->db->join('material m','pgm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material($col,$val){
		$cols="pgm.idpgm, pgm.idpgr, pgm.cantidad,pgm.verificado,pgm.observacion,
			m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_grupo_material pgm");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="mi.idmi" || $col=="pgm.idpgm" || $col=="pgm.idp" || $col=="pgm.idpgr"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->join('material m','pgm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_grupo($col,$val){
		$cols="pgm.idpgm, pgm.idm, pgm.cantidad, pgm.observacion,
			pg.idpgr,pg.idp,
			g.idgr,g.nombre,g.abr";
		$this->db->select($cols);
		$this->db->from("producto_grupo_material pgm");
		if($col!="" && $val!=""){
			if($col=="pgm.idm" || $col=="pgm.idpgm" || $col=="pgm.idp" || $col=="pgm.idpgr"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->join('producto_grupo pg','pg.idpgr = pgm.idpgr','inner');
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpgr,$idm,$cantidad,$observacion){
		$datos=array(
			'idpgr' => $idpgr,
			'idm' => $idm,
			'cantidad' => $cantidad,
			'observacion' => $observacion,
		);
		if($this->db->insert('producto_grupo_material',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$cantidad,$verificado,$observacion){
		$datos=array(
			'cantidad' => $cantidad,
			'verificado' => $verificado,
			'observacion' => $observacion
		);
		if($this->db->update('producto_grupo_material',$datos,array('idpgm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar2($id,$idm,$cantidad,$observacion){
		$datos=array(
			'idm' => $idm,
			'cantidad' => $cantidad,
			'observacion' => $observacion
		);
		if($this->db->update('producto_grupo_material',$datos,array('idpgm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo_material',['idpgm' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_grupo_material");
		$max=$query->result();
		return $max[0]->max*1;
	}
}

/* End of file m_producto_grupo_material.php */
/* Location: ./application/models/m_producto_grupo_material.php*/