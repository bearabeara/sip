<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_proceso',['idppr' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_proceso WHERE idppr='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_proceso',[$col => $val]);
		return $query->result();
	}
	function producto_proceso($col,$val,$order_by,$order){
		$cols="pp.idppr, pp.idp,
			p.idpr,p.nombre,p.detalle";
		$this->db->select($cols);
		$this->db->from("producto_proceso pp");
		$this->db->join('proceso p','p.idpr = pp.idpr','inner');
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="pp.idppr" || $col=="pp.idp" || $col=="p.idpr"){
				$this->db->where("$col = '$val'");
			}
			if($col=="p.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		if($order_by=="" || $order=="" || $order_by==NULL || $order==NULL){
			$this->db->order_by("pp.idppr", "asc");
		}else{
			$this->db->order_by($order_by, $order);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idppr,$idpr,$idp){
		$datos=array(
			'idppr' => $idppr,
			'idpr' => $idpr,
			'idp' => $idp
		);
		if($this->db->insert('producto_proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpa,$nombre,$abreviatura){
		$datos=array(
			'idpa' => $idpa,
			'nombre' => $nombre,
			'abreviatura' => $abreviatura
		);
		if($this->db->update('producto_proceso',$datos,array('idppr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_proceso',['idppr' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_proceso");
		$max=$query->result();
		return $max[0]->max*1;
	}
}

/* End of file m_producto_proceso.php */
/* Location: ./application/models/m_producto_proceso.php*/