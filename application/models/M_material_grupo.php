<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_grupo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('material_grupo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_grupo',['idmg' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_grupo WHERE idmg='$id'");
		return $query->result();
	}

	function get_all_use_insumo(){
		$query=$this->db->query("SELECT idmg, nombre, descripcion, (SELECT count(*) FROM material WHERE material.idmg=material_grupo.idmg)as nro FROM material_grupo");
		return $query->result();
	}
	function insertar($nombre, $abr,$descripcion){
		$datos=array(
			'nombre' => $nombre,
			'abr' => $abr,
			'descripcion' => $descripcion 
		);
		if($this->db->insert('material_grupo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$abr,$descripcion){
		$datos=array(
			'nombre' => $nombre,
			'abr' => $abr,
			'descripcion' => $descripcion
		);
		if($this->db->update('material_grupo',$datos,array('idmg' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_grupo',['idmg' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_material_grupo.php */
/* Location: ./application/models/m_material_grupo.php*/