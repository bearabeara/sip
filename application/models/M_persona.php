<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_persona extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$col="ci,nombre,nombre2,paterno,materno,telefono,email,fotografia,descripcion,cargo,CONCAT(nombre,' ',nombre2,' ',paterno,' ',materno) as nombre_completo";
		$this->db->select($col);
		$this->db->from("persona");
		$this->db->order_by("nombre_completo", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$col="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,
			pa.idpa,pa.nombre as pais,
			ci.idci,ci.nombre as ciudad";
		$this->db->select($col);
		$this->db->from("persona p");
		$this->db->join('ciudad ci','ci.idci = p.idci','inner');
		$this->db->join('pais pa','pa.idpa = ci.idpa','inner');
		$this->db->where("p.ci = '$id'");
		$this->db->order_by("p.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM persona WHERE ci='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('persona',[$col => $val]);
		return $query->result();
	}
	function insertar($ci,$idci,$nombre,$nombre2,$paterno,$materno,$cargo,$telefono,$email,$fotografia,$descripcion){
		$datos=array(
			'ci' => $ci,
			'idci' => $idci,
			'nombre' => $nombre,
			'nombre2' => $nombre2,
			'paterno' => $paterno,
			'materno' => $materno,
			'cargo' => $cargo,
			'telefono' => $telefono,
			'email' => $email,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->insert('persona',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$ci,$idci,$nombre,$nombre2,$paterno,$materno,$cargo,$telefono,$email,$fotografia,$descripcion){//en uso: CAPITAL HUMANO,
		$datos=array(
			'ci' => $ci,
			'idci' => $idci,
			'nombre' => $nombre,
			'nombre2' => $nombre2,
			'paterno' => $paterno,
			'materno' => $materno,
			'cargo' => $cargo,
			'telefono' => $telefono,
			'email' => $email,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->update('persona',$datos,array('ci'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_usuario($id,$ci,$idci,$nombre,$nombre2,$paterno,$materno,$cargo,$telefono,$fotografia){
		$datos=array(
			'ci' => $ci,
			'idci' => $idci,
			'nombre' => $nombre,
			'nombre2' => $nombre2,
			'paterno' => $paterno,
			'materno' => $materno,
			'cargo' => $cargo,
			'telefono' => $telefono,
			'fotografia' => $fotografia,
		);
		if($this->db->update('persona',$datos,array('ci'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($ci){
		if($this->db->delete('persona',['ci' => $ci])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_persona.php */
/* Location: ./application/models/m_persona.php*/