<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_proveedor_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("razon", "asc");
		$query=$this->db->get('proveedor_material');
		return $query->result();
	}
	function get($id){
		$this->db->order_by("razon", "asc");
		$query=$this->db->get_where('proveedor_material',['idpm' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM proveedor_material WHERE idpm='$id'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
      $query=$this->db->get_where('proveedor_material',[$col => $val,$col2 => $val2]);
      return $query->result();
    }
	function get_row($atrib,$val){
		$query=$this->db->get_where('proveedor_material',[$atrib => $val]);
		return $query->result();
	}
	function insertar($idpr,$idmi){
		$datos=array(
			'idpr' => $idpr,
			'idmi' => $idmi
		);
		if($this->db->insert('proveedor_material',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpr,$idmi){
		$datos=array(
			'idpr' => $idpr,
			'idmi' => $idmi
		);
		if($this->db->update('proveedor_material',$datos,array('idpm'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('proveedor_material',['idpm' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_historial_insumo.php */
/* Location: ./application/models/m_historial_insumo.php*/