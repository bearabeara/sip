<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_grupo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('grupo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('grupo',['idgr' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM grupo WHERE idgr='$id'");
		return $query->result();
	}
	function get_search($col,$val){
		$this->db->select("*");
		$this->db->from("grupo");
		if($col!="" && $val!=""){
			if($col=="idgr"){
				$this->db->where("$col = '$val'");
			}
			if($col=="nombre" || $col=="abr"){
				$this->db->where("$col like '$val%'");
			}
		}
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($nombre,$abr){
		$datos=array(
			'nombre' => $nombre,
			'abr' => $abr
		);
		if($this->db->insert('grupo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$abr){
		$datos=array(
			'nombre' => $nombre,
			'abr' => $abr
		);
		if($this->db->update('grupo',$datos,array('idgr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('grupo',['idgr' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_grupo.php */
/* Location: ./application/models/m_grupo.php*/