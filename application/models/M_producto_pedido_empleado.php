<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_pedido_empleado extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre");
		$query=$this->db->get('producto_pedido_empleado');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_pedido_empleado',['idppe' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_pedido_empleado WHERE idppe='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$this->db->order_by("nombre");
		$query=$this->db->get_where('producto_pedido_empleado',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$c_terminado="`".$this->lib->encriptar_str("terminado")."`";
		$cols="idppe,idsdp,idpre,".$c_cantidad." as cantidad,".$c_terminado." as terminado,fecha_inicio,fecha_fin,observacion,ide,idpr,proceso,tipo";
		$this->db->select($cols);
		$this->db->from("producto_pedido_empleado");
		if($col!="" && $val!=""){
			if($col=="idppe" || $col=="idsdp"){ $this->db->where("$col = '$val'");}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_produccion($col,$val){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$c_terminado="`".$this->lib->encriptar_str("terminado")."`";
		$cols="idppe,idsdp,idpre,".$c_cantidad." as cantidad,".$c_terminado." as terminado,fecha_inicio,fecha_fin,observacion,ide,idpr,proceso,tipo";
		$this->db->select($cols);
		$this->db->from("producto_pedido_empleado");
		if($col!="" && $val!=""){
			if($col=="idppe" || $col=="idsdp"){ $this->db->where("$col = '$val'");}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idsdp,$idpre,$cantidad,$terminado,$fecha_inicio,$fecha_fin,$observacion,$ide,$idpr,$proceso,$tipo){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$c_terminado="`".$this->lib->encriptar_str("terminado")."`";
		$cantidad=$this->lib->encriptar_num($cantidad);
		$terminado=$this->lib->encriptar_num($terminado);
		$datos=array(
			'idsdp' => $idsdp,
			'idpre' => $idpre,
			$c_cantidad => $cantidad,
			$c_terminado => $terminado,
			'fecha_inicio' => $fecha_inicio,
			'fecha_fin' => $fecha_fin,
			'observacion' => $observacion,
			'ide' => $ide,
			'idpr' => $idpr,
			'proceso' => $proceso,
			'tipo' => $tipo
		);
		if($this->db->insert('producto_pedido_empleado',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar_1($id,$cantidad,$terminado,$fecha_inicio,$fecha_fin,$observacion){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$c_terminado="`".$this->lib->encriptar_str("terminado")."`";
		$cantidad=$this->lib->encriptar_num($cantidad);
		$terminado=$this->lib->encriptar_num($terminado);
		$datos=array(
			$c_cantidad => $cantidad,
			$c_terminado => $terminado,
			'fecha_inicio' => $fecha_inicio,
			'fecha_fin' => $fecha_fin,
			'observacion' => $observacion
		);
		if($this->db->update('producto_pedido_empleado',$datos,array('idppe' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_pedido_empleado',['idppe' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_pedido_empleado");
		$max=$query->result();
		return $max[0]->max*1;
	}
}
/* End of file m_producto_pedido_empleado.php */
/* Location: ./application/models/m_producto_pedido_empleado.php*/