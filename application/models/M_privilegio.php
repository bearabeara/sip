<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_privilegio extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('privilegio');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('privilegio',['idpri' => $id]);
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->query("SELECT * FROM privilegio WHERE $col = '$val'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('privilegio',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_administrador($idus,$grupo){
		$c0="ad";
		$c1="ad1c, ad1r, ad1u, ad1d, ad1p";
		$c2="ad2r,ad2u";
		$c3="ad3c, ad3r, ad3u, ad3d";
		$cols=$c0.",".$c1.",".$c2.",".$c3;
		if($grupo!=""){
			if($grupo=="usuario"){ $cols=$c0.",".$c1;}
			if($grupo=="privilegio"){ $cols=$c0.",".$c2;}
			if($grupo=="config"){ $cols=$c0.",".$c3;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
        $this->db->where("idus = '$idus'");
        $query=$this->db->get();
        return $query->result();		
	}
	function get_almacen($idus,$grupo){
		$c0="al";
		$c1="al1c, al1r, al1u, al1d, al1p, al1a";
		$c2="al2r, al2d, al2p, al2a";
		$c3="al3r, al3d, al3p";
		$c4="al11c, al11r, al11u, al11d, al11p, al11a";
		$c5="al12r, al12u, al12a";
		$c6="al13r, al13u, al13a";
		$c7="al15c, al15r, al15u, al15d, al15a";
		$cols=$c0.",".$c1.",".$c2.",".$c3.",".$c4.",".$c5.",".$c6.",".$c7;
		if($grupo!=""){
			if($grupo=="almacen"){ $cols=$c0.",".$c1;}
			if($grupo=="historial_material"){ $cols=$c0.",".$c2;}
			if($grupo=="historial_producto"){ $cols=$c0.",".$c3;}
			if($grupo=="material"){ $cols=$c0.",".$c4;}
			if($grupo=="ingreso"){ $cols=$c0.",".$c5;}
			if($grupo=="salida"){ $cols=$c0.",".$c6;}
			if($grupo=="config_material"){ $cols=$c0.",".$c7;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
        $this->db->where("idus = '$idus'");
        $query=$this->db->get();
        return $query->result();		
	}
	function get_producion($idus,$grupo){
		$c0="idus,al,pr,mo,ca,cl,ac,ot,co,ad";
		$c1="pr1c, pr1r, pr1u, pr1d, pr1p";
		$c2="pr2c, pr2r, pr2u, pr2d, pr2p";
		$c5="pr5c, pr5r, pr5u, pr5d, pr5hc";
		$cols=$c0.",".$c1.",".$c2.",".$c5;
		if($grupo!=""){
			if($grupo=="producto"){ $cols=$c0.",".$c1;}
			if($grupo=="produccion"){ $cols=$c0.",".$c2;}
			if($grupo=="config"){ $cols=$c0.",".$c5;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
       if($idus!="" || $idus!=NULL){
       		$this->db->where("idus = '$idus'");
       }
        $query=$this->db->get();
        return $query->result();		
	}
	function get_cliente_proveedor($idus,$grupo){
		$c0="al,pr,mo,ca,cl,ac,ot,co,ad";
		$c1="cl1c, cl1r, cl1u, cl1d, cl1p";
		$c2="cl2c, cl2r, cl2u, cl2d, cl2p";
		$c5="cl5r";
		$cols=$c0.",".$c1.",".$c2.",".$c5;
		if($grupo!=""){
			if($grupo=="cliente"){ $cols=$c0.",".$c1;}
			if($grupo=="proveedor"){ $cols=$c0.",".$c2;}
			if($grupo=="config"){ $cols=$c0.",".$c5;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
        $this->db->where("idus = '$idus'");
        $query=$this->db->get();
        return $query->result();		
	}
	function get_capital_humano($idus,$grupo){
		$c0="al,pr,mo,ca,cl,ac,ot,co,ad";
		$c1="ca1c, ca1r, ca1u, ca1d, ca1p";
		$c2="ca2c, ca2r, ca2u, ca2p";
		$c3="ca3c, ca3r, ca3u, ca3d, ca3p";
		$c5="ca5r";
		$cols=$c0.",".$c1.",".$c2.",".$c5;
		if($grupo!=""){
			if($grupo=="empleado"){ $cols=$c0.",".$c1;}
			if($grupo=="planilla"){ $cols=$c0.",".$c2;}
			if($grupo=="directivo"){ $cols=$c0.",".$c3;}
			if($grupo=="config"){ $cols=$c0.",".$c5;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
        $this->db->where("idus = '$idus'");
        $query=$this->db->get();
        return $query->result();		
	}
	function get_movimiento($idus,$grupo){
		$c0="al,pr,mo,ca,cl,ac,ot,co,ad";
		$c1="mo1c, mo1r, mo1u, mo1d, mo1p, mo1a";
		$c2="mo2c, mo2r, mo2u, mo2d, mo2p";
		$c3="mo3c, mo3r, mo3u, mo3d, mo3p";
		$cols=$c0.",".$c1.",".$c2.",".$c3;
		if($grupo!=""){
			if($grupo=="pedido"){ $cols=$c0.",".$c1;}
			if($grupo=="venta"){ $cols=$c0.",".$c2;}
			if($grupo=="compra"){ $cols=$c0.",".$c3;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
        $this->db->where("idus = '$idus'");
        $query=$this->db->get();
        return $query->result();		
	}
	function get_usuario_producion($idus,$grupo){
		$cols_user="u.ci,u.usuario,u.tipo,u.fecha_ingreso,";
		$c0="p.idus, p.al,p.pr,p.mo,p.ca,p.cl,p.ac,p.ot,p.co,p.ad";
		$c1="p.pr1c, p.pr1r, p.pr1u, p.pr1d, p.pr1p";
		$c2="p.pr2r, p.pr2u";
		$c5="p.pr5c, p.pr5r, p.pr5u, p.pr5d, p.pr5hc";
		$cols=$c0.",".$c1.",".$c2.",".$c5;
		if($grupo!=""){
			if($grupo=="producto"){ $cols=$c0.",".$c1;}
			if($grupo=="config"){ $cols=$c0.",".$c5;}
		}
        $this->db->select($cols_user.$cols);
        $this->db->from("privilegio p");
        $this->db->join("usuario u","u.idus = p.idus","inner");
       if($idus!="" || $idus!=NULL){
       		$this->db->where("p.idus = '$idus'");
       }
        $query=$this->db->get();
        return $query->result();		
	}
	function get_material_indirecto($idus,$grupo){
		$c0="al,pr,mo,ca,cl,ac,ot,co,ad";
		$c1="ot1c, ot1r, ot1u, ot1d, ot1p";
		$c2="ot11r, ot11u";
		$c3="ot12r, ot12u";
		$c4="ot13r";
		$c5="ot2c, ot2r, ot2u, ot2d, ot2p";
		$c6="ot3r";
		$cols=$c0.",".$c1.",".$c2.",".$c3.",".$c4.",".$c5.",".$c6;
		if($grupo!=""){
			if($grupo=="indirectos"){ $cols=$c0.",".$c1;}
			if($grupo=="ingresos"){ $cols=$c0.",".$c2;}
			if($grupo=="salidas"){ $cols=$c0.",".$c3;}
			if($grupo=="historial"){ $cols=$c0.",".$c4;}
			if($grupo=="otros"){ $cols=$c0.",".$c5;}
			if($grupo=="config"){ $cols=$c0.",".$c6;}
		}
        $this->db->select($cols);
        $this->db->from("privilegio");
        $this->db->where("idus = '$idus'");
        $query=$this->db->get();
        return $query->result();		
	}
	function insertar($idus){
		$datos=array(
			'idus' => $idus
		);
		if($this->db->insert('privilegio',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$col,$pri){
		$datos=array(
			''.$col => $pri
		);
		if ($this->db->update('privilegio', $datos, array('idpri' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('privilegio',['idpri' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_privilegio.php */
/* Location: ./application/models/m_privilegio.php*/