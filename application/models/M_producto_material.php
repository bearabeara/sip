<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('producto_material');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_material',['idpm' => $id]);
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_material',[$col => $val]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_material WHERE idpm='$id'");
		return $query->result();
	}
	function get_complet($idpm){
		$cols="pm.idpm, pm.idp, pm.cantidad,pm.verificado,pm.observacion,
			m.idm,m.idco,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_material pm");
		$this->db->where("pm.idpm = '$idpm'");
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search_producto($idp,$col,$val){// en uso: PRODUCTO
		$cols="pm.idpm, pm.idp, pm.cantidad,pm.verificado,pm.observacion,
			m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c,c.abr as abr_c,
			u.idu, u.nombre as nombre_u,u.abr as abr_u";
		$this->db->select($cols);
		$this->db->from("producto_material pm");
		if($col!="" && $val!=""){
			if($col=="mi.idmi" || $col=="pm.idpm" || $col=="m.idm"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->where("pm.idp = '$idp'");
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material($col,$val){
		$cols="pm.idpm, pm.idp, pm.cantidad,pm.verificado,pm.observacion,
			m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_material pm");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="mi.idmi" || $col=="pm.idpm" || $col=="pm.idp"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto($col,$val){
		$cols="pm.idpm, pm.idp, pm.cantidad,pm.verificado,pm.observacion,
			m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_material pm");
		if($col!="" && $val!=""){
			if($col=="mi.idmi" || $col=="pm.idpm" || $col=="pm.idp"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->join('material m','pm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idp,$idm,$cantidad,$observacion){
		$datos=array(
			'idp' => $idp,
			'idm' => $idm,
			'cantidad' => $cantidad,
			'observacion' => $observacion,
		);
		if($this->db->insert('producto_material',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$cantidad,$verificado,$observacion){
		$datos=array(
			'cantidad' => $cantidad,
			'verificado' => $verificado,
			'observacion' => $observacion
		);
		if($this->db->update('producto_material',$datos,array('idpm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar2($id,$idm,$cantidad,$observacion){
		$datos=array(
			'idm' => $idm,
			'cantidad' => $cantidad,
			'observacion' => $observacion
		);
		if($this->db->update('producto_material',$datos,array('idpm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_material',['idpm' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}
/* End of file m_producto_material.php */
/* Location: ./application/models/m_producto_material.php*/