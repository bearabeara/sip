<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class lib{
	/*---- MANEJO DE ARCHIVO BIOMETRICO---- */
function para_insertar_biometrico($empleado,$tipo,$datos){//preparamos los datos del archivo para insertar a la BD hora_biometrico
	$resp=array();//resultado final
	$horaAct="";
	$horaAnt="";
	$fechaAct="";
	$fechaAnt="";
	foreach($datos as $row):
		if($this->hora_empleado($empleado,$row,$tipo)){
			$save=false;
			$fecha_hora=explode(" ", $row[4]);//dividimos en fecha y hora, 01/12/2000 08:00
			$vf=explode("/", $fecha_hora[0]);
			$fecha=$vf[2].'-'.$this->dosDigitos($vf[1]).'-'.$this->dosDigitos($vf[0]);
			if($this->hora_iguales($fecha_hora[1],$horaAnt)){
				if($fecha!=$fechaAnt){
					$save=true;
				}
			}else{
				$save=true;
			}
			if($save){ $resp[]=array('fecha' => $fecha,'hora' => $fecha_hora[1]);}
			$horaAnt=$fecha_hora[1];
			$fechaAnt=$fecha;
		}
		endforeach;
		return json_encode($resp);
	}
function hora_empleado($empleado,$fila,$tipo){//tipo de busqueda biometrico, por codigo(0) o cedula de identidad(1)
	$result=false;
	if($tipo==0){//Por codigo
		if($empleado->codigo==$fila[1]){
			$result=true;
		}
	}else{
		if($tipo==1){//Por cedula de identidad
			if($empleado->ci==$fila[2]){
				$result=true;
			}
		}
	}
	return $result;
}
function hora_iguales($hora1,$hora2){//comprar si dos horas son iguales segun su rango de error de 2min
	if($hora2!=NULL){
		$hra1=new DateTime($hora1);
		$hra2=new DateTime($hora2);
		$diff=new DateTime();
		$diff=$hra1->diff($hra2);
		$minutos=(($diff->h)*60)+$diff->i+(($diff->s)/60);
		if($minutos>5){
			return false; 
		}else{
			return true; 
		}
	}else{
		return false;
	}
}
/*---- END MANEJO DE ARCHIVO BIOMETRICO---- */
/*--- Manejo de costo de produccion ---*/
function costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,$idpim){
	$resultado = array();$cantidad=0;
	for($i=0; $i < count($accesorios) ; $i++){ $material=$accesorios[$i]; //calculando costo de materiales como argollas,cierres, etc;
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $material->cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
	}
	for($i=0; $i < count($materiales_liquidos) ; $i++){ $material=$materiales_liquidos[$i]; //calculando costo de materiales que estans relacionados a las piezas, comos clefa, hilo, pintura, etc;
		if($material->equivalencia>0){
			$cantidad=($material->area*$material->variable)/$material->equivalencia;
		}
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
	}
	for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i]; //calculando costo de materiales que estans relacionados con el grupo de piezas como: aguayo, cuero, tela, carton, etc.;
		if($material->color_producto!=1){
			if($material->equivalencia>0){
				$cantidad=$material->area/$material->equivalencia;
			}
			$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
		}
	}
	if(isset($idpim)){
		if($idpim!="" && $idpim!=NULL){
			for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i];//calculando costo de materiales que estans relacionados con el grupo de piezas como: aguayo, cuero, tela, carton, etc.;
				if($material->idpim==$idpim){
					if($material->equivalencia>0){
						$cantidad=$material->area/$material->equivalencia;
					}
					
					$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
				}
			}
		}
	}
	for($i=0; $i < count($indirectos) ; $i++) { $material=$indirectos[$i]; //calculando costo de materiales que estans relacionados a las piezas, comos clefa, hilo, pintura, etc;
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => "vacio",'costo_unitario' => $material->costo,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => '','codigo_color' =>'','fotografia' => $material->fotografia,'idmi' => $material->idmi);
	}
	return json_encode($resultado);
}
function costo_materiales_color($materiales){
	$resultado = array();$cantidad=0;
	for($i=0; $i < count($materiales) ; $i++){ $material=$materiales[$i]; //calculando costo de materiales que estans relacionados con el grupo de piezas como: aguayo, cuero, tela, carton, etc.;
		if($material->color_producto==1){
			if($material->equivalencia>0){
				$cantidad=$material->area/$material->equivalencia;
			}
			$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->nombre_u,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->nombre_color,'codigo_color' => $material->codigo_color,'fotografia' => $material->fotografia,'idmi' => $material->idmi);
		}
	}
	return json_encode($resultado);
}
function costo_procesos($procesos,$procesos_pieza){
	$resultado = array();
	for($i=0; $i < count($procesos) ; $i++){ $proceso=$procesos[$i]; //calculando costo de materiales como argollas,cierres, etc;
		$resultado[count($resultado)]=array('nombre' => $proceso->nombre.": ".$proceso->sub_proceso,'cantidad' => '','segundos' => $proceso->tiempo_estimado,'costo' => $proceso->costo);
	}
	for($i=0; $i < count($procesos_pieza) ; $i++){ $proceso=$procesos_pieza[$i]; //calculando costo de materiales como argollas,cierres, etc;
		$resultado[count($resultado)]=array('nombre' => $proceso->nombre.": ".$proceso->nombre_pi,'cantidad' => $proceso->unidades,'segundos' => $proceso->tiempo_estimado,'costo' => $proceso->costo);
	}
	return json_encode($resultado);
}

function costo_total_produccion($accesorios,$materiales,$idpim,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza){//CALCULANDO EL COSTO DE PRODUCCION DE UNA PRODUCTO SEGUN SU COLOR
	$costo_material=json_decode($this->costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,NULL));
	$costo_total=0;
	foreach ($costo_material as $clave => $valor){
		if($valor->cantidad=="vacio"){
			$costo_total+=$valor->costo_unitario;//solo para el caso de materiales indirectos
		}else{
			$costo_total+=($valor->cantidad*$valor->costo_unitario);
		}
	}
	$costo_total+=$this->costo_material_color($materiales,$idpim);
	$procesos=json_decode($this->costo_procesos($procesos,$procesos_pieza));
	foreach ($procesos as $clave => $valor){
		$costo_total+=$valor->costo;
	}
	return $costo_total;
}
function costo_material_color($materiales,$idpim){
	$costo=0;$cantidad=0;
	for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i]; //calculando costo de materiales que esta relacionados con el color delproducto;
		if($material->idpim==$idpim){
			if($material->equivalencia>0){
				$cantidad=$material->area/$material->equivalencia;
			}
			$costo=$cantidad*$material->costo_unitario;
			break;
		}
	}
	return $costo;
}
function costo_total_produccion_sc($accesorios,$materiales,$materiales_liquidos,$indirectos,$procesos,$procesos_pieza){//CALCULANDO EL COSTO DE PRODUCCION DE UNA PRODUCTO SEGUN TODOS SUS COLORES
	$costo_material=json_decode($this->costo_materiales($accesorios,$materiales,$materiales_liquidos,$indirectos,NULL));
	$costo_total=0;
	foreach ($costo_material as $clave => $valor){
		if($valor->cantidad=="vacio"){
			$costo_total+=$valor->costo_unitario;//solo para el caso de materiales indirectos
		}else{
			$costo_total+=($valor->cantidad*$valor->costo_unitario);
		}
	}
	$procesos=json_decode($this->costo_procesos($procesos,$procesos_pieza));
	foreach ($procesos as $clave => $valor){
		$costo_total+=$valor->costo;
	}
	$colores_producto=json_decode($this->costo_materiales_color($materiales));
	$resultado=array();
	foreach ($colores_producto as $valor => $material) {
		$total=$costo_total+($material->costo_unitario*$material->cantidad);
		$resultado[count($resultado)]=array('nombre' => $material->nombre,'cantidad' => $material->cantidad,'costo_unitario' => $material->costo_unitario,'unidad' => $material->unidad,'abreviatura' => $material->abreviatura,'codigo' => $material->codigo,'color' => $material->color,'codigo_color' => $material->codigo_color,'costo_total' => $total);
	}
	return json_encode($resultado);
}
/*--- End Manejo de costo de produccion ---*/
/*--- Manejo de horas por empleado ---*/
function horas_empleado($ide,$j_horas){
	$horas=json_decode($j_horas);
	$resultado = array();
	foreach ($horas as $clave => $valor){
		if($valor->ide==$ide){
			$resultado[]=array('idhb' => $valor->idhb, 'fecha' => $valor->fecha,'hora' => $valor->hora);
		}
	}
	return $resultado;
}
	function horas_dia($j_horas,$empleado,$j_feriados,$fecha1,$fecha2){//calculo de las horas trabajadas por dia
		//$horas_trabajo,$tipo_contrato,$c_feriado
		//echo($empleado->nombre);
		$inicio=strtotime($fecha1);
		$horas=array();
		while($inicio<=strtotime($fecha2)){
			$fecha=date('Y-m-d',$inicio);
			$hras_dia=$this->search_horas($j_horas,$fecha);
			$horas_trabajadas=$this->horas_trabajo_dia(json_encode($hras_dia),$empleado,$j_feriados,$fecha);//calculando las horas trabajadas en el dia
			$horas[]=array('fecha'=>$fecha,'horas'=>$hras_dia,'horas_trabajadas'=>$horas_trabajadas);
			$inicio=strtotime('+1 day',$inicio);
		}
		return json_encode($horas);
	}
	function search_horas($j_horas,$fecha){
		$j_horas=json_decode($j_horas);
		$horas= array();
		$cont=0;
		foreach($j_horas as $clave => $valor){
			if($valor->fecha==$fecha){
				if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
				$hora_redondo=$this->redondeo_hora($valor->hora,$sigla);
				$horas[]=array('idhb'=>$valor->idhb,'tipo'=>$tipo,'sigla'=>$sigla,'hora' => $valor->hora,'hora_redondo' => $hora_redondo);
				$cont++;
			}
		}
		return $horas;
	}
	function horas_trabajo_dia($j_horas_dia,$empleado,$j_feriados,$fecha){
		//$horas_trabajo,$tipo_contrato,$c_feriado
		$vf=explode("-", $fecha);
		$horas_trabajadas=json_decode($this->tiempo_trabajo_dia($j_horas_dia,$empleado,$j_feriados,$fecha));
		$resp=array('horas_trabajadas' => $horas_trabajadas->horas,'horas_adeudadas' => $horas_trabajadas->descuento,'horas_trabajadas_no_redondo' => $horas_trabajadas->horas_original);
		return $resp;
	}
	function redondeo_hora($hora,$tipo){//redondeando la hora
		$horaFinal="00:00";
		switch ($tipo){
			case 'E':
			$hra=explode(":", $hora);
			if($hra[1]>=0 & $hra[1]<=10){ $horaFinal=$hra[0].":00";}
			if($hra[1]>=11 & $hra[1]<=40){ $horaFinal=$hra[0].":30";}
			if($hra[1]>=41 & $hra[1]<=59){ $horaFinal=($hra[0]+1).":00";}
			break;
			case 'S':
			$hra=explode(":", $hora);
			if($hra[1]>=0 & $hra[1]<=29){ $horaFinal=$hra[0].":00";}
			if($hra[1]>=30 & $hra[1]<=59){ $horaFinal=$hra[0].":30";}
			break;
		}
		return $horaFinal;
	}
	function tiempo_trabajo_dia($j_horas_dia,$empleado,$j_feriados,$fecha){
		//$horas_trabajo,$tipo_contrato,$c_feriado
		//verificando si es sabado
		$vf=explode("-", $fecha);
		$dia=$this->es_habil_domingo_sabado_feriado($fecha,$j_feriados);
		if($empleado->c_feriado==0){//se calculara con feriados como dia libre
			if($dia=="D" || $dia=="F"){
				$horas_descuento=0;
			}else{
				if($dia=='S' || $dia=="MF"){//es sabado o medio feriado
					if($empleado->tipo_contrato==0){//tiempo completo
						$horas_descuento=$empleado->horas/2;	
					}else{
						$horas_descuento=$empleado->horas;	
					}
				}else{
					$horas_descuento=$empleado->horas;
				}
			}
		}else{//se calculara los feriados como dia de trabajo normal
			if($dia=="D"){
				$horas_descuento=0;
			}else{
				if($dia=='S'){//es sabado
					if($empleado->tipo_contrato==0){//tiempo completo
						$horas_descuento=$empleado->horas/2;	
					}else{
						$horas_descuento=$empleado->horas;	
					}
				}else{
					$horas_descuento=$empleado->horas;
				}
			}
		}
		$horas=json_decode($j_horas_dia);
		$hora1="";$hora1_org="";
		$hora2="";$hora2_org="";
		$total_hras=0;$total_hras_org=0;
		foreach ($horas as $key => $hra) {
			$hora2=$hra->hora_redondo;
			$hora2_org=$hra->hora;
			if($hora1==""){
				$hora1=$hora2;
				$hora1_org=$hora2_org;
			}else{
				$hra0=new DateTime($hora1);//hora de entrada
				$hra1=new DateTime($hora2);//hora de salida
				$diff=new DateTime();//diferencia entre ambas horas
				$diff=$hra0->diff($hra1);
				$diferencia=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
				$total_hras+=$diferencia;//sumamos al tiempo trabajado en el dia
				// para el caso de hora original
				$hra0=new DateTime($hora1_org);//hora de entrada
				$hra1=new DateTime($hora2_org);//hora de salida
				$diff=new DateTime();//diferencia entre ambas horas
				$diff=$hra0->diff($hra1);
				$diferencia=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
				$total_hras_org+=$diferencia;//sumamos al tiempo trabajado en el dia
				$hora1="";$hora1_org="";
			}
		}
		$descuento=$horas_descuento-$total_hras;
		$result = array('horas' => $total_hras, 'descuento' => $descuento,'horas_original' => $total_hras_org);
		return json_encode($result);
	}
	function sueldo_dia_hora($j_cant_dias,$tipo_contrato,$horas_trabajo,$salario_mes){//calculamos dos tipos de horas, para sueldos y para calculo de tiempo de proceso (con tiempo real)
		$cant_dias=json_decode($j_cant_dias);
		if($tipo_contrato==0){//es empleado a tiempo completo
			$horas=($cant_dias->habiles-$cant_dias->sabados)*$horas_trabajo;//solo horas de lunes a viernes
			$horas+=$cant_dias->sabados*($horas_trabajo/2);//solo horas de sabados
		}else{
			$horas=$cant_dias->habiles*$horas_trabajo;
		}
		$por_hora=$salario_mes/($cant_dias->habiles*$horas_trabajo);
		$por_dia=$salario_mes/$cant_dias->habiles;
		$por_hora_real=$salario_mes/$horas;
		$result = array('por_hora' => $por_hora,'por_hora_real' => $por_hora_real,'por_dia' => $por_dia);
		return $result;
	}
	function total_horas_trabajo($j_horas,$empleado,$j_feriados,$fecha1,$fecha2,$sueldo){//calculando el total de las horas trabajadas segun horas biometrico(EN USO VER PLANILLA)
		//$horas_trabajo,$tipo_contrato,$c_feriado
		$horas_dia=json_decode($this->horas_dia($j_horas,$empleado,$j_feriados,$fecha1,$fecha2));
		$h_trabajadas=0;
		$h_adeudadas=0;
		$h_no_redondo=0;
		$control=true;//control aque los marcados en el dia sega de catidad par
		$dias_habiles_periodico=26;
		foreach($horas_dia as $clave => $dia){
			$horas_trabajadas=json_decode(json_encode($dia->horas_trabajadas));
			$h_trabajadas+=($horas_trabajadas->horas_trabajadas);
			$h_adeudadas+=$horas_trabajadas->horas_adeudadas;
			$h_no_redondo+=$horas_trabajadas->horas_trabajadas_no_redondo;
			if(count($dia->horas)%2!=0){$control=false;}
		}
		//if($dias_habiles==NULL || $dias_habiles==""){ $dias_habiles=$this->dias_habiles_2($fecha1,$fecha2,$c_feriado,$j_feriados); }
		$meses=$this->meses($fecha1,$fecha2);
		$sueldo_total=$sueldo*$meses;
		$dias_habiles=$dias_habiles_periodico*$meses;
		$por_dia=$sueldo_total/$dias_habiles;
		$por_dia=$por_dia;
		$por_hora=$por_dia/$empleado->horas;
		//obteniendo la penalizacion por faltas mayores a una semana
		$penalizacion=json_decode($this->penalizacion($empleado,$h_adeudadas));
		$descuento=($h_adeudadas+$penalizacion->penalizacion)*$por_hora;//sumando penalizacion 
		$pagar=$sueldo_total-$descuento;
		$pagar_no_redondo=$h_no_redondo*$por_hora;
		if($h_trabajadas<=0){ $pagar=0;$pagar_no_redondo=0; }
		$resultado = array('sueldo_total' => $sueldo_total,'dias_habiles' => $dias_habiles,'por_dia' => $por_dia,'horas_trabajo' => $empleado->horas,'por_hora' => $por_hora,'descuento' => $descuento,'hra_trabajada' => $h_trabajadas,'hra_adeudadas' => $h_adeudadas,'penalizacion' => $penalizacion->penalizacion,'semanas_faltadas' => $penalizacion->semana,'sueldo' => $pagar,'hra_trabajada_no_redondo' => $h_no_redondo,'sueldo_no_redondo'=>$pagar_no_redondo,'control'=>$control);
		return json_encode($resultado);
	}
	function penalizacion($empleado,$h_adeudadas){//calcaula la penalizaicion por faltas
		//$tipo_contrato,$horas_trabajo
		$penalizacion=0;
		$semana=0;
		if($empleado->c_descuento==0){
			if($empleado->tipo_contrato==0){ $semana=($empleado->horas*5)+($empleado->horas/2);}else{$semana=$empleado->horas*6;}
			if($h_adeudadas>$semana){ $penalizacion=floor($h_adeudadas/$semana)*($empleado->horas/2);}
		}
		$result = array('penalizacion' => $penalizacion, 'semana' => $semana);
		return json_encode($result);		
	}
	function meses($fecha1,$fecha2){
		$meses=0;
		$inicio=strtotime($fecha1);
		while($inicio<=strtotime($fecha2)){
			$meses++;
			$inicio=strtotime("+1 month",$inicio);
		}
		return$meses;
	}
	function add_json_horas($j_horas,$ide,$fecha,$hora,$pos){
		$horas=json_decode($j_horas);
		$result=array();
		$sw=true;
		foreach($horas as $key => $h){
			if($pos=="ini"){
				if($sw){ $result[] = array('idhb' => '0','ide' => $ide,'fecha' => $fecha, 'hora' => $hora); $sw=false; }
			}
			$result[] = array('idhb' => $h->idhb,'ide' => $h->ide,'fecha' => $h->fecha, 'hora' => $h->hora);
		}
		if($pos=="fin"){ $result[] = array('idhb' => '0','ide' => $ide,'fecha' => $fecha, 'hora' => $hora); }
		return json_encode($result);
	}
	function horas_trabajo_modificado($j_horas,$ide,$fecha,$hora,$pos,$horas_trabajo,$tipo_contrato,$j_feriados){//calcula la horas trabajadas anterior o posterior. EN USO TAREA->CALCULAR TIEMPO DE TRABAJO
		$j_horas_antes=$this->add_json_horas($j_horas,$ide,$fecha,$hora,$pos);
		$j_horas_antes=json_encode($this->search_horas($j_horas_antes,$fecha));
		$res_horas=$this->tiempo_trabajo_dia($j_horas_antes,$horas_trabajo,$tipo_contrato,$j_feriados,$fecha);// devuelde las horas antes o despues de la fecha de inicio de trabajo y finalizacion
		return $res_horas;
	}
	function que_dia_es($dia,$mes,$anio){
		$dia=date('w', mktime(0,0,0,$mes,$dia,$anio));
		return $dia;
	}
	function dias_habiles($anio,$mes,$feriados){
		$habil=0;
		$sabado=0;
		$max=$this->ultimo_dia($mes,$anio);
		for($i=1; $i <= $max; $i++){ 
			$dia=$this->que_dia_es($i,$mes,$anio);
			$fecha=$anio."-".$mes."-".$this->dosDigitos($i);
			if(!$this->existe_fecha($feriados,$fecha)){
				if($dia!=0){
					if($dia==6){
						$sabado++;
					}
					$habil++;
				}
			}
		}
		$dia = array('sabados' => $sabado, 'habiles' => $habil);
		return $dia;
	}
	function dias_habiles_2($fecha1,$fecha2,$c_feriado,$j_feriados){
		$inicio=strtotime($fecha1);
		$habil=0;
		while ($inicio<=strtotime($fecha2)){
			$fecha=date('Y-m-d',$inicio);
			if($c_feriado!=1){
				if(!$this->existe_fecha($j_feriados,$fecha)){
					$vf=explode("-", $fecha);
					$dia=$this->que_dia_es($vf[2],$vf[1],$vf[0]);
					if($dia!=0){
						$habil++;
					}
				}
			}else{
				$vf=explode("-", $fecha);
				$dia=$this->que_dia_es($vf[2],$vf[1],$vf[0]);
				if($dia!=0){
					$habil++;
				}
			}
			$inicio=strtotime('+1 day',$inicio);
		}
		return $habil;
	}
















	
	function horas_fecha($ide,$horas,$fecha){//sacamos las horas de los registros de la BD $horas en formato array bidimencional
		$resp = array();
		$cont=0;
		$tipo="";
		$sigla="";
		foreach (json_decode($horas) as $key => $hora) {
			if($hora->fecha==$fecha && $hora->ide==$ide){
				if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
				$redondo=$this->redondeo_hora($hora->hora,$sigla);
				$resp[$cont]=array('tipo' => $tipo,'sigla' => $sigla,'hora' => $hora->hora,'redondo' => $redondo,'fecha' => $fecha);
				$cont++;
			}
		}
		return $resp;
	}
	function horas_fecha_adicionar($ide,$horas,$fecha,$hora_adicionar,$antes_despues){//sacamos las horas de los registros de la BD $horas en formato array bidimencional
		$resp = array();
		$cont=0;
		$tipo="";
		$sigla="";
		if($antes_despues=='despues'){
			$redondo=$this->redondeo_hora($hora_adicionar,"E");
			$resp[$cont]=array('tipo' => 'Entrada','sigla' => "E",'hora' => $hora_adicionar,'redondo' => $redondo,'fecha' => $fecha);
			$cont++;
		}
		foreach (json_decode($horas) as $key => $hora) {
			if($hora->fecha==$fecha && $hora->ide==$ide){
				if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
				$redondo=$this->redondeo_hora($hora->hora,$sigla);
				$resp[$cont]=array('tipo' => $tipo,'sigla' => $sigla,'hora' => $hora->hora,'redondo' => $redondo,'fecha' => $fecha);
				$cont++;
			}
		}
		if($antes_despues=='antes'){
			if($cont%2==0){ $tipo='Entrada'; $sigla="E"; }else{ $tipo='Salida'; $sigla="S"; }
			$redondo=$this->redondeo_hora($hora_adicionar,$sigla);
			$resp[$cont]=array('tipo' => $tipo,'sigla' => $sigla,'hora' => $hora_adicionar,'redondo' => $redondo,'fecha' => $fecha);
			$cont++;
		}
		return $resp;
	}

	function calcula_horas_trabajo($v_horas,$tipo_contrato,$horas_trabajo,$salario_mes,$fecha,$j_feriados){ //tipo de dias H,S,D,F
		$resp = array();
		$horas_trabajadas="";
		$horas_trabajadas_redondo="";
		$horas_adeudadas="";
		$horas_adeudadas_redondo="";
		$vf=explode("-", $fecha);
		$dias=json_encode($this->dias_habiles($vf[0],$vf[1],$j_feriados));
		$sueldo_hora=json_encode($this->sueldo_por_hora_dia($dias,$tipo_contrato,$horas_trabajo,$salario_mes));
		$horas_trabajadas=json_decode(json_encode($this->horas_trabajo($v_horas,$tipo_contrato,$horas_trabajo,$sueldo_hora,$fecha)));
		$resp = array('horas_trabajadas' => $horas_trabajadas->horas,'horas_adeudadas' => $horas_trabajadas->descuento,'por_hora' => $horas_trabajadas->por_hora,'por_dia' => $horas_trabajadas->por_dia);
		return $resp;
	}
	function sueldo_por_hora_dia($j_cant_dias,$tipo_contrato,$horas_trabajo,$salario_mes){//calculamos dos tipos de horas, para sueldos y para calculo de tiempo de proceso (con tiempo real)
		$cant_dias=json_decode($j_cant_dias);
		if($tipo_contrato==0){//es empleado a tiempo completo
			$horas=($cant_dias->habiles-$cant_dias->sabados)*$horas_trabajo;//solo horas de lunes a viernes
			$horas+=$cant_dias->sabados*($horas_trabajo/2);//solo horas de sabados
		}else{
			$horas=$cant_dias->habiles*$horas_trabajo;
		}
		$por_hora=$salario_mes/($cant_dias->habiles*$horas_trabajo);
		$por_dia=$salario_mes/$cant_dias->habiles;
		$por_hora_real=$salario_mes/$horas;
		$result = array('por_hora' => $por_hora,'por_hora_real' => $por_hora_real,'por_dia' => $por_dia);
		return $result;
	}
	function horas_trabajo($v_horas,$tipo_contrato,$horas_trabajo,$j_sueldo_hora,$fecha){
		$sueldo_hora=json_decode($j_sueldo_hora);
		//verificando si es sabado
		$vf=explode("-", $fecha);
		$dia=$this->que_dia_es($vf[2],$vf[1],$vf[0]);
		if($dia==6){//es sabado
			if($tipo_contrato==0){//tiempo completo
				$horas_descuento=$horas_trabajo/2;	
			}else{
				$horas_descuento=$horas_trabajo;	
			}
		}else{
			$horas_descuento=$horas_trabajo;
		}
		if(count($v_horas)>1 && count($v_horas)%2==0){
			$horas=json_decode(json_encode($v_horas));
			$hora1="";
			$hora2="";
			$total_hras=0;
			foreach ($horas as $key => $hra) {
				$hora2=$hra->redondo;
				if($hora1==""){
					$hora1=$hora2;
				}else{
					$hra0=new DateTime($hora1);//hora de entrada
					$hra1=new DateTime($hora2);//hora de salida
					$diff=new DateTime();//diferencia entre ambas horas
					$diff=$hra0->diff($hra1);
					$diferencia=$diff->h+($diff->i/60)+0;//calculamos el tiempo trabajado
					$total_hras+=$diferencia;//sumamos al tiempo trabajado en el dia
					$hora1="";
				}
			}
			$result = array('horas' => $total_hras, 'descuento' => $horas_descuento-$total_hras,'por_hora' => $sueldo_hora->por_hora,'por_dia' => $sueldo_hora->por_dia);
		}else{
			$result = array('horas' => '0', 'descuento' => $horas_descuento,'por_hora' => $sueldo_hora->por_hora,'por_dia' => $sueldo_hora->por_dia);
		}
		return $result;
	}
	/*--- End manejo de horas por empleado ---*/
	/*--- Manejo de horas por empleado produccion ---*/
	function calculo_total_horas_trabajo($j_horas,$feriados,$empleado,$fini,$ffin,$horas_antes,$horas_despues){
		$feriados=json_encode($feriados);
		$vf1=explode(" ", $fini);$fecha1=$vf1[0];
		$vf2=explode(" ", $ffin);$fecha2=$vf2[0];
		if(!empty($empleado)){
			$empleado=$empleado[0];
			$hora_antes=0;
			$tota_antes=0;
			$hora_despues=0;
			$total_despues=0;
			if(count($horas_antes)>0){
				$j_horas_antes=json_encode($horas_antes);
				if(count($horas_antes)%2==0){
					$v_hora_empleado=$this->horas_fecha($empleado->ide,$j_horas_antes,$fecha1);//horas en el dia
				}else{
					$v_hora_empleado=$this->horas_fecha_adicionar($empleado->ide,$j_horas_antes,$fecha1,$vf1[1],'antes');//horas en el dia
				}
				$antes=json_decode(json_encode($this->calcula_horas_trabajo($v_hora_empleado,$empleado->tipo_contrato,$empleado->horas,$empleado->salario,$fecha1,$feriados)));//totales por dia
				$hora_antes=$antes->horas_trabajadas;
				$total_antes=($antes->horas_adeudadas*-1*$antes->por_hora)+$antes->por_dia;
			}
			if(count($horas_despues)>0){
				$j_horas_despues=json_encode($horas_despues);
				if(count($horas_despues)%2==0){
					$v_hora_empleado=$this->horas_fecha($empleado->ide,$j_horas_despues,$fecha2);//horas en el dia
				}else{
					$v_hora_empleado=$this->horas_fecha_adicionar($empleado->ide,$j_horas_despues,$fecha2,$vf2[1],'despues');//horas en el dia
				}
				$despues=json_decode(json_encode($this->calcula_horas_trabajo($v_hora_empleado,$empleado->tipo_contrato,$empleado->horas,$empleado->salario,$fecha2,$feriados)));//totales por dia
				$hora_despues=$despues->horas_trabajadas;
				$total_despues=($despues->horas_adeudadas*-1*$despues->por_hora)+$despues->por_dia;
			}
			$inicio=strtotime($fecha1);
			$total_horas_trabajadas=0;
			$total_sueldo=0;
			while ($inicio<=strtotime($fecha2)){
				$fecha=date('Y-m-d',$inicio);
				$v_hora_empleado=$this->horas_fecha($empleado->ide,$j_horas,$fecha);//horas en el dia
				$result=json_decode(json_encode($this->calcula_horas_trabajo($v_hora_empleado,$empleado->tipo_contrato,$empleado->horas,$empleado->salario,$fecha,$feriados)));//totales por dia
				$total_horas_trabajadas+=$result->horas_trabajadas;
				//SELECT * FROM `hora_biometrico` WHERE fecha='2015-05-04' and ide='11' and hora<='14:15:46'
				//$horas_adeudadas=$result->horas_adeudadas;
				$total_sueldo+=($result->horas_adeudadas*-1*$result->por_hora)+$result->por_dia;
				$inicio=strtotime('+1 day',$inicio);
			}
		}
		$result = array('horas_trabajadas' => ($total_horas_trabajadas-$hora_antes-$hora_despues),'sueldo' => ($total_sueldo-$total_antes-$total_despues));
		return $result;
	}
	/*--- End manejo de horas por empleado produccion ---*/
	/*--- Manejo de fechas ---*/
	function es_habil_domingo_sabado_feriado($fecha,$j_feriados){//calcula si es dia habil feriado o domingo
		$resp="";
		$v=explode("-", $fecha);
		$fer=$this->search_feriado($j_feriados,$fecha);
		if($fer==""){//en caso de no ser feriado
			$diaN=$this->que_dia_es($v[2],$v[1],$v[0]);
				if($diaN==0){//si es domingo
					$resp="D";
				}else{// dia normal lunes a viernes;
					if($diaN==6){
						$resp="S";
					}else{
						$resp="H";
					}
				}
			}else{
				$resp=$fer;
			}
			return $resp;
		}
	function search_feriado($j_fechas,$fecha){// busca si existe la fecha en los feriados F(Feriado completo), MF(Medio feriado)
		$result="";
		$fechas=json_decode($j_fechas);
		foreach ($fechas as $key => $fila) {
			if($fecha==$fila->fecha){
				if($fila->tipo==0){
					$result="F";
				}else{
					$result="MF";
				}
				break;
			}
		}
		return $result;
	}
	function existe_fecha($j_fechas,$fecha){// busca si existe la fecha en el array asociativo: 2000-12-31
		$fechas=json_decode($j_fechas);
		foreach ($fechas as $key => $fila) {
			if($fecha==$fila->fecha && $fila->tipo!=1){//si exite la fecha y no es medio feriado
				return true;
			}
		}
		return false;
	}

	function ultimo_dia($mes,$anio){
		$final=date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
		return $final;
	}
	function obtiene_meses($fecha1,$fecha2){
		$resp=array();
		$inicio=strtotime($fecha1);
		while ($inicio<=strtotime($fecha2)) {
			$resp[count($resp)]=date('Y-m',$inicio);
			$inicio=strtotime('+1 day',$inicio);
		}
		$resp=array_unique($resp);
		return $resp;
	}
	function mes_literal($mes){
		$mes=$this->dosDigitos($mes);
		$m="";
		switch ($mes) {
			case '01':  $m="ENERO"; break;
			case '02':  $m="FEBRERO"; break;
			case '03':  $m="MARZO"; break;
			case '04':  $m="ABRIL"; break;
			case '05':  $m="MAYO"; break;
			case '06':  $m="JUNIO"; break;
			case '07':  $m="JULIO"; break;
			case '08':  $m="AGOSTO"; break;
			case '09':  $m="SEPTIEMBRE"; break;
			case '10':  $m="OCTUBRE"; break;
			case '11':  $m="NOVIEMBRE"; break;
			case '12':  $m="DICIEMBRE"; break;
		}
		return $m;
	}
	function format_date($fecha,$cond){
		if($fecha!="" && $fecha!=NULL && $fecha!="0000-00-00" && $fecha!="0000-00-00 00:00:00"){
			$f=explode("-", $fecha);
			if(count($f)>=3){
				$anio=$f[0];
				$mes=$f[1];
				$f2=explode(" ", $f[2]);
				if(count($f2)>1){
					$dia=$f2[0];
					$hora=$f2[1];
				}else{
					$dia=$f[2];
					$hora="";
				}
				switch ($cond) {
					case 'Y-m-d': $resp=$anio."-".$mes."-".$dia; if($hora!=""){ $resp.=" ".$hora;} break;
					case 'Y-ml-d': $resp=$anio."-".$this->ini_mayuscula($this->mes_literal($mes))."-".$dia;if($hora!=""){ $resp.=" ".$hora;} break;
					case 'd-m-Y': $resp=$dia." - ".$mes." - ".$anio;  if($hora!=""){ $resp.=" ".$hora;}break;
					case 'd-ml-Y': $resp=$dia." - ".$this->ini_mayuscula($this->mes_literal($mes))." - ".$anio;if($hora!=""){ $resp.=" ".$hora;} break;
					case 'Y/m/d': $resp=$anio."/".$mes."/".$dia;  if($hora!=""){ $resp.=" ".$hora;} break;
					case 'Y/ml/d': $resp=$anio."/".$this->ini_mayuscula($this->mes_literal($mes))."/".$dia; if($hora!=""){ $resp.=" ".$hora;} break;
					case 'd/m/Y': $resp=$dia."/".$mes."/".$anio; if($hora!=""){ $resp.=" ".$hora;} break;
					case 'd/ml/Y': $resp=$dia." / ".$this->ini_mayuscula($this->mes_literal($mes))." / ".$anio; if($hora!=""){ $resp.=" ".$hora;} break;
					case 'd ml Y': $resp=$dia." de ".$this->ini_mayuscula($this->mes_literal($mes))." de ".$anio; if($hora!=""){ $resp.=" a las ".$hora;} break;
					case 'dl ml Y': $resp=$this->nombre_de_dia($this->que_dia_es($dia,$mes,$anio)).", ".($dia*1)." de ".$this->ini_mayuscula($this->mes_literal($mes))." de ".$anio; if($hora!=""){ $resp.=" a las ".$hora;} break;
					case 'dl ml2 Y': $resp=$this->nombre_de_dia($this->que_dia_es($dia,$mes,$anio)).", ".$dia." de ".$this->ini_mayuscula($this->mes_literal($mes))." de ".$anio; if($hora!=""){ $resp.=" a las ".$hora;} break;
					default: $resp=$anio."-".$mes."-".$dia; break;
				}
			}else{
				$resp="";
			}
		}else{
			$resp="";
		}
		return $resp;
	}
	function tiempo_transcurrido($seg1,$seg2,$formato){
		$usuario="";$fecha=NULL;
		if(!empty($seg1) || !empty($seg2)){
			if(!empty($seg1) && !empty($seg2)){
				$v1=explode(" ", $seg1[0]->fecha);
				$v2=explode(" ", $seg2[0]->fecha);
				if(count($v1)==2 && count($v2)==2){
					$fecha=$seg1[0]->fecha;
					$usuario=$seg1[0]->usuario;
					if($seg1[0]->fecha<$seg2[0]->fecha){
						$fecha=$seg2[0]->fecha;
						$usuario=$seg2[0]->usuario;
					}

				}else{
					if(count($v1)==2){
						$fecha=$seg1[0]->fecha;
						$usuario=$seg1[0]->usuario;
					}else{
						if(count($v2)==2){
							$fecha=$seg2[0]->fecha;
							$usuario=$seg2[0]->usuario;
						}
					}
				}
			}else{
				if(!empty($seg1)){
					$v=explode(" ", $seg1[0]->fecha);
					if(count($v)==2){
						$fecha=$seg1[0]->fecha;
						$usuario=$seg1[0]->usuario;
					}
				}else{
					if(!empty($seg2)){
						$v=explode(" ", $seg2[0]->fecha);
						if(count($v)==2){
							$fecha=$seg2[0]->fecha;
							$usuario=$seg2[0]->usuario;
						}
					}
				}
			}
		}
		return $this->mensaje_tiempo_transcurrido($fecha,$usuario,$formato);
	}
	function diferencia_fecha($fecha1,$fecha2){
		$segundos=NULL;
		if($fecha1>=$fecha2){
			$f1=new DateTime($fecha1);
			$f2=new DateTime($fecha2);
			$diff=date_diff($f1, $f2);
			$segundos=((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
		}
		return $segundos;
	}
	function mensaje_tiempo_transcurrido($fecha,$usuario,$formato){
		$segundos=NULL;$msj="";$msj2="";
		if($fecha!="" && $fecha!=NULL && $fecha!="0000-00-00" && $fecha!="0000-00-00 00:00:00"){
			date_default_timezone_set("America/La_Paz");
			$segundos=$this->diferencia_fecha(date('Y-m-d H:i:s'),$fecha);
			$resp=array('tipo'=>'');
			if($segundos!=NULL){
				if($segundos>=60){
					$minutos=$segundos/60;
					if($minutos>=60){
						$horas=$minutos/60;
						if($horas>=24){
							$dias=$horas/24;
							if($dias>=21){
								$v=explode(" ", $fecha);
								$fecha=NULL;
								if(count($v)>=1){
									$fecha=$this->format_date($v[0], $formato);
									if(count($v)==2){
										$fecha.=" ".$v[1];
									}
								}
								$resp=array('tipo'=>'f', 'valor'=>$fecha);
							}else{
								$resp=array('tipo'=>'dia', 'valor'=>$dias);
							}
						}else{
							$resp=array('tipo'=>'hora', 'valor'=>$horas);
						}
					}else{
						$resp=array('tipo'=>'minuto', 'valor'=>$minutos);	
					}
				}else{
					$resp=array('tipo'=>'segundo', 'valor'=>$segundos);
				}
			}
			$tiempo=json_decode(json_encode($resp));
			if($tiempo->tipo!=""){
				$msj="Modificado por ".$usuario;
				if($tiempo->tipo=="f"){
					$msj.=" el ".$tiempo->valor."";
					$msj2=" el ".$tiempo->valor."";
				}else{
					$valor=intval($tiempo->valor);
					if($valor>1){
						$msj.=" hace ".$valor." ".$tiempo->tipo."s";
						$msj2=" hace ".$valor." ".$tiempo->tipo."s";
					}else{
						$msj.=" hace ".$valor." ".$tiempo->tipo."";
						$msj2=" hace ".$valor." ".$tiempo->tipo."";
					}
				}
			}
		}
		return json_decode(json_encode(array("msj" => $msj, "segundos" => $segundos,"tiempo" => $msj2)));
	}
	function nombre_de_dia($date){
		$dia="";
		switch($date){
			case 0: $dia="Domingo"; break;
			case 1: $dia="Lunes"; break;
			case 2: $dia="Martes"; break;
			case 3: $dia="Miercoles"; break;
			case 4: $dia="Jueves"; break;
			case 5: $dia="Viernes"; break;
			case 6: $dia="Sábado"; break;
		}
		return $dia;
	}
	function calcula_edad($fecha){
		$edad="";
		if($fecha!="" && $fecha!=NULL && $fecha!="0000-00-00" && $fecha!="0000-00-00 00:00:00"){
			$cumpleanos = new DateTime($fecha);
			$hoy = new DateTime();
			$anios = $hoy->diff($cumpleanos);
			$edad=$anios->y;
			if($edad>1){
				$edad.=" años";
			}else{
				$edad.=" año";
			}
		}
		return $edad;
	}
	function antiguedad($fecha){
		$sol="";
		if($fecha!="" && $fecha!=NULL && $fecha!="0000-00-00" && $fecha!="0000-00-00 00:00:00"){
			$sol=$this->formato_tiempo($this->diferencia_fecha(date('Y-m-d'),$fecha));
		}
		return $sol;
	}
	function formato_tiempo($segundos){
		$resultado=0;
		$dia=($segundos/(60*60*24));
		if($dia>30){
			$mes=$dia/30;
			if($mes>12){
				$anio=floor($mes/12);
				$resultado=$anio." año";
				if($anio>1){ $resultado.="s";}
				$mes_anio=$mes%12;
				$res_mes_anio="";
				if($mes_anio>0){
					$res_mes_anio=$mes_anio." mes";
					if($mes_anio>1){ $res_mes_anio.="es";}
					$resultado.=" ".$res_mes_anio;
				}
			}else{
				$resultado=floor($mes)." mes";
				if($resultado>1){ $resultado.="es"; }
				$dia_mes=$mes%30;
				$res_dia_mes="";
				if($dia_mes>0){
					$res_dia_mes=$dia_mes." dia";
					if($dia_mes>1){ $res_dia_mes.="s"; }
					$resultado.=" ".$res_dia_mes;
				}
			}
		}else{
			$resultado=round($dia)." dia";
			if(round($dia)>1){ $resultado.="s";}
		}
		return $resultado;
	}
	/*--- end manejo de fechas ---*/
	/*--- Manejo de cadenas ---*/
	function ini_mayuscula($cad){//primera letra en mayuscula
		return ucfirst($this->all_minuscula($cad));
	}
	function all_mayuscula($cad){
		setlocale(LC_CTYPE, 'es');
		return utf8_encode(utf8_decode(mb_convert_case($cad,MB_CASE_UPPER,"UTF-8")));
	}
	function all_minuscula($cad){
		return utf8_encode(utf8_decode(mb_convert_case($cad,MB_CASE_LOWER,"UTF-8")));
	}
	function may_palabra($cad){//convierte en mayuscula el primer caracter de cada palabra
		return ucwords($cad);
	}
	function may_cadena($cad){//primera letra en mayuscula
		return ucfirst($this->all_minuscula($cad));
	}
	/*--- Manejo de cadenas ---*/
	/*--- Manejo de numeros ---*/
	function dosDigitos($num){
		$num=$num*1;
		if($num>=0 & $num<10){
			$num="0".$num;
		}else{
			$num="".$num;
		}
		return $num;
	}
	/*--- End Manejo de numeros ---*/	
	/*--- Manejo de imagenes ---*/
	function subir_imagen_miniatura($FILES,$ruta,$pos,$resize,$cod){
		$img=NULL;
		if(count($FILES)>0){
			$rutaMiniatura=$ruta.'miniatura/';
			$key=$FILES['archivo'.$pos];
			$nameImg=$this->subir_imagen($key,$cod,$ruta);
			if($nameImg!="error_type" && $nameImg!="error" && $nameImg!="error_size_img"){
				if($this->crear_miniatura($resize,$ruta.$nameImg,$rutaMiniatura.$nameImg)){
					//subio y creo miniatura
					$img=$nameImg;
				}else{//no subio 
					if($this->eliminar_archivo($nameImg,$ruta)){}
						$img="error";
				}
			}else{
				$img=$nameImg;
			}
		}
		return $img;
	}
	function subir_imagen($file,$nombre,$ubicacion){
		$key=$file;
		$name="";
		if($this->size_valido($key,1.5)){//verificando que el tamaño de la imagen sea menor o igual a 1.5 MB
			if($this->es_imagen_valida($key)){
				if(!file_exists($ubicacion)){ mkdir($ubicacion);}
				if($key['error'] == UPLOAD_ERR_OK ){
					//Si el archivo se paso correctamente Continuamos
					$atrib=explode(".", $key['name']);
					if($nombre==NULL || $nombre==""){$nombre=rand(0,9999999);}
					$nombre=$nombre."-".rand(0,9999999).".".$atrib[count($atrib)-1];
					$temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
					$Destino = $ubicacion.$nombre;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					if(file_exists($Destino)){ unlink($Destino);}
					move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada
					$name=$nombre;
				}
				if($key['error']!=''){
					$name="error";
				}
			}else{
				$name="error_type";
			}
		}else{
			$name="error_size_img";
		}
		return $name;
	}
	function copiar_imagen($origen,$destino,$imagen,$nombre){
		$img=NULL;
		if(file_exists($origen)){
			$rutaMiniatura=$origen.'miniatura/';
			$atrib=explode(".", $imagen);
			if($nombre==NULL || $nombre==""){$nombre=rand(0,9999);}
			$nombre.="-".rand(0,9999999)."-".rand(0,9999999).".".$atrib[count($atrib)-1];
			if(copy($origen.$imagen, $destino.$nombre)){
				if(copy($origen."miniatura/".$imagen, $destino."miniatura/".$nombre)){
					$img=$nombre;
				}else{
					if($this->eliminar_imagen($nombre,$destino)){}
				}
		}
	}
	return $img;
}
function size_valido($file,$max_size){
	if(($file['size']*1)>0 && ($file['size']*1) <= ($max_size*1024*1024)){
		return true;
	}else{
		return false;
	}
}
function es_imagen_valida($file){
	if($file['type'] =='image/jpeg' || $file['type'] =='image/jpg' || $file['type'] =='image/gif' || $file['type'] =='image/png'){
		return true;
	}else{
		return false;
	}
}
function valida_imagenes($FILES){
	$size="";$atrib="";
	for($i=0; $i < count($FILES) ; $i++) { 
		if(!$this->size_valido($FILES['archivo'.$i],1.5)){ $size="error_size_imgs"; }
		if(!$this->es_imagen_valida($FILES['archivo'.$i])){ $atrib="error_type_imgs"; }
	}

	if($size!=""){
		return $size;
	}else{
		if($atrib!=""){
			return $atrib;
		}else{
			return "ok";
		}
	}
}
function rotar_imagen($url,$file,$grado){
	$resultado=NULL;
	$img=$url.$file;
	if(file_exists($img) && ($grado==90 || $grado==-90)){
		$v=explode(".", $img);
		$atrib=$v[count($v)-1];
		$new_nombre="r-".rand(10,9999)."-".rand(10,99999)."-".rand(10,999999999).".".$atrib;
		$destino=$url.$new_nombre;
		if(strtolower($atrib)=="jpeg" || strtolower($atrib)=="jpg"){$origen=imagecreatefromjpeg($img);}
		if(strtolower($atrib)=="png"){$origen=imagecreatefrompng($img);}
		if(strtolower($atrib)=="gif"){$origen=imagecreatefromgif($img);}
		$rotado=imagerotate($origen,$grado,0);
		if(strtolower($atrib)=="jpeg" || strtolower($atrib)=="jpg"){imagejpeg($rotado, $destino);}
		if(strtolower($atrib)=="png"){imagepng($rotado, $destino);}
		if(strtolower($atrib)=="gif"){imagegif($rotado, $destino);}
		$resultado=$new_nombre;
		//miniatura
		$img=$url."miniatura/".$file;
		if(file_exists($img)){
			$destino=$url."miniatura/".$new_nombre;
			if(strtolower($atrib)=="jpeg" || strtolower($atrib)=="jpg"){$origen=imagecreatefromjpeg($img);}
			if(strtolower($atrib)=="png"){$origen=imagecreatefrompng($img);}
			if(strtolower($atrib)=="gif"){$origen=imagecreatefromgif($img);}
			$rotado=imagerotate($origen,$grado,0);
			if(strtolower($atrib)=="jpeg" || strtolower($atrib)=="jpg"){imagejpeg($rotado, $destino);}
			if(strtolower($atrib)=="png"){imagepng($rotado, $destino);}
			if(strtolower($atrib)=="gif"){imagegif($rotado, $destino);}
		}
		$this->eliminar_imagen($file,$url);
	}
	return $resultado;
}
function subir_excel($file,$ubicacion,$nombre,$atributo){
	if($this->excel_valido($file,$atributo)){
		if(!file_exists($ubicacion)){ mkdir($ubicacion);}
			if($file['error'] == UPLOAD_ERR_OK ){
				//Si el archivo se paso correctamente Continuamos
				$atrib=explode(".", $file['name']); 
				$nombre=str_replace(".xls", ' '.$nombre,$file['name']);
				$nombre=$nombre.".".$atrib[count($atrib)-1];
				$temporal = $file['tmp_name']; //Obtenemos la ruta Original del archivo
				$destino = $ubicacion.$nombre;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
				if(file_exists($destino)){ unlink($destino);}
				move_uploaded_file($temporal, $destino); //Movemos el archivo temporal a la ruta especificada

				$name=$nombre;
			}
			if($file['error']!=''){
				$name="error";
			}
		}else{
			$name="error_type";
		}
		return $name;
	}
	function excel_valido($file,$extencion){
		$v_nom=explode(".", $file['name']);
		if($file['type']=='application/vnd.ms-excel' && $extencion==$v_nom[count($v_nom)-1]){
			return true;
		}else{
			return false;
		}
	}
	function crear_miniatura($funcion,$origen,$destino){
		if(file_exists($origen)){
			$min=$funcion;
			$min->inicio($origen);
			$min->resizeImage(130, 130, 'crop');
			if(!$min->saveImage($destino, 100)){
				return true;
			}else{//error en la creacion, eliminar archivo
				return false;
			}
		}else{
			return false;
		}
	}
	function eliminar_imagen($file,$ruta){
		if($file!="" && $file!=NULL){
			$rutaMiniatura=$ruta.'miniatura/';
			if($this->eliminar_archivo($file,$ruta) && $this->eliminar_archivo($file,$rutaMiniatura)){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	function eliminar_archivo($namefile,$dir){
		$namef=str_replace(' ', '', $namefile);
		if($namef!="" & $namef!=NULL){
			$dir_file=$dir.$namefile;
			if(file_exists($dir_file)){
				if(unlink($dir_file)){
					return true;
				}else{
					return false;
				}
			}else{
				return true;// no exitste archivo
			}
		}else{
			return true;// no exite imagen
		}
		
	}
	function cambiar_imagen($FILES,$ruta,$pos,$resize,$origen,$id){
		$img=$origen;
		if(count($FILES)>0){
			$key=$FILES['archivo'.$pos];
			if($this->size_valido($key,1.5)){//verificando que el tamaño de la imagen sea menor o igual a 1.5 MB
				$rutaMiniatura=$ruta.'miniatura/';
				$nameImg=$this->subir_imagen($key,$id,$ruta);
				if($nameImg!="error_type" && $nameImg!="error" && $nameImg!="error_size_img"){
					if($this->crear_miniatura($resize,$ruta.$nameImg,$rutaMiniatura.$nameImg)){
						//subio y creo miniatura
						//eliminado archivo
						if($this->eliminar_archivo($img,$ruta) && $this->eliminar_archivo($img,$rutaMiniatura)){}
							$img=$nameImg;
					}else{//no subio 
						if($this->eliminar_archivo($nameImg,$ruta)){}
					}
			}else{
				$img=$nameImg;
			}
		}else{
			$img="error_size_img";
		}
	}
	return $img;
}
/*--- End manejo de imagenes ---*/
/*--- CALCULO DE COSTO DE DEPRECIACION ---*/
function costo_depreciacion($monto,$anioDeVida,$fechaInicial){
	$costo_actual=0;
		//calculando depreciacion
		$dep_anual=$monto/$anioDeVida;//depreciacion anual
		//calculando dias existente en un año
		$fecha_final=strtotime('+1 year',strtotime($fechaInicial));
		$fecha_final=date('Y-m-j',$fecha_final);
		$segundos=strtotime($fecha_final)-strtotime($fechaInicial);
		$dias=intval($segundos/60/60/24);
		$depreciacion_por_dia=$dep_anual/$dias;
		//dias transcurridos
		$segundos=strtotime('now')-strtotime($fechaInicial);
		$dias=intval($segundos/60/60/24);
		$costo_actual=$monto-($depreciacion_por_dia*$dias);		
		return $costo_actual;
	}
	/*--- END CALCULO DE COSTO DE DEPRECIACION ---*/
	/*--- MANEJO DE HORAS ---*/
	function hms($segundos){
		$s=0;
		$h=0;
		$m=0;
		if($segundos!=0 && $segundos!=NULL && $segundos!=""){
			$s+=$segundos%60;
			$aux=floor($segundos/60);//minutos
			$m+=$aux%60;
			$h+=floor($aux/60);
		}
		return $this->dosDigitos($h).':'.$this->dosDigitos($m).':'.$this->dosDigitos($s);
	}

	/*--- MANEJO DE CAPITAL HUMANO ---*/
	function biometrico_valido($file){
		$cont=0;
		$sw=true;
		foreach($file as $row) : 
			if($cont==2){
				if(isset($row[1]) && isset($row[2]) && isset($row[3]) && isset($row[4])){
					if(($row[1]!="" || $row[2]!="") && $row[3]!="" && $row[4]!=""){
					}else{
						$sw=false;
					}
				}else{
					$sw=false;
				}
				break;
			}
			$cont++;			
			endforeach;
			if($cont>0 && $sw){
				return true;
			}else{
				return false;
			}
		}
	function min_max_fecha_biometrico($file){//en uso CAPITAL HUMANO,
		//usamos para hallar la minima o maxima fecha del archivo biometrico
		$merge=array();
		$max=strtotime('1970-01-01');
		$min=strtotime('2035-12-31');
		foreach($file as $row) : 
			$fecha_hora=explode(" ", $row[4]);//dividimos en fecha y hora, 01/12/2000 08:00
		$vf=explode("/", $fecha_hora[0]);
		if(count($vf)==3){
			$fecha=strtotime($vf[2].'-'.$vf[1].'-'.$vf[0]);
			if($fecha<=$min){ $min=$fecha; }
			if($fecha>=$max){ $max=$fecha; }
		}
		endforeach;
		$res = array('fecha_min' => date('Y-m-d',$min), 'fecha_max' => date('Y-m-d',$max));
		return json_encode($res);
	}
	/*--- para pedidos ---*/
	function es_feriado($fecha,$feriados){
		$sw=false;
		for ($i=0; $i < count($feriados) ; $i++) { 
			if($feriados[$i]->fecha==$fecha){
				$sw=true;
				break;
			}
		}
		return $sw;
	}

	function siguiente_dia_habil($fecha,$feriados){
		$fecha_habil="0000-00-00";
		$fin = new DateTime($fecha);
		while (true) {
			$fin->modify('+1 days');
			if(!$this->es_feriado($fin->format('Y-m-d'),$feriados) && $fin->format('l')!= 'Sunday'){//no es feriado y domingo
				$fecha_habil=$fin->format('Y-m-d');
				return $fecha_habil;
			}
		}
		return $fecha_habil;
	}
	function fecha_de_entrega_pedido($fini,$segundos,$feriados,$carga_horaria){
		$fecha_entrega=$fini;
		if(!empty($carga_horaria)){
			$segundos_por_dia=$carga_horaria[0]->horas*60*60;
			$seg=$segundos;
			$fin = new DateTime($fini);
			while ($seg>($segundos_por_dia/2)){
				if($fin->format('l')== 'Saturday'){
					$seg-=($segundos_por_dia/2);//tomamos los segundos trabajados en un sabado(medio )
				}else{
					$seg-=$segundos_por_dia;//tomamos los segundos trabajados en un jornal
				}
				if($seg>0){
					$fecha_entrega=$this->siguiente_dia_habil($fin->format('Y-m-d'),$feriados);
					$v=explode("-", $fecha_entrega);
					$fin->setDate($v[0],$v[1],$v[2]);
				}
			}
		}
		return $fecha_entrega;
	}
	/*--- end para pedidos ---*/
	/*--- save notifications ---*/
	function notifications($emisor,$usuarios,$notificaciones,$tipo){
		$resp=array();
		if(count($notificaciones)>0){
			for($i=0;$i<count($usuarios);$i++){ $usuario=$usuarios[$i];
				if($emisor!=$usuario->idus && ($usuario->tipo=="1" || $usuario->tipo=="2")){
					//buscando si existe notificaciones pendientes para el usuario
					$no_esta=true;//no se encuentra registrado en notificaciones
					for($j=0; $j < count($notificaciones) ; $j++) { $n=$notificaciones[$j];
						if($n->emisor==$emisor && $n->reseptor==$usuario->idus){//existe usuario
							if($tipo=="almacen"){
								$cantidad=($n->almacen*1)+1;
								$resp[] = array('idno' => $n->idno,'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => $cantidad);
							}
							if($tipo=="material"){
								$cantidad=($n->material*1)+1;
								$resp[] = array('idno' => $n->idno,'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => $cantidad);
							}
							if($tipo=="producto"){
								$cantidad=($n->producto*1)+1;
								$resp[] = array('idno' => $n->idno,'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => $cantidad);
							}
							if($tipo=="producto_categoria"){
								$cantidad=($n->producto_categoria*1)+1;
								$resp[] = array('idno' => $n->idno,'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => $cantidad);
							}
							if($tipo=="producto_categoria_costo"){
								if($usuario->pr==1 && $usuario->pr5r==1 && $usuario->pr5hc==1){
									$cantidad=($n->producto_categoria*1)+1;
									$resp[] = array('idno' => $n->idno,'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => $cantidad);
								}
							}
							if($tipo=="pedido"){
								if($usuario->mo==1 && $usuario->mo1r==1 && $usuario->mo1u==1){
									$cantidad=($n->pedido*1)+1;
									$resp[] = array('idno' => $n->idno,'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => $cantidad);
								}
							}
							$no_esta=false;
							break;
						}
					}
					if($no_esta){
						$resp[] = array('idno' => "none",'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => "1");
					}
				}
			}
		}else{
			for($i=0;$i<count($usuarios);$i++){ $usuario=$usuarios[$i];
				if($emisor!=$usuario->idus && ($usuario->tipo=="1" || $usuario->tipo=="2")){
					$resp[] = array('idno' => "none",'emisor' => $emisor,'reseptor' => $usuario->idus, 'cantidad' => "1");
				}
			}
		}
		return json_decode(json_encode($resp));
	}
	/*--- save notifications ---*/
	/*--- HISTORIAL ---*/
	function alert_almacen($alerta){
		$result="";$msj="";
		switch ($alerta->accion) {
			case 'insert': $msj="Creo "; break;
			case 'delete': $msj="Elimino "; break;
			case 'update': $msj="Modifico "; break;
			default: $msj="NULL"; break;
		}
		if($msj!=""){$result=$msj."el almacen <strong>".$alerta->almacen."</strong>";}else{ $result="Error!";}
		return $result;
	}
	function alert_producto($alerta){
		$result="";$msj="";
		switch($alerta->accion){
			case 'c': $result="Creó el producto "; break;
			case 'u': $result=""; break;
			case 'd': $result="Eliminó el producto "; break;
			case 'cf': $msj=""; break;
			case 'df': $msj=""; break;
			case 'uf': $msj=""; break;
			case 'cm': $msj=""; break;
			case 'dm': $msj=""; break;
			case 'um': $msj=", Modifico un material en el producto"; break;
			case 'cp': $msj=""; break;
			case 'up': $msj=", Modifico una pieza del producto"; break;
			case 'dp': $msj=""; break;
			default: $result="NULL"; break;
		}
		if($result!="NULL"){
			$result.="<strong>".$alerta->producto."</strong> ".$msj; if($alerta->msj_adicional!=""){ $result.=" (".$alerta->msj_adicional.")";}
		}else{$result="";}
		return $result;
	}
	function alert_producto_grupo_color($alerta){
		$msj="";$result="";
		switch($alerta->accion){
			case 'cg': $msj=""; break;
			case 'dg': $result="Eliminó la categoría "; break;
			case 'ug': $msj=""; break;
			case 'cgm': $msj=""; break;
			case 'ugm': $msj="Modificó un material de la categoría"; break;
			case 'dgm': $msj=""; break;
			case 'uga': $msj="Modificó los atributos de la categoría"; break;
			case 'cc': $msj=""; break;
			case 'uc': $msj=""; break;
			case 'dc': $result="Eliminó el color "; break;
			case 'cgcm': $msj=""; break;
			case 'dgcm': $msj=""; break;
			case 'ugcm': $msj=""; break;
			case 'cgcf': $msj=""; break;
			case 'dgcf': $msj="(Eliminó una fotografía en el color del producto)."; break;
			case 'ugcf': $msj=""; break;
			case 'cgcp': $msj=""; break;
			case 'ugcp': $msj=""; break;
			case 'dgcp': $msj=""; break;
			case 'uca': $msj=""; break;
/*
			case 'ccf': $msj="Adicionó una fotografía en color <strong>".$alerta->color."</strong> de la categoría "; break;
			case 'dcf': $msj="Eliminó una fotografía en el color <strong>".$alerta->color."</strong> de la categoría "; break;
			
			case 'ugcm': $msj="Modificó el color <strong>".$alerta->color."</strong> de la categoría "; break;*/			
		}
		$result.="<strong>".$alerta->producto."</strong> ".$msj;if($alerta->msj_adicional!=""){ $result.=" (".$alerta->msj_adicional.")";}
		return $result;
	}
	function alert_pedido($alerta){
		$msj="";
		/*switch($alerta->accion){
			case 'c': $result="Creó el producto "; break;
			case 'u': $result=""; break;
			case 'd': $result="Eliminó el producto "; break;
			case 'cf': $msj=""; break;
			case 'df': $msj=""; break;
			case 'uf': $msj=""; break;
			case 'cm': $msj=""; break;
			case 'dm': $msj=""; break;
			case 'um': $msj=", Modifico un material en el producto"; break;
			case 'cp': $msj=""; break;
			case 'up': $msj=", Modifico una pieza del producto"; break;
			case 'dp': $msj=""; break;
			default: $result="NULL"; break;
		}*/
		$mensaje=str_replace("<ul>", "<ul class='basic-list'>",$alerta->msj);
		$mensaje=str_replace(", Modificó el producto", "<br> - Modificó el producto", $mensaje);
		$mensaje=str_replace("(Modificó el producto", "<br> - Modificó el producto", $mensaje);
		$msj=$mensaje;
		return $msj;
	}
	/*--- END HISTORIAL ---*/
	/*--- BUSQUEDAS ----*/
	function producto_grupo_material($idp,$producto_grupos,$producto_grupos_materiales,$grupos){
		$v=array();
		for ($i=0; $i < count($producto_grupos) ; $i++) { $e=$producto_grupos[$i];
			if($e->idp==$idp){
				$v[]=array('idpgr' => $e->idpgr,'idgr' => $e->idgr);
			}
		}
		$j_pg=json_decode(json_encode($v));
		$v = array();
		foreach ($j_pg as $key => $pg) {
			for ($i=0; $i < count($producto_grupos_materiales) ; $i++) { $e=$producto_grupos_materiales[$i];
				if($pg->idpgr==$e->idpgr){
					$v[] = array('idpgm' => $e->idpgm, 'idpgr'=> $e->idpgr, 'idm' => $e->idm, 'cantidad' =>$e->cantidad ,'observacion' => $e->observacion, 'idgr' => $pg->idgr);
				}
			}
		}
		$j_pgm=json_decode(json_encode($v));
		$v = array();
		foreach ($j_pgm as $key => $pgm){
			for ($i=0; $i < count($grupos) ; $i++) { $e=$grupos[$i];
				if($pgm->idgr==$e->idgr){
					$v[] = array('nombre_g'=>$e->nombre,'abr_g'=>$e->abr,'idpgm' => $pgm->idpgm, 'idpgr'=> $pgm->idpgr, 'idm' => $pgm->idm, 'cantidad' =>$pgm->cantidad ,'observacion' => $pgm->observacion, 'idgr' => $pgm->idgr,'idp' => $idp);
				}
			}
		}
		return json_decode(json_encode($v));
	}
	function producto_grupo_color_material($idp,$producto_grupos,$grupos,$productos_grupos_colores,$productos_grupos_colores_materiales,$colores){
		$v=array();
		for ($i=0; $i < count($producto_grupos) ; $i++) { $e=$producto_grupos[$i];
			if($e->idp==$idp){
				$v[]=array('idpgr' => $e->idpgr,'idgr' => $e->idgr);
			}
		}
		$j_pg=json_decode(json_encode($v));
		$v = array();
		foreach($j_pg as $key => $pg){
			for ($i=0; $i < count($grupos) ; $i++) { $e=$grupos[$i];
				if($pg->idgr==$e->idgr){
					$v[] = array('nombre_g'=>$e->nombre,'abr_g'=>$e->abr,'idpgr' => $pg->idpgr,'idgr' => $pg->idgr);
				}
			}
		}
		$j_pg=json_decode(json_encode($v));
		$v = array();
		foreach($j_pg as $key => $pg){
			for ($i=0; $i < count($productos_grupos_colores) ; $i++) { $e=$productos_grupos_colores[$i];
				if($pg->idpgr==$e->idpgr){
					$v[]=array('idpgrc'=>$e->idpgrc,'idco' => $e->idco,'costo'=>$e->costo,'nombre_g'=>$pg->nombre_g,'abr_g'=>$pg->abr_g,'idpgr' => $pg->idpgr,'idgr' => $pg->idgr);
				}
			}
		}
		$j_pgc=json_decode(json_encode($v));
		$v = array();
		foreach($j_pgc as $key => $pgc) {
			for ($i=0; $i < count($colores) ; $i++) { $e=$colores[$i];
				if($pgc->idco==$e->idco){
					$v[] = array('nombre_c'=>$e->nombre,'abr_c'=>$e->abr,'codigo_c'=>$e->codigo, 'idpgrc'=>$pgc->idpgrc,'idco'=>$pgc->idco,'costo'=>$pgc->costo,'nombre_g'=>$pgc->nombre_g,'abr_g'=>$pgc->abr_g,'idpgr' => $pgc->idpgr,'idgr' => $pgc->idgr);
				}
			}
		}
		$j_pgc=json_decode(json_encode($v));
		$v = array();
		foreach($j_pgc as $key => $pgc) {
			for ($i=0; $i < count($productos_grupos_colores_materiales) ; $i++) { $e=$productos_grupos_colores_materiales[$i];
				if($pgc->idpgrc==$e->idpgrc){
					$v[] = array('idpgcm' => $e->idpgcm,'idm'=> $e->idm,'cantidad' =>$e->cantidad ,'observacion' => $e->observacion, 'nombre_c'=>$pgc->nombre_c,'abr_c'=>$pgc->abr_c,'codigo_c'=>$pgc->codigo_c, 'idpgrc'=>$pgc->idpgrc,'idco'=>$pgc->idco,'costo'=>$pgc->costo,'nombre_g'=>$pgc->nombre_g,'abr_g'=>$pgc->abr_g,'idpgr' => $pgc->idpgr,'idgr' => $pgc->idgr);
				}
			}
		}
		return json_decode(json_encode($v));
	}
	function grupo_producto($grupos,$producto){
		$resp=-1;
		for($i=0; $i<count($grupos); $i++){
			if($grupos[$i]->idgr==$producto->idgr){
				$resp=$i;
				break;
			}
		}
		return $resp;
	}
	function color_producto($colores,$producto){
		$resp=-1;
		for($i=0; $i<count($colores); $i++){
			if($colores[$i]->idco==$producto->idco){
				$resp=$i;
				break;
			}
		}
		return $resp;
	}
	function producto_grupo($productos,$grupo){
		$resp=-1;
		for($i=0; $i<count($productos); $i++){
			if($productos[$i]->idgr==$grupo->idgr){
				$resp=$i;
				break;
			}
		}
		return $resp;
	}
	function producto_color($productos,$color){
		$resp=-1;
		for($i=0; $i<count($productos); $i++){
			if($productos[$i]->idco==$color->idco){
				$resp=$i;
				break;
			}
		}
		return $resp;
	}
	function producto_grupo_color($producto,$producto_grupos,$grupos,$producto_grupos_colores,$colores){
		$resp=-1;
		for($i=0; $i<count($productos); $i++){
			if($productos[$i]->idco==$color->idco){
				$resp=$i;
				break;
			}
		}
		return $resp;
	}
	function search_elemento($elementos,$col,$val){
		for ($i=0; $i < count($elementos); $i++) { $elemento=$elementos[$i];
			if($elemento->$col==$val){
				return $elemento;
			}
		}
		return null;
	}
	function search_elemento_2n($elementos,$col,$val,$col2,$val2){
		for($i=0; $i < count($elementos); $i++){ $elemento=$elementos[$i];
			if($elemento->$col==$val && $elemento->$col2==$val2){
				return $elemento;
			}
		}
		return null;
	}
	function search_json($elementos_json,$col,$val){
		foreach($elementos_json as $key => $elemento){
			if(is_string($elemento)){
				$elemento=json_decode($elemento);
			}
			if($elemento->$col==$val){
				return $elemento;
			}
		}
		return null;
	}
	function select_from($elementos,$col,$val){
		$tabla=[];
		for($i=0;$i<count($elementos);$i++){$elemento=$elementos[$i];
			if($elemento->$col==$val){
				$tabla[]=json_decode(json_encode($elemento));
			}
		}
		return $tabla;
	}
	function search_where($elemento,$col,$val,$type){
			if($type=="like"){
				if(strpos($this->all_minuscula($elemento->$col), $this->all_minuscula($val))!==false){
					return $elemento;
				}
			}
			if($type=="equals"){
				if($elemento->$col==$val.""){
					return $elemento;
				}
			}
		return null;
	}
	function str_replace_all($cad,$search,$tag){
		if(strlen($cad)<=0 && strlen($cad)<strlen($search)){
	        return "";
	    }else{
	        $cad_aux=$this->all_minuscula(trim($cad));
	        $pos=strpos($cad_aux,$search);
	        if($pos!==false){
	          $cad_new=substr($cad,$pos+strlen($search),strlen($cad));
	          if($pos==0){
	            $cad_search=substr($cad,0,strlen($search));
	          }else{
	            $cad_search=substr($cad,$pos,strlen($search));
	          }
	          switch($tag){
	            case 'mark': $cad=substr($cad,0,$pos)."<mark>".$cad_search."</mark>"; break;
	            default: $cad=substr($cad,0,$pos)."<mark>".$cad_search."</mark>"; break;
	          }
	        }else{
	          $cad_new="";
	        }
	        return($cad.$this->str_replace_all($cad_new,$search,$tag));
	        //return $cad;
	    }
	}
	/*--- BUSQUEDAS ----*/
	/*---- */
	function varios_grupo($grupos){
		$anterior=NULL;
		for($i=0; $i < count($grupos); $i++){$grupo=$grupos[$i];
			if($anterior!=NULL){
				if($grupo->idpgr!=$anterior){
					return true;
					break;
				}
			}else{
				$anterior=$grupo->idpgr;
			}
		}
		return false;
	}
	/*-----*/
	function movimientos_producto($producto_seg,$producto_grupo_color_seg,$tipo){
		$vector = array();
		for($i=0; $i < count($producto_seg) ; $i++){ $h=$producto_seg[$i];
			$vector[] = array('idp' => $h->idp,'producto' => $h->producto,'idpgr' => 'none','idgr' => 'none','grupo' => 'none','idpgrc' => 'none','idco' => 'none','color' => 'none','accion' => $h->accion,'msj_adicional' => $h->msj_adicional,'idu' => $h->idu,'usuario' => $h->usuario,'fecha' => $h->fecha);
		}
		for($i=0; $i < count($producto_grupo_color_seg) ; $i++){ $h=$producto_grupo_color_seg[$i];
			$vector[] = array('idp' => $h->idp,'producto' => $h->producto,'idpgr' => $h->idpgr,'idgr' => $h->idgr,'grupo' => $h->grupo,'idpgrc' => $h->idpgrc,'idco' => $h->idco,'color' => $h->color,'accion' => $h->accion,'msj_adicional' => $h->msj_adicional,'idu' => $h->idu,'usuario' => $h->usuario,'fecha' => $h->fecha);
		}
		foreach ($vector as $key => $row) {
			$aux[$key] = $row['fecha'];
		}
		if($tipo=="desc"){ $tipo=SORT_DESC;}else{ $tipo=SORT_ASC;}
		if(count($vector)>0){ array_multisort($aux, $tipo, $vector); }
		return json_decode(json_encode($vector));
	}
	/*---- MANEJO DE ARRAYS---- */

	function sort_2d($array,$column,$tipo){
		$aux=array();
		foreach($array as $key => $row){
			$aux[$key] = $row[$column];
		}
		switch(strtolower($tipo)){
			case 'asc': array_multisort($aux, SORT_ASC, $array); break;
			case 'desc': array_multisort($aux, SORT_DESC, $array); break;
		}
		return json_decode(json_encode($array));
	}
	function array_multi_unique($multi_array,$key){
		$tmp = array();
		$result = array();
		$array=$multi_array;
		foreach ($array as $value) {
			if (!in_array($value[$key], $tmp)) {
				array_push($tmp, $value[$key]);
				array_push($result, $value);
			}
		}
		return $array = $result;
	}
	/*---- END MANEJO DE ARRAYS---- */
	function es_portada($fotografia,$tipo,$portada){
		$result=false;
		if($portada!="" && $portada!=NULL){
			$v=explode("|", $portada);
			if(count($v)==2){
				if($tipo==$v[0]){
					if($v[0]==1){
						if($fotografia->idpi==$v[1]){
							$result=true;
						}
					}
					if($v[0]==3){
						if($fotografia->idpig==$v[1]){
							$result=true;
						}
					}
				}
			}
		}
		return $result;
	}
	/*--- encriptar*/
	function encriptar_str($cad){
		$res="";
		if($cad!="" && $cad!=null && strlen($cad)>1){
			$res.=$cad[0]."gmg";
			for ($i=1; $i < strlen($cad)-1 ; $i++) { 
				$res.=$cad[$i];
			}
			$res.="dev".$cad[strlen($cad)-1];
		}
		return base64_encode($res);
	}
	function desencriptar_str($cad){
		$cad=base64_decode($cad);
		$res="";
		if($cad!="" && $cad!=null && strlen($cad)>7){
			$res.=$cad[0];
			$res2=$cad[strlen($cad)-1];
			$res3=substr($cad, 1,strlen($cad)-2);
			if(strlen($res3)>6){
				$res.=substr($res3, 3,strlen($cad)-8).$res2;
			}else{
				$res.=$res2;
			}
		}
		return $res;
	}
	function numero_cadena($num){
		$v = array('9' => 'a','8' => 'x','7' => 'b','6' => 'y','5' => 'c','4' => 'z','3' => 'd','2' => 'w','1' => 'e','0' => 'i','-'=>'o','.'=>'u',','=>'n');
		$num.="";
		$res="";
		for ($i=0; $i < strlen($num) ; $i++) { 
			$res.=$v[$num[$i]];
		}
		return $res;
	}
	function cadena_numero($cad){
		$v = array('9' => 'a','8' => 'x','7' => 'b','6' => 'y','5' => 'c','4' => 'z','3' => 'd','2' => 'w','1' => 'e','0' => 'i','-'=>'o','.'=>'u',','=>'n');
		$res="";
		for($i=0; $i < strlen($cad) ; $i++){
			foreach ($v as $clave => $valor){
				if($valor==$cad[$i]){
					$res.=$clave;
					break;
				}
			}
		}
		return $res;
	}
	function encriptar_num($num){
		$new_num="";
		$num*=1;
		if($num>=0 && $num<=9999999999.9999){
			$new_num=$this->numero_cadena($num+11235.91);
			$new_num="gdev2|".$new_num;
		}
		return base64_encode($new_num);
	}
	function desencriptar_num($cad){
		$num="";
		if($cad!="" && $cad!=null){
			$cad=base64_decode($cad);
			$aux=explode("|", $cad);
			$salt=11235813.913;
			if(count($aux)=="2"){
				if($aux[0]=="gdev2"){
					$salt=11235.91;
					$cad=$aux[1];
				}
			}
			$num=($this->cadena_numero($cad)*1)-$salt;
		}
		return $num;
	}
	/*-- MANEJO DE PEDIDOS --*/
	function productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by){
		$v_detalles_parte_pedido=[];
		$posiciones=array();
		$v_productos=array();
		for($i=0; $i<count($productos_grupos_colores);$i++){ $producto_grupo_color=$productos_grupos_colores[$i];
			for($j=0; $j<count($detalles_pedidos); $j++){$detalle_pedido=$detalles_pedidos[$j];
				if($producto_grupo_color->idpgrc==$detalle_pedido->idpgrc){
						$producto=$this->search_elemento($productos,"idp",$producto_grupo_color->idp);
						if($producto!=null){
							$codigo="";
							$nombre="";
							if($producto_grupo_color->abr_g!=NULL && $producto_grupo_color->abr_g!=""){
								$codigo=$producto_grupo_color->abr_g;$nombre=$producto_grupo_color->nombre_g;
							}
							if($codigo!=""){$codigo.="-";}
							$codigo.=$producto_grupo_color->abr_c;
							if($nombre!=""){$nombre.=" - ";}
							$nombre.=$producto_grupo_color->nombre_c;
							//fotografia de detalle de producto
							$url=base_url().'libraries/img/';
							$img="sistema/miniatura/default.jpg";
							if($producto_grupo_color->portada!="" && $producto_grupo_color->portada!=null){
								$v=explode("|", $producto_grupo_color->portada);
								if(count($v==2)){
									if($v[0]=="1"){$control=$this->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
									if($v[0]=="3"){$control=$this->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
								}
							}else{
								if($producto->portada!="" && $producto->portada!=null){
									$v=explode("|", $producto->portada);
									if(count($v==2)){
										if($v[0]=="1"){$control=$this->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
										if($v[0]=="3"){$control=$this->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
									}
								}
							}
							//sucursales de detalle de producto
							$sucursales=array();
							$v_sucursales=$this->select_from($sucursales_detalles_pedidos,"iddp",$detalle_pedido->iddp);
							for($k=0;$k<count($v_sucursales);$k++){$sucursal=$v_sucursales[$k];
								$cantidad=$this->desencriptar_num($sucursal->cantidad)."";
								$sucursales[]=array('idsdp'=>$sucursal->idsdp,'idsc'=>$sucursal->idsc,'cantidad'=>$cantidad);
							}
							$costo_unitario_venta=$this->desencriptar_num($detalle_pedido->cu)."";
							$costo_venta=$this->desencriptar_num($detalle_pedido->cv)."";
							//echo $detalle_pedido->tipo;
							$v_detalles_parte_pedido[]=array('iddp'=>$detalle_pedido->iddp,'idpp'=>$detalle_pedido->idpp,'idp'=>$producto_grupo_color->idp,'idpgr'=>$producto_grupo_color->idpgr,'idpgrc' =>$producto_grupo_color->idpgrc,'codigo' => $codigo,'nombre' => $nombre,'fotografia' => $url.$img,'costo_unitario' => $producto_grupo_color->costo,'costo_unitario_venta' => $costo_unitario_venta,'costo_venta' => $costo_venta,'posicion' => $detalle_pedido->posicion,'porcentaje' => $detalle_pedido->porcentaje,'observacion' => $detalle_pedido->observacion,'sucursales'=>$sucursales);
							$posiciones[]=$detalle_pedido->posicion*1;
							$v_productos[]=$producto_grupo_color->idp*1;
						}
					
				}
			}
		}
		$result_productos=array();
		if($order_by=="posicion"){
			//ordenando por posicion de entrega
			//echo "<br>-------------------------<br>";
			$posiciones=array_unique($posiciones);
			//var_export($posiciones);
			//echo "<br>-------------------------<br>";
			asort($posiciones);
			//var_export($posiciones);
			$j_detalles_parte_pedido=json_decode(json_encode($v_detalles_parte_pedido));
			//var_export($j_detalles_parte_pedido);
			$j_posiciones=json_decode(json_encode($posiciones));
			$v_detalles_parte_pedido=array();
			$v_productos=array();
			foreach ($j_posiciones as $key => $posicion) {
				//echo "<br>".$posicion."<br>";
				foreach($j_detalles_parte_pedido as $key => $detalle){
					if($detalle->posicion==$posicion){
						$sucursales=array();
						foreach ($detalle->sucursales as $key => $sucursal) {
							$sucursales[]=array('idsdp'=>$sucursal->idsdp,'idsc'=>$sucursal->idsc,'cantidad'=>$sucursal->cantidad);
						}
						$v_detalles_parte_pedido[]=array('iddp'=>$detalle->iddp,'idpp'=>$detalle->idpp,'idp'=>$detalle->idp,'idpgr'=>$detalle->idpgr,'idpgrc' =>$detalle->idpgrc,'codigo' => $detalle->codigo,'nombre' => $detalle->nombre,'fotografia' => $detalle->fotografia,'costo_unitario' => $detalle->costo_unitario,'costo_unitario_venta' => $detalle->costo_unitario_venta,'costo_venta' => $detalle->costo_venta,'posicion' => $detalle->posicion,'porcentaje' => $detalle->porcentaje,'observacion' => $detalle->observacion,'sucursales'=>$sucursales);
						$v_productos[]=$detalle->idp*1;
					}
				}
			}//buscando productos
			$v_productos=array_unique($v_productos);
			$j_productos=json_decode(json_encode($v_productos));
			$detalles=json_decode(json_encode($v_detalles_parte_pedido));
			foreach($j_productos as $key=>$idp){//agrupando categorias por producto
				$producto=$this->search_elemento($productos,'idp',$idp);
				if($producto!=null){
					$detalles_parte_pedido=array();
					foreach($detalles as $key => $detalle){
							if($producto->idp==$detalle->idp){
								$sucursales=array();
								foreach ($detalle->sucursales as $key => $sucursal) {
									$sucursales[]=array('idsdp'=>$sucursal->idsdp,'idsc'=>$sucursal->idsc,'cantidad'=>$sucursal->cantidad);
								}
								$detalles_parte_pedido[]=array('iddp'=>$detalle->iddp,'idpp'=>$detalle->idpp,'idp'=>$detalle->idp,'idpgr'=>$detalle->idpgr,'idpgrc'=>$detalle->idpgrc,'codigo'=>$detalle->codigo,'nombre'=>$detalle->nombre,'fotografia'=>$detalle->fotografia,'costo_unitario'=>$detalle->costo_unitario,'costo_unitario_venta'=>$detalle->costo_unitario_venta,'costo_venta'=>$detalle->costo_venta,'posicion'=>$detalle->posicion,'porcentaje'=>$detalle->porcentaje,'observacion'=>$detalle->observacion,'sucursales'=>$sucursales);;
							}
					}
					$url=base_url().'libraries/img/';
					$img="sistema/miniatura/default.jpg";
					if($producto->portada!="" && $producto->portada!=null){
						$v=explode("|", $producto->portada);
						if(count($v==2)){
							if($v[0]=="1"){$control=$this->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
							if($v[0]=="3"){$control=$this->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
						}
					}
					$result_productos[]=array('idp'=>$producto->idp,'codigo'=>$producto->codigo,'nombre'=>$producto->nombre,'fotografia'=>$url.$img,'estado'=>$producto->estado, 'estado_registro'=>$producto->estado_registro,'categorias'=>$detalles_parte_pedido);
				}
			}
		}
		if($order_by=="codigo"){
			$v_productos=array_unique($v_productos);
			$j_productos=json_decode(json_encode($v_productos));
			$detalles=json_decode(json_encode($v_detalles_parte_pedido));
			for($i=0;$i<count($productos);$i++){$producto=$productos[$i];
				foreach($j_productos as $key=>$idp){//agrupando categorias por producto
					if($idp==$producto->idp){
						$detalles_parte_pedido=array();
						foreach($detalles as $key => $detalle){
							if($producto->idp==$detalle->idp){
								$sucursales=array();
								foreach ($detalle->sucursales as $key => $sucursal) {
									$sucursales[]=array('idsdp'=>$sucursal->idsdp,'idsc'=>$sucursal->idsc,'cantidad'=>$sucursal->cantidad);
								}
								$detalles_parte_pedido[]=array('iddp'=>$detalle->iddp,'idpp'=>$detalle->idpp,'idp'=>$detalle->idp,'idpgr'=>$detalle->idpgr,'idpgrc'=>$detalle->idpgrc,'codigo'=>$detalle->codigo,'nombre'=>$detalle->nombre,'fotografia'=>$detalle->fotografia,'costo_unitario'=>$detalle->costo_unitario,'costo_unitario_venta'=>$detalle->costo_unitario_venta,'costo_venta'=>$detalle->costo_venta,'posicion'=>$detalle->posicion,'porcentaje'=>$detalle->porcentaje,'observacion'=>$detalle->observacion,'sucursales'=>$sucursales);
							}
						}
						$url=base_url().'libraries/img/';
						$img="sistema/miniatura/default.jpg";
						if($producto->portada!="" && $producto->portada!=null){
							$v=explode("|", $producto->portada);
							if(count($v==2)){
								if($v[0]=="1"){$control=$this->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
								if($v[0]=="3"){$control=$this->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
							}
						}
						$result_productos[]=array('idp'=>$producto->idp,'codigo'=>$producto->codigo,'nombre'=>$producto->nombre,'fotografia'=>$url.$img,'estado'=>$producto->estado, 'estado_registro'=>$producto->estado_registro, 'categorias'=>$detalles_parte_pedido);
					}
				}
			}
		}
		return json_decode(json_encode($result_productos));
	}
	function colores_producto($producto,$colores,$productos_imagenes,$productos_imagenes_colores){
		$url=base_url().'libraries/img/';
		$productos=array();
		for($i=0;$i<count($colores);$i++){$color=$colores[$i];
			$codigo=$producto->codigo;
			$nombre=$producto->nombre;
			if($color->abr_g!=NULL && $color->abr_g!=""){
				$codigo=$color->abr_g;$nombre=$color->nombre_g;
			}
			if($color->abr_c!=NULL && $color->abr_c!=""){
				if($codigo!=""){
					$codigo.="-".$color->abr_c;
				}else{
					$codigo.=$color->abr_c;	
				}
				if($nombre!=""){
					$nombre.=" - ".$color->nombre_c;
				}else{
					$nombre.=$color->nombre_c;	
				}
			}
			//fotografia de detalle de producto
			$img="sistema/miniatura/default.jpg";
			if($color->portada!="" && $color->portada!=null){
				$v=explode("|", $color->portada);
				if(count($v==2)){
					if($v[0]=="1"){$control=$this->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
					if($v[0]=="3"){$control=$this->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
				}
			}else{
				if($producto->portada!="" && $producto->portada!=null){
					$v=explode("|", $producto->portada);
					if(count($v==2)){
						if($v[0]=="1"){$control=$this->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
						if($v[0]=="3"){$control=$this->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
					}
				}
			}
			//sucursales de detalle de producto
			$productos[]=array('idp'=>$producto->idp,'idpgr'=>$color->idpgr,'idgr'=>$color->idgr,'idpgrc'=>$color->idpgrc,'idco'=>$color->idco,'codigo'=>$codigo,'nombre'=>$nombre,'fotografia'=>$url.$img,'idpgrc' =>$color->idpgrc,'codigo' => $codigo,'nombre' => $nombre,'fotografia' => $url.$img,'costo'=>$color->costo);
		}
		return json_decode(json_encode($productos));
	}
	function validate_colores_producto_pedido($j_detalles,$productos,$detalles_pedido,$sucursales,$sucursales_detalles_pedidos,$val){
		$control=true;
		$detalles=json_decode($j_detalles);
		$resultado=array();
		foreach($detalles as $key => $detalle){
			//verificando sucursales
			$cantidad=0;
			$v_sucursales=array();
			if(isset($detalle->sucursales)){
				$j_sucursales=json_decode(json_encode($detalle->sucursales));
				foreach($j_sucursales as $key => $sucursal){
					$this_sucursal=$this->search_elemento($sucursales,'idsc',$sucursal->sc);
					if($this_sucursal!=null && $val->entero($sucursal->cantidad,0,4) && $sucursal->cantidad<=9999){
						$cantidad+=($sucursal->cantidad);
						if(isset($sucursal->sdp)){
							$detalle_producto_sucursal=$this->search_elemento($sucursales_detalles_pedidos,"idsdp",$sucursal->sdp);
							if($detalle_producto_sucursal!=null){
								$v_sucursales[]=array("sc"=>$sucursal->sc,"sdp"=>$sucursal->sdp,"cantidad"=>$sucursal->cantidad);//modificar
							}else{
								$control=false;
								break;
							}
						}else{
							$v_sucursales[]=array("sc"=>$sucursal->sc,"cantidad"=>$sucursal->cantidad);//nuevo
						}
					}else{
						$control=false;
						break;
					}
				}//end for
			}else{
				$control=false;
				break;
			}
			if($cantidad>0){
				//verificado detalle de pedido
				$producto_grupo_color=$this->search_elemento($productos,"idpgrc",$detalle->pgc);
				if($producto_grupo_color!=null && $val->decimal($detalle->cu,6,1) && ($detalle->cu*1)>=0 && ($detalle->cu*1)<=999999.9 && $val->textarea($detalle->observacion,0,300)){
					if(isset($detalle->posicion)){
						$posicion=$detalle->posicion."";
					}else{
						$posicion="none";
					}
					if(isset($detalle->dp)){//modificar

						$detalle_anterior=$this->search_elemento($detalles_pedido,"iddp",$detalle->dp);
						if($detalle_anterior!=null){
							$resultado[]=array("pgc"=>$detalle->pgc,"dp"=>$detalle->dp,"cu"=>$detalle->cu,"cantidad"=>$cantidad,"observacion"=>$detalle->observacion,"posicion"=>$posicion,"sucursales"=>$v_sucursales);
						}else{
							$control=false;
							break;
						}
					}else{//nuevo
						$resultado[]=array("pgc"=>$detalle->pgc,"cu"=>$detalle->cu,"cantidad"=>$cantidad,"observacion"=>$detalle->observacion,"posicion"=>$posicion,"sucursales"=>$v_sucursales);
					}
				}else{
					$control=false;
					break;
				}
			}else{
				$control=false;
				break;
			}
		}
		return json_decode(json_encode(array("control"=>$control,"productos"=>$resultado)));
	}
	function productos_empleado($productos_empleado,$productos_grupos_colores){
		$v_pgc_proc=[];
		//obteniendo los productos
		for($i=0; $i<count($productos_grupos_colores);$i++){ $pgc=$productos_grupos_colores[$i];
			for($j=0; $j<count($productos_empleado); $j++){ $pe=$productos_empleado[$j];
				if($pgc->idpgrc==$pe->idpgrc){
					$v_pgc_proc[] = array('idp'=>$pgc->idp,'codigo_p'=>$pgc->codigo,'nombre_p'=>$pgc->nombre,'portada_producto' => $pgc->portada_producto,'idpgr'=>$pgc->idpgr,'idgr' =>$pgc->idgr,'nombre_g' => $pgc->nombre_g,'abr_g' => $pgc->abr_g,'idpgrc' => $pgc->idpgrc,'costo' => $pgc->costo,'portada_color' => $pgc->portada,'idco' => $pgc->idco,'nombre_c' => $pgc->nombre_c,'codigo_c' => $pgc->codigo_c,'abr_c' => $pgc->abr_c,'idproe' => $pe->idproe,'calidad' => $pe->calidad,'idpre' => $pe->idpre,'ide' => $pe->ide,'tipo'=>$pe->tipo,'idpr'=>$pe->idpr,'nombre_proceso'=>$pe->nombre);
				}
			}
		}
		$v_pgc=$this->array_multi_unique($v_pgc_proc,'idpgrc');
		$v_productos=$this->array_multi_unique($v_pgc,'idp');
		$productos=[];
		for($i=0; $i<count($v_productos); $i++){ $producto=$v_productos[$i];
			$grupos=[];
			for($j=0; $j < count($v_pgc); $j++){ $pgc=$v_pgc[$j];
				if($producto['idp']==$pgc['idp']){
					$procesos=[];
					for($k=0; $k < count($v_pgc_proc) ; $k++){ $proc=$v_pgc_proc[$k];
						if($proc['idpgrc']==$pgc['idpgrc']){
							$procesos[]=array('idproe' => $proc['idproe'],'calidad' => $proc['calidad'],'idpre' => $proc['idpre'],'ide' => $proc['ide'],'tipo' => $proc['tipo'],'idpr' => $proc['idpr'],'nombre_proceso' => $proc['nombre_proceso']);
						}
					}
					$grupos[] = array('idpgr'=>$pgc['idpgr'],'idgr'=>$pgc['idgr'],'nombre_g' => $pgc['nombre_g'],'abr_g' => $pgc['abr_g'],'idpgrc' => $pgc['idpgrc'],'costo' => $pgc['costo'],'portada' => $pgc['portada_color'],'idco' => $pgc['idco'],'nombre_c' => $pgc['nombre_c'],'codigo_c' => $pgc['codigo_c'],'abr_c' => $pgc['abr_c'],'idproe' => $pgc['idproe'], 'procesos' => $procesos);
				}
			}
			$productos[]=array('idp'=>$producto['idp'],'codigo'=>$producto['codigo_p'],'nombre'=>$producto['nombre_p'],'portada' => $producto['portada_producto'],'grupos'=>$grupos);
		}
		return json_decode(json_encode($productos));
	}
	function material_productos($productos,$material,$productos_material,$productos_grupos_material,$productos_colores_material){ //en uso en materiales
		$v1=[];
		$v2=[];
		$v3=[];
		if(!empty($productos_material)){
			for($j=0; $j < count($productos); $j++){ $p=$productos[$j];
				for($i=0; $i < count($productos_material) ; $i++) { $pm=$productos_material[$i];
					if($pm->idp==$p->idp){
						$v1[] = array('idpm'=>$pm->idpm, 'idp'=>$p->idp, 'codigo'=>$p->codigo, 'nombre_p'=>$p->nombre, 'portada_p'=>$p->portada, 'idgr'=>null, 'nombre_g'=>null, 'abr_g'=>null, 'idpgrc'=>null, 'idco'=>null, 'nombre_c'=>null, 'abr_c'=>null, 'codigo_c'=>null, 'portada_c'=>null, 'cantidad'=> $pm->cantidad, 'unidad'=> $material->medida, 'abr_u'=>$material->abr);
					}
				}
			}
		}
		if(!empty($productos_grupos_material)){
			for($j=0; $j < count($productos); $j++){ $p=$productos[$j];
				for ($i=0; $i < count($productos_grupos_material) ; $i++) { $pm=$productos_grupos_material[$i];
					if($pm->idp==$p->idp){
						$v2[] = array('idpgm'=>$pm->idpgm, 'idp'=>$p->idp, 'codigo'=>$p->codigo, 'nombre_p'=>$p->nombre, 'portada_p'=>$p->portada, 'idgr'=>$pm->idgr, 'nombre_g'=>$pm->nombre, 'abr_g'=>$pm->abr, 'idpgrc'=>null, 'idco'=>null, 'nombre_c'=>null, 'abr_c'=>null, 'codigo_c'=>null, 'portada_c'=>null, 'cantidad'=> $pm->cantidad, 'unidad'=> $material->medida, 'abr_u'=>$material->abr);
					}
				}
			}
		}
		if(!empty($productos_colores_material)){
			for($j=0; $j < count($productos); $j++){ $p=$productos[$j];
				for ($i=0; $i < count($productos_colores_material) ; $i++){ $pm=$productos_colores_material[$i];
					if($pm->idp==$p->idp){
						$v3[] = array('idpgcm'=>$pm->idpgcm, 'idp'=>$p->idp, 'codigo'=>$p->codigo, 'nombre_p'=>$p->nombre, 'portada_p'=>$p->portada, 'idgr'=>$pm->idgr, 'nombre_g'=>$pm->nombre_g, 'abr_g'=>$pm->abr_g, 'idpgrc'=>$pm->idpgrc, 'idco'=>$pm->idco, 'nombre_c'=>$pm->nombre_c, 'abr_c'=>$pm->abr_c, 'codigo_c'=>$pm->codigo_c, 'portada_c'=>$pm->portada, 'cantidad'=> $pm->cantidad, 'unidad'=> $material->medida, 'abr_u'=>$material->abr);
					}
				}
			}
		}
		$pr1=json_decode(json_encode($v1));
		$pr2=json_decode(json_encode($v2));
		$pr3=json_decode(json_encode($v3));
		$result=[];$result1=[];$result2=[];$result3=[];
		for($i=0; $i < count($productos); $i++){ $p=$productos[$i];
			foreach($pr1 as $key => $producto){
				if($producto->idp==$p->idp && $producto->idgr==null && $producto->idco==null){ 
					$result1[]=array('clave'=>$producto->idpm.'|'.$producto->cantidad.'|'.$producto->abr_u, 'idp'=>$producto->idp, 'codigo'=>$producto->codigo, 'nombre_p'=>$producto->nombre_p, 'portada_p'=>$producto->portada_p, 'idgr'=>$producto->idgr, 'nombre_g'=>$producto->nombre_g, 'abr_g'=>$producto->abr_g, 'idpgrc'=>$producto->idpgrc, 'idco'=>$producto->idco, 'nombre_c'=>$producto->nombre_c, 'abr_c'=>$producto->abr_c, 'codigo_c'=>$producto->codigo_c, 'portada_c'=>$producto->portada_c, 'cantidad'=> $producto->cantidad, 'unidad'=> $producto->unidad, 'abr_u'=>$producto->abr_u);
					
				}
			}
			foreach($pr2 as $key => $producto){
				if($producto->idp==$p->idp && $producto->idgr==$p->idgr && $producto->idco==null){ 
					$result2[]=array('clave'=>$producto->idpgm.'|'.$producto->cantidad.'|'.$producto->abr_u,'idp'=>$producto->idp, 'codigo'=>$producto->codigo, 'nombre_p'=>$producto->nombre_p, 'portada_p'=>$producto->portada_p, 'idgr'=>$producto->idgr, 'nombre_g'=>$producto->nombre_g, 'abr_g'=>$producto->abr_g, 'idpgrc'=>$producto->idpgrc, 'idco'=>$producto->idco, 'nombre_c'=>$producto->nombre_c, 'abr_c'=>$producto->abr_c, 'codigo_c'=>$producto->codigo_c, 'portada_c'=>$producto->portada_c, 'cantidad'=> $producto->cantidad, 'unidad'=> $producto->unidad, 'abr_u'=>$producto->abr_u);
				}
			}
			foreach($pr3 as $key => $producto){
				if($producto->idp==$p->idp){
					$result3[]=array('clave'=>$producto->idpgcm.'|'.$producto->cantidad.'|'.$producto->abr_u,'idp'=>$producto->idp, 'codigo'=>$producto->codigo, 'nombre_p'=>$producto->nombre_p, 'portada_p'=>$producto->portada_p, 'idgr'=>$producto->idgr, 'nombre_g'=>$producto->nombre_g, 'abr_g'=>$producto->abr_g, 'idpgrc'=>$producto->idpgrc, 'idco'=>$producto->idco, 'nombre_c'=>$producto->nombre_c, 'abr_c'=>$producto->abr_c, 'codigo_c'=>$producto->codigo_c, 'portada_c'=>$producto->portada_c, 'cantidad'=> $producto->cantidad, 'unidad'=> $producto->unidad, 'abr_u'=>$producto->abr_u);
				}
			}
		}
		$result[]=$this->array_multi_unique($result1,'clave');
		$result[]=$this->array_multi_unique($result2,'clave');
		$result[]=$this->array_multi_unique($result3,'clave');
		return json_decode(json_encode($result));
	}
	function descuentos_pedidos($partes,$detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
	$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$descuentos,$sucursales_cliente,$usuarios){
		$v=json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
		$v_productos=array();
		for($i=0;$i<count($partes);$i++){$parte=$partes[$i];
			$detalles_pedidos=$this->select_from($detalles_partes_pedido,'idpp',$parte->idpp);
			$j_productos=$this->productos_pedido($productos,$productos_grupos_colores,$productos_imagenes,
			$productos_imagenes_colores,$detalles_pedidos,$sucursales_detalles_pedidos,$order_by);
			foreach($j_productos as $key => $producto){
				$categorias=json_decode(json_encode($producto->categorias));
				foreach ($categorias as $key => $categoria) {
					$sucursales=json_decode(json_encode($categoria->sucursales));
					foreach($sucursales as $key => $sucursal){
						$v_descuentos=$this->select_from($descuentos,'idsdp',$sucursal->idsdp);
						//var_dump($v_descuentos);
						if(count($v_descuentos)>0){
							$sucursal_cliente=$this->search_elemento($sucursales_cliente,'idsc',$sucursal->idsc);
							if($sucursal_cliente!=null){
								for($j=0;$j<count($v_descuentos);$j++){ $v_descuento=$v_descuentos[$j];
									$usuario=$this->search_elemento($usuarios,'idus',$v_descuento->idus);
									if($usuario!=null){
										$codigo=$producto->codigo;
										if($categoria->codigo!=""){$codigo.="-".$categoria->codigo;}
										$nombre=$producto->nombre;
										if($categoria->nombre!=""){$nombre.="-".$categoria->nombre;}
										$cantidad=number_format(($this->desencriptar_num($v_descuento->cantidad)*1),0,'.','');
										$costo_unitario_venta=number_format(($categoria->costo_unitario_venta*1),1,'.','');
										$costo_total=number_format(($cantidad*$costo_unitario_venta),1,'.','');
										$descuento=number_format(($costo_total*$v_descuento->porcentaje),1,'.','');
										$v_productos[]=array('idp' => $producto->idp,'idpgrc'=>$categoria->idpgrc,'idpp'=>$categoria->idpp,
										'iddp'=>$categoria->iddp,'idsdp'=>$sucursal->idsdp,'iddes'=>$v_descuento->iddes,
										'fotografia'=>$categoria->fotografia,'codigo'=>$codigo,'nombre'=>$nombre,'tipo'=>$v[$parte->tipo]." ".$parte->numero,
										'cantidad'=>$cantidad,'porcentaje'=>$v_descuento->porcentaje,'observaciones'=>$v_descuento->observacion,
										'costo_unitario_venta'=>$categoria->costo_unitario_venta,'costo_total'=>$costo_total,'descuento'=>$descuento,'idsc'=>$sucursal_cliente->idsc,
										'nombre_sucursal'=>$sucursal_cliente->nombre_sucursal,'idus'=>$usuario->idus,'nombre_usuario'=>$usuario->nombre_completo);	
									}
								}
							}
						}
					}
				}
			}
		}
		return $v_productos;
	}
	/*-- END MANEJO DE PEDIDOS --*/
	/*---- MANEJO DE PRODUCCION -----*/
	function producto_categoria($productos_grupos_colores){
		$v=[];
		for ($i=0; $i < count($productos_grupos_colores) ; $i++){ $pgc=$productos_grupos_colores[$i];
			$v[]=array("idp"=>$pgc->idp,"codigo"=>$pgc->codigo,"nombre"=>$pgc->nombre,"fecha_creacion"=>$pgc->fecha_creacion,"observaciones"=>$pgc->observaciones,"portada"=>$pgc->portada_producto);
		}
		$productos=json_decode(json_encode($this->array_multi_unique($v,"idp")));
		$productos_categorias=[];
		foreach ($productos as $key => $producto){
			$categorias=[];
			for ($j=0; $j < count($productos_grupos_colores) ; $j++){ $pgc=$productos_grupos_colores[$j];
				if($producto->idp==$pgc->idp){
					$categorias[]=array("idpgr"=>$pgc->idpgr,"idgr"=>$pgc->idgr,"nombre_g"=>$pgc->nombre_g,"abr_g"=>$pgc->abr_g,"idpgrc"=>$pgc->idpgrc,"portada"=>$pgc->portada,"nombre_c"=>$pgc->nombre_c,"codigo_c"=>$pgc->codigo_c,"abr_c"=>$pgc->abr_c,"costo"=>$pgc->costo);
				}
			}
			$productos_categorias[]=array("idp"=>$producto->idp,"codigo"=>$producto->codigo,"nombre"=>$producto->nombre,"fecha_creacion"=>$producto->fecha_creacion,"observaciones"=>$producto->observaciones,"portada"=>$producto->portada,'categorias' => $categorias);
		}
		return $productos_categorias;
	}
	function producto_grupo_color_procesos($color,$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso){//optiene procesos 
		$procesos=[];
		for($j=0; $j < count($producto_proceso); $j++){ $proceso=$producto_proceso[$j];
			$control1=$this->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
			$control2=$this->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
			if($control1==null && $control2==null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
			}else{
				$control=$this->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$color->idpgr);
				if($control!=null){
					$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
				}
				$control=$this->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$color->idpgrc);
				if($control!=null){
					$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
				}
			}
		}// enf for
		return $procesos;
	}
	/*---- END MANEJO DE PRODUCCION -----*/
	function actualizar($actualizaciones){
		$result=false;
		if(!empty($actualizaciones)){
			for($i=0; $i < count($actualizaciones); $i++){ 
				
			}
		}
	}
	/*--- MANEJO DE JSON ----*/
	function json_empty($obj){
		if(is_null($obj)){ return true; }
		foreach($obj as $key => $val){ return false; }
		return true;
	}
	/*--- END MANEJO DE JSON ----*/
	function img($img,$url){
		if($img!="" && $img!=NULL){
			$url=$url."/miniatura/".$img;
		}else{
			$url="sistema/miniatura/default.jpg";
		}
		return $url;
	}
	/*ordenar objeto por fecha*/
    function ordenarPorFecha( $obj1, $obj2 ) {
        return strtotime($obj1->fecha) - strtotime($obj2->fecha);
    }
}
/* End of file lib.php */
/* Location: ./application/libraries/lib.php*/