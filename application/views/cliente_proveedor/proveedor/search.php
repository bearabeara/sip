<table cellspacing="0" cellpadding="0">
	<tr>
		<td class='img-thumbnail-60 hidden-sm'><div class="img-thumbnail-60"></div></td>
		<td class='celda-sm-15 hidden-sm'>
			<form class="view_proveedores">
				<input type="number" id="s_nit" placeholder='NIT/CI' class="form-control form-control-sm search_proveedor" onwheel="$(this).reset_input()" min='0'>
			</form>
		</td>
		<td class='celda-sm-30'>
			<form class="view_proveedores">
				<div class="input-group input-group-sm">
					<input type="search" id="s_razon" class="form-control form-control-sm search_proveedor" maxlength="100" placeholder='Buscar razon social...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_proveedores" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td style="width:20%" class=" hidden-sm">
			<form class="view_proveedores">
				<input type="search" id="s_encargado" placeholder='Nombre del responsable' class="form-control form-control-sm search_proveedor">
			</form>
		</td>
		<td style="width:35%" class=" hidden-sm"></td>
		<td class="hidden-sm">
			<?php 
				$search=json_encode(array('function'=>'view_proveedores','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_proveedores','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>Onfocus('s_nit');$(".view_proveedores").view_proveedores();$(".search_proveedor").search_proveedor();</script>