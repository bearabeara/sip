<?php 
$help1="Ingrese nombre de usuario, Tenga en cuenta que el sistema de validacion no distingue entre mayuscula y minuscula Ej. (Juan != juan)";
$popover1='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Nombre de Usuario</h4>" data-content="'.$help1.'"';
$help2="Ingrese su contraseña evitando usar letras mayusculas";
$popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="<h4>Ingresar Contraseña<h4>" data-content="'.$help2.'"';
$dir="/final";$min=".min";$version="15";
?>
<!DOCTYPE html>
  <html lang="es">
  <head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
   <meta name="description" content="Sistena de control de producción" />
   <meta name="keywords" content="marroquineria, cuero, maletines, billeteras" />
   <meta name="author" content="bearabeara.co.uk" />
   <title>Ingreso</title>
   <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/icomon<?php echo $min;?>.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/gdev.materialize<?php echo $min;?>.css?v=<?php echo $version;?>">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/gdev<?php echo $min;?>.css?v=<?php echo $version;?>">
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 </head>
 <body>
  <div class="container container-login">
    <div class="row">
      <div class="col s12 m8 offset-m2 l6 offset-l2 xl6 offset-xl3">
        <div class="card-panel">
          <div class="row">
            <div class="col s12 center">
              <img src="<?php echo base_url(); ?>libraries/img/sistema/logo.png"  alt="" class="responsive-img image-login">
            </div>
          </div>
          <div class="row"><hr style="border: 1px solid rgba(160,160,160,0.2);"></div>
          <div class="row row-login">
            <div class="form base" <?php if(!empty($exist)){?> data-alert='<?php echo $exist;?>'<?php }?> data-base="<?php echo base_url();?>"></div>
          </div>
          <div class="progress" style="display: none;"><div class="indeterminate"></div></div> 
        </div>
        <div class="author"><small><address title="bearabeara.co.uk">BY GDEV</address></small></div>
      </div>
    </div>
  </div>
  <footer class="page-footer grey lighten-5">
    <div class="row" style="margin-bottom: 0;">
      <div class="col s4 left-align">
        <img src="<?php echo base_url(); ?>libraries/img/sistema/killa-print-140.jpg"  alt="" class="responsive-img image-login" width="100">
      </div>
      <div class="col s4 center">
        <a href="https://bearabeara.co.uk/" target="__blank"> <img src="<?php echo base_url(); ?>libraries/img/sistema/bearabeara.png"  alt="" class="responsive-img image-login" width="120"></a>
      </div><div class="col s4 right-align"><img src="<?php echo base_url(); ?>libraries/img/sistema/logow.png"  alt="" class="responsive-img image-login" width="100"></div>
    </div>
  </footer>
</body>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous""></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/login<?php echo $min;?>.js?v=<?php echo $version;?>"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/materialize<?php echo $min;?>.js"></script>
</html>
