<?php
if(isset($fab_delete) || isset($fab_help) || isset($fab_new) || isset($fab_report)){
	if(isset($fab_delete)){if($fab_delete!=""){$j_fab_delete=json_decode($fab_delete);}}
	if(isset($fab_help)){if($fab_help!=""){$j_fab_help=json_decode($fab_help);}}
	if(isset($fab_cmat)){if($fab_cmat!=""){$j_fab_cmat=json_decode($fab_cmat);}}
	if(isset($fab_new)){if($fab_new!=""){$j_fab_new=json_decode($fab_new);}}
	if(isset($fab_report)){if($fab_report!=""){$j_fab_report=json_decode($fab_report);}}
?>
<div id="fab-button">
	<div id="btns-fab-wrapper" class="btns-fab-wrapper">
	<?php if(isset($j_fab_delete)){?>
		<div class="horizontal-fab">
		    <button class="sub_fab_btn bg-danger waves-effect waves-light <?php echo $j_fab_delete->function;?>" <?php if(isset($j_fab_delete->atribs)){foreach(json_decode($j_fab_delete->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?> data-toggle="tooltip" data-placement="left" title="<?php echo $j_fab_delete->title;?>" style="padding-top: 3.5px;">
		        <span><i class="icon-bin"></i></span>
		    </button>
		</div>
	<?php }?>
	<?php if(isset($j_fab_help)){?>
		<div class="horizontal-fab">
		    <button class="sub_fab_btn bg-primary waves-effect waves-light <?php echo $j_fab_help->function;?>" <?php if(isset($j_fab_help->atribs)){foreach(json_decode($j_fab_help->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?> data-toggle="tooltip" data-placement="left" title="<?php echo $j_fab_help->title;?>" style="padding-top: 3.5px;">
		        <span><i class="icon-question-circle"></i></span>
		    </button>
		</div>
	<?php }?>
	<?php if(isset($j_fab_report)){?>
		<div class="horizontal-fab">
		    <button class="sub_fab_btn bg-yellow-darken waves-effect waves-light <?php echo $j_fab_report->function;?>" <?php if(isset($j_fab_report->atribs)){foreach(json_decode($j_fab_report->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?> data-toggle="tooltip" data-placement="left" title="<?php echo $j_fab_report->title;?>">
		        <span><i class="icon-printer" style="padding-top: 1.5px;"></i></span>
		    </button>
		</div>
	<?php }?>
	<?php if(isset($j_fab_new)){?>
		<div class="horizontal-fab">
		    <button class="sub_fab_btn bg-success waves-effect waves-light <?php echo $j_fab_new->function;?>" <?php if(isset($j_fab_new->atribs)){foreach(json_decode($j_fab_new->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?> data-toggle="tooltip" data-placement="left" title="<?php echo $j_fab_new->title;?>">
		        <span>+</span>
		    </button>
		</div>
	<?php }?>
	<?php if(isset($j_fab_cmat)){?>
		<div class="horizontal-fab">
		    <button class="sub_fab_btn bg-info waves-effect waves-light <?php echo $j_fab_cmat->function;?>" <?php if(isset($j_fab_cmat->atribs)){foreach(json_decode($j_fab_cmat->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?> data-toggle="tooltip" data-placement="left" title="<?php echo $j_fab_cmat->title;?>" style="padding-top: 3.5px;">
		        <span><i class="fa fa-cube"></i></span>
		    </button>
		</div>
	<?php }?>
	</div>
	<button id="btn-fab-main" class="gfab-main-btn bg-danger waves-effect waves-light"><span><i class="icon-menu"></i></span></button>
</div>
<?php }?>
<?php if(isset($fab_delete) || isset($fab_help) || isset($fab_new) || isset($fab_report)){?>
<script>
$("div#fab-button").jqueryFab();
<?php if(isset($j_fab_delete)){?>$("button.<?php echo $j_fab_delete->function;?>").<?php echo $j_fab_delete->function;?>();<?php }?>
<?php if(isset($j_fab_new)){?>$("button.<?php echo $j_fab_new->function;?>").<?php echo $j_fab_new->function;?>();<?php }?>
<?php if(isset($j_fab_report)){?>$("button.<?php echo $j_fab_report->function;?>").<?php echo $j_fab_report->function;?>();<?php }?>
<?php if(isset($j_fab_help)){?>$("button.<?php echo $j_fab_help->function;?>").<?php echo $j_fab_help->function;?>();<?php }?>
</script>
<?php }?>