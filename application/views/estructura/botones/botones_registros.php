<?php 
	$width=0;
if(isset($detalle)){ if($detalle!=""){ $width+=33;}}
if(isset($reportes)){ if($reportes!=""){ $width+=33;}}
if(isset($reset)){ if($reset!=""){ $width+=33;}}
if(isset($guardar)){ if($guardar!=""){ $width+=33;}}
if(isset($configuracion)){ if($configuracion!=""){ $width+=33;}}
if(isset($eliminar)){ if($eliminar!=""){ $width+=33;}}
?>
<div style="width:<?php echo $width;?>px; float: right;">
	<div class="btn-group " role="group">
		<?php if(isset($detalle)){ if($detalle!=""){?><button type="button" class="btn btn-inverse-default btn-mini waves-effect" style="font-size:.8em;width:33px;" <?php echo $detalle;?> title="Detalles"><i class='fa fa-info-circle'></i></button><?php }}?>
		<?php if(isset($reportes)){ if($reportes!=""){?><button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Reportes" onclick="<?php echo $reportes;?>"><i class="fa fa-newspaper-o"></i></button><?php }} ?>
		<?php if(isset($reset)){ if($reset!=""){?><button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Reset" onclick="<?php echo $reset;?>"><i class="fa fa-search"></i></button><?php }} ?>
		<?php if(isset($guardar)){ if($guardar!=""){?><button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:1em;width:33px; padding-left:8px; padding-bottom:4.01px;" title="Guardar" onclick="<?php echo $guardar;?>"><i class="icon-file_download"></i></button><?php }}?>
		<?php if(isset($configuracion)){ if($configuracion!=""){?><button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Configurar" onclick="<?php echo $configuracion;?>"><i class="fa fa-cog"></i></button><?php }} ?>
		<?php if(isset($eliminar)){ if($eliminar!=""){?><button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Eliminar" <?php if($eliminar!='disabled'){?>onclick="<?php echo $eliminar;?>"<?php }?> <?php if($eliminar=='disabled'){ echo "disabled";}?>><span class="fa fa-trash"></span></button><?php }} ?>
	</div>
</div>