<ul class="nav nav-tabs  tabs" role="tablist">
    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#alter_almacen" role="tab">Almacen</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#alert_material" role="tab">Material</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#alert_producto" role="tab">Productos</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#alert_producto_categoria" role="tab">Productos categorias</a></li>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#alert_pedido" role="tab">Pedido</a></li>
<?php }?>
</ul>
<div class="tab-content tabs">
<div class="tab-pane active" id="alter_almacen" role="tabpanel" aria-expanded="true">
    <p><br>
		<table class="table table-bordered" style="font-size:.75em">
			<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php for ($i=0; $i<count($notificaciones_almacen); $i++){ $alerta=$notificaciones_almacen[$i]; ?>
		    	<tr <?php if(isset($alert)){if($i<$alert->almacen){?> class="table-info"<?php }} ?>>
		    		<td><?php echo $i+1;?></td>
		    		<td><?php echo $alerta->fecha;?></td>
		    		<td><?php echo $alerta->usuario;?></td>
		    		<td><?php echo $this->lib->alert_almacen($alerta);?></td>
		    	</tr>
		    <?php } ?>
		    </tbody>
		</table>
    </p>
</div>
<div class="tab-pane" id="alert_material" role="tabpanel" aria-expanded="false">
        <p><br>
		<table class="table table-bordered" style="font-size:.75em">
			<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php for ($i=0; $i < count($notificaciones_material) ; $i++) { $alerta=$notificaciones_material[$i]; ?>
		    	<tr <?php if(isset($alert)){ if($i<$alert->material){?> class="table-info" <?php }}?>>
		    		<td><?php echo $i+1;?></td>
		    		<td><?php echo $alerta->fecha;?></td>
		    		<td><?php echo $alerta->usuario;?></td>
		    <?php 
		    	switch ($alerta->accion) {
		    		case 'insert': $msj="Creo "; break;
		    		case 'delete': $msj="Elimino "; break;
		    		case 'update': $msj="Modifico "; break;
		    	}
		    ?>
		    		<td><?php if($msj!=""){ echo $msj."el material <strong>".$alerta->material."</strong> en el almacen <strong>".$alerta->almacen."</strong>";}else{ echo "Error!";}?></td>
		    	</tr>
		    <?php } ?>
		    </tbody>
		</table>
    </p>
</div>
<div class="tab-pane" id="alert_producto" role="tabpanel" aria-expanded="false">
        <p><br>
		<table class="table table-bordered" style="font-size:.75em">
			<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php for ($i=0; $i < count($notificaciones_producto) ; $i++) { $alerta=$notificaciones_producto[$i]; ?>
		    	<tr <?php if(isset($alert)){ if($i<$alert->producto){?> class="table-info" <?php }}?>>
		    		<td><?php echo $i+1;?></td>
		    		<td><?php echo $alerta->fecha;?></td>
		    		<td><?php echo $alerta->usuario;?></td>
		    		<td><?php echo $this->lib->alert_producto($alerta);?></td>
		    	</tr>
		    <?php } ?>
		    </tbody>
		</table>
    </p>
</div>
<div class="tab-pane" id="alert_producto_categoria" role="tabpanel" aria-expanded="false">
        <p><br>
		<table class="table table-bordered" style="font-size:.75em">
			<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php for ($i=0; $i < count($notificaciones_producto_categoria) ; $i++) { $alerta=$notificaciones_producto_categoria[$i]; ?>
			    <?php 
			    	$control=true;
			    	if($privilegio->pr!=1 || $privilegio->pr5r!=1 || $privilegio->pr5hc!=1){
			    		if(strpos($alerta->msj_adicional, "costo")!==false){
			    			$control=false;
			    		}
			    	}
			    	if($control){
			    ?>
			    	<tr <?php if(isset($alert)){ if($i<$alert->producto_categoria){?> class="table-info" <?php }}?>>
			    		<td><?php echo $i+1;?></td>
			    		<td><?php echo $alerta->fecha;?></td>
			    		<td><?php echo $alerta->usuario;?></td>
						<td><?php echo $this->lib->alert_producto_grupo_color($alerta);?></td>
			    	</tr>
			    <?php } ?>
		    <?php } ?>
		    </tbody>
		</table>
    </p>
</div>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
<div class="tab-pane" id="alert_pedido" role="tabpanel" aria-expanded="false">
        <p><br>
		<table class="table table-bordered" style="font-size:.75em">
			<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php for ($i=0; $i < count($notificaciones_pedido); $i++) { $alerta=$notificaciones_pedido[$i]; ?>
		    	<tr <?php if(isset($alert)){ if($i<$alert->pedido){?> class="table-info" <?php }}?>>
		    		<td><?php echo $i+1;?></td>
		    		<td><?php echo $alerta->fecha;?></td>
		    		<td><?php echo $alerta->usuario;?></td>
					<td><?php echo $this->lib->alert_pedido($alerta);?></td>
		    	</tr>
		    <?php } ?>
		    </tbody>
		</table>
    </p>
</div>
<?php } ?>
<?php 
	if(isset($alert)){
	if($this->M_notificacion->eliminar($alert->idno)){ } 
?>
<script type="text/javascript">
	var atrib=new FormData(); atrib.append('idno','<?php echo $alert->idno;?>');
	refresh_notifications("controls/refresh_alters",atrib,"badge_content");
</script>
<?php } ?>