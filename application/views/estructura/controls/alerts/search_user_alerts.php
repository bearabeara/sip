<?php
	$help1='title="Ususario" data-content="Seleccione el usuario de quien desea ver sus notificaciónes."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row">
	<div class="col-sm-5 offset-sm-7">
	<div class="input-group input-group-xs">
		<select class="form-control" onchange="search_user_alerts(this)" id="user-alert-all">
			<option value="">Seleccionar...</option>
		<?php for($i=0; $i < count($usuarios) ; $i++) { 
				if($usuarios[$i]->idus!=$this->session->userdata('id')){
		?>
			<option value="<?php echo $usuarios[$i]->idus;?>"><?php echo $usuarios[$i]->nombre." ".$usuarios[$i]->nombre2." ".$usuarios[$i]->paterno;?></option>
		<?php 	} 
			} 
		?>
		</select>
		<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
	</div>
</div>
</div>

<div id="content_11"></div>
<script>Onfocus("user-alert-all");$('[data-toggle="popover"]').popover({html:true});</script>