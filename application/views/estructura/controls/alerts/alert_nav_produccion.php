<li class="not-head"><b class="text-primary"><?php echo count($alerts)?></b> pedido<?php if(count($alerts)>1){echo 's';}?> en producción</li>
<?php for ($i=0; $i < count($alerts) ; $i++){ $pedido=$alerts[$i];
		$detalles=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido->idpe);
		$porcentaje=0;
		for($j=0; $j<count($detalles); $j++){$porcentaje+=($detalles[$j]->porcentaje);}
		if(count($detalles)>0){ $porcentaje/=count($detalles);$porcentaje=number_format($porcentaje,3,'.','');$porcentaje*=100;}
?>
	<li class="bell-notification">
	<div class="media">
		<div class="media-body"><span class="block"><?php echo "Pedido: ".$pedido->nombre;?></span>
			<div class="progress" style="height: 19px;">
    			<div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: <?php echo $porcentaje."%"; ?>">
    			<?php if($porcentaje>15){?>
					<strong><?php echo $porcentaje."%"; ?></strong>
    			<?php }?>
    			</div>
    			<?php if($porcentaje<=15){?>
   				<strong style="margin-left: 5px;"><?php echo $porcentaje."%"; ?></strong>
   				<?php }?>
   			</div>
		</div>
	</div>
	    

    </li>
<?php }?>
<li class="not-footer">
    <a href="<?php echo base_url().'produccion?p=2';?>" style="font-size: .8rem;">Ver toda la producción</a>
</li>