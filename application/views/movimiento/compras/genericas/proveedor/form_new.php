<?php
date_default_timezone_set("America/La_Paz");
$help1='title="NIT de proveedor" data-content="Ingrese un numero de NIT o CI con valores numericos <b>sin espacios</b>, de 1 a 25 digitos"';
$help2='title="Nombre o razón social" data-content="Ingrese un Nombre o Razon social alfanumerico de 2 a 100 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>	"';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; padding-top: 0px !important;">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-5 col-xs-12">
					<form class="save_proveedor" data-container="<?php echo $container;?>" data-type="<?php echo $type;?>">
						<label class="col-form-label">NIT de proveedor:</label>
						<div class="input-group">
							<input type="number" class='form-control form-control-xs' id="f2_nit" placeholder="0" min="0" max="9999999999999999999999999">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-7 col-xs-12">
					<form class="save_proveedor" data-container="<?php echo $container;?>" data-type="<?php echo $type;?>">
						<label class="col-form-label">Nombre o razón social:</label>
						<div class="input-group">
							<input type="text" class='form-control form-control-xs' id="f2_raz" placeholder="Prueba.SRL" minlength="2" maxlength="100">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$("form.save_proveedor").submit(function(e){$(this).save_proveedor();e.preventDefault();})</script>