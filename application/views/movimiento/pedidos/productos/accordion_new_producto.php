<?php 
  $help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
  $url=base_url().'libraries/img/';
  $img="sistema/miniatura/default.jpg";
  if($producto->portada!="" && $producto->portada!=null){
    $v=explode("|", $producto->portada);
    if(count($v==2)){
      if($v[0]=="1"){$control=$this->lib->search_elemento($productos_imagenes,'idpi',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
      if($v[0]=="3"){$control=$this->lib->search_elemento($productos_imagenes_colores,'idpig',$v[1]);if($control!=null){$img="productos/miniatura/".$control->archivo;}}
    }
  }
  $id=$producto->idp."-".rand(0,999999);
  $rand=rand(10,999999);
?>
<div class="accordion-panel" id="accordion-panel<?php echo $id;?>" data-cl="<?php echo $cliente->idcl;?>" data-tipo="<?php if(isset($tipo)){ echo $tipo;}else{ echo '0';}?>">
  <div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
    <span class="card-title accordion-title">
        <span class="g-control-accordion">
          <button class="drop_elemento<?php echo $rand;?>" data-padre="accordion-panel" data-type-padre="atr" data-module="producto_pedido"><i class="icon-bin"></i></button>
        </span>
      <a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
      <div class="item"></div>
      	<span class="img-thumbnail-50"><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-50"></span>
        <span><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
        <label class="badge badge-inverse-danger" id="badge<?php echo $id;?>"> Total: 0 unidades</label>
      </a>
    </span>
  </div>
  <div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
    <div class="accordion-content accordion-desc" data-pr="<?php echo $producto->idp;?>">
        <div class="table-responsive g-table-responsive">
        <?php $idtb=$producto->idp.'-'.rand(0,999999);?>
          <table class="table table-bordered table-hover" id="<?php echo $idtb;?>" data-accordion="div#accordion-panel<?php echo $id;?>" data-badge="label#badge<?php echo $id;?>">
            <thead>
              <tr>
                <th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>#Item</th>
                <th width="32%">Producto</th>
                <th width="14%">C/U (Bs.)</th>
            <?php for($i=0;$i < count($sucursales); $i++){ $sucursal=$sucursales[$i]; ?>
                <th><?php echo $sucursal->nombre; ?></th>
            <?php } ?>
                <th>Total</th>
                <th width="17%">Costo calculado(Bs.)</th>
                <th width="17%">Costo de venta(Bs.)</th>
                <th width="18%">Observaciónes</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php $c=1;
              foreach($grupos as $key=>$grupo){ ?>
              <tr class="row-producto" id="<?php echo $grupo->idpgrc."-".rand(1,9999999);?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-p="<?php echo $producto->idp;?>" <?php if(isset($iddp)){?> data-dp="<?php echo $iddp;?>"<?php }?>>
                <?php $pgc=$grupo->idpgrc."-".rand(1,999999);?>
                    <td><div class="item"><?php echo $c++;?></div><img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $producto->nombre.' - '.$grupo->nombre;?>" data-desc="<br>"></td>
                    <td><?php echo $grupo->nombre;?></td>
                    <td>
                    <?php $costo_unitario=$grupo->costo; if(isset($tipo)){if($tipo==2){$costo_unitario*=2;/*si es muestra*/}}?>
                      <div class="input-group input-80">
                        <input class="form-control form-control-sm cu<?php echo $rand;?>" type="text" id="cu<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $costo_unitario;?>" data-value="<?php echo $costo_unitario;?>">
                        <span class="input-group-addon form-control-sm">Bs.</span>
                      </div>
                    </td>
                    <?php for($i=0; $i < count($sucursales) ; $i++){ $sucursal=$sucursales[$i]; 
                            $pgc_sc=$sucursal->idsc."-".rand(10,999999);
                    ?>
                    <td>
                        <div class="input-group input-100">
                          <input class="form-control form-control-sm cantidad<?php echo $rand;?>" type="number" id="cantidad<?php echo $pgc_sc;?>" data-sc="<?php echo $sucursal->idsc;?>" data-tbl="table#<?php echo $idtb;?>" data-type="new" placeholder="0" min="0" max="9999" >
                          <span class="input-group-addon form-control-sm">u.</span>
                        </div>
                    </td>
                    <?php } ?>
                    <td>
                      <div class="input-group input-100">
                        <input class="form-control form-control-sm" type="text" id="ct<?php echo $pgc;?>" placeholder="0" disabled="disabled">
                        <span class="input-group-addon form-control-sm">u.</span>
                      </div>
                    </td>
                    <td>
                      <div class="input-group input-100">
                        <input class="form-control form-control-sm" type="text" id="st<?php echo $pgc;?>" placeholder="0" disabled="disabled">
                        <span class="input-group-addon form-control-sm">Bs.</span>
                      </div>
                    </td>
                    <td>
                        <div class="input-group input-100">
                          <input class="form-control form-control-sm costo_venta<?php echo $rand;?>" id="costo_venta<?php echo $pgc;?>" placeholder="0" disabled="disabled">
                          <span class="input-group-addon form-control-sm">Bs.</span>
                        </div>
                    </td>
                    <td>
                      <div class="input-group input-150">
                        <textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"></textarea>
                        <span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
                      </div>
                    </td>
                    <td>
                      <span class="g-control-accordion">
                        <button class="drop_elemento<?php echo $rand;?>" data-padre="row-producto" data-type-padre="atr" data-module="producto_color_pedido" data-tbl='table#<?php echo $idtb;?>'><i class="icon-close"></i></button>
                      </span>
                    </td>
              </tr>
              <?php }?>
            </tbody>
            <thead>
              <tr class="row-total">
                <th colspan="<?php echo 3+count($sucursales);?>" class="text-right">Totales</th>
                <th class="sub_can_pro">0</th>
                <th class="sub_cos_pro">0</th>
                <th class="sub_tot_cos_pro">0</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
          </table>
        </div> 
        <div class="row text-right" style="padding-right:15px; margin-top: 5px;">
          <button type="button" class="btn btn-inverse-info waves-effect btn-mini colores_producto<?php echo $rand;?>" data-tbl="<?php echo $idtb;?>" data-p="<?php echo $producto->idp;?>" data-><i class="fa fa-plus"></i><span class="m-l-10">categoría</span>
          </button>
        </div>
    </div>
  </div>
  <script>$('[data-toggle="popover"]').popover({html:true});$(".cantidad<?php echo $rand;?>").costo_venta_tbl();$(".drop_elemento<?php echo $rand;?>").drop_elemento();$(".colores_producto<?php echo $rand;?>").colores_producto();$(".view_producto<?php echo $rand;?>").view_producto();$("img.img-thumbnail-45").visor();</script>
</div>