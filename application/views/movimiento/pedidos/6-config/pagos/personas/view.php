<?php $url=base_url().'libraries/img/';?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="2%" class="img-thumbnail-50"><div class="img-thumbnail-50">#item</div></th>
					<th width="85%">Nombre completo</th>
					<th width="5%">Empleado</th>
					<th width="5%">Usuario</th>
					<th width="5%">Directivo</th>
				</tr>
			</thead>
			<tbody>
		<?php if(count($personas)>0){ $cont=1;?>
			<?php for ($i=0; $i < count($personas); $i++) { $persona=$personas[$i]; 
					$es_empleado=false;
					$es_usuario=false;
					$es_directivo=false;
					$empleado=$this->lib->search_elemento($empleados,"ci",$persona->ci);
					$usuario=$this->lib->search_elemento($usuarios,"ci",$persona->ci);
					$directivo=$this->lib->search_elemento($directivos,"ci",$persona->ci);
					if($empleado!=null){if($empleado->estado=="1"){$es_empleado=true;}}
					if($usuario!=null){$es_usuario=true;}
					if($directivo!=null){$es_directivo=true;}
					if($es_empleado || $es_usuario || $es_directivo){
						$img="sistema/miniatura/default.jpg";
						if($persona->fotografia!="" && $persona->fotografia!=NULL){$img="personas/miniatura/".$persona->fotografia;}
			?>
				<tr>
					<td><div class="item"><?php echo $cont++;?></div><img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail img-thumbnail-50" data-title="<?php echo $persona->nombre_completo; ?>" data-desc="<br>"></td>
					<td><?php echo $persona->nombre_completo;?></td>
					<td><?php if($es_empleado){ echo "si";}?></td>
					<td><?php if($es_usuario){ echo "si";}?></td>
					<td><?php if($es_directivo){ echo "si";}?></td>
				</tr>
			<?php }//end if
			}//end for ?>
		<?php }else{?>
				<tr>
					<td colspan="6" class="text-center"><h3>0 registros encontrados...</h3></td>
				</tr>
		<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script>$(".img-thumbnail-50").visor();</script>