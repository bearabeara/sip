<?php 
	$url=base_url().'libraries/img/';
	$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$total_descuentos=0;
	for($i=0;$i<count($descuentos_producto);$i++){
		$total_descuentos+=($this->lib->desencriptar_num($descuentos_producto[$i]->cantidad)*1);
	}
    $cantidad=number_format(($this->lib->desencriptar_num($descuento_producto->cantidad)*1),0,'.','');
    $cu_venta=($this->lib->desencriptar_num($parte_pedido->cu)*1);
    $total=$cu_venta*$cantidad;
    $descuento=$total*$descuento_producto->porcentaje;
	$porcentaje=$descuento_producto->porcentaje*100;
	$max=(($this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1)-$total_descuentos)+$cantidad;
	$help1='title="Cantidad" data-content="Ingrese la cantidad de productos a ser descontados, la cantidad total pedida es <strong>'.($this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1).'u.</strong>, la cantidad total descontada es <strong>'.($total_descuentos-$cantidad).'u.</strong> y la <strong>cantidad maxima que puede ingresar es '.$max.'u.</strong>"';
	$help2='title="CU/venta" data-content="Costo unitario de venta registado en el pedido."';
	$help3='title="Costo total" data-content="Representa al costo total del descuento por producto."';
	$help4='title="Porcentaje" data-content="Ingrese el porcentaje de descuento, el valor debe ser <strong>mayor a cero y menor o igual a 100</strong>. y solo se aceptan numeros <strong>con una sola decimal</strong>"';
	$help5='title="Descuento" data-content="Representa al descuento a realizarse en el pedido."';
	$help6='title="Observaciónes" data-content="Ingrese algunas observaciónes de descuento en el producto. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 900 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
    $rand=rand(10,99999999);
?>
<ul class="nav nav-tabs md-tabs" role="tablist" style="border-bottom: 0px;">
    <li class="nav-item">
        <a class="nav-link config_descuento_producto<?php echo $rand;?> active" data-des="<?php echo $descuento_producto->iddes;?>" data-pe="<?php echo $parte_pedido->idpe;?>" href="javascript:" style="padding: 0px 0px 8px 0px !important;">Modificar</a>
        <div class="slide"></div>
    </li>
    <li class="nav-item">
        <a class="nav-link config_descuento_producto_detalle<?php echo $rand;?>" data-des="<?php echo $descuento_producto->iddes;?>" data-pe="<?php echo $parte_pedido->idpe;?>" href="javascript:" style="padding: 0px 0px 8px 0px !important;">Detalles</a>
        <div class="slide"></div>
    </li>
</ul>
<div class="list-group">
    <div class="list-group-item" style="max-width:100%">
        <div class="table-responsive">
            <table class="table table-bordered" style="margin-bottom: 0rem;">
                <tr>
                    <td width="15%"><?php echo $v[$parte_pedido->tipo]." ".$parte_pedido->numero;?></td>
                    <td class='img-thumbnail-35'><div class="img-thumbnail-35"></div>
                    <img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-35" data-title="<?php echo $grupo->nombre;?>" data-desc="<br/>">
                    </td>
                    <td width="15%"><?php echo $grupo->codigo;?></td>
                    <td width="50%"><?php echo $grupo->nombre;?></td>
                    <td width="30%"><?php echo $sucursal->nombre_sucursal;?></td>
                </tr>
            </table>
        </div>
    </div>
	<div class="list-group-item" style="max-width:100%">
    <div class="row hidden-sm"><div class="col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
    <div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Cantidad:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_banco">
						<div class="input-group">
                            <input type='number' class="form-control form-control-xs calc-ct" id="cantidad-2" value="<?php echo $cantidad;?>" min="0" max="<?php echo $max;?>" data-max="<?php echo $max;?>">
                            <span class="input-group-addon form-control-sm">u.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">C/U venta:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_banco">
						<div class="input-group">
                            <input type='number' class="form-control form-control-xs" id="cu-2" disabled="disabled" value="<?php echo $cu_venta;?>" data-value="<?php echo $cu_venta;?>">
                            <span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class="fa fa-info-circle"></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Costo total:</label>
                <div class="col-sm-4 col-xs-12">
					<form class="save_banco">
						<div class="input-group">
                            <input type='number' class="form-control form-control-xs" id="ct-2" disabled="disabled" value="<?php echo number_format($total,1,'.','');?>">
                            <span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class="fa fa-info-circle"></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>% de desc.</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_banco">
						<div class="input-group">
                            <input type='number' class="form-control form-control-xs calc-ct" id="porcentaje-2" step="any" min="0" max="100" value="<?php echo $porcentaje;?>">
                            <span class="input-group-addon form-control-sm">%</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class="fa fa-info-circle"></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Descuento:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_banco">
						<div class="input-group">
                            <input type='number' class="form-control form-control-xs" id="descuento-2" disabled="disabled" value="<?php echo number_format($descuento,1,'.','');?>">
                            <span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class="fa fa-info-circle"></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Observaciónes:</label>
				<div class="col-sm-10 col-xs-12">
					<form class="save_banco">
						<div class="input-group">
                            <textarea class="form-control form-control-xs" id="observacion-2" placeholder="Observaciónes" maxlength="900" rows="3"><?php echo $descuento_producto->observacion;?></textarea>
            			    <span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class="fa fa-info-circle"></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
		</div>
    </div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$("img.img-thumbnail-35").visor();$(".adicionar_detalle_descuento<?php echo $rand;?>").adicionar_detalle_descuento();$(".config_descuento_producto<?php echo $rand;?>").config_descuento_producto();$(".config_descuento_producto_detalle<?php echo $rand;?>").config_descuento_producto_detalle();$("input.calc-ct").total_update_descuento_producto();</script>