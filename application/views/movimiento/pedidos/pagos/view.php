<?php $pedido=$pedido[0];
	$help1='title="<h4>Nombre de unidad de medida<h4>" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 40 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="<h4>Abreviatura de unidad de medida<h4>" data-content="Ingrese una abreviatura alfanumerica <strong>de 1 a 10 caracteres con espacios</strong>, ademas la abreviatura solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>. Ej. Centímetros = cm"';
	$help3='title="<h4>Equivalencia de unidad<h4>" data-content="Ingrese una equivalencia numerica mayor que cero y con un maximo de 7 decimales. Esta equivalencia en usada por el sistema para calcular los materiales necesarios en producción."';
	$help4='title="<h4>Descripción de equivalencia<h4>" data-content="Ingrese una descripción de equivalencia alfanumerica de 0 a 200 caracteres <b>con espacios</b>, ademas la descripción solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>. Esta descripción debe contener a que unidad pertenece la equivalencia."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$t_pag=0;
	for ($i=0; $i < count($pagos) ; $i++) { $t_pag+=$pagos[$i]->monto;}
?>
<div class="row">
	<div class="col-xs-12"><strong><h3>Nº de pedido <?php echo $pedido->numero;?></h3></strong></div>
	<div class="col-sm-6 col-md-4 col-xs-12"><strong>Costo de pedido (Bs.): <ins><?php echo number_format($pedido->monto_total,2,'.',',');?></ins></strong> </div>
	<div class="col-sm-6 col-md-4 col-xs-12"><strong>Total Pagos (Bs.): <ins><?php echo number_format($t_pag,2,'.',',');?></ins></strong> </div>
	<div class="col-sm-6 col-md-4 col-xs-12"><strong><?php if(($pedido->monto_total-$t_pag)>=0){ echo "Saldo"; $valor=($pedido->monto_total-$t_pag);}else{ echo "Excedente";$valor=($pedido->monto_total-$t_pag)*-1;}?> de pedido (Bs.): <ins><?php echo number_format($valor,2,'.',',');?></ins></strong> </div>
</div>
<div class="table-responsive">
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="25%">Fecha de pago</th>
				<th width="25%">Monto (Bs.)</th>
				<th width="35%">Observaciónes</th>
				<th width="10%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($pagos) ; $i++) { $pago=$pagos[$i];?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td>
				<div class="input-group">
					<input class="form-control input-sm" type="date" placeholder='2000-12-31' id="p_fec<?php echo $pago->idpa;?>" value="<?php echo $pago->fecha;?>">
					<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
			</td>
			<td>
				<form onsubmit="return update_pago('<?php echo $pago->idpa;?>','<?php echo $pedido->idpe;?>')">
					<div class="input-group">
						<input class="form-control input-sm" type="number" placeholder="Monto" id="p_pag<?php echo $pago->idpa;?>" min='0' max='9999999.9' step='any' value="<?php echo $pago->monto;?>">
						<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</form>
			</td>
			<td>
				<div class="input-group">
					<textarea class="form-control input-sm" id="p_obs<?php echo $pago->idpa;?>" placeholder='Observaciónes del pago' maxlength="500"><?php echo $pago->observaciones;?></textarea>
					<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
				</div>
			</td>
			<td>
				<?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"update_pago('".$pago->idpa."','".$pedido->idpe."')",'eliminar'=>"alerta_pago('".$pago->idpa."','".$pedido->idpe."')"]);?>
			</td>
		</tr>
	<?php }?>
		</tbody>
		<thead>
			<tr>
				<th colspan="5" class="text-center"><ins>NUEVO PAGO</ins></th>
			</tr>
			<tr>
				<td colspan="2">
					<div class="input-group">
						<input class="form-control input-sm" type="date" placeholder='2000-12-31' id="p_fec" value="<?php echo date('Y-m-d');?>">
						<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</td>
				<td>
				<form onsubmit="return save_pago('<?php echo $pedido->idpe;?>')">
					<div class="input-group">
						<input class="form-control input-sm" type="number" placeholder="Monto" id="p_pag" min='0' max='9999999.9' step='any' value="0">
						<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</form>
				</td>
				<td>
					<div class="input-group">
						<textarea class="form-control input-sm" id="p_obs" placeholder='Observaciónes del pago' maxlength="500"></textarea>
						<span class="input-group-addon" <?php echo $popover2.$help4;?>><i class='glyphicon glyphicon-info-sign'></i></span>
					</div>
				</td>
				<td>
					<?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_pago('".$pedido->idpe."')",'eliminar'=>""]);?>
				</td>
			</tr>
		</thead>
	</table>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true});</script>