<?php 
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Materiales-pedido-$pedido->nombre-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Materiales-pedido-$pedido->nombre-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");



$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
$url=base_url().'libraries/img/';
$detalles_parte_pedido=$this->lib->select_from($detalles_pedido,'idpp',$parte->idpp);
$v_material = array();
for($i=0;$i<count($detalles_parte_pedido);$i++){$detalle_parte_pedido=json_decode($detalles_parte_pedido[$i]);
	/*buscando material en producto*/
	$pgc=$this->lib->search_elemento($productos_grupos_colores,"idpgrc",$detalle_parte_pedido->idpgrc);
	if($pgc!=null){
		$pg=$this->lib->search_elemento($productos_grupos,"idpgr",$pgc->idpgr);
		if($pg!=null){
				//buscando cantidad pedida
			$cantidad=0;
			$sucursales=$this->lib->select_from($sucursales_detalles_pedidos,'iddp',$detalle_parte_pedido->iddp);
			for($j=0; $j<count($sucursales);$j++){ 
				$cantidad+=($this->lib->desencriptar_num(json_decode($sucursales[$j])->cantidad)*1);
			}
				//buscando materiales de pila de productos
			$producto=$this->M_producto->get($pg->idp);
			$materiales=$this->lib->select_from($productos_materiales,"idp",$pg->idp);
			for($j=0; $j<count($materiales); $j++){$material=json_decode($materiales[$j]);
				$unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
				if($unidad!=null){
					$img="sistema/miniatura/default.jpg";
					if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
					$v_material[]= array('idmi'=>$material->idmi,'nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad,'cantidad_pedida' =>$cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad->nombre,'abr_medida' => $unidad->abr);
				}
			}
			$materiales=$this->lib->select_from($productos_grupos_materiales,"idpgr",$pg->idpgr);
			for($j=0; $j<count($materiales); $j++){$material=json_decode($materiales[$j]);
				$unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
				if($unidad!=null){
					$img="sistema/miniatura/default.jpg";
					if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
					$v_material[]= array('idmi'=>$material->idmi,'nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad,'cantidad_pedida' =>$cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad->nombre,'abr_medida' => $unidad->abr);
				}
			}
			$materiales=$this->lib->select_from($productos_colores_materiales,"idpgrc",$pgc->idpgrc);
			for($j=0; $j<count($materiales); $j++){$material=json_decode($materiales[$j]);
				$unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
				if($unidad!=null){
					$img="sistema/miniatura/default.jpg";
					if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
					$v_material[]= array('idmi'=>$material->idmi,'nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad,'cantidad_pedida' =>$cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad->nombre,'abr_medida' => $unidad->abr);
				}
			}
		}
	}
}
$materiales_productos=json_decode(json_encode($v_material));
$total_materiales=array();
for($i=0;$i<count($all_materiales);$i++){$all_material=$all_materiales[$i];
	$cantidad=0;
	$aux=array();
	foreach ($materiales_productos as $key => $producto_material) {
		if($all_material->idmi==$producto_material->idmi){
			$cantidad+=($producto_material->cantidad_pedida*1*$producto_material->cantidad_por_producto);
			$aux=$producto_material;
		}
	}
	if($cantidad>0){
		$material=json_decode(json_encode($aux));
		$total_materiales[]=array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $material->imagen,'color' => $material->color,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad_por_producto,'cantidad_necesaria' =>$cantidad,'cantidad_almacenes' =>$all_material->cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$material->medida,'abr_medida' => $material->abr_medida);
	}
}
$total_materiales=json_decode(json_encode($total_materiales));

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3>MATERIALES NECESARIOS EN PEDIDO: <?php echo $pedido->nombre;?></h3></center>
<table border="1" cellpadding="5" cellspacing="0">
	<thead>
		<tr class="fila">
			<th>#</th>
		<?php if($file=="doc"){?>
			<th>Fotografía</th>
		<?php }?>
			<th>Código</th>
			<th>Material</th>
			<th>Color</th>
			<th>Cantidad el almacenes</th>
			<th>Cantidad el nesesaria</th>
			<th>Stock</th>
		</tr>
	</thead>
	<tbody>
		<?php $cont=0;
		foreach($total_materiales as $key=>$material){ $cont++;
			?>
			<tr class="fila">
				<td><?php echo $cont;?></td>
			<?php if($file=="doc"){?>
				<td><img src="<?php echo $material->imagen;?>" width="50px;"></td>
			<?php }?>
				<td><?php echo $material->codigo;?></td>
				<td><?php echo $material->nombre;?></td>
				<td><?php echo $material->color;?></td>
				<td><?php echo $material->cantidad_almacenes;?></td>
				<td><?php echo $material->cantidad_necesaria;?></td>
				<td><?php if($material->cantidad_almacenes<$material->cantidad_necesaria){?>insuficiente<?php }else{?>suficiente<?php } ?></td>
			</tr>
			<?php } ?>

		</tbody>
	</table>