<?php
	$url=base_url().'libraries/img/';
	$tipo="";
	$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$rand=rand(10,9999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link reporte_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab">Pedido</a></div></li>
	<li class="nav-item"><a class="nav-link active taller_pedido" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"> Taller </a></div></li>
	<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
	    <li class="nav-item"><a class="nav-link informe_economico_pedido_detallado" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico det.</a></div></li>
	    <li class="nav-item"><a class="nav-link informe_economico_pedido_general" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico gral.</a></div></li>
        <li class="nav-item"><a class="nav-link informe_depositos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Depositos</a></div></li>
	<?php }?>
	<li class="nav-item"><a class="nav-link informe_materiales" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Materiales necesarios</a></div></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; padding-bottom: 0px !important;">
		<?php
			if(count($partes_pedido)>0){
				for($i=0;$i<count($partes_pedido);$i++){ $parte=$partes_pedido[$i];
					$seleccionado=false;
					if($parte_pedido->idpp==$parte->idpp){
						$seleccionado=true;
					}
		?>
		<button type="button" class="btn btn-<?php if(!$seleccionado){?>inverse-<?php }?>info btn-mini waves-effect taller_pedido" data-pe="<?php echo $pedido->idpe;?>" data-pp="<?php echo $parte->idpp;?>" style="margin-bottom: 5px;"><?php echo $v[$parte->tipo]." ".$parte->numero;?></button>
		<?php 	}
			}
		?>
	<?php
		$fun = array('function' => 'taller_pedido', 'atribs' => array('pe' => $pedido->idpe,'pp' => $parte_pedido->idpp));
		$excel = array('controller' => 'movimiento/exportar_taller_pedido?pe='.$pedido->idpe.'&pp='.$parte_pedido->idpp.'&file=xls');
		$word = array('controller' => 'movimiento/exportar_taller_pedido?pe='.$pedido->idpe.'&pp='.$parte_pedido->idpp.'&file=doc');
		$default = array('top' => 0.5,'right' => 2,'bottom' => 0.5,'left' => 2,'page' => 1);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true,'default'=>json_encode($default)]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive" id="area">
			<table border="0" class="tabla tbl-bordered">
			<thead>
				<tr class="fila title" style="text-align: center;">
					<td class="celda title" colspan="<?php echo count($sucursales)+3;?>" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
						<?php $this->load->view('estructura/print/header-print',['titulo'=>'PEDIDO '.$pedido->nombre." - ".$v[$parte_pedido->tipo]." ".$parte_pedido->numero]);?>
					</td>
				</tr>
			</thead>
		<?php $cpro=1; 
			foreach($productos as $key => $producto){
			$tot_suc=[];
			$tot=0;for($i=0;$i<count($sucursales);$i++){ $tot_suc[$i]=0;}
			$grupos=json_decode(json_encode($producto->categorias));
		?>
			<tbody>
				<tr class="fila">
					<td class="celda td img" width="54px" style="vertical-align: top;">
						<div class="item"><?php echo $cpro++;?></div>
						<img src="<?php echo $producto->fotografia;?>" width="54px;">
					</td>
					<td class="celda td sub-table" width="100%">
						<table width="100%">
							<tr>
								<td width="55%"><strong><?php echo $this->lib->all_mayuscula($producto->codigo." - ".$producto->nombre); ?></strong></td>
							<?php for($s=0;$s<count($sucursales);$s++){ ?>
								<td width="8%" class="text-center"><strong><?php echo $this->lib->ini_mayuscula($sucursales[$s]->nombre);?></strong></td>
							<?php } ?>
								<td width="10%" class="text-center"><strong>Total [u.]</strong></td>
							</tr>
							<?php foreach ($grupos as $key => $grupo){ ?>
								<tr class="tr">
									<td><?php echo $grupo->nombre;?>
										<?php if($grupo->observacion!="" && strpos($grupo->observacion, "Precio") === false){?><span style="color: rgba(255, 0, 0, .7);">(<?php echo $grupo->observacion; ?>)</span><?php }?>
										
									</td>
									<?php $sub_total=0;
										for($i=0;$i<count($sucursales);$i++){ $sucursal=$sucursales[$i];
											$cantidad=0;
											$j_sucursales=json_decode(json_encode($grupo->sucursales));
											foreach($j_sucursales as $key => $suc){
												if($suc->idsc==$sucursal->idsc){ $cantidad=$suc->cantidad; break; }
											}
											$sub_total+=($cantidad*1);
									?>
										<td style="text-align: right;"><?php if($cantidad>0){ echo $cantidad; $tot_suc[$i]+=($cantidad*1);} ?></td>
									<?php } ?>
									<td style="text-align: right;"><?php echo $sub_total; $tot+=$sub_total; ?></td>
								</tr>
							<?php } ?>
							<tr>
								<td class="text-right"><strong>TOTAL</strong></td>
							<?php for($s=0;$s<count($sucursales);$s++){ ?>
								<td class="text-right"><strong><?php if($tot_suc[$s]>0){ echo $tot_suc[$s];}?></strong></td>
							<?php } ?>
								<td class="text-right"><strong><?php if($tot>0){echo $tot;}?></strong></td>
							</tr>
						</table>
					</td>
				</tr>
			<?php } ?>
			</tbody>
			</table>
		</div>
	</div>
</div>
<script>$("a.reporte_pedido<?php echo $rand;?>").reporte_pedido();$(".taller_pedido").taller_pedido();$("a.informe_economico_pedido_detallado").informe_economico_pedido_detallado();$("a.informe_economico_pedido_general").informe_economico_pedido_general();$("a.informe_depositos").informe_depositos();$("a.informe_materiales").informe_materiales();</script>