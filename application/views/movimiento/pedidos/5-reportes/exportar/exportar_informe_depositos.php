<?php
    $fecha=date("Y-m-d");
    if($file=="xls"){
        header("Content-type: application/vnd.ms-excel; name='excel'");
    }
    if($file=="doc"){
        header("Content-type: application/vnd.ms-word; name='word'");
    }
    header('Pragma: public');
    header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1
    header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
    header('Pragma: no-cache');
    header('Expires: 0');
    header('Content-Transfer-Encoding: none');
    if($file=="xls"){
        header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera
        header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest
        header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-detalle_depositos.xls");
    }
    if($file=="doc"){
        header('Content-type: application/vnd.ms-word;charset=utf-8');
        header('Content-type: application/x-msword; charset=utf-8');
        header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-detalle_depositos.doc");
    }
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
$total_monto=0;
$total_monto_bs=0;
$total_pedido=0;
$total_descuento=0;
$total_recibido=0;
$total_recibido_bs=0;
for($i=0; $i < count($detalles_pedido); $i++){ $detalle_pedido=$detalles_pedido[$i];
    $total_pedido+=$this->lib->desencriptar_num($detalle_pedido->cv);
}
$total_pedido-=$descuento;
?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <center><h3>Reporte de detalle de depósitos</h3></center>
    <table border="1" cellspacing="0" cellpadding="4" width="100%">
        <thead>
        <tr>
            <th width="1%">#</th>
            <th width="15%">Registrado por</th>
            <th width="11%">Fecha de deposito</th>
            <th width="11%">Depositante</th>
            <th width="16%">Propietario de cuenta</th>
            <th width="13%">Banco</th>
            <th width="5%">N° de cuenta</th>
            <th width="6%">Monto depositado</th>
            <th width="6%">Tipo de cambio</th>
            <th width="6%">Monto [Bs.]</th>
            <th width="6%">Descuento</th>
            <th width="6%">Monto recibido</th>
            <th width="6%">Monto recibido [Bs.]</th>
            <th width="6%" width="6%">Saldo Pendiente [Bs.]</th>
            <th width="19%">Observaciónes</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($pagos)>0){?>
            <?php for($i=0; $i<count($pagos);$i++){$pago=$pagos[$i];
                $usuario=$this->lib->search_elemento($personas,"ci",$pago->ci_usuario);
                if($usuario!=null){$usuario=$usuario->nombre_completo;}

                $fecha=$pago->fecha;
                $banco=$this->lib->search_elemento($bancos,"idba",$pago->idba);
                if($banco!=null){
                    $banco=$banco->razon;
                }
                $propietario=$this->lib->search_elemento($personas,"ci",$pago->ci_persona);
                if($propietario!=null){
                    $propietario=$propietario->nombre_completo;
                }
                $monto_depositado=$this->lib->desencriptar_num($pago->monto);
                $monto_bs=$this->lib->desencriptar_num($pago->monto_bs);
                $moneda="";
                $tc=$this->lib->search_elemento($tipos_cambio,"idtc",$pago->idtc);
                if($tc!=null){
                    $moneda=$tc->simbolo;
                }
                $descuentos=$this->lib->select_from($descuentos_pagos,"idpa",$pago->idpa);
                $s_descuento=0;
                for($j=0;$j<count($descuentos);$j++){
                    $descuento=json_decode(json_encode($descuentos[$j]));
                    $s_descuento+=($this->lib->desencriptar_num($descuento->monto)*1);
                }
                $s_descuento=number_format($s_descuento,1,'.','');
                $recibido_bs=number_format((($monto_depositado-$s_descuento)*$pago->tipo_cambio),1,'.','');
                ?>
                <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $usuario;?></td>
                    <td><?php echo $this->lib->format_date($fecha,'Y/m/d');?></td>
                    <td><?php echo $pago->depositante;?></td>
                    <td><?php echo $propietario;?></td>
                    <td><?php echo $banco;?></td>
                    <td><?php echo " ".$pago->cuenta.". ";?></td>
                    <td style="text-align: right;"><?php echo number_format($monto_depositado,1,',','');$total_monto+=$monto_depositado;?></td>
                    <td style="text-align: right;"><?php echo $pago->tipo_cambio;?></td>
                    <td style="text-align: right;"><?php echo number_format($monto_bs,1,',','');$total_monto_bs+=$monto_bs;?></td>
                    <td style="text-align: right;"><?php echo number_format($s_descuento,1,',','');$total_descuento+=$s_descuento; ?></td>
                    <td style="text-align: right;"><?php echo number_format(($monto_depositado-$s_descuento),1,',','');$total_recibido+=($monto_depositado-$s_descuento);?></td>
                    <td style="text-align: right;"><?php echo number_format($recibido_bs,1,',','');$total_recibido_bs+=$recibido_bs;?></td>
                    <td style="text-align: right;"><?php echo number_format($total_pedido - $total_recibido_bs,2,'.',',');?></td>
                    <td><?php echo $pago->observacion;?></td>
                </tr>
            <?php } ?>
        <?php }else{?>
            <tr class="fila"><th colspan="15" class="text-center"><h3>0 depositos registrados...</h3></th></tr>
        <?php }?>
        </tbody>
        <thead>
        <tr>
            <th colspan="7" class="text-right">TOTAL</th>
            <th style="text-align: right"><?php echo number_format($total_monto,1,',',''); ?></th>
            <th></th>
            <th style="text-align: right"><?php echo number_format($total_monto_bs,1,',',''); ?></th>
            <th style="text-align: right"><?php echo number_format($total_descuento,1,',',''); ?></th>
            <th style="text-align: right"><?php echo number_format($total_recibido,1,',',''); ?></th>
            <th style="text-align: right"><?php echo number_format($total_recibido_bs,1,',',''); ?></th>
            <th colspan="2"></th>
        </tr>
        <tr class="fila">
            <th colspan="12" style="text-align: right">TOTAL COSTO DE PEDIDO [Bs.]</th>
            <th style="text-align: right"><?php echo number_format($total_pedido,1,',',''); ?></th>
            <th colspan="2"></th>
        </tr>
        <tr class="fila">
            <th colspan="12" style="text-align: right;">SALDO PENDIENTE [Bs.]</th>
            <th style="text-align: right"><?php echo number_format($total_pedido-$total_recibido_bs,1,',',''); ?></th>
            <th colspan="2"></th>
        </tr>
        </thead>
    </table>

