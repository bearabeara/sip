<?php
$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
$help2='title="Tipo de sub pedido" data-content="Seleccione el tipo de sub pedido si es una <strong>parte</strong>, <strong>extra</strong> o <strong>muestra</strong>, el número es generado automaticamente."';
$help3='title="Fecha de sub pedido" data-content="Ingrese la fecha de registro de este pedido."';
$help4='title="Observaciónes del sub pedido" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 300 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
$help5='title="Costo del sub pedido" data-content="Es el costo total del pedido segun el costo de venta de cada producto, este costo es <strong>generado automaticamente</strong> por el sistema."';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
$url=base_url().'libraries/img/';
$rand=rand(10,99999);
$v =json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
$tipo="";
$vd=[];
foreach($productos as $key => $producto){
	$grupos=json_decode(json_encode($producto->categorias));
	foreach($grupos as $key => $grupo){
		$vd[]=array('iddp'=>$grupo->iddp,'status' => '1');
	}
}
$detalles=json_encode($vd);
?>
<ul class="nav nav-tabs" role="tablist">
	<?php for($i=0; $i < count($partes) ; $i++){
		foreach ($v as $clave => $opcion){
			if($clave==$partes[$i]->tipo){ $tipo=$opcion;break;}
		}
	?>
		<li class="nav-item"><a class="nav-link config_produccion<?php echo $rand;?> <?php if($partes[$i]->idpp==$parte_pedido->idpp){ echo 'active';} ?>" href="javascript:" role="tab" data-pp="<?php echo $partes[$i]->idpp;?>" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i><?php echo $tipo;?> <?php echo $partes[$i]->numero;?></a></div></li>
		<?php } ?>
	</ul>
	<div class="list-group">
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="col-xs-12">
					<span class="g-control-accordion">
						<button class="config_produccion<?php echo $rand;?>" id="datos_parte_pedido" data-pp="<?php echo $parte_pedido->idpp;?>" data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-refresh"></i></button>
						<button id="btn-mov-part" data-pp="<?php echo $parte_pedido->idpp;?>"  data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-clock-o"></i></button>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Fecha de sub pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<label  style="text-transform: none;"><?php echo $this->lib->format_date($parte_pedido->fecha,"dl ml2 Y"); ?></label>
					</div>
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<label  style="text-transform: none;"><?php echo $pedido->nombre; ?></label>
					</div>
				</div><i class="clearfix"></i>
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
					<div class="col-sm-10 col-xs-12">
						<label  style="text-transform: none;"><?php echo $parte_pedido->observacion; ?></label>
					</div>
				</div><i class="clearfix"></i>
			</div>
		</div>
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="form-group col-xs-12">
					<div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true" data-cli='<?php echo $pedido->idcl;?>'>
						<?php $c1=1;
						foreach($productos as $key => $producto){
							$id=$producto->idp."-".rand(0,999999); 
							$producto_proceso=$this->M_producto_proceso->producto_proceso("pp.idp",$producto->idp,"p.nombre","asc");
							$grupos=json_decode(json_encode($producto->categorias));
							$porcentaje=0;
							$cont=0;
							foreach($grupos as $key => $grupo_aux){
								$porcentaje+=($grupo_aux->porcentaje*1);
								$cont++;
							}
							if(count($cont)>0){$porcentaje/=$cont;$porcentaje=number_format($porcentaje,3,'.','');$porcentaje*=100;}
							?>
							<div class="accordion-panel" id="accordion-panel<?php echo $id;?>" data-cli="<?php echo $cliente->idcl;?>">
								<div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
									<span class="card-title accordion-title">
										<span class="g-control-accordion">
											<?php $idtbl=$producto->idp.'-'.rand(0,999999);?>
											<button class="refresh_producto_pedido" data-p="<?php echo $producto->idp;?>" data-pp="<?php echo $parte_pedido->idpp;?>" data-tbl="<?php echo $idtbl;?>"><i class="fa fa-refresh"></i></button>
											<?php if($pedido->estado!="2"){?>
											<?php }?>
										</span>
										<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>" style="padding: 10px 6px 10px 10px;">
											<div class="item"><?php echo $c1++;?></div>
											<span class="g-img-modal"><img src="<?php echo $producto->fotografia;?>" class="img-thumbnail g-thumbnail-modal"></span>
											<span><?php echo $producto->codigo.' '.$producto->nombre; ?></span>
											<div class="accordion-progress" id="accordion-progress-<?php echo $idtbl;?>" style="float: right; margin-top: 5px;">
												<div class="progress" style="height: 17px;">
													<div class="progress-bar progress-bar-striped <?php if($porcentaje<100){?>progress-bar-animated<?php }?>" role="progressbar" aria-valuemax="100" style="width: <?php echo $porcentaje."%";?>"><?php if($porcentaje>17){ echo $porcentaje."%";} ?></div>
													<?php if($porcentaje<=17){?><span style="margin-left: 5px;"><?php echo $porcentaje."%";?></span><?php } ?>
												</div>
											</div>
											</a>
										</span>
								</div>
									<div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
										<div class="accordion-content accordion-desc" id="accordion-content-<?php echo $idtbl;?>" data-pr="<?php echo $producto->idp;?>">
											<?php $idtab=rand(10,9999999); ?>
											<ul class="nav nav-tabs md-tabs tabs-left b-none" role="tablist">
											<?php $sw=0;
												foreach($grupos as $key => $grupo){
													$idtbc=$producto->idp.'-'.$grupo->idpgr.'-'.$grupo->idpgrc;
													$pgc=$grupo->idpgrc."-".rand(1,9999);
													$porcentaje=$grupo->porcentaje*100;
											?>
													<li class="nav-item">
														<a class="nav-link <?php if($sw==0){?>active<?php $sw=1;}?>" data-toggle="tab" href="#color<?php echo $grupo->idpgrc;?>" role="tab" style="padding-top: 0px !important;">
															<table style="margin-left: 5px;" width="250px;">
																<tr>
																	<td>
																		<div class="img-thumbnail-45"><img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail img-thumbnail-45"></div>
																	</td>
																	<td width="100%" style="padding-left: 6px;">
																		<div class="progress" id="color-progress-<?php echo $idtbc;?>" style="height: 15px;font-size: 10px; width: 100%">
																			<div class="progress-bar progress-bar-striped <?php if($porcentaje<100){?>progress-bar-animated<?php }?> bg-info" role="progressbar" style="width: <?php echo $porcentaje;?>%"><strong><?php if($porcentaje>15){ echo $porcentaje."%";} ?></strong></div>
																			<?php if($porcentaje<=15){?><strong style="margin-left: 5px;"><?php echo $porcentaje."%";?></strong><?php } ?>
																		</div>
																		<?php echo $grupo->nombre;?>
																	</td>
																</tr>
															</table>
														</a>
														<div class="slide" style="height: 45px;"></div>
													</li>
											<?php
												}//End foreach
											?>
											</ul>
													<div class="tab-content tabs-left-content" style="padding-left: 10px; width: 100%;">
													<?php $sw=0;
														foreach($grupos as $key => $grupo){
															$idtbc=$producto->idp.'-'.$grupo->idpgr.'-'.$grupo->idpgrc;
													?>
															<div class="tab-pane <?php if($sw==0){?>active<?php $sw=1;}?>" id="color<?php echo $grupo->idpgrc;?>" role="tabpanel">
																<div class="panels-wells well well-sm">
																	<span class="g-control-accordion">
																		<button class="refresh_producto_color_pedido" data-dp="<?php echo $grupo->iddp;?>" data-p="<?php echo $producto->idp;?>" data-pp="<?php echo $parte_pedido->idpp;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-tbl-color="<?php echo $idtbc;?>" data-tbl-producto="<?php echo $idtbl;?>"><i class="icon-refresh"></i></button>
																	</span><br>
																	<div id="content-colors-sucursal-<?php echo $idtbc;?>">
																		<?php /*procesos del color*/
																			$procesos=$this->lib->producto_grupo_color_procesos($grupo,$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso);
																			$procesos=json_decode(json_encode($procesos));
																			/*sucursales*/
																			$j_sucursales=json_decode(json_encode($grupo->sucursales));
																			for($i=0; $i < count($sucursales); $i++){ $sucursal=$sucursales[$i];
																				$cantidad_pedida=0;
																				$idsdp=null;
																				foreach($j_sucursales as $key => $suc){
																					if($suc->idsc==$sucursal->idsc){ $cantidad_pedida=$suc->cantidad; $idsdp=$suc->idsdp; break;}
																				}
																				if($cantidad_pedida>0 && $idsdp!=null){
																					/*productos asignados*/
																					$cantidad_produccion=0;
																					$cantidad_terminada=0;
																					$asignados=$this->lib->select_from($productos_pedido_empleado,"idsdp",$idsdp);
																					if(count($asignados)>0){
																						$cantidad_terminada=99999999999;
																						foreach($procesos as $key => $proceso){
																							$cantidad_produccion_proceso=0;
																							$cantidad_terminada_proceso=0;
																							for($y=0; $y<count($asignados); $y++){$ppe=$asignados[$y];
																								$cantidad=$this->lib->desencriptar_num($ppe->cantidad)*1;
																								if($ppe->idpr==$proceso->idpr){
																									if($ppe->fecha_fin!="" && $ppe->fecha_fin!=null){
																										$cantidad_terminada_proceso+=$this->lib->desencriptar_num($ppe->cantidad)*1;
																									}else{
																										$cantidad_produccion_proceso+=$this->lib->desencriptar_num($ppe->cantidad)*1;
																									}
																								}
																							}
																							if($cantidad_produccion<$cantidad_produccion_proceso){$cantidad_produccion=$cantidad_produccion_proceso;}
																							if($cantidad_terminada>$cantidad_terminada_proceso){$cantidad_terminada=$cantidad_terminada_proceso;}
																						}
																					}
																					?>

																					<div class="panel panel-info">
																						<div class="panel-heading bg-info"><?php echo $sucursal->nombre;?></div>
																						<div class="panel-body" style="padding: 0px;">
																							<div class="card-group text-xs-center">
																								<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion" data-type="cantidad" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
																									<div class="card-block">
																										<h2><?php echo $cantidad_pedida;?></h2>
																										<p class="card-text">Cantidad total</p>
																									</div>
																								</div>
																								<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion" data-type="pendiente" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
																									<div class="card-block">
																										<h2><?php echo $cantidad_pedida-$cantidad_produccion-$cantidad_terminada; ?></h2>
																										<p class="card-text">Pendiente</p>
																									</div>
																								</div>
																								<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion" data-type="produccion" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
																									<div class="card-block">
																										<h2><?php echo $cantidad_produccion; ?></h2>
																										<p class="card-text">En producción</p>
																									</div>
																								</div>
																								<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion" data-type="terminado" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
																									<div class="card-block">
																										<h2><?php echo $cantidad_terminada;?></h2>
																										<p class="card-text">Terminado</p>
																									</div>
																								</div>
																							</div>
																						</div>
																						<div class="panel-footer txt-info">
																							<div class="row text-right" style="padding-right:10px;">
																								<button type="button" class="btn btn-inverse-info waves-effect btn-mini asignar_empleado_sucursal" data-tbl-color="<?php echo $idtbc;?>" data-tbl-producto="<?php echo $idtbl;?>" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
																									<i class="fa fa-cog"></i><span class="m-l-10">Configurar</span>
																								</button>
																							</div>
																						</div>
																					</div>
																			<br/><?php
																				}//end if
																			}// end for ?>
																		</div>
																		</div>
																	</div>
																	<?php }// end foreach?>
																</div>
															</div>
														</div>
													</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<script>$('[data-toggle="popover"]').popover({html:true});$("button.asignar_empleado_sucursal").asignar_empleado_sucursal();$("div.detalle_produccion").detalle_produccion();$(".config_produccion<?php echo $rand;?>").config_produccion();
								$("button.refresh_producto_pedido").refresh_producto_pedido();$("button.refresh_producto_color_pedido").refresh_producto_color_pedido();
								</script>