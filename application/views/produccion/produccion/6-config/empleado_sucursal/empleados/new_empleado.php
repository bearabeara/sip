<?php
	$url=base_url().'libraries/img/';
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<div class="table-responsive g-table-responsive" id="datos-empleado-proceso" data-pr="<?php echo $proceso->idpr;?>" data-pgc="<?php echo $idpgrc;?>"  data-tbl="">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%" class="img-thumbnail-45">#Item</th>
				<th width="60%">Nombre</th>
				<th width="30%">Calidad</th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($empleados)){
				for($i=0; $i < count($empleados) ;$i++){ $empleado=$empleados[$i];
					$asignado=false;
					$elemento=$this->lib->search_elemento($producto_empleados,"idpre",$empleado->idpre);
					if($elemento!=null){ $asignado=true; }
						$img="sistema/miniatura/default.jpg";
						if($empleado->fotografia!=null && $empleado->fotografia!=""){ $img="personas/miniatura/".$empleado->fotografia;}
			?>
					<tr>
						<td>
							<div class="img-thumbnail-45"></div>
							<div class="item"><?php echo $i+1;?></div>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="<?php echo $proceso->nombre.' - '.$t[$empleado->tipo*1];?>">
						</td>
						<td><?php echo $empleado->nombre_completo."(".$t[$empleado->tipo*1].")";?></td>
						<td>
						<?php if(!$asignado){?>
							<?php $id_rango=$empleado->idpre; ?>
							<div class="rangos" id="rangos_empleado<?php echo $id_rango;?>" style="width: 225px;">
								<input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
							</div>
						<?php }else{?>
								<div class="rangos" style="width: 225px;">
								<?php for ($r=0; $r < $elemento->calidad ; $r++) { ?>
									<label class="rango fa fa-star" style="color: #1b8bf9; cursor: default; float: left;"></label>
								<?php }?>
								</div>
						<?php }?>
						</td>
						<td>
							<button type="button" class="btn btn-<?php if($asignado){ echo 'default';}else{ echo 'inverse-primary add_empleado_proceso';}?> btn-mini waves-effect waves-light"<?php if(!$asignado){?> data-pre="<?php echo $empleado->idpre;?>" data-type="produccion" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }else{ ?> disabled="disabled" <?php }?>> <i class="fa fa-plus"></i> add </button>
						</td>
					</tr>
					<?php 
					}// end for
				}else{?>
				<tr>
					<td colspan="6" class="text-center"><h5>0 empleados registrados con el proceso <strong><?php echo $proceso->nombre;?></strong> para el producto</h5></td>
				</tr>
				<?php }?>
		</tbody>
	</table>
</div>
<script>
	$("button.add_empleado_proceso").add_empleado_proceso();
	$("img.g-img-thumbnail").visor();
</script>