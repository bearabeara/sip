<?php
	$url=base_url().'libraries/img/';
	$cantidad=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1;
	$codigo=$producto->codigo;
	$nombre=$producto->nombre;
	if($producto_grupo_color->abr_g!="" && $producto_grupo_color->abr_g!=NULL){ $codigo.="-".$producto_grupo_color->abr_g; $nombre.=" - ".$producto_grupo_color->nombre_g;}
	if($producto_grupo_color->abr_c!="" && $producto_grupo_color->abr_c!=NULL){ $codigo.="-".$producto_grupo_color->abr_c; $nombre.=" - ".$producto_grupo_color->nombre_c;}
	$img="sistema/miniatura/default.jpg";
	if($producto_grupo_color->portada!="" && $producto_grupo_color->portada!=null){
		$v=explode("|", $producto_grupo_color->portada);
		if(count($v==2)){
			if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
		}
	}else{
		if($producto->portada!="" && $producto->portada!=null){
			$v=explode("|", $producto->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			}
		}
	}
?>
<div class="list-group-item" style="max-width:100%">
	<table class="table table-bordered">
		<tr>
			<td class="g-thumbnail">
				<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $nombre;?>" data-desc="<?php echo 'Cantidad pedido: '.$cantidad.' u.';?>" width="100%">
			</td>
			<td><strong>Código</strong><p class="lead m-t-0"><?php echo $codigo; ?></td>
			<td><strong>Nombre</strong><p class="lead m-t-0"><?php echo $nombre; ?></td>
			<td><strong>Sucursal</strong><p class="lead m-t-0"><?php echo $sucursal->nombre_sucursal; ?></p></td>
			<td><strong>Cantidad</strong><p class="lead m-t-0"><?php echo $cantidad." u."; ?></p></td>
		</tr>
	</table>
</div>
<div class="list-group-item" style="max-width:100%">
	<div class="table-responsive g-table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="">#</th>
					<th width="16%">Proceso</th>
					<th width="5%">Cant. total</th>
					<!--<th width="79%">Empleados encargados de producción</th>-->
				<?php if($type=="pendiente"){?>
					<th width="5%">Cant. Pendiente</th>
				<?php }?>
				<?php if($type=="produccion" || $type=="terminado"){?>
					<th width="50%">Empleados</th>
				<?php }?>
					<th width="79%">Estado de producción</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($procesos)){
					$cont=1;
					foreach($procesos as $key => $proceso){
						$cantidad_terminada=0;
						$cantidad_asignada=0;
							for($i=0; $i<count($productos_pedidos_empleados); $i++){ $producto_pedido_empleado=$productos_pedidos_empleados[$i];
								$estado="0";
								if(($producto_pedido_empleado->fecha_inicio!="" && $producto_pedido_empleado->fecha_inicio!=NULL) && ($producto_pedido_empleado->fecha_fin!="" && $producto_pedido_empleado->fecha_fin!=NULL)){
									$estado="1";
								}
								if($producto_pedido_empleado->idpr==$proceso->idpr){
									if($estado=="1"){
										$cantidad_terminada+=$this->lib->desencriptar_num($producto_pedido_empleado->cantidad)*1;
									}
									$cantidad_asignada+=$this->lib->desencriptar_num($producto_pedido_empleado->cantidad)*1;
								}
							}
				?>
						<tr>
							<td><?php echo $cont++;?></td>
							<td><?php echo $proceso->nombre;?></td>
							<td><?php echo $cantidad." u.";?></td>
							<?php if($type=="pendiente"){?>
							<td><?php echo $cantidad-$cantidad_asignada.' u.'; ?></td>
							<?php }?>
							<?php if($type=="produccion" || $type=="terminado"){?>
							<td class="cards">
								<div class="flex-container">
							<?php for($i=0; $i<count($productos_pedidos_empleados); $i++){ $producto_pedido_empleado=$productos_pedidos_empleados[$i];
									
									$control=false;
									if($type=="produccion"){
										if($producto_pedido_empleado->fecha_fin=="" || $producto_pedido_empleado->fecha_fin==NULL){
											$control=true;
										}
									}
									if($type=="terminado"){
										if($producto_pedido_empleado->fecha_fin!="" && $producto_pedido_empleado->fecha_fin!=NULL){
											$control=true;
										}
									}
									$estado="Asignado";$class="bg-danger";
									if($control){
										if($producto_pedido_empleado->fecha_inicio!="" && $producto_pedido_empleado->fecha_inicio!=NULL){
											if($producto_pedido_empleado->fecha_fin=="" && $producto_pedido_empleado->fecha_fin==NULL){
												$estado="producción";
												$class="bg-info";
											}else{
												$estado="Entregado";
												$class="bg-success";
											}
										}
									}
									$empleado=$this->lib->search_elemento($productos_procesos_empleados,'ide',$producto_pedido_empleado->ide);
									if($producto_pedido_empleado->idpr==$proceso->idpr && $empleado!=NULL && $control){
										$cantidad_empleado=$this->lib->desencriptar_num($producto_pedido_empleado->cantidad)*1;
										$img="sistema/miniatura/default.jpg";
										if($empleado->fotografia!="" && $empleado->fotografia!=null){ $img="personas/miniatura/".$empleado->fotografia;}
							?>
									<div class="flex-element flex-150">
										<div class="card-group">
											<div class="card img-thumbnail-45">
												<div class="label-main" style="float: left;"><label class="label <?php echo $class;?>" style="position: absolute;padding:2px;font-size: 51%;opacity: .9;bottom: 0px;margin-bottom: 0px;"><?php echo $estado;?></label></div>
												<img class="card-img-top" src="<?php echo $url.$img;?>" alt="image" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="<?php echo $proceso->nombre.', cantidad: '.$cantidad_empleado.' u.';?>" style="vertical-align: top;cursor: zoom-in;">
											</div>
											<div class="card card-padding-0">
													<div class="card-block">
														<p class="card-text">
															<small><?php echo $empleado->nombre." ".$empleado->paterno; ?></small>
															<h6><?php echo $cantidad_empleado; ?> u.</h6>
														</p>
													</div>
											</div>
										</div>
									</div>
							<?php }
								}// end for 
							?>
								</div>
							</td>
							<?php }?>
							<td>
							<?php 
								$porcentaje=0;
								if($cantidad>0){
									$porcentaje=$cantidad_terminada/$cantidad*100;
								}
								$porcentaje=number_format($porcentaje,1,'.','');
							?>
								<div class="progress" style="height: 20px; width: 100%">
							    	<div class="progress-bar progress-bar-striped <?php if($porcentaje<100){?>progress-bar-animated<?php }?> bg-info" role="progressbar" style="width: <?php echo $porcentaje;?>%"><strong><?php if($porcentaje>15){ echo $porcentaje."%";} ?></strong></div>
							   		<?php if($porcentaje<=15){?><strong style="margin-left: 5px;"><?php echo $porcentaje."%";?></strong><?php } ?>
							  	</div>
							</td>
						</tr>
						<?php
				}// end for
			}// end if
			?>
		</tbody>
	</table>
</div>
</div>
<script>$("img.card-img-top").visor();</script>