<?php
	$url=base_url().'libraries/img/';
	$total=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1;
	$cantidad_actual=$this->lib->desencriptar_num($producto_pedido_empleado->cantidad)*1;
	$cantidad_terminada=$this->lib->desencriptar_num($producto_pedido_empleado->terminado)*1;
	$limite=$total-($asignado-$cantidad_actual);
	if($limite>=0 && $limite<=$total){
		$help1='title="Cantidad" data-content="Ingrese la nueva cantidad a producir, los valores adminitidos deben ser entre rango de 1 a '.$limite.'."';
		$popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';
		$img="sistema/miniatura/default.jpg";
		if($empleado->fotografia!="" && $empleado->fotografia!=NULL){ $img="personas/miniatura/".$empleado->fotografia;}
		$estado="Asignado";$class="bg-danger";$status="0";
		if(($producto_pedido_empleado->fecha_inicio!="" && $producto_pedido_empleado->fecha_inicio!=NULL) || ($producto_pedido_empleado->fecha_fin!="" && $producto_pedido_empleado->fecha_fin!=NULL)){
			if($producto_pedido_empleado->fecha_fin=="" && $producto_pedido_empleado->fecha_fin==NULL){
				$estado="En producción";
				$class="bg-info";
				$status="2";
			}else{
				$estado="Entregado";
				$class="bg-success";
				$status="3";
			}
		}
?>
<div class="list-group-item" style="max-width:100%" id="datos-producto-empleado" data-p="<?php echo $producto->idp;?>" data-sdp="<?php echo $sucursal_detalle_pedido->idsdp;?>" data-ppe="<?php echo $producto_pedido_empleado->idppe;?>">
	<table class="table table-bordered" style="margin-bottom: 0px;">
		<tr>
			<td class="g-thumbnail" style="width: 60px;">
				<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail g-img-thumbnail-60" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="<br>" width="100%">
			</td>
			<td><strong>Nombre</strong><p class="lead m-t-0"><?php echo $empleado->nombre_completo; ?></td>
		</tr>
	</table>
</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Asignado:</label>
				<div class="col-sm-9 col-xs-12">
					<?php if($status!=3){?>
					<form class="update_producto_pedido_empleado">
					<?php }?>
						<div class="input-group">
							<input class='form-control form-control-xs color-class' type="number" id="can-prod-3" placeholder='0' min="1" max="<?php echo $limite;?>" data-max="<?php echo $limite;?>" value="<?php echo $cantidad_actual;?>" onkeyup="pendiente($(this));" oninput="pendiente($(this));" onwheel="pendiente($(this));">
							<span class="input-group-addon form-control-sm">u.</span>
							<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					<?php if($status!=3){?>
					</form>
					<?php }?>
				</div><i class="clearfix"></i>
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Terminado:</label>
				<div class="col-sm-9 col-xs-12">
					<?php if($status!=3){?>
					<form class="update_producto_pedido_empleado">
					<?php }?>
						<div class="input-group">
							<input class='form-control form-control-xs color-class' type="number" id="ter-prod-3" placeholder='0' min="0" max="<?php echo $limite;?>" data-max="<?php echo $limite;?>" value="<?php echo $cantidad_terminada;?>" onkeyup="pendiente($(this));" oninput="pendiente($(this));" onwheel="pendiente($(this));">
							<span class="input-group-addon form-control-sm">u.</span>
							<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					<?php if($status!=3){?>
					</form>
					<?php }?>
				</div><i class="clearfix"></i>
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Pendiente:</label>
				<div class="col-sm-9 col-xs-12">
					<div class="input-group">
						<input class='form-control form-control-xs color-class' type="number" id="pen-prod-3" placeholder='0' min="1" value="<?php echo $cantidad_actual-$cantidad_terminada;?>" disabled>
						<span class="input-group-addon form-control-sm">u.</span>
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div><i class="clearfix"></i>
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Inicio:</label>
				<div class="col-sm-9 col-xs-12">
					<div class="input-group">
						<input type="datetime-local" id="fini-prod-3" value="<?php if($producto_pedido_empleado->fecha_inicio!='' && $producto_pedido_empleado->fecha_inicio!=NULL){ echo str_replace(" ", "T", $producto_pedido_empleado->fecha_inicio);}?>" class="form-control form-control-xs">
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div><i class="clearfix"></i>
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Entrega:</label>
				<div class="col-sm-9 col-xs-12">
					<div class="input-group">
						<input type="datetime-local" id="ffin-prod-3" value="<?php if($producto_pedido_empleado->fecha_fin!='' && $producto_pedido_empleado->fecha_fin!=NULL){ echo str_replace(" ", "T", $producto_pedido_empleado->fecha_fin);}?>" class="form-control form-control-xs">
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div><i class="clearfix"></i>
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Observaciónes:</label>
				<div class="col-sm-9 col-xs-12">
					<div class="input-group">
						<textarea id="obs-prod-3" rows="3" class="form-control form-control-xs" placeholder="Observaciónes"><?php echo $producto_pedido_empleado->observacion;?></textarea>
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div><i class="clearfix"></i>
				<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Estado:</label>
				<div class="col-sm-9 col-xs-12">
					<span class="label <?php echo $class;?> label-md"><?php echo $estado;?></span>
				</div><i class="clearfix"></i>
			</div>
		<?php /*if(($producto_pedido_empleado->fecha_inicio!="" && $producto_pedido_empleado->fecha_inicio!=NULL) || ($producto_pedido_empleado->fecha_fin!="" && $producto_pedido_empleado->fecha_fin!=NULL)){
				if($producto_pedido_empleado->fecha_fin=="" && $producto_pedido_empleado->fecha_fin==NULL){
					$text="Alerta, el producto se encuentra en producción en <strong>".$proceso->nombre."</strong>, ¿esta seguro en modificar la cantidad?. ";
				}else{
					$text="Alerta, el producto ya fue terminado en el proceso <strong>".$proceso->nombre."</strong>, ¿esta seguro en modificar la cantidad?. ";
				}
		?>
				<div class="col-xs-12"><small class="text-danger"><?php echo $text;?></small></div>
		<?php } */?>
		</div>
	</div>
	<script>$('[data-toggle="popover"]').popover({html:true});Onfocus("ter-prod-3");
	<?php if($status==3){ ?>
		//clear_datas("btn_32");/*Eliminando datos de boton*/$("#btn_32").removeAttr("onclick");$("#btn_32").css("display","none");
	<?php } ?>
	<?php if($status!=3){?>$("form.update_producto_pedido_empleado").submit(function(e){$(this).update_producto_pedido_empleado(); e.preventDefault();});<?php }?>
	</script>
<?php
	}
?>