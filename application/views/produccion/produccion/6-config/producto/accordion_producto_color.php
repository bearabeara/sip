<?php /*procesos del color*/
//$procesos=$this->lib->producto_grupo_color_procesos($grupo,$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso);
//$procesos=json_decode(json_encode($procesos));
/*sucursales*/
//$j_sucursales=json_decode(json_encode($grupo->sucursales));
//var_dump($color_sucursales);
$id_rand=rand(10,999999);
for($i=0; $i < count($color_sucursales); $i++){ $color_sucursal=$color_sucursales[$i];
	$datos_sucursal=$this->lib->search_elemento($sucursales,'idsc',$color_sucursal->idsc);
	$cantidad_pedida=$this->lib->desencriptar_num($color_sucursal->cantidad)*1;
	if($cantidad_pedida>0 && $datos_sucursal!=null){
		/*productos asignados*/
		$cantidad_produccion=0;
		$cantidad_terminada=0;
		$asignados=$this->lib->select_from($productos_pedido_empleado,"idsdp",$color_sucursal->idsdp);
		if(count($asignados)>0){
			$cantidad_terminada=99999999999;
			foreach($procesos as $key => $proceso){
				$cantidad_produccion_proceso=0;
				$cantidad_terminada_proceso=0;
				for($y=0; $y<count($asignados); $y++){$ppe=$asignados[$y];
					$cantidad=$this->lib->desencriptar_num($ppe->cantidad)*1;
					if($ppe->idpr==$proceso->idpr){
						if($ppe->fecha_fin!="" && $ppe->fecha_fin!=null){
							$cantidad_terminada_proceso+=$this->lib->desencriptar_num($ppe->cantidad)*1;
						}else{
							$cantidad_produccion_proceso+=$this->lib->desencriptar_num($ppe->cantidad)*1;
						}
					}
				}
				if($cantidad_produccion<$cantidad_produccion_proceso){ $cantidad_produccion=$cantidad_produccion_proceso;}
				if($cantidad_terminada>$cantidad_terminada_proceso){ $cantidad_terminada=$cantidad_terminada_proceso;}
			}
		}
?>
		<div class="panel panel-info">
			<div class="panel-heading bg-info"><?php echo $datos_sucursal->nombre;?>				
			</div>
			<div class="panel-body" style="padding: 0px;">
				<div class="card-group text-xs-center">
					<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="cantidad" data-sdp="<?php echo $color_sucursal->idsdp;?>" data-p="<?php echo $producto->idp;?>">
						<div class="card-block">
							<h2><?php echo $cantidad_pedida;?></h2>
							<p class="card-text">Cantidad total</p>
						</div>
					</div>
					<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="pendiente" data-sdp="<?php echo $color_sucursal->idsdp;?>" data-p="<?php echo $producto->idp;?>">
						<div class="card-block">
							<h2><?php echo $cantidad_pedida-$cantidad_produccion-$cantidad_terminada; ?></h2>
							<p class="card-text">Pendiente</p>
						</div>
					</div>
					<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="produccion" data-sdp="<?php echo $color_sucursal->idsdp;?>" data-p="<?php echo $producto->idp;?>">
						<div class="card-block">
							<h2><?php echo $cantidad_produccion; ?></h2>
							<p class="card-text">En producción</p>
						</div>
					</div>
					<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="terminado" data-sdp="<?php echo $color_sucursal->idsdp;?>" data-p="<?php echo $producto->idp;?>">
						<div class="card-block">
							<h2><?php echo $cantidad_terminada;?></h2>
							<p class="card-text">Terminado</p>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer txt-info">
				<div class="row text-right" style="padding-right:10px;">
					<button type="button" class="btn btn-inverse-info waves-effect btn-mini asignar_empleado_sucursal<?php echo $id_rand;?>" data-tbl-color="<?php echo $tbl_color_progress;?>" data-tbl-producto="<?php echo $tbl_producto_progress;?>" data-sdp="<?php echo $color_sucursal->idsdp;?>" data-p="<?php echo $producto->idp;?>">
						<i class="fa fa-cog"></i><span class="m-l-10">Configurar</span>
					</button>
				</div>
			</div>
		</div>
		<!--<br/>-->
		<div><form class="save_porcentaje" data-dp="<?php echo $detalle_pedido->iddp;?>"><input class="form-control form-control-sm" id="up_porcentaje" type="text"></form></div>
		<?php 	
	}//end if
}// end for ?>
<script>$("button.asignar_empleado_sucursal<?php echo $id_rand;?>").asignar_empleado_sucursal();$("div.detalle_produccion<?php echo $id_rand;?>").detalle_produccion();
var atrib=new FormData();atrib.append("dp",'<?php echo $detalle_pedido->iddp;?>');
var controls=JSON.stringify({id:"color-progress-"+'<?php echo $tbl_color_progress;?>',refresh:false,type:"html"});
$(this).get_1n('produccion/progress_producto_color',atrib,controls);
atrib.append("p",'<?php echo $producto->idp;?>');
atrib.append("pp",'<?php echo $parte_pedido->idpp;?>');
var controls=JSON.stringify({id:"accordion-progress-"+'<?php echo $tbl_producto_progress;?>',refresh:false,type:"html"});
$(this).get_1n('produccion/progress_producto',atrib,controls);
	$("form.save_porcentaje").save_porcentaje();
</script>