<?php $j_pedidos=json_encode($pedidos);?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th width="3%">#</th>
			<th width="11%" class="hidden-sm">Fecha de Pedido</th>
			<th width="6%" class="hidden-sm">N° Pedido</th>
			<th width="17%">Nombre de pedido</th>
			<th width="15%" class="hidden-sm">Cliente</th>
			<th width="8%">Estado de producción</th>
			<th width="37%">Progreso de producción</th>
			<th width="3%"></th>
		</tr>
	</thead>
	<tbody>
<?php if(count($pedidos)>0){ ?>
	<?php for($i=0;$i<count($pedidos); $i++) { $pedido=$pedidos[$i]; 
			$control=null;
			$atrib2=$atrib;
			if($atrib=="numero"){$atrib2="idpe";}
			if($atrib=="p.nombre"){$atrib2="nombre";}
			if($atrib=="c.idcl"){$atrib2="idcl";}
			if($atrib=="p.estado"){$atrib2="estado";}
			$control=$this->lib->search_where($pedido,$atrib2,$val,$type_search);
			$detalles=$this->lib->select_from($all_detalles,'idpe',$pedido->idpe);
			$porcentaje=0;
			for($j=0; $j<count($detalles); $j++){$porcentaje+=($detalles[$j]->porcentaje);}
			if(count($detalles)>0){$porcentaje/=count($detalles);$porcentaje=number_format($porcentaje,3,'.','');$porcentaje*=100;}
			if($pedido->estado==2){$porcentaje=100;}
	?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td class="hidden-sm"><?php echo $this->lib->format_date($pedido->fecha,'d ml Y');?></td>
			<td class="hidden-sm" data-col="2"><?php if($atrib=="numero" && $val!="" && $control!=null){ echo $this->lib->str_replace_all($pedido->idpe,$this->lib->all_minuscula($val),"mark");}else{echo $pedido->idpe;}?></td>
			<td data-col="3"><?php if($atrib=="p.nombre" && $val!="" && $control!=null){ echo $this->lib->str_replace_all($pedido->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $pedido->nombre;}?></td>
			<td class="hidden-sm" data-col="4" data-value="<?php echo $pedido->idcl;?>"><?php if($atrib=="c.idcl" && $control!=null){?><mark><?php }?><?php echo $pedido->razon;?><?php if($atrib=="c.idcl" && $control!=null){?></mark><?php }?></td>
			<td>
				<?php if($pedido->estado==0){?><span class="label label-danger label-md">Pendiente</span><?php }?>
				<?php if($pedido->estado==1){?><span class="label label-info label-md">En producción</span><?php }?>
				<?php if($pedido->estado==2){?><span class="label label-success label-md">Entregado</span><?php }?>
				<span data-col="5" data-value="<?php echo $pedido->estado;?>"></span>
			</td>
			<td class="text-right">
<div class="progress" style="height: 20.5px;">
    <div class="progress-bar progress-bar-striped <?php if($porcentaje<100){?>progress-bar-animated bg-primary<?php }else{?>bg-success<?php }?>" style="width: <?php echo $porcentaje;?>%"><strong><?php if($porcentaje>7){ echo $porcentaje."%";} ?></strong></div>
   	<?php if($porcentaje<=7){?><strong style="margin-left: 5px;"><?php echo $porcentaje."%";?></strong><?php } ?>
  </div>
			</td>
			<td class="celda td text-right">
				<?php
					$conf=""; if($privilegio->pr2u=="1"){ $conf=json_encode(array('function'=>'config_produccion','atribs'=> json_encode(array('pe' => $pedido->idpe)),'title'=>'Configurar'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",['config'=>$conf]);?>
			</td>
		</tr>
	<?php } ?>
<?php }else{?>
	<tr><td colspan='11' class="text-center"><h2>0 Registros encontrados...</h2></td></tr>
	<?php } ?>
	</tbody>
</table>
<?php 
	if($privilegio->pr2r=="1" && ($privilegio->pr2p=="1")){
		if($privilegio->pr2p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_help"=>$help]);
	}
?>
<script>$(".config_produccion").config_produccion();</script>