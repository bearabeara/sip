<table class="tabla tabla-border-false">
	<tr>
		<td style="width: 13.8%; text-align: left;" class='hidden-sm'>
			<span class="label label-lg label-inverse-danger" style="cursor: default;"><label class="text-danger">Beta</label></span>
		</td>
		<td class='celda-sm-6 hidden-sm' data-type="search">
			<form class="view_produccion"><input type="number" id="sp_num" placeholder='Numero de pedido' class="form-control form-control-sm search_produccion" data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="2"></form>
		</td>
		<td style="width:17%">
			<form class="view_produccion" data-type="search"><div class="input-group input-group-sm"><input type="search" id="sp_nom" class="form-control form-control-sm search_produccion" placeholder='Buscar nombre...' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="3"/><span class="input-group-addon form-control-sm view_produccion" data-type="search"><i class='icon-search2'></i></span></div></form>
		</td>
		<td class='celda-sm-15 hidden-sm'>
			<select id="sp_cli" class="form-control form-control-sm search_produccion" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="4">
				
			</select>
		</td>
		<td class='hidden-sm' style="width: 12%">
			<select id="sp_est" class="form-control form-control-sm search_produccion" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="5">
				<option value="">Seleccionar...</option>
				<option value="0">Pendiente</option>
				<option value="1">En producción</option>
				<option value="2">Entregado</option>
			</select>
		</td>
		<td class='celda-sm-15 hidden-sm'></td>
		<td class='text-right hidden-sm' style="width:3%">
		<?php
			$search=json_encode(array('function'=>'view_produccion','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
			$all=json_encode(array('function'=>'view_produccion','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>$(".search_produccion").search_produccion();$(".view_produccion").view_produccion();</script>