<?php $url=base_url().'libraries/img/';
  $help='title="Observaciónes" data-content="Ingrese las observaciónes del material en formato alfanumérico hasta 200 caracteres <b>puede incluir espacios, sin saltos de linea</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<?php if($type=="pgcm-modal"){ ?>
	<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<thead>
		<?php if($type=="pgcm-modal"){ ?>
				<tr>
					<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
					<th style="width:40%">Nombre</th>
					<th style="width:20%">Cantidad</th>
					<th></th>
					<th style="width:40%">Observaciónes</th>
					<th width="5%"></th>
				</tr>
		<?php } ?>
			</thead>
			<tbody>
		<?php if(count($color_materiales)>0){
				for($i=0; $i < count($color_materiales); $i++){ $material=$color_materiales[$i];
					$img="sistema/miniatura/default.jpg";
				    if($material->fotografia!="" && $material->fotografia!=NULL){
						$img="materiales/miniatura/".$material->fotografia;
					}
		?>
				<tr>
					<td><div id="item"><?php echo $i+1;?></div>
					 	<img src="<?php echo $url.$img;?>" class='img-thumbnail g-img-thumbnail' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
					</td>
					<td><?php echo $material->nombre;?><br>
						<span style="font-weight:bold; color: <?php echo $material->codigo_c;?>">(<?php echo $material->nombre_c;?>)</span>
					</td>
						<td>
							<div class="input-group input-120">
								<form class="update_producto_material" data-pgcm="<?php echo $material->idpgcm;?>" data-refresh="none">
									<input type="number" id="can_mat<?php echo $material->idpgcm;?>" class="form-control form-control-sm input-90" placeholder="0,0" min="0" step="any" max="999999.9999" value="<?php echo $material->cantidad;?>">
								</form>
								<span class="input-group-addon unid form-control-sm"><?php echo $material->abr_u.".";?></span>
							</div>
						</td>
						<td>
							<div class="checkbox-fade fade-in-success" style="margin: 2px 0px 0px 0px;">
							    <label>
							        <input type="checkbox" value="<?php echo $material->verificado;?>" id="ver_mat<?php echo $material->idpgcm;?>" <?php if($material->verificado==1){?>checked<?php }?>>
							            <span class="cr" style="width: 1.4em;"><i class="cr-icon fa fa-check txt-success"></i></span>
							    </label>
							</div>
						</td>
						<td>
							<div class="input-group">
								<textarea class="form-control input-sm" id="obs_mat<?php echo $material->idpgcm;?>" placeholder="Observaciónes del material en el producto" rows="3" maxlength="200"><?php echo $material->observacion;?></textarea>
								<span class="input-group-addon help form-control-sm btn-default" <?php echo $popover.$help;?>><i class="fa fa-info-circle"></i></span>
							</div>
						</td>
					<td>
					<div class="g-control-accordion" style="width:90px;">
			      		<button class="update_producto_material" data-pgcm="<?php echo $material->idpgcm;?>" data-refresh="none"><i class="icon-floppy-o"></i></button>
			      		<button class="material-change" data-pgcm="<?php echo $material->idpgcm;?>" data-type="pgcm-modal-change"><i class="fa fa-exchange"></i></button>
		      			<button class="confirmar_material" data-pgcm="<?php echo $material->idpgcm;?>" data-type="<?php echo $type;?>"><i class="icon-trashcan2"></i></button>
					</div>
				</tr>	
		<?php
				}
			}?>
			</tbody>
		</table>
	</div>
<?php }?>
<?php if($type=="pgm-modal" || $type=="pgcm-modal"){ ?>
	<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<thead>
		<?php if($type=="pgcm-modal"){ ?>
				<tr><th colspan="6" class="text-center">MATERIALES GENERALES DE LA CATEGORIA</th></tr>
		<?php } ?>
		<?php if($type=="pgm-modal"){ ?>
				<tr>
					<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
					<th style="width:40%">Nombre</th>
					<th style="width:20%">Cantidad</th>
					<th></th>
					<th style="width:34%">Observaciónes</th>
					<th width="5%"></th>
				</tr>
		<?php } ?>
			</thead>
			<tbody>
		<?php if(count($grupo_materiales)>0){
				for($i=0; $i < count($grupo_materiales); $i++){ $material=$grupo_materiales[$i];
					$img="sistema/miniatura/default.jpg";
				    if($material->fotografia!="" && $material->fotografia!=NULL){
						$img="materiales/miniatura/".$material->fotografia;
					}
		?>
				<tr>
					<td class="g-thumbnail-modal"><div id="item"><?php echo $i+1;?></div><div class="g-img-modal"></div>
					 	<img src="<?php echo $url.$img;?>" class='img-thumbnail g-img-thumbnail' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
					</td>
					<td style="width:40%"><?php echo $material->nombre;?><br>
						<span style="font-weight:bold; color: <?php echo $material->codigo_c;?>">(<?php echo $material->nombre_c;?>)</span>
					</td>
						<td style="width:20%">
							<?php if($type!="pgcm-modal"){ ?>
								<div class="input-group input-120">
									<form class="update_producto_material" data-pgm="<?php echo $material->idpgm;?>" data-refresh="none">
										<input type="number" id="can_mat<?php echo $material->idpgm;?>" class="form-control form-control-sm input-90" placeholder="0,0" min="0" step="any" max="999999.9999" value="<?php echo $material->cantidad;?>">
									</form>
									<span class="input-group-addon unid form-control-sm"><?php echo $material->abr_u.".";?></span>
								</div>
							<?php }else{
									echo $material->cantidad." [".$material->abr_u."]"; 
								} ?>
						</td>
						<td>
							<div class="checkbox-fade fade-in-success" style="margin: 2px 0px 0px 0px;">
							    <label>
							        <input type="checkbox" value="<?php echo $material->verificado;?>" id="ver_mat<?php echo $material->idpgm;?>" <?php if($material->verificado==1){?>checked<?php }?> <?php if($type=="pgcm-modal"){ ?>disabled<?php }?>>
							            <span class="cr" style="width: 1.4em;"><i class="cr-icon fa fa-check txt-success"></i></span>
							    </label>
							</div>
						</td>
							<td style="width:40%">
							<?php if($type!="pgcm-modal"){ ?>
								<div class="input-group">
									<textarea class="form-control input-sm" id="obs_mat<?php echo $material->idpgm;?>" placeholder="Observaciónes del material en el producto" rows="3" maxlength="200"><?php echo $material->observacion;?></textarea>
									<span class="input-group-addon help form-control-sm btn-default" <?php echo $popover.$help;?>><i class="fa fa-info-circle"></i></span>
								</div>
							<?php }else{ echo $material->observacion; } ?>
							</td>
					<td width="5%">
					<div class="g-control-accordion" style="width:90px;">
					<?php if($type!="pgcm-modal"){ ?>
			      		<button class="update_producto_material" data-pgm="<?php echo $material->idpgm;?>" data-refresh="none"><i class="icon-floppy-o"></i></button>
			      		<button class="material-change" data-pgm="<?php echo $material->idpgm;?>" data-type="pgm-modal-change"><i class="fa fa-exchange"></i></button>
		      			<button class="confirmar_material" data-pgm="<?php echo $material->idpgm;?>" data-type="<?php echo $type;?>"><i class="icon-trashcan2"></i></button>
		      		<?php } ?>
					</div>
				</tr>	
		<?php
				}
			}?>
			</tbody>
		</table>
	</div>
<?php }?>

<div class="table-responsive">
	<table class="table table-bordered table-striped">
		<thead>
	<?php if($type=="pgm-modal" || $type=="pgcm-modal"){ ?>
			<tr><th colspan="6" class="text-center">MATERIALES GENERALES DEL PRODUCTO</th></tr>
	<?php } ?>
	<?php if($type=="pm-modal"){ ?>
			<tr>
				<th class="g-thumbnail-modal"><div class="g-img-modal">#Item</div></th>
				<th style="width:40%">Nombre</th>
				<th style="width:20%">Cantidad</th>
				<th></th>
				<th style="width:40%">Observaciónes</th>
				<th width="5%"></th>
			</tr>
	<?php } ?>
		</thead>
		<tbody>
	<?php for($i=0; $i < count($materiales); $i++){ $material=$materiales[$i]; 
			$img="sistema/miniatura/default.jpg";
				if($material->fotografia!="" && $material->fotografia!=NULL){
				$img="materiales/miniatura/".$material->fotografia;
			}
	?>
			<tr>
				<td class="g-thumbnail-modal">
				<div class="g-img-modal"></div>
				<div id="item"><?php echo $i+1;?></div>
				 	<img src="<?php echo $url.$img;?>" class='img-thumbnail g-img-thumbnail' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
				</td>
				<td style="width:40%"><?php echo $material->nombre;?><br>
					<span style="font-weight:bold; color: <?php echo $material->codigo_c;?>">(<?php echo $material->nombre_c;?>)</span>
				</td>
						<td style="width:20%">
						<?php if($type!="pgm-modal" &&  $type!="pgcm-modal"){ ?>
							<div class="input-group input-120">
								<form class="update_producto_material" data-pm="<?php echo $material->idpm;?>" data-refresh="none">
									<input type="number" id="can_mat<?php echo $material->idpm;?>" class="form-control form-control-sm input-90" placeholder="0,0" min="0" step="any" max="999999.9999" value="<?php echo $material->cantidad;?>">
								</form>
								<span class="input-group-addon unid form-control-sm"><?php echo $material->abr_u.".";?></span>
							</div>
						<?php }else{ echo $material->cantidad." [".$material->abr_u."]"; }?>
						</td>
						<td>
							<div class="checkbox-fade fade-in-success" style="margin: 2px 0px 0px 0px;">
							    <label>
							        <input type="checkbox" value="<?php echo $material->verificado;?>" id="ver_mat<?php echo $material->idpm;?>" <?php if($material->verificado==1){?>checked<?php }?> <?php if($type=="pgm-modal" ||  $type=="pgcm-modal"){ ?>disabled<?php }?>>
							            <span class="cr" style="width: 1.4em;"><i class="cr-icon fa fa-check txt-success"></i></span>
							    </label>
							</div>
						</td>
						<td style="width:40%">
						<?php if($type!="pgm-modal" &&  $type!="pgcm-modal"){ ?>
							<div class="input-group">
								<textarea class="form-control input-sm" id="obs_mat<?php echo $material->idpm;?>" placeholder="Observaciónes del material en el producto" rows="3" maxlength="200"><?php echo $material->observacion;?></textarea>
								<span class="input-group-addon help form-control-sm btn-default" <?php echo $popover.$help;?>><i class="fa fa-info-circle"></i></span>
							</div>
						<?php }else{ echo $material->observacion; }?>
						</td>
				<td width="5%">
				<div class="g-control-accordion" style="width:90px;">
				<?php if($type!="pgm-modal" &&  $type!="pgcm-modal"){ ?>
		      		<button class="update_producto_material" data-pm="<?php echo $material->idpm;?>" data-refresh="none"><i class="icon-floppy-o"></i></button>
		      		<button class="material-change" data-pm="<?php echo $material->idpm;?>" data-type="pm-modal-change"><i class="fa fa-exchange"></i></button>
	      			<button class="confirmar_material" data-pm="<?php echo $material->idpm;?>" data-type="<?php echo $type;?>"><i class="icon-trashcan2"></i></button>
	      		<?php }?>
				</div>
			</tr>		
	<?php }?>
		</tbody>
	</table>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$(".update_producto_material").update_producto_material();$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});$("button.material-change").click(function(){$(this).material();});$("button.confirmar_material").confirmar_material();</script>