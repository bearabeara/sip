<?php 
  $help1='title="Portada" data-content="Seleccionar como portada del <strong>producto.</strong>"';
  $help2='title="Portada" data-content="Seleccionar como portada del <strong>color del producto.</strong>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/productos/miniatura/';
	$url2=base_url().'libraries/img/productos/';
?>
    <div class="row">
      <div class="col-xs-12">
        <span class="g-control-accordion">
          <button class="producto_fotografias" data-type="<?php echo $type;?>" <?php if(isset($pgc)){?>data-pgc="<?php echo $pgc;?>"<?php }?>><i class="fa fa-refresh"></i></button>
          <button class="<?php if($type=='pf-modal'){ ?>producto_seg_fot<?php }else{?>producto_grupo_color_seg_fot<?php } ?>" data-p="<?php echo $producto->idp;?>" data-type="<?php echo $type;?>"  <?php if(isset($pgc)){?>data-pgc="<?php echo $pgc;?>"<?php }?>><i class="fa fa-clock-o"></i></button>
        </span>
      </div>
    </div>
<?php if(isset($pgc)){ $idpgrc=$pgc;}?>
<?php if($type=="pgcf-modal"){ ?>
<div class="table-responsive">
	<table class="table table-bordered table-striped" id="datos-pgc" <?php if(isset($idpgrc)){ ?>data-pgc="<?php echo $idpgrc;?>"<?php }?>>
		<thead>
			<tr><th colspan="6" class="text-center">FOTOGRAFIAS EN EL COLOR DEL PRODUCTO</th></tr>
			<tr>
				<th class="g-thumbnail-modal"><div class="g-img-modal"></div></th>
				<th></th>
				<th width="100%">Descripción</th>
				<th></th>
				<th><span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span></th>
				<th><span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help2;?>><i class="fa fa-info-circle"></i></span></th>
			</tr>
		</thead>
		<tbody>
<?php for ($i=0; $i < count($fotografias_color) ; $i++) { $fotografia=$fotografias_color[$i]; ?>
			<tr>
				<td class="item-relative"><div class="g-thumbnail-modal"></div><div id="item"><?php echo $i+1;?></div>
					<?php $desc="<br>"; if($fotografia->descripcion!="" && $fotografia->descripcion!=null){ $desc=$fotografia->descripcion;}?>
					<img src="<?php echo $url.$fotografia->archivo;?>" id="img<?php echo $fotografia->idpig;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo $desc;?>" data-pig="<?php echo $fotografia->idpig;?>" width="100%">
				</td>
				<td style="padding: 0px;" width="60px">
					<div class="g-control-accordion" style="width:60px;">
						<button class="rotate_fotografia" data-pig="<?php echo $fotografia->idpig;?>" data-type="<?php echo $type;?>" data-rotate="90" data-pgc="<?php echo $fotografia->idpgrc?>"><i class="fa fa-rotate-left"></i>&nbsp 90°</button>
					</div>
					<div class="g-control-accordion" style="width:60px;">
						<button class="rotate_fotografia" data-pig="<?php echo $fotografia->idpig;?>" data-type="<?php echo $type;?>" data-rotate="-90" data-pgc="<?php echo $fotografia->idpgrc?>"><i class="fa fa-rotate-right"></i> -90°</button>
					</div>
				</td>
				<td width="100%"><?php echo $fotografia->descripcion;?></td>
				<td style="padding: 0px;" width="60px">
					<div class="g-control-accordion" style="width:60px;">
						<button class="config_fotografia" data-pig="<?php echo $fotografia->idpig;?>" data-pgc="<?php echo $fotografia->idpgrc?>" data-type="<?php echo $type;?>"><i class="fa fa-cog"></i></button>
						<button class="confirmar_fotografia" data-pig="<?php echo $fotografia->idpig;?>" data-type="<?php echo $type;?>" data-pgc="<?php echo $fotografia->idpgrc;?>"><i class="icon-trashcan2"></i></button>
					</div>
				</td>
				<td>
				<?php 
				$es_portada=false;
				if($producto->portada!=NULL){ $v=explode("|", $producto->portada); if(count($v)==2){ if($v[0]=="3"){ if($fotografia->idpig==$v[1]){ $es_portada=true;} }}}
				if($es_portada){ ?>
					<div class="btn-circle btn-circle-red btn-circle-30 btn-portada" data-type-save="producto" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $fotografia->idpgrc;?>" data-type="<?php echo $type;?>" data-status="<?php echo '3|'.$fotografia->idpig;?>"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 btn-portada" data-type-save="producto" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $fotografia->idpgrc;?>" data-type="<?php echo $type;?>" data-status="<?php echo '3|'.$fotografia->idpig;?>"></div>
				<?php }?>
				</td>
				<td>
				<?php 
				$es_portada=false;
				$producto_grupo_color=$this->M_producto_grupo_color->get($fotografia->idpgrc);
				$producto_grupo_color=$producto_grupo_color[0];
				if($producto_grupo_color->portada!=NULL && $producto_grupo_color->portada!=""){  $v=explode("|", $producto_grupo_color->portada); if(count($v)==2){ if($v[0]=="3"){ if($fotografia->idpig==$v[1]){ $es_portada=true;} }}}
				if($es_portada){ ?>
					<div class="btn-circle btn-circle-info btn-circle-30 btn-portada" data-type-save="color" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $fotografia->idpgrc;?>" data-type="<?php echo $type;?>" data-status="<?php echo '3|'.$fotografia->idpig;?>"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 btn-portada" data-type-save="color" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $fotografia->idpgrc;?>" data-type="<?php echo $type;?>" data-status="<?php echo '3|'.$fotografia->idpig;?>"></div>
				<?php }?>
				</td>
			</tr>
	<?php } ?>
		</tbody>
	</table>
</div>
<div class="row text-right" style="padding-right:15px;">
<?php
	$producto_grupo_color=$this->M_producto_grupo_color->get($pgc);
	$pgc=$this->M_producto_grupo_color->get_row('idpgr',$producto_grupo_color[0]->idpgr);
	if(count($pgc)>1){
?>
  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_fotografias" data-type="<?php echo $type;?>" <?php if(isset($idpgrc)){?>data-pgc="<?php echo $idpgrc;?>"<?php }?>>
        <i class="fa fa-plus"></i><span class="m-l-10">Adicionar fotografía</span>
   </button>
<?php } ?>
</div><br>
<?php } ?>
<div class="table-responsive">
	<table class="table table-bordered table-striped" id="datos-pgc" <?php if(isset($idpgrc)){ ?>data-pgc="<?php echo $idpgrc;?>"<?php }?>>
		<thead>
<?php if($type=="pgcf-modal"){ ?>
			<tr>
				<th colspan="2" class="text-center">FOTOGRAFIAS GENERALES DEL PRODUCTO</th>
				<th></th>
			</tr>
<?php } ?>
<?php if($type=="pf-modal"){ ?>
			<tr>
				<th class="g-thumbnail-modal"><div class="g-img-modal"></div></th>
				<th></th>
				<th width="100%">Descripción</th>
				<th></th>
				<th><span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span></th>
			</tr>
<?php } ?>
		</thead>
		<tbody>
<?php for($i=0; $i < count($fotografias); $i++){ $fotografia=$fotografias[$i] ?>
			<tr>
				<td class="item-relative g-thumbnail-modal"><div class="g-thumbnail-modal"></div><div id="item"><?php echo $i+1;?></div>
					<?php $desc="<br>"; if($fotografia->descripcion!="" && $fotografia->descripcion!=null){ $desc=$fotografia->descripcion;}?>
					<img src="<?php echo $url.$fotografia->archivo;?>" id="img<?php echo $fotografia->idpi;?>" class="img-thumbnail g-img-thumbnail g-thumbnail-modal" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo $desc;?>" data-pi="<?php echo $fotografia->idpi;?>">
				</td>
			<?php if($type=="pf-modal"){ ?>
				<td style="padding: 0px;" width="60px">
					<div class="g-control-accordion" style="width:60px;">
						<button class="rotate_fotografia" data-pi="<?php echo $fotografia->idpi;?>" data-type="<?php echo $type;?>" data-rotate="90"><i class="fa fa-rotate-left"></i>&nbsp 90°</button>
					</div>
					<div class="g-control-accordion" style="width:60px;">
						<button class="rotate_fotografia" data-pi="<?php echo $fotografia->idpi;?>" data-type="<?php echo $type;?>" data-rotate="-90"><i class="fa fa-rotate-right"></i> -90°</button>
					</div>
				</td>
			<?php }?>
				<td width="100%"><?php echo $fotografia->descripcion;?></td>
			<?php if($type=="pf-modal"){ ?>
				<td style="padding: 0px;" width="60px">
					<div class="g-control-accordion" style="width:60px;">
						<button class="config_fotografia" data-pi="<?php echo $fotografia->idpi;?>" data-type="<?php echo $type;?>"><i class="fa fa-cog"></i></button>
						<button class="confirmar_fotografia" data-pi="<?php echo $fotografia->idpi;?>" data-type="<?php echo $type;?>"><i class="icon-trashcan2"></i></button>
					</div>
				</td>
				<td>
				<?php 
				$es_portada=false;
				if($producto->portada!=NULL){ $v=explode("|", $producto->portada); if(count($v)==2){ if($v[0]=="1"){ if($fotografia->idpi==$v[1]){ $es_portada=true;} }}}
				if($es_portada){ ?>
					<div class="btn-circle btn-circle-red btn-circle-30 btn-portada" data-type-save="producto" data-p="<?php echo $producto->idp;?>" data-type="<?php echo $type;?>" data-status="<?php echo '1|'.$fotografia->idpi;?>"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 btn-portada" data-type-save="producto" data-p="<?php echo $producto->idp;?>" data-type="<?php echo $type;?>" data-status="<?php echo '1|'.$fotografia->idpi;?>"></div>
				<?php }?>
				</td>
			<?php } ?>
			<?php if($type=="pgcf-modal"){ ?>
					<td>
						<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light copiar_imagen_color" data-pi="<?php echo $fotografia->idpi;?>" data-pgc="<?php echo $idpgrc;?>" data-type="<?php echo $type;?>" title="Copiar la fotografía en el color del producto.">
							<i class="fa fa-copy"></i> Copy.
						</button>
					</td>
			<?php } ?>
			</tr>
<?php } ?>
		</tbody>
	</table>
</div>
<?php if($type=="pf-modal"){ ?>
<div class="row text-right" style="padding-right:15px;">
  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_fotografias" data-type="<?php echo $type;?>">
  		<i class="fa fa-plus"></i><span class="m-l-10">Adicionar fotografía</span>
   </button>
</div>
<?php } ?>
<script>$('[data-toggle="popover"]').popover({html:true});$("button.producto_fotografias").producto_fotografias();
<?php if($type=='pf-modal'){ ?>$('button.producto_seg_fot').producto_seg();<?php }else{?>$('button.producto_grupo_color_seg_fot').producto_grupo_color_seg();<?php } ?>
$('button.new_fotografias').new_fotografias();$("button.config_fotografia").config_fotografia();$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});
$("button.confirmar_fotografia").confirmar_fotografia();$("div.btn-portada").portada();
$("button.copiar_imagen_color").copiar_imagen_color();
$("button.rotate_fotografia").rotate_fotografia();
</script>