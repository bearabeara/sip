<?php 
	$help1='title="Procesos" data-content="Seleccione eun proceso que realice el empleado"';
	$help2='title="Categoría" data-content="Seleccione la categoría que desempeña en el proceso."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
	<form class="view_proceso">
							<div class="input-group">
								<input id="s3_nom" type="search" class="form-control form-control-xs" placeholder="Nombre de proceso" data-type-save="<?php echo $type_save;?>"<?php if(isset($pg)){?> data-pg="<?php echo $pg;?>"<?php }?><?php if(isset($pgc)){?> data-pgc="<?php echo $pgc;?>"<?php }?><?php if(isset($container)){?> data-container="<?php echo $container;?>"<?php }?>>
								<span class="input-group-addon form-control-sm view_proceso"><i class='fa fa-search'></i></span>
							</div>
						</form>
	<!--	<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="100%">
						
					</td>
					<td class="text-right">
						<div style="width:67px;">
							<div class="btn-group " role="group">
								<?php /*
									$b=json_encode(array("funcion" => "view_proceso","atributos"=>""));
									$v=json_encode(array("funcion" => "new_proceso","atributos"=>json_encode(array("type"=>"new_pro_emp"))));
								?>
								<?php $this->load->view('estructura/botones/buscador2',['id' => '2','f_buscar'=>$b,'f_nuevo'=>$v]);*/?>
							</div>
						</div>
					</td>
				</tr>		
			</tbody>
		</table>-->
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_3"></div>
</div>
<script>Onfocus("s3_nom");$('[data-toggle="popover"]').popover({html:true}); $(".view_proceso").view_proceso();</script>