<?php 
	$help1='title="Procesos" data-content="Seleccione eun proceso que realice el empleado"';
	$help2='title="Categoría" data-content="Seleccione la categoría que desempeña en el proceso."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="2%">#</th>
						<th width="97%">Proceso</th>
					<?php if($type_save=="pre"){?>
						<th>Grado de resposabilidad.</th>
					<?php }?>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			<?php if(count($procesos)>0){ 
				for ($i=0; $i < count($procesos); $i++) { $proceso=$procesos[$i];
					if($type_save!="pre"){
						$control=$this->lib->search_elemento($producto_proceso,"idpr",$proceso->idpr);
					}else{
						$control=$this->lib->search_json($asignados,"pr",$proceso->idpr);
					}
			?>
				<tr>
					<td><?php echo $i+1;?></td>
					<td>
						<form class="update_proceso" data-pr="<?php echo $proceso->idpr;?>">
							<div class="input-group">
								<input type="text" class="form-control form-control-sm color-class" minlength="3" maxlength="100" id="proc<?php echo $proceso->idpr; ?>" value="<?php echo $proceso->nombre;?>">
								<span class="input-group-addon form-control-sm update_proceso" data-pr="<?php echo $proceso->idpr;?>"><i class="fa fa-floppy-o"></i></span>
							</div>

						</form>
					</td>
					<?php if($type_save=="pre"){?>
					<td>
						<select class="form-control form-control-sm input-140" <?php if($control!=null){?> disabled="disabled"<?php }else{?> id="grado<?php echo $proceso->idpr;?>"<?php }?>>
							<option value="">Seleccionar...</option>
						<?php for ($j=0; $j < count($t) ; $j++) { ?>
							<option value="<?php echo $j;?>" <?php if($control!=null){ if($control->tipo==$j){?> selected="selected"<?php }}?>><?php echo $t[$j];?></option>
						<?php }?>	
						</select>
					</td>
					<?php }?>
					<td>
						<div class="g-control-accordion" style="width:30px;">
							<button class="confirmar_proceso" data-pr="<?php echo $proceso->idpr;?>" data-event="confirmar"><i class="icon-trashcan2"></i></button>
						</div>
					</td>
					<td>
				<?php if($control==null){?>
						<button type="button" class="btn btn-inverse-primary <?php if($type_save=="pre"){?>append_proceso_empleado<?php }else{?>add_proceso_producto<?php }?> btn-mini waves-effect waves-light" data-pr="<?php echo $proceso->idpr;?>"<?php if($type_save=="pre"){?> data-disableds="select#grado<?php echo $proceso->idpr;?>"<?php }?> data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"><i class="fa fa-plus"></i> add</button>
				<?php }else{ ?>
						<button type="button" class="btn btn-default btn-mini waves-effect waves-light" disabled="disabled"><i class="fa fa-plus"></i> add</button>
				<?php } ?>
					</td>
				</tr>
			<?php
				}//en for 
			}else{ ?>
			<?php } ?>
				</tbody>
			</table>
</div>
<div class="row text-right" style="padding-right:15px;">
  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_proceso"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar proceso</span></button>
</div>
<script>$(".new_proceso").new_proceso();$(".update_proceso").update_proceso();$(".confirmar_proceso").confirmar_proceso();<?php if($type_save=="pre"){?>$(".append_proceso_empleado").append_proceso_empleado();<?php }else{?>$("button.add_proceso_producto").add_proceso_producto();<?php }?>


$('.color-class').maxlength();

</script>