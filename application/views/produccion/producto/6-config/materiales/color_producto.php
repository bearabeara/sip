<?php $url=base_url().'libraries/img/materiales/miniatura/';
      $materiales_producto=$this->M_producto_material->get_search_producto($producto->idp,"","");
      for($j=0; $j < count($materiales_producto); $j++){ $material_producto=$materiales_producto[$j];
        $img="default.png";
        $fotografia=$this->M_material_imagen->get_row_2n("idmi",$material_producto->idmi,"portada","1");
        if(!empty($fotografia)){
          $img=$fotografia[0]->nombre;
        }
    ?>
    <div class="col-md-6 col-xs-12">
      <table class="table table-bordered">
        <tr>
           <td><div class="g-img-modal"><div id="item"><?php echo $j+1;?></div>
           <a href"javascript:"  <?php if(!empty($fotografia)){ ?> onclick="img_material($(this))" data-mi="<?php echo $fotografia[0]->idmi;?>" <?php }?>><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail-modal"></a></div></td>
           <td width="65%"><?php echo $material_producto->nombre." (".$material_producto->nombre_c.")";?></td>
           <td width="20%"><?php echo $material_producto->cantidad;?></td>
           <td width="15%"></td>
        </tr>
      </table>
  </div>
<?php }// end for ?>
    <?php 
      $materiales_pgc=$this->M_producto_grupo_color_material->get_material("pgcm.idpgrc",$producto->idpgrc);
      for ($m=0; $m < count($materiales_pgc) ; $m++) { $material_pgc=$materiales_pgc[$m];
        $img="default.png";
        $fotografia=$this->M_material_imagen->get_row_2n("idmi",$material_pgc->idmi,"portada","1");
        if(!empty($fotografia)){
          $img=$fotografia[0]->nombre;
        }
    ?>
    <div class="col-md-6 col-xs-12">
<table class="table table-bordered">
  <tr>
     <td><div class="g-img-modal"><div id="item"><?php echo $j+1;$j++;?></div>
     <a href"javascript:" <?php if(!empty($fotografia)){ ?> onclick="img_material($(this))" data-mi="<?php echo $fotografia[0]->idmi;?>" <?php }?>><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail-modal"></a></div></td>
     <td width="65%">
    <select id="color-mat<?php echo $material_pgc->idpgcm;?>" class="form-control form-control-sm" onchange="update_color_material($(this))" data-pgcm="<?php echo $material_pgc->idpgcm;?>" data-pgc="<?php echo $producto->idpgrc;?>" data-refresh="true">
      <option>Seleccionar....</option>
     <?php for ($k=0; $k < count($materiales); $k++){ $material=$materiales[$k]; ?>
        <option value="<?php echo $material->idm;?>" <?php if($material->idm==$material_pgc->idm){ echo "selected";}?>><?php echo $material->nombre." (".$material->nombre_c.")";?></option>
     <?php } ?>
     </select></td>
     <td width="20%">
        <form onsubmit="return update_color_material($(this))" data-pgcm="<?php echo $material_pgc->idpgcm;?>" data-pgc="<?php echo $producto->idpgrc;?>" data-refresh="false">
          <input type="number" class="form-control form-control-sm" placeholder="Cantidad por producto" id="color-cant<?php echo $material_pgc->idpgcm;?>" value="<?php echo $material_pgc->cantidad;?>" step="any" min="0" max="9999999.9999">
        </form>
      </td>
     <td width="15%"><div class="g-control-accordion" style="width:60px;">
        <button onclick="update_color_material($(this))" data-pgcm="<?php echo $material_pgc->idpgcm;?>" data-pgc="<?php echo $producto->idpgrc;?>" data-refresh="true"><i class="icon-floppy-o"></i></button>
        <button onclick="confirm_color_material($(this))" data-pgcm="<?php echo $material_pgc->idpgcm;?>" data-pgc="<?php echo $producto->idpgrc;?>"><i class="icon-trashcan2"></i></button>
     </div></td>
  </tr>
</table>
  </div>
    <?php }// end for ?>