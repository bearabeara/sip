<?php $rand=rand(10,999999);?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a href="javascript:" class="nav-link config_producto<?php echo $rand;?>" data-p='<?php echo $producto->idp;?>'>Producto</a></li>
	<li class="nav-item"><a href="javascript:" class="nav-link categoria" data-p="<?php echo $producto->idp;?>">Categorias</a></li>
	<li class="nav-item"><a href="javascript:" class="nav-link produccion active" data-p="<?php echo $producto->idp;?>">Producción</a></li>
</ul>
<?php $url=base_url().'libraries/img/';
	$t = array(0 => 'Maestro(a)', 1 => 'Ayudante');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group col-xs-12">
	<?php if(!empty($colores)){ ?>
				<div id="list_color" class="accordion-border" role="tablist" aria-multiselectable="true">
					<?php for($i=0; $i < count($colores) ; $i++){ $color=$colores[$i];
						$id="color".$color->idpgrc."-".rand(10,99);
						$codigo=$color->codigo;$nombre=$color->nombre;
						if($color->abr_g!="" && $color->abr_g!=NULL){ $codigo.="-".$color->abr_g;$nombre.=" - ".$color->nombre_g;}
						$codigo.="-".$color->abr_c;$nombre.=" - ".$color->nombre_c;
						$img="sistema/miniatura/default.jpg";
						if($color->portada!="" && $color->portada!=null){
							$v=explode("|", $color->portada);
							if(count($v==2)){
								if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
								if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
							}
						}else{
							$v=explode("|", $color->portada_producto);
							if(count($v==2)){
								if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
								if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
							}
						}
						?>
						<div class="accordion-panel" id="accordion-panel<?php echo $id;?>">
							<div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
								<span class="card-title accordion-title">
									<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
										<div class="item"><?php echo $i+1;?></div>
										<span class="g-img-modal"><img src="<?php echo $url.$img; ?>" class="img-thumbnail g-thumbnail-modal"></span>
										<span><?php echo $codigo.": ".$nombre; ?></span>
									</a>
								</span>
							</div>
							<div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
								<div class="accordion-content accordion-desc">
								<?php $procesos=[];
									for($j=0; $j < count($producto_proceso); $j++){ $proceso=$producto_proceso[$j];
										$control1=$this->lib->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
										$control2=$this->lib->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
										if($control1==null && $control2==null){
											$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
										}else{
											$control=$this->lib->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$color->idpgr);
											if($control!=null){
												$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
											}
											$control=$this->lib->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$color->idpgrc);
											if($control!=null){
												$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
											}
										}
									}// enf for
								?>
									<div class="list-group">
										<div class="list-group-item" style="max-width:100%">
											<div class="row">
												<div class="form-group col-xs-12">
										<?php if(!empty($procesos)){ ?>
													<div id="list_proceso" class="accordion-border" role="tablist" aria-multiselectable="true">
										<?php
											$procesos=json_decode(json_encode($procesos));
											foreach($procesos as $key => $proceso){
												$id2="proceso".$proceso->idppr."-".rand(10,99);
										?>
										<?php $tbl="tbl-".rand(1,999)."-".$proceso->idppr."-".$color->idpgrc;?>
														<div class="accordion-panel" id="accordion-panel<?php echo $id2;?>">
															<div class="accordion-heading" role="tab" id="heading<?php echo $id2;?>">
																<span class="card-title accordion-title">
																	<span class="g-control-accordion" style="margin-top: 10px; margin-right: 16px;">
																		<button class="refresh_producto_proceso" data-tbl="<?php echo $tbl;?>"><i class="fa fa-refresh"></i></button>
																	</span>
																	<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id2;?>" aria-expanded="false" aria-controls="collapse<?php echo $id2;?>">
																		<span><?php echo $proceso->nombre; ?></span>
																	</a>
																</span>
															</div>
															<div id="collapse<?php echo $id2;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id2;?>" aria-expanded="false" style="height: 0px;">
																<div class="accordion-content accordion-desc" id="datos-proceso-empleado-<?php echo $proceso->idppr."-".$color->idpgrc;?>">
																	<div class="g-table-responsive" id="<?php echo $tbl;?>" data-pgc="<?php echo $color->idpgrc;?>" data-pr="<?php echo $proceso->idpr;?>" data-ppr="<?php echo $proceso->idppr;?>">
																<?php 
																$empleados=[];
																for($e=0; $e < count($productos_procesos_empleados) ; $e++){ $pre=$productos_procesos_empleados[$e];
																	if($pre->idpgrc==$color->idpgrc && $pre->idpr==$proceso->idpr){
																		$img="sistema/default.jpg";
																		if($pre->fotografia!=null && $pre->fotografia!=""){ $img="personas/miniatura/".$pre->fotografia;}
																		$empleados[]=array('idproe'=>$pre->idproe,'idpgrc'=>$pre->idpgrc, 'ide'=>$pre->ide, 'ci'=>$pre->ci, 'fotografia'=>$img,'nombre'=>$pre->nombre_completo, 'calidad'=>$pre->calidad, 'tipo'=>$pre->tipo);
																	}
																} ?>
																		<table class="table table-bordered table-hover">
																			<thead>
																				<tr>
																					<th class="img-thumbnail-50">#Item</th>
																					<th width="60%">Nombre</th>
																					<th width="10%">Área</th>
																					<th width="30%">Calidad</th>
																					<th></th>
																				</tr>
																			</thead>
																			<tbody>
																		<?php
																		if(!empty($empleados)){
																			$j_empleados=json_decode(json_encode($empleados));
																			$cont=1;
																			foreach($j_empleados as $key => $empleado){
																		?>
																			<tr>
																				<td class="img-thumbnail-50">
																					<div class="img-thumbnail-50"></div>
																					<div class="item"><?php echo $cont++;?></div>
																					<img src="<?php echo $url.$empleado->fotografia;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50" data-title="<?php echo $empleado->nombre;?>" data-desc="">
																				</td>
																				<td><?php echo $empleado->nombre; ?></td>
																				<td><?php echo $t[$empleado->tipo*1];?></td>
																				<td>
														                            <?php $id_rango=rand(0,9999)."-".$empleado->idproe; ?>
														                            <div class="rangos" id="rangos_proe<?php echo $id_rango;?>" data-proe="<?php echo $empleado->idproe;?>">
														                                <input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad>=10){?> checked="checked"<?php } ?> data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==9){?> checked="checked"<?php } ?> data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==8){?> checked="checked"<?php } ?> data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==7){?> checked="checked"<?php } ?> data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==6){?> checked="checked"<?php } ?> data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==5){?> checked="checked"<?php } ?> data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==4){?> checked="checked"<?php } ?> data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==3){?> checked="checked"<?php } ?> data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==2){?> checked="checked"<?php } ?> data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
														                                <input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==1){?> checked="checked"<?php } ?> data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
														                            </div>
																				</td>
																				<td>
																					<div class="g-control-accordion" style="width:60px;">
																				      	<button class="update_producto_empleado" data-proe="<?php echo $empleado->idproe;?>" data-rangos="<?php echo $id_rango;?>" data-tbl="<?php echo $tbl;?>"><i class="icon-floppy-o"></i></button>
																			      		<button class="confirmar_producto_empleado" data-proe="<?php echo $empleado->idproe;?>" data-tbl="<?php echo $tbl;?>"><i class="icon-trashcan2"></i></button>
																					</div>
																				</td>
																			</tr>
																		<?php
																			}
																		}else{
																			echo "<tr><td colspan='5' class='text-center'><h4>0 empleados asignados.</h4></td></tr>";
																		}
																	?>
																			</tbody>
																		</table>
																	</div>
																	<div class="row text-right" style="padding-right:15px;">
																		<button type="button" class="btn btn-info btn-mini waves-effect waves-light view_empleado_proceso" data-tbl="<?php echo $tbl;?>">
																			<i class="fa fa-plus"></i><span class="m-l-10">Adicionar</span>
																		</button>
																		<button type="button" class="btn btn-primary btn-mini waves-effect waves-light save_empleados_producto" data-tbl="<?php echo $tbl;?>">
																			<i class="fa fa-floppy-o"></i><span class="m-l-10">guardar</span>
																		</button>
																	</div>
																</div>
															</div>
														</div>
										<?php
											}// end foreach
										?>
												</div>
											<?php }else{
													echo "<div class='text-center'><h4>0 procesos registrados en el producto.</h4></div>";
											}?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php }?>
					</div>
	<?php }else{
		echo "<div class='text-center'><h3>0 colores registrados en el producto.</h3></div>";
	}?>
				</div>
			</div>
		</div>
	</div>
<script>
$(".config_producto<?php echo $rand;?>").config_producto();



$("a.produccion").click(function(){ $(this).produccion();});$("a.categoria").categoria();$(".configuracion_producto").click(function(){ $(this).configuracion_producto();});$("button.view_empleado_proceso").click(function(){$(this).view_empleado_proceso();});$("button.confirmar_producto_empleado").click(function(){ $(this).confirmar_producto_empleado();});$("button.update_producto_empleado").click(function(){ $(this).update_producto_empleado();});$("button.save_empleados_producto").click(function(){ $(this).save_empleados_producto();});$("button.refresh_producto_proceso").click(function(){ $(this).refresh_producto_proceso();});$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});</script>