<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="40%">Color</th>
				<th width="20%">Abr.</th>
				<th width="20%">Codigo</th>
				<th width="10%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($colores); $i++) { $color=$colores[$i]; ?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td>
					<form class="update_color" data-co="<?php echo $color->idco;?>">
						<input type="text" class="form-control form-control-sm" id="nom<?php echo $color->idco;?>" value="<?php echo $color->nombre;?>" maxlength="50">
					</form>
				</td>
				<td>
					<form class="update_color" data-co="<?php echo $color->idco;?>">
						<input type="text" class="form-control form-control-sm" id="abr<?php echo $color->idco;?>" value="<?php echo $color->abr;?>" maxlength="10">
					</form>
				</td>
				<td><input type="color" class="form-control form-control-sm" id="col<?php echo $color->idco;?>" value="<?php echo $color->codigo;?>" placeholder"#FFF" style="padding:.15rem;"></td>
				<td>
					<div class="g-control-accordion" style="width:60px;">
				      	<button class="update_color" data-co="<?php echo $color->idco;?>"><i class="icon-floppy-o"></i></button>
			      		<button class="confirmar_color" data-co="<?php echo $color->idco;?>"><i class="icon-trashcan2"></i></button>
					</div>
				<td>
					<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light seleccionar_color" data-co="<?php echo $color->idco;?>">
		                <?php if($type=="new"){ ?><i class="fa fa-plus"></i> add<?php }?>
		                <?php if($type=="update"){ ?><i class="fa fa-exchange"></i> Sel.<?php }?>
		            </button>
				</td>
			</tr>		
	<?php }?>
		</tbody>
	</table>
</div>
	<div class="row text-right" style="padding-right:15px;">
	  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_color">
	    <i class="fa fa-plus"></i><span class="m-l-10"> Color</span>
	  </button>
	</div>
<script>$(".update_color").update_color();$("button.confirmar_color").confirmar_color();$("button.seleccionar_color").seleccionar_color();$("button.new_color").new_color();</script>