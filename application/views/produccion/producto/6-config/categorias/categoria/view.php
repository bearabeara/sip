<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="60%">Categorías</th>
				<th width="20%">Abrev.</th>
				<th width="10%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($categorias); $i++) { $categoria=$categorias[$i]; ?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td>
					<form class="update_grupo" data-g="<?php echo $categoria->idgr;?>">
						<input type="text" class="form-control form-control-sm" id="cat<?php echo $categoria->idgr;?>" value="<?php echo $categoria->nombre;?>" maxlength="50">
					</form>
				</td>
				<td>
					<form class="update_grupo" data-g="<?php echo $categoria->idgr;?>">
						<input type="text" class="form-control form-control-sm" id="abr<?php echo $categoria->idgr;?>" value="<?php echo $categoria->abr;?>" maxlength="10">
					</form>
				</td>			
				<td>
					<div class="g-control-accordion" style="width:60px;">
				      	<button class="update_grupo" data-g="<?php echo $categoria->idgr;?>"><i class="icon-floppy-o"></i></button>
			      		<button class="confirmar_grupo" data-g="<?php echo $categoria->idgr;?>"><i class="icon-trashcan2"></i></button>
					</div>
	      		</td>
				<td>
					<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light seleccionar_grupo" data-p='<?php echo $idp;?>' data-g="<?php echo $categoria->idgr;?>" data-type="<?php echo $type;?>"  <?php if($type=="update"){ echo "data-pg='".$idpgr."'"; } ?>>
		                <?php if($type=="new"){ ?><i class="fa fa-plus"></i> add<?php }?>
		                <?php if($type=="update"){ ?><i class="fa fa-exchange"></i> Sel.<?php }?>
		            </button>
				</td>
			</tr>		
	<?php }?>
		</tbody>
	</table>
</div>
<div class="row text-right" style="padding-right:15px;">
  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_grupo">
    <i class="fa fa-plus"></i><span class="m-l-10"> Categoria</span>
  </button>
</div>
<script>$(".update_grupo").update_grupo();$("button.confirmar_grupo").confirmar_grupo();$("button.seleccionar_grupo").seleccionar_grupo();$("button.new_grupo").new_grupo();</script>