<?php
	$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>, puede seleccionar <strong>hasta 10 imágenes</strong>"';
	$help2='title="Nombre" data-content="Ingrese un nombre del repujado o sello en formato alfanumerico de 1 a 150 caracteres <b>puede incluir espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help3='title="Tipo" data-content="Seleccione el tipo de elemento a introducir."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$v = array('0' => 'Repujado', '1'=>'Sello');
	$e=json_decode(json_encode($v));
?>
<div class="row"><div class="col-sm-6 offset-sm-6 col-xs-12 text-xs-right"><span class="text-danger">(*)</span> Campo obligatorio</div></div>
		  	<div class="row">
				<div class="form-group">
					<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Fotografía:</label>
					<div class="col-sm-9 col-xs-12">
						<div class="input-group">
							<input class='form-control form-control-sm' type="file" id="img_rs">
							<span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label"><span class="text-danger">(*)</span>Nombre:</label>
					<div class="col-sm-9 col-xs-12">
						<form class="update_repujado_sello" data-rs="<?php echo $repujado_sello->idrs;?>"<?php if(isset($destino)){?> data-destino="<?php echo $destino;?>"<?php }?><?php if(isset($type)){?> data-type="<?php echo $type;?>"<?php }?>>
							<div class="input-group">
								<input class='form-control form-control-sm' type="text" id="nom_rs" maxlength="150" placeholder="Nombre del repujado o sello" value="<?php echo $repujado_sello->nombre;?>">
								<span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label"><span class="text-danger">(*)</span>Tipo:</label>
					<div class="col-sm-9 col-xs-12">
						<div class="input-group">
							<select id="tip_rs" class="form-control form-control-sm">
						<?php foreach ($e as $clave => $valor) { ?>
								<option value="<?php echo $clave;?>" <?php if($repujado_sello->tipo==$clave){ echo "selected";} ?>><?php echo $valor;?></option>
						<?php }?>
							</select>
							<span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
				</div>
			</div>
<script>$('[data-toggle="popover"]').popover({html:true}); Onfocus("img_rs");$("form.update_repujado_sello").submit(function(e){ $(this).update_repujado_sello();e.preventDefault();});</script>