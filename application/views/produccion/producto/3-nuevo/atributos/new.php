<?php
	$help1='title="Nombre de atributo" data-content="Ingrese un nombre alfanumerico de 2 a 100 caracteres <b>puede incluir espacios espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>

		  	<div class="row">
					<div class="col-xs-12">
					<form class="save_atributo">
						<div class="input-group">
							<input class='form-control form-control-xs' type="text" id="n_atrib" placeholder='Nombre de atributo' maxlength="100">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
					</div>
			</div>
<script>$("form.save_atributo").submit(function(e){ $(this).save_atributo();e.preventDefault();}); Onfocus("n_atrib");$('[data-toggle="popover"]').popover({html:true});</script>