<?php
$help1='title="Costo de venta" data-content="Ingrese el costo de venta del producto, el valor debe ser mayor a 0 y menor p igual a 9999999.9"';
$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
$url=base_url().'libraries/img/';
?>
<div class="accordion-border" role="tablist" aria-multiselectable="false">
	<?php $producto_categoria=$this->lib->producto_categoria($productos_grupos_colores);
	if(count($producto_categoria)>0){
		$producto_categoria=json_decode(json_encode($producto_categoria));
		foreach ($producto_categoria as $key => $producto) {
			$id=$producto->idp;
			$categorias=json_decode(json_encode($producto->categorias));
			$costo_promedio=0;
			if(count($producto->categorias)>0){
				foreach($categorias as $key => $categoria){
					$costo_promedio+=($categoria->costo*1);
				}
				$costo_promedio/=count($producto->categorias);
				$costo_promedio=number_format($costo_promedio,1,'.','');
			}
			?>
			<div class="accordion-panel g-accordion-panel accordion-box ui-accordion ui-widget ui-helper-reset" id="accordion-panel<?php echo $id;?>" >
				<div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
					<span class="card-title accordion-title">
						<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
							<span style="font-size: .9em;"><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
							<?php ?>
							<label class="badge badge-inverse-default" style="float: right; font-size: .7rem;">Costo prom.: <span class="promedio"><?php echo number_format($costo_promedio,2,'.','');?></span>Bs.</label>
						</a>
					</span>
				</div>
				<div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
					<div class="accordion-content accordion-desc" data-pr="<?php echo $producto->idp;?>">
						<div class="table-responsive g-table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<th><div class="img-thumbnail-40">#Item</div></th>
									<th width="65%">Nombre</th>
									<th width="35%">Costo</th>
									<th width="5%"></th>
								</thead>
								<tbody>
									<?php if(count($producto->categorias)>0){
										foreach($categorias as $key => $categoria) {
											$img="sistema/miniatura/default.jpg";
											if($categoria->portada!="" && $categoria->portada!=null){
												$v=explode("|", $categoria->portada);
												if(count($v==2)){
													if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
													if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
												}
											}else{
												if($producto->portada!="" && $producto->portada!=null){
													$v=explode("|", $producto->portada);
													if(count($v==2)){
														if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
														if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
													}
												}
											}
											$codigo="";$nombre="";
											if($categoria->abr_g!="" && $categoria->abr_g!=NULL){ $codigo=$categoria->abr_g;$nombre=$categoria->nombre_g;}
											if($codigo!=""){ $codigo.="-";}if($nombre!=""){ $nombre.=" - ";}
											$codigo.=$categoria->abr_c;$nombre.=$categoria->nombre_c;
											?>
											<tr>
												<td><img src="<?php echo $url.$img;?>" class="g-img-thumbnail img-thumbnail-40"></td>
												<td><?php echo $nombre;?></td>
												<td>
													<form class="form-costo" data-pgc="<?php echo $categoria->idpgrc;?>" data-p="<?php echo $producto->idp;?>">
														<div class="input-group">
															<input type="number" class="form-control form-control-sm input-150" placeholder="0.0" step="any" min="0" max="9999999.9" id="cos<?php echo $categoria->idpgrc;?>" value="<?php echo $categoria->costo; ?>">
															<span class="input-group-addon form-control-sm btn-default" <?php echo $popover2.$help1; ?>><i class="fa fa-info-circle"></i></span>
														</div>
													</form>
												</td>
												<td><div class="g-control-accordion" style="width:30px;"><button class="btn-costo" data-pgc="<?php echo $categoria->idpgrc;?>" data-p="<?php echo $producto->idp;?>"><i class="icon-floppy-o"></i></button></div></td>
											</tr>
											<?php 
												}// end for
											}else{?>
											<tr>
												<td colspan="4" class="text-center"><h5>0 categorías registradas...</h5></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</div>   
							</div>
						</div>
					</div>
					<?php
				}
			}
			?>
		</div>
		<script>$("button.btn-costo").click(function(){ $(this).save_costo();});$("form.form-costo").submit(function(event){ $(this).save_costo(); event.preventDefault();; });</script>