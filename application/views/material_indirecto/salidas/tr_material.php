<?php date_default_timezone_set("America/La_Paz");
	$url=base_url().'libraries/img/';
	$popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="Observaciónes" data-content="la observacion puede poseer un formato alfanumerico de 0 a 300 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$img="sistema/miniatura/default.jpg";
	if($material->fotografia!="" && $material->fotografia!=NULL){
		$img="materiales/miniatura/".$material->fotografia;
	}
	$rand=rand(10,99999999);
?>
		<td>
			<div id="item"><?php echo $item;?></div>
			<img src="<?php echo $url.$img;?>" class='img-thumbnail g-img-thumbnail' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
		</td>
		<td><?php echo $material->nombre; ?></td>
		<td>
			<div class="input-group input-120">
				<input type="text" id="c<?php echo $material->idme;?>" class="form-control form-control-sm" value="<?php echo $material->cantidad; ?>" disabled>
				<span class="input-group-addon form-control-sm"><?php echo $material->abr."."; ?></span>
			</div>
		</td>
		<td>
			<select id="emp<?php echo $material->idme;?>" class="form-control form-control-sm">
				<option value="">Seleccionar...</option>
			<?php for ($e=0; $e < count($empleados) ; $e++){ $empleado=$empleados[$e]; 
					if($empleado->estado=="1"){ ?>
					<option value="<?php echo $empleado->ide;?>"><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
			<?php 	}
				}?>
			</select>
		</td>
		<td><input type="datetime-local" id="fech<?php echo $material->idme;?>" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" class="form-control form-control-sm"></td>
		<td>
			<div class="input-group input-120">
				<form class="save_movimiento<?php echo $rand;?>" data-me="<?php echo $material->idme;?>" data-type="s" data-item="<?php echo $item;?>">
					<input type="number" id="can<?php echo $material->idme;?>" class="form-control form-control-sm input-100" placeholder="0" min='0' step="any" min='0' max="999999999.9999999">
				</form>
				<span class="input-group-addon form-control-sm"><?php echo $material->abr."."; ?></span>
			</div>
		</td>
		<td>
			<div class="input-group">
				<textarea id="obs<?php echo $material->idme;?>" class="form-control form-control-sm" placeholder="Observaciónes de ingreso de material" rows="2"></textarea>	
				<span class="input-group-addon form-control-sm" <?php echo $popover3;?>><i class='fa fa-info-circle'></i></span>
			</div>
		</td>
	<?php if($privilegio->ot11u==1){?>
		<td><div class="btn-group"><a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button"><i class="fa fa-ellipsis-v"></i></a><div class="dropdown-menu g-dropdown-menu-right"><button class="dropdown-item save_movimiento<?php echo $rand;?>" type="button" data-toggle="tooltip" data-placement="left" title="Guardar" data-me="<?php echo $material->idme;?>" data-type="s" data-item="<?php echo $item;?>"><i class="icon-floppy2"></i><span class="dropdown-item-text"> Guardar</span></button></div></div><script>$('.save_movimiento<?php echo $rand;?>').save_movimiento();</script></td>
	<?php }?>