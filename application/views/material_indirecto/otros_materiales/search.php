<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='img-thumbnail-60 hidden-sm'><div class="img-thumbnail-60"></div></td>
		<td class='hidden-sm celda-sm-10'>
			<form class="view_otro_material">
				<input class='form-control form-control-sm search_otro_material' type="search" id="s_cod" placeholder='Código'/>
			</form>
		</td>
		<td class='celda-sm-30'>
			<form class="view_otro_material">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_otro_material" placeholder='Buscar...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_otro_material" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' style="width:55%"></td>
		<td style="width:5%" class="hidden-sm">
			<?php 
				$search=json_encode(array('function'=>'view_otro_material','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_otro_material','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>		
</table>
<script>Onfocus('s_cod');$(".search_otro_material").search_otro_material();$(".view_otro_material").view_otro_material();</script>