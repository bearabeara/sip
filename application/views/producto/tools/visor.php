<?php $url=base_url().'libraries/img/';?>
<div class="carousel g-carousel carousel-slider center" data-indicators="true">
<?php 
  $position=0;
  foreach ($fotografias as $key => $fotografia) { ?>
    <div class="carousel-item">
        <figure>
          <figcaption><b><?php echo $producto->nombre; ?></b><br><?php echo $fotografia->descripcion;?></figcaption>
          <img width="100%" src="<?php echo $url.$fotografia->archivo;?>">
        </figure>
        
    </div>
<?php $position=$fotografia->position; }?>
  </div>
<script>
  $('.carousel.carousel-slider').carousel({fullWidth: true});
  document.querySelector('.carousel.carousel-slider').style.height = (window.innerHeight+(window.innerHeight/2.5)) + "px"
  $('.carousel.carousel-slider').carousel('set',<?php echo $position;?>);
</script>