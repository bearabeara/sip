 <div class="navbar-fixed">
  <nav class="blue accent-2">
    <div class="nav-wrapper">
      <form id="form_search">
        <div class="input-field">
          <input type="search" id="nom" placeholder="Nombre del producto">
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>
 </div>