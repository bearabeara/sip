<?php $dir="/final";$min=".min";$version="8";?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Detalle de productos</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="" />
    <meta name="keywords" content="marroquineria, cuero, maletines, billeteras" />
    <meta name="author" content="bearabeara.co.uk" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/icomon<?php echo $min;?>.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/gdev.materialize<?php echo $min;?>.css?v=<?php echo $version;?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/gdev<?php echo $min;?>.css?v=<?php echo $version;?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
<?php $this->load->view('producto/tools/modal');?>
<div id="search"></div>
<div id="preloader" class="preloader" style="display: visible;">
    <div class="preloader-wrapper active">
    <div class="spinner-layer spinner-blue-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
 </div>
<div id="container"></div>
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning"><h1>Advertencia!!</h1><p>Estás utilizando una versión desactualizada de Internet Explorer, actualiza por favor a cualquiera de los siguientes navegadores web para acceder a este sitio web.</p><div class="iew-container"><ul class="iew-download"><li><a href="http://www.google.com/chrome/"><img src="assets/images/browser/chrome.png" alt="Chrome"><div>Chrome</div></a></li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Lo siento por los inconvenientes ocasionados!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous""></script>
    <!--<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/jquery-3.1.1.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/gmdev<?php echo $min;?>.js?v=<?php echo $version;?>"></script><script>$(document).ready(function($){ var atrib=new FormData();atrib.append('pgc','<?php echo $pgc;?>');get_2n('../../productos/search_detalle',atrib,'search',false,'../../productos/view_detalle',atrib,'container',true);});</script><script type="text/javascript" src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/materialize<?php echo $min;?>.js"></script>
</body>
</html>
