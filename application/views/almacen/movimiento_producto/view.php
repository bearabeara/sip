<?php 
$j_movimientos=json_encode($movimientos); ?>
<table class="table table-bordered table-striped">
<thead>
	<tr>
    	<th colspan="9"></th> 
		<th colspan="2">
		<?php if($privilegio[0]->al3d=="1"){ ?>
			<button id="bh" class="col-xs-12 btn btn-danger btn-sm">Borrar Historial</button>
		<?php }?>
		</th>
    </tr>
    <tr>
    	<th width='5%'>#Item</th> 
	    <th width='7%'>Código de Material</th>
		<th width='16%;'>Nombre de Material</th>
		<th width='15%'>Almacen</th>
		<th width='10%'>Usuario</th>
		<th width='6%'>Hora</th>
		<th width='13%'>fecha</th>  
		<th width='7%;'>Stock</th>
		<th width='7%;'>Ingreso</th>
		<th width='7%;'>Salida</th>
		<th width='7%;'>Saldo</th>
    </tr>
</thead>
<?php if(count($movimientos)<=0){
	echo "<tr><td colspan='11'><h2>0 registros encontrados...</h2></td></tr>";
 }else{ ?>
    <tbody>
    <?php for ($i=0; $i < count($movimientos) ; $i++){ $movimiento=$movimientos[$i]; ?>
    	<tr>
    		<td><?php echo $i+1;?></td>
		    <td><?php echo $movimiento->cod;?></td>
			<td><?php echo $movimiento->nombre;?></td>
			<td><?php echo $movimiento->almacen;?></td>
			<td><?php echo $movimiento->usuario;?></td>
			<td><?php echo $movimiento->hora;?></td>
			<td><?php echo $this->validaciones->formato_fecha($movimiento->fecha,'Y,m,d');?></td> 
			<td><?php echo $movimiento->cantidad;?></td>
			<td><?php echo $movimiento->ingreso;?></td>
			<td><?php echo $movimiento->salida;?></td>
			<td><?php echo ($movimiento->cantidad+$movimiento->ingreso-$movimiento->salida);?></td>
    	</tr>
    	<?php }//end else
    }//end ifelse ?>
    </tbody>
</table>

<script language="javascript">
	$("#print").removeAttr("onclick");
	$("#print").unbind("click");
	$("#print").click(function(){ imprimir_almacen_hp('<?php echo $j_movimientos;?>'); }); 
	$("#bh").click(function(){ confirmar_historial_producto(); }); 
</script>