<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm g-thumbnail'><div class="g-thumbnail"></div></td>
		<td class='hidden-sm celda-sm-10'>
			<form class="view_almacen" data-type="search">
				<input type="search" id="s_cod" class="form-control form-control-sm search_almacen" maxlength="10" placeholder='Código' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="2">
			</form>
		</td>
		<td class='celda-sm-30 search-principal'>
			<form class="view_almacen" data-type="search">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_almacen" maxlength="100" placeholder='Buscar nombre...' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="3"/>
					<span class="input-group-addon form-control-sm view_almacen" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' style='width:15%'>
			<select id="s_tip" class="form-control form-control-sm search_almacen" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="like" data-col="4">
				<option value=''>Seleccionar...</option>	
				<option value='0'>Materia Prima</option>
				<option value='1'>Productos Acabados</option>
			</select>
		</td>
		<td class='hidden-sm' style='width:45%'></td>
		<td class='hidden-sm' style='width:0%;'>
		<?php 
			$search=json_encode(array('function'=>'view_almacen','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
			$all=json_encode(array('function'=>'view_almacen','title'=>'Actualizar','atribs'=> json_encode(array('type' => "all"))));
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>		
	</tr>
</table>
<script>Onfocus('s_cod');$(".view_almacen").view_almacen();$(".search_almacen").search_almacen();</script>