<?php 
	$help1='title="Subir Fotografía" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>"';
	$help2='title="Código de almacen" data-content="Ingrese un código alfanumerico de<b> 2 a 10</b> caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help3='title="Nombre de almacen" data-content="Ingrese un nombre alfanumerico de<b> 2 a 100</b> caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help4='title="Tipo de almacen" data-content="Seleccione el tipo de material que almacenara el almacen, <b>[Selecione materiales si el almacen almacena materiales como: Telas, accesorios, material prima, etc.][Selecione productos acabados, si el almacen almacena productos ya terminados como: mochilas, carteras, etc.]</b>, El valor vacio no es aceptado<b>"';
	$help5='title="Descripción de almacen" data-content="Ingrese una descripción alfanumerica <b>hasta 400</b> caracteres <b>puede incluir espacios y sin saltos de línea</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>""';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Fotografía: </label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group input-group-xs">
						<input type="file" name="img" id="archivo" class="form-control input-xs">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>> <i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Código:</label>
				<div class="col-sm-10 col-xs-12">
					<form class="update_almacen" data-a="<?php echo $almacen->ida;?>">
						<div class="input-group input-group-xs">
							<input type="text" placeholder='Código' maxlength="10" id="cod" class="form-control input-xs" value="<?php echo $almacen->codigo;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>> <i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Nombre:</label>
				<div class="col-sm-10 col-xs-12">
					<form class="update_almacen" data-a="<?php echo $almacen->ida;?>">
						<div class="input-group input-group-xs">
							<input type="text" placeholder='Nombre de Almacen' id="nom" maxlength="100" class="form-control input-xs" value="<?php echo $almacen->nombre;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>> <i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Tipo: </label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group input-group-xs">
						<select id="tip" class="form-control input-xs" disabled="">
							<option value="">Seleccione...</option>
							<option value="material" <?php if($almacen->tipo=='0'){ echo "selected";}?>>Almacen de Materiales</option>
							<option value="producto" <?php if($almacen->tipo=='1'){ echo "selected";}?>>Almacen de Productos Acabados</option>
						</select>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>> <i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Descripción</label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group input-group-xs">
						<textarea id="des" placeholder='Descripción del almacen' rows="3" maxlength="300" class="form-control input-xs"><?php echo $almacen->descripcion;?></textarea>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>> <i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>Onfocus("cod");$('[data-toggle="popover"]').popover({html:true});$("form.update_almacen").submit(function(e){$(this).update_almacen();e.preventDefault();});</script>