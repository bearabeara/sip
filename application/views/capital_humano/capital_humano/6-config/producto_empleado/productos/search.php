<div class="list-group">
	<div class="list-group-item" id="search_2" style="max-width:100%">
		<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="100%">
						<form id="f2_nom">
							<input id="s2_nom" type="search" class="form-control form-control-sm" placeholder="Nombre de producto" <?php if(isset($type)){?> data-type="<?php echo $type;?>" <?php }?> <?php if(isset($elemento)){?> data-elemento="<?php echo $elemento;?>"<?php }?>>
						</form>
					</td>
					<td>
						<div style="width:66px;">
							<div class="btn-group " role="group">
							<?php $b = json_encode(array("funcion" => "search_producto","atributos"=>""));
								$v = json_encode(array("funcion" => "all_producto","atributos"=>""));
							?>
								<?php $this->load->view('estructura/botones/buscador2',['id' => '2','f_buscar'=>$b,'f_ver'=>$v]);?>
							</div>
						</div>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_2"></div>
</div>
<script>Onfocus("s2_nom"); $("#f2_nom").submit(function(e){ $("#f2_nom").search_producto(); e.preventDefault();}); </script>