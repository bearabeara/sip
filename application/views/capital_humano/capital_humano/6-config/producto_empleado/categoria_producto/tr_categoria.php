<?php 
	$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/'; $img="sistema/miniatura/default.jpg";
	$id=$color->idp."-".rand(0,999999);
	$control=0;
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
	$procesos=[];
	for($j=0; $j < count($producto_procesos); $j++){ $proceso=$producto_procesos[$j];
		$control1=$this->lib->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
		$control2=$this->lib->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
		if($control1==null && $control2==null){
			$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr);
		}else{
			$control=$this->lib->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$color->idpgr);
			if($control!=null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr);
			}
			$control=$this->lib->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$color->idpgrc);
			if($control!=null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr);
			}
		}
	}// enf for
	$procesos=json_decode(json_encode($procesos));
?>
	<tr class="row-producto" id="<?php echo $color->idpgrc."-".rand(1,99999);?>" data-pgc="<?php echo $color->idpgrc;?>" data-type="<?php echo $type;?>">
		<?php $c_gr=""; $gr="";if($color->abr_g!="" || $color->abr_g!=NULL){ $gr=$color->nombre_g." - ";$c_gr=$color->abr_g." - ";}
		$pgc=$color->idpgrc."-".rand(1,9999);
		$img="sistema/miniatura/default.jpg";
		if($color->portada!="" && $color->portada!=null){
			$v=explode("|", $color->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			}
		}else{
			if($color->portada_producto!="" && $color->portada_producto!=null){
				$v=explode("|", $color->portada_producto);
				if(count($v==2)){
					if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
					if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				}
			}
		}
		?>
		<td><div class="item item-2"></div><div class="img-thumbnail-50"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50"></div></td>
		<td><?php echo $gr.$color->nombre_c;?></td>
		<td>
	<?php if(!empty($procesos_empleado)){
			$idtb_2="tbl-proc-".rand(1,9999)."-".$color->idpgrc;
	?>
		<table class="table table-bordered table-hover" id="<?php echo $idtb_2;?>" style="margin-bottom: 0px;">
			<thead></thead>
			<tbody>
		<?php for($k=0; $k<count($procesos_empleado); $k++){ $proceso_empleado=$procesos_empleado[$k]; 
				$es_proceso=$this->lib->search_json($procesos,"idpr",$proceso_empleado->idpr);
				if($es_proceso!=null){
		?>
				<tr class="row-proceso" data-pre="<?php echo $proceso_empleado->idpre;?>">
					<td width="70%"><?php echo $proceso_empleado->nombre." (".$t[$proceso_empleado->tipo*1].")"; ?></td>
					<td width="30%">
					<?php $id_rango=rand(0,9999)."-".$color->idpgrc.'-'.$proceso_empleado->idpre; ?>
                        <div class="rangos" id="rangos<?php echo $id_rango;?>" data-pre="<?php echo $proceso_empleado->idpre;?>" data-pgc="<?php echo $color->idpgrc;?>" data-type-save="new">
							<input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
                        </div>
					</td>
					<td><span class="g-control-accordion" style="width: 54px; text-align: right;">
							<button class="drop_this_elemento" data-padre="row-proceso" style="padding-bottom: 0px;"><i class="icon-close"></i></button>
						</span>
					</td>
				</tr>
		<?php 	}// en if
			}?>
			</tbody>
		</table>
		<div class="row text-right" style="padding-right:15px; margin-top: 5px;">
			<button type="button" class="btn btn-flat flat-info txt-primary waves-effect btn-mini procesos_empleado" data-tbl="<?php echo $idtb_2; ?>" data-pgc="<?php echo $color->idpgrc;?>" data-p="<?php echo $producto->idp;?>" data-type="empleado_proceso">
				<i class="fa fa-plus"></i><span class="m-l-10">proceso</span>
			</button>
		</div>
		<script>$("button.drop_this_elemento").click(function(){$(this).drop_this_elemento();});$("button.procesos_empleado").click(function(){ $(this).procesos_empleado();});</script>
	<?php }else{ echo "0 prosesos asignados en el empleado...";}?>
		</td>
	</tr>