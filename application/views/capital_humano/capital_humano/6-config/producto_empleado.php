<?php
	$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/';
	$productos=$this->lib->productos_empleado($productos_empleado,$productos_grupos_colores);
	$tipo="";$vd=[];
	foreach($productos as $key => $producto){
		$grupos=json_decode(json_encode($producto->grupos));
		foreach($grupos as $key => $grupo){
			$procesos=json_decode(json_encode($grupo->procesos));
			foreach($procesos as $key => $proceso){
				$vd[]=array('idproe'=>$proceso->idproe,'status' => '1');
			}
		}
	}
	$detalles=json_encode($vd);
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
	$rand=rand(10,999999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link config_empleado<?php echo $rand;?>" href="javascript:" role="tab" data-e='<?php echo $empleado->ide;?>'><i class="icofont icofont-home"></i>Modificar empleado</a></li>
	<li class="nav-item"><a class="nav-link producto_empleado<?php echo $rand;?> active" href="javascript:" role="tab" data-e="<?php echo $empleado->ide;?>"><i class="icofont icofont-ui-user"></i>Productos</a></li>
</ul>
<div class="list-group" id="datos-empleado" data-e="<?php echo $empleado->ide;?>">
	<div class="list-group-item" style="max-width:100%">
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group col-xs-12">
				<div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true">
					<?php if(!empty($productos_empleado)){?>
					<?php $c1=1;
						foreach($productos as $key => $producto){ 
							$id=$producto->idp."-".rand(0,999999);
							$p_img="sistema/miniatura/default.jpg";
							if($producto->portada!="" && $producto->portada!=null){
								$v=explode("|", $producto->portada);
								if(count($v==2)){
									if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
									if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
								}
							}
							$grupos=json_decode(json_encode($producto->grupos));
							$vd=[];
							foreach($grupos as $key => $grupo){
								$procesos=json_decode(json_encode($grupo->procesos));
								foreach($procesos as $key => $proceso){
									$vd[]=array('idproe'=>$proceso->idproe,'status' => '1');
								}
							}
							$detalles=json_encode($vd);
						?>
							<div class="accordion-panel" id="accordion-panel<?php echo $id;?>">
								<div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
									<span class="card-title accordion-title">
											<?php $tbl_refresh="tbl-refresh-".$producto->idp.'-'.rand(1,9999);?>
											<span class="g-control-accordion">
												<button class="refresh_producto_empleado" data-p="<?php echo $producto->idp;?>" data-tbl="<?php echo $tbl_refresh;?>"><i class="fa fa-refresh"></i></button>
												<button class="drop_producto" data-padre="accordion-panel"><i class="icon-bin"></i></button>
											</span>
										<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
										<div class="item item-1"><?php echo $c1++;?></div>
											<span class="g-img-modal"><img src="<?php echo $url.$p_img;?>" class="img-thumbnail g-thumbnail-modal"></span>
											<span><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
										</a>
									</span>
								</div>
								<div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
									<div class="accordion-content accordion-desc" id="<?php echo $tbl_refresh;?>" data-p="<?php echo $producto->idp;?>">
										<div class="g-table-responsive">
											<?php $idpe_db=$producto->idp."-".rand(5,100);?>
											<?php $idtb="prod".$producto->idp.'-'.rand(0,999999);?>
											<span class="productos_empleado_db" id="<?php echo $idpe_db;?>" style="display: none;"><?php echo $detalles;?></span>
											<table class="table table-bordered" id="<?php echo $idtb;?>">
												<thead>
													<tr>
										                <th class="img-thumbnail-50"><div class="img-thumbnail-50"></div>#Item</th>
										                <th width="30%">Producto</th>
										                <th width="70%">Proceso - calidad</th>
													</tr>
												</thead>
												<tbody class="producto">
													<?php if(!empty($grupos)){ $c2=1; ?>
													<?php foreach ($grupos as $key => $grupo){ ?>
													<tr class="row-producto" id="<?php echo $grupo->idpgrc."-".rand(10,99999);?>" data-pgc="<?php echo $grupo->idpgrc;?>">
														<?php $c_gr=""; $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";$c_gr=$grupo->abr_g." - ";}
														$pgc=$grupo->idpgrc."-".rand(1,9999);
														$img="sistema/miniatura/default.jpg";
														if($grupo->portada!="" && $grupo->portada!=null){
															$v=explode("|", $grupo->portada);
															if(count($v==2)){
																if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
																if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
															}
														}else{
															$img=$p_img;
														} ?>
														<td class="img-thumbnail-50"><div class="item item-2"><?php echo $c2++;?></div><div class="img-thumbnail-50"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50"></div></td>
														<td><?php echo $gr.$grupo->nombre_c;?></td>
														<td>
													<?php if(count($grupo->procesos)>0){
															$procesos=json_decode(json_encode($grupo->procesos));
															$idtb_2="tbl-proc".rand(0,99999)."-".$grupo->idpgrc;
													?>
															<table class="table table-bordered table-hover" id="<?php echo $idtb_2;?>" style="margin-bottom: 0px;">
																<thead></thead>
																<tbody>
															<?php foreach ($procesos as $key => $proceso) {?>
																	<tr class="row-proceso" data-pre="<?php echo $proceso->idpre;?>" data-proe="<?php echo $proceso->idproe;?>" data-save="update" data-json-db="<?php echo $idpe_db;?>">
																		<td width="70%"><?php echo $proceso->nombre_proceso." (".$t[$proceso->tipo].")"; ?></td>
																		<td width="30%">
											                            <?php $id_rango=rand(0,9999)."-".$grupo->idpgrc.'-'.$proceso->idpre; ?>
											                              <div class="rangos" id="rangos_proe<?php echo $id_rango;?>" data-type-save="update" data-proe="<?php echo $proceso->idproe;?>">
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad>=10){?> checked="checked"<?php } ?> data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==9){?> checked="checked"<?php } ?> data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==8){?> checked="checked"<?php } ?> data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==7){?> checked="checked"<?php } ?> data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==6){?> checked="checked"<?php } ?> data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==5){?> checked="checked"<?php } ?> data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==4){?> checked="checked"<?php } ?> data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==3){?> checked="checked"<?php } ?> data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==2){?> checked="checked"<?php } ?> data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
											                                  <input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" <?php if($proceso->calidad==1){?> checked="checked"<?php } ?> data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
											                              </div>
																		</td>
																		<td>
																			<div class="g-control-accordion" style="width:54px;">
																				<button class="update_producto_empleado" data-proe="<?php echo $proceso->idproe;?>" data-rangos="<?php echo $id_rango;?>"><i class="icon-floppy-o"></i></button>
																			    <button class="drop_this_elemento" data-proe="<?php echo $proceso->idproe;?>" data-padre="row-proceso"><i class="icon-trashcan2"></i></button>
																			</div>
																		</td>
																	</tr>
															<?php } ?>
																</tbody>

															</table>
															<div class="row text-right" style="padding-right:15px; margin-top: 5px;">
																<button type="button" class="btn btn-flat flat-info txt-primary waves-effect btn-mini procesos_empleado" data-tbl="<?php echo $idtb_2;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-p="<?php echo $producto->idp;?>" data-type="empleado_proceso">
																	<i class="fa fa-plus"></i><span class="m-l-10">proceso</span>
																</button>
															</div>
													<?php
														}else{
															echo "<h4>0 procesos asignados en el empleado</h4>";
														} ?>
														</td>
															
															<?php } ?>
														</tr>          
														<?php }else{ ?>
														<tr><td colspan="7"><h3>0 colores registrados en el producto</h3></td></tr>
														<?php } ?>
												</tbody>
											</table>
										</div>
										<div class="row text-right" style="padding-right:15px; margin-top: 5px;">
											<button type="button" class="btn btn-inverse-info waves-effect btn-mini colores_producto" data-tbl="<?php echo $idtb;?>" data-p="<?php echo $producto->idp;?>" data-type="new_prod_emp">
												<i class="fa fa-plus"></i><span class="m-l-10">categoría</span>
											</button>
											<button type="button" class="btn btn-inverse-primary waves-effect btn-mini update_producto_colores_empleado" data-tbl="<?php echo $idtb;?>" data-tbl-refresh="<?php echo $tbl_refresh;?>" data-p="<?php echo $producto->idp;?>">
												<i class="fa fa-floppy-o"></i><span class="m-l-10">guardar</span>
											</button>
										</div>
										<script>$("button.update_producto_colores_empleado").click(function(){ $(this).update_producto_colores_empleado();});</script>
									</div>
								</div>
									<script> $("button.colores_producto").click(function(){ $(this).colores_producto();});</script>
									<script>$(".view_prod").click(function(){ $(this).view_producto();});$('[data-toggle="popover"]').popover({html:true});</script>
							</div>
									<?php } ?>
					<?php }//end if ?>
				</div>
			</div>
		</div><br>
		<div class="row text-right" style="padding-right:15px;">
          	<button type="button" class="btn btn-primary btn-mini waves-effect waves-light view_producto" data-type="emp_new_prod">
          		<i class="fa fa-plus"></i><span class="m-l-10">Adicionar producto</span>
            </button>
		</div>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$(".config_empleado<?php echo $rand;?>").config_empleado();$(".producto_empleado<?php echo $rand;?>").producto_empleado();



$("button.refresh_producto_empleado").click(function(){ $(this).refresh_producto_empleado();});  $("button.view_producto").click(function(){ $(this).view_producto();});$("button.procesos_empleado").click(function(){ $(this).procesos_empleado();});
	$("button.update_producto_empleado").click(function(){ $(this).update_producto_empleado();});$("button.drop_this_elemento").click(function(){$(this).drop_this_elemento();});
	$("button.drop_producto").click(function(){ $(this).drop_producto();});
</script>