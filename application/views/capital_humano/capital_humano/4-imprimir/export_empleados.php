<?php
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Empleados-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Empleados-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$url=base_url().'libraries/img/';
	$tipo_contrato = array('Tiempo completo', 'Medio tiempo');
	$estado = array('Inactivo', 'Activo');
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3>REPORTE DE EMPLEADOS</h3></center>
<table border="1" cellpadding="5" cellspacing="0">
	<thead>
		<tr class="fila">
			<th>#</th>
		<?php if($file=="doc"){?>
			<th>Fotografía</th>
		<?php }?>
			<th>Cód. bio.</th>
			<th>CI</th>
			<th>Nombre completo</th>
			<th>Grado académico</th>
			<th>Fecha de nacimiento</th>
			<th>Edad</th>
			<th>Telf. o Cel.</th>
			<th>Email</th>
			<th>Fecha de ingreso</th>
			<th>Cargo</th>
			<th>Calidad de producción</th>
			<th>Antiguedad</th>
			<th>Tipo de empleado</th>
			<th>Areas de trabajo</th>
			<th>Tipo de contrato</th>
		<?php if($privilegio->ca2r==1){ ?>
			<th>Salario [Bs.]</th>
		<?php }?>
			<th>Estado</th>
			<th>Características</th>
		</tr>
	</thead>
	<tbody>
		<?php $cont=0;
		for($i=0; $i < count($empleados); $i++){ $empleado=$empleados[$i];
			$img='sistema/miniatura/default.jpg';
			if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img="personas/miniatura/".$empleado->fotografia;}
			$calidad_produccion=0;
			$total_registros=0;
			$text_procesos="";
			$procesos_empleado=$this->lib->select_from($procesos_empleados,"ide",$empleado->ide);
			for($j=0;$j<count($procesos_empleado);$j++){$proceso_empleado=json_decode($procesos_empleado[$j]);
				$text_procesos.="- ".$proceso_empleado->nombre;
				if($proceso_empleado->tipo==0){$text_procesos.=" (Maestro)<br/>";}else{$text_procesos.=" (Ayudante) <br/>";}
				$producto_empleado=$this->lib->search_elemento($productos_empleados,'idpre',$proceso_empleado->idpre);
				if($producto_empleado!=null){
					$calidad_produccion+=$producto_empleado->calidad;
					$total_registros++;
				}
			}
			if($total_registros>0){$calidad_produccion=number_format(($calidad_produccion/$total_registros),0,'.','');}
			if($calidad_produccion>0){if($calidad_produccion>1){$calidad_produccion.="pts.";}else{$calidad_produccion.="pt.";}}else{ $calidad_produccion="";}
	?>
		<tr>
			<td><?php echo $i+1;?></td>
		<?php if($file=="doc"){?>
			<td><img src="<?php echo $url.$img;?>" style="width: 50px;"></td>
		<?php }?>
			<td><?php echo $empleado->codigo;?></td>
			<td><?php echo $empleado->ci.$empleado->abreviatura;?></td>
			<td><?php echo $empleado->nombre_completo;?></td>
			<td><?php echo $empleado->grado_academico;?></td>
			<td><?php if($this->val->fecha($empleado->fecha_nacimiento)){echo $this->lib->format_date($empleado->fecha_nacimiento,'d/m/Y');}?></td>
			<td><?php echo $this->lib->calcula_edad($empleado->fecha_nacimiento);?></td>
			<td><?php echo $empleado->telefono;?></td>
			<td><?php echo $empleado->email;?></td>
			<td><?php if($this->val->fecha($empleado->fecha_ingreso)){echo $this->lib->format_date($empleado->fecha_ingreso,'d/m/Y');}?></td>
			<td><?php echo $empleado->cargo;?></td>
			<td><?php echo $calidad_produccion;?></td>
			<td><?php echo $this->lib->antiguedad($empleado->fecha_ingreso);?></td>
			<td><?php if($empleado->tipo==0){ echo "Empleado en planta"; }if($empleado->tipo==1){ echo "Empleado administrativo";}?></td>
			<td><?php echo $text_procesos;?></td>
			<td><?php echo $tipo_contrato[$empleado->tipo_contrato]." (".$empleado->horas."hrs.)";?></td>
		<?php if($privilegio->ca2r==1){ ?>
			<td><?php echo number_format($empleado->salario,2,'.',',');?></td>
		<?php }?>
			<td><?php echo $estado[$empleado->estado];?></td>
			<td><?php echo $empleado->descripcion;?></td>
		</tr>
		<?php }//end for ?>
	</tbody>
</table>