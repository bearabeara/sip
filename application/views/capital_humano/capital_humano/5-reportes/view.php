<?php 
	$url=base_url().'libraries/img/personas/miniatura/';
	$img='default.png';
	$contPagina=1;
	$nombre=$empleado->nombre;
	if($empleado->nombre2!=""){ $nombre.=" ".$empleado->nombre2; }
	if($empleado->paterno!=""){ $nombre.=" ".$empleado->paterno; }
	if($empleado->materno!=""){ $nombre.=" ".$empleado->materno; }
	$rand=rand(10,9999999);
?>
<ul class="nav nav-tabs" role="tablist" id="datos-empleado" data-e="<?php echo $empleado->ide;?>">
	<li class="nav-item">
		<a class="nav-link detalle_empleado<?php echo $rand;?> active" href="javascript:" role="tab" data-e='<?php echo $empleado->ide;?>'><i class="icofont icofont-home"></i>Empleado</a>
	</li>
	<li class="nav-item">
		<a class="nav-link produccion_empleado" href="javascript:" role="tab" data-e="<?php echo $empleado->ide;?>"><i class="icofont icofont-ui-user"></i>Produccion</a>
	</li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<?php $fun = array('function' => 'detalle_empleado', 'atribs' => array('e' => $empleado->ide));
			$default = array('top' => '1','right' => '1','bottom' => '1','left' => '1.5');
			$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'default' => json_encode($default)]);?>
	</div>
	<div class="list-group-item" style="max-width:100%">
<div class="detalle table-responsive" id="area">
	<table class="tabla tbl-bordered font-10">
		<thead>
			<tr class="fila title" style="text-align: center;">
				<td class="celda title padding-4" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
					<?php $this->load->view('estructura/print/header-print',['titulo'=>'CAPITAL HUMANO']);?>
				</td>
			</tr>
		</thead>
		<tr class='fila'>
		<?php if($empleado->fotografia!="" & $empleado->fotografia!=NULL){ $img=$empleado->fotografia; } ?>
			<td class="celda td padding-4 text-center" width="20%"><img src="<?php echo $url.$img; ?>" width='80%' class="img-thumbnail"></td>
			<td class="celda td padding-4" style="text-align: center;" width="80%"><span style="font-size: 1.9rem;"><?php echo $nombre;?></span><p style="font-size: 1.3rem;"><?php echo $empleado->cargo; ?></p></td>
		</tr>
		<tr class="fila">
			<td class="celda td padding-4" style="font-size: 1.2rem; text-align: right;"><strong>Datos personales</strong></td>
			<td class="celda td padding-4">
				<table border="0" width="100%">
					<tr>
						<th width="40%" style="text-align: left;">Cédula de Identidad:</th>
						<td width="60%"><?php echo $empleado->ci." ".$empleado->abreviatura;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Pais de origen:</th>
						<td width="60%"><?php echo $empleado->pais;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Fecha de Nacimiento:</th>
						<td width="60%"><?php echo $this->lib->format_date($empleado->fecha_nacimiento,'dl ml Y')?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Teléfono:</th>
						<td width="60%"><?php if($empleado->telefono!=0 && $empleado->telefono!=''){ echo $empleado->telefono;}?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Correo Electrónico:</th>
						<td width="60%"><?php if($empleado->email!=null && $empleado->email!=''){ echo $empleado->email;}?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Dirección:</th>
						<td width="60%"><?php if($empleado->direccion!=null && $empleado->direccion!=''){ echo $empleado->direccion;}?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Edad:</th>
						<td width="60%"><?php echo $this->lib->calcula_edad($empleado->fecha_nacimiento);?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="fila">
			<td class="celda td padding-4" style="font-size: 1.2rem; text-align: right;"><strong>Datos laborales</strong></td>
			<td class="celda td padding-4">
				<table border="0" width="100%">
					<tr>
						<th width="40%" style="text-align: left;">Código biométrico:</th>
						<td width="60%"><?php echo $empleado->codigo;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Fecha de ingreso:</th>
						<td width="60%"><?php echo $this->lib->format_date($empleado->fecha_ingreso,'dl ml Y')?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Antiguedad:</th>
						<td width="60%"><?php echo $this->lib->antiguedad($empleado->fecha_ingreso);?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Tipo de Contrato:</th>
						<td width="60%"><?php if($empleado->tipo_contrato==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";}?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Carga Horaria:</th>
						<td width="60%"><?php echo $empleado->horas;?> Hrs/dia</td>
					</tr>
				<?php if($privilegio[0]->ca2r==1){ ?>
					<tr>
						<th width="40%" style="text-align: left;">Suedo basico:</th>
						<td width="60%"><?php echo number_format($empleado->salario,2,'.',',');?> Bs.</td>
					</tr>
				<?php } ?>
					<tr>
						<th width="40%" style="text-align: left;">Calidad de producción:</th>
						<td class="text-left">
							<div class="rangos" style="width: <?php echo ($calidad_produccion)*22.4;?>px; float: left;">
					<?php for($i=0; $i<($calidad_produccion); $i++){ ?>
								<label class="rango fa fa-star" style="color: #1b8bf9;"></label>
					<?php } ?>
							</div>
							<div class="rango-pointer" style="float: left; margin-left: 10px;"><span class="label label-inverse-primary label-md"><?php echo $calidad_produccion." pts."?></span></div>
						</td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Grado académico:</th>
						<td width="60%"><?php echo $empleado->grado_academico;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Areas de Trabajo:</th>
						<td width="60%"><?php
							if(!empty($procesos)){
								if($procesos[0]->tipo==0){ $tipo="Maestro";}else{ $tipo="Ayudante";}
								$resp=$procesos[0]->nombre."(".$tipo.")";
								for($i=1; $i < count($procesos); $i++){ $proceso=$procesos[$i];
									if($proceso->tipo==0){ $tipo="Maestro";}else{ $tipo="Ayudante";}
									$resp.=", ".$proceso->nombre."(".$tipo.")";
								}
								echo $resp;
							}
						?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="fila">
			<td class="celda td padding-4" style="font-size: 1.2rem; text-align: right;"><strong>Datos generales</strong></td>
			<td class="celda td padding-4">
				<table border="0" width="100%">
					<tr>
						<th width="40%" style="text-align: left;">Caracteristicas Personales:</th>
						<td width="60%"><?php echo $empleado->descripcion;?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</div>
</div>
<script>$("a.detalle_empleado<?php echo $rand;?>").detalle_empleado();$("a.produccion_empleado").produccion_empleado()</script>
	