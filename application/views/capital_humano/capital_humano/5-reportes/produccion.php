<?php $url=base_url().'libraries/img/';
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
	$rand=rand(10,9999999);
?>
<ul class="nav nav-tabs" role="tablist" id="datos-empleado" data-e="<?php echo $empleado->ide;?>">
	<li class="nav-item">
		<a class="nav-link detalle_empleado<?php echo $rand;?>" href="javascript:" role="tab" data-e='<?php echo $empleado->ide;?>'><i class="icofont icofont-home"></i>Empleado</a>
	</li>
	<li class="nav-item">
		<a class="nav-link active produccion_empleado" href="javascript:" role="tab" data-e="<?php echo $empleado->ide;?>"><i class="icofont icofont-ui-user"></i>Produccion</a>
	</li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
<div class="g-table-responsive" id="area">
	
	<table class="tabla tbl-bordered">
		<thead>
			<tr class="fila title" style="text-align: center;">
				<td class="celda td title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
					<?php $this->load->view('estructura/print/header-print',['titulo'=>'CALIDAD DE PRODUCCION','sub_titulo'=>$empleado->nombre_completo]);?>
				</td>
			</tr>
			<tr class="fila">
				<th class="celda th"><div class="img-thumbnail-40"></div>#Item</th>
				<th width="20%" class="celda th padding-4">Código</th>
				<th width="40%" class="celda th padding-4">Producto</th>
				<th width="20%" class="celda th padding-4">Proceso</th>
				<th width="20%" class="celda th padding-4">Calidad</th>
			</tr>
		</thead>
		<tbody>
	<?php $count=1;
		foreach ($productos as $key => $producto){
	?>
			<tr class="fila padding-4">
				<td class='celda td'><div class="item"><?php echo $count++; ?></div><img src="<?php echo $producto->img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-40" style="max-width: 40px;"></td>
				<td class='celda td'><?php echo $producto->codigo; ?></td>
				<td class='celda td'><?php echo $producto->nombre; ?></td>
				<td class='celda td'><?php echo $producto->nombre_proceso; ?></td>
				<td class="text-right celda td">
					<div class="rangos" style="width: <?php echo ($producto->calidad+1)*22.6;?>px;">
			<?php for($i=0; $i<($producto->calidad); $i++){ ?>
						<label class="rango fa fa-star" style="color: #1b8bf9;"></label>
			<?php } ?>
					</div>
				</td>
			</tr>
	<?php
		}// end for
	?>
		</tbody>
	</table>
</div>
</div>
</div>
<script>$("a.detalle_empleado<?php echo $rand;?>").detalle_empleado();$("a.produccion_empleado").produccion_empleado()</script>
	