<?php
	$help1='title="Entidad bancaria" data-content="Seleccione la entidad bancaria de la persona."';
	$help2='title="N° de cuenta" data-content="Ingrese el número de cuenta de la persona en la entidad bancaria, ingrese un valor entero de 4 a 100 digitos."';
	$help3='title="Tipo de cuenta" data-content=" Seleccione el tipo de cuenta de la persona."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$c = array(1 => 'Cuenta corriente', 2 => 'Caja de ahorros', 3 => 'Cuenta remunerada');
 ?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="form-group">
			<div class="col-sm-3 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class="text-danger">(*)</span> Entidad bancaria:</label></div>
			<div class="col-sm-9 col-xs-12">
				<div class="input-group">
					<select class="form-control form-control-xs" id="b3_ban">
						<option value="">Seleccionar...</option>
				<?php for($i=0; $i<count($bancos); $i++){$banco=$bancos[$i]; ?>
						<option value="<?php echo $banco->idba;?>">
							<?php echo $banco->razon;?>
						</option>
				<?php } ?>
					</select>
					<span class="input-group-addon form-control-sm view_bancos" data-container="b3_ban"><i class='fa fa-cog'></i></span>
					<span class="input-group-addon form-control-sm new_banco" data-container="b3_ban" data-type="new"><i class='fa fa-plus'></i></span>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-3 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class="text-danger">(*)</span> N° de cuenta:</label></div>
			<div class="col-sm-9 col-xs-12">
				<form class="append_cuenta_banco" data-container="<?php echo $container;?>">
					<div class="input-group">
						<input type="text" class="form-control form-control-xs color-class" id="b3_cta" placeholder='Número de cuenta bancaria' minlength="4" maxlength="100">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-3 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class="text-danger">(*)</span> Tipo de cuenta:</label></div>
			<div class="col-sm-9 col-xs-12">
				<div class="input-group">
					<select class="form-control form-control-xs" id="b3_tip">
						<option value="">Seleccionar...</option>
				<?php for($i=1; $i<=count($c); $i++){ ?>
						<option value="<?php echo $i;?>"><?php echo $c[$i];?></option>
				<?php } ?>
					</select>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$(".view_bancos").view_bancos();$("span.new_banco").new_banco();$("form.append_cuenta_banco").submit(function(e){$(this).append_cuenta_banco(); e.preventDefault();});</script>