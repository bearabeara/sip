<table table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm'><div class="g-img-modal"></div></td>
		<td style="width:24%">
			<form class="view_salida" data-type="search">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class='form-control form-control-sm search_salida' maxlength="100" placeholder='Buscar nombre...' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="1"/>
					<span class="input-group-addon form-control-sm view_salida" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' style="width:12%">
			<form class="view_salida" data-type="search">
				<input class='form-control form-control-sm search_salida' type="number" id="s_can" placeholder='Stock' min='0' data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="like" data-col="2">
			</form>
		</td>
		<td class='hidden-sm' style="width:12%">
			<select class='form-control form-control-sm search_salida' id="s_uni" title="Unidad de medida" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="3">
				<option value="">Seleccionar...</option>
				<?php for($i=0; $i<count($unidad);$i++){$res=$unidad[$i];?>
					<option value="<?php echo $res->idu; ?>"><?php echo $res->nombre;?></option>	
				<?php } ?>
			</select>
		</td>
		<td class='hidden-sm' style="width:47%"></td>
		<td class='hidden-sm' style="width:5%">
			<?php 
				$search=json_encode(array('function'=>'view_salida','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_salida','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>Onfocus('s_nom');$(".search_salida").search_salida();$(".view_salida").view_salida();</script>