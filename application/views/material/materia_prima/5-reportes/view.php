<?php 
	$material=$material[0]; 
	$url=base_url().'libraries/img/';
	$img="sistema/miniatura/default.jpg";
	if($material->fotografia!="" && $material->fotografia!=NULL){
		$img="materiales/miniatura/".$material->fotografia;
	}
	$rand=rand(10,999999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a href="javascript:" role="tab" class="nav-link active reportes_material" data-am='<?php echo $material->idam;?>'>Detalle material</a></li>
	<li class="nav-item"><a href="javascript:" role="tab" class="nav-link material_productos" data-am="<?php echo $material->idam;?>"><?php echo $material->nombre; ?> en uso</a></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-<?php echo $rand;?>" alt="img" data-title="<?php echo $material->nombre;?>" data-desc="<?php if(strlen($material->descripcion)>0){ echo $material->descripcion;}else{ echo '<br>';}?>"><hr></div>
			<div class="col-sm-9 cols-xs-12">
				<div class="table-responsive">
					<table border="0" class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda th">Código:</th>
							<td class="celda td" colspan="3"><?php echo $material->codigo;?></td>
						</tr>
						<tr class="fila">
							<th class="celda th">Nombre:</th><td class="celda td" colspan="3"><?php echo $material->nombre;?></td>
						</tr>
						<tr class="fila">
							<th class="celda th" width="17.5%">Grupo:</th><td class="celda td" width="17.5%"><?php echo $material->nombre_g;?></td>
							<th class="celda th" width="17.5%">Cantidad:</th><td class="celda td" width="17.5%"><?php echo $material->cantidad;?></td>
						</tr>
						<tr class="fila">
							<th class="celda th" width="17.5%">Unidad de medida:</th><td class="celda td" width="17.5%"><?php echo $material->medida;?></td>
							<th class="celda th" width="17.5%">Color:</th><td class="celda td" width="17.5%"><?php echo $material->nombre_c;?></td>
						</tr>
						<tr class="fila">
							<th class="celda th">Costo promedio por unidad (Bs.):</th><td class="celda td" colspan="3"><?php echo $material->costo_unitario;?></td>
						</tr>
						<tr class="fila">
							<th class="celda th">Descripción:</th><td class="celda td" colspan="3"><?php echo $material->descripcion;?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>$("a.material_productos").material_productos();$('a.reportes_material').reportes_material();$("img.img-thumbnail-<?php echo $rand;?>").visor();</script>