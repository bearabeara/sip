<?php $j_usuarios=json_encode($usuarios);
$url=base_url().'libraries/img/'; ?>
<table class="table table-small-font table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class="img-thumbnail-60"><div class="img-thumbnail-60">#Item</div></th>
			<th class="celda-sm-10 hidden-sm">NIT/CI</th>
			<th class="celda-sm-30">Nombre completo</th>
			<th class="hidden-sm" width="15%">Teléfono</th>
			<th class="hidden-sm" width="15%">Cargo</th>
			<th width="19%">Usuario</th>
			<th class="hidden-sm" width="11%"></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if(count($usuarios)>0){
		for ($j=0; $j < count($usuarios); $j++) { $usuario=$usuarios[$j]; 
				$img="sistema/miniatura/default.jpg";
				if($usuario->fotografia!='' && $usuario->fotografia!=NULL){ $img="personas/miniatura/".$usuario->fotografia; } ?>
			<tr data-us="<?php echo $usuario->idus;?>">
			<td class="img-thumbnail-60"><div id="item"><?php echo $j+1;?></div><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-60" data-title="<?php echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno;?>" data-desc="<br>"></td>
				<td class="hidden-sm"><?php echo $usuario->ci.' '.$usuario->abreviatura;?></td>
				<td><?php echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno; ?></td>
				<td class="hidden-sm"><?php echo $usuario->telefono;?></td>
				<td class="hidden-sm"><?php echo $usuario->cargo;?></td>
				<td><?php echo $usuario->usuario;?></td>
				<td class="hidden-sm"></td>
				<td>
				<?php 
					$det=json_encode(array('function'=>'detalle_usuario','atribs'=> json_encode(array('us' => $usuario->idus)),'title'=>'Detalle'));
					$conf=""; if($privilegio->ad1u=="1"){ $conf=json_encode(array('function'=>'config_usuario','atribs'=> json_encode(array('us' => $usuario->idus)),'title'=>'Configurar'));}
					$del=""; if($privilegio->ad1d=="1"){ $del=json_encode(array('function'=>'confirm_usuario','atribs'=> json_encode(array('us' => $usuario->idus)),'title'=>'Eliminar'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
				</td>
			</tr>
		<?php }
		}else{ ?>
			<tr><td colspan="11"><h2>0 Registros encontrados...</h2></td></tr>
		<?php } ?>
	</tbody>
</table>
<?php if($privilegio->ad1r=="1" && ($privilegio->ad1p=="1" || $privilegio->ad1c=="1")){?>
	<div id="fab-usuario">
		<div id="btns-fab-wrapper" class="btns-fab-wrapper">
		<?php if($privilegio->ad1r=="1" && $privilegio->ad1p=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-yellow-darken waves-effect waves-light print_usuarios" data-tbl="table#tbl-container" data-toggle="tooltip" data-placement="left" title="Reportes">
			        <span><i class="icon-clipboard7"></i></span>
			    </button>
			</div>
		<?php }?>
		<?php if($privilegio->ad1r=="1" && $privilegio->ad1c=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-success waves-effect waves-light new_usuario" data-toggle="tooltip" data-placement="left" title="Nuevo usuario">
			        <span>+</span>
			    </button>
			</div>
		<?php }?>
		</div>
		<button id="btn-fab-main" class="gfab-main-btn bg-danger waves-effect waves-light"><span><i class="icon-menu"></i></span></button>
	</div>
<?php }?>
<script>$("img.img-thumbnail-60").visor();$(".detalle_usuario").detalle_usuario();$(".config_usuario").config_usuario();$(".confirm_usuario").confirm_usuario();
<?php if($privilegio->ad1r=="1" && $privilegio->ad1c=="1" || $privilegio->ad1p=="1"){?>
	$("div#fab-usuario").jqueryFab();
	<?php if($privilegio->ad1c=="1"){?>
		$("button.new_usuario").new_usuario();
	<?php }?>
	<?php if($privilegio->ad1p=="1"){?>
		$("button.print_usuarios").print_usuarios();
	<?php }?>
<?php }?></script>