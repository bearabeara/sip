<?php 
$rand=rand(10,999999999);
switch ($col) {
	case 'al': 
?>
<div class="row">
	<div class="col-sm-8 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>ALMACENES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th><th>Ayuda</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al1a==1){ ?>
								<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1a" data-color="grey" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al1a" data-color="grey" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-4 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>MOV. DE MATERIALES</ins></th></tr>
					<tr><th>Ver</th><th>Imprimir</th><th>Eliminar</th><th>Ayuda</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al2p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al2d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al2a==1){ ?>
								<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2a" data-color="grey" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al2a" data-color="grey" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-4 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="3" class="text-center"><ins>MOV. DE PRODUCTOS</ins></th></tr>
					<tr><th>Ver</th><th>Imprimir</th><th>Eliminar</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->al3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al3r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al3r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al3p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al3p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al3p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al3d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al3d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al3d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-sm-8 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>MATERIALES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th><th>Ayuda</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al11r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al11c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al11p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al11u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al11d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al11a==1){ ?>
								<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11a" data-color="grey" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al11a" data-color="grey" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-4 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>IMGRESOS</ins></th></tr>
					<tr><th>Ver</th><th>Modificar</th><th>Ayuda</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al12r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al12r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al12r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al12u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al12u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al12u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al12a==1){ ?>
								<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al12a" data-color="grey" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al12a" data-color="grey" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-4 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>SALIDAS</ins></th></tr>
					<tr><th>Ver</th><th>Modificar</th><th>Ayuda</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->al13r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al13r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al13r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al13u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al13u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al13u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al13a==1){ ?>
								<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al13a" data-color="grey" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al13a" data-color="grey" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>CONFIGURACION</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Modificar</th><th>Eliminar</th><th>Ayuda</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->al15r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al15c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al15u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al15d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->al15a==1){ ?>
								<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15a" data-color="grey" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="al15a" data-color="grey" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>		
</div>
<?php
	break;
	case 'pr':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>PRODUCTOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->pr1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>PRODUCCIÓN</ins></th></tr>
					<tr><th>Ver</th><th>Adicionar</th><th>Imprimir</th><th>Configurar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->pr2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr2p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr2u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr2d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr2d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>CONFIGURACIÓN</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Modificar</th><th>Eliminar</th><th>Hoja de costos</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->pr5r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr5c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr5u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr5d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->pr5hc==1){ ?>
								<div class="btn-circle btn-circle-purple btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5hc" data-color="purple" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="pr5hc" data-color="purple" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'mo':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>PEDIDOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th><th>Adelanto</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->mo1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo1a==1){ ?>
								<div class="btn-circle btn-circle-purple btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1a" data-color="purple" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo1a" data-color="purple" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>VENTAS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->mo2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo2d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo2d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>COMPRAS Y/O EGRESOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->mo3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->mo3d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="mo3d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ca':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>CAPITAL HUMANO</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ca1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-4 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>PLANILLA</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ca2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca2p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca2u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca2u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ca5r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca5r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca5r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>DIRECTIVOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ca3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca3c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca3p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca3u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ca3d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ca3d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
</div>
<?php
		break;
	case 'cl':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>CLIENTES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->cl1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>PROVEEDORES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->cl2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->cl2d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl2d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->cl5r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl5r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="cl5r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ac':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>ACTIVOS FIJOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ac1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ac1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIGURACION</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ac2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ac2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ot':
?>
<div class="row">
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>MATERIALES INDIRECTOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="2" class="text-center"><ins>INGRESO</ins></th></tr>
					<tr><th>Ver</th><th>Modificar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot11r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot11r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot11r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot11u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot11u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot11u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="2" class="text-center"><ins>INGRESO</ins></th></tr>
					<tr><th>Ver</th><th>Modificar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot12r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot12r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot12r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot12u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot12u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot12u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="2" class="text-center"><ins>HISTORIAL</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot13r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot13r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot13r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>OTROS MATERIALES E INSUMOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ot2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ot2d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot2d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ot3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot3r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ot3r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'co':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>COMPROBANTES</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->co1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>L. MAYOR</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-3 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>SUMAS SALDOS</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co3r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co3r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>E. CPV.</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co4r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co4r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co4r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>E. RESUL.</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co5r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co5r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co5r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>BALANCE</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co6r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co6r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co6r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="5" class="text-center"><ins>PLAN DE CUENTAS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->co7r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->co7d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co7d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center"><ins>CONFIG</ins></th></tr>
					<tr><th>Ver</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->co8r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co8r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="co8r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
	case 'ad':
?>
<div class="row">
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="6" class="text-center"><ins>USUARIOS</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Imprimir</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ad1r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1p==1){ ?>
								<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1p" data-color="yellow" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1p" data-color="yellow" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad1d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad1d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-2 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th class="text-center" colspan="2"><ins>PRIVILEGIOS</ins></th></tr>
					<tr><th>Ver</th><th>Mod.</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><center>
							<?php if($privilegio->ad2r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad2r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad2r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad2u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad2u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad2u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-5 col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr><th colspan="4" class="text-center"><ins>CONFIGURACION</ins></th></tr>
					<tr><th>Ver</th><th>Crear</th><th>Modificar</th><th>Eliminar</th></tr>
				</thead>
				<tbody>	
					<tr>
						<td><center>
							<?php if($privilegio->ad3r==1){ ?>
								<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3r" data-color="info" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3r" data-color="info" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad3c==1){ ?>
								<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3c" data-color="success" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3c" data-color="success" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad3u==1){ ?>
								<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3u" data-color="orange" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3u" data-color="orange" data-size="30"></div>
							<?php } ?>
						</center></td>
						<td><center>
							<?php if($privilegio->ad3d==1){ ?>
								<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3d" data-color="red" data-size="30"></div>
							<?php }else{ ?>
								<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio<?php echo $rand;?>" data-pri="<?php echo $privilegio->idpri;?>" data-col="ad3d" data-color="red" data-size="30"></div>
							<?php } ?>
						</center></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
		break;
}?>
<script>$("div.update_privilegio<?php echo $rand;?>").update_privilegio();</script>