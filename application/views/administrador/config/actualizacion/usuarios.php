<?php $url=base_url().'libraries/img/'; ?>
<div class="list-group" id="datos_config_cliente" data-content="<?php echo $content;?>">
	<div class="list-group-item" style="max-width:100%">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="img-thumbnail-50"></th>
					<th width="95%">Nombre</th>
					<th width="5%"></th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($usuarios)){ ?>
				<?php for ($i=0; $i < count($usuarios) ; $i++) { $usuario=$usuarios[$i];
						$control=true;
						foreach($asignados as $key => $asignado){ if($asignado==$usuario->idus){ $control=false; break;}}
							$img=$this->lib->img($usuario->fotografia,"personas");
				?>
					<tr>
						<td class="img-thumbnail-50">
							<div class="img-thumbnail-50"></div>
							<div class="item"><?php echo $i+1;?></div>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50" data-title="<?php echo $usuario->nombre_completo;?>" data-desc="">
						</td>
						<td><?php echo $usuario->nombre_completo; ?></td>
						<td>
							<button type="button" class="btn btn-<?php if($control){?>inverse-primary btn_add_user_config <?php }else{?>default<?php } ?> btn-mini waves-effect waves-light" <?php if($control){?> data-u="<?php echo $usuario->idus;?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light" <?php }else{?> disabled="disabled"<?php }?>><i class="fa fa-plus"></i> add</button>
						</td>
					</tr>
				<?php 
						}//end for ?>
				<?php }else{ ?>
					<tr><td colspan="3" class="text-center"><h4>0 usuarios registrados...</h4></td></tr>
				<?php } ?>
			</tbody>
			</table>
		</div>
</div>
<script>$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});$("button.btn_add_user_config").click(function(){ $(this).add_user_config();});</script>