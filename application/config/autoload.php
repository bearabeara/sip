<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Drivers
| 4. Helper files
| 5. Custom config files
| 6. Language files
| 7. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in system/libraries/ or your
| application/libraries/ directory, with the addition of the
| 'database' library, which is somewhat of a special case.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'email', 'session');
|
| You can also supply an alternative library name to be assigned
| in the controller:
|
|	$autoload['libraries'] = array('user_agent' => 'ua');
*/
$autoload['libraries'] = array('database','session','val','lib','resize','excel','phpwebsocket');

/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
| These classes are located in system/libraries/ or in your
| application/libraries/ directory, but are also placed inside their
| own subdirectory and they extend the CI_Driver_Library class. They
| offer multiple interchangeable driver options.
|
| Prototype:
|
|	$autoload['drivers'] = array('cache');
|
| You can also supply an alternative property name to be assigned in
| the controller:
|
|	$autoload['drivers'] = array('cache' => 'cch');
|
*/
$autoload['drivers'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('url','form');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/
$autoload['config'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('first_model', 'second_model');
|
| You can also supply an alternative model name to be assigned
| in the controller:
|
|	$autoload['model'] = array('first_model' => 'first');
*/
$autoload['model']=array('M_actualizacion','M_actualizacion_usuario','M_almacen','M_almacen_material','M_almacen_material_mov','M_almacen_material_seg','M_almacen_seg','M_atributo','M_banco','M_banco_persona','M_ciudad','M_cliente','M_color','M_compra','M_detalle_pedido','M_directivo','M_empleado','M_feriado','M_grupo','M_material','M_material_extra','M_material_extra_mov','M_material_grupo','M_material_item','M_material_vario','M_mensaje','M_notificacion','M_pago','M_descuento_pago','M_descuento_producto','M_detalle_descuento_producto','M_pais','M_parte_pedido','M_pedido','M_pedido_seg','M_persona','M_privilegio','M_proceso','M_proceso_empleado','M_producto','M_producto_atributo','M_producto_empleado','M_producto_grupo','M_producto_grupo_atributo','M_producto_grupo_color','M_producto_grupo_color_seg','M_producto_grupo_material','M_producto_grupo_proceso','M_producto_grupo_color_atributo','M_producto_grupo_color_material','M_producto_grupo_color_pieza','M_producto_grupo_color_proceso','M_producto_imagen','M_producto_imagen_color','M_producto_material','M_producto_pedido_empleado','M_producto_pieza','M_producto_proceso','M_producto_seg','M_proveedor','M_proveedor_material','M_repujado_sello','M_sucursal_cliente','M_sucursal_detalle_pedido','M_tipo_cambio','M_tipo_contrato','M_unidad','M_usuario');
